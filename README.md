# LISEZ MOI #

Petit guide rapide vous permet rapidement de télécharger et compiler les sources de SOFI Framework.

### Téléchargement des sources ###

Pour réaliser l’installation sur un poste de travail, il suffit de faire un clone du projet GIT en spécifiant la branche désirée. Par exemple, vous pouvez vous créer un dossier /Java/sources/sofi à l’endroit désiré et dans le répertoire sofi, vous pouvez faire votre clone du branche spécifique du référentiel de SOFI Framework.
 
Voici un exemple de téléchargement (git clone) de la branche release/4.0 pour sofi-framework.

```
#!java
git clone -b release/4.0 https://bitbucket.org/sofiframework/sofi-framework.git
```

### Compilation des sources ###

Les sources se compilent avec l’aide de Maven et les fichiers JAR définit par les modules sont créé est déposé dans le référentiel local du poste utilisateur. Il faut noter que l’outil de référentiel des librairies utilisés est bintray.com (de JFrog créateur de Artifactory) à l’adresse suivante : [https://bintray.com/sofiframework/repo/sofi-framework](Link URL).
 
Avant de lancer la compilation, vous devez créer un fichier settings.xml dans le dossier .m2 dans la racine de votre compte utilisateur (~/.m2). Il suffit de copier le fichier **settings-sample.xml** situé dans la racine du projet sofiframework dans votre fichier **settings.xml**. 

Après avoir configuré le fichier settings.xml. Vous pouvez ouvrir un terminal et de vous positionner à la racine du dossier sofiframework, la commande mvn install permet de tout installer correctement. 

Si le poste du développeur n’a pas Maven, il est possible d’appeler la commande mvnw.bat install (Windows) ou ./mvnw.cmd install (MacOS, Linux). 

Si vous avez déjà installé Maven, il est aussi possible de faire la simple commande suivante :

```
#!java

mvn clean install
```

### Outil de développement ###

Il est très simple maintenant d'utiliser votre outil de développement préféré ([Eclipse](https://www.eclipse.org/downloads/), [IntelliJ](https://www.jetbrains.com/idea/?fromMenu) ou [Oracle JDeveloper](http://www.oracle.com/technetwork/developer-tools/jdev/downloads/index.html)), car il suffit d'ouvrir en important un projet Maven. 

***Eclipse demeure toutefois celui recommandé.***

### Étapes suivantes ###

Le développement SOFI nécessite l'installation de son infrastructure ainsi que de l'Application d'authentification (SSO) pour les applications SOFI. 

Pour poursuivre, veuillez consulter le guide rapide d'installation disponible dans les dépôts des applications suivantes :

**SOFI Infrastructure** : https://bitbucket.org/sofiframework/sofi-infrastructure

**SOFI Login** : https://bitbucket.org/sofiframework/sofi-login