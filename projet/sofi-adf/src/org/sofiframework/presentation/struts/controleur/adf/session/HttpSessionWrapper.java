/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf.session;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import oracle.jbo.common.ampool.SessionCookie;
import oracle.jbo.http.HttpContainer;

import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.modele.adf.BaseServiceImpl;
import org.sofiframework.presentation.struts.controleur.UtilitaireControleur;
import org.sofiframework.presentation.struts.controleur.adf.RequestProcessorHelper;

import java.io.Serializable;

public class HttpSessionWrapper implements HttpSession, Serializable {
  
  private boolean invaliderSession;
  
  protected HttpSession session;

  /**
   * Est-ce que l'on doit invalider la session.
   * @return si la session doit être invalidée
   */
  public boolean isInvaliderSession() {
    return this.invaliderSession;
  }
  
  public HttpSessionWrapper(HttpSession session) {
      this.session = session;
  }
  
  public HttpSession getWrappedSession() {
      return session;
  }

  public Object getAttribute(String name) {
      return session.getAttribute(name);
  }

  public Enumeration getAttributeNames() {
      return session.getAttributeNames();
  }

  public long getCreationTime() {
      return session.getCreationTime();
  }

  public String getId() {
      return session.getId();
  }

  public long getLastAccessedTime() {
      return session.getLastAccessedTime();
  }

  public int getMaxInactiveInterval() {
      return session.getMaxInactiveInterval();
  }

  public javax.servlet.ServletContext getServletContext() {
      return session.getServletContext();
  }

  /**
   * @deprecated
   */
  public HttpSessionContext getSessionContext() {
      return session.getSessionContext();
  }

  /**
   * @deprecated
   */
  public Object getValue(String name) {
      return session.getValue(name);
  }

  /**
   * @deprecated
   */
  public String[] getValueNames() {
      return session.getValueNames();
  }
  
  /**
   * Au lieu d'invalider la session, on identifie 
   * la session comme devant être invalidée. 
   * On doit libérer le modèle BC4J avant 
   * d'invalider réellement la session.
   */
  public void invalidate() {
   this.invaliderSession = true; 
  }

  public boolean isNew() {
      return session.isNew();
  }

  /**
   * @deprecated
   */
  public void putValue(String name, Object value) {
      session.putValue(name, value);
  }

  public void removeAttribute(String name) {
      session.removeAttribute(name);
  }

  /**
   * @deprecated
   */
  public void removeValue(String name) {
      session.removeValue(name);
  }

  public void setAttribute(String name, Object value) {
      session.setAttribute(name, value);
  }

  public void setMaxInactiveInterval(int interval) {
      session.setMaxInactiveInterval(interval);
  }
}