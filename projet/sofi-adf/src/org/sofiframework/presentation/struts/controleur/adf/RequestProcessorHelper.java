/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.modele.adf.BaseServiceImpl;
import org.sofiframework.modele.adf.exception.ModeleADFException;
import org.sofiframework.presentation.struts.controleur.UtilitaireControleur;

import oracle.jbo.JboException;

import oracle.jbo.common.JboEnvUtil;
import oracle.jbo.common.PropertyConstants;
import oracle.jbo.common.PropertyMetadata;
import oracle.jbo.common.ampool.ApplicationPool;
import oracle.jbo.common.ampool.ApplicationPoolException;
import oracle.jbo.common.ampool.PoolMgr;
import oracle.jbo.common.ampool.SessionCookie;

import oracle.jbo.html.jsp.JSPApplicationRegistry;

import oracle.jbo.http.HttpContainer;
import oracle.jbo.http.HttpSessionCookieFactory;
import oracle.jbo.http.HttpUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionServlet;

import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import oracle.jbo.common.ampool.DefaultSessionCookieFactory;
import oracle.jbo.common.ampool.SessionCookieFactory;

/**
 * Utilitaire du Request Processor pour le traitement avec ADF Business Components
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class RequestProcessorHelper
  extends org.sofiframework.presentation.struts.controleur.RequestProcessorHelper {
  private static final Log log = LogFactory.getLog(RequestProcessorHelper.class);

  /**
   * Construit le nom que le pool de connnextion BC4J s'attend de recevoir.
   * <p>
   * Cette méthode sert à construire le nom que le pool de connexction s'attend de recevoir. Le nom
   * est de format : "Nom de fichier du Data Model" + "." + "Nom du module d'application BC4J".
   * Un paramètre se nommant : BC4JDefinition doit se trouver dans le fichier web.xml
   * pour spécifier le fichier .cpx qui spécifie les paramètres du modèle.<p>
   * Voici un exemple:<br>
   * &lt;init-param&gt;<br>
   * &nbsp;&nbsp;&lt;param-name&gt;BC4JDefinition&lt;/param-name&gt;<br>
   * &nbsp;&nbsp;&lt;param-value&gt;Presentation&lt;/param-value&gt;<br>
   * &lt;/init-param&gt;
   * <p>
   * @param appId Le nom de l'Application Module BC4J
   * @return le nom complet que le pool de connexion s'attend de recevoir
   */
  protected static String buildApplicationDefinitionName(
    ActionServlet servlet, String appId) {
    // Prendre la valeur correspondant a BC4JDefinition dans le fichier web.xml
    String bc4jDefFileName = servlet.getServletConfig()
                                             .getInitParameter("BC4JDefinition");

    return bc4jDefFileName + "." + appId;
  }

  /**
   * Méthode qui permet de se connecter sur un Application Module sur le serveur d'application.
   * <p>
   * Cette méthode sert à aller reprendre l'Application Module réservé pour un utilisateur (selon
   * son numéro de session) sur le serveur d'application, si le mode de libération est Stateful. Sinon
   * s'il est Stateless, il va emprunter un quelquonque Application Module
   * Vous devez inscrire le nom de l'application  module principal dans le fichier web.xml
   * Le nom du paramètre doit se nommer: modele<p>
   * Voici un exemple:<br>
   * &lt;init-param&gt;<br>
   * &nbsp;&nbsp;&lt;param-name>modele&lt;/param-name&gt;<br>
   * &nbsp;&nbsp;&lt;param-value>ModeleXXX&lt;/param-value&gt;<br>
   * &lt;/init-param&gt;
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @return L'Application Module BC4J servant de modèle.
   */
  public static Object getModele(ActionServlet servlet, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) {
    SessionCookie cookie = null;
    String releaseMode = null;

    try {
      Properties props = new Properties();
      props.put(HttpSessionCookieFactory.PROP_HTTP_REQUEST, request);

      // Prendre la valeur correspondant a appId dans le fichier web.xml
      String appId = servlet.getServletConfig().getInitParameter("modele");

      String definitionPool = buildApplicationDefinitionName(servlet, appId);

      String adfPackage = 
        servlet.getServletConfig().getInitParameter("adf.package");
      String adfConfiguration = 
        servlet.getServletConfig().getInitParameter("adf.configuration");
      
      ApplicationPool pool = 
        PoolMgr.getInstance().findPool(definitionPool, adfPackage, 
                                       adfConfiguration, null);

      if (adfPackage == null) {
        if (appId == null) {
          // Aucun modèle de configurer alors retourner null
          return null;
        }

        cookie = 
            HttpContainer.findSessionCookie(request.getSession(), appId, definitionPool, 
                                            props);
      } else {
        SessionCookieFactory factory = pool.getSessionCookieFactory();

        if (PropertyMetadata.ENV_AMPOOL_COOKIE_FACTORY_CLASS_NAME.getDefault().equals(factory.getClass().getName())) {
          pool.setSessionCookieFactory(new HttpSessionCookieFactory());
        }

        cookie = 
            (SessionCookie)request.getSession().getAttribute(appId + "adfSOFICookie");
      }
        
      if (cookie == null || !pool.validateSessionCookieInPool(cookie)) {
        try {
          cookie = 
              pool.createSessionCookie(appId, request.getSession().getId(), 
                                       props);
        } catch (ApplicationPoolException e) {
          // Cookie déjà existant, donc le supprimer et en construire un nouveau.
          //pool.releaseApplicationModule(e.getSessionCookie(), SessionCookie.SHARED_UNMANAGED_RELEASE_MODE);
          pool.removeSessionCookie(e.getSessionCookie());
          cookie = 
              pool.createSessionCookie(appId, request.getSession().getId(), 
                                       props);
        }

        request.getSession().setAttribute((appId + "adfSOFICookie"), cookie);
      }

      // réserver la l'application module pour la session de l'utilisateur.
      BaseServiceImpl am = (BaseServiceImpl)cookie.useApplicationModule();

      am.initialiserGestionnaires();

      Locale locale = HttpUtil.determineLocale(request);

      // Configurer l'application module avec le locale de l'utilisateur
      if (locale != null) {
        am.getSession().setLocale(locale);
      }

      // Spécifier seulement le mode de libération lors de la première requête.
      releaseMode = (String)request.getAttribute(getReleaseMode(cookie));

      if ((releaseMode == null) || (releaseMode.length() == 0)) {
        releaseMode = 
            (String)cookie.getEnvironment(PropertyMetadata.AM_RELEASE_MODE.getName());

        if ((releaseMode == null) || (releaseMode.length() == 0)) {
          releaseMode =  
              JboEnvUtil.getProperty(PropertyMetadata.AM_RELEASE_MODE.getName(), 
                                     PropertyMetadata.AM_RELEASE_MODE.getDefault());
        }

        request.setAttribute(getReleaseMode(cookie), releaseMode);
      }

      // Reserve a passivation id if the release mode is stateful
      if (PropertyConstants.AM_RELEASE_STATEFUL.equals(releaseMode)) {
        cookie.reservePassivationId();
      }

      cookie.writeValue(response);

      Utilisateur utilisateur = 
        UtilitaireControleur.getUtilisateur(request.getSession());

      // Fixer l'utilisateur dans le modèle si ce n'est déjà fait.
      if ((utilisateur != null) && 
          ((am.getUtilisateur() == null) || (am.getUtilisateur().getCodeUtilisateur() == 
                                             null))) {
        am.setUtilisateur(utilisateur);
      }

      if (utilisateur == null) {
        utilisateur = new Utilisateur();
        am.setUtilisateur(utilisateur);
      }

      return am;
    } catch (Exception e) {
      if (cookie != null) {
        try {
          releaseApplicationModule(response, cookie, releaseMode);
        } catch (Exception e1) {
        }
      }

      log.error("Erreur lors de la récupération du modèle avec le sessionCookie", 
                e);
      throw new ModeleADFException(e);
    }
  }

  /**
   * Libère le module d'application BC4J.
   * <p>
   * Cette méthode libère le module d'application BC4J. Ce dernier restera réservé pour l'utilisateur
   * tant et aussi longtemps que la session de ce dernier ne sera pas morte.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   */
  public static void libererModele(ActionServlet servlet, 
                                   HttpServletRequest request, 
                                   HttpServletResponse response) {
    // Accès au modèle courant.
    BaseServiceImpl serviceCourant = 
      (BaseServiceImpl)UtilitaireControleur.getModele(request);

    if (serviceCourant != null) {
      // Appliquer du traitement spécifique sur la libération du modèle si applicable.
      serviceCourant.libererModele();
    }

    HttpContainer container = 
      HttpContainer.getInstanceFromSession(request.getSession());

    SessionCookie[] cookies = container.getSessionCookies();

    for (int i = 0; i < cookies.length; i++) {
      synchronized (cookies[i].getSyncLock()) {
        if (!cookies[i].isApplicationModuleReleasedByThread()) {
          String releaseMode = 
            (String)request.getAttribute(getReleaseMode(cookies[i]));

          releaseApplicationModule(response, cookies[i], 
                                   releaseMode);
        }
      }
    }
    
    // Prendre la valeur correspondant a appId dans le fichier web.xml
    String appId = servlet.getServletConfig().getInitParameter("modele");
    SessionCookie cookieADF = 
      (SessionCookie)request.getSession().getAttribute(appId + "adfSOFICookie");

    if (cookieADF != null) {
      synchronized (cookieADF.getSyncLock()) {
        if (!cookieADF.isApplicationModuleReleasedByThread()) {
          String releaseMode = 
            (String)request.getAttribute(getReleaseMode(cookieADF));

          releaseApplicationModule(response, cookieADF, 
                                   releaseMode);
        }
      }
    }

    if (request.getAttribute(Constantes.IS_FERMER_SESSION) != null) {
      try {
        GestionSurveillance.getInstance().decrementerCompteurUsagerAuthentifier();
      } catch (Exception e) {
        //Le session context n'est pas initialiser.
      }

      RequestProcessorHelper.specfifierTypeNavigateur(request);
    }
  }

  /**
   * Méthode qui sert à savoir quel type releaseMode utilisé.
   * <p>
   * @param cookie le témoin de session contenant les informations sur le module d'application
   * @return la clé identifiant le code de releaseMode
   */
  protected static String getReleaseMode(SessionCookie cookie) {
    StringBuffer releaseModeKey = new StringBuffer(cookie.getApplicationId());

    releaseModeKey.append('_');
    releaseModeKey.append(PropertyMetadata.AM_RELEASE_MODE.getName());

    return releaseModeKey.toString();
  }

  /**
   * Méthode qui sert à libérer le module d'application.
   * <p>
   * @param response la réponse HTTP qui sera retournée à l'utilisateur
   * @param cookie le témoin de session contenant les informations sur le module d'application
   * @param releaseMode le mode à utiliser pour libérer le module d'application
   */
  protected static void releaseApplicationModule(HttpServletResponse response, 
                                                 SessionCookie cookie, 
                                                 String releaseMode) {
    if ((releaseMode == null) || (releaseMode.length() <= 0)) {
      throw new JboException("");
    }

    if (JSPApplicationRegistry.RESERVED.equals(releaseMode)) {
      cookie.releaseApplicationModule(SessionCookie.RESERVED_UNMANAGED_RELEASE_MODE);
    }
    // Si le mode de libération est stateful qui est spécifié
    // l'application module est retourné dans le pool lorsque l'utilisateur a fini sa session.
    // Ce mode n'est pas supporté si l'objet response n'est pas spécifié.
    else if (JSPApplicationRegistry.STATEFUL.equals(releaseMode)) {
      cookie.releaseApplicationModule(SessionCookie.SHARED_MANAGED_RELEASE_MODE);
      cookie.writeValue(response);
    }
    // Si le mode de libération est stateless, seulement retourner l'application
    // dans le pool de connexion.
    else {
      cookie.releaseApplicationModule(SessionCookie.SHARED_UNMANAGED_RELEASE_MODE);
      cookie.writeValue(response);
    }
  }
}
