/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Permet de wrapper la session Http.
 * 
 * @author Jean-Maxime Pelletier
 */
public class HttpServletRequestWrapper extends javax.servlet.http.HttpServletRequestWrapper {

  private HttpSessionWrapper sessionWrapper;
  
  public HttpServletRequestWrapper(HttpServletRequest request) {
    super(request);
  }

  public HttpSession getSession() {
    if (this.sessionWrapper == null) {
      this.sessionWrapper = new HttpSessionWrapper(super.getSession());
    }
    return this.sessionWrapper;
  }
}
