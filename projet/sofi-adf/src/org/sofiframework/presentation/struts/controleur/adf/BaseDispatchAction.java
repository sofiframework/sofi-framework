/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf;

import org.sofiframework.modele.exception.ModeleException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.presentation.struts.controleur.adf.session.HttpServletRequestWrapper;



/**
 * Classe de spécialisation du BaseDispathAction générique pour qui offre
 * des fonctionnalité sur les composantes de Oracle ADF.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.controleur.BaseDispatchAction
 */
public class BaseDispatchAction
extends org.sofiframework.presentation.struts.controleur.BaseDispatchAction {

  /**
   * Est-ce que l'exception lancé est une contrainte d'intégrité concernant
   * la clé d'unicité ?
   * @param detailException l'exception du modele
   * @return true si l'exception lancé est une contrainte d'intégrité est concernant la clé d'unicité.
   */
  public boolean isExceptionCleUnique(ModeleException detailException) {
    if (detailException.getDetail() == null) {
      return false;
    }

    return org.sofiframework.modele.adf.exception.UtilitaireExceptionOracle.isCleUniqueException(detailException.getDetail());
  }

  /**
   * Retourne le message d'erreur bd
   * @param detailException le détail de l'exception.
   * @return le message d'erreur bd.
   */
  public String getMessageErreurBD2(ModeleException detailException) {
    if (detailException.getDetail() == null) {
      return detailException.getMessage();
    }

    return org.sofiframework.modele.adf.exception.UtilitaireExceptionOracle.getMessageErreurBD(detailException.getDetail());
  }
  
  /**
   * Libère le modèle et invalide la session. 
   * 
   * Il est TRÈS important de toujours utiliser cette méthode pour invalider la session.
   * 
   * @param request Requete HTTP
   */
  protected void invaliderSession(HttpServletRequest  request, HttpServletResponse response) {
    HttpServletRequestWrapper requestWrapper = (HttpServletRequestWrapper) request;
    requestWrapper.getSession().invalidate();
  }    

}