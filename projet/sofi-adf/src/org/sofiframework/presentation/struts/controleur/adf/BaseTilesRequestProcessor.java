/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.presentation.struts.controleur.adf.session.HttpServletRequestWrapper;
import org.sofiframework.presentation.struts.controleur.adf.session.HttpSessionWrapper;


/**
 * Classe abstraite spécialisée jouant le rôle de contrôleur permettant la
 * gestion automatique de l'accès au modèle via Oracle ADF.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.controleur.BaseTilesRequestProcessor
 */
public class BaseTilesRequestProcessor
  extends org.sofiframework.presentation.struts.controleur.BaseTilesRequestProcessor {
  
  public void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
     /*
      * Wrap la session pour protéger la session et le modèle BC4J 
      * d'une invalidation. On doit toujours libérer 
      * le modèle avant d'invalider.
      */
    HttpServletRequestWrapper requestWrapper = new HttpServletRequestWrapper(request);
    super.process(requestWrapper, response);
  }

  /**
  * Méthode qui permet de se connecter sur un Application Module sur le serveur d'application.
  * <p>
  * Cette méthode sert à aller reprendre l'Application Module réservé pour un utilisateur (selon
  * son numéro de session) sur le serveur d'application, si le mode de libération est Stateful. Sinon
  * s'il est Stateless, il va emprunter un quelquonque Application Module
  * Vous devez inscrire le nom de l'application  module principal dans le fichier web.xml
  * Le nom du paramètre doit se nommer: modele<p>
  * Voici un exemple:<br>
  * &lt;init-param&gt;<br>
  * &nbsp;&nbsp;&lt;param-name>modele&lt;/param-name&gt;<br>
  * &nbsp;&nbsp;&lt;param-value>ModeleXXX&lt;/param-value&gt;<br>
  * &lt;/init-param&gt;
  * <p>
  * @param request La requête HTTP en traitement
  * @param response La réponse HTTP à fournir
  * @return L'Application Module BC4J servant de modèle.
  */
  protected Object getModele(HttpServletRequest request,
    HttpServletResponse response) {
    return RequestProcessorHelper.getModele(servlet, request, response);
  }

  /**
   * Libère le module d'application BC4J.
   * <p>
   * Cette méthode libère le module d'application BC4J. Ce dernier restera réservé pour l'utilisateur
   * tant et aussi longtemps que la session de ce dernier ne sera pas morte.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   */
  protected void libererModele(HttpServletRequest request,
    HttpServletResponse response) {
    RequestProcessorHelper.libererModele(servlet, request, response);
    
    if (request instanceof HttpServletRequestWrapper) {
      /*
       * On doit invalider la session si la méthode invalidate 
       * à été appelée par une action Struts 
       */
      HttpServletRequestWrapper requestWrapper = (HttpServletRequestWrapper) request;
      HttpSessionWrapper sessionWrapper = (HttpSessionWrapper) requestWrapper.getSession();
      if (sessionWrapper.isInvaliderSession()) {
        sessionWrapper.getWrappedSession().invalidate();
      }
    }
  }
}
