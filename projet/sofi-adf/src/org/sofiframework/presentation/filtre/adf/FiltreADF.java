/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre.adf;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.adf.WebApplicationModuleFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class FiltreADF implements Filter {

  private String nomApplicationModuleFactory = null;

  @Override
  public void init(FilterConfig config) throws ServletException {
    this.nomApplicationModuleFactory = config.getInitParameter("applicationModuleFactory");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;

    // Obtenir l'application module
    WebApplicationModuleFactory factory = getAMFactory(httpRequest);
    factory.get(httpRequest);

    try {
      filterChain.doFilter(request, response);
    } catch(Exception e) {
      throw new SOFIException("Erreur inatendu.", e);
    } finally {
      factory.releaseApplicationModuleCourant(httpRequest);
    }
  }

  private WebApplicationModuleFactory getAMFactory(HttpServletRequest httpRequest) {
    ApplicationContext contexte = WebApplicationContextUtils.getWebApplicationContext(
        httpRequest.getSession().getServletContext());
    return  (WebApplicationModuleFactory) contexte.getBean(nomApplicationModuleFactory);
  }

  @Override
  public void destroy() {
  }
}
