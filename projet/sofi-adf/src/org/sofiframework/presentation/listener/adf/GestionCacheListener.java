/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.listener.adf;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import oracle.jbo.client.Configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.modele.adf.BaseServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Initialise la gestion des caches lors du démarrage du conteneur Web.
 * Il est possible de chager les caches à l'aide d'un modèle instancié
 * directement pour les caches ou avec un modèle du contexte
 * d'application Spring.
 * 
 * @author Jean-Maxime Pelletier
 */
public class GestionCacheListener implements ServletContextListener {

  /**
   * L'instance de journalisation pour cette classe
   */
  private static final Log log = LogFactory.getLog(GestionCacheListener.class);

  @Override
  public void contextInitialized(ServletContextEvent evenement) {
    // Initialiser le path absolut vers le repertoire web-inf
    String cheminReelApp = evenement.getServletContext().getRealPath("");
    GestionParametreSysteme.getInstance().setEmplacementWebInf(cheminReelApp);

    String adfPackage = evenement.getServletContext().getInitParameter("cache.adf.package");
    if (adfPackage == null) {
      adfPackage = evenement.getServletContext().getInitParameter("adf.package");
    }

    String adfConfiguration = evenement.getServletContext().getInitParameter("cache.adf.configuration");
    if (adfConfiguration == null) {
      adfConfiguration = evenement.getServletContext().getInitParameter("adf.configuration");
    }

    String nomBeanModele = evenement.getServletContext().getInitParameter("cache.adf.nom.bean");

    BaseServiceImpl modele = null;

    if (adfPackage != null && adfConfiguration != null) {
      try {
        modele = (BaseServiceImpl) Configuration.createRootApplicationModule(adfPackage, adfConfiguration);
        modele.initialiserGestionCache();
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur lors du chargement des caches!!!", e);
        }
      } finally {
        if (modele != null) {
          Configuration.releaseRootApplicationModule(modele, true);
        }
      }
    } else if (nomBeanModele != null) {
      ApplicationContext contexte = WebApplicationContextUtils.getWebApplicationContext(evenement.getServletContext());
      modele = (BaseServiceImpl) contexte.getBean(nomBeanModele);
      modele.initialiserGestionCache();
    }
  }


  @Override
  public void contextDestroyed(ServletContextEvent evenement) {
  }
}
