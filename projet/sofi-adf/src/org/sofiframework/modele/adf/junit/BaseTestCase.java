/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.junit;

import java.util.HashMap;
import java.util.Iterator;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.adf.BaseServiceImpl;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;


/**
 * Classe de base de SOFI pour effectuer des essais unitaires des divers services du modèle.
 * <p>
 * Permet de simplifier le développement des classes d'essais unitaire avec ADF Business Components.
 * La classe en cours se charge d'effectuer la connexion à la base de données et de la détruire
 * à la fin de l'utilisation du cas de test.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @since 2.0.4 Permet maintenant de faciliter le commit vers la base de données avec l'aide de la méthode commit();
 * Il est maintenant possible de référencer sur plusieurs services via des configurations différentes.
 */
public class BaseTestCase extends AbstractSpringTest {
  /** Utilisateur qui sera associé au modèle. */
  private Utilisateur utilisateur = null;

  /** La liste des services utilisé par le test unitaire **/
  private HashMap listeServices = null;

  /** Constructeur par défaut */
  public BaseTestCase() {
    super("baseTest");
  }

  /**
   * Fichier de configuration Spring pour la classe de tests.
   * @return les fichiers de configuration necessaire.
   */
  @Override
  protected String[] getConfigurationsSpring() {
    return

        new String[] { };
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    listeServices = new HashMap();
    this.initialiserUtilisateurParDefaut();
  }

  /**
   * Initialise un utilisateur par défaut qui a comme
   * code utilisateur la valeur "junit".
   */
  public void initialiserUtilisateurParDefaut() {
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur("junit");
    this.setUtilisateur(utilisateur);
  }

  /**
   * Obtenir la référence au modèle de l'application.
   * <p>
   * @param classeModele la classe du modèle pour lequel on désire une référence, ici
   *        ce n'est pas l'implémentation du modèle que l'on désire, mais bien l'interface.
   * @param nomConfiguration le nom de la configuration du modèle à utiliser pour se connecter à la base de données
   * @return la référence au modèle de l'application
   */
  public ApplicationModule getModele(Class classeModele,
      String nomConfiguration) {

    ApplicationModule service = null;
    if (!listeServices.containsKey(classeModele)) {

      String nomPackage = classeModele.getPackage().getName();
      String nomClasse = classeModele.getName();

      if (nomClasse.indexOf("common") != -1) {
        service =
            Configuration.createRootApplicationModule(nomPackage.substring(0,
                nomPackage.lastIndexOf(".")) +
                nomClasse.substring(nomClasse.lastIndexOf(".")),
                nomConfiguration);
      } else {
        service =
            Configuration.createRootApplicationModule(nomClasse, nomConfiguration);
      }

      if (service instanceof BaseServiceImpl) {
        ((BaseServiceImpl)service).setUtilisateur(this.utilisateur);
      }

      ((BaseServiceImpl)service).setUtilisateur(getUtilisateur());
      listeServices.put(classeModele, service);
    } else {
      service = (ApplicationModule)listeServices.get(classeModele);
    }

    return service;
  }

  /**
   * Permet d'envoyer la transaction en cours vers la base de données.
   * @since SOFI 2.0.4
   */
  public void commit() {
    Iterator iterateur = listeServices.values().iterator();
    while (iterateur.hasNext()) {
      ApplicationModule service = (ApplicationModule)iterateur.next();
      service.getTransaction().commit();
    }
  }

  /**
   * Permet d'annuler la transaction en cours vers la base de données.
   * @since SOFI 2.0.4
   */
  public void rollback() {
    Iterator iterateur = listeServices.values().iterator();
    while (iterateur.hasNext()) {
      ApplicationModule service = (ApplicationModule)iterateur.next();
      service.getTransaction().rollback();
    }
  }

  /**
   * Méthode qui s'exécute automatiquement à la fin du cas de test.
   * <p>
   * Cette méthode est chargée de libérer la connexion à la base de donnée qui a été établie.
   */
  @Override
  public void tearDown() throws Exception {
    super.tearDown();
    Iterator iterateur = listeServices.values().iterator();
    while (iterateur.hasNext()) {
      ApplicationModule service = (ApplicationModule)iterateur.next();
      Configuration.releaseRootApplicationModule(service, true);
    }
  }

  /**
   * Obtenir l'utilisateur.
   *
   * @return un utilisateur
   */
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  /**
   * Fixer l'utilisateur.
   * @param utilisateur Utilisateur qui sera associé au modèle.
   */
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
}
