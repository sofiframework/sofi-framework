/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jbo.AttributeDef;
import oracle.jbo.JboException;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.DBTransaction;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.SparseArray;
import oracle.jbo.server.TransactionEvent;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.modele.adf.utilitaire.UtilitairePLSQL;
import org.sofiframework.modele.exception.ModeleException;


/**
 * Entité de base offrant des fonctionnalités supplémentaire aux classe de BC4J.
 *
 * Il est possible d'utiliser des méthodes pour réécrire le DML de l'entité.
 *
 * De plus, elle offre une méthode pour assigner une nouvelle séquence à une entité.
 *
 */
public class BaseEntityImpl extends EntityImpl {
  /**
   * Création d'un object BaseEntity.
   */
  public BaseEntityImpl() {
  }

  /**
   * Permet de populer les attributs provenants d'une table de la base de
   * données. Cette méthode est utiliser pour surcharger le traitement DML
   * de l'entité lorsque'un entité est composée des données de deux tables.
   *
   * @param nomTable Nom de la table dans la base de données
   * @param listeChamp Liste des champs a populer
   * @param champCle Position de la clé primaire dans l'entité
   * @param champCleEtrangere Position du champ clé étrangère dans la table
   *        enfant
   *
   * @throws JboException À COMPLÉTER
   */
  protected void populerEntitePolymorphique(String nomTable, int[] listeChamp,
      int champCle, int champCleEtrangere) {
    CallableStatement statement = null;

    try {
      EntityDefImpl definitionEntite = this.getEntityDef();

      /* Composer le select avec les informations que l'on possède. */
      StringBuffer sql = new StringBuffer();
      sql.append("select ").append(getChampSQL(listeChamp)).append(" from ")
      .append(nomTable).append(" t ");
      sql.append("where t.");

      /* Retrouver le nom de la colonne cle etrangere */
      AttributeDef definitionCle = definitionEntite.getAttributeDef(champCleEtrangere);
      sql.append(definitionCle.getColumnName());
      sql.append(" = :1");

      DBTransaction transaction = getDBTransaction();
      statement = transaction.createCallableStatement(sql.toString(), 1);

      /* Assigner la valeur de la clé */
      Object id = this.getAttribute(champCle);
      statement.setObject(1, id);

      ResultSet rs = statement.executeQuery();
      rs.next();

      for (int i = 0; i < listeChamp.length; i++) {
        AttributeDef definitionAttribut = definitionEntite.getAttributeDef(listeChamp[i]);

        Object valeur = rs.getObject(definitionAttribut.getColumnName());
        Object conversion = null;

        boolean populer = !this.isAttributePopulated(listeChamp[i]);

        if ((valeur != null) && populer) {
          if (valeur instanceof String) {
            conversion = valeur;
          } else if (valeur instanceof BigDecimal) {
            conversion = new Integer(((BigDecimal) valeur).intValue());
          } else if (valeur instanceof java.sql.Date) {
            conversion = new oracle.jbo.domain.Timestamp(((java.sql.Date) valeur).getTime());
          }

          this.populateAttribute(listeChamp[i], conversion);
        }
      }
    } catch (Exception ex) {
      throw new JboException(ex);
    } finally {
      try {
        if (statement != null) {
          statement.close();
        }

        statement = null;
      } catch (SQLException s) { /* ignore */
      }
    }
  }

  /**
   * Permet d'ajouter une entrée dans une table de la base de données.
   *
   * @param nomTable Nomde la table
   * @param listeChamp Liste des indexes de champs qui composent la table a
   *        insérer
   *
   * @throws JboException À COMPLÉTER
   */
  protected void insererEntitePolymorphique(String nomTable, int[] listeChamp) {
    CallableStatement statement = null;

    try {
      StringBuffer sql = new StringBuffer();
      sql.append("INSERT INTO ").append(nomTable).append(" t ");
      sql.append("(").append(this.getChampSQL(listeChamp)).append(")");
      sql.append(" VALUES (");

      for (int i = 1; i <= listeChamp.length; i++) {
        sql.append(":").append(i);

        if (i < (listeChamp.length)) {
          sql.append(",");
        }
      }

      sql.append(")");

      DBTransaction transaction = getDBTransaction();
      statement = transaction.createCallableStatement(sql.toString(), 1);

      for (int i = 1; i <= listeChamp.length; i++) {
        statement.setObject(i, this.getAttribute(listeChamp[i - 1]));
      }

      statement.execute();
    } catch (Exception ex) {
      throw new JboException(ex);
    } finally {
      try {
        if (statement != null) {
          statement.close();
        }
      } catch (SQLException s) { /* ignore */
      }
    }
  }

  /**
   * Permet de modifier les valeurs des attributs d'une table de la base de
   * données.
   *
   * @param nomTable Nomde la table à mettre à jour
   * @param listeChamp Liste des indexes de champs que l'on désire mettre à
   *        jour.
   * @param champCle Indexe de la clé primaire de la table.
   *
   * @throws JboException À COMPLÉTER
   */
  protected void modifierEntitePolymorphique(String nomTable, int[] listeChamp,
      int champCle) {
    CallableStatement statement = null;

    try {
      EntityDefImpl definitionEntite = this.getEntityDef();

      StringBuffer sql = new StringBuffer();
      sql.append("UPDATE ").append(nomTable).append(" t ");

      String nomChamp = this.getChampSQL(listeChamp);
      StringTokenizer stk = new StringTokenizer(nomChamp, ",", false);

      sql.append("SET ");

      ArrayList listeChampAModifier = new ArrayList();

      for (int i = 1; i <= listeChamp.length; i++) {
        /*
         * On doit vérifier si le champ est modifié et qu'il est permit de le modifier
         */
        if (this.isAttributeUpdateable(listeChamp[i - 1])) {
          sql.append(stk.nextToken()).append(" = ");
          listeChampAModifier.add(this.getAttribute(listeChamp[i - 1]));

          sql.append(":").append(i);

          if (i < (listeChamp.length)) {
            sql.append(",");
          }
        }
      }

      sql.append(" WHERE ");

      /* Retrouver le nom de la colonne cle primaire */
      AttributeDef definitionCle = definitionEntite.getAttributeDef(champCle);
      sql.append(definitionCle.getColumnName());
      sql.append(" = :").append(listeChampAModifier.size() + 1);

      DBTransaction transaction = getDBTransaction();
      statement = transaction.createCallableStatement(sql.toString(), 1);

      int count = 1;

      for (Iterator i = listeChampAModifier.iterator(); i.hasNext(); count++) {
        statement.setObject(count, i.next());
      }

      statement.setObject(listeChampAModifier.size() + 1,
          this.getAttribute(champCle));

      statement.execute();
    } catch (Exception ex) {
      throw new JboException(ex);
    } finally {
      try {
        if (statement != null) {
          statement.close();
        }
      } catch (SQLException s) { /* ignore */
      }
    }
  }

  /**
   * Permet de supprimer une rangée de la base de données.
   *
   * @param nomTable Nom de la table
   * @param champCle Indexe de l'attribut clé primaire dans l'entité.
   *
   * @throws JboException À COMPLÉTER
   */
  protected void supprimerEntitePolymorphique(String nomTable, int champCle) {
    CallableStatement statement = null;

    try {
      EntityDefImpl definitionEntite = this.getEntityDef();

      StringBuffer sql = new StringBuffer();
      sql.append("DELETE FROM ").append(nomTable).append(" WHERE ");

      /* Retrouver le nom de la colonne cle primaire */
      AttributeDef definitionCle = definitionEntite.getAttributeDef(champCle);
      sql.append(definitionCle.getColumnName());
      sql.append(" = :1");

      DBTransaction transaction = getDBTransaction();
      statement = transaction.createCallableStatement(sql.toString(), 1);
      statement.setObject(1, this.getAttribute(champCle));
      statement.execute();
    } catch (Exception ex) {
      throw new JboException(ex);
    } finally {
      try {
        if (statement != null) {
          statement.close();
        }
      } catch (SQLException s) { /* ignore */
      }
    }
  }

  /**
   * Obtenir une liste des nom de champs dans la base de données
   *
   * @param listeChamp Liste des indexes d'attributs dont on désire obtenir
   *        le nom
   *
   * @return Une liste de nom de champs séparés par des vigules
   */
  private String getChampSQL(int[] listeChamp) {
    StringBuffer champSQL = new StringBuffer();
    EntityDefImpl definitionEntite = this.getEntityDef();

    /* Composer la liste des colonnes qui compose le select
     */
    for (int i = 0; i < listeChamp.length; i++) {
      AttributeDef definitionAttribut = definitionEntite.getAttributeDef(listeChamp[i]);
      champSQL.append("t.").append(definitionAttribut.getColumnName());

      if (i < (listeChamp.length - 1)) {
        champSQL.append(", ");
      }
    }

    return champSQL.toString();
  }

  /**
   * Popule une nouvelle sequence en attribut de l'entité.
   *
   * @param attributSequence Index de l'attribut on l'on assigne la nouvelle
   *        séquence
   * @param nomSequence Nom de la séquence dans la base de données
   */
  protected void populerNouvelleSequence(int attributSequence,
      String nomSequence) {
    SequenceImpl s = new SequenceImpl(nomSequence, getDBTransaction());

    Class classType = getDefinitionEntite().getAttributeDef(attributSequence)
        .getJavaType();

    if (classType.isAssignableFrom(Integer.class)) {
      this.setAttribute(attributSequence,
          new Integer(s.getSequenceNumber().intValue()));
    }

    if (classType.isAssignableFrom(Long.class)) {
      this.setAttribute(attributSequence,
          new Long(s.getSequenceNumber().intValue()));
    }

    if (!classType.isAssignableFrom(Integer.class) &&
        !classType.isAssignableFrom(Long.class)) {
      this.setAttribute(attributSequence, s.getData());
    }
  }

  /**
   * Vérifier si les données de l'entité sont à jour avec la base de données.
   * Valide que l'enregistrement n'a pas été modifié par une autre personne.
   *
   * @return Si l'enregistrement est toujours à jour
   * @param lock Si l'enregistrement est barré
   * @param sparsearray Contenu de l'entité
   */
  @Override
  protected boolean checkConsistency(SparseArray sparsearray, boolean lock) {
    boolean identique = super.checkConsistency(sparsearray, lock);

    //si l'enregistrement est locker
    if (lock) {
      BaseEntityDefImpl definitionEntite = getDefinitionEntite();
      AttributeDefImpl attribut = definitionEntite.getColonneIndicateurChangement();

      if (attribut != null) {
        int index = attribut.getIndex();

        //valeur initale que l'on recupere de la BD
        Object valeurOriginal = this.getAttribute(index);

        //valeur contenu dans l'entité
        Object valeurBD = sparsearray.get(index);

        if ((valeurOriginal != null) && (valeurBD != null)) {
          identique = valeurOriginal.equals(valeurBD);
        } else { // si un des 2 est à null

          //les valeurs sont identique seulement si les 2 valeurs sont null
          identique = ((valeurOriginal == null) && (valeurBD == null));
        }
      }
    }

    return identique;
  }

  /**
   * Récupère la définition de l'entité
   *
   * @return Définition de l'entité
   */
  public BaseEntityDefImpl getDefinitionEntite() {
    return (BaseEntityDefImpl) this.getEntityDef();
  }

  /**
   * Effectue les opérations DML (Data Manipulation Language) de l'entité
   *
   * @param e Evénement de transaction BC4J de l'entité
   * @param operation Opération DML qui est en train d'être faite
   */
  @Override
  protected void doDML(int operation, TransactionEvent e) {
    if ((operation == DML_UPDATE) || (operation == DML_INSERT)) {
      BaseEntityDefImpl definitionEntite = getDefinitionEntite();

      //Recupérer l'attribut qui est identifié comme "change indicator" dans l'entité
      //On ne peut qu'en avoir un attribut d'identifié par entité
      AttributeDefImpl attribut = definitionEntite.getColonneIndicateurChangement();

      if (attribut != null) {
        int index = attribut.getIndex();

        //si l'attribut Change Indicator est de type Long, on incrémente la valeur de 1
        if (attribut.getJavaType() == Long.class) {
          Long valeur = (Long) this.getAttribute(index);

          if (valeur != null) {
            valeur = new Long(valeur.intValue() + 1);
            this.setAttribute(index, valeur);
          } else { //si la valeur est null
            this.setAttribute(index, new Long(1L));
          }
        }
      }
    }

    super.doDML(operation, e);
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   *
   * Utiliser cette signature avec les types lorsque certains de vos paramètres peuvent être nuls.
   *
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de la classe ConstantesSQL
   * pour le type de retour, sinon le type JDBC de retour sera java.sql.Types.
   *
   * @param typeSQLRetour Un tableau contenant les types des données JDBC de retour, voir ConstantesSQL pour les types disponibles.
   * @param procedure la fonction stocké BD.
   * @param positionPremierParametreEntree la position du premier paramètre en entrée.*
   * @param positionPremierParametreSortie la position du premier paramètre en sortie.
   * @param variableAssocies  Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies Un tableau contenant les types des paramètres, voir ConstantesSQL pour les types disponibles.
   * @return Retourne un tableau contenant les valeurs des objets de retour.
   * @throws ModeleException l'exception résultante de l'éxécution de la procédure.
   */
  protected Object[] executeProcedurePLSQL(int[] typeSQLRetour, String fonction,
      int positionPremierParametreSortie, Object[] variableAssocies,
      int[] typesAssocies) throws ModeleException {
    return UtilitairePLSQL.executerProcedurePLSQL(typeSQLRetour, fonction,
        positionPremierParametreSortie, variableAssocies, typesAssocies,
        getDBTransaction());
  }

  /**
   * Retourne l'utilisateur pour le rendre disponible dans tous les
   * services.
   * @return l'utilisateur authentifié
   */
  protected Utilisateur getUtilisateur() {
    return (Utilisateur) getDBTransaction().getSession().getUserData().get(Constantes.UTILISATEUR);
  }
}
