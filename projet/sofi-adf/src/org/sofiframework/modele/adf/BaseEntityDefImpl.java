/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import oracle.jbo.Key;
import oracle.jbo.rules.JboValidatorInterface;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.DBTransaction;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;

import com.sun.java.util.collections.Iterator;
import com.sun.java.util.collections.List;


/**
 * Classe de définition de l'entité. Corrige certains bus rencontrés
 * avec BC4J en ce qui concerne les entités polymorphiques.
 */
public class BaseEntityDefImpl extends EntityDefImpl {

  /**
   * Documentation fournie avec l'exemple polymorphisme sur le site
   * diveintobc4j  Workaround for Bug 3267441: FindByPrimaryKey On Parent
   * Entity Doesn't Find Extended Child EO Instance -------------------
   * Overridden framework method to search in the entity caches of subtyped
   * entity defs if the findByPrimaryKey() returns null for the current
   * entity def.
   *
   * @param txn DBTransaction to use
   * @param key Key of the entity instance being looked-up
   *
   * @return Entity instance if found, or null of not found.
   */
  @Override
  public EntityImpl findByPrimaryKey(DBTransaction txn, Key key) {
    EntityImpl e = super.findByPrimaryKey(txn, key);

    if (e == null) {
      List extendedDefs = getExtendedDefObjects();

      if (extendedDefs != null) {
        Iterator iter = extendedDefs.iterator();

        while (iter.hasNext()) {
          e = ((EntityDefImpl)iter.next()).findByPrimaryKey(txn,key);

          if (e != null) {
            break;
          }
        }
      }
    }
    return e;
  }

  /**
   * Ajouter un nouveau validateur à la définition de l'entité.
   * Cette méthode est maintenant publique.
   * 
   * @param listener Validateur à ajouter
   */
  @Override
  public synchronized void addValidator(JboValidatorInterface listener) {
    super.addValidator(listener);
  }

  /**
   * Permet de récupérer l'attribut identifié comme indicateur de changement
   * 
   * @return La définition de l'attribut
   */
  public AttributeDefImpl getColonneIndicateurChangement() {
    return this.getChangeIndicatorColumn();
  }

}