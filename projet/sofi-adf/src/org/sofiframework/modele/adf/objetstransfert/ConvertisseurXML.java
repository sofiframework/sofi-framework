/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.objetstransfert;

/**
 * @deprecated
 */
@Deprecated
public class ConvertisseurXML  {

  public ConvertisseurXML() {}

  /*public void convertirEnObjetTransfert(Class classe, String nomAttribut,
    Object valeurAttribut, ObjetTransfert objetTransfert)
    throws InvocationTargetException, NoSuchMethodException,
      IllegalAccessException {

      //Convertir jbo.domain.BlobDomain en byte[]
      if (BlobDomain.class.isAssignableFrom(classe) &&
          (valeurAttribut != null)) {
        org.sofiframework.utilitaire.UtilitaireObjet.setSimpleProperty(objetTransfert,
          nomAttribut, ((BlobDomain) valeurAttribut).toByteArray());

        return;
      }

      //Convertir oracle.jbo.domain.Timestamp en java.sql.Timestamp
      if (oracle.jbo.domain.Timestamp.class.isAssignableFrom(classe) &&
          (valeurAttribut != null)) {
        org.sofiframework.utilitaire.UtilitaireObjet.setSimpleProperty(objetTransfert,
          nomAttribut,
          new java.sql.Timestamp(((oracle.jbo.domain.Timestamp) valeurAttribut).getTime()));

        return;
      }

      super.convertirEnObjetTransfert(classe, nomAttribut, valeurAttribut, objetTransfert);
    }*/
}