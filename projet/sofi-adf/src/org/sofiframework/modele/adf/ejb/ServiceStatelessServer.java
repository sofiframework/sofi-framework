/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.ejb;

import oracle.jbo.server.ejb.SessionBeanImpl;

/**
 * Classe abstraite permettant de déployer un service (Application Module) en
 * EJB sans état (Stateless).
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
abstract public class ServiceStatelessServer extends SessionBeanImpl {

  /**
   * 
   */
  private static final long serialVersionUID = -4704114200287857477L;

  /**
   * À COMPLÉTER
   */
  public void commit() {
  }

  /**
   * À COMPLÉTER
   */
  public void releaseResources() {
    // Libérer la connexion JDBC
    disconnectFromDataSource();
  }

  /**
   * À COMPLÉTER
   */
  public void acquireResources() {
    // Connexion JDBC depuis le datasource
    connectToDataSource();

  }
}
