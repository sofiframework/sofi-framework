package org.sofiframework.modele.adf;

import oracle.jbo.ApplicationModule;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.framework.ProxyFactory;

public class TransactionInterceptor implements MethodInterceptor {

  private ApplicationModuleFactory applicationModuleFactory = null;


  public TransactionInterceptor() {
    super();
  }

  public TransactionInterceptor(ApplicationModuleFactory factory) {
    this.applicationModuleFactory = factory;
  }

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    Object resultat = null;
    ApplicationModule appModule = null;

    try {
      appModule = this.applicationModuleFactory.getApplicationModuleCourant();

      resultat = invocation.proceed();

      if (ApplicationModule.class.isAssignableFrom(invocation.getMethod().getReturnType())) {

        ProxyFactory factory = new ProxyFactory(resultat.getClass().getInterfaces());
        factory.addAdvice(new TransactionInterceptor(this.applicationModuleFactory));
        factory.setTarget(resultat);
        resultat = factory.getProxy();
      } else {
        if (appModule != null) {
          appModule.getTransaction().commit();
        }
      }
    } catch(Throwable t) {
      if (appModule != null) {
        appModule.getTransaction().rollback();
      }
      throw t;
    }

    return resultat;
  }

  public void setApplicationModuleFactory(ApplicationModuleFactory applicationModuleFactory) {
    this.applicationModuleFactory = applicationModuleFactory;
  }

  public ApplicationModuleFactory getApplicationModuleFactory() {
    return applicationModuleFactory;
  }
}
