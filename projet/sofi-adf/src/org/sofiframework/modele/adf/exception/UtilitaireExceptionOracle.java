/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.exception;

import java.sql.SQLException;

import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.utilitaire.UtilitaireString;

public class UtilitaireExceptionOracle {
  // Constantes pour les erreurs ORA.
  private static final String CHECKCONS = "ORA-02290";
  private static final String FOREIGNKEY = "ORA-02291";
  private static final String RESTRICTDEL = "ORA-02292";
  private static final String UNIQUEKEY = "ORA-00001";

  //private static final String ERREURORA = "ORA-";

  // Constantes pour les erreurs JBO.
  //private static final String ERREURJBO = "JBO-";

  // Constante pour les erreurs DQS.
  //private static final String ERREURDQS = "ORA-20000";

  /**
   * Permet de traiter les contraintes d'intégration de la base de données
   * Oracle.
   *
   * Voici le possibilité traités:
   * <ul>
   * <li>ORA-02290: Check constraints</li>
   * <li>ORA-02291: Clé étragère</li>
   * <li>ORA-02292: Suppression refusé par la clé étrangère</li>
   * <li>ORA-00001: Clé unique</li>
   * </ul>
   */
  public static String violationContrainteBD(Object[] details) {
    String violatedConstraintName = null;

    if ((details.length != 0) && details[0] instanceof SQLException) {
      SQLException sqlException = (SQLException) details[0];
      String sqlMsg = sqlException.getMessage();

      if (sqlMsg.startsWith(CHECKCONS) || sqlMsg.startsWith(FOREIGNKEY) ||
          sqlMsg.startsWith(RESTRICTDEL) || sqlMsg.startsWith(UNIQUEKEY)) {
        // le nom de contrainte est préfixer par le propriétaire du schéma, séparé par "."
        int posStart = sqlMsg.indexOf(".");
        int posEnd = sqlMsg.indexOf(")", posStart);
        violatedConstraintName = sqlMsg.substring(posStart + 1, posEnd);
      }
    }

    return violatedConstraintName;
  }

  /**
   * Est-ce que l'exception est une contrainte d'intégrité sur la clé unique ?
   * @param details le detail de l'erreur de la base de données.
   * @return true si une contrainte d'intégrité sur la clé unique est lancé.
   */
  static public boolean isCleUniqueException(Object[] details) {
    if ((details.length != 0) && details[0] instanceof SQLException) {
      SQLException sqlException = (SQLException) details[0];
      String sqlMsg = sqlException.getMessage();

      if (sqlMsg.startsWith(UNIQUEKEY)) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * Est-ce que l'exception est une contrainte d'intégrité sur la clé unique ?
   * @param codeErreurBD le code d'erreur de la base de donnée.
   * @return true si une contrainte d'intégrité sur la clé unique est lancé.
   */
  static public String getMessageErreurBD(Object[] details) {
    if ((details.length != 0) && details[0] instanceof SQLException) {
      SQLException sqlException = (SQLException) details[0];
      String messageSql = sqlException.getMessage();

      String separateur = GestionParametreSysteme.getInstance().getString("separateurCodeErreurBD");
      String separateurRemplacement = GestionParametreSysteme.getInstance()
          .getString("separateurRemplacementCodeErreurBD");
      String codeApplication = GestionMessage.getInstance().getApplication();

      if (!UtilitaireString.isVide(separateur) &&
          !UtilitaireString.isVide(codeApplication)) {
        StringBuffer codeErreurBD = new StringBuffer();
        codeErreurBD.append(codeApplication.toUpperCase());
        codeErreurBD.append(separateur);

        // Rechercher un code d'erreur dans dans le message SQL
        String messageCodeErreur = messageSql.substring(messageSql.indexOf(
            codeErreurBD.toString()));
        int indicePremierBlanc = messageCodeErreur.indexOf("ORA");
        messageCodeErreur = messageCodeErreur.substring(0, indicePremierBlanc);

        messageCodeErreur = messageCodeErreur.replace(separateur.toCharArray()[0],
            separateurRemplacement.toCharArray()[0]);

        return messageCodeErreur;
      }

      return messageSql;
    }

    return "";
  }
}
