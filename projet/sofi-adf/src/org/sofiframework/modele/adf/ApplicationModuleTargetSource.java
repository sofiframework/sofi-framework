/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import org.springframework.aop.target.AbstractBeanFactoryBasedTargetSource;

/**
 * Cible utilisable par un proxy pour l'utilisation
 * du pool de module d'application ADF.
 * 
 * Exemple d'utilisation de la cible d'un proxy
 * de module d'application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ApplicationModuleTargetSource extends AbstractBeanFactoryBasedTargetSource {

  /**
   * 
   */
  private static final long serialVersionUID = 3742591661071188169L;
  /**
   * Factory qui est utilisé pour obtenir les instances de module d'application.
   */
  private ApplicationModuleFactory factory = null;

  /**
   * Obtenir une instance du module d'application qui
   * est utilisé comme cible par le proxy.
   * 
   * @return Application module
   */
  @Override
  public Object getTarget() throws Exception {
    return factory.get();
  }

  /**
   * Libère l'instance du module d'application.
   */
  @Override
  public void releaseTarget(Object target) throws Exception {
  }

  /**
   * Obtenir le factory d'application module.
   * @return Factory
   */
  public ApplicationModuleFactory getFactory() {
    return factory;
  }

  /**
   * Affecter le factory d'application module.
   * @param factory Application module factory
   */
  public void setFactory(ApplicationModuleFactory factory) {
    this.factory = factory;
  }
}
