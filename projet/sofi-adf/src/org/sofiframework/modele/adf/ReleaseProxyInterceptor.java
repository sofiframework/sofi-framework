/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import oracle.jbo.ApplicationModule;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.Service;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.springframework.aop.framework.ProxyFactory;

/**
 * Intercepteur qui permet de libérer le module d'application suite
 * à une méthode qui ne retourne pas un service. Cet intercepteur est récurcif.
 * C'est à dire qu'il va remplacer les BaseServiceImpl retournés par les getService
 * par un proxy avec ce même intercepteur. Ainsi, c'est le premier appel à une méthode
 * non getService qui relâche le modele. Le proxy de service créé implémente toutes les
 * interfaces implémentées par l'instance du service.
 * 
 * Exemple : modele.getServiceA().getServiceB().mamethodedeservice().
 *   getServiceA est intercepté la valeur retournée est un proxy.
 *   getServiceB est intercepté la valeur retournée est un proxy.
 *   mamethodedeservice() est intercepté et le modèle est relâché suite à l'exécution.
 * 
 * IMPORTANT : Cet intercepteur n'est utilisé que pour le cas ou on utilise
 * ADF dans Spring en cas de tests seulement. Le filtre ADF est
 * utilisé pour relâcher l'application module associé à l'utilisateur
 * à la fin de la requête lors de l'exécution dans un conteneur Web.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ReleaseProxyInterceptor implements MethodInterceptor {

  private ApplicationModuleFactory applicationModuleFactory = null;
  private GestionnaireUtilisateur gestionnaireUtilisateur = null;

  public ReleaseProxyInterceptor() {
    super();
  }

  public ReleaseProxyInterceptor(ApplicationModuleFactory factory) {
    this.applicationModuleFactory = factory;
  }

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    Object resultat = null;
    try {
      if (applicationModuleFactory.getApplicationModuleCourant() != null) {
        // Fixer le gestionnaire de l'utilisateur dans le service.
        Service modele = (Service) applicationModuleFactory.getApplicationModuleCourant();
        if (getGestionnaireUtilisateur() != null) {
          modele.setGestionnaireUtilisateur(getGestionnaireUtilisateur());
        }
      }

      resultat = invocation.proceed();
    } finally {
      if (resultat instanceof ApplicationModule) {
        ProxyFactory factory = new ProxyFactory(resultat.getClass().getInterfaces());
        factory.addAdvice(new ReleaseProxyInterceptor(this.applicationModuleFactory));
        factory.setTarget(resultat);
        resultat = factory.getProxy();
      } else {
        if (this.applicationModuleFactory.getApplicationModuleCourant() == null) {
          throw new SOFIException("Vous devez toujours passer par le modèle lors " +
              "des appels basés sur l'intercepteur. " +
              "Exemple : modele.getServiceA().methodeServiceA(); " +
              "Ne jamais utiliser : Service x = modele.getServiceX(); " +
              "List a = x.getListeA(); " +
              "List b = x.getListeB();");
        }

        applicationModuleFactory.releaseApplicationModuleCourant();
      }
    }

    return resultat;
  }

  public ApplicationModuleFactory getApplicationModuleFactory() {
    return applicationModuleFactory;
  }

  public void setApplicationModuleFactory(ApplicationModuleFactory applicationModuleFactory) {
    this.applicationModuleFactory = applicationModuleFactory;
  }

  public GestionnaireUtilisateur getGestionnaireUtilisateur() {
    return gestionnaireUtilisateur;
  }

  public void setGestionnaireUtilisateur(
      GestionnaireUtilisateur gestionnaireUtilisateur) {
    this.gestionnaireUtilisateur = gestionnaireUtilisateur;
  }
}
