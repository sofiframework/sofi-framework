/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.generique.utilitaire;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;

/**
 * Classe utilitaire qui permet de générer des nom de services et
 * d'accès de données à partir des classes d'objet de transfert.
 * La nomenclature utilisée est paramétrisée à partir du fichier
 * modele-parametre-sofi.xml du projet modele de l'application.
 */
public class UtilitaireServiceGenerique {

  /*
   * Paramétrisation de la nomenclature
   */
  public static String DEFINITION_SERVICE_GENERIQUE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("definitionServiceGenerique");
  public static String PAQUETAGE_OBJET_TANSFERT = (String) GestionParametreSysteme.getInstance().getParametreSysteme("paquetageObjetTransfert");

  public static String PAQUETAGE_SERVICE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("paquetageService");
  public static String PREFIXE_SERVICE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("prefixeService");
  public static String SUFFIXE_SERVICE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("suffixeService");

  public static String PAQUETAGE_ACCES_DONNEE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("paquetageAccesDonnee");
  public static String PREFIXE_ACCES_DONNEE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("prefixeAccesDonnee");
  public static String SUFFIXE_ACCES_DONNEE = (String) GestionParametreSysteme.getInstance().getParametreSysteme("suffixeAccesDonnee");


  /**
   * Constructeur par défaut privé. Cette classe n'a pas besoin d'être instanciée.
   */
  private UtilitaireServiceGenerique() {
  }

  /**
   * Permet de générer le nom de la définition d'un service BC4J à partir du nom de la classe de l'objet de transfert.
   * 
   * Les particularités de nomenclature du projet doivent êter définit par des paramètes. Voir la définitinon de cette
   * classe pour plus de détail.
   * 
   * Exemple :
   * Objet de transfert : org.sofiframework.app.modele.domaine.dto.Groupe
   * Nom de définitinon de Service : org.sofiframework.app.modele.domaine.service.ServiceGroupe
   * 
   * @return NOm de définityion de service
   * @param classeObjetTransfert Classe de l'objet de transfert dont on désire obtenir le nom de service générique
   */
  public static String genererNomDefinitionService(Class classeObjetTransfert) {
    return genererDefinition(classeObjetTransfert, PAQUETAGE_SERVICE, PREFIXE_SERVICE, SUFFIXE_SERVICE);
  }

  /**
   * Permet de générer le nom de la définition d'un acès de données BC4J à partir du nom de classe de l'objet de transfert.
   * 
   * Les particularités de nomenclature du projet doivent êter définit par des paramètes. Voir la définitinon de cette
   * classe pour plus de détail.
   * 
   * Exemple : org.sofiframework.app.modele.domaine.objetTransfert.Client
   * Objet de transfert : org.sofiframework.app.modele.domaine.ad.ClientAD
   * 
   * @return
   * @param classeObjectTransfert
   */
  public static String genererNomDefinitionAD(Class classeObjetTransfert) {
    return genererDefinition(classeObjetTransfert, PAQUETAGE_ACCES_DONNEE, PREFIXE_ACCES_DONNEE, SUFFIXE_ACCES_DONNEE);
  }

  /**
   * Modifie le nom d'une classe pour générer un nom de définition BC4J. On remplace
   * le nom de paquetage de l'objet de transfert et on ajoute un prefixe et un suffixe.
   * 
   * @param suffixe Nomenclature standard qui se place après le nom de l'entité. Ex. : AD (GroupeAD)
   * @param prefixe Nomenclature standard qui se place avant ;le nom de l'entité. Ex. : Service (ServiceGroupe)
   * @param paquetage Nom de paquetage qui doit remplacer le paquetage de l'objet de transfert
   *        Ex. : "service" qui remplace "dto" dans com.nurun.app.modele.groupe.dto.Groupe.
   * @param classeObjetTransfert Classe de l'objet de transfert dont on veut obtenir une définition
   * de composant d'affaire.
   */
  public static String genererDefinition(Class classeObjetTransfert, String paquetage, String prefixe, String suffixe) {

    StringBuffer nomDefinition = new StringBuffer();
    String nomClasseObjetTransfert = classeObjetTransfert.getName();

    int indexPaquetageOT = nomClasseObjetTransfert.indexOf("." + PAQUETAGE_OBJET_TANSFERT + ".");
    int indexDernierPoint = nomClasseObjetTransfert.lastIndexOf(".");

    nomDefinition.append(nomClasseObjetTransfert.substring(0, indexPaquetageOT));
    nomDefinition.append(".").append(paquetage).append(".");

    if (prefixe != null) {
      nomDefinition.append(prefixe);
    }

    nomDefinition.append(nomClasseObjetTransfert.substring(indexDernierPoint + 1));

    if (suffixe != null) {
      nomDefinition.append(suffixe);
    }

    return nomDefinition.toString();
  }
}