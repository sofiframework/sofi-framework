/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.generique;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.modele.adf.BaseServiceImpl;
import org.sofiframework.modele.adf.BaseViewObjectImpl;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Classe qui permet d'efectuer les opérations de base sur un accès de données
 * à partir de la classe d'un objet de transfert (ajout, modification,
 * lecture, destruction).
 * <p/>
 * La classe se base sur les règles de nomenclature
 * des packages de l'objet de  transfert pour savoir quelle vue dans être
 * utilisée.
 * <p/>
 * Les paramètres suivants  sont nécessaires au bon fonctionnement
 * de la classe :  <br/>
 * -  <br/>
 * -  <br/>
 * Les paramètres doivent être ajouté au fichier de paramètres SOFI.
 * <p/>
 * Pour obtenir un service générique, utiliser le modèle de
 * l'application et appler la méthode  getServiceGenerique(...).
 */
public class BaseServiceGeneriqueImpl extends BaseServiceImpl implements org.sofiframework.modele.adf.generique.common.BaseServiceGenerique  {

  private static Log log = LogFactory.getLog(BaseServiceGeneriqueImpl.class);

  /**
   * Contient la corespondance entre les objets de transfert et les types de
   * colonnes de l'objet de transfert. Idéaliement lorsque l'on fait un
   * service générique utilisant des notions de polymorphisme, on pourrait
   * prédéfinir  la corresponmdance entre rangée du DAO et objet lors de la
   * programmation du service. On peut utiliser une clause suivante pour la
   * définir dans le constructeur.
   */
  private HashMap classeRangeeClasseObjetTransfert = new HashMap();

  /**
   * Nom de la vue qui correspond à l'objet de transfert spécifié lors de
   * l'accès au service.
   */
  private String nomDefinitionAD = null;

  /**
   * Dans le cas d'un service polymorphique, la correspondance entre les
   * classes DTO et rangée doit être prédéfinie. Cet indicateur permet
   * d'empêcher d'assigner une valeur  par défaut au service et d'écraser la
   * valeur prédéfinie.
   */
  private boolean servicePolymorphique = false;

  /**
   * Constructeur par défaut
   */
  public BaseServiceGeneriqueImpl() {
  }

  /**
   * Permet d'initialiser un service avec les informations qui lui sont essentielles.
   * @param correspondancePolymorphique  HashMap qui contient les correspondances entre
   *        les classes de rangees d'accès de données et les classes d'objets de transfert
   * @param nomDefinitionAD Nom de la définition de l'accès de données polymorphique à utiliser
   * 
   */
  public void initialiserServicePolymorphique(String nomDefinitionADPolymorphique, HashMap classeRangeeClasseObjetTransfert) {
    this.setServicePolymorphique(true);
    this.setNomDefinitionAD(nomDefinitionAD);
    this.setClasseRangeeClasseObjetTransfert(classeRangeeClasseObjetTransfert);
  }

  /**
   * Ajoute l'objet spécifié à la base de données.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException Erreur survenu lors de l'acces au modèle
   * @return Objet après son insertion. Par exemple, la clé primaire est ajouté à l'objet.
   * @param nouvelObjetTranfert Objet que l'on désire insérer
   */
  @Override
  public ObjetTransfert ajouter(ObjetTransfert nouvelObjetTranfert)
      throws ModeleException {
    long debut = System.currentTimeMillis();

    try {
      String definition = servicePolymorphique ? org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique.genererNomDefinitionAD(nouvelObjetTranfert.getClass()) : this.nomDefinitionAD;
      BaseViewObjectImpl ad = this.getAccesDonnees(definition);
      nouvelObjetTranfert = ad.ajouterRangee(this, nouvelObjetTranfert);
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu lors de l'insertion d'un objet de classe "
          + nouvelObjetTranfert.getClass().getName(), ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp ajouter() : " + (fin - debut));
      }
    }

    return nouvelObjetTranfert;
  }

  /**
   * Modifie l'objet spécifié dans la base de données.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException Erreur survenu lors de l'accès au modèle
   * @return Objet une foi la mise à jour de la base de données effectuée
   * @param objetTranfert Objet de transfert que l'on désire mettre à jour dans la base de données
   */
  @Override
  public ObjetTransfert modifier(ObjetTransfert objetTranfert)
      throws ModeleException {
    long debut = System.currentTimeMillis();

    try {
      String definition = servicePolymorphique ? org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique.genererNomDefinitionAD(objetTranfert.getClass()) : this.nomDefinitionAD;
      BaseViewObjectImpl ad = this.getAccesDonnees(definition);
      objetTranfert = ad.modifierRangee(this, objetTranfert);
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu lors de l'insertion d'un objet de classe "
          + objetTranfert.getClass().getName(), ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp modifier() : " + (fin - debut));
      }
    }

    return objetTranfert;
  }

  /**
   * Supprime un enregistrement de la base de données à l'aide de sa clé primaire
   * 
   * @throws org.sofiframework.modele.exception.ModeleException Exception survenu lors de l'accès au modèle
   * @param cle Clé primaire de l'objet que l'on désire supprimer de la base de données
   */
  @Override
  public void supprimer(Object[] cle)
      throws ModeleException {
    long debut = System.currentTimeMillis();
    boolean erreurCleIncoherente = false;

    try {
      BaseViewObjectImpl ad = this.getAccesDonnees();

      if (ad.isCleCoherente(cle)) {
        ad.supprimerRangee(this, ad.getConditionSqlClePrimaire(), cle);
      } else {
        erreurCleIncoherente = true;
      }
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu lors de la suppression d'un objet avec le service "
          + this.getName() + " et de l'accès de données " + this.nomDefinitionAD
          + " avec la clé " + cle, ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp supprimer() : " + (fin - debut));
      }
    }

    if (erreurCleIncoherente)  {
      throw new ModeleException("Clé primaire spécifié pour la suppression n'est pas cohérente avec la définition des attributs de la clé dans l'accès de données.");
    }
  }

  /**
   * Obtenir un objet à l'aide de sa clé primaire.
   * @throws org.sofiframework.modele.exception.ModeleException Exception survenu lors de l'accès au modèle
   * @return Objet de transfert demandé
   * @param clé primaire qui identifie uniquement l'objet que l'in désire obtenir
   */
  @Override
  public ObjetTransfert getObjetTransfert(Object[] cle)
      throws ModeleException {
    long debut = System.currentTimeMillis();
    ObjetTransfert objetTransfert = null;

    try  {
      BaseViewObjectImpl ad = this.getAccesDonnees();
      objetTransfert = this.getObjetTransfert(ad, classeRangeeClasseObjetTransfert, ad.getConditionSqlClePrimaire(), cle, null);
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu pour obtenir un objet  à l'aide du service "
          + this.getName() + " et de l'accès de données " + this.nomDefinitionAD, ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp getObjetTransfert() : " + (fin - debut));
      }
    }

    return objetTransfert;
  }

  /**
   * Obtenir la liste paginée des objets de transferts. La liste des affectée
   * par l'attribut filtre inclus dans la liste est par les attributs de
   * tries configurés.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException Erreur survenu lors de l'accès au modèle
   * @return Liste de navigation qui contient la page de résultat provenant de l'accès de données
   * @param liste Liste de navigation
   */
  @Override
  public ListeNavigation getObjetTransfertListeNavigation(ListeNavigation liste)
      throws ModeleException {
    long debut = System.currentTimeMillis();

    try {
      BaseViewObjectImpl ad = this.getAccesDonnees();
      liste = this.getObjetTransfertListeNavigation(ad, this.classeRangeeClasseObjetTransfert, liste);
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu pour obtenir une liste de navigation à l'aide du service "
          + this.getName() + " et de l'accès de données " + this.nomDefinitionAD, ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp getObjetTransfertListeNavigation() : " + (fin - debut));
      }
    }

    return liste;
  }

  /**
   * Spécifier la classe de l'objet de transfert associé au service. Cette spécification est entre autre
   * utilisée lorsque le service retourne un objet de transfert.
   * L'appelant de la méthode n'a pas a spécifier la classe à retourner. Le nom de classe est aussi
   * utilisé pour générer le nom d'accès de données qui sera utilisé par le service.
   * 
   * @param classeObjectTransfert Classe de l'objet de transfert qui est associé au service en cours.
   */
  @Override
  public void specifierClasseObjetTransfertPourService(Class classeObjectTransfert) {
    /*
     * On doit seulement assigner une valeur de correspondance si il s'agit d'un
     * service non polymorphique. Dans le cas d'une service polymorphique il faut
     * spécifier manuellemenent les valeurs dans le constructeur du service :
     *   this.initialiserServicePolymorphique(...);
     */
    if (!this.servicePolymorphique) {
      // Permet d'utiliser la signature de méthode de BaseService qui utilise un HashMap...
      HashMap classeObjetTransfertMap = new HashMap();
      classeObjetTransfertMap.put("", classeObjectTransfert);
      this.classeRangeeClasseObjetTransfert = classeObjetTransfertMap;

      // Nom de l'accès de donné est déduit à partir du nom de la classe de l'objet de transfert
      this.nomDefinitionAD = org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique.genererNomDefinitionAD(classeObjectTransfert);
    }
  }

  /**
   * Obtenir l'accès de données associé au service générique en cours de traitement. Le nom de
   * l'accès de données est déduit à partir du nom de l'objet de transfert spécifié lors de
   * l'accès au service.
   * @param nomDefinitionAccesDonnees Nom de la definition de l'accès de données que l'on désire obtenir
   * @return Accès de données
   */
  protected BaseViewObjectImpl getAccesDonnees(String nomDefinitionAccesDonnees) throws ModeleException {
    long debut = System.currentTimeMillis();
    String nomAD = nomDefinitionAccesDonnees.substring(this.nomDefinitionAD.lastIndexOf(".") + 1);
    BaseViewObjectImpl ad = (BaseViewObjectImpl)this.findViewObject(nomAD);

    // on doit crée l'accès de données si il n,est pas déjà associé au service.
    if (ad == null) {
      try {
        ad = (BaseViewObjectImpl)this.createViewObject(nomAD, nomDefinitionAccesDonnees);
      } catch (Exception e) {
        throw new ModeleException("Erreur lors de la création de l'accès de données "
            + nomDefinitionAccesDonnees + ". Veuillez vérifier si cette accès de données existe.");
      }
    }

    if (log.isDebugEnabled()) {
      long fin = System.currentTimeMillis();
      log.debug("Temp getAD : " + (fin - debut));
    }

    return ad;
  }

  /**
   * Obtenir l'accès de données associé au service générique en cours de traitement. Le nom de
   * l'accès de données est déduit à partir du nom de l'objet de transfert spécifié lors de
   * l'accès au service.
   * @param nomDefinitionAccesDonnees Nom de la definition de l'accès de données que l'on désire obtenir
   * @throws org.sofiframework.modele.exception.ModeleException Erreur survenu lors de l'accès au modèle
   * @return Accès de données
   */
  protected BaseViewObjectImpl getAccesDonnees() throws ModeleException {
    return this.getAccesDonnees(this.nomDefinitionAD);
  }

  /**
   * Obtenir la liste des objets de transfert qui correspondent aux critères du filtre spécifié.
   * @throws org.sofiframework.modele.exception.ModeleException Erreur lors de l'accès au modèle
   * @return Liste des objets de tranfert
   * @param filtre Objet filtre spécifiant les critères de recherche
   */
  @Override
  public List getObjetTransfertListe(ObjetFiltre filtre) throws ModeleException {
    ArrayList liste = null;
    long debut = System.currentTimeMillis();

    try  {
      BaseViewObjectImpl ad = this.getAccesDonnees();
      liste = this.getObjetTransfertListe(ad, this.classeRangeeClasseObjetTransfert, filtre);
    } catch (ModeleException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new ModeleException("Erreur inatendu survenu pour obtenir une liste filtrée à partir du service "
          + this.getName() + " et de l'accès de données " + this.nomDefinitionAD, ex);
    } finally {
      if (log.isDebugEnabled()) {
        long fin = System.currentTimeMillis();
        log.debug("Temp getObjetTransfertListe() : " + (fin - debut));
      }
    }

    return liste;
  }

  /**
   * Affecter si le service est de type polymorphique.
   * @param servicePolymorphique Si le service est de type polymorphique.
   */
  public void setServicePolymorphique(boolean servicePolymorphique) {
    this.servicePolymorphique = servicePolymorphique;
  }

  /**
   * Obtenir si le service est  de type polymorphique.
   * @return si le service est polymorphique
   */
  public boolean isServicePolymorphique() {
    return servicePolymorphique;
  }

  /**
   * Affecter le nom de la définitnion de l'accès de données
   * qui est utilisé utilisé par le service.
   * 
   * @param nomDefinitionAD Nom de l'accès de données
   */
  public void setNomDefinitionAD(String nomDefinitionAD) {
    this.nomDefinitionAD = nomDefinitionAD;
  }

  /**
   * Obtenir le nom de la définition d'accès de données associé au service
   * en cours. Cette définitione est utilisée pour récupérer dynamiquement
   * l'accès de données lors d'appels des méthodes du service.
   * 
   * @return Nom d'une définition d'accès de données
   */
  public String getNomDefinitionAD() {
    return nomDefinitionAD;
  }

  /**
   * Affecter la valeur de correspondances entre les types d'objets de transferts
   * et les classes des rangées supportés par l'accès de données du service. Cette
   * méthode doit être utilisée dans le cas d'un service est d'une vue
   * utilisant le polymorphisme
   * 
   * @param classeRangeeClasseObjetTransfert Correspondances entre
   * les objets de transfert est les classe de rangées
   */
  public void setClasseRangeeClasseObjetTransfert(HashMap classeRangeeClasseObjetTransfert) {
    this.classeRangeeClasseObjetTransfert = classeRangeeClasseObjetTransfert;
  }

  /**
   * Obtenir la structure des correspondances entre les objets de transfert et
   * les classes de rangées.
   * 
   * @return Correspondances entre
   * les objets de transfert est les classe de rangées
   */
  public HashMap getClasseRangeeClasseObjetTransfert() {
    return classeRangeeClasseObjetTransfert;
  }

  /**
   * 
   *  Sample main for debugging Business Components code using the tester.
   */
  public static void main(String[] args) {
    launchTester("org.sofiframework.modele.adf.generique", "BaseServiceGeneriqueLocal");
  }
}