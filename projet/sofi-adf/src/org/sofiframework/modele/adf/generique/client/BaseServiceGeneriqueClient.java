/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.generique.client;

import oracle.jbo.client.remote.ApplicationModuleImpl;

public class BaseServiceGeneriqueClient extends ApplicationModuleImpl implements org.sofiframework.modele.adf.generique.common.BaseServiceGenerique {
  /**
   * 
   *  This is the default constructor (do not remove)
   */
  public BaseServiceGeneriqueClient() {
  }

  @Override
  public org.sofiframework.objetstransfert.ObjetTransfert ajouter(org.sofiframework.objetstransfert.ObjetTransfert nouvelObjetTranfert) {
    Object _ret = this.riInvokeExportedMethod(this, "ajouter", new String[] {"org.sofiframework.objetstransfert.ObjetTransfert"}, new Object[] {nouvelObjetTranfert});
    return (org.sofiframework.objetstransfert.ObjetTransfert)_ret;
  }

  @Override
  public org.sofiframework.objetstransfert.ObjetTransfert modifier(org.sofiframework.objetstransfert.ObjetTransfert objetTranfert) {
    Object _ret = this.riInvokeExportedMethod(this, "modifier", new String[] {"org.sofiframework.objetstransfert.ObjetTransfert"}, new Object[] {objetTranfert});
    return (org.sofiframework.objetstransfert.ObjetTransfert)_ret;
  }

  @Override
  public void supprimer(Object[] cle) {
    this.riInvokeExportedMethod(this, "supprimer", new String[] {"[Ljava.lang.Object;"}, new Object[] {cle});
    return;
  }

  @Override
  public org.sofiframework.objetstransfert.ObjetTransfert getObjetTransfert(Object[] cle) {
    Object _ret = this.riInvokeExportedMethod(this, "getObjetTransfert", new String[] {"[Ljava.lang.Object;"}, new Object[] {cle});
    return (org.sofiframework.objetstransfert.ObjetTransfert)_ret;
  }

  @Override
  public org.sofiframework.composantweb.liste.ListeNavigation getObjetTransfertListeNavigation(org.sofiframework.composantweb.liste.ListeNavigation liste) {
    Object _ret = this.riInvokeExportedMethod(this, "getObjetTransfertListeNavigation", new String[] {"org.sofiframework.composantweb.liste.ListeNavigation"}, new Object[] {liste});
    return (org.sofiframework.composantweb.liste.ListeNavigation)_ret;
  }

  @Override
  public void specifierClasseObjetTransfertPourService(Class classeObjectTransfert) {
    this.riInvokeExportedMethod(this, "specifierClasseObjetTransfertPourService", new String[] {"java.lang.Class"}, new Object[] {classeObjectTransfert});
    return;
  }

  @Override
  public java.util.List getObjetTransfertListe(org.sofiframework.composantweb.liste.ObjetFiltre filtre) {
    Object _ret = this.riInvokeExportedMethod(this, "getObjetTransfertListe", new String[] {"org.sofiframework.composantweb.liste.ObjetFiltre"}, new Object[] {filtre});
    return (java.util.List)_ret;
  }
}