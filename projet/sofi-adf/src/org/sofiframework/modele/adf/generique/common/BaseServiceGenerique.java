/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.generique.common;

import java.util.List;

import oracle.jbo.ApplicationModule;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

public interface BaseServiceGenerique extends ApplicationModule {

  public ObjetTransfert ajouter(ObjetTransfert nouvelObjetTranfert) throws ModeleException;

  public ObjetTransfert modifier(ObjetTransfert objetTranfert) throws ModeleException;

  public void supprimer(Object[] cle) throws ModeleException;

  public ObjetTransfert getObjetTransfert(Object[] cle) throws ModeleException;

  public ListeNavigation getObjetTransfertListeNavigation(ListeNavigation liste) throws ModeleException;

  public void specifierClasseObjetTransfertPourService(Class classeObjectTransfert);

  public List getObjetTransfertListe(ObjetFiltre filtre) throws ModeleException;
}