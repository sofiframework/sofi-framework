/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.sql.ResultSet;

import oracle.jbo.server.ViewRowImpl;

/**
 * Classe pour les objets d'accès aux données destinés aux listes de navigations.
 *
 * Donne essentiellement les mêmes services que la classe ViewRowImpl et
 * comporte des optimisations spécialisées permettant de trancher en deux le temps
 * d'exécution de la procédure de requête des données.
 * 
 * @see ViewRowImpl, ListeViewRowImpl
 * 
 * @author Steven Charette (Nurun inc.)
 * @version SOFI 2.0
 */
public class ListeViewRowImpl extends ViewRowImpl  {
  /**
   * @override
   */
  @Override
  protected void populate(ResultSet resultset)
  {
    super.populate(resultset);
    //populer l'attribut dynamique.
    try{
      Object obj = resultset.getObject(ListeViewObjectImpl.OPTIMIZER_COLUMN_NAME);
      populateAttribute(getAttributeIndexOf(ListeViewObjectImpl.OPTIMIZER_COLUMN_NAME), obj);
    }catch(Exception e){
      throw new RuntimeException("L'attribut : "
          + ListeViewObjectImpl.OPTIMIZER_COLUMN_NAME + " n'a put être sourcé.");
    }
  }
}