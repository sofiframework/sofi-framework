/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import oracle.jbo.AttributeDef;
import oracle.jbo.JboException;
import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.domain.Array;
import oracle.jbo.server.SQLBuilder;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.server.ViewRowSetImpl;
import oracle.sql.ArrayDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.AttributFiltre;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ListeTriAttribut;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.composantweb.liste.Operateur;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.modele.adf.convertisseur.Convertisseur;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe de base pour les objets d'accès aux données
 *
 * Permet d'appliquer plusieurs utilitaires sur un objet d'accès aux données.
 * Comme par exemple d'appliquer un fitre avec l'aide d'un objet de type filtre,
 * d'appliquer une liste de navigation page par page, de rechercher une rangée
 * particulière avec l'aide d'une clé, etc..
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since 2.0.2 Ajout de la méthode isExiste(...).
 */
public abstract class BaseViewObjectImpl extends ViewObjectImpl {
  // L'instance de journalisation pour cette classe.
  private Log log = LogFactory.getLog(BaseViewObjectImpl.class);

  // Est-ce que l'objet d'accès aux données est optimisé pour l'itération page par page.
  private boolean optimisationIterateurPageParPage = true;

  // Est-ce que la methode getEstimatedRowCount pour estimer le nombre de rangée retournée doit être traité.
  private boolean estimationNombreRangeeAutomatique = true;

  // Le filtre utilisé.
  private Filtre filtre = null;

  // Permettre l'ajout de condition SQL dans la condition original de l'accès aux données.
  private boolean ajouterConditionPourSqlOriginal = false;

  // Variable permettant de spécifié si on doit populer les objets imbriqués s'il y a lieu
  // dans les traitements personnalisé de l'objet d'accès aux données.
  private boolean populerObjetImbrique = false;

  /**
   * Appliquer un filtre à un objet d'accès aux données.
   * @param filtre Filtre à appliquer
   */
  public void appliquerFiltre(Filtre filtre) throws ModeleException {
    appliquerFiltre(filtre, true);
  }

  /**
   * Appliquer un filtre à un objet d'accès aux données.
   * @param filtre Filtre à appliquer
   * @param initialiser true si vous désirez faire une initialisation de la vue avant traitement.
   */
  public void appliquerFiltre(Filtre filtre,
      boolean initialiser) throws ModeleException {
    try {

      // Fixer le filtre
      setFiltre(filtre);

      if (initialiser) {
        this.initialiser();
      }

      Collection attributs = ((ObjetFiltre) filtre).produireValeurs();

      ViewCriteria viewCriteria = createViewCriteria();

      for (Iterator it = attributs.iterator(); it.hasNext(); ) {
        ViewCriteriaRow rangee = viewCriteria.createViewCriteriaRow();
        viewCriteria.add(rangee);

        rangee.setConjunction(ViewCriteriaRow.VCROW_CONJ_AND);

        AttributFiltre attribut = (AttributFiltre)it.next();
        String nom =
            UtilitaireString.convertirPremiereLettreEnMajuscule(attribut.getNom());
        Object valeur = attribut.getValeur();
        Operateur op = attribut.getOperateur();

        if (valeur instanceof Filtre) {
          throw new UnsupportedOperationException("Les filtre nested ne sont pas supporté " +
              "par la recherche ADF, vous devez utiliser des aliases.");
        } else if (valeur instanceof Date) {
          Date date = (Date)valeur;
          AttributeDef def = getAttributeDef(getAttributeIndexOf(nom));
          Class typeAttribut = def.getJavaType();

          if ((typeAttribut == oracle.jbo.domain.Timestamp.class) ||
              (typeAttribut == java.sql.Timestamp.class) && (def.getSQLType() == Types.TIMESTAMP)){
            rangee.setAttribute(nom, op + "'" + UtilitaireDate.convertirEn_AAAA_MM_JJHeureSecondeMilli(date) + "'");
          } else if ((typeAttribut == oracle.jbo.domain.Timestamp.class) ||
              (typeAttribut == java.sql.Timestamp.class)) {
            rangee.setAttribute(nom, op + formatDateHeure(date));
          } else if ((typeAttribut == oracle.jbo.domain.Date.class) ||
              (typeAttribut == java.sql.Date.class)) {
            rangee.setAttribute(nom, op + formatDate(date));
          } else {
            throw new IllegalArgumentException("La propriété '" + nom +
                "' n'est pas de type Date tel qu'attendu.");
          }
        } else if (valeur instanceof Boolean) {
          rangee.setAttribute(nom, op + " '" + format((Boolean)valeur) + "'");
        } else if (valeur instanceof Number) {
          rangee.setAttribute(nom, op + " " + valeur);
        } else if (valeur instanceof String) {
          rangee.setUpperColumns(true);

          String str = ((String)valeur).toUpperCase();
          str = UtilitaireString.traiterApostrophe(str);
          str = attribut.getMatchMode().toMatchString(str);

          if (op == Operateur.EGALE) {
            rangee.setAttribute(nom, "LIKE '" + str + "'");
          } else {
            rangee.setAttribute(nom, op + " '" + str + "'");
          }
        } else if (valeur.getClass().isArray()) {
          // Traitement pour les Array de String, ils sont tranformé en clause
          // IN avec le SQL
          // ex. [NOM_ATTRIBUT] IN ('VALEUR1', 'VALEUR2' ..., 'VALEURX')
          Object[] listeValeur = (Object[])valeur;

          if (listeValeur != null) {
            if (listeValeur.length > 0) {
              if ((op != null) && (op != Operateur.EGALE) &&
                  !UtilitaireString.isVide(listeValeur[0].toString())) {
                rangee.setAttribute(nom, op + " " + listeValeur[0]);
              } else {
                // Éviter de faire un traitement lorsqu'il n'y a qu'une seule
                // valeur et qu'elle est vide. Cela signifique que l'utilisateur
                // a seulement choisi la ligne vide, donc qu'il ne veux pas de
                // filtre pour cette attribut.
                if (!((listeValeur.length == 1) && (listeValeur[0] != null) &&
                    UtilitaireString.isVide(listeValeur[0].toString()))) {
                  StringBuffer condition = new StringBuffer(" IN (");

                  for (int i = 0; i < listeValeur.length; i++) {
                    if (i > 0) {
                      condition.append(",");
                    }

                    if (valeur instanceof String[]) {
                      condition.append("'");
                    }

                    condition.append(listeValeur[i]);

                    if (valeur instanceof String[]) {
                      condition.append("'");
                    }
                  }

                  condition.append(")");

                  rangee.setAttribute(UtilitaireString.convertirPremiereLettreEnMajuscule(nom),
                      condition);
                }
              }
            }
          }
        } else if (valeur == null) {
          rangee.setAttribute(nom, "IS NULL");
        } else {
          if (log.isInfoEnabled()) {
            log.info("Impossible de traiter la classe " +
                valeur.getClass().getName());
          }
        }
      }

      applyViewCriteria(viewCriteria);

      if (log.isDebugEnabled()) {
        debugSQL();
      }
    } catch (Exception e) {
      throw new ModeleException(e.getMessage(), e);
    }
  }

  private String format(Boolean valeur) {
    GestionParametreSysteme instance = GestionParametreSysteme.getInstance();
    String oui = instance.getString("oui");
    String non = instance.getString("non");

    return (valeur.booleanValue() ? oui : non).toUpperCase();
  }

  private String formatDateHeure(Date date) {
    return "to_date('" +
        UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(date) +
        "', 'YYYY-MM-DD HH24:MI:SS')";
  }

  private String formatDate(Date date) {
    return "to_date('" + UtilitaireDate.convertirEn_AAAA_MM_JJ(date) +
        "', 'YYYY-MM-DD')";
  }

  public void setWhereClauseArrayParam(int position, Object valeur,
      String type) throws ModeleException {
    Array arr = null;

    try {
      Connection conn = getConnection();
      ArrayDescriptor descriptor = new ArrayDescriptor(type, conn);
      arr = new Array(descriptor, conn, valeur);
    } catch (SQLException sqle) {
      throw new ModeleException("Erreur lors de la construction du parametre tableau.",
          sqle);
    }

    if (arr != null) {
      setWhereClauseParam(position, arr);
    }
  }

  private Connection getConnection() throws SQLException {
    PreparedStatement st =
        getDBTransaction().createPreparedStatement("commit", 1);
    Connection conn = st.getConnection();
    st.close();

    return conn;
  }

  /**
   * Appliquer un page par page itérateur en spécifiant s'il y a lieu une conditionSql
   * ainsi que les paramètres dynamiques associés.
   * @param liste La liste de navigation qui permet le page par page itérateur
   * @param conditionSql Condition SQL a appliquer à la vue.
   * @param parametresConditionSql valeurs des paramètre qui sont inclus dans la condition
   * @return Lignes qui correspondent aux critères de la recherche
   */
  public Row[] appliquerListeNavigation(ListeNavigation liste,
      String conditionSql,
      Object[] parametresConditionSql) {
    if (isOptimiserIterateurPageParPage() &&
        (liste.getMaxParPage().intValue() != -1)) {
      setAccessMode(RowSet.RANGE_PAGING);
    } else {
      if (log.isInfoEnabled()) {
        log.info("La vue n'est pas optimisé pour l'itération page par page");
      }
    }

    if (liste.getMaxParPage().intValue() != -1) {
      setIterMode(RowIterator.ITER_MODE_LAST_PAGE_PARTIAL);
    }

    if (conditionSql != null) {
      this.setWhereClause(conditionSql);
    }

    if (parametresConditionSql != null) {
      this.setWhereClauseParams(parametresConditionSql);
    }

    //Vérifie si on tiens compte des accents. C'est un paramètre système qui le détermine
    boolean fonctionTri = false;

    if (GestionParametreSysteme.getInstance().getParametreSysteme("fonctionTri") !=
        null) {
      fonctionTri = true;
    }

    if (liste.getTriAttribut() != null) {
      StringBuffer tri = new StringBuffer();

      if (liste.getTriFixe() != null) {
        tri.append(liste.getTriFixe());
        tri.append(", ");
      }

      // Si la liste d'attributs à trier est vide, trier selon l'ordre par défaut
      if (liste.getNbAttributLogiqueATrier() == 0) {
        String attributATrier = liste.getTriAttribut();
        String ordreTri = liste.getOrdreTri();
        if (liste.getTri().getTriComplexe(attributATrier) != null) {
          String sql = liste.getTri().getTriComplexe(attributATrier);
          tri.append(sql);
        } else {
          this.ajouterTri(attributATrier, ordreTri, fonctionTri, tri);
        }
        tri.append(" ");
        tri.append(ordreTri);
        tri.append(" ");
        tri.append(getNullsOrdre(ordreTri));
      } else {
        // Itérer parmis les éléments à trier
        for (int i = 0; i < liste.getNbAttributLogiqueATrier(); i++) {
          ListeTriAttribut listeTriAttribut =
              (ListeTriAttribut)liste.getListeAttributTriLogique(liste.getTriAttribut()).get(i);
          // Initialier les attributs pour le ordreBy du tri
          String attributATrier =
              listeTriAttribut.getTriAttributSupplementaire();

          // Ascendant - Descendant
          String ordreTri = "";
          // Dépendamment du cas, on initialise l'attribut triOrdre
          if (i == 0) {
            ordreTri = liste.getOrdreTri();
          } else {
            ordreTri = listeTriAttribut.getTriAttributOrdre();
          }

          /* Déduire le tri à partir du nom de l'attribut */
          this.ajouterTri(attributATrier, ordreTri, fonctionTri, tri);

          tri.append(" ");
          tri.append(ordreTri);
          tri.append(" ");
          tri.append(getNullsOrdre(ordreTri));

          // Si ce n'est pas le dernier élément à trier, il faut ajouter une virgule
          if (i < (liste.getNbAttributLogiqueATrier() - 1)) {
            tri.append(", ");
          }
        }
      }

      setOrderByClause(tri.toString());
    }

    int positionListe = liste.getPositionListe();
    int maxParPage = liste.getMaxParPage().intValue();

    // Spécifier le maximum par page
    this.setRangeSize(maxParPage);

    if (log.isInfoEnabled()) {
      log.info("Maximum de la page est : " + maxParPage);
    }

    // Spécifier le début de la liste;
    this.setRangeStart(positionListe);

    if (log.isInfoEnabled()) {
      log.info("Début de la page est : " + positionListe);
    }

    Row[] rows = getAllRowsInRange();

    // Si le numéro de page est différent de -1, faire l'estimation de nombre de rangée.
    if ((liste.getNoPage() != -1) && estimationNombreRangeeAutomatique) {
      liste.setNbListe(getEstimatedRowCount());
    }

    if ((liste.getNoPage() == -1) || !estimationNombreRangeeAutomatique) {
      // Si le numéro de page est -1, fixer le nombre dans la liste correspondant au résultat de la recherche.
      liste.setNbListe(rows.length);
    }

    setAccessMode(RowSet.SCROLLABLE);

    this.applyViewCriteria(null);

    if (log.isDebugEnabled()) {
      debugSQL();
    }

    return rows;
  }

  /**
   * Cette fonction permet de spécifier
   * l'ordre de tri des valeurs nulls selon
   * un tri ascendant ou descendant.
   * 
   * @param ordreTri
   * @return
   */
  private String getNullsOrdre(String ordreTri) {
    // le tri par défaut dans sofi
    String nullsOrdre = "Nulls last";

    if (ordreTri.equals("ASC")) {
      String nullsOrdreASC =
          (String)GestionParametreSysteme.getInstance().getParametreSysteme(ConstantesParametreSysteme.ORDRE_TRI_ASCENDANT_POUR_NULL);
      if (!UtilitaireString.isVide(nullsOrdreASC)) {
        nullsOrdre = nullsOrdreASC;
      }
    }
    if (ordreTri.equals("DESC")) {
      String nullsOrdreDESC =
          (String)GestionParametreSysteme.getInstance().getParametreSysteme(ConstantesParametreSysteme.ORDRE_TRI_DESCENDANT_POUR_NULL);
      if (!UtilitaireString.isVide(nullsOrdreDESC)) {
        nullsOrdre = nullsOrdreDESC;
      }
    }
    return nullsOrdre;
  }

  protected void ajouterTri(String attribut, String ordreTri,
      boolean fonctionTri, StringBuffer tri) {
    AttributeDef definition = this.getDefinitionAttribut(attribut);
    String nomAttributSQL = definition.getColumnNameForQuery();
    Class classeAttribut = definition.getJavaType();

    //Si on gère les accents, on concatène au order by
    //le nom du package qui sera appellé pour enlever les accents
    if (fonctionTri && classeAttribut.isAssignableFrom(String.class)) {
      tri.append(getFonctionTri(nomAttributSQL));
    } else {
      tri.append(nomAttributSQL);
    }
  }

  protected AttributeDef getDefinitionAttribut(String attribut) {
    AttributeDef[] attributeDef = getAttributeDefs();
    // Itérer parmis les attributs afin de trouver le nom de la colonne à trier
    boolean trouve = false;
    AttributeDef attr = null;

    for (int j = 0; (j < attributeDef.length) && !trouve; j++) {
      attr = attributeDef[j];
      if (attr.getName().equals(attribut)) {
        trouve = true;
      }
    }

    return attr;
  }

  /**
   * Ajoute au order by l'appel au package ou de fonction
   * @param service la colonne sur laquelle le tri est effectué
   * @return la string SQL qui appelle le package
   */
  private String getFonctionTri(String colonne) {
    StringBuffer retour = new StringBuffer();

    if (colonne != null) {
      String fonctionTri =
          GestionParametreSysteme.getInstance().getString("fonctionTri");
      retour.append(fonctionTri);
      retour.append("(upper(");
      retour.append(colonne.toString());
      retour.append("))");
    }

    return retour.toString();
  }

  /**
   * Appliquer un page par page itérateur.
   * @param liste La liste de navigation qui permet le page par page itérateur
   * @return lignes qui correspondent aux critères de la recherche
   */
  public Row[] appliquerListeNavigation(ListeNavigation liste) {
    return appliquerListeNavigation(liste, null, null);
  }

  /**
   * Permet l'initialisation de l'objet d'accès aux données en annulant
   * les conditions qui lui peuvent être associés.
   */
  public void initialiser() {
    this.clearViewCriterias();
    this.setWhereClause(null);
    this.setWhereClauseParams(new Object[] { });
    this.clearCache();
  }

  /**
   * Permet la recherche d'une rangée ou des rangées de l'objet d'accès
   * aux données correspondant à une clé,
   * @param attribut Clé
   * @return lignes qui correspondent à la clé fournie
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException objet de transfert non trouvé
   */
  public Row[] rechercherParCleSimple(Object attribut) throws ObjetTransfertNotFoundException {
    // Définition de la clé afin d'accèder à un utilisateur
    Key cle = new Key(new Object[] { attribut });
    Row[] rangees = this.findByKey(cle, 1);

    if (rangees.length == 0) {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }

    return rangees;
  }

  /**
   * Permet la recherche d'une ou des rangées de l'objet d'accès
   * aux données correspondant à une clé multiple,
   * @param attributs Clé multiple
   * @return lignes qui correspondent à la clé fournie
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException objet de transfert non trouvé
   */
  public Row[] rechercherParCleMultiple(Object[] attributs) throws ObjetTransfertNotFoundException {
    // Définition de la clé afin d'accèder à un utilisateur
    Key cle = new Key(attributs);
    Row[] rangees = this.findByKey(cle, 1);

    if ((rangees.length == 0) || (attributs == null)) {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }

    return rangees;
  }

  /**
   * Permet la recherche d'une ou des rangées de l'objet d'accès
   * aux données correspondant à une clé multiple.
   * @param attributs Clé
   * @param nbLigne nombre de ligne maximal désiré
   * @return un vecteur de rangée.
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException objet de transfert non trouvé
   */
  public Row[] rechercherParCleMultiple(Object[] attributs,
      int nbLigne) throws ObjetTransfertNotFoundException {
    // Définition de la clé afin d'accèder à un utilisateur
    Key cle = new Key(attributs);
    Row[] rangees = this.findByKey(cle, nbLigne);

    if (rangees.length == 0) {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }

    return rangees;
  }

  /**
   * Permet de créer un nouvel itérateur sur l'objet d'accès aux données.
   * Le cursor revient donc au début de liste et permet d'avoir plusieur
   * itération à l'intérieur du même objet d'accès aux données
   * @return RowSetIterator un nouvel itérateur
   */
  public RowSetIterator creerIterateur() {
    return this.createRowSetIterator(null);
  }

  /**
   * Indique si le paramètre système "traiterObjetTransfertNonModifie"
   * est vrai ou faux.
   */
  private static boolean traiterObjetTransfertNonModifie() {
    Boolean valeur =
        GestionParametreSysteme.getInstance().getBoolean("traiterObjetTransfertNonModifie");

    return (valeur != null) && valeur.booleanValue();
  }

  /**
   * Créer une nouvelle rangée de l'objet d'accès aux données avec
   * l'aide d'un objet de transfert.
   * <p>
   * Un postChanges est automatiquement lancé.
   * @param service le service courant utilisé (appliquer this).
   * @param objetTransfert l'objet de transfert à ajouter dans l'objet d'accès aux données.
   * @return l'objet de transfert ajouté.
   */
  public ObjetTransfert ajouterRangee(BaseServiceImpl service,
      ObjetTransfert objetTransfert) throws ModeleException {
    ObjetTransfert nouvelObjetTransfert = null;

    if (objetTransfert.isModifie() || traiterObjetTransfertNonModifie()) {
      // Traitement personnalisé pouvant être surchargé pour traitement avant début nouvelle insertion.
      traitementAvantInsertionRangee(objetTransfert);
      Row nouvelleRangee = this.createRow();

      String[] attributsCle = new String[this.getKeyAttributeDefs().length];

      for (int i = 0; i < this.getKeyAttributeDefs().length; i++) {
        attributsCle[i] = this.getKeyAttributeDefs()[i].getName();
      }

      for (int i = 0; i < attributsCle.length; i++) {
        Object valeurCle = null;

        try {
          valeurCle =
              UtilitaireObjet.getValeurAttribut(objetTransfert, attributsCle[i].substring(0,
                  1).toLowerCase() +
                  attributsCle[i].substring(1));
        } catch (Exception e) {
        }

        if ((valeurCle == null) &&
            (nouvelleRangee.getAttribute(attributsCle[i]) != null)) {
          try {
            UtilitaireObjet.setPropriete(objetTransfert,
                attributsCle[i].substring(0,
                    1).toLowerCase() +
                    attributsCle[i].substring(1),
                    nouvelleRangee.getAttribute(attributsCle[i]));
          } catch (InvocationTargetException e) {
          } catch (NoSuchMethodException e) {
            log.error(e.toString(), e);
          } catch (IllegalAccessException e) {
          }
        }
      }

      // Traitement personnalisé pouvant être surchargé.
      traitementAvantPopulationRangee(objetTransfert, nouvelleRangee);

      // Population de la rangée.
      service.populerRangee(objetTransfert, nouvelleRangee);

      this.insertRow(nouvelleRangee);

      try {
        service.postChanges();
      } catch (JboException e) {
        throw new ModeleException(e.getDetailMessage(), e);
      }

      nouvelObjetTransfert =
          service.populerObjetTransfert(getCurrentRow(), objetTransfert);
    } else {
      nouvelObjetTransfert = objetTransfert;
    }

    return nouvelObjetTransfert;
  }

  /**
   * Supprimer le ou les rangées correspondant à la clé spécifié
   * <p>
   * Un postChanges est automatiquement lancé.
   * @param service le service courant utilisé (appliquer this).
   * @param attributCle la ou les clés de la rangée à supprimer
   * @throws org.sofiframework.modele.exception.ModeleException l'exception lancé
   */
  public void supprimerRangee(BaseServiceImpl service, Object[] attributCle,
      boolean initialiser) throws ModeleException,
      ObjetTransfertNotFoundException {
    if (initialiser) {
      initialiser();
    }

    Row[] resultat = rechercherParCleMultiple(attributCle);

    for (int i = 0; i < resultat.length; i++) {
      Row rangee = resultat[i];

      // Traitement personnalisé pouvant être surchargé.
      traitementAvantSuppressionRangee(rangee);

      // Suprression de la rangée.
      rangee.remove();
      service.postChanges();
    }
  }

  /**
   * Supprimer le ou les rangées correspondant à la clé spécifié
   * <p>
   * Un postChanges est automatiquement lancé.
   * @param service le service courant utilisé (appliquer this).
   * @param attributCle la ou les clés de la rangée à supprimer
   * @throws org.sofiframework.modele.exception.ModeleException l'exception lancé
   */
  public void supprimerRangee(BaseServiceImpl service,
      Object[] attributCle) throws ModeleException,
      ObjetTransfertNotFoundException {
    supprimerRangee(service, attributCle, false);
  }

  /**
   * Supprimer le ou les rangées correspondant à la condition sql spécifié
   * <p>
   * Un postChanges est automatiquement lancé.
   * @param service le service courant utilisé (appliquer this).
   * @param conditionSql la ou les clés de la rangée à supprimer
   * @param parametresConditionSql paramètre sql à la condition
   * @throws org.sofiframework.modele.exception.ModeleException l'exception lancé
   */
  public void supprimerRangee(BaseServiceImpl service, String conditionSql,
      Object[] parametresConditionSql) throws ModeleException,
      ObjetTransfertNotFoundException {
    Row[] rangees =
        rechercherParConditionSql(conditionSql, parametresConditionSql);

    if (rangees == null) {
      throw new ObjetTransfertNotFoundException("Objet transfert non trouvé");
    }

    for (int i = 0; i < rangees.length; i++) {
      Row rangee = rangees[i];

      // Traitement personnalisé pouvant être surchargé.
      traitementAvantSuppressionRangee(rangee);

      // Suprression de la rangée.
      rangee.remove();
    }

    service.postChanges();
  }

  /**
   * Créer une nouvelle rangée de l'objet d'accès aux données avec
   * l'aide d'un objet de transfert.
   * <p>
   * Un postChanges automatiquement lancé.
   * @param le service courant utilisé (appliquer this).
   * @param objetTransfert l'objet de transfert à ajouter dans l'objet d'accès aux données.
   * @param la clé pour trouver la rangée désiré.
   * @return l'objet de transfert ajouté.
   */
  public ObjetTransfert modifierRangee(BaseServiceImpl service,
      ObjetTransfert objetTransfert,
      Object[] cle,
      boolean initialiser) throws ModeleException,
      ObjetTransfertNotFoundException {
    if (initialiser) {
      initialiser();
    }

    Row resultat = rechercherParCleMultiple(cle)[0];

    // Traitement personnalisé pouvant être surchargé.
    traitementAvantPopulationRangee(objetTransfert, resultat);

    // Population de la rangée.
    service.populerRangee(objetTransfert, resultat);
    service.postChanges();

    ObjetTransfert nouvelObjetTransfert =
        service.populerObjetTransfert(resultat, objetTransfert);

    return nouvelObjetTransfert;
  }

  /**
   * Créer une nouvelle rangée de l'objet d'accès aux données avec
   * l'aide d'un objet de transfert.
   * <p>
   * Un postChanges automatiquement lancé.
   * @param le service courant utilisé (appliquer this).
   * @param objetTransfert l'objet de transfert à ajouter dans l'objet d'accès aux données.
   * @param la clé pour trouver la rangée désiré.
   * @return l'objet de transfert ajouté.
   */
  public ObjetTransfert modifierRangee(BaseServiceImpl service,
      ObjetTransfert objetTransfert,
      Object[] cle) throws ModeleException,
      ObjetTransfertNotFoundException {
    return modifierRangee(service, objetTransfert, cle, false);
  }

  /**
   * Retourne le ou les rangées correspondant à une condition Sql, ainsi que les paramètres sql dynanmique,
   * et/ou un ordre de tri.
   * @param conditionSql la condition sql (obligatoire)
   * @param parametreSql le ou les paramètres dynamique associé à la condition sql (optionnel).
   * @return le ou les rangées sélectionné.
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException
   */
  public Row[] rechercherParConditionSql(String conditionSql,
      Object[] parametreSql) throws ObjetTransfertNotFoundException {
    // Définition de la clé afin d'accèder{
    this.initialiser();

    this.setWhereClause(conditionSql);
    this.setWhereClauseParams(parametreSql);

    int nbRange = 0;

    while (hasNext()) {
      next();
      nbRange++;
    }

    if (nbRange == 0) {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    } else {
      this.setRangeSize(nbRange);

      Row[] resultat = this.getAllRowsInRange();
      this.setRangeSize(1);

      return resultat;
    }
  }

  /**
   * Modification d'une rangée de l'objet d'accès aux données avec
   * l'aide d'un objet de transfert et d'une condition sql.
   * @param service le service utilisé (appliquer this).
   * @param objetTransfert l'objet de transfert qui contient la modification
   * @param conditionSql la condition sql
   * @param parametresConditionSql les paramètres dynamiques spécifier dans la condition sql.
   * @return l'objet de transfert modifié.
   * @throws org.sofiframework.modele.exception.ModeleException
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException
   */
  public ObjetTransfert modifierRangee(BaseServiceImpl service,
      ObjetTransfert objetTransfert,
      String conditionSql,
      Object[] parametresConditionSql) throws ModeleException,
      ObjetTransfertNotFoundException {
    Row[] resultat =
        rechercherParConditionSql(conditionSql, parametresConditionSql);

    // Traitement personnalisé pouvant être surchargé.
    traitementAvantPopulationRangee(objetTransfert, resultat[0]);

    service.populerRangee(objetTransfert, resultat[0]);
    service.postChanges();

    ObjetTransfert nouvelObjetTransfert =
        service.populerObjetTransfert(resultat[0], objetTransfert);

    return nouvelObjetTransfert;
  }

  /**
   * Modification d'une rangée de l'objet d'accès aux données avec
   * l'aide d'un objet de transfert. La requête Sql est construite
   * dynamiquement à partir des définitions des attributs clés
   * configurés dans l'accès de données.
   *
   * @param service le service utilisé (appliquer this).
   * @param objetTransfert l'objet de transfert qui contient la modification
   * @throws org.sofiframework.modele.exception.ModeleExceptionException survenu lors de l'accès au modèle
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException Erreur qui survient
   * lorsque l'objet de transfert ne possède pas de rangée dans la base de données
   */
  public ObjetTransfert modifierRangee(BaseServiceImpl service,
      ObjetTransfert objetTransfert) throws ModeleException,
      ObjetTransfertNotFoundException {
    String conditionSql = this.getConditionSqlClePrimaire();
    Object[] cle = this.getClePrimaire(objetTransfert);

    return this.modifierRangee(service, objetTransfert, conditionSql, cle);
  }

  /**
   * Permet d'obtenir une condition SQL qui
   * représente les attributs de la clé primaire.
   *
   * @return Une condition SQL
   */
  public String getConditionSqlClePrimaire() {
    /*
     * On identifie les attributs qio constitue la clé primaire
     */
    AttributeDef[] attributsCle = this.getKeyAttributeDefs();
    StringBuffer conditionSql = new StringBuffer();

    /*
     * On construit une clause sql qui permet de retrouver la bonne
     * rangée de la bd.
     */
    for (int i = 0; i < attributsCle.length; i++) {
      AttributeDef attribut = attributsCle[i];

      conditionSql.append(attribut.getColumnNameForQuery()).append(" = :").append(i +
          1);

      if ((i + 1) < attributsCle.length) {
        conditionSql.append(" AND ");
      }
    }

    return conditionSql.toString();
  }

  /**
   * Permet de récupérer les valeurs d'attributs de la clé primaire
   * d'un objet de transfert à partir des définitions de l'accès de données.
   *
   * @return Clé primaire
   * @param objetTransfert Objewt de transfert dont on désire obtenir la valeur
   * de la clé primaire
   */
  public Object[] getClePrimaire(ObjetTransfert objetTransfert) {
    /*
     * On identifie les attributs qio constitue la clé primaire
     */
    AttributeDef[] attributsCle = this.getKeyAttributeDefs();
    Object[] cle = new Object[attributsCle.length];

    /*
     * On construit une clause sql qui permet de retrouver la bonne
     * rangée de la bd. De plus, on récupère les valeurs de ces
     * attributs dans l'objet de transfert.
     */
    for (int i = 0; i < attributsCle.length; i++) {
      AttributeDef attribut = attributsCle[i];
      cle[i] =
          objetTransfert.getPropriete(UtilitaireString.convertirPremiereLettreEnMinuscule(attribut.getName()));
    }

    return cle;
  }

  /**
   * On valide si la clé est cohérente avec la définition des attributs de l'accès de données.
   * @return Si la clé est cohérente
   * @param cle Clé primaire
   */
  public boolean isCleCoherente(Object[] cle) {
    AttributeDef[] attributsCle = this.getKeyAttributeDefs();
    boolean cleNonVide = (cle != null) && (cle.length > 0);
    boolean definitionCleNonVide =
        (attributsCle != null) && (attributsCle.length > 0);

    // Si le nombre d'éléments de la clé est cohérent on considère la clé valide
    return cleNonVide && definitionCleNonVide &&
        (cle.length == attributsCle.length);
  }

  /**
   * Est-ce que l'objet d'accès aux données est optimisé pour l'itération page
   * par page.
   * <p>
   * Le défaut est true.
   * @return true si l'objet d'accès aux données est optimisé pour l'itération page
   */
  public boolean isOptimiserIterateurPageParPage() {
    if (GestionParametreSysteme.getInstance().getParametreSysteme(ConstantesParametreSysteme.OPTIMISATION_ITERATEUR_PAGE_PAR_PAGE) !=
        null) {
      boolean optimiser =
          GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.OPTIMISATION_ITERATEUR_PAGE_PAR_PAGE).booleanValue();
      setOptimiserIterateurPageParPage(optimiser);
    }

    return optimisationIterateurPageParPage;
  }

  /**
   * Fixer true si l'objet d'accès aux données est optimisé pour l'itération page
   * par page.
   * <p>
   * Le défaut est true.
   * @param optimisationIterateurPageParPage true si l'objet d'accès aux données est optimisé pour l'itération page
   */
  public void setOptimiserIterateurPageParPage(boolean optimisationIterateurPageParPage) {
    this.optimisationIterateurPageParPage = optimisationIterateurPageParPage;
  }

  /**
   * Retourne une valeur avec l'aide de la vue courante,
   * d'un attribut désirée, d'une condition et des paramètres
   * associés et de l'ordre de tri s'il y a lieu.
   * @param nomAttribut. Le nom d'attribut dont on désire extraire la valeur. (Obligatoire)
   * @param whereClause la condition sql. (Obligatoire)
   * @param whereClauseParams les paramètres dynamique de la condition sql (Facultatif)
   * @param orderByClause l'ordre de la liste (Facultatif).
   * @return La valeur désirée
   * @throws org.sofiframework.modele.exception.ModeleException une exception du modèle
   */
  public Object getValeurDansRangee(String nomAttribut, String whereClause,
      Object[] whereClauseParams,
      String orderByClause) throws ModeleException,
      ObjetTransfertNotFoundException {
    initialiser();
    setWhereClause(whereClause);
    setWhereClauseParams(whereClauseParams);

    if (orderByClause != null) {
      setOrderByClause(orderByClause);
    }

    if (hasNext()) {
      try {
        Row rangee = next();

        return UtilitaireObjet.getValeurAttribut(rangee, nomAttribut);
      } catch (Exception e) {
        throw new ModeleException(e);
      }
    } else {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }
  }

  /**
   * Permet de modifier une valeur d'un attribut dans un rangée.
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException la rangée pour la condition SQL n'a pas été trouvé.
   * @throws org.sofiframework.modele.exception.ModeleException une exception du modèle
   * @return
   * @param orderByClause l'ordre de la liste (Facultatif).
   * @param whereClauseParams les paramètres dynamique de la condition sql (Facultatif)
   * @param whereClause la condition sql. (Obligatoire)
   * @param valeur la valeur a fixer. (Obligatoire)
   * @param nomAttribut Le nom d'attribut dont on désire extraire la valeur. (Obligatoire)
   * @param service le service courant (Obligatoire).
   */
  public void modifierValeurDansListeRangees(BaseServiceImpl service,
      String nomAttribut, Object valeur,
      String whereClause,
      Object[] whereClauseParams,
      String orderByClause) throws ModeleException,
      ObjetTransfertNotFoundException {
    initialiser();
    setWhereClause(whereClause);
    setWhereClauseParams(whereClauseParams);

    if (orderByClause != null) {
      setOrderByClause(orderByClause);
    }

    while (hasNext()) {
      try {
        Row rangee = next();
        Class type = UtilitaireObjet.getClasse(rangee, nomAttribut);
        Object valeurConvertie = Convertisseur.convertirEnRangee(type, valeur);
        UtilitaireObjet.setPropriete(rangee, nomAttribut, valeurConvertie);

        service.postChanges();
      } catch (Exception e) {
        throw new ModeleException(e.getMessage(), e.getCause());
      }
    }
  }

  /**
   * Permet de modifier une valeur d'un attribut dans un rangée.
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException la rangée pour la condition SQL n'a pas été trouvé.
   * @throws org.sofiframework.modele.exception.ModeleException une exception du modèle
   * @return
   * @param orderByClause l'ordre de la liste (Facultatif).
   * @param whereClauseParams les paramètres dynamique de la condition sql (Facultatif)
   * @param whereClause la condition sql. (Obligatoire)
   * @param valeur la valeur a fixer. (Obligatoire)
   * @param nomAttribut Le nom d'attribut dont on désire extraire la valeur. (Obligatoire)
   * @param service le service courant (Obligatoire).
   */
  public void modifierValeurDansRangee(BaseServiceImpl service,
      String nomAttribut, Object valeur,
      String whereClause,
      Object[] whereClauseParams,
      String orderByClause) throws ModeleException,
      ObjetTransfertNotFoundException {
    initialiser();
    setWhereClause(whereClause);
    setWhereClauseParams(whereClauseParams);

    if (orderByClause != null) {
      setOrderByClause(orderByClause);
    }

    if (hasNext()) {
      try {
        Row rangee = next();
        Class type = UtilitaireObjet.getClasse(rangee, nomAttribut);
        Object valeurConvertie = Convertisseur.convertirEnRangee(type, valeur);
        UtilitaireObjet.setPropriete(rangee, nomAttribut, valeurConvertie);

        service.postChanges();
      } catch (Exception e) {
        throw new ModeleException(e);
      }
    } else {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }
  }

  /**
   * Permet d'ajouter dans un log la requêtre SQL au complet.
   * @param vue la vue dont on désire la requête SQL.
   */
  public String debugSQL() {
    String query = "";

    if (log.isDebugEnabled()) {
      Object[] params = getWhereClauseParams();

      query = getQuery();

      int bindingStyle = getBindingStyle();

      if (SQLBuilder.BINDING_STYLE_ORACLE == bindingStyle) {
        for (int i = 0; i < params.length; i++) {
          query =
              query.replaceAll(":" + i, (params[i] != null) ? params[i].toString() :
                  "is null");
        }
      } else if (SQLBuilder.BINDING_STYLE_JDBC == bindingStyle) {
        for (int i = 0; i < params.length; i++) {
          query = query.replaceFirst("\\?", params[i].toString());
        }
      }

      log.debug(query);
    }

    return query;
  }

  protected String debugSQL(Key key) {
    String findByKey = "";

    if (log.isDebugEnabled()) {
      StringBuffer keys = new StringBuffer();
      int count = key.getKeyValues().length;

      for (int i = 0; i < count; i++) {
        keys.append(key.getKeyValues()[i]).append(", ");
      }

      if (keys.length() > 0) {
        keys.deleteCharAt(keys.length() - 1);
        keys.deleteCharAt(keys.length() - 1);
      }

      findByKey = " - Find By Key (" + keys.toString() + ") - " + debugSQL();
      log.debug(findByKey);
    }

    return findByKey;
  }

  /**
   * Fixe la variable permettant de choisir le type d'estimation des rangées retournées.
   * @param estimationNombreRangeeAutomatique true si l'estimation doit être fait par sofi,
   * false si le développeur prend en charge l'estimation.
   */
  public void setEstimationNombreRangeeAutomatique(boolean estimationNombreRangeeAutomatique) {
    this.estimationNombreRangeeAutomatique = estimationNombreRangeeAutomatique;
  }

  /**
   * Retoune le choix du type d'estimation des rangées retournées.
   * @return le choix du type d'estimation des rangées retournées.
   */
  public boolean isEstimationNombreRangeeAutomatique() {
    return estimationNombreRangeeAutomatique;
  }

  /**
   * Méthode qui doit être surchargée qui permet de fixer un type
   * de classe d'objet de transfert spécifique selon un traitement particulier
   * dans le service.
   * @return type de classe spécifique à utiliser
   * @param rangee la rangée en traitement.
   * @since SOFI 2.0.1
   */
  protected Class fixerClasseObjetTransertSpecifique(Row rangee) {
    return null;
  }

  /**
   * Méthode qui doit être surchargée qui permet d'application du traitement sur
   * une rangée avant insertion des données.
   * @param rangee la rangée en traitement.
   * @param objetTransfert l'objet de transfert en traitement.
   * @since SOFI 2.0.2
   */
  protected void traitementAvantPopulationRangee(ObjetTransfert objetTransfert,
      Row rangee) {
  }

  /**
   * Méthode qui doit être surchargée qui permet d'application du traitement sur
   * un objet de transfert avec l'aide d'un rangée après sa population.
   * @param objetTransfert l'objet de transfert en traitement.
   * @param rangee la rangée en traitement.
   * @param service le service en traitement.
   * @since SOFI 2.0.3
   * @deprecated Utiliser plutot la méthode traitementApresPopulationObjetTransfert(rangee, objetTransfert);
   */
  @Deprecated
  protected void traitementApresPopulationObjetTransfert(BaseServiceImpl service,
      Row rangee,
      ObjetTransfert objetTransfert) {
  }

  /**
   * Méthode qui doit être surchargée qui permet d'application du traitement sur
   * un objet de transfert avec l'aide d'un rangée après sa population.
   * @param objetTransfert l'objet de transfert en traitement.
   * @param rangee la rangée en traitement.
   * @since SOFI 2.0.4
   */
  protected void traitementApresPopulationObjetTransfert(Row rangee,
      ObjetTransfert objetTransfert) {
    traitementApresPopulationObjetTransfert(getService(), rangee,
        objetTransfert);
  }

  /**
   * Méthode qui doit être surchargée qui permet d'application du traitement sur
   * une rangée avant insertion d'une nouvelle rangée dans la base de données.
   * @param objetTransfert l'objet de transfert en traitement.
   * @since SOFI 2.0.4
   */
  protected void traitementAvantInsertionRangee(ObjetTransfert objetTransfert) {
  }

  /**
   * Méthode qui doit être surchargée qui permet d'application du traitement sur
   * une rangée avant la suppression d'une rangée dans la base de données.
   * @param rangee rangee la rangée en traitement.
   * @since SOFI 2.0.4
   */
  protected void traitementAvantSuppressionRangee(Row rangee) {
  }

  /**
   * Valide s'il existe une rangée pour une condition spécifique.
   * @return true s'il existe une rangée pour une condition spécifique.
   * @param parametresConditionSql les paramètres associés à la condition.
   * @param conditionSql la condition SQL.
   * @since SOFI 2.0.2
   */
  public boolean isExiste(String conditionSql,
      Object[] parametresConditionSql) {
    initialiser();

    // Fixer condition.
    setWhereClause(conditionSql);

    // Fixer le ou les valeurs de la condition.
    setWhereClauseParams(parametresConditionSql);

    if (hasNext()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne le service associé à l'objet d'accès aux données en cours.
   * @return le service associé à l'objet d'accès aux données en cours.
   * @since SOFI 2.0.4
   */
  public BaseServiceImpl getService() {
    return (BaseServiceImpl)this.getApplicationModule();
  }

  /**
   * Permet de fixer l'optimisation page par page.
   * <p>
   * Défaut : true.
   * @param optimisationIterateurPageParPage true si vous désirez activer l'optimisation page
   * par page.
   */
  public void setOptimisationIterateurPageParPage(boolean optimisationIterateurPageParPage) {
    this.optimisationIterateurPageParPage = optimisationIterateurPageParPage;
  }

  /**
   * Est-ce que l'optimisation page par page est actif?
   * <p>
   * Défaut : true.
   * @return true si l'optimisation page par page est actif.
   */
  public boolean isOptimisationIterateurPageParPage() {
    return optimisationIterateurPageParPage;
  }

  /**
   * Fixer le filtre
   * @param filtre le filtre en traitement.
   * @since SOFI 2.0.3
   */
  public void setFiltre(Filtre filtre) {
    this.filtre = filtre;
  }

  /**
   * Retourne le filtre en traitement.
   * @return le filtre en traitement.
   * @since SOFI 2.0.3
   */
  public Filtre getFiltre() {
    return filtre;
  }

  /**
   * Fixer true si vous désirez ajouter une condition Sql directement
   * dans celui spécifié dans le composant de l'accès au données.
   * <p>
   * Ainsi il est permit d'ajouter des conditions Sql sur des attributs
   * disponible depuis les tables spécifiés en FROM et non pas seulement les attributs
   * du Select. Facilite ainsi l'utilisation des SELECT DISTINCT.
   * @param ajouterConditionPourSqlOriginal true si vous désirez ajouter une condition Sql directement
   * dans celui spécifié dans le composant de l'accès au données.
   * @since SOFI 2.0.4
   */
  public void setAjouterConditionPourSqlOriginal(boolean ajouterConditionPourSqlOriginal) {
    this.ajouterConditionPourSqlOriginal = ajouterConditionPourSqlOriginal;
  }

  /**
   * Retourne true si vous désirez ajouter une condition Sql directement
   * dans celui spécifié dans le composant de l'accès au données.
   * <p>
   * Ainsi il est permit d'ajouter des conditions Sql sur des attributs
   * disponible depuis les tables spécifiés en FROM et non pas seulement les attributs
   * du Select. Facilite ainsi l'utilisation des SELECT DISTINCT.
   * @return true si vous désirez ajouter une condition Sql directement
   * dans celui spécifié dans le composant de l'accès au données.
   * @since SOFI 2.0.4
   */
  public boolean isAjouterConditionPourSqlOriginal() {
    return ajouterConditionPourSqlOriginal;
  }

  /**
   * Méthode spécialisé de ViewObjetImpl afin de permettre d'ajouter des
   * conditions SQL à condition orignal de l'AD.
   * @param stringbuffer la requête SQL en traitement.
   * @param i
   * @return
   */
  @Override
  protected boolean buildWhereClause(StringBuffer stringbuffer, int i) {
    String requeteSqlOriginal = stringbuffer.toString();
    // La condition SQL spécifié dans l'AD.
    String conditionSql = getWhereClause();
    if (isAjouterConditionPourSqlOriginal()) {
      // Metre la condition a vide car déjà traité.
      setWhereClause("");

      if (!conditionSql.trim().startsWith("AND")) {
        conditionSql = " AND " + conditionSql;
      }

      boolean conditionAjoute = false;

      // Traiter l'exception GROUP BY
      int positionGroupBy =
          requeteSqlOriginal.toUpperCase().indexOf("GROUP BY");
      if (positionGroupBy != -1) {
        stringbuffer.replace(positionGroupBy, positionGroupBy, conditionSql);
        conditionAjoute = true;
      }
      // Traiter l'exception UNION
      int positionUnion = requeteSqlOriginal.toUpperCase().indexOf("UNION");
      if (positionUnion != -1) {

        stringbuffer.replace(positionUnion, positionUnion, conditionSql);
        conditionAjoute = true;
      }

      if (!conditionAjoute) {
        stringbuffer.replace(requeteSqlOriginal.indexOf(") QRSLT"),
            requeteSqlOriginal.indexOf(") QRSLT"),
            conditionSql);
      }

    }

    return super.buildWhereClause(stringbuffer, i);

  }


  /**
   * Fixer true si vous désirez populer les objets imbriqués demandés
   * par des traitements spécifiques de l'objet d'accès aux données.
   * <p>
   * Utiliser comme indicateur pour faciliter l'implémentation des
   * traitement personnalisé des objets d'accès aux données lors de la population
   * manuel d'objets imbriqués.
   * @param populerObjetImbrique true si si vous désirez populer les objets imbriqués demandés
   * par des traitements spécifiques de l'objet d'accès aux données.
   * @since SOFI 2.0.4
   */
  public void setPopulerObjetImbrique(boolean populerObjetImbrique) {
    this.populerObjetImbrique = populerObjetImbrique;
  }

  /**
   * Retourne true si vous désirez populer les objets imbriqués demandés
   * par des traitements spécifiques de l'objet d'accès aux données.
   * <p>
   * Utiliser comme indicateur pour faciliter l'implémentation des
   * traitement personnalisé des objets d'accès aux données lors de la population
   * manuel d'objets imbriqués.
   * @return true si si vous désirez populer les objets imbriqués demandés
   * par des traitements spécifiques de l'objet d'accès aux données.
   * @since SOFI 2.0.4
   */
  public boolean isPopulerObjetImbrique() {
    return populerObjetImbrique;
  }

  /**
   * Surchage afin de permettre de fixer des valeurs par défaut sur les
   * variables bind de l'accès aux données.
   * <p>
   * Solution proposé par Oracle (Steve Muench).
   * @param qc
   * @param params
   * @param numUserParams
   */
  @Override
  protected void executeQueryForCollection(Object qc, Object[] params,
      int numUserParams) {

    if (numUserParams == 0 && getValeursVariableBindParDefaut() != null) {
      numUserParams = getValeursVariableBindParDefaut().length;
      int numFwkSuppliedBindVals = (params != null) ? params.length : 0;
      if (numFwkSuppliedBindVals > 0) {
        /*
         * Allocate a new Object[] with enough room for both the
         * user bind values and the framework supplied bind values
         */
        Object[] newBinds = new Object[numFwkSuppliedBindVals + numUserParams];
        /*
         * Copy the framework-supplied bind variables into a new Object[]
         * leaving 'numUserParams' slots at the beginning of the array to
         * add in the user supplied bind values *before* the framework
         * supplied bind values.
         */
        System.arraycopy(params, 0, newBinds, numUserParams,
            numFwkSuppliedBindVals);
        /*
         * Copy the user-supplied bind variables to the front of the newBinds
         * Object array.
         */
        System.arraycopy(getValeursVariableBindParDefaut(), 0, newBinds, 0,
            numUserParams);
        params = newBinds;
      } else {
        params = getValeursVariableBindParDefaut();
      }
      /*
       * It's too late in the execute cycle to use *only* the
       * setWhereClauseParams() to effect the current view object's
       * query execution, but we set them using this API anyway so
       * that they will be remembered on the rowset as the current
       * values for the bind variables. This will insure, for example,
       * that a subsequent call to getEstimatedRowCount() or another API
       * will have the right default bind variables set.
       */
      setWhereClauseParams(getValeursVariableBindParDefaut());
    }

    if (log.isDebugEnabled()) {
      debugSQL();
    }

    super.executeQueryForCollection(qc, params, numUserParams);
  }

  /**
   * Overridden framework method that gets invoked when the
   * getEstimatedRowCount() needs to fetch the estimated count
   * from the database.
   * <p>
   * Solution proposé par Oracle (Steve Muench).
   * @return estimate of number of rows to be returned by the query
   * @param viewRowSet view row set whose row count is being estimated
   */
  @Override
  public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
    Object[] whereClauseParams = getWhereClauseParams();
    if ((whereClauseParams == null) || (whereClauseParams.length == 0)) {
      setWhereClauseParams(getValeursVariableBindParDefaut());
    }
    return super.getQueryHitCount(viewRowSet);
  }

  /**
   * Fixer les valeurs par défaut pour les variables bind de l'accès aux données.
   * @param valeursVariableBindParDefaut des  les valeurs par défaut pour les variables bind de l'accès aux données.
   * @since 2.1
   */
  public void setValeursVariableBindParDefaut(Object[] valeursVariableBindParDefaut) {
    getService().setAttributTemporaire(getClass().getName().toString() +
        "_valeurVariableBindDefaut",
        valeursVariableBindParDefaut);

  }

  /**
   * Retourne les valeurs par défaut pour les variables bind de l'accès aux données.
   * @return les valeurs par défaut pour les variables bind de l'accès aux données.
   * @since 2.1
   */
  public Object[] getValeursVariableBindParDefaut() {
    return (Object[])getService().getAttributTemporaire(getClass().getName().toString() +
        "_valeurVariableBindDefaut");

  }
}
