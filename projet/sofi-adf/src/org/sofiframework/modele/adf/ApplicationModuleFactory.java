/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;

/**
 * Factory pour la construction d'un module d'application. Une référence
 * à l'instance est concervée pour être relachée (méthode release).
 * 
 * Exemple d'utilisation avec Spring :
 * 
 *  <bean id="modeleFactory" class="org.sofiframework.modele.adf.ApplicationModuleFactory">
 *    <property name="adfConfiguration" value="ModeleTestLocal" />
 *    <property name="adfPackage" value="org.sofiframework.modele.adf.test.ModeleTest" />
 *  </bean>
 * 
 * @author Jean-Maxime Pelletier
 */
public class ApplicationModuleFactory implements org.springframework.beans.factory.DisposableBean {

  private static final Log log = LogFactory.getLog(ApplicationModuleFactory.class);

  private ThreadLocal amCourant = new ThreadLocal() {};

  private ApplicationModule amStatic = null;

  private String adfPackage = null;
  private String adfConfiguration = null;

  /**
   * Obtenir une instance d'un application module correspondant
   * un paquetage et à la configuration spécifiée.
   * @return Application module ADF
   */
  public ApplicationModule get() {
    ApplicationModule am = getApplicationModuleCourant();
    if (am == null)  {
      am = creer();
      this.amCourant.set(am);
    }
    return am;
  }

  public ApplicationModule getStatic() {
    if (this.amStatic == null) {
      this.amStatic = creer();
    }

    return this.amStatic;
  }

  @Override
  public void destroy() throws Exception {
    if (this.amStatic != null) {
      this.release(this.amStatic);
    }
  }

  public ApplicationModule creer() {
    try {
      return Configuration.createRootApplicationModule(adfPackage, adfConfiguration);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors du checkout d'un module d'application.", e);
      }
      throw new SOFIException(e);
    }
  }

  public void releaseApplicationModuleCourant() {
    ApplicationModule am = this.getApplicationModuleCourant();
    if (am != null) {
      this.amCourant.set(null);
      this.release(am);
    }
  }

  public ApplicationModule getApplicationModuleCourant() {
    return (ApplicationModule) this.amCourant.get();
  }

  /**
   * Relâcher l'instance de l'application module.
   * @param applicationModule Instance de l'application module
   */
  public void release(ApplicationModule applicationModule) {
    Configuration.releaseRootApplicationModule(applicationModule, true);
  }

  /**
   * Obtenir le paquetage du module d'application.
   * <p>
   *  Exemple : org.sofiframework.modele.adf.test.ModeleTest
   * </p>
   * 
   * @return paquetage ADF
   */
  public String getAdfPackage() {
    return adfPackage;
  }

  /**
   * Affecter le paquetage du module d'application.
   * <p>
   *  Exemple : org.sofiframework.modele.adf.test.ModeleTest
   * </p>
   * 
   * @param paquetage ADF
   */
  public void setAdfPackage(String adfPackage) {
    this.adfPackage = adfPackage;
  }

  /**
   * Obtenir le nom de la configuration ADF
   * <p>
   *  Exemple : ModeleTestLocal
   * </p>
   * 
   * @return configuration ADF
   */
  public String getAdfConfiguration() {
    return adfConfiguration;
  }

  /**
   * Affecter le nom de la configuration ADF
   * <p>
   *  Exemple : ModeleTestLocal
   * </p>
   * 
   * @param configuration ADF
   */
  public void setAdfConfiguration(String adfConfiguration) {
    this.adfConfiguration = adfConfiguration;
  }
}
