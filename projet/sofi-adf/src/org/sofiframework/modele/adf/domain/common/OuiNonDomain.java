/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.domain.common;

import java.io.Serializable;

import oracle.jbo.Transaction;
import oracle.jbo.domain.DomainInterface;
import oracle.jbo.domain.DomainOwnerInterface;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Domaine permettant de spécifier les Oui et les Non dans la base de de données
 * et qui se convertisse en true/false dans les classes java.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class OuiNonDomain implements DomainInterface, Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 3720391127827202349L;
  private Object donnee;

  /**
   * Constructeur interne par défaut.
   */
  protected OuiNonDomain() {
    donnee = null;
  }

  /**
   * Constructeur depuis la valeur en caractère.
   * @param val la valeur en caractère.
   */
  public OuiNonDomain(String val) {
    if ((val != null) &&
        (GestionParametreSysteme.getInstance().getString("oui").equals(val) ||
            "O".equals(val))) {
      donnee = Boolean.TRUE;
    } else {
      donnee = Boolean.FALSE;
    }

    validate();
  }

  /**
   * Constructeur depuis la valeur en Boolean.
   * @param val la valeur en Boolean.
   */
  public OuiNonDomain(Boolean val) {
    donnee = val;
  }

  /**
   * Constructeur depuis la valeur en boolean.
   * @param val la valeur en boolean.
   */
  public OuiNonDomain(boolean val) {
    donnee = new Boolean(val);
  }

  /**
   * Retourne la valeur du domaine
   * @return  la valeur du domaine
   */
  @Override
  public Object getData() {
    return toString();
  }

  /**
   * Retourne la valeur en Boolean
   * @return true/false
   */
  public Object getBoolean() {
    return donnee;
  }

  /**
   *
   *  <b>Internal:</b> <em>Applications should not use this method.</em>
   */
  @Override
  public void setContext(DomainOwnerInterface owner, Transaction trans,
      Object obj) {
  }

  /**
   *
   *  Implements domain validation logic and throws a JboException on error.
   */
  protected void validate() {
    //   ### Implement custom domain validation logic here. ###
  }

  @Override
  public String toString() {
    String resultatAMettreDansBD = null;
    if (donnee != null) {
      if (((Boolean) donnee).booleanValue()) {
        resultatAMettreDansBD = GestionParametreSysteme.getInstance().getString("oui");
        if (UtilitaireString.isVide(resultatAMettreDansBD))  {
          resultatAMettreDansBD = "O";
        }
        return resultatAMettreDansBD;
      } else {
        resultatAMettreDansBD = GestionParametreSysteme.getInstance().getString("non");
        if (UtilitaireString.isVide(resultatAMettreDansBD))  {
          resultatAMettreDansBD = "N";
        }
        return resultatAMettreDansBD;
      }
    }

    resultatAMettreDansBD = GestionParametreSysteme.getInstance().getString("non");
    if (UtilitaireString.isVide(resultatAMettreDansBD))  {
      resultatAMettreDansBD = "N";
    }
    return resultatAMettreDansBD;
  }

  @Override
  public boolean equals(Object obj) {
    boolean identiques = false;

    if (obj != null) {
      if (obj instanceof DomainInterface) {
        identiques = this.getData().equals(((DomainInterface) obj).getData());
      }
    } else {
      identiques = (this.getData() != null);
    }

    return identiques;
  }
}
