/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.domain;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import oracle.jbo.Transaction;
import oracle.jbo.domain.DataCreationException;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.DatumFactory;
import oracle.jbo.domain.DomainInterface;
import oracle.jbo.domain.DomainOwnerInterface;
import oracle.jbo.domain.KeyAttributeInterface;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.domain.TypeConvMapEntry;
import oracle.jbo.domain.TypeFactory;
import oracle.jbo.domain.XMLDomainFactory;
import oracle.jbo.domain.XMLDomainReaderFactory;
import oracle.jbo.domain.XMLDomainWriter;
import oracle.sql.CustomDatum;
import oracle.sql.CustomDatumFactory;
import oracle.sql.Datum;
import oracle.sql.TIMESTAMP;

public class TimestampLTZ extends oracle.sql.TIMESTAMP
implements DomainInterface,
XMLDomainWriter,
KeyAttributeInterface,
CustomDatum,
Serializable {

  private static final long serialVersionUID = 5600076615126868258L;

  static CustomDatumFactory fac = null;
  static
  {

    TypeFactory.addCustomConverter(org.sofiframework.modele.adf.domain.TimestampLTZ.class, java.lang.String.class,
        new TypeConvMapEntry()
    {
      @Override
      protected Object convert(Class toClass, Class valClass, Object val)
      {
        try
        {
          return new TimestampLTZ((String)val);
        }
        catch (Exception e)
        {
          //check for null/ String and 0 length.
          if (val instanceof String && ((String)val).trim().length() == 0)
          {
            return null;
          }
          throw new DataCreationException(toClass.getName(), val, e);
        }
      }
    }
        );

  }
  private int mHashCode = 0;

  /**
   * <b>Internal:</b> <em>Applications should not invoke this method.</em>
   * <p>Initializes the <code>Timestamp</code> Domain.
   *
   * <p>This method is invoked when JBO is initialized.
   * Applications should not call this method directly.
   *
   * @return the <code>CustomDatumFactory</code> for the
   * <code>Timestamp</code> Domain.
   */
  public static CustomDatumFactory getCustomDatumFactory()
  {
    if( fac == null )
    {
      class facClass implements DatumFactory
      {
        @Override
        public Datum createDatum(java.sql.CallableStatement jdbc, int index)
            throws SQLException
            {
          byte[] data = jdbc.getBytes(index);
          return (data != null) ? new TimestampLTZ(data) : null;
            }

        @Override
        public Datum createDatum(java.sql.ResultSet jdbc, int index)
            throws SQLException
            {
          byte[] data = jdbc.getBytes(index);
          return (data != null) ? new TimestampLTZ(data) : null;
            }

        @Override
        public CustomDatum create(Datum d, int sql_type_code) throws SQLException
        {
          if (d != null)
          {
            return new TimestampLTZ( d.getBytes());
          }
          return null;
        }
      };
      fac = new facClass();
    }
    return fac;
  }

  /**
   * <b>Internal:</b> <em>Applications should not invoke this method.</em>
   * <p>Converts this <code>Timestamp</code> Domain object back into an
   * SQL <code>TIMESTAMP</code> object.
   *
   * @param <code>conn</code> Not used.
   * @return A <code>Datum</code> containing <code>TIMESTAMP</code> object.
   * @throws <code>SQLException</code> Never.
   */
  @Override
  public Datum toDatum(oracle.jdbc.driver.OracleConnection conn) throws SQLException
  {
    return new TIMESTAMP(getBytes());
  }

  /**
   * Creates a default <code>Timestamp</code> Domain object.
   *
   * <p>This constructor does not create a null date:
   * use one of the <code>NullValue()</code> constructors.
   */
  public TimestampLTZ() {
    super();
  }

  /**
   * <b>Internal:</b> <em>Applications should not invoke this method.</em>
   * <p>Creates a <code>Timestamp</code> Domain object from the given byte array.
   *
   * @param value a value returned by a previous call to
   * <code>getBytes()</code> on an SQL object compatable with
   * <code>Timestamp</code>.
   */
  public TimestampLTZ(byte[] value) {
    super(value);
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from an Oracle SQL
   * <code>TIMESTAMP</code>.
   *
   * @param value a <code>TIMESTAMP</code> SQL object.
   */
  public TimestampLTZ(TIMESTAMP value) {
    super(value.getBytes());
  }

  /**
   * Creates a <code>Timestamp</code> identical to an
   * existing <code>Timestamp</code>.
   *
   * @param value a <code>Timestamp</code> Domain object.
   */
  public TimestampLTZ(Timestamp value) {
    super(value.getBytes());
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from a JDBC
   * <code>Timestamp</code>.
   *
   * @param value a <code>TIMESTAMP</code> SQL object.
   */
  public TimestampLTZ(java.sql.Timestamp value) {
    super(value);
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from a long
   * <code>Timestamp</code> value. Uses java.sql.Timestamp(long)
   * to create this domain instance. This value should usually
   * be the value returned from System.currentTimeMillis()
   * or from Timestamp.getTime() method
   */
  public TimestampLTZ(long value) {
    super(new java.sql.Timestamp(value));
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from a JDBC
   * <code>java.sql.Date</code> object.
   *
   * @param value a <code>Time</code> SQL object.
   */
  public TimestampLTZ(java.sql.Date value) {
    super(value);
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from a JDBC
   * <code>java.util.Date</code> object.
   *
   * @param value a <code>TimeStamp</code> SQL object.
   */
  public TimestampLTZ(java.util.Date value) {
    super(new java.sql.Timestamp(value.getTime()));
  }

  /**
   * Creates a <code>Timestamp</code> Domain object from a
   * Java <code>String</code>.
   *
   * @param   value   a textual representation of a <code>Timestamp</code>.
   */
  public TimestampLTZ(String value) {
    setBytes(toTimestamp(value).toBytes());

  }

  /**
   * Returns the time as a long value using timestampValue().getTime().
   **/
  public long getTime()
  {
    try
    {
      return timestampValue().getTime();
    }
    catch (java.sql.SQLException sqle)
    {
      throw new oracle.jbo.JboException(sqle);
    }
  }

  /**
   * Return a java.sql.Timestamp object with this domain's value.
   * This method may be used to access the value for this domain in EL-expressions.
   * 
   * @javabean.property
   */
  public java.sql.Timestamp getValue()
  {
    return (java.sql.Timestamp)getData();
  }

  /**
   * <b>Internal:</b> <em>Applications should not invoke this method.</em>
   *
   * @value The JDBC representation of <code>this</code>,
   */
  @Override
  public Object getData() {
    try
    {
      return timestampValue();
    }
    catch (SQLException e)
    {
      throw new oracle.jbo.JboException(e);
    }
  }

  /**
   * Convert to a Date representation of the Timestamp object
   * 
   * @return java.sql.Date representation of the Timestamp object
   * @exception SQLException, if no Date representation exists
   */
  @Override
  public java.sql.Date dateValue() throws SQLException
  {
    return new java.sql.Date(timestampValue().getTime());
  }

  /**
   * <b>Internal:</b> <em>Applications should not invoke this method.</em>
   */
  @Override
  public void setContext(DomainOwnerInterface owner, Transaction trans, Object ctx) {
  }

  /**
   * Converts an Oracle Timestamp expressed as a string to a Java Timestamp. The Java
   * date can be either an <tt>java.sql.Timestamp</tt> or an <tt>java.sql.Timestamp</tt>.
   * @return returns a <tt>java.sql.Timestamp</tt> object.
   */
  public static Timestamp toTimestamp(String value)
  {
    TIMESTAMP dt;
    value = value.trim();
    try
    {
      //this would try timestamp format.
      dt = new TIMESTAMP(value);
    }
    catch(Exception e)
    {
      java.sql.Timestamp javaObj = java.sql.Timestamp.valueOf(value);
      dt = new TIMESTAMP(javaObj);
    }

    return new Timestamp(dt);
  }

  /**
   * For testing purposes only: converts <code>this</code> to a textual
   * representation.
   */
  @Override
  public String toString() {
    Object ts = getData();
    if( ts != null  )
    {
      return ts.toString();
    }
    return null;
  }

  /**
   * Tests <code>this</code> for equality with another object.
   *
   * The argument is converted to a <code>Timestamp</code> object, if necessary.
   *
   * @param other  an arbitrary <code>Object</code>.
   * @return <code>true</code> if conversion was successful and the converted
   * argument is identical to <code>this</code>.
   */
  @Override
  public boolean equals(Object other) {
    if (other == null)
    {
      return false;
    }

    if (!other.getClass().equals(getClass()))
    {
      Timestamp charOther;
      try
      {
        if (other instanceof String)
        {
          charOther = new Timestamp((String)other);
        }
        else if (other instanceof java.sql.Timestamp)
        {
          charOther = new Timestamp((java.sql.Timestamp)other);
        }
        else if (other instanceof java.util.Date)
        {
          charOther = new Timestamp((java.util.Date)other);
        }
        else if (other instanceof Date)
        {
          charOther = new Timestamp((Date)other);
        }
        else
        {
          return false;
        }
      }
      catch( Exception sqle )
      {
        //for any exception simply return false from equals.
            return false;
      }
      return super.equals(charOther);
    }
    return super.equals(other);
  }


  /**
   * Computes a hash code for <code>this</code>.
   *
   * @return the hash code of <code>this</code>.
   */
  @Override
  public int hashCode()
  {
    if (mHashCode == 0)
    {
      mHashCode = getData().hashCode();
      if (mHashCode == 0)
      {
        mHashCode = -6234;
      }
    }
    return mHashCode;
  }

  private void writeObject(java.io.ObjectOutputStream out)
      throws IOException
      {
    byte[] bytes = getBytes();
    out.writeInt(bytes.length);
    out.write(bytes);
      }

  private void readObject(java.io.ObjectInputStream in)
      throws IOException, ClassNotFoundException
      {
    int size = in.readInt();
    byte[] bytes = new byte[size];
    in.read(bytes);
    setBytes(bytes);
      }


  /**
   * Prints the DTD info for this domain in the given print writer.
   * Returns the DTD string to be added to this domain's container
   * entity/domain.
   * <p> The <tt>allDefs</tt> hashtable contains predefined XML definitions and
   * is passed by whatever calls this method.
   * <p>
   * @param allDefs a hashtable of predefined XML definitions passed from whatever
   * calls this method.
   * @param pw print writer into which the defnition is being printed.
   * @param bContainees if <tt>true</tt>, prints definitions of contained objects.
   **/
  @Override
  public String printXMLDefinition(java.util.Hashtable allDefs, java.io.PrintWriter pw,
      boolean bContainees)
  {
    return "#PCDATA";
  }

  /**
   * Creates the xml node in the given xml document for this domain's data.
   * @param xmlDoc name of the XML document in which the node should be created.
   **/
  @Override
  public org.w3c.dom.Node getXMLContentNode(org.w3c.dom.Document xmlDoc)
  {
    return xmlDoc.createTextNode(toString());
  }

  /**
   * Creates the xml node in the given xml document for this domain's data in hex format of the byte[]
   * representation of the data.
   * @param xmlDoc name of the XML document in which the node should be created.
   **/
  @Override
  public org.w3c.dom.Node getSerializedDomainXML(org.w3c.dom.Document xmlDoc)
  {
    //for now render as String. If we find that this leads to NLS issues, we should
    //revert back to the RepConversion solution.
    return xmlDoc.createTextNode(toString());
    //return xmlDoc.createTextNode(oracle.jbo.common.RepConversion.bArray2String(getBytes()));
  }

  /**
   * <b>Internal:</b> <em>Applications should not use this method.</em>
   */
  public static XMLDomainReaderFactory getXMLDomainFactory(Class attrClass)
  {
    class facClass implements XMLDomainReaderFactory, XMLDomainFactory
    {
      @Override
      public DomainInterface createDomainFromXMLElement(org.w3c.dom.Element attrElem)
      {
        try
        {
          org.w3c.dom.Node valNode = attrElem.getFirstChild();
          if( valNode != null)
          {
            String attrValue = valNode.getNodeValue();
            if (attrValue != null)
            {
              //get the properly typed-attribute value
              return new TimestampLTZ(attrValue);
            }
          }
        }
        catch (oracle.jbo.JboException e)
        {
          throw e;
        }
        catch (Exception e)
        {
          //throw new jbo exception here.
          throw new oracle.jbo.JboException(e);
        }
        return null;
      }

      @Override
      public Object createDomainFromSerializedXML(org.w3c.dom.Element attrElem)
      {
        try
        {
          org.w3c.dom.Node valNode = attrElem.getFirstChild();
          if( valNode != null)
          {
            String attrValue = valNode.getNodeValue();
            if (attrValue != null)
            {
              //for now render as String. If we find that this leads to NLS issues, we should
              //revert back to the RepConversion solution.
              //return new Timestamp(oracle.jbo.common.RepConversion.convertHexStringToByte(attrValue));
              return new TimestampLTZ(attrValue);
            }
          }
        }
        catch (oracle.jbo.JboException e)
        {
          throw e;
        }
        catch (Exception e)
        {
          //throw new jbo exception here.
          throw new oracle.jbo.JboException(e);
        }
        return null;
      }
    }

    return new facClass();
  }
}