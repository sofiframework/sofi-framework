/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.convertisseur;

import java.util.Date;

import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.DBSequence;

import org.sofiframework.modele.adf.domain.TimestampLTZ;
import org.sofiframework.modele.adf.domain.common.BooleanDomain;
import org.sofiframework.modele.adf.domain.common.OuiNonDomain;
import org.sofiframework.objetstransfert.DateTZ;

/**
 * Convertie la valeur d'une propriété d'un objet de transfert
 * en valeur du type de la rangée.
 */
public class Convertisseur {

  /**
   * Constructeur
   */
  private Convertisseur() {
  }

  /**
   * Convertie une valeur qui provient d'un objet de transfert vers un
   * objet de classe correspondant a une propriété d'une entité/accès de
   * données Oracle,
   * 
   * @return Classe de
   * 
   * @param valeurAttribut
   * @param classeDestination
   */
  public static Object convertirEnRangee(Class classeDestination, Object valeurAttribut) {

    Object valeurConvertie = null;

    if (valeurAttribut != null) {
      if (BlobDomain.class.isAssignableFrom(classeDestination)) {
        // Traiter si le type de reception est oracle.jbo.domain.BlobDomain
        valeurConvertie = new BlobDomain((byte[]) valeurAttribut);

      } else if (ClobDomain.class.isAssignableFrom(classeDestination)) {
        // Traiter si le type de reception est oracle.jbo.domain.ClobDomain
        valeurConvertie = new ClobDomain((String) valeurAttribut);

      } else if (oracle.jbo.domain.Timestamp.class.isAssignableFrom(classeDestination)) {
        // Traiter si le type est oracle.jbo.domin.Timestamp
        valeurConvertie = new oracle.jbo.domain.Timestamp((java.util.Date) valeurAttribut);

      } else if (java.sql.Date.class.isAssignableFrom(classeDestination) && (valeurAttribut != null)) {
        // Traiter si le type est
        valeurConvertie = new java.sql.Date(((java.util.Date) valeurAttribut).getTime());

      } else if (java.sql.Timestamp.class.isAssignableFrom(classeDestination)) {
        valeurConvertie = new java.sql.Timestamp(((java.util.Date) valeurAttribut).getTime());

        // Traiter si le type est oracle.jbo.domin.DBSequence
      } else if (DBSequence.class.isAssignableFrom(classeDestination)) {
        if (Integer.class.isInstance(valeurAttribut)) {
          valeurConvertie = new DBSequence((Integer) valeurAttribut);
        } else {
          // Traiter un Long
          valeurConvertie = new DBSequence((Long) valeurAttribut);
        }

        // Traiter si le type est org.sofiframework.adf.modele.OuiNonDomain
      } else if (OuiNonDomain.class.isAssignableFrom(classeDestination)) {
        // Traiter si le type est org.sofiframework.adf.modele.domain.common.OuiNonDomain
        if (Boolean.class.isInstance(valeurAttribut)) {
          valeurConvertie = new OuiNonDomain((Boolean) valeurAttribut);

        } else {
          boolean valeur = ((Boolean) valeurAttribut).booleanValue();
          valeurConvertie = new OuiNonDomain(valeur);
        }

        // Traiter si le type est org.sofiframework.adf.modele.OuiNonDomain
      } else if (BooleanDomain.class.isAssignableFrom(classeDestination)) {
        // Traiter si le type est org.sofiframework.adf.modele.domain.common.BooleanDomain
        if (Boolean.class.isInstance(valeurAttribut)) {
          valeurConvertie = new BooleanDomain((Boolean) valeurAttribut);

        } else {
          int valeur = ((Integer) valeurAttribut).intValue();
          valeurConvertie = new BooleanDomain(valeur);
        }
      } else if (org.sofiframework.modele.adf.domain.TimestampLTZ.class.isAssignableFrom(classeDestination)) {
        Date date = (Date) valeurAttribut;
        valeurConvertie = new TimestampLTZ(new java.sql.Timestamp(date.getTime()));
      } else {
        valeurConvertie = valeurAttribut;
      }
    }

    return valeurConvertie;
  }

  /**
   * Permet de convertir une propriété d'un rangée en vleur correspondant à une
   * propriété de l'objet de transfert.
   * 
   * @return Valeur convertie dans la classe spécifiée
   * @param valeurAttribut Valeur originale de la propriété
   * @param classeDestination Classe dans laquelle on veut convertir la données
   * @param classeOrigine Classe de la propriété de la rangée
   */
  public static Object convertirEnObjetTransfert(Class classeOrigine, Class classeDestination, Object valeurAttribut) {
    Object valeurConvertie = null;

    if (valeurAttribut != null) {
      // Convertir jbo.domain.BlobDomain en byte[]
      if (BlobDomain.class.isAssignableFrom(classeOrigine)) {
        // Traiter si le type est oracle.jbo.domain.BlobDomain
        valeurConvertie = ((BlobDomain) valeurAttribut).toByteArray();

        // Convertir jbo.domain.ClobDomain en String
      } else if (ClobDomain.class.isAssignableFrom(classeOrigine)) {
        // Traiter si le type est oracle.jbo.domain.ClobDomain
        valeurConvertie = ((ClobDomain) valeurAttribut).toString();

        // Convertir oracle.jbo.domain.Date en java.util.Date
      } else if (oracle.jbo.domain.Date.class.isAssignableFrom(classeOrigine)) {
        valeurConvertie = (((oracle.jbo.domain.Date) valeurAttribut).getValue());

        // Convertir oracle.jbo.domain.Timestamp en java.util.Date
      } else if (oracle.jbo.domain.Timestamp.class.isAssignableFrom(classeOrigine)) {
        valeurConvertie = new java.util.Date(((oracle.jbo.domain.Timestamp) valeurAttribut).getTime());

        // Convertir java.sql.Timestamp en java.util.Date
      } else if (java.sql.Timestamp.class.isAssignableFrom(classeOrigine)) {
        valeurConvertie = new java.util.Date(((java.sql.Timestamp) valeurAttribut).getTime());

        // Convertir oracle.jbo.domain.DBSequence en java.lang.Integer ou java.lang.Long
      } else if (oracle.jbo.domain.DBSequence.class.isAssignableFrom(classeOrigine)) {
        if (classeDestination != null) {
          oracle.jbo.domain.Number seq = ((oracle.jbo.domain.DBSequence) valeurAttribut).getSequenceNumber();
          // Conversion en Integer
          if (classeDestination.isAssignableFrom(java.lang.Integer.class)) {
            valeurConvertie = new Integer(seq.stringValue());

            // Conversion en Long
          } else if (classeDestination.isAssignableFrom(Long.class)) {
            valeurConvertie = new Long(seq.stringValue());
          }
        }

        // Convertir OuiNonDomain en Boolean
      } else if (OuiNonDomain.class.isAssignableFrom(classeOrigine)) {
        // Traiter si le type est oracle.jbo.domain.BlobDomain
        valeurConvertie = ((OuiNonDomain) valeurAttribut).getBoolean();

        // Convertir BooleanDomain en Boolean
      } else if (BooleanDomain.class.isAssignableFrom(classeOrigine)) {
        // Traiter si le type est oracle.jbo.domain.BlobDomain
        valeurConvertie = ((BooleanDomain) valeurAttribut).getData();

      } else if (org.sofiframework.modele.adf.domain.TimestampLTZ.class.isAssignableFrom(classeOrigine)) {
        valeurConvertie = new DateTZ(((TimestampLTZ)valeurAttribut).getTime());

      } else {
        valeurConvertie = valeurAttribut;
      }
    }

    return valeurConvertie;
  }
}