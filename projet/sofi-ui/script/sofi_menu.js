
// Detect if the browser is IE or not.
// If it is not IE, we assume that the browser is NS.
var IE = document.all?true:false

// If NS -- that is, !IE -- then set up for mouse capture
if (!IE) document.captureEvents(Event.MOUSEMOVE)



activateMenu = function(nav) {
    /* currentStyle restricts the Javascript to IE only */
	if (document.all && document.getElementById(nav) && document.getElementById(nav).currentStyle) {  
		var navroot = document.getElementById(nav);
        /* Get all the list items within the menu */
        var lis=navroot.getElementsByTagName("LI");  
		for (i=0; i<lis.length; i++) {
            /* If the LI has another menu level */
            if(lis[i].lastChild.tagName=="UL"){
                /* assign the function to the LI */
             	lis[i].onmousemove=function() {		
                   /* display the inner menu */
                   this.lastChild.style.display="block";
                   if (IE6) {
                    cacherLesChampsSelect();
                   }
      			}
				lis[i].onmouseout=function() {                       
                    this.lastChild.style.display="none";
                    if (IE6) {
                      afficherLesChampsSelect();
                    }
				}
            }
		}
	}
}
/* I do this differently on the test page to fix a problem float container problem in IE5 Mac*/
/*Uncomment the function below*/

window.onload= function(){
   
    activateMenu('nav'); 
}


// Permet de cacher tous les champs de type liste déroulante.
function cacherLesChampsSelect() {
		var tags = new Array("applet", "iframe", "select");
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;

			for (var i = ar.length; i > 0;) {
				cc = ar[--i];

				cc.style.visibility = "hidden";
			}
		}

}

// Permet d'afficher tous les champs de type liste déroulante.
function afficherLesChampsSelect() {
		var tags = new Array("applet", "iframe", "select");
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;

			for (var i = ar.length; i > 0;) {
				cc = ar[--i];

				cc.style.visibility = "visible";
			}
		}

}






