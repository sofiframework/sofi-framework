// FR lang variables
// Modified by Shadow Walker, last updated 2007-04-13 with modification from Cmoine

tinyMCE.addToLang('template',{
title : 'Mod&egrave;les',
label : 'Mod&egrave;le',
desc_label : 'Description',
desc : 'Ins&eacute;rer un mod&egrave;le pr&eacute;d&eacute;finit',
select : 'Selectionner un mod&egrave;le',
preview : 'Pr&eacute;visualisation',
warning : 'Attention: Remplacer un mod&egrave;le par un autre peut vous faire perdre des donn&eacute;es.',
def_date_format : '%Y-%m-%d %H:%M:%S',
months_long : new Array("Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre"),
months_short : new Array("Jan", "F&eacute;v", "Mar", "Avr", "Mai", "Jun", "Jul", "Ao&ucirc;", "Sep", "Oct", "Nov", "D&eacute;c"),
day_long : new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"),
day_short : new Array("Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim")
});
