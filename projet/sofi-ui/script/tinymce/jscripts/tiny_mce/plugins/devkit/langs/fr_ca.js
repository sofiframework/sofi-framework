// FR lang variables
// Modified by keyko-web.net, last updated 2007-03-08

tinyMCE.addToLang('devkit',{
title : 'TinyMCE D&eacute;veloppement Kit',
info_tab : 'Info',
settings_tab : 'Configuration',
log_tab : 'Log',
content_tab : 'Contenu',
command_states_tab : 'Commandes',
undo_redo_tab : 'Annuler/Refaire',
misc_tab : 'Divers',
filter : 'Filtre:',
clear_log : 'Effacer log',
refresh : 'Rafra&icirc;chir',
info_help : 'Appuyer sur rafra&icirc;chir pour voir l\'info.',
settings_help : 'Appuyer sur rafra&icirc;chir pour afficher de la tableau de configuration de chaque instance de TinyMCE_Control.',
content_help : 'Appuyer sur rafra&icirc;chir pour afficher les donn&eacute;es brutes et un contenu HTML nettoyer de chaque instance deTinyMCE_Control.',
command_states_help : 'Appuyer sur rafra&icirc;chir pour afficher le statut des commandes de inst.queryCommandState. Ceci va aussi affich&eacute; les commandes non support&eacute;es.',
undo_redo_help : 'Appuyer sur rafra&icirc;chir pour afficher les instances Annuler/Refaire et leur niveau.',
misc_help : 'Voici divers outils pour corriger les erreurs et le d&eacute;veloppement.',
debug_events : 'Evenement de d&eacute;boggage',
undo_diff : 'Diff&eacute;rents niveaux d\'annulation'
});
