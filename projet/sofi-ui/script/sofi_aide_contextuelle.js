/* =================================================================================================
* NiceTitles
* 20th September 2004
* http://neo.dzygn.com/code/nicetitles
*
* NiceTitles turns your boring (X)HTML tags into a dynamic experience
* Modified by Kevin Hale for unfoldedorigami.com use. 
* Nicetitles now works with images, form fields and follows the mouse.
*
* Copyright (c) 2003 - 2004 Stuart Langridge, Paul McLanahan, Peter Janes, 
* Brad Choate, Dunstan Orchard, Ethan Marcotte, Mark Wubben, Kevin Hale
* 
*
* Licensed under MIT - http://www.opensource.org/licenses/mit-license.php
==================================================================================================*/

var isXML = (document.documentElement && document.documentElement.namespaceURI) ? true : false;

// Lightweight fix for window.addEventListener in KHTML:
if(navigator.userAgent.match(/(Konqueror|AppleWebKit)/i)) {
	KHTMLLoadListener = {
		handlers : [],
		add : function(evt, handler, capture) {
			var k = KHTMLLoadListener;
			k.handlers[k.handlers.length] = handler;
		},
		fire : function() {
			var k = KHTMLLoadListener;
			for(var i=0; i<k.handlers.length; i++) k.handlers[i]();
		}
	}
	window.addEventListener = KHTMLLoadListener.add;
	window.onload = KHTMLLoadListener.fire;
}

var browser = new Browser();

function NiceTitles(sTemplate, nDelay, nStringMaxLength, nMarginX, nMarginY, sContainerID, sClassName){

	var oTimer;
	var isActive = false;
  var isACacher = false;
	var showingAlready = false; //added var to start cursor tracking only AFTER delay occurs in show();
	var sNameSpaceURI = "http://www.w3.org/1999/xhtml";
  
  var texte;
	
	if(!sTemplate){ sTemplate = "attr(nicetitle)";}
	if(!nDelay || nDelay <= 0){ nDelay = false;}
	if(!nStringMaxLength){ nStringMaxLength = 30; }
	if(!nMarginX){ nMarginX = 15; }
	if(!nMarginY){ nMarginY = 35; }
	if(!sContainerID){ sContainerID = "nicetitlecontainer";}
	if(!sClassName){ sClassName = "aideContextuelle";}

	var oContainer = document.getElementById(sContainerID);
	if(!oContainer){
		oContainer = document.createElementNS ? document.createElementNS(sNameSpaceURI, "div") : document.createElement("div");
		oContainer.setAttribute("id", sContainerID);
		oContainer.className = sClassName;
		oContainer.style.display = "none";
		document.getElementsByTagName("body").item(0).appendChild(oContainer);
	}
	
	//=====================================================================
	// Method addElements (Public)
	//=====================================================================
	this.addElements = function addElements(collNodes, sAttribute){
		var currentNode, sTitle;

		for(var i = 0; i < collNodes.length; i++){
			currentNode = collNodes[i];

      var classEnTraitement = currentNode.className;
      styleErreur = false;
      
      if (classEnTraitement.indexOf('Erreur') != -1) {
        styleErreur = true;
      }
      
			sTitle = currentNode.getAttribute(sAttribute);

			if (sTitle && classEnTraitement != 'libelleErreur' ) {
				currentNode.setAttribute("nicetitle", sTitle);
				currentNode.removeAttribute(sAttribute);
				if (currentNode.type) {
					if ((currentNode.type == 'submit' || currentNode.type == 'button' || currentNode.type == 'reset' || currentNode.type == 'image') && !styleErreur) {
						currentNode.setAttribute("typeChamps","affichage");
					}else {
						currentNode.setAttribute("typeChamps","saisie");
					}
				}else {
					currentNode.setAttribute("typeChamps","affichage");
				}

        if (styleErreur) {
          addEvent(currentNode, 'blur', sortiChamp);     
          addEvent(currentNode, 'focus', show);  
          addEvent(currentNode, 'focus', show2);   
          addEvent(document, 'click', hide);          
          addEvent(currentNode, 'focus', focusChamp);        
          addEvent(document, 'click', hide);   
          removeEvent(currentNode, 'mouseover', show);  
        } 
        else {
          if (classEnTraitement && classEnTraitement.indexOf('aide') != -1) {
            addEvent(currentNode, 'click', show); 
            addEvent(currentNode, 'click', show2);          
            addEvent(document, 'click', hide);  
            addEvent(currentNode, 'mouseout', sortiChamp);  
            addEvent(currentNode, 'focus', focusChamp);   
          }else{
            addEvent(currentNode, 'mousemove', reposition); //added to allow cursor tracking       
            addEvent(currentNode, 'mouseover', show);             
            addEvent(currentNode, 'mouseout', hideObligatoire);     
          }
        }
			} 
      if (styleErreur == false) {
        addEvent(currentNode, 'focus', hideObligatoire);  
      }
		}
	}
	
	//=====================================================================
	// Other Methods (All Private)
	//=====================================================================
	function show(e){
      try {
        hideObligatoire();
        var oNode = window.event ? window.event.srcElement : e.currentTarget;
        if(!oNode.getAttribute("nicetitle")){ 
          while(oNode.parentNode){
            oNode = oNode.parentNode; // immediately goes to the parent, thus we can only have element nodes
            if(oNode.getAttribute("nicetitle")){ break;	}
          }
        }

        var sOutput = parseTemplate(oNode);
       
        setContainerContent(sOutput);
          
        var oPosition = getPosition(e, oNode);
        oContainer.style.left = oPosition.x;
        oContainer.style.top = oPosition.y;
        
        mx = oPosition.x;
        my = oPosition.y;
        
        reposition(e);
        
        var aide = oNode.getAttribute("nicetitle");		
        
        if (aide && aide.length) {
          STD_WIDTH = 500;
        
          t=0;
          t = aide.length;
        
          if (w <= STD_WIDTH && w != 0) {
            oContainer.style.width = w + 'px';
          }else {
            if (w != 0) {
              oContainer.style.width = 500 + 'px';
            }
          }
        }

        var commonEventInterface = window.event ? window.event : e;
          
        //added showingAlready. cursor tracks only after delay occurs
        if(nDelay){	
          if(commonEventInterface.type == "keydown") {
            oContainer.style.display = "block";
          }else {
            texte = oContainer.innerHTML;
            oTimer = setTimeout(function(){oContainer.innerHTML=texte;oContainer.style.display = "block"; oContainer.style.display = "block";showingAlready = true;cacherSelect(e, oNode);}, nDelay);
          }
        } else {
          oContainer.style.display = "block";
        }
          
        isActive = true;		
        // Let's put this event to a halt before it starts messing things up
        window.event ? window.event.cancelBubble = true : e.stopPropagation();
      } catch(e) {
      }

	}
  
  	//=====================================================================
	// Other Methods (All Private)
	//=====================================================================
	function show2(e){
      show(e);
	}
	
	
	function hide(){
    if (isActive && isACacher) {
      hideObligatoire();
    }
	}
  
	function hideObligatoire(){
    clearTimeout(oTimer);
    oContainer.style.display = "none";
    removeContainerContent();
    isActive = false;
    showingAlready = false;
    if (browser.isIE) {
      afficherLesChampsSelect();
    }
    afficherLesChampsSelect()    
	}
	
	function sortiChamp(){
    isACacher = true;
	}
  
	function focusChamp(){
    isACacher = false;
	}
  
  function copierTexte(e){
    var unicode=e.keyCode? e.keyCode : e.charCode;
   
 		try {
      if (unicode = 33) {
        var oNode = window.event ? window.event.srcElement : e.currentTarget;
        if(!oNode.getAttribute("nicetitle")){ 
          while(oNode.parentNode){
            oNode = oNode.parentNode; // immediately goes to the parent, thus we can only have element nodes
            if(oNode.getAttribute("nicetitle")){ break;	}
          }
        }
    
        var sOutput = parseTemplate(oNode);
        copy_clip(sOutput);
      }
    } catch(e) {
    }
  }

  
	//function added to allow cursor tracking by Kevin Hale
	function reposition(e){
		var oNode = window.event ? window.event.srcElement : e.currentTarget;

		var oPosition = getPosition(e, oNode);
		oContainer.style.left = oPosition.x;
		oContainer.style.top = oPosition.y;
		
		if(showingAlready){
			oContainer.style.display = "block";}
		else{
			oContainer.style.display = "none";}
		
		cacherSelect(e, oNode);
		
		isActive = true;
		// Let's put this event to a halt before it starts messing things up
		window.event ? window.event.cancelBubble = true : e.stopPropagation();
	}

	function setContainerContent(sOutput){
    if (oContainer.innerHTML == '') {
      sOutput = sOutput.replace(/&/g, "&amp;");
      if(document.createElementNS && window.DOMParser){
        var oXMLDoc = (new DOMParser()).parseFromString("<root xmlns=\""+sNameSpaceURI+"\">"+sOutput+"</root>", "text/xml");
        var oOutputNode = document.importNode(oXMLDoc.documentElement, true);
        var oChild = oOutputNode.firstChild;
        var nextChild;
        while(oChild){
          nextChild = oChild.nextSibling; // Once the child is appended, the nextSibling reference is gone
          oContainer.appendChild(oChild);
          oChild = nextChild;
        }
      } else {
        oContainer.innerHTML = sOutput;
      }
    }
	}
	
	function removeContainerContent(){
		var oChild = oContainer.firstChild;
		var nextChild;

		if(!oChild){ return; }
		while(oChild){
			nextChild = oChild.nextSibling;
			oContainer.removeChild(oChild);
			oChild =  nextChild;
		}
	}
	
	function getPosition(e, oNode){
  
  	var oViewport = getViewport();
		var oCoords;
		var commonEventInterface = window.event ? window.event : e;

		oCoords = getNodePosition(oNode);	
    
		oCoords.x += nMarginX;
		oCoords.y += nMarginY;	
		
    mx = oCoords.x;
    my = oCoords.y;
		
		
		// oContainer needs to be displayed before width and height can be retrieved
		if(showingAlready == false) // if statement prevents flickering in os x firefox when tracking cursor
			{oContainer.style.visiblity = "hidden"; 
			 oContainer.style.display =  "block";}
		var containerWidth = oContainer.offsetWidth;
		var containerHeight = oContainer.offsetHeight;
		if(showingAlready == false)
			{oContainer.style.display = "none"; 
			 oContainer.style.visiblity = "visible";}

		if(oCoords.x + containerWidth + 10 >= oViewport.width + oViewport.x){
			oCoords.x = oViewport.width + oViewport.x - containerWidth - 10;
		}
		
		var aide = oNode.getAttribute("nicetitle");		
		 	
    t=0;
       
    if (aide && aide.length) {
    	h = aide.length+1;
      t = aide.length;
    }

		w = t*8;

		hauteur = 25;
    

		if (w >= 650 && w < 1200) {
			hauteur = 42;
      
      if (IE7) {
        hauteur = 30;
      }
		}
		if (w >= 1200 && w < 1800) {
			hauteur = 58;
		}	

		if (w >= 1800 && w < 2300) {
			hauteur = 70;
		}	
		
		if (w >= 2300 ) {
			hauteur = 82;
		}	

    
		type = oNode.getAttribute("typeChamps");
		
		if (type != 'affichage' && type != null) {
    	
      oCoords.y -= hauteur;
    	oCoords.y -= 40;
    	oCoords.x -= 18;	
      
      if (IE7) {
        oCoords.y = oCoords.y - 15;
      }
      
      nDelay=200;
    }else {      
      oCoords.y -= 10;
    	oCoords.x -= 8;	
      
      if (IE7) {
        oCoords.y = oCoords.y + 15;
      }
      
      nDelay=500;
    }

		oCoords.x += "px";
		oCoords.y += "px";

		return oCoords;
	}

	function cacherSelect(e, oNode){
    if (IE && !IE7) {
      var oViewport = getViewport();
      var oCoords;
      var commonEventInterface = window.event ? window.event : e;
  
      oCoords = getNodePosition(oNode);	
  
      oCoords.x += nMarginX;
      oCoords.y += nMarginY;	
      
      mx = oCoords.x;
      my = oCoords.y;
      
    
      
      var aide = oNode.getAttribute("nicetitle");		
    
      STD_WIDTH = 500;
      
  
      if (aide && aide.length) {
        h = aide.length+1;
        t = aide.length;
      }
  
      STD_WIDTH = 500;
      
      t=0;
      
      if (aide && aide.length) {
        h = aide.length+1;
        t = aide.length;
      }
  
      w = t*8;
  
      hauteur = 25;
  
      if (w >= 550 && w < 1200) {
        hauteur = 52;
      }
      if (w >= 1200 && w < 1800) {
        hauteur = 58;
      }	
  
      if (w >= 1800 && w < 2300) {
        hauteur = 70;
      }	
      
      if (w >= 2300 ) {
        cacherLesChampsSelect();
      }	
  
      
      type = oNode.getAttribute("typeChamps");
      
      
      my= my-35;
      
	    if (type == 'affichage') {
	    	cacherChampsSelectPourAideContextuelle(mx-100, my+hauteur, w, t);
	    
	    }else {
	    	cacherChampsSelectPourAideContextuelle(oCoords.x-100, my-hauteur, w, t);
	    }
	  }

		return oCoords;
	}

	function parseTemplate(oNode){
		var sAttribute, collOptionalAttributes;
		var oFound = {};
		var sResult = sTemplate;
		
		if(sResult.match(/content\(\)/)){
			sResult = sResult.replace(/content\(\)/g, getContentOfNode(oNode));
		}
		
		var collSearch = sResult.split(/attr\(/);
		for(var i = 1; i < collSearch.length; i++){
			sAttribute = collSearch[i].split(")")[0];
			oFound[sAttribute] = oNode.getAttribute(sAttribute);
			if(oFound[sAttribute] && oFound[sAttribute].length > nStringMaxLength){
				//oFound[sAttribute] = oFound[sAttribute].substring(0, nStringMaxLength) + "...";
			}
		}
		
		var collOptional = sResult.split("?")
		for(var i = 1; i < collOptional.length; i += 2){
			collOptionalAttributes = collOptional[i].split("attr(");
			for(var j = 1; j < collOptionalAttributes.length; j++){
				sAttribute = collOptionalAttributes[j].split(")")[0];

				if(!oFound[sAttribute]){ sResult = sResult.replace(new RegExp("\\?[^\\?]*attr\\("+sAttribute+"\\)[^\\?]*\\?", "g"), "");	}
			}
		}
		sResult = sResult.replace(/\?/g, "");
		
		for(sAttribute in oFound){
			sResult = sResult.replace("attr\("+sAttribute+"\)", oFound[sAttribute]);
		}
		
		return sResult;
	}	
		
	function getContentOfNode(oNode){
		var sContent = "";
		var oSearch = oNode.firstChild;

		while(oSearch){
			if(oSearch.nodeType == 3){
				sContent += oSearch.nodeValue;
			} else if(oSearch.nodeType == 1 && oSearch.hasChildNodes){
				sContent += getContentOfNode(oSearch);
			}
			oSearch = oSearch.nextSibling
		}

		return sContent;
	}

	function getNodePosition(el) {
		var SL = 0, ST = 0;
		var is_div = /^div$/i.test(el.tagName);
		if (is_div && el.scrollLeft)
			SL = el.scrollLeft;
		if (is_div && el.scrollTop)
			ST = el.scrollTop;
		var r = { x: el.offsetLeft - SL, y: el.offsetTop - ST };
		if (el.offsetParent) {
			var tmp = getNodePosition(el.offsetParent);
			r.x += tmp.x;
			r.y += tmp.y;
		}
		return {x : r.x, y : r.y}
	}
	
	// Idea from 13thParallel: http://13thparallel.net/?issue=2002.06&title=viewport
	function getViewport(){
		var width = 0;
		var height = 0;
		var x = 0;
		var y = 0;
		
		if(document.documentElement && document.documentElement.clientWidth){
			width = document.documentElement.clientWidth;
			height = document.documentElement.clientHeight;
			x = document.documentElement.scrollLeft;
			y = document.documentElement.scrollTop;
		} else if(document.body && document.body.clientWidth){
			width = document.body.clientWidth;
			height = document.body.clientHeight;
			x = document.body.scrollLeft;
			y = document.body.scrollTop;
		}
		// we don't use an else if here, since Opera 7 tends to get the height on the documentElement wrong
		if(window.innerWidth){ 
			width = window.innerWidth - 18;
			height = window.innerHeight - 18;
		}
		
		if(window.pageXOffset){
			x = window.pageXOffset;
			y = window.pageYOffset;
		} else if(window.scrollX){
			x = window.scrollX;
			y = window.scrollY;
		}
		
		return {width : width, height : height, x : x, y : y };		
	}
}

//=====================================================================
// Event Listener
// by Scott Andrew - http://scottandrew.com
// edited by Mark Wubben, <useCapture> is now set to false
//=====================================================================
function addEvent(obj, evType, fn){
	if(obj.addEventListener){
		obj.addEventListener(evType, fn, false); 
		return true;
	} else if (obj.attachEvent){
		var r = obj.attachEvent('on'+evType, fn);
		return r;
	} else {
		return false;
	}
}

// Determine browser and version.

function Browser() {
// blah, browser detect, but mouse-position stuff doesn't work any other way
  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}


function cacherChampsSelectPourAideContextuelle(mx, my, w, nbCarac) {
		var tags = new Array("applet", "select");
	
		// Calcule du nombre ligne approx.
		var nbLigne = parseInt(nbCarac/60);

		reste= (nbCarac/60) - nbLigne;

		if (reste > 0) {
			++nbLigne;	
		}
		
		hauteur = parseInt(30);
		
		var nbLigneAAjouter = nbLigne/2;
		resteNbLigne = (nbLigne/2) - nbLigneAAjouter;
		if (resteNbLigne == 0 ) {
			hauteur=hauteur*nbLigneAAjouter;
			
		}
	
		var EX1 = mx;
		var EX2 = w + EX1;
		var EY1 = my;
		var EY2 = hauteur + EY1;
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;
			
			for (var i = ar.length; i > 0;) {
				cc = ar[--i];
				p = getAbsolutePos(cc);
				var CX1 = p.x;
				var CX2 = cc.offsetWidth + CX1;
				var CY1 = p.y;
				var CY2 = cc.offsetHeight + CY1;

				if (self.hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {

				} else {
					cc.style.visibility = "hidden";
				}
				
				
			}
		}

}

function afficherLesChampsSelect() {
		var tags = new Array("applet", "iframe", "select");
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;

			for (var i = ar.length; i > 0;) {
				cc = ar[--i];

				cc.style.visibility = "visible";
			}
		}

}

getAbsolutePos = function(el) {
	var SL = 0, ST = 0;
	var is_div = /^div$/i.test(el.tagName);
	if (is_div && el.scrollLeft)
		SL = el.scrollLeft;
	if (is_div && el.scrollTop)
		ST = el.scrollTop;
	var r = { x: el.offsetLeft - SL, y: el.offsetTop - ST };
	if (el.offsetParent) {
		var tmp = this.getAbsolutePos(el.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
};


//=====================================================================
// Here the default nice titles are created
//=====================================================================
NiceTitles.autoCreation = function(){

	if(!document.getElementsByTagName){ return; }

	NiceTitles.autoCreated = new Object();

	NiceTitles.autoCreated.anchors = new NiceTitles("<p class=\"titletext\">attr(nicetitle)</p>", 300);
	NiceTitles.autoCreated.acronyms = new NiceTitles("<p class=\"titletext\">content(): attr(nicetitle)</p>", 300);
	//for image nicetitles based off alt tags
	NiceTitles.autoCreated.images = new NiceTitles("<p class=\"titletext\">attr(nicetitle)</p>", 300);
	//for form nicetitles. just add title to input and textarea tags
	NiceTitles.autoCreated.input = new NiceTitles("<p class=\"destination\">attr(nicetitle)</p>", 300);
	
	
	NiceTitles.autoCreated.anchors.addElements(document.getElementsByTagName("a"), "title");
	NiceTitles.autoCreated.images.addElements(document.getElementsByTagName("td"), "alt"); //added
	NiceTitles.autoCreated.acronyms.addElements(document.getElementsByTagName("acronym"), "title");
	NiceTitles.autoCreated.acronyms.addElements(document.getElementsByTagName("abbr"), "title");
	NiceTitles.autoCreated.input.addElements(document.getElementsByTagName("input"), "title"); //added
	NiceTitles.autoCreated.input.addElements(document.getElementsByTagName("textarea"), "title"); //added
	NiceTitles.autoCreated.anchors.addElements(document.getElementsByTagName("span"), "title"); //added
	NiceTitles.autoCreated.anchors.addElements(document.getElementsByTagName("label"), "title"); //added
	NiceTitles.autoCreated.anchors.addElements(document.getElementsByTagName("select"), "title"); //added
}


addEvent(window, "load", NiceTitles.autoCreation);


// Add an eventListener to browsers that can do it somehow.
// Originally by the amazing Scott Andrew.
function addEvent(obj, evType, fn){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, false);
    return true;
  } else if (obj.attachEvent){
	var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
	return false;
  }
}

function removeEvent(obj, evType, fn){
  if (obj.removeEventListener){
    obj.removeEventListener(evType, fn, false);
    return true;
  } else if (obj.detachEvent){
	var r = obj.detachEvent("on"+evType, fn);
    return r;
  } else {
	return false;
  }
}

function copy_clip(meintext)
{
 if (window.clipboardData) 
   {
   
   // the IE-manier
   window.clipboardData.setData("Text", meintext);
   
   // waarschijnlijk niet de beste manier om Moz/NS te detecteren;
   // het is mij echter onbekend vanaf welke versie dit precies werkt:
   }
   else if (window.netscape) 
   { 
   
   // dit is belangrijk maar staat nergens duidelijk vermeld:
   // you have to sign the code to enable this, or see notes below 
   netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
   
   // maak een interface naar het clipboard
   var clip = Components.classes['@mozilla.org/widget/clipboard;1']
                 .createInstance(Components.interfaces.nsIClipboard);
   if (!clip) return;
   
   // maak een transferable
   var trans = Components.classes['@mozilla.org/widget/transferable;1']
                  .createInstance(Components.interfaces.nsITransferable);
   if (!trans) return;
   
   // specificeer wat voor soort data we op willen halen; text in dit geval
   trans.addDataFlavor('text/unicode');
   
   // om de data uit de transferable te halen hebben we 2 nieuwe objecten 
   // nodig om het in op te slaan
   var str = new Object();
   var len = new Object();
   
   var str = Components.classes["@mozilla.org/supports-string;1"]
                .createInstance(Components.interfaces.nsISupportsString);
   
   var copytext=meintext;
   
   str.data=copytext;
   
   trans.setTransferData("text/unicode",str,copytext.length*2);
   
   var clipid=Components.interfaces.nsIClipboard;
   
   if (!clip) return false;
   
   clip.setData(trans,null,clipid.kGlobalClipboard);
   
   }
 
   return false;
}

// Permet de cacher tous les champs de type liste déroulante.
function cacherLesChampsSelect() {
		var tags = new Array("applet", "iframe", "select");
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;

			for (var i = ar.length; i > 0;) {
				cc = ar[--i];

				cc.style.visibility = "hidden";
			}
		}

}

// Permet d'afficher tous les champs de type liste déroulante.
function afficherLesChampsSelect() {
		var tags = new Array("applet", "iframe", "select");
		
		for (var k = tags.length; k > 0; ) {
			var ar = document.getElementsByTagName(tags[--k]);
			var cc = null;

			for (var i = ar.length; i > 0;) {
				cc = ar[--i];

				cc.style.visibility = "visible";
			}
		}

}
