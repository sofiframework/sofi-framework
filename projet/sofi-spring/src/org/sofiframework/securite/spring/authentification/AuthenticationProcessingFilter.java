/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.spring.authentification;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.securite.spring.context.ContexteAuthentification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class AuthenticationProcessingFilter
extends org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
{

  private static final Log log = LogFactory.getLog(AuthenticationProcessingFilter.class);

  private ContexteAuthentification contexte = null;

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException, ServletException {
    super.successfulAuthentication(request, response, authResult);
    // On complete
    try {
      if (authResult != null) {
        String codeUtilisateur = authResult.getName();
        contexte.ajouterUtilisateurDansSession(codeUtilisateur, request);
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour compléter l'authentification.", e);
      }
    }
  }

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, javax.servlet.ServletException {
    super.unsuccessfulAuthentication(request, response, exception);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)  throws AuthenticationException {
    Authentication authentification = super.attemptAuthentication(request, response);
    Locale locale = request.getLocale();

    return authentification;
  }

  @Override
  protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
    boolean authentificationRequise = super.requiresAuthentication(request, response);
    return authentificationRequise;
  }

  public ContexteAuthentification getContexte() {
    return contexte;
  }

  public void setContexte(ContexteAuthentification contexte) {
    this.contexte = contexte;
  }
}
