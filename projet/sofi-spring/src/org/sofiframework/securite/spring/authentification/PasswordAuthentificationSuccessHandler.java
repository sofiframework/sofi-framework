package org.sofiframework.securite.spring.authentification;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.securite.spring.authentification.exception.NewPasswordException;
import org.sofiframework.securite.spring.authentification.exception.PasswordExpiredException;
import org.sofiframework.securite.spring.context.ContexteAuthentification;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;


/**
 * Comportement a appliquer lorsqu'on désire une gestion des mots de passe après authentification.
 * Ce Handler permet de gérer les mots de passes expirés ou encore le traitement pour un nouveau mot de passe.
 * @author jfbrassard
 * @version 3.0
 */

public class PasswordAuthentificationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

  private static final Log log = LogFactory.getLog(PasswordAuthentificationSuccessHandler.class);

  private ContexteAuthentification contexte = null;

  /**
   * URL cible pour le changement d'un mot de passe expiré.
   */
  private String motPasseExpireTargetUrl;

  /**
   * URL cible pour la saisie d'un nouveau mot de passe.
   */
  private String nouveauMotPasseTargetUrl;

  private static final String PASSWORD_EXPIRED = "pwd_expired";

  private static final String NEW_PASSWORD = "new_pwd";

  /**
   * @return the contexte
   */
  public ContexteAuthentification getContexte() {
    return contexte;
  }

  /**
   * @param contexte the contexte to set
   */
  public void setContexte(ContexteAuthentification contexte) {
    this.contexte = contexte;
  }

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
      HttpServletResponse response, Authentication authentication)
          throws ServletException, IOException {

    try {
      if (authentication != null) {
        String codeUtilisateur = authentication.getName();
        contexte.ajouterUtilisateurDansSession(codeUtilisateur, request);
        ServiceUtilisateur serviceUtilisateur = contexte.getServiceUtilisateur();
        Utilisateur utilisateur = serviceUtilisateur.getUtilisateur(codeUtilisateur);

        // Extraire le paramètre système DUREE_MOT_PASSE si politique de changement de mot de passe impose.
        Integer pwdExpiration = GestionParametreSysteme.getInstance().getInteger("DUREE_MOT_PASSE");

        // If expiration date is null, then password just have been set by administrator, and user must choose his own password
        if (utilisateur.getDateMotPasse() == null) {
          this.getRedirectStrategy().sendRedirect(request, response, this.getNouveauMotPasseTargetUrl());
          throw new NewPasswordException();
        }
        else {
          // Verifier la date d'expiration du mot de passe
          Calendar cal = Calendar.getInstance();
          cal.setTime(utilisateur.getDateMotPasse());
          cal.add(Calendar.DAY_OF_YEAR, pwdExpiration);
          if (cal.getTime().before(new Date())) {

            this.getRedirectStrategy().sendRedirect(request, response, this.getMotPasseExpireTargetUrl());
            throw new PasswordExpiredException();
          }
        }
      }

      String urlDefaut = this.getDefaultTargetUrl();

      this.getRedirectStrategy().sendRedirect(request, response, urlDefaut);


    } catch (Exception e) {
      if (e instanceof SecuriteException) {
        throw (SecuriteException) e;
      } else {
        if (log.isErrorEnabled()) {
          log.error("Erreur pour compléter l'authentification.", e);
        }
      }
    }
  }

  public String getMotPasseExpireTargetUrl() {
    return motPasseExpireTargetUrl;
  }

  public void setMotPasseExpireTargetUrl(String motPasseExpireTargetUrl) {
    this.motPasseExpireTargetUrl = motPasseExpireTargetUrl;
  }

  public String getNouveauMotPasseTargetUrl() {
    return nouveauMotPasseTargetUrl;
  }

  public void setNouveauMotPasseTargetUrl(String nouveauMotPasseTargetUrl) {
    this.nouveauMotPasseTargetUrl = nouveauMotPasseTargetUrl;
  }

}
