/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.spring.authentification;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.securite.spring.authentification.exception.NewPasswordException;
import org.sofiframework.securite.spring.authentification.exception.PasswordExpiredException;
import org.sofiframework.securite.spring.context.ContexteAuthentification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Filtre de sécurité pour le traitement de vérification du mot de passe lors d'une authentification.
 * @author Jérôme Fiolleau, Nurun inc.
 * @version 3.0
 */
public class PasswordProcessingFilter extends org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter {

  private static final Log log = LogFactory.getLog(PasswordProcessingFilter.class);

  private ContexteAuthentification contexte = null;


  private static final String PASSWORD_EXPIRED = "pwd_expired";

  private static final String NEW_PASSWORD = "new_pwd";


  /* (non-Javadoc)
   * @seeorg.springframework.security..ui.AbstractProcessingFilter#onSuccessfulAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,org.springframework.security..Authentication)
   */
  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
      throws IOException, ServletException {

    if (log.isInfoEnabled()) {
      log.info("Tentative de verification du mot de passe pour : " + authResult.getPrincipal());
    }

    SecurityContextHolder.getContext().setAuthentication(authResult);

    try {
      try {
        request.getSession().removeAttribute(NEW_PASSWORD);
        request.getSession().removeAttribute(PASSWORD_EXPIRED);

        // Call method that control current situation toward the user password
        super.successfulAuthentication(request, response, authResult);


      } catch (NewPasswordException e) {
        request.getSession().setAttribute(SPRING_SECURITY_LAST_EXCEPTION_KEY, null);
        request.getSession().setAttribute(NEW_PASSWORD, true);
      } catch (PasswordExpiredException e) {
        request.getSession().setAttribute(SPRING_SECURITY_LAST_EXCEPTION_KEY, null);
        request.getSession().setAttribute(PASSWORD_EXPIRED, true);
      }
    } catch (Exception e) {
      if (e instanceof SecuriteException) {
        throw (SecuriteException) e;
      } else {
        if (log.isErrorEnabled()) {
          log.error("Erreur pour compléter l'authentification.", e);
        }
      }
    }
  }


  /**
  private void finishSuccessfulAuthentication(HttpServletRequest request,
      HttpServletResponse response, Authentication authResult) {

    getRememberMeServices().loginSuccess(request, response, authResult);
    // Fire event
    if (this.eventPublisher != null) {
      eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
    }

  }
   **/

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
    super.unsuccessfulAuthentication(request, response, exception);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    Authentication authentification = super.attemptAuthentication(request, response);
    return authentification;
  }

  @Override
  protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
    boolean authentificationRequise = super.requiresAuthentication(request, response);
    return authentificationRequise;
  }

  /**
   * @return the contexte
   */
  public ContexteAuthentification getContexte() {
    return contexte;
  }

  /**
   * @param contexte the contexte to set
   */
  public void setContexte(ContexteAuthentification contexte) {
    this.contexte = contexte;
  }


}
