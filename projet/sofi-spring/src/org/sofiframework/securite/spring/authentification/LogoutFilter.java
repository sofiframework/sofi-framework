/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.spring.authentification;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutFilter extends org.springframework.security.web.authentication.logout.LogoutFilter {

  public LogoutFilter(String logoutSuccessUrl, LogoutHandler[] handlers) {
    super(logoutSuccessUrl, handlers);
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chaine) throws IOException, ServletException {
    super.doFilter(request, response, chaine);
  }

  @Override
  protected boolean requiresLogout(HttpServletRequest request, HttpServletResponse response) {
    boolean logoutRequis = super.requiresLogout(request, response);
    return logoutRequis;
  }
}
