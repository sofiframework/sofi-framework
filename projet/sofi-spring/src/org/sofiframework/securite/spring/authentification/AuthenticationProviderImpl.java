package org.sofiframework.securite.spring.authentification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.application.securite.service.ServiceValidationAuthentification;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class AuthenticationProviderImpl implements AuthenticationProvider,
ApplicationContextAware {

  private static final Log log = LogFactory
      .getLog(AuthenticationProviderImpl.class);

  private ApplicationContext context;
  private Collection<GrantedAuthority> grantedAuthorities;
  private ServiceValidationAuthentification serviceValidationAuthentification;
  private ServiceMessage serviceMessage;
  private ServiceUtilisateur serviceUtilisateur;

  @Override
  public Authentication authenticate(Authentication authentication)
      throws AuthenticationException {
    boolean autoLogin = authentication instanceof RememberMeAuthenticationToken;
    return this.authenticate(authentication, autoLogin);
  }

  @Override
  public Authentication authenticate(Authentication authentication,
      boolean autoLogin) throws AuthenticationException {
    // Determine username
    String username = "NONE_PROVIDED";
    String password = "NONE";
    UserDetails user = null;

    Locale locale = Locale.getDefault();

    if (log.isInfoEnabled()) {
      log.info("Verification de l'authentifiadtion");
    }

    if (authentication.getPrincipal() != null && !"".equals(authentication.getPrincipal().toString().trim())) {
      username = authentication.getPrincipal().toString().trim();

      if (log.isInfoEnabled()) {
        log.info("Tentative pour l'utilisateur" + username);
      }

      if (!autoLogin) {
        password = authentication.getCredentials().toString().trim();
      }

      if (autoLogin
          || serviceValidationAuthentification.validerAuthentification(
              username, password)) {
        // On récupère l'utilisateur SOFI
        Utilisateur utilisateur = this.getServiceUtilisateur().getUtilisateur(
            username);
        
        
        String authorizedStatus = GestionParametreSysteme.getInstance().getString("authentificationStatutActif");
        boolean utilisateurInactif = !"A".equals(utilisateur.getCodeStatut());
        
        if (utilisateurInactif && !StringUtils.isEmpty(authorizedStatus)) {
          String[] results = authorizedStatus.split(",");
          for (int i = 0; i< results.length && utilisateurInactif; i++) {
            if (utilisateur.getCodeStatut().equals(results[i].trim())) {
              utilisateurInactif = false;
            }
          }   
        }

        // On doit seulement authentifier les utilisateurs actifs.
        if (utilisateur != null && utilisateurInactif) {
          utilisateur = null;
        }

        if (utilisateur != null) {
          /*
           * grantedAuthorities = convertirRole List roles =
           * utilisateur.getListeRoles();
           */
          setGrantedAuthorities(utilisateur.getListeRoles());
          user = new User(utilisateur.getCodeUtilisateur(), // Code utilisateur
              authentication.getCredentials().toString(), // Mot de passe
              true, // Actif
              true, // Non Expiré
              true, // Mot de passe n'est pas à changé
              true, // Non verrouillé
              grantedAuthorities); //
          if (log.isInfoEnabled()) {
            log.info("Utilisateur a reussit son authentification");
          }
        } else {
          BadCredentialsException bce = new BadCredentialsException("login.erreur.authentification.codeUtilisateur.invalide");
          publierEvenenementContext(new AuthenticationFailureBadCredentialsEvent(
              authentication, bce));
          throw bce;
        }
      } else {
        BadCredentialsException bce = new BadCredentialsException("login.erreur.authentification.codeUtilisateur.invalide");

        if (log.isInfoEnabled()) {
          log.info("Utilisateur a echoue son authentification");
        }
        // On alerte que l'authentification est raté
        publierEvenenementContext(new AuthenticationFailureBadCredentialsEvent(
            authentication, bce));
        throw bce;
      }
    } else {
      BadCredentialsException bce = new BadCredentialsException("login.erreur.authentification.codeUtilisateur.obligatoire");
      // On alerte que l'authentification est raté
      publierEvenenementContext(new AuthenticationFailureBadCredentialsEvent(
          authentication, bce));
      throw bce;
    }

    // On alerte que l'authentification est réussis
    publierEvenenementContext(new AuthenticationSuccessEvent(authentication));
    Object principalToReturn = user;
    return createSuccessAuthentication(principalToReturn, authentication,
        grantedAuthorities);
  }

  protected Authentication createSuccessAuthentication(Object principal,
      Authentication authentication, Collection<GrantedAuthority> grantedAuthorities) {
    // Ensure we return the original credentials the user supplied,
    // so subsequent attempts are successful even with encoded passwords.
    // Also ensure we return the original getDetails(), so that future
    // authentication events after cache expiry contain the details
    UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
        principal, authentication, grantedAuthorities);
    result.setDetails((authentication.getDetails() != null) ? authentication
        .getDetails() : null);
    return result;
  }

  @Override
  public boolean supports(Class authentication) {
    boolean isAuthenticationForm = UsernamePasswordAuthenticationToken.class
        .isAssignableFrom(authentication);
    boolean isRememberMe = RememberMeAuthenticationToken.class
        .isAssignableFrom(authentication);
    return isAuthenticationForm || isRememberMe;
  }

  private void publierEvenenementContext(ApplicationEvent applicationEvent) {
    if (this.context != null) {
      context.publishEvent(applicationEvent);
    }
  }

  public void setGrantedAuthorities(List newGrantedAuthorities) {
    // convert the granted authorities list passed in to a
    // GrantedAuthorities[]
    Collection<GrantedAuthority> grantedArr = new ArrayList();
    Iterator it = newGrantedAuthorities.iterator();
    Role role = null;
    while (it.hasNext()) {
      role = (Role) it.next();
      grantedArr.add(new GrantedAuthorityImpl(role.getCodeApplication() + "_"
          + role.getNom()));
    }
    grantedAuthorities = grantedArr;
  }

  /* Seront injectés par spring.... */
  @Override
  public void setApplicationContext(ApplicationContext applicationContext)
      throws BeansException {
    this.context = applicationContext;
  }

  public ServiceValidationAuthentification getServiceValidationAuthentification() {
    return serviceValidationAuthentification;
  }

  public void setServiceValidationAuthentification(
      ServiceValidationAuthentification serviceValidationAuthentification) {
    this.serviceValidationAuthentification = serviceValidationAuthentification;
  }

  public ServiceMessage getServiceMessage() {
    return serviceMessage;
  }

  public void setServiceMessage(ServiceMessage serviceMessage) {
    this.serviceMessage = serviceMessage;
  }

  public ServiceUtilisateur getServiceUtilisateur() {
    return this.serviceUtilisateur;
  }

  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }

  public Collection getGrantedAuthorities() {
    return grantedAuthorities;
  }

  public void setGrantedAuthorities(Collection grantedAuthorities) {
    this.grantedAuthorities = grantedAuthorities;
  }
}
