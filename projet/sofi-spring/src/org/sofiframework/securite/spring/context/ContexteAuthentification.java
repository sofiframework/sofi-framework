/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.spring.context;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceHistoriqueMotPasse;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.beans.factory.InitializingBean;

/**
 * Regroupement des configurations des traitements qui sont associés
 * aux certificats d'authenification de SOFI et à leur paramétrisation.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ContexteAuthentification implements InitializingBean {

  private static final Log log = LogFactory.getLog(ContexteAuthentification.class);

  private ServiceUtilisateur serviceUtilisateur;
  private ServiceHistoriqueMotPasse serviceHistoriqueMotPasse;
  private String nomCookie = null;
  private Integer ageCookie = null;
  private String domaineCookie = null;
  private ArrayList listeDomaineCookie = null;
  private Boolean secureCookie = Boolean.FALSE;

  @Override
  public void afterPropertiesSet() throws java.lang.Exception {

    if (log.isInfoEnabled()) {
      log.info("Initialisation du contexte de l'authentification.");
    }

    this.initialiserNomCookie();
    this.initialiserDomaineCookie();
    this.initialiserAgeCookie();

    if (log.isDebugEnabled()) {
      log.debug("NomCookie" + nomCookie);
      log.debug("AgeCookie" + ageCookie);
      log.debug("DomaineCookie" + domaineCookie);
    }
  }

  private void initialiserNomCookie() {
    // Extraire le dommaine (dont le cookie sera accessible par l'authentification) des paramètres systèmes si disponible.
    String paramNomCookie = getParam(ConstantesParametreSysteme.AUTHENTIFICATION_NOM_COOKIE);
    if (nomCookie == null) {
      nomCookie = paramNomCookie;
    }

    GestionSecurite.getInstance().setNomCookieCertificat(getNomCookie());
  }

  private void initialiserDomaineCookie() {
    // Extraire le domaine (dont le cookie sera accessible par l'authentification) des paramètres systèmes si disponible.
    String paramDomaineCookie = getParam(ConstantesParametreSysteme.AUTHENTIFICATION_DOMAINE_COOKIE);
    if (domaineCookie == null) {
      domaineCookie = paramDomaineCookie;
    }

/*    if (domaineCookie != null) {
      listeDomaineCookie = getListeNomDomaineCookie(domaineCookie);
    }*/

    GestionSecurite.getInstance().setNomDomaineCookie(domaineCookie);
  }

  private void initialiserAgeCookie() {
    // Extraire le dommaine (dont le cookie sera accessible par l'authentification) des paramètres systèmes si disponible.
    String paramAgeCookie = getParam(ConstantesParametreSysteme.AUTHENTIFICATION_AGE_COOKIE);
    if (ageCookie == null) {
      ageCookie = new Integer(paramAgeCookie);
    }
    
    // Extraire le dommaine (dont le cookie sera accessible par l'authentification) des paramètres systèmes si disponible.
    Boolean paramSecureCookie = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AUTHENTIFICATION_SECURE_COOKIE);
    if (paramSecureCookie != null) {
      secureCookie = paramSecureCookie;
    }    
  }

  public void ajouterUtilisateurDansSession(String codeUtilisateur, HttpServletRequest request) throws Exception {
    // Si l'utilisateur n'est pas dans la session, on l'ajoute
    if (codeUtilisateur != null
        && !codeUtilisateur.equals("anonymousUser")
        && UtilitaireControleur.getUtilisateur(request.getSession()) == null) {
      UtilitaireControleur.specifierNouvelleAuthentification(request);
      Utilisateur utilisateur = GestionSecurite.getInstance().getUtilisateur(codeUtilisateur);
      UtilitaireControleur.ajouterUtilisateurDansSession(request.getSession(), utilisateur);
      if (utilisateur != null) {
        GestionSecurite.genererAutorisationUtilisateur(utilisateur);
      }
    }
  }

  public String getCertificat(HttpServletRequest request, HttpServletResponse response) {
    String certificat = request.getParameter(this.getNomCookie());

    if (certificat == null) {
      Cookie cookie = UtilitaireRequest.getCookie(request, this.getNomCookie());
      if (cookie != null) {
        certificat = cookie.getValue();
      }

      if (certificat == null) {
        certificat = (String) request.getSession().getAttribute(this.getNomCookie());
      }
    }

    return certificat;
  }

  public void terminerCertificat(HttpServletRequest request, HttpServletResponse response) {
    String certificat = this.getCertificat(request, response);

    if (certificat != null) {
      GestionSecurite.getInstance().getServiceAuthentification().terminerCertificat(certificat);
    }
  }

  public boolean certificatValide(String certificat) {
    boolean valide = true;

    if (certificat != null) {
      if (GestionSecurite.getInstance().getServiceAuthentification().getCodeUtilisateur(certificat) == null) {
        valide = false;
      }
    }

    return valide;
  }

  public void ajouterCookies(String certificat, HttpServletRequest request, HttpServletResponse response) {
    /* Si le filtre est configuré pour supporter une groupe de domaines.
     * On ajoute un cookie pour chaque domaine
     */

    if (log.isInfoEnabled()) {
      log.info("Creation du cookie d'authentifiation pour le certificat :" + certificat);
    }
    if (listeDomaineCookie != null) {
      for (Iterator iterateurListeDomaine = listeDomaineCookie.iterator(); iterateurListeDomaine.hasNext();) {
        String domaine = (String) iterateurListeDomaine.next();
        UtilitaireRequest.setCookie(response, nomCookie,
            certificat, "/", domaine, ageCookie, secureCookie);
      }
    } else {
      UtilitaireRequest.setCookie(response, nomCookie,
          certificat, "/", domaineCookie, ageCookie, secureCookie);
    }
  }

  public void supprimerCookies(HttpServletRequest request, HttpServletResponse response) {
    UtilitaireRequest.supprimerCookie(request, response, this.getNomCookie(), "/");
  }

  public void nettoyerCertificat(HttpServletRequest request, HttpServletResponse response) {
    this.terminerCertificat(request, response);
    this.supprimerCookies(request, response);
    UtilitaireControleur.supprimerUtilisateur(request.getSession());
    request.getSession().removeAttribute(this.getNomCookie());
  }

  /**
   * Retourne la liste des nom de domaine que le cookie sera accessible.
   * @param nomDomaineCookie la liste des nom de domaine que le cookie sera accessible.
   */
  private ArrayList getListeNomDomaineCookie(String nomDomaineCookie) {
    ArrayList liste = new ArrayList();

    if (nomDomaineCookie.indexOf(",") != -1) {
      // Plusieurs paramètres y sont associés.
      StringTokenizer listeToken = new StringTokenizer(nomDomaineCookie, ",");

      while (listeToken.hasMoreTokens()) {
        String valeur = listeToken.nextToken().trim();
        liste.add(valeur);
      }
    } else {
      liste.add(nomDomaineCookie);
    }

    return liste;
  }

  public String getNomCookie() {
    return nomCookie;
  }

  public void setNomCookie(String nomCookie) {
    this.nomCookie = nomCookie;
  }

  public Integer getAgeCookie() {
    return ageCookie;
  }

  public void setAgeCookie(Integer ageCookie) {
    this.ageCookie = ageCookie;
  }

  private String getParam(String cle) {
    return GestionParametreSysteme.getInstance().getParametreSystemeEJB(cle);
  }

  public String getDomaineCookie() {
    return domaineCookie;
  }

  public void setDomaineCookie(String domaineCookie) {
    this.domaineCookie = domaineCookie;
  }

  public ServiceUtilisateur getServiceUtilisateur() {
    return this.serviceUtilisateur;
  }

  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }

  /**
   * @return the serviceHistoriqueMotPasse
   */
  public ServiceHistoriqueMotPasse getServiceHistoriqueMotPasse() {
    return serviceHistoriqueMotPasse;
  }

  /**
   * @param serviceHistoriqueMotPasse the serviceHistoriqueMotPasse to set
   */
  public void setServiceHistoriqueMotPasse(
      ServiceHistoriqueMotPasse serviceHistoriqueMotPasse) {
    this.serviceHistoriqueMotPasse = serviceHistoriqueMotPasse;
  }

  public Boolean getSecureCookie() {
    return secureCookie;
  }

  public void setSecureCookie(Boolean secureCookie) {
    this.secureCookie = secureCookie;
  }
}
