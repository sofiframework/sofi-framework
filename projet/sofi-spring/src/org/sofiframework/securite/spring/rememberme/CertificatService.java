/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.spring.rememberme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.utilitaire.UtilitaireApache;
import org.sofiframework.securite.spring.context.ContexteAuthentification;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.RememberMeServices;

public class CertificatService implements RememberMeServices {

  private ContexteAuthentification contexte = null;

  private static final Log log = LogFactory.getLog(CertificatService.class);

  @Override
  public Authentication autoLogin(HttpServletRequest request, HttpServletResponse response) {
    String certificat = contexte.getCertificat(request, response);
    Authentication auth = null;
    if (certificat != null) {
      if (!contexte.certificatValide(certificat)) {
        contexte.nettoyerCertificat(request, response);
        if (log.isDebugEnabled()) {
          log.debug("CertificatService autoLogin : On doit nettoyer le certificat car il est invalide.");
        }
      } else {
        String codeUtilisateur = GestionSecurite.getInstance().getServiceAuthentification().getCodeUtilisateur(certificat);

        if (codeUtilisateur != null) {
          auth = new RememberMeAuthenticationToken(certificat, codeUtilisateur, null);

          // completer l'utilisateur
          try {
            contexte.ajouterUtilisateurDansSession(codeUtilisateur, request);
            request.getSession().setAttribute(contexte.getNomCookie(), certificat);
          } catch (Exception e) {
            if (log.isErrorEnabled()) {
              log.error("CertificatService autoLogin : Échec pour ajouter l'utilisateur dans la session.");
            }
            auth = null;
          }
        }

        if (log.isDebugEnabled()) {
          log.debug("CertificatService autoLogin : Certificat valide, login "  + (auth == null ? " échec " : "réussi"));
        }
      }
    }
    return auth;
  }

  @Override
  public void loginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentification) {

    Integer ageCookie = GestionParametreSysteme.getInstance().getInteger(ConstantesParametreSysteme.AUTHENTIFICATION_AGE_COOKIE);
   
    String rememberMe = request.getParameter("_spring_security_remember_me");

    if (!"on".equals(rememberMe)) {
      contexte.setAgeCookie(new Integer(-1));
    }else {
      contexte.setAgeCookie(ageCookie*60*60*24);
    }

    User user = (User) authentification.getPrincipal();
    String codeUtilisateur = user.getUsername();
    
    String ip = UtilitaireApache.getUserIp(request);
    String userAgent = UtilitaireApache.getUserAgent(request);
        
    String urlRetour = (String) request.getParameter("url_retour");
    
    String nouveauCertificat = GestionSecurite.getInstance().getServiceAuthentification().genererCertificat(codeUtilisateur,null, ip, userAgent, urlRetour );
    contexte.ajouterCookies(nouveauCertificat, request, response);
    request.getSession().setAttribute(contexte.getNomCookie(), nouveauCertificat);
    if (log.isDebugEnabled()) {
      log.debug("CertificatService loginSucces");
    }
  }

  @Override
  public void loginFail(HttpServletRequest request, HttpServletResponse response) {
    // Supprimer les cookies avec le certificat terminé
    contexte.nettoyerCertificat(request, response);
    if (log.isDebugEnabled()) {
      log.debug("CertificatService loginFail");
    }
  }

  public ContexteAuthentification getContexte() {
    return contexte;
  }

  public void setContexte(ContexteAuthentification contexte) {
    this.contexte = contexte;
  }
}
