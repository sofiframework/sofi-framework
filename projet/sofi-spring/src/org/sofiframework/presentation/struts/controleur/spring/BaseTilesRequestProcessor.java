/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.Modele;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.modele.spring.securite.constante.Constantes;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


/**
 * Processeur de requêtes qui utilise l'engin de gabarit Tiles et qui est adapté aux composants Spring de SOFI.
 */
public class BaseTilesRequestProcessor
extends org.sofiframework.presentation.struts.controleur.BaseTilesRequestProcessor {
  //surcharge
  @Override
  protected Object getModele(HttpServletRequest request,
      HttpServletResponse response) {
    Object modele = null;
    String nomModele = servlet.getServletConfig().getInitParameter("modele");

    if (nomModele != null) {
      WebApplicationContext contexte = WebApplicationContextUtils.getWebApplicationContext(servlet.getServletContext());
      modele = contexte.getBean(nomModele);
    }

    return modele;
  }

  /**
   * Récriture. Ajout de la gestion de la référence de l'utilisateur pour l'ajouter et le libérer du gestionnaire.
   * @throws javax.servlet.ServletException Exception Servlet
   * @throws java.io.IOException Exception d'entrée/sortie
   * @param response Réponse HTTP
   * @param request Requête HTTP
   */
  @Override
  public void process(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    this.placerUtilisateurDansGestionnaire(request, response);

    Modele modele = (Modele) getModele(request, response);

    if (modele != null) {
      modele.initialiserTransaction();
      modele.setGestionnaireUtilisateur(getGestionnaireUtilisateur());
    }

    try {
      super.process(request, response);
    } finally {
      if (modele != null) {
        modele.libererTransaction();
      }

      this.libererUtilisateurDuGestionnaire(request, response);
    }
  }

  /**
   * Place l'utilisateur dans le gestionnaire pour qu'il puisse être récupéré
   * plus loin dans l'exécution de la logique d'affaire.
   * @param response Réponse HTTP
   * @param request Requête HTTP
   */
  private void placerUtilisateurDansGestionnaire(HttpServletRequest request,
      HttpServletResponse response) {
    // Accéder à l'utilisateur en cours.
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
    getGestionnaireUtilisateur().setUtilisateur(utilisateur);
  }

  /**
   * Enleve l'utilisateur du gestionnaire.
   * @param response Réponse HTTP
   * @param request Requête HTTP
   */
  private void libererUtilisateurDuGestionnaire(HttpServletRequest request,
      HttpServletResponse response) {
    getGestionnaireUtilisateur().libererUtilisateur();
  }

  /**
   * Obtenir un bean du contexte Spring en cours
   * @return Instance du bean
   * @param nomBean Nom du bean que l'on désire obtenir
   */
  private Object getBean(String nomBean) {
    WebApplicationContext contexte = WebApplicationContextUtils.getWebApplicationContext(servlet.getServletContext());

    return contexte.getBean(nomBean);
  }

  /**
   * Obtenir le gestionnaire utilisateur
   * @return gestionnaire utilisateur
   */
  private GestionnaireUtilisateur getGestionnaireUtilisateur() {
    return (GestionnaireUtilisateur) getBean(Constantes.GESTIONNAIRE_UTILISATEUR);
  }
}
