package org.sofiframework.presentation.spring.filtre;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.util.WebUtils;

/**
 * Spécialisation du filtre de locale pour application Spring.
 * @author jfbrassard
 *
 */
public class FiltreLocale extends org.sofiframework.presentation.filtre.FiltreLocale {

  @Override
  public void fixerLocale(HttpServletRequest request, Locale locale) {
    if (locale != null) {
      WebUtils.setSessionAttribute(request,
          SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, locale);
      request.setAttribute("locale", locale.toString());
      request.getSession().setAttribute("language", locale.getLanguage());
      request.getSession().setAttribute("locale", locale.toString());
      LocaleContextHolder.setLocale(locale);
    }
  }



}
