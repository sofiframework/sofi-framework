package org.sofiframework.presentation.spring;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.stereotype.Component;

@Component("messageSource")
@DependsOn("configurationServices")
/**
 * Extension Spring pour communication avec le gestionnaire des libellés et messages.
 * @author jfbrassard
 * @see <code>GestionLibelle</code><GestionMessage</code>
 * @since 3.1
 */
public class MessageSource extends AbstractMessageSource implements InitializingBean {

  @Autowired
  private ServiceParametreSysteme serviceParametre;

  private Map<String, String> defaultMessageKeys = new HashMap<String, String>();

  private String applicationCode;

  private String[] commonApplicationCodes;

  public static final String ERROR_MSG_REQUIRED_FIELD = "infra_sofi.erreur.commun.champ.obligatoire";

  @Override
  public void afterPropertiesSet() throws Exception {
    this.defaultMessageKeys.put("typeMismatch.java.util.Date", "infra_sofi.erreur.commun.format.date");
    this.defaultMessageKeys.put("typeMismatch.java.lang.Integer", "infra_sofi.erreur.commun.format.numerique");
    this.defaultMessageKeys.put("NotEmpty.java.lang.String", ERROR_MSG_REQUIRED_FIELD);
    this.defaultMessageKeys.put("NotNull.java.lang.Integer", ERROR_MSG_REQUIRED_FIELD);
    this.defaultMessageKeys.put("creditCard.cardNumber", "infra_sofi.erreur.commun.champ.carteCredit.invalide");
    this.defaultMessageKeys.put("Email", "infra_sofi.erreur.commun.format.courriel");

    this.applicationCode = GestionSecurite.getInstance().getCodeApplication();
    final String value = serviceParametre.getValeur(this.applicationCode,
        ConstantesParametreSysteme.CODE_APPLICATION_COMMUN);
    if (value != null) {
      this.setCommonApplicationCodes(value.split(","));
    }
    // FKR DEBUG:
    setUseCodeAsDefaultMessage(true);

  }

  @Override
  protected MessageFormat resolveCode(final String code, final Locale locale) {
    MessageFormat messageFormat = null;
    final String codeLocale = locale.toString();

    if (isSofiManageLabel(code)) {

      final Message libelle = GestionLibelle.getInstance().get(code, codeLocale, (Object) null, null);

      if (libelle.getMessage().equals(code)) {
        final Message message = GestionMessage.getInstance().get(code, codeLocale, (Object) null, null);
        if (!message.getMessage().equals(code)) {

          messageFormat = message.getMessageFormat();
        }

      } else {
        messageFormat = libelle.getMessageFormat();
      }
    } else {
      if (code.startsWith("H:")) {
        messageFormat = new MessageFormat(code.substring(2));
      } else {
        final String sofiMessageKey = this.defaultMessageKeys.get(code);

        if (sofiMessageKey != null) {
          messageFormat = this.resolveCode(sofiMessageKey, locale);
        }

      }
    }

    return messageFormat;
  }

  private boolean isSofiManageLabel(final String code) {
    if (code.startsWith(this.applicationCode.toLowerCase().trim())) {
      return true;
    }

    for (final String commonAppplicationCode : this.commonApplicationCodes) {
      if (code.startsWith(commonAppplicationCode.toLowerCase().trim())) {
        return true;
      }
    }

    return false;
  }

  public String getMessage(final String code) {
    return getMessage(code, null);
  }

  public String getMessage(final String code, final Object[] args) {
    // Get locale use.
    final Locale locale = LocaleContextHolder.getLocale();
    final MessageFormat messageFormat = resolveCode(code, locale);
    String message = null;

    if (messageFormat != null) {
      synchronized (messageFormat) {
        message = messageFormat.format(args);
      }
    }

    return message;
  }

  public String getMessage(final String code, final Object[] args, final String localeParam) {
    // Get locale use.
    final Locale locale = GestionMessage.getInstance().getLocaleDisponible(localeParam);
    final MessageFormat messageFormat = resolveCode(code, locale);
    String message = null;

    if (messageFormat != null) {
      synchronized (messageFormat) {
        message = messageFormat.format(args);
      }
    }

    return message;
  }

  public Map<String, String> getDefaultMessageKeys() {
    return defaultMessageKeys;
  }

  public void setDefaultMessageKeys(final Map<String, String> defaultMessageKeys) {
    this.defaultMessageKeys = defaultMessageKeys;
  }

  public String getApplicationCode() {
    return applicationCode;
  }

  public void setApplicationCode(final String applicationCode) {
    this.applicationCode = applicationCode;
  }

  public ServiceParametreSysteme getServiceParametre() {
    return serviceParametre;
  }

  public void setServiceParametre(final ServiceParametreSysteme serviceParametre) {
    this.serviceParametre = serviceParametre;
  }

  public String[] getCommonApplicationCodes() {
    return commonApplicationCodes;
  }

  public void setCommonApplicationCodes(final String[] commonApplicationCodes) {
    this.commonApplicationCodes = commonApplicationCodes;
  }
}