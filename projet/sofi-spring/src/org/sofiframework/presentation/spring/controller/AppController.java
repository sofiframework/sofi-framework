package org.sofiframework.presentation.spring.controller;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.spring.cache.service.ServiceCache;
import org.sofiframework.modele.spring.cache.service.ServiceEhCacheImpl;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Port of "UtilitaireAction" for Spring Mvc.
 * 
 * Need to add a <context:component-scan
 * base-package="org.sofiframework.presentation.spring" /> in the Application
 * context( view-servlet.xml for instance) Note: don't forget to handle that Uri
 * in security if you use Spring Security
 * 
 * 
 */
@RequestMapping(value = "/app")
@Controller
public class AppController {

  private static final String OK_MESSAGE = "OK";

  /**
   * Indicateur d'initialisation de tous les libellés.
   */
  public static final String CACHE_LIBELLE = "LIBELLE";

  /**
   * Indicateur d'initialisation de tous les messages.
   */
  public static final String CACHE_MESSAGE = "MESSAGE";

  /**
   * Indicateur d'initialisation de tous les aides en ligne.
   */
  public static final String CACHE_AIDE = "AIDE";

  /**
   * Indicateur d'initialisation d'un domaine de valeur.
   */
  public static final String CACHE_DOMAINE_VALEUR = "DOMAINEVALEUR";

  /**
   * Need to be wired to be able to display ServiceCache instances.
   */
  @Autowired
  private ApplicationContext applicationContext;

  @RequestMapping(value = "/viderCache/{nom}/{usagerGestion}/{motPasse}")
  /**
   * Vider une cache. On doit passer le nom de la cache, l'utilisateur gestionnaire de l'application et le mot de passe de l'utilisateur.
   * 
   * @param response
   *          l'objet réponse http
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception
   *           En cas de problème
   */
  public ResponseEntity<String> viderCache(@PathVariable("nom") String nom,
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse) {
    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {

      final String nomCacheAVider = nom;

      if (UtilitaireString.isVide(nomCacheAVider)) {
        throw new SOFIException(
            "Vous devez spécifier le nom de la cache à vider");
      }

      retrieveAndValidateUser(usagerGestion, motPasse);

      boolean cacheTrouvee = false;

      // Vider la cache demandée
      if (nomCacheAVider.equals(CACHE_AIDE)) {
        GestionAide.getInstance().setIsViderCache(true);

        final Iterator<?> iterator = GestionCache.getInstance()
            .getCache("aideEnLigne").iterateurCles();

        while (iterator.hasNext()) {
          iterator.next();
          iterator.remove();
        }
        cacheTrouvee = true;

      }
      if (!cacheTrouvee && nomCacheAVider.equals(CACHE_LIBELLE)) {
        GestionLibelle.getInstance().initialiserCache();
        cacheTrouvee = true;
      }
      if (!cacheTrouvee && nomCacheAVider.equals(CACHE_MESSAGE)) {
        GestionMessage.getInstance().initialiserCache();
        cacheTrouvee = true;
      }
      if (!cacheTrouvee && nomCacheAVider.equals(CACHE_DOMAINE_VALEUR)) {
        GestionDomaineValeur.getInstance().setIsViderCache(true);
        cacheTrouvee = true;
      }
      if (!cacheTrouvee) {
        cacheTrouvee = videServiceCache(nomCacheAVider);
      }

      if (!cacheTrouvee) {
        throw new SOFIException(
            "La cache demandée n'est pas un nom valide de cache de SOFI.");
      }
    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Recharger un objet cache. On doit passer le nom de l'objet cache,
   * l'utilisateur gestionnaire de l'application et le mot de passe de
   * l'utilisateur.
   * 
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une
   *         exception
   */
  @RequestMapping(value = "/majObjetCache/{nom}/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> rechargerObjetCache(
      @PathVariable("nom") String nom,
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse) {
    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {

      // Obtention de la cache à recharger
      final String nomObjetCache = nom;

      if (UtilitaireString.isVide(nomObjetCache)) {
        throw new SOFIException(
            "Vous devez spécifier le nom de l'objet cache à recharger");
      }

      retrieveAndValidateUser(usagerGestion, motPasse);

      boolean cacheTrouvee = false;
      final GestionCache gestionCache = GestionCache.getInstance();

      if (gestionCache.isObjetCacheExiste(nomObjetCache)) {
        cacheTrouvee = true;
        gestionCache.rechargerCache(nomObjetCache);
      }
      if (!cacheTrouvee) {
        cacheTrouvee = videServiceCache(nomObjetCache);
      }
      if (!cacheTrouvee) {
        throw new SOFIException(
            "Cette cache n'existe pas et ne peut donc pas être rechargée.");
      }

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Recherche une cache de type ServiceCache dans le contexte spring, et la
   * vide si c'est le cas. Ne sait actuellement gerer que des Caches de type
   * ServiceEhCacheImpl car ServiceCache n'expose pas de methode pour vider
   * celle ci.
   * 
   * @param nomObjetCache
   *          nom de la cache ( == identifiant de bean spring )
   * @return true is la cache a été trouvée et vidée.
   * @throws SOFIException
   *           si on decele un probleme de recuperation / vidage de la cache.
   */
  protected boolean videServiceCache(final String nomObjetCache)
      throws SOFIException {
    boolean cacheTrouvee = false;
    // chercher le cache dans Spring
    if (applicationContext != null) {
      // il est necessaire d'exploiter BeanFactoryUtils pour chercher dans les
      // eventuels parent contexts
      final Map<String, ServiceCache> caches = BeanFactoryUtils
          .beansOfTypeIncludingAncestors(applicationContext, ServiceCache.class);
      if (caches != null && caches.containsKey(nomObjetCache)) {
        ServiceCache cache = caches.get(nomObjetCache);

        // les services de cache peuvent se cacher derriere un proxy.
        if (AopUtils.isJdkDynamicProxy(cache)) {
          try {
            cache = (ServiceCache) ((Advised) cache).getTargetSource()
                .getTarget();
          } catch (final Exception e) {
            throw new SOFIException("Probleme de recuperation de la cache");

          }
        }

        // TODO ServiceCache devrait probablement exposer une methode
        // d'invalidation
        if (cache instanceof ServiceEhCacheImpl) {

          // on ne considere que la cache a été trouvée que si on est capable de
          // la vider.
          cacheTrouvee = true;

          final ServiceEhCacheImpl ehCache = (ServiceEhCacheImpl) cache;
          ehCache.getCache().removeAll();
        }

      }
    }
    return cacheTrouvee;
  }

  /**
   * Mettre à jour un paramètre système. On doit passer le code du paramètre,
   * l'utilisateur gestionnaire de l'application et le mot de passe de
   * l'utilisateur.
   * 
   * @param code
   * @param usagerGestion
   * @param motPasse
   * @param codeClient
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une
   *         exception
   */
  @RequestMapping(value = "/majParametreSysteme/{code}/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> mettreAJourParametreSysteme(
      @PathVariable("code") String code,
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse,  
      @RequestParam(value = "codeClient", required = false) String codeClient) {
    
    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {

      // Obtention du paramètres à mettre à jour
      final String codeParametre = code;

      if (UtilitaireString.isVide(codeParametre)) {
        throw new SOFIException(
            "Vous devez spécifier le code du paramètre système a mettre à jour");
      }

      retrieveAndValidateUser(usagerGestion, motPasse);

      final GestionParametreSysteme gestionParametre = GestionParametreSysteme
          .getInstance();

      if (UtilitaireString.isVide(codeClient)) {
        if (gestionParametre.getParametreSysteme(codeParametre) == null) {
          throw new SOFIException("Ce paramètre système n'existe pas et ne peut donc pas être mis à jour.");
        } else {
          gestionParametre.supprimerParametreSysteme(codeParametre);

        } 
      }else {
        if (gestionParametre.getParametreSystemeClient(codeParametre, codeClient) == null) {
          throw new SOFIException("Ce paramètre système n'existe pas et ne peut donc pas être mis à jour.");
        } else {
          gestionParametre.supprimerParametreSysteme(codeParametre,codeClient);
        }
        
      }

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Mettre à jour un libellé. On doit passer le code du libellé, l'utilisateur
   * gestionnaire de l'application et le mot de passe de l'utilisateur.
   * 
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une
   *         exception
   */
  @RequestMapping(value = "/majLibelle/{cle}/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> mettreAJourLibelle(
      @PathVariable("cle") String cle,
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse,
      @RequestParam(value = "codeClient", required = false) String codeClient) {
    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {
      // Obtention de la clé du libellé à mettre à jour
      final String cleLibelle = cle;

      if (UtilitaireString.isVide(cleLibelle)) {
        throw new SOFIException(
            "Vous devez spécifier la clé du libellé a mettre à jour");
      }

      retrieveAndValidateUser(usagerGestion, motPasse);

      GestionLibelle.getInstance().supprimer(cleLibelle);
      if (UtilitaireString.isVide(codeClient)) {
        GestionLibelle.getInstance().supprimer(cleLibelle, codeClient);
      }

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Mettre à jour un message. On doit passer la clé du message, l'utilisateur
   * gestionnaire de l'application et le mot de passe de l'utilisateur.
   * 
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une
   *         exception
   * @throws java.lang.Exception
   *           En cas de problème
   */
  @RequestMapping(value = "/majMessage/{cle}/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> mettreAJourMessage(
      @PathVariable("cle") String cle,
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse,
      @RequestParam(value = "codeClient", required = false) String codeClient) {
    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {
      // Obtention de la clé du message à mettre à jour
      final String cleMessage = cle;

      if (UtilitaireString.isVide(cleMessage)) {
        throw new SOFIException(
            "Vous devez spécifier la clé du message a mettre à jour");
      }

      retrieveAndValidateUser(usagerGestion, motPasse);

      GestionMessage.getInstance().supprimer(cleMessage);
      
      if (UtilitaireString.isVide(codeClient)) {
        GestionMessage.getInstance().supprimer(cleMessage, codeClient);
      }      

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Obtenir la liste des objets caches configurés dans l'application. On doit
   * passer le code d'usager de gestion et le mot de passe de l'usager.
   * 
   * @param response
   *          l'objet réponse http
   * @param request
   *          l'objet requête http
   * @param form
   *          le formulaire (vide dans notre cas)
   * @param mapping
   *          le mapping struts
   * @return une réponse texte contenant la liste des caches séparés par des "|"
   *         en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception
   *           En cas de problème
   */
  @RequestMapping(value = "/majReferentiel/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> rechargerListeCaches(
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse) {

    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {

      GestionSecurite.getInstance().genererReferentiel(true);

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Obtenir la liste des objets caches configurés dans l'application. On doit
   * passer le code d'usager de gestion et le mot de passe de l'usager.
   * 
   * @param response
   *          l'objet réponse http
   * @param request
   *          l'objet requête http
   * @param form
   *          le formulaire (vide dans notre cas)
   * @param mapping
   *          le mapping struts
   * @return une réponse texte contenant la liste des caches séparés par des "|"
   *         en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception
   *           En cas de problème
   */
  @RequestMapping(value = "/getListeCaches/{usagerGestion}/{motPasse}")
  public ResponseEntity<String> obtenirListeCaches(
      @PathVariable("usagerGestion") String usagerGestion,
      @PathVariable("motPasse") String motPasse) {

    String reply = OK_MESSAGE;
    HttpStatus status = HttpStatus.OK;
    try {

      reply = getListeCaches(usagerGestion, motPasse);

    } catch (final SOFIException sofiEx) {
      reply = sofiEx.getMessage();
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply, status);
  }

  /**
   * Retourne une liste d'objet cache séparé d'un |.
   * @param usagerGestion
   * @param motPasse
   * @return
   */
  private String getListeCaches(String usagerGestion, String motPasse) {
    String reply;
    retrieveAndValidateUser(usagerGestion, motPasse);

    //
    final StringBuilder reponse = new StringBuilder("");
    if (applicationContext != null) {
      // il est necessaire d'exploiter BeanFactoryUtils pour chercher dans les
      // eventuels parent contexts
      final String[] nomsServiceCache = BeanFactoryUtils
          .beanNamesForTypeIncludingAncestors(applicationContext,
              ServiceCache.class);
      if (nomsServiceCache != null) {
        for (final String nomServiceCache : nomsServiceCache) {
          if (reponse.length() > 0) {
            reponse.append('|');
          }

          reponse.append(nomServiceCache);
        }
      }
    }

    // expose les caches internes.
    GestionCache gestionCache = GestionCache.getInstance();
    if (gestionCache != null
        && gestionCache.getListeCachesConfigurees() != null) {

      final Iterator<?> iterateurCaches = gestionCache
          .getListeCachesConfigurees().iterator();

      while (iterateurCaches.hasNext()) {
        final String nomCache = (String) iterateurCaches.next();

        if (reponse.length() > 0) {
          reponse.append('|');
        }

        reponse.append(nomCache);

      }

    }

    reply = reponse.toString();
    if (reponse.length() == 0) {
      throw new SOFIException(
          "Le gestionnaire de la cache n'est pas activé dans l'application.");
    }
    return reply;
  }

  /**
   * Méthode qui retourne le statut de l'application
   * 
   * @param request
   *          l'objet requête http
   * @return une réponse texte, "Date de lancement" en cas de réussite, sinon
   *         lance une exception
   */
  @RequestMapping(value = "/info")
  public ResponseEntity<String> info(final HttpServletRequest request) {

    StringBuffer reply = new StringBuffer("FAIL : ");
    HttpStatus status = HttpStatus.OK;
    try {
      if (GestionSurveillance.getInstance().getDateDepartApplication() != null) {
        reply = new StringBuffer("system:");
        reply.append(GestionSecurite.getInstance().getCodeApplication());
        reply.append(";systemStartDate:");
        reply.append(GestionSurveillance.getInstance()
            .getDateDepartApplication());
        reply.append(";cacheUpdateDate:");
        reply.append(GestionCache.gestionCache.getDateDerniereMaj());

      }

    } catch (final SOFIException sofiEx) {
      reply.append(sofiEx.getMessage());
      status = HttpStatus.FORBIDDEN;

    }
    return new ResponseEntity<String>(reply.toString(), status);
  }

  /**
   * Retrieve and validate User credentials from Request Parameters
   * 
   * @param request
   *          current request.
   */
  private void retrieveAndValidateUser(String usagerGestion, String motPasse) {

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    // Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);
  }

  /**
   * Valider le code d'utilisateur et le mot de passe de gestion avec le service
   * application
   * 
   * @param admUsrCode
   *          le code de l'utilisateur de gestion
   * @param motPasse
   *          le mot de passe de l'utilisateur de gestion
   * @return vrai si la validation a réussie, sinon lance une SOFIException
   * @throws java.lang.Exception
   *           En cas de problème de validation
   */
  private boolean validerCodeUtilisateurMotPasseAdmin(final String admUsrCode,
      final String motPasse) throws SOFIException {
    // Obtenir l'application via le service application
    final ServiceApplication appSrv = GestionSecurite.getInstance()
        .getServiceApplicationLocal();

    if (appSrv == null) {
      throw new SOFIException("Le service application n'est pas disponible.");
    }

    final Application application = appSrv.getApplication(GestionSecurite
        .getInstance().getCodeApplication());

    if (application == null) {
      throw new SOFIException(
          "Le système n'est pas défini dans la console de gestion SOFI.");
    } else {
      if (admUsrCode.equals(application.getCodeUtilisateurAdmin())
          && motPasse.equals(application.getMotPasseAdmin())) {
        return true;
      } else {
        throw new SOFIException(
            "Le code d'utilisateur et le mot de passe administrateur de l'application sont invalides.");
      }
    }
  }
}
