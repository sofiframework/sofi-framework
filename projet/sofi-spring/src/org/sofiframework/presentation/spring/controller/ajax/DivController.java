package org.sofiframework.presentation.spring.controller.ajax;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.controleur.RequestProcessorHelper;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping(value = "/div")
@Controller
public class DivController {

  @RequestMapping(method = RequestMethod.GET, value = "/setDivState/{divId}/{ouvert}")
  public ResponseEntity<String> setDivState(
      @PathVariable("divId") String divId,
      @PathVariable("ouvert") String ouvert, final HttpServletRequest request) {

    String reply = "OK";
    HttpStatus status = HttpStatus.OK;


    if (divId != null && ouvert != null) {
      RequestProcessorHelper.setEtatPersistanceDiv(divId, Boolean.valueOf(ouvert),
          request);
    }else {
      reply = "FAIL";
      status = HttpStatus.FORBIDDEN;
    }
    return new ResponseEntity<String>(reply, status);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/getDivState/{divId}")
  public ResponseEntity<String> getDivState(
      @PathVariable("divId") String divId,
      final HttpServletRequest request) {

    String reply = null;
    HttpStatus status = HttpStatus.OK;

    if (divId != null) {
      if (UtilitaireControleur.isDivOuvert(divId, request)) {
        reply = "true";
      }else {
        reply = "false";
      }
    }else {
      reply = "FAIL";
      status = HttpStatus.FORBIDDEN;
    }
    return new ResponseEntity<String>(reply, status);
  }

}
