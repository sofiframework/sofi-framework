/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;

import ro.isdc.wro.http.WroFilter;

/**
 * Filtre Wro configurable. Utiliser le paramètre système WRO_DEBUG (true/false) 
 * pour activer le mode développeur.
 */
public class FiltreWro extends WroFilter {

  private static final String WRO_DEBUG = "WRO_DEBUG";

  @Override
  protected void doInit(FilterConfig config) throws ServletException {
    super.doInit(config);
    Boolean debug = GestionParametreSysteme.getInstance().getBoolean(WRO_DEBUG);
    if (debug != null && debug) {
      this.getConfiguration().setDebug(true);
      this.getConfiguration().setDisableCache(true);
    }
  }
}
