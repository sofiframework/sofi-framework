/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * Filtre qui permet de forcer un codage de caractère spécifique
 * pour le contenu des requêtes HTTP.
 * 
 * Étant du filtre "CharacterEncodingFilter" de Spring.
 * 
 *  Exemple d'utilisation dans le web.xml :
 *    <!-- Filtre -->
 *    <filter>
 *      <filter-name>FiltreCaractere</filter-name>
 *      <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
 *      <init-param>
 *        <param-name>encoding</param-name>
 *        <param-value>ISO-8859-1</param-value>
 *      </init-param>
 *      <init-param>
 *        <param-name>forceEncoding</param-name>
 *        <param-value>true</param-value>
 *      </init-param>
 *    </filter>
 * 
 *    <!-- Mapping -->
 *    <filter-mapping>
 *      <filter-name>FiltreCaractere</filter-name>
 *      <url-pattern>*.do</url-pattern>
 *    </filter-mapping>
 * 
 * @author Jean-Maxime Pelletier
 * @since SOFI 2.0.4
 */
public class FiltreCodageCaractere extends CharacterEncodingFilter {
  public FiltreCodageCaractere() {
    super();
  }
}
