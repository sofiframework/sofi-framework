/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.conf;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.sofiframework.exception.SOFIException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Permet de résoudre les valeurs de paramètres à partir d'un contexte Jndi.
 * @author Jean-Maxime Pelletier
 */
public class ConfigurationJndi extends PropertyPlaceholderConfigurer implements InitializingBean {

  private Context contexte = null;
  private String nomContexte;

  @Override
  public void afterPropertiesSet() throws Exception {
    contexte = new InitialContext();
    if (nomContexte != null) {
      contexte = (Context) contexte.lookup(nomContexte);
    }
  }

  @Override
  protected String resolvePlaceholder(String jeton, Properties proprietes) {
    try {
      Object valeur = contexte.lookup(jeton);
      return valeur != null ? valeur.toString() : null;
    } catch (NamingException e) {
      throw new SOFIException("Erreur pour obtenir la valeur " + jeton + " dans le contexte jndi.", e);
    }
  }


  public void setNomContexte(String nomContexte) {
    this.nomContexte = nomContexte;
  }


  public String getNomContexte() {
    return nomContexte;
  }
}