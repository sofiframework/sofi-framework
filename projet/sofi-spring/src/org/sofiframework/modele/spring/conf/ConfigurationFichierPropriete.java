/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.conf;

import java.util.Properties;
import java.util.ResourceBundle;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Permet de remplacer des jetons de style Ant à partir
 * d'un fichier de resource (PropertyResourceBundle).
 */
public class ConfigurationFichierPropriete extends PropertyPlaceholderConfigurer implements InitializingBean {

  private String nomResource = null;

  private ResourceBundle resource = null;

  @Override
  public void afterPropertiesSet() throws Exception {
    this.resource = ResourceBundle.getBundle(nomResource);
  }

  @Override
  protected String resolvePlaceholder(String jeton, Properties proprietes) {
    return this.resource.getString(jeton);
  }

  public void setNomResource(String nomResource) {
    this.nomResource = nomResource;
  }

  public String getNomResource() {
    return nomResource;
  }
}