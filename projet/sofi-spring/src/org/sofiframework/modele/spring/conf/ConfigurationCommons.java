/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.conf;

import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Résoud les propriétés dynamique de la configuration
 * Spring à l'aide d'une configuration commons-configuration.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ConfigurationCommons extends PropertyPlaceholderConfigurer implements InitializingBean {

  /**
   * Instance qui implémente l'interface Configuration de commons-configuration
   */
  private Configuration configuration = null;

  /**
   * Traitement lors du chargement du contexte.
   * @throws Exception Erreur survenu lors du chargement
   */
  @Override
  public void afterPropertiesSet() throws Exception {
    if (configuration == null) {
      throw new RuntimeException("Vous devez vous déclarer un objet " +
          "CommonsConfigurationFactoryBean dans le contexte Spring " +
          "et l'injecter à l'objet ConfigurationCommons par la " +
          "propriété configuration.");
    }
  }

  /**
   * Obtenir la valeur d'une propriété dynamique de la configuration Spring.
   * @param jeton Nom de la propriété
   * @param proprietes
   * @return valeur obtenu à parti de l'objet Configuration
   */
  @Override
  protected String resolvePlaceholder(String jeton, Properties proprietes) {
    return configuration.getString(jeton);
  }

  public Configuration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(Configuration configuration) {
    this.configuration = configuration;
  }
}
