/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.conf;

import java.io.File;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.FactoryBean;


/**
 * Construit une instance qui implémente l'interface Configuration de commons-configuration.
 * La configuration doit être initialisée à l'aide d'un fichier xml contenant
 * les sources de paramètres.
 * 
 * @author Jean-Maxime Pelletier
 */
public class CommonsConfigurationFactoryBean implements FactoryBean {

  /**
   * Instance de journalisation.
   */
  private static final Log log = LogFactory.getLog(CommonsConfigurationFactoryBean.class);

  private String nomPropriete = "sofi.configuration";

  private String basePath;

  private String nomFichierConfiguration = null;

  /**
   * Obtenir une instance de Configuration.
   * @return Configuration
   * @throws Exception Erreur survevnu lors de la
   * construction de la configuration
   */
  @Override
  public Object getObject() throws Exception {
    String nomFichierSysteme = System.getProperty(nomPropriete);

    /*
     * Si l'argument de vm n'est pas présent on utilise la propriété
     * nomFichierConfiguration directement injectée par le fichier
     * de context Spring.
     * 
     * Si ni un ni l'autre ne sont présent on lance une
     * exception runtime.
     */
    if (nomFichierSysteme == null || nomFichierSysteme.trim().equals("")) {
      if(nomFichierConfiguration == null || nomFichierConfiguration.trim().equals("")) {
        throw new RuntimeException("Vous devez ajouter un argument de vm avec le nom suivant : " + nomPropriete);
      }
    } else {
      nomFichierConfiguration = nomFichierSysteme;
    }
    String nomComplet = nomFichierConfiguration;

    if (getBasePath() != null && nomFichierConfiguration.indexOf("..") != -1 ) {
      // Traitement lorsque le fichier de configuration est relatif au projet.
      StringBuffer nomRelative = new StringBuffer();
      nomRelative.append(getBasePath());
      if (getBasePath().indexOf("\\") != -1 && !nomFichierConfiguration.substring(0,1).equals("\\")) {
        nomRelative.append("\\");
      }
      nomRelative.append(nomFichierConfiguration);
      nomComplet = nomRelative.toString();
    }

    ConfigurationFactory factory = new ConfigurationFactory(nomComplet);

    if (log.isWarnEnabled()) {
      String pathCourant = (new File("./")).getAbsolutePath();
      log.warn("Emplacement du fichier de configuration : " + nomComplet + ". Le path courant est " + pathCourant);
    }

    return factory.getConfiguration();
  }

  @Override
  public Class getObjectType() {
    return Configuration.class;
  }

  @Override
  public boolean isSingleton() {
    return true;
  }

  public String getNomPropriete() {
    return nomPropriete;
  }

  public void setNomPropriete(String nomPropriete) {
    this.nomPropriete = nomPropriete;
  }

  public String getNomFichierConfiguration() {
    return nomFichierConfiguration;
  }

  public void setNomFichierConfiguration(String nomFichierConfiguration) {
    this.nomFichierConfiguration = nomFichierConfiguration;
  }

  public String getBasePath() {
    return basePath;
  }

  public void setBasePath(String baseRealPath) {
    this.basePath = baseRealPath;
  }
}
