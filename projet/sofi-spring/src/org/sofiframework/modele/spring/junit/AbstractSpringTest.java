/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.junit;

import junit.framework.TestCase;

import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Classe de base pour un cas de test utilisant
 * un contexte Spring.
 * 
 * @author Jean-Maxime Pelletier (Nurun)
 * @author Guillaume Poirier
 */
public abstract class AbstractSpringTest extends TestCase {

  /**
   * Contexte Spring
   */
  private ClassPathXmlApplicationContext contexte = null;

  public AbstractSpringTest(String name) {
    super(name);
  }

  /**
   * Contruit le contexte Spring
   */
  @Override
  protected void setUp() throws Exception {
    String[] configurationSpring =
        new String[getConfigurationsSpring().length + 1];

    if (!UtilitaireString.contient(getConfigurationsSpring(),
        "/org/sofiframework/modele/spring/conf/sofi.xml")) {

      configurationSpring[0] = "/org/sofiframework/modele/spring/conf/sofi.xml";
      for (int i = 0; i < getConfigurationsSpring().length; i++) {
        int y = i + 1;
        configurationSpring[y] = getConfigurationsSpring()[i];
      }

    }
    contexte = new ClassPathXmlApplicationContext(configurationSpring);
    configurer();
  }

  @Override
  protected void tearDown() throws Exception {
    contexte.destroy();
    contexte = null;
    detruire();
  }

  /**
   * Méthode à implémenter par les sous-classe afin de faire l'initialisation
   * pour chaque test.
   */
  protected void configurer() throws Exception {

  }

  /**
   * Méthode à implémenter par les sous-classe afin de faire le nettoyage
   * après chaque test.
   */
  protected void detruire() throws Exception {

  }

  /**
   * Obtenir les fichiers de configuration
   * Spring utilisés
   * 
   * @return tableau de chemin d'accès aux fichier XML Spring
   */
  protected abstract String[] getConfigurationsSpring();

  /**
   * Obtenir le contexte Spring
   * @return Contexte d'application SPring
   */
  protected ApplicationContext getContexte() {
    return this.contexte;
  }

  /**
   * Retourne un bean dans le contexte
   * @param nom le nom du bean à retourner
   * @return un bean dans le contexte
   */
  protected Object getBean(String nom) {
    return getContexte().getBean(nom);
  }
}
