/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.application;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.support.WebApplicationObjectSupport;

/**
 * Configuration des composants Web des gestionnaires.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ConfigurationWeb extends WebApplicationObjectSupport implements InitializingBean {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(ConfigurationWeb.class);

  private String[] fichiersParametreSysteme;
  
  private String codeApplication;

  public ConfigurationWeb() {
  }

  @Override
  public void afterPropertiesSet() throws Exception {

    if (log.isInfoEnabled()) {
      log.info("Chargement du bean Spring pour la configuration Web ...");
    }
    
    if (StringUtils.isNotEmpty(codeApplication)) {
    	GestionSecurite.getInstance().setCodeApplication(codeApplication);
    }

    GestionParametreSysteme gestionParametre = GestionParametreSysteme.getInstance();
    String cheminReelApp = UtilitaireRequest.getEmplacementWebInfReel(getServletContext());
    gestionParametre.setEmplacementWebInf(cheminReelApp);
    for (int i = 0; i < fichiersParametreSysteme.length; i++)  {
      String fichier = UtilitaireRequest.getEmplacementReel(getServletContext(),fichiersParametreSysteme[i] );
      try {
        gestionParametre.chargerFichierParametres(fichier);
      } catch (Exception e) {
    	 throw new SOFIException("Problème de chargement des fichiers de parametre systeme, assurez-vous d'avoir specifier un code application.");
      }
    }

    /*
     * Le fichier configuration message doit être initialisé dans le gestionnaire de messages.
     */
    GestionMessage.getInstance().initialiser();

    // Activer la gestion de menu
    getServletContext().setAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU, new RepertoireMenu());
  }

  public void setFichiersParametreSysteme(String[] fichiersParametreSysteme) {
    this.fichiersParametreSysteme = fichiersParametreSysteme;
  }

  public String getRealPath() {
    return getServletContext().getRealPath("");
  }

public String getCodeApplication() {
	return codeApplication;
}

public void setCodeApplication(String codeApplication) {
	this.codeApplication = codeApplication;
}
}
