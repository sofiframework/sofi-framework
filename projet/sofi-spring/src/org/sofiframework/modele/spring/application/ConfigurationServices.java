/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.application;

import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.application.journalisation.GestionJournalisation;
import org.sofiframework.application.journalisation.service.ServiceJournalisation;
import org.sofiframework.application.locale.objetstransfert.LocaleApplication;
import org.sofiframework.application.locale.service.ServiceLocalisation;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.application.rapport.GestionRapport;
import org.sofiframework.application.rapport.service.ServiceRapport;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.application.tacheplanifiee.GestionTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireLocale;
import org.springframework.beans.factory.InitializingBean;

/**
 * Configuration des gesitonnaires avec les services de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ConfigurationServices implements InitializingBean {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(ConfigurationServices.class);

  private ServiceParametreSysteme serviceParametreSysteme;

  private ServiceLibelle serviceLibelle;

  private ServiceMessage serviceMessage;

  private ServiceSecurite serviceSecurite;

  private ServiceAide serviceAide;

  private ServiceObjetSecurisable serviceReferentielObjetJava;

  private ServiceAuthentification serviceAuthentification;

  private ServiceDomaineValeur serviceDomaineValeur;

  private ServiceApplication serviceApplication;

  private ServiceClient serviceClient;

  private ServiceJournalisation serviceJournalisation;

  private ServiceUtilisateur serviceUtilisateur;

  private ServiceTachePlanifiee serviceTachePlanifiee;

  private ServiceLocalisation serviceLocalisation;

  private ServiceRapport serviceRapport;

  private String codeApplication;

  private String codeFacette;

  private Integer seqObjetSecurisablePourPageAccueil;

  private boolean pageAccueilInclusDansMenu;

  private boolean autorisationPositive = true;

  private String urlAide;

  private String gabaritVelocityAide;

  private HashSet<String> mapLangue;

  private String langueParDefaut;

  @Override
  public void afterPropertiesSet() throws Exception {

    try {
      if (log.isInfoEnabled()) {
        log.info("Chargement du bean Spring pour la configuration des services de l'infrastructure SOFI ...");
      }
      this.initialiserGestionParametres();
      this.initialiserGestionTachePlanifiee();
      this.initialiserGestionSecurite();
      this.initialiserLangueSupportee();
      this.initiliserGestionLibelle();
      this.initiliserGestionMessage();
      this.initialiserGestionAide();
      this.initialiserGestionDomaineValeur();
      this.initialiserServiceApplication();
      this.initialiserServiceJournalisation();
      this.initialiserServiceUtilisateur();
      this.initialiserServiceLocalisation();
      this.initialiserGestionRapport();

      GestionSurveillance.getInstance().setDateDepartApplication();
    } catch (Exception e) {
      throw new SOFIException(e);
    }
  }

  /**
   * Initialise le singleton {@link GestionRapport} avec le service de rapport (si et seulement si il est différent de
   * null).
   */
  private void initialiserGestionRapport() {
    if (this.serviceRapport != null) {
      GestionRapport.getInstance().setServiceRapport(this.serviceRapport);
    }
  }

  private void initialiserServiceLocalisation() {
    if (this.serviceLocalisation != null) {
      GestionServiceDistant.ajouterServiceDistant("serviceLocalisation", this.serviceLocalisation);
    }
  }

  private void initialiserGestionTachePlanifiee() {
    GestionTachePlanifiee gestionTachePlanifiee = GestionTachePlanifiee.getInstance();
    gestionTachePlanifiee.setService(serviceTachePlanifiee);
  }

  /**
   * Initialiser les langues supportées dans l'application et la langue par defaut.
   */
  public void initialiserLangueSupportee() {
	  
    if (getLangueParDefaut() == null) {
	  setLangueParDefaut("fr_CA");
	}	  

    // Rechercher les langues offertes
    this.mapLangue = new HashSet<String>();

    Application application = GestionSecurite.getInstance().getApplication();

    if (application != null && application.getListeLocales() != null) {
      Iterator<LocaleApplication> iterateur = application.getListeLocales().iterator();
      while (iterateur.hasNext()) {
        LocaleApplication localeApplication = iterateur.next();
        if (localeApplication.isDefaut()) {
          this.langueParDefaut = localeApplication.getCodeLocale();
          GestionLibelle.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(langueParDefaut));
          GestionMessage.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(langueParDefaut));
        }
        mapLangue.add(localeApplication.getCodeLocale());
      }
    } 
    
    if (mapLangue.size() == 0) {
      // Support de la langue par défaut.
      mapLangue.add(getLangueParDefaut());
      GestionLibelle.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(getLangueParDefaut()));
      GestionMessage.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(getLangueParDefaut()));
    }
  }

  public void initialiserGestionParametres() {
    GestionParametreSysteme gestionParametre = GestionParametreSysteme.getInstance();
    gestionParametre.setService(serviceParametreSysteme);
  }

  private void initialiserGestionSecurite() {
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    gestionSecurite.setServiceReferentielLocal(serviceReferentielObjetJava);
    gestionSecurite.setServiceSecuriteLocal(this.serviceSecurite);
    gestionSecurite.setServiceAuthentification(this.serviceAuthentification);
    gestionSecurite.setServiceClient(this.serviceClient);
    gestionSecurite.setCodeApplication(codeApplication);
    gestionSecurite.setCodeFacette(this.codeFacette);

    if (getSeqObjetSecurisablePourPageAccueil() != null) {
      gestionSecurite.setPageAccueilDefaut(getSeqObjetSecurisablePourPageAccueil());
    }

    gestionSecurite.setPageAccueilInclusDansMenu(isPageAccueilInclusDansMenu());
    gestionSecurite.setObjetSecurisableActif(false);
    gestionSecurite.setAutorisationPositive(this.autorisationPositive);

    GestionSecurite.genererReferentiel();
  }

  private void initiliserGestionLibelle() {
    GestionLibelle.getInstance().setServiceLibelle(serviceLibelle);
    GestionLibelle.getInstance().setApplication(this.codeApplication);
    GestionLibelle.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(this.langueParDefaut));
    GestionLibelle.getInstance().setListeLocale(this.mapLangue);
  }

  private void initiliserGestionMessage() {
    GestionMessage.getInstance().setServiceMessage(serviceMessage);
    GestionMessage.getInstance().setApplication(this.codeApplication);
    GestionLibelle.getInstance().setLocaleParDefaut(UtilitaireLocale.getLocale(this.langueParDefaut));
    GestionMessage.getInstance().setListeLocale(this.mapLangue);
  }

  private void initialiserGestionAide() {
    GestionAide gestionAide = GestionAide.getInstance();
    // Fixer la date et l'heure de configuration du gestionnaire de l'aide en ligne.
    gestionAide.setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
    gestionAide.setGabaritAide(this.gabaritVelocityAide);
    gestionAide.setUrlAide(this.urlAide);
    gestionAide.setServiceAide(this.serviceAide);
  }

  private void initialiserGestionDomaineValeur() {
    GestionDomaineValeur gestionDomaineValeur = GestionDomaineValeur.getInstance();
    gestionDomaineValeur.setServiceDomaineValeur(this.serviceDomaineValeur);
    gestionDomaineValeur.setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
  }

  private void initialiserServiceApplication() {
    if (this.serviceApplication != null) {
      GestionSecurite.getInstance().setServiceApplicationLocal(this.serviceApplication);
    }
  }

  private void initialiserServiceJournalisation() {
    if (this.serviceJournalisation != null) {
      GestionJournalisation.getInstance().setServiceJournalisation(this.serviceJournalisation);
    }
  }

  private void initialiserServiceUtilisateur() {
    if (this.serviceUtilisateur != null) {
      GestionSecurite.getInstance().setServiceUtilisateur(this.serviceUtilisateur);
    }
  }

  public void setServiceParametreSysteme(ServiceParametreSysteme serviceParametreSysteme) {
    this.serviceParametreSysteme = serviceParametreSysteme;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  public void setServiceMessage(ServiceMessage serviceMessage) {
    this.serviceMessage = serviceMessage;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public boolean isPageAccueilInclusDansMenu() {
    return pageAccueilInclusDansMenu;
  }

  public void setPageAccueilInclusDansMenu(boolean pageAccueilInclusDansMenu) {
    this.pageAccueilInclusDansMenu = pageAccueilInclusDansMenu;
  }

  public Integer getSeqObjetSecurisablePourPageAccueil() {
    return seqObjetSecurisablePourPageAccueil;
  }

  public void setSeqObjetSecurisablePourPageAccueil(Integer seqObjetSecurisablePourPageAccueil) {
    this.seqObjetSecurisablePourPageAccueil = seqObjetSecurisablePourPageAccueil;
  }

  public void setAutorisationPositive(boolean autorisationPositive) {
    this.autorisationPositive = autorisationPositive;
  }

  public boolean isAutorisationPositive() {
    return autorisationPositive;
  }

  public void setServiceSecurite(ServiceSecurite serviceSecurite) {
    this.serviceSecurite = serviceSecurite;
  }

  public ServiceSecurite getServiceSecurite() {
    return serviceSecurite;
  }

  public void setServiceAide(ServiceAide serviceAide) {
    this.serviceAide = serviceAide;
  }

  public ServiceAide getServiceAide() {
    return serviceAide;
  }

  public void setUrlAide(String urlAide) {
    this.urlAide = urlAide;
  }

  public String getUrlAide() {
    return urlAide;
  }

  public void setGabaritVelocityAide(String gabaritVelocityAide) {
    this.gabaritVelocityAide = gabaritVelocityAide;
  }

  public String getGabaritVelocityAide() {
    return gabaritVelocityAide;
  }

  public void setServiceReferentielObjetJava(ServiceObjetSecurisable serviceReferentielObjetJava) {
    this.serviceReferentielObjetJava = serviceReferentielObjetJava;
  }

  public ServiceObjetSecurisable getServiceReferentielObjetJava() {
    return serviceReferentielObjetJava;
  }

  public void setServiceAuthentification(ServiceAuthentification serviceAuthentification) {
    this.serviceAuthentification = serviceAuthentification;
  }

  public ServiceAuthentification getServiceAuthentification() {
    return serviceAuthentification;
  }

  public ServiceDomaineValeur getServiceDomaineValeur() {
    return serviceDomaineValeur;
  }

  public void setServiceDomaineValeur(ServiceDomaineValeur serviceDomaineValeur) {
    this.serviceDomaineValeur = serviceDomaineValeur;
  }

  public ServiceApplication getServiceApplication() {
    return serviceApplication;
  }

  public void setServiceApplication(ServiceApplication serviceApplication) {
    this.serviceApplication = serviceApplication;
  }

  public ServiceJournalisation getServiceJournalisation() {
    return serviceJournalisation;
  }

  public void setServiceJournalisation(ServiceJournalisation serviceJournalisation) {
    this.serviceJournalisation = serviceJournalisation;
  }

  public ServiceUtilisateur getServiceUtilisateur() {
    return serviceUtilisateur;
  }

  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }

  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  public String getCodeFacette() {
    return codeFacette;
  }

  public ServiceTachePlanifiee getServiceTachePlanifiee() {
    return serviceTachePlanifiee;
  }

  public void setServiceTachePlanifiee(ServiceTachePlanifiee serviceTachePlanifiee) {
    this.serviceTachePlanifiee = serviceTachePlanifiee;
  }

  public String getLangueParDefaut() {
    return langueParDefaut;
  }

  public void setLangueParDefaut(String langueParDefaut) {
    this.langueParDefaut = langueParDefaut;
  }

  public void setServiceLocalisation(ServiceLocalisation serviceLocalisation) {
    this.serviceLocalisation = serviceLocalisation;
  }

  public ServiceLocalisation getServiceLocalisation() {
    return serviceLocalisation;
  }

  public ServiceRapport getServiceRapport() {
    return serviceRapport;
  }

  public void setServiceRapport(ServiceRapport serviceRapport) {
    this.serviceRapport = serviceRapport;
  }

  public ServiceClient getServiceClient() {
    return serviceClient;
  }

  public void setServiceClient(ServiceClient serviceClient) {
    this.serviceClient = serviceClient;
  }

}
