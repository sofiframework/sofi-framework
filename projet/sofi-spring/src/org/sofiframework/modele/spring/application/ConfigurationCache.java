/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.application;

import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.springframework.beans.factory.InitializingBean;

/**
 * Configure le gestionnaire des objets cache.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ConfigurationCache implements InitializingBean {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(ConfigurationCache.class);

  /**
   * Fichier de configuration des caches.
   * 
   * Exemple : org/sofiframework/exemple/modele/conf/objet-cache.xml
   */
  private String fichier = null;

  private Object modele = null;

  private boolean chargementSurDemande = false;

  /*
   * (non-Javadoc)
   * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
   */
  @Override
  public void afterPropertiesSet() throws Exception {

    if (log.isInfoEnabled()) {
      log.info("Chargement du bean Spring pour la gestion de la cache...");
    }
    initialiserGestionCache();
  }

  /**
   * Initialise le gestionnaire de cache
   * @throws Exception Erreur survenu lors de l'initialisation
   */
  private void initialiserGestionCache() throws Exception {
    InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(fichier);
    GestionCache gestionCache = GestionCache.getInstance();
    gestionCache.setChargementSurDemande(chargementSurDemande);
    gestionCache.chargerDocumentXml(in);
    if (this.modele == null) {
      gestionCache.construire();
    } else {
      gestionCache.construire(this.modele);
    }
    gestionCache.setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
  }

  public String getFichier() {
    return fichier;
  }

  public void setFichier(String fichier) {
    this.fichier = fichier;
  }

  public Object getModele() {
    return modele;
  }

  public void setModele(Object modele) {
    this.modele = modele;
  }

  public boolean isChargementSurDemande() {
    return chargementSurDemande;
  }

  public void setChargementSurDemande(boolean chargementSurDemande) {
    this.chargementSurDemande = chargementSurDemande;
  }
}
