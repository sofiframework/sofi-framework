package org.sofiframework.modele.spring.tacheplanifiee;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.tacheplanifiee.GestionTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.JobStoreImpl;
import org.sofiframework.application.tacheplanifiee.SemaphoreImpl;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.modele.exception.OrdonnanceurNotFoundException;
import org.springframework.scheduling.SchedulingException;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

public class OrdonnanceurFactoryBean extends SchedulerFactoryBean {

  private static final Log log = LogFactory.getLog(OrdonnanceurFactoryBean.class);

  private Properties ordonnanceurProperties;
  private String codeScheduler;
  private Long schedulerId;
  private ServiceTachePlanifiee serviceTachePlanifie;
  private String actif;

  public OrdonnanceurFactoryBean(){
    super();
    serviceTachePlanifie = GestionTachePlanifiee.getInstance().getService();
  }

  /**
   * Surchargé pour injecter le schedulerId dans l'objet de type {@link JobStoreImpl}
   * et dans l'objet de type {@link SemaphoreImpl} utiles au fonctionnement de l'ordonnanceur
   */
  @Override
  public void afterPropertiesSet() throws Exception  {
    String codeApplication = GestionSecurite.getInstance().getCodeApplication();

    try {
      // on doit recupere le schedulerId
      schedulerId = serviceTachePlanifie.getSchedulerId(codeApplication,codeScheduler);
      ordonnanceurProperties.put("org.quartz.jobStore.schedulerId", String.valueOf(schedulerId));
      ordonnanceurProperties.put("org.quartz.jobStore.lockHandler.schedulerId", String.valueOf(schedulerId));

      this.setQuartzProperties(ordonnanceurProperties);
      super.afterPropertiesSet();
    } catch (OrdonnanceurNotFoundException exception) {
      if(log.isWarnEnabled()) {
        log.warn("Pas d'ordonnanceur actif pour l'application " + codeApplication + ".");
      }
    }
  }

  @Override
  public void start() throws SchedulingException {
    if(this.schedulerId != null && isSchedulerActive()) {
      String nomHote = this.getNomHoteLocal() + "/" + this.getSchedulerInstanceId();
      String codeApplication = GestionSecurite.getInstance().getCodeApplication();
      // On vérifie le statut en BD de l'ordonnanceur ; cela ne l'empêchera pas de démarrer mais affichera
      // un message dans les logs s'il y a lieux
      Boolean isStatutDejaDemarre = this.getServiceTachePlanifie().verifierOrdonnanceurStatutDemarre(schedulerId, nomHote);
      if(isStatutDejaDemarre && log.isWarnEnabled()) {
        StringBuffer message = new StringBuffer("L'ordonnanceur [").append(codeApplication)
            .append(", ").append(codeScheduler).append("] a déjà le statut \"Démarré\" avant d'être lancé : ")
            .append(" soit il ne s'est pas arrêté correctement, soit le mode cluster est activé.");
        log.warn(message);
      }
      super.start();
      this.getServiceTachePlanifie().signalerOrdonnanceurDemarre(schedulerId, nomHote);
    } else {
      if(log.isWarnEnabled() && isSchedulerActive()) {
        log.warn("Pas de schedulerId, l'ordonnanceur ne peut pas être démarré.");
      }else {
        log.warn("L'ordonnancer n'est pas actif.");
      }
    }
  }

  @Override
  public void stop() throws SchedulingException {
    if(this.schedulerId != null) {
      super.stop();
      String nomHote = this.getNomHoteLocal() + "/" + this.getSchedulerInstanceId();
      this.getServiceTachePlanifie().signalerOrdonnanceurArrete(schedulerId, nomHote);
    }
  }

  public Properties getOrdonnanceurProperties() {
    return ordonnanceurProperties;
  }

  public void setOrdonnanceurProperties(Properties ordonnanceurProperties) {
    this.ordonnanceurProperties = ordonnanceurProperties;
  }

  public String getCodeScheduler() {
    return codeScheduler;
  }

  public void setCodeScheduler(String codeScheduler) {
    this.codeScheduler = codeScheduler;
  }

  public ServiceTachePlanifiee getServiceTachePlanifie() {
    return serviceTachePlanifie;
  }

  public void setServiceTachePlanifie(ServiceTachePlanifiee serviceTachePlanifie) {
    this.serviceTachePlanifie = serviceTachePlanifie;
  }

  /**
   * Permet d'obtenir le nom de l'hôte où l'application roule
   * @return
   */
  private String getNomHoteLocal() {
    String nomHote = null;
    try {
      nomHote = InetAddress.getLocalHost().toString();
    } catch (UnknownHostException e) {
      if(log.isWarnEnabled()) {
        log.warn("Impossible d'obtenir le nom de l'hôte où est hébergé l'ordonnanceur");
      }
    }

    return nomHote;
  }

  /**
   * Permet d'obtenir l'instanceId de l'ordonnanceur
   * @return
   */
  private String getSchedulerInstanceId() {
    String instanceId = null;
    try {
      instanceId = this.getScheduler().getSchedulerInstanceId();
    } catch (SchedulerException e) {
      if(log.isWarnEnabled()) {
        log.warn("Impossible d'obtenir l'instanceId de l'ordonnanceur");
      }
    }
    return instanceId;
  }

  /**
   * @return the actif
   */
  public String getActif() {
    return actif;
  }

  /**
   * @param actif the actif to set
   */
  public void setActif(String actif) {
    this.actif = actif;
  }

  /**
   * Est-ce que que l'ordonnanceur est actif ou pas.
   * @return
   */
  public boolean isSchedulerActive() {
    if ((getActif() == null || StringUtils.isEmpty(getActif())) || "true".equals(getActif())) {
      return true;
    }else {
      return false;
    }
  }

  @Override
  protected void startScheduler(Scheduler scheduler, int startupDelay)
      throws SchedulerException {
    if (isSchedulerActive()) {
      if(log.isInfoEnabled()) {
        log.info("L'ordonnanceur est lancé..");
      }
      super.startScheduler(scheduler, startupDelay);
    }else {
      if(log.isInfoEnabled()) {
        log.info("L'ordonnanceur n'est pas actif ..");
      }

    }
  }



}
