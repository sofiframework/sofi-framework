/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.intercepteur;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Intercepte les exceptions inatendus et journalise les erreurs à l'aide d'une
 * instance de Log.
 * 
 * @author Jean-Maxime Pelletier
 */
public class LogExceptionIntercepteur extends BaseInterceptor {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory
      .getLog(LogExceptionIntercepteur.class);

  /**
   * Appelé lors de l'appel d'une méthode de la classe interceptée.
   * 
   * @param pjp
   *          Décrit l'invocation de la méthode en cours de traitement
   */
  public Object log(ProceedingJoinPoint pjp) throws Throwable {
    Object objet = null;
    try {
      objet = pjp.proceed();
    } catch (Throwable e) {
      if (log.isInfoEnabled()) {
        log.error("RuntimeException ", e);
      }
      throw e;
    }
    return objet;
  }
}