/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.intercepteur;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.sofiframework.modele.spring.cache.service.ServiceCache;

/**
 * Intercepte les appels a une classe pour cacher les réponses selon la
 * signature de la méthode et les valeurs de parametres passés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class CacheInterceptor extends BaseInterceptor {

  private static final Log log = LogFactory.getLog(CacheInterceptor.class);

  private ServiceCache serviceCache;

  @Override
  public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
    String id = this.getIdentifiantAppel(pjp);
    Object retour = null;
    boolean enCache = false;

    if (this.serviceCache.isEnCache(id)) {
      enCache = true;
      retour = this.serviceCache.getValeur(id);
    } else {
      retour = pjp.proceed();
      this.serviceCache.ajouter(id, retour);
    }

    if (log.isInfoEnabled()) {
      log.info(MessageFormat.format("Appel {0} {1}", new Object[] { id,
          (enCache ? " Cache " : " NoCache ") }));
    }

    return retour;
  }

  public ServiceCache getServiceCache() {
    return serviceCache;
  }

  public void setServiceCache(ServiceCache serviceCache) {
    this.serviceCache = serviceCache;
  }

}
