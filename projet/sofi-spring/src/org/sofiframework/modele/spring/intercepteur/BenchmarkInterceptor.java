/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.intercepteur;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Permet de mesurer les temps d'exécution des méthodes.
 * 
 * @author Jean-Maxime Pelletier
 */
public class BenchmarkInterceptor extends BaseInterceptor {

  private static final Log log = LogFactory.getLog(BenchmarkInterceptor.class);

  @Override
  public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
    long debut = System.currentTimeMillis();
    String nomMethode = getIdentifiantAppel(pjp);

    if (log.isInfoEnabled()) {
      log.info("invoke " + nomMethode + " ...");
    }

    Object retour = pjp.proceed();

    if (log.isInfoEnabled()) {
      Long time = (System.currentTimeMillis() - debut);
      log.info(MessageFormat.format("{1} ms T {0}",
          new Object[] {
          nomMethode, time
      }));
    }

    return retour;
  }
}
