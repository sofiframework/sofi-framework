/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.intercepteur;

import java.text.MessageFormat;

import org.aspectj.lang.ProceedingJoinPoint;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * Classe de base pour des intercepteur Spring.
 * 
 * @author Jean-Maxime Pelletier
 */
abstract public class BaseInterceptor {

  /**
   * Méthode par défaut pour appliquer un traitement AOP.
   * 
   * @param pjp
   * @return retour de l'appel de méthode
   * @throws Throwable
   */
  public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
    return pjp.proceed();
  }

  /**
   * Obtenir l'identifiant unique de l'appel de la méthode.
   * 
   * @param pjp
   *          invocation d'une méthode
   */
  protected String getIdentifiantAppel(ProceedingJoinPoint pjp) {
    String signature = pjp.getSignature().toLongString();
    Object[] arguments = pjp.getArgs();
    StringBuffer valeurArguments = new StringBuffer();

    for (int i = 0; i < arguments.length; i++) {
      Object argument = arguments[i];
      valeurArguments.append("[");
      if (argument != null) {
        if (argument instanceof ObjetTransfert) {
          String valeurCle = this.getValeurCle((ObjetTransfert) argument);
          valeurArguments.append(valeurCle);
        } else {
          valeurArguments.append(argument.toString());
        }
      } else {
        valeurArguments.append("null");
      }
      valeurArguments.append("]");
    }

    String identifiantAppel = MessageFormat.format(
        "signature = [{0}] arguments = [{1}]",
        new Object[] {
            signature, valeurArguments.toString()
        });

    return identifiantAppel;
  }

  /**
   * Obtenir la valeur en string de la clé d'un objet de transfert.
   * 
   * @param objet
   *          Objet de transfert
   * @return valeur de la clé
   */
  protected String getValeurCle(ObjetTransfert objet) {
    StringBuffer cle = new StringBuffer();
    String[] attributsCle = objet.getCle();

    if (attributsCle != null) {
      for (int i = 0; i < attributsCle.length; i++) {
        String attributCle = attributsCle[i];
        cle.append(UtilitaireObjet.getValeurAttribut(objet, attributCle));
      }
    }

    return cle.toString();
  }
}