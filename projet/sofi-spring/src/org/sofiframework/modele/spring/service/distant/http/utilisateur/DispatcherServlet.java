/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.distant.http.utilisateur;

import java.security.Key;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Classe surchargée du Dispatcher de Spring pour prendre en charge un header
 * additionnel contenant le code d'utilisateur
 * 
 * <p>
 * Le code est encrypter et doit être décrypter à l'aide d'une clé secrete.
 * 
 * @author Sergio Couture
 */
public class DispatcherServlet extends org.springframework.web.servlet.DispatcherServlet {

  private static final long serialVersionUID = -5743785920246633904L;

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(DispatcherServlet.class);

  private static final String NOM_PARAMETRE = "audit_utilisateur";
  private static final String CLE = "ceci_est_une_cle_secret";

  /**
   * Surcharge la méthode doDispatch
   * 
   * @param request
   * @param response
   */
  @Override
  protected void doDispatch(HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    GestionnaireUtilisateur gestionnaireUtilisateur = getGestionnaireUtilisateur(request);
    gestionnaireUtilisateur.setUtilisateur(null);
    String encryptCodeUtilisateur = request.getHeader(NOM_PARAMETRE);

    if (!UtilitaireString.isVide(encryptCodeUtilisateur)) {
      if (log.isDebugEnabled()) {
        log.debug("Reception d'un paramètre");
      }

      Key key = UtilitaireEncryption.generateKey("DES", CLE);
      String codeUtilisateur = UtilitaireEncryption.decrypt(key, encryptCodeUtilisateur);
      Utilisateur utilisateur = new Utilisateur();
      utilisateur.setCodeUtilisateur(codeUtilisateur);
      gestionnaireUtilisateur.setUtilisateur(utilisateur);

      if (log.isDebugEnabled()) {
        log.debug("Paramètre reçu et injecté.");
      }
    } else {
      if (log.isDebugEnabled()) {
        log.debug("Requête exécutée sans paramètre additionné");
      }
    }

    super.doDispatch(request, response);
  }

  private GestionnaireUtilisateur getGestionnaireUtilisateur(HttpServletRequest request) {
    ApplicationContext context = WebApplicationContextUtils
        .getWebApplicationContext(request.getSession().getServletContext());
    GestionnaireUtilisateur gestionnaireUtilisateur = (GestionnaireUtilisateur) context.getBean("gestionnaireUtilisateur");
    return gestionnaireUtilisateur;
  }
}
