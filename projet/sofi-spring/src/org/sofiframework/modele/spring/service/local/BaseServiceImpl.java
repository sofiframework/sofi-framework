/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.local;

import java.util.Date;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.entite.Tracable;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.modele.spring.service.BaseService;

/**
 * Classe ancêtre de Service local
 * @author Jean-maxime Pelletier
 */
public class BaseServiceImpl implements BaseService {

  /**
   * Accès à l'utilisateur en cours
   */
  private GestionnaireUtilisateur gestionnaireUtilisateur = null;

  /**
   * Permet d'obtenir l'utilisateur en cours.
   * 
   * @return Utilisateur
   */
  public Utilisateur getUtilisateur() {
    return this.gestionnaireUtilisateur.getUtilisateur();
  }

  /**
   * Fixe l'utilisateur en cours
   * 
   * @param gestionnaireUtilisateur Gestionnaire des utilisateurs
   */
  public void setGestionnaireUtilisateur(GestionnaireUtilisateur gestionnaireUtilisateur) {
    this.gestionnaireUtilisateur = gestionnaireUtilisateur;
  }

  /**
   * Obtenir le gestionnaire des utilisateurs.
   * 
   * @return Gesitonnaire des utilisateurs
   */
  public GestionnaireUtilisateur getGestionnaireUtilisateur() {
    return gestionnaireUtilisateur;
  }

  /**
   * Obtenir l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * 
   * @return
   */
  public Utilisateur getUtilisateurEnCours() {
    return this.getGestionnaireUtilisateur().getUtilisateur();
  }


  /**
   * Obtenir le code l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * 
   * @return
   */
  public String getCodeUtilisateurEnCours() {
    return this.getGestionnaireUtilisateur().getCodeUtilisateur();
  }

  /**
   * Mise à jour des propriétés de
   * trace de modification de l'entité.
   * 
   * @param tracable Entité traçable
   */
  protected void traceModification(Tracable tracable) {
    Utilisateur u = this.getUtilisateur();
    if (u != null) {
      tracable.setModifiePar(u.getCodeUtilisateur());
    }
    tracable.setDateModification(new Date());
  }

  /**
   * Mise à jour des propriétés de trace de
   * création de l'entité.
   * 
   * @param tracable Entité traçable
   */
  protected void traceCreation(Tracable tracable) {
    Utilisateur u = this.getUtilisateur();
    if (u != null) {
      tracable.setCreePar(u.getCodeUtilisateur());
    }
    tracable.setDateCreation(new Date());
  }
}