/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.distant;

import javax.naming.NamingException;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.springframework.ejb.access.SimpleRemoteStatelessSessionProxyFactoryBean;

public class ServiceEJBClient extends SimpleRemoteStatelessSessionProxyFactoryBean  {

  private static Log log = LogFactory.getLog(ServiceEJBClient.class);

  /**
   * Exécuté suivant l'initilisation des propriétés.
   * @throws javax.naming.NamingException Erreur jndi survenu lors du traitement
   */
  @Override
  public void afterPropertiesSet() throws NamingException {
    super.afterPropertiesSet();
    GestionServiceDistant.ajouterServiceDistant(
        this.getJndiName(), this.getObject());
  }

  /**
   * On ajoute des traces pour savoir en tout temps la performance du service.
   */
  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    long debut = 0L;
    if (log.isInfoEnabled()) {
      debut = System.currentTimeMillis();
    }

    Object retour = super.invoke(invocation);

    if (log.isInfoEnabled()) {
      log.info("Temps appel Ejb : " + ((System.currentTimeMillis() - debut)/1000.000F) + " sec.");
    }

    return retour;
  }
}