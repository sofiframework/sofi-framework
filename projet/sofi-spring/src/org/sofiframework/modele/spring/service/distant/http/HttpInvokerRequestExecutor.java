/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.distant.http;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.remoting.httpinvoker.CommonsHttpInvokerRequestExecutor;

public class HttpInvokerRequestExecutor extends CommonsHttpInvokerRequestExecutor implements InitializingBean {

  private String proxyHost = null;

  private String proxyPort = null;

  @Override
  public void afterPropertiesSet() throws Exception {
    HttpClient client = new HttpClient();

    HttpConnectionManager manager = new org.apache.commons.httpclient.MultiThreadedHttpConnectionManager();
    client.setHttpConnectionManager(manager);

    if (proxyHost != null && proxyPort != null) {
      System.setProperty("http.proxyHost", proxyHost);
      System.setProperty("http.proxyPort", proxyPort);
      client.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
    }

    this.setHttpClient(client);
  }

  public String getProxyHost() {
    return proxyHost;
  }


  public void setProxyHost(String proxyHost) {
    this.proxyHost = proxyHost;
  }


  public String getProxyPort() {
    return proxyPort;
  }


  public void setProxyPort(String proxyPort) {
    this.proxyPort = proxyPort;
  }
}
