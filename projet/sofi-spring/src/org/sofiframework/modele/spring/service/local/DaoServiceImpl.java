/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.local;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.modele.spring.dao.BaseDao;
import org.sofiframework.modele.spring.service.DaoService;

/**
 * Classe de service qui utilise un accèes de données de base.
 * 
 * @author Jean-Maxime Pelletier
 */
public class DaoServiceImpl extends BaseServiceImpl implements DaoService {

  /**
   * Accès de données qui est utilisé pour les méthodes génériques CRUD
   */
  private BaseDao dao = null;

  @Override
  public Object get(Serializable id) {
    return this.dao.get(id);
  }

  @Override
  public Object charger(Serializable id) {
    return this.dao.charger(id);
  }

  @Override
  public Serializable ajouter(Object entite) {
    return this.dao.ajouter(entite);
  }

  @Override
  public void modifier(Object entite) {
    this.dao.modifier(entite);
  }

  @Override
  public void supprimer(Serializable id) {
    this.dao.supprimer(id);
  }

  @Override
  public List getListe() {
    return this.dao.getListe();
  }

  @Override
  public ListeNavigation getListe(ListeNavigation liste) {
    return this.dao.getListe(liste);
  }

  @Override
  public List getListe(Filtre filtre) {
    return this.dao.getListe(filtre);
  }

  @Override
  public List getListe(Filtre filtre, Tri tri) {
    return this.dao.getListe(filtre, tri);
  }

  /**
   * Obtenir l'accès de données utilisé par ce service.
   * 
   * @return Accées de données
   */
  public BaseDao getDao() {
    return this.dao;
  }

  /**
   * Fixer l'accès de données utilisé par ce service.
   * 
   * @param dao
   *          Accès de données
   */
  public void setDao(BaseDao dao) {
    this.dao = dao;
  }
}