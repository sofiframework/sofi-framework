/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service.distant.http.utilisateur;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.securite.encryption.EncryptionException;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.springframework.remoting.httpinvoker.HttpInvokerClientConfiguration;

/**
 * Invoker HTTP surchargé pour envoyer une information additionnel dans le
 * header.
 * <p>
 * Le HTTPInvokerRequestExecutor doit être configuré pour avoir le gestionnaireUtilisateur
 * comme propriété dans sa définition de bean (service_distant_http.xml)
 * @author Sergio Couture
 */
public class HttpInvokerRequestExecutor
extends org.sofiframework.modele.spring.service.distant.http.HttpInvokerRequestExecutor {

  //L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(HttpInvokerRequestExecutor.class);

  private static final String NOM_PARAMETRE = "audit_utilisateur";

  private static final String CLE = "ceci_est_une_cle_secret";

  private GestionnaireUtilisateur gestionnaireUtilisateur;

  public GestionnaireUtilisateur getGestionnaireUtilisateur() {
    return gestionnaireUtilisateur;
  }

  public void setGestionnaireUtilisateur(
      GestionnaireUtilisateur gestionnaireUtilisateur) {
    this.gestionnaireUtilisateur = gestionnaireUtilisateur;
  }

  /**
   * Méthode surchargé pour permettre l'ajout d'un paramètre dans le header
   * @param config
   * @param postMethod
   * @param baos
   */
  @Override
  protected void setRequestBody(HttpInvokerClientConfiguration config,
      PostMethod postMethod, ByteArrayOutputStream baos) throws IOException {

    if (log.isDebugEnabled()) {
      log.debug("Ajout du code utilisateur dans la requête");
    }
    Utilisateur utilisateur = this.getGestionnaireUtilisateur().getUtilisateur();

    if (utilisateur != null) {
      String codeUtilisateur = utilisateur.getCodeUtilisateur();
      String encryptCodeUtilisateur;

      try {
        Key key = UtilitaireEncryption.generateKey("DES", CLE);
        encryptCodeUtilisateur = UtilitaireEncryption.encrypt(key, codeUtilisateur);
      } catch (EncryptionException e) {
        throw new SOFIException("Impossible de générer une communication sécurisé pour le code utilisateur. " +
            "Voir HttpInvokerRequestExecutor.",e.getCause());
      }

      // Ajout le code Utilisateur dans le header
      postMethod.addRequestHeader(NOM_PARAMETRE,
          encryptCodeUtilisateur);
    }
    super.setRequestBody(config, postMethod, baos);
  }

}
