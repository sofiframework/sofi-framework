/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.service;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;

/**
 * Interface de Service qui utilise les méthodes d'un dao.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface DaoService extends BaseService {

  /**
   * Obtenir un objet de l'accès de données "dao" associé à ce service.
   * 
   * @param id
   *          clé unique de l'entité
   * @return Entité
   */
  public Object get(Serializable id);

  /**
   * Obtenir une entité à l,aide de son identifiant unique. Si l'entité n'est
   * pas trouvé la méthode lance une exception.
   * 
   * @return Entité correspondant à l'identifiant
   * @param id
   *          Identifiant
   */
  public Object charger(Serializable id);

  /**
   * Ajouter un objet par l'accès de données "dao" associé à ce service
   * 
   * @param entite
   *          Objet qui sera ajouté
   * @return Identifiant unique de l'entité
   */
  public Serializable ajouter(Object entite);

  /**
   * Modifier un objet de l'accès de données "dao" associé à ce service.
   * 
   * @param entite
   *          Objet à modifier
   */
  public void modifier(Object entite);

  /**
   * Supprimer un objet de l'accès de données "dao" associé à ce service.
   * 
   * @param id
   *          clé unique de l'objet à supprimer
   */
  public void supprimer(Serializable id);

  /**
   * Obtenir la liste des objet accessiblpar l'accès de données
   * 
   * @return Liste de toutes les entités de l'accès de données "dao" associé à
   *         ce service
   */
  public List getListe();

  /**
   * Obtenir une liste paginée de tous les objets accessibles par l'accès de
   * données "dao"
   */
  public ListeNavigation getListe(ListeNavigation liste);

  /**
   * Obtenir la liste des entités qui correspondent à un filtre.
   */
  public List getListe(Filtre filtre);

  /**
   * Obtenir la liste des entités qui correspondent à un filtre selon l'ordre
   * spécifié par l'objet Tri.
   */
  public List getListe(Filtre filtre, Tri tri);
}