/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring;
import java.sql.Connection;

import org.sofiframework.modele.Modele;
import org.sofiframework.modele.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Classe de base de modèle. Implémente l'interface de modèle de SOFI qui
 * décrit la gestion des commit et rollback de la transaction.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ModeleImpl extends org.sofiframework.modele.spring.service.local.BaseServiceImpl implements Modele, Service {

  /**
   * Définition de la stratégie de gestion de la transaction.
   */
  private DefaultTransactionDefinition definition = new DefaultTransactionDefinition();

  /**
   * Gestionnaire de transaction en cours (injecté par Spring)
   */
  private PlatformTransactionManager gestionnaireTransaction = null;

  private ThreadLocal statut = new ThreadLocal() {};

  /**
   * Constructeur par défaut
   */
  public ModeleImpl() {
    super();
    // Join la transaction si il en existe une, si aucune existe en crée une.
    definition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
  }

  /**
   * Conserve une transaction pour le thread en cours.
   */
  @Override
  public void initialiserTransaction() {
    if (this.gestionnaireTransaction != null) {
      TransactionStatus nouvelleTransaction = this.gestionnaireTransaction.getTransaction(this.definition);
      this.statut.set(nouvelleTransaction);
    }
  }

  // Implentation de l'interface Modele

  @Override
  public void commit() {
    if (this.gestionnaireTransaction != null) {
      this.gestionnaireTransaction.commit(getTransactionEnCours());
      this.libererTransaction();
    }
  }

  @Override
  public void rollback() {
    if (this.gestionnaireTransaction != null) {
      this.gestionnaireTransaction.rollback(getTransactionEnCours());
      this.libererTransaction();
    }
  }

  /**
   * @todo A Implémenter
   * @return la connexion de la base de données courante.
   */
  @Override
  public Connection getConnexionBD() {
    return null;
  }

  // Fin de l'implementatnion de l'interface Modele

  @Override
  public void libererTransaction() {
    this.statut.set(null);
  }

  public void setGestionnaireTransaction(PlatformTransactionManager gestionnaireTransaction) {
    this.gestionnaireTransaction = gestionnaireTransaction;
  }

  public PlatformTransactionManager getGestionnaireTransaction() {
    return gestionnaireTransaction;
  }

  private TransactionStatus getTransactionEnCours() {
    return (TransactionStatus) this.statut.get();
  }
}