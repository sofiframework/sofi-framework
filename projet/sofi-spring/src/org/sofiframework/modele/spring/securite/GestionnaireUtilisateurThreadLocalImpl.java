/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.securite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;

/**
 * Implémentation du gestionnaire utilisateur qui utilise le TreadLocal pour conserver
 * l'utilisateur en cours.
 * @author Jean-Maxime Pelletier
 */
public class GestionnaireUtilisateurThreadLocalImpl implements GestionnaireUtilisateur {

  /**
   * Conserve l'utilisateur dans le contexte du Thread en cours
   */
  private ThreadLocal contenantUtilisateur = new ThreadLocal(){};

  /**
   * Fixe l'utilisateur pour qu'il puisse être récupéré plus tard.
   * @param utilisateur Objet utilisateur en cours
   */
  @Override
  public void setUtilisateur(Utilisateur utilisateur) {
    this.contenantUtilisateur.set(utilisateur);
  }

  /**
   * Obtenir l'utilisateur en cours.
   * @return Objet Utilisateur en cours
   */
  @Override
  public Utilisateur getUtilisateur() {
    return (Utilisateur) this.contenantUtilisateur.get();
  }

  /**
   * Libère les ressources occupés par le maintient de la référence à l'utilisateur en cours.
   */
  @Override
  public void libererUtilisateur() {
    this.contenantUtilisateur.set(null);
  }

  /**
   * Obtenir le code de l'utilisateur en cours.
   */
  @Override
  public String getCodeUtilisateur() {
    Utilisateur utilisateur = this.getUtilisateur();
    return utilisateur != null ? utilisateur.getCodeUtilisateur() : null;
  }

}