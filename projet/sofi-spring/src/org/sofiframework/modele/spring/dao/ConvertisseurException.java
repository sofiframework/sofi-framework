/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import org.sofiframework.modele.exception.AccesRessourceException;
import org.sofiframework.modele.exception.ConcurrenceException;
import org.sofiframework.modele.exception.DonneeInvalideException;
import org.sofiframework.modele.exception.EchecNettoyageDonneeException;
import org.sofiframework.modele.exception.IntegriteDonneeException;
import org.sofiframework.modele.exception.InterblocageException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;
import org.sofiframework.modele.exception.UsageApiException;
import org.sofiframework.modele.exception.VerrouillageOptimisteException;
import org.sofiframework.modele.exception.VerrouillagePessimisteException;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.CannotSerializeTransactionException;
import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DeadlockLoserDataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.dao.TypeMismatchDataAccessException;
import org.springframework.dao.UncategorizedDataAccessException;

/**
 * Convertie les exceptions de Spring (dao) vers des exceptions
 * personnalisées du canevas SOFI.
 * @author Jean-Maxime Pelletier
 */
public class ConvertisseurException {

  /**
   * On converti tous les types d'exception Spring vers un type SOFI
   * @param e Exception à traiter
   */
  public ModeleException convertirExceptionSpring(DataAccessException e) {
    ModeleException nouvelleException = null;

    if (e instanceof OptimisticLockingFailureException) {
      nouvelleException = new VerrouillageOptimisteException(e.getMessage(), e);

    } else if (e instanceof DataRetrievalFailureException) {
      nouvelleException = new ObjetTransfertNotFoundException(e.getMessage(), e);

      // Enfants de DataRetrievalFailureException
      if (e instanceof EmptyResultDataAccessException) {
      } else if (e instanceof IncorrectResultSizeDataAccessException) {
      } else {
      }
    } else if (e instanceof InvalidDataAccessResourceUsageException) {
      nouvelleException = new DonneeInvalideException(e.getMessage(), e);

      // Enfants de InvalidDataAccessResourceUsageException
      if (e instanceof IncorrectUpdateSemanticsDataAccessException) {
      } else if (e instanceof TypeMismatchDataAccessException) {
      } else {
      }
    } else if (e instanceof CleanupFailureDataAccessException) {
      nouvelleException = new EchecNettoyageDonneeException(e.getMessage(), e);

    } else if (e instanceof DataAccessResourceFailureException) {
      nouvelleException = new AccesRessourceException(e.getMessage(), e);

    } else if (e instanceof InvalidDataAccessApiUsageException) {
      nouvelleException = new UsageApiException(e.getMessage(), e);

    } else  if (e instanceof DataIntegrityViolationException) {
      nouvelleException = new IntegriteDonneeException(e.getMessage(), e);

    } else if (e instanceof UncategorizedDataAccessException) {
      // Va être ModeleException

    } else if (e instanceof ConcurrencyFailureException) {
      nouvelleException = new ConcurrenceException(e.getMessage(), e);

      if (e instanceof PessimisticLockingFailureException) {
        // Enfants de PessimisticLockingFailureException
        if (e instanceof CannotSerializeTransactionException) {
        } else if (e instanceof CannotAcquireLockException) {
        } else if (e instanceof DeadlockLoserDataAccessException) {
          nouvelleException = new InterblocageException(e.getMessage(), e);
        }

        if (nouvelleException == null)  {
          nouvelleException = new VerrouillagePessimisteException(e.getMessage(), e);
        }
      }
    } else if (e instanceof PermissionDeniedDataAccessException) {

      // Exception de base Spring dao

    } else {
      // Va être ModeleException
    }

    /* Si une exception n'est pas déjà créé, on utilise
     * ModeleException
     */
    if (nouvelleException == null) {
      nouvelleException = new ModeleException(e.getMessage(), e);
    }

    return nouvelleException;
  }
}