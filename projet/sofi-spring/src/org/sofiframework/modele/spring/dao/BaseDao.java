/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;

/**
 * Interface de base d'un accès de données.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface BaseDao {

  /**
   * Obtenir une entité à l'aide de son identifiant unique.
   * 
   * @param id
   *          Identifiant
   * @return Entité correspondant à l'identifiant
   */
  Object get(Serializable id);

  /**
   * Déterminer si une entité existe à partir de sa clé.
   * Si la cléest composée, on peut passer l'entité elle
   * même.
   * 
   * @param id Identifiant unique de l'entité
   * @return true si l'entité existe, false si non
   */
  boolean isExiste(Serializable id);

  /**
   * Obtenir une entité à l,aide de son identifiant unique. Si l'entité n'est
   * pas trouvé la méthode lance une exception.
   * 
   * @return Entité correspondant à l'identifiant
   * @param id
   *          Identifiant
   */
  Object charger(Serializable id);

  /**
   * Ajouter une nouvelle entité dans la base de données.
   * 
   * @param objet
   *          Nouvelle entité
   * @return Identifiant géré pour la nouvelle entité
   */
  Serializable ajouter(Object objet);

  /**
   * Applique les modification apportées à l'entité dans la base de données.
   * 
   * @param entite
   *          Entité à modifier
   */
  void modifier(Object entite);

  /**
   * Supprime l'entité qui correspond à l'identifiant spécifié.
   * 
   * @param id
   *          Identifiant de l'entité à supprimer
   */
  void supprimer(Serializable id);

  /**
   * Obtenir la liste des toutes les entités de l'accès de données.
   * 
   * @return Liste d'entités
   */
  List getListe();

  /**
   * Obtenir une liste à partir des critères d'un filtre.
   * 
   * @param filtre
   *          Filtre
   * @return Liste des entitées correspondant aux critères du filtre
   */
  List getListe(Filtre filtre);

  /**
   * Obtenir une liste à partir des critères d'un filtre.
   * 
   * @param filtre
   *          Filtre
   * @param tri Objet spécifiant le tri de la liste
   * @return Liste des entitées correspondant aux critères du filtre
   */
  List getListe(Filtre filtre, Tri tri);

  /**
   * Obtenir la liste des entités qui correspondent à la page décrite par la
   * liste navigation.
   * 
   * @param liste
   *          Liste de navigation
   * @return Liste de navigation
   */
  ListeNavigation getListe(ListeNavigation liste);
  
  
  /**
   * Permet de détacher une entité de la session.
   * @param entity
   */
  void evict(Object entity);

}
