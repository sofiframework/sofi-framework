/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.sql;

import java.io.IOException;
import java.io.InputStream;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Implémentation de <code>FactoryBean</code> pour rendre disponible le
 * <code>DataSource</code> d'HSQLDB dans un contexte Spring.
 * 
 * @author Guillaume Poirier
 * @author Jean-Maxime Pelletier
 */
public class HsqldbFactoryBean extends HsqldbDataSource
implements FactoryBean, InitializingBean, DisposableBean {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(HsqldbFactoryBean.class);

  /** La référence au script SQL d'initilisation d'HSQLDB */
  private String resource;

  @Override
  public Object getObject() throws Exception {
    return this;
  }

  @Override
  public Class getObjectType() {
    return DataSource.class;
  }

  /**
   * Est-ce que spring doit gérer l'instance de
   * cette classe comme un singleton.
   */
  @Override
  public boolean isSingleton() {
    return true;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(this.resource);
    long avant = System.currentTimeMillis();
    try {
      this.init(in);
    } finally {
      this.close(in);
    }
    long apres = System.currentTimeMillis();
    if (log.isInfoEnabled()) {
      log.info("Initialisation de la base de données Hsql : " + (apres - avant) + "ms");
    }
  }

  /**
   * Traitement a effectuer lors de la destruction de
   * l'instance du singleton.
   */
  @Override
  public void destroy() throws Exception {
    this.tearDown();
  }

  /**
   * Définit la référence au script SQL d'initilisation d'HSQLDB.
   */
  public void setResource(String resource) {
    this.resource = resource;
  }

  /*
   * Fermer le stream.
   */
  private void close(InputStream in) {
    try {
      in.close();
    } catch (IOException e) {}
  }
}
