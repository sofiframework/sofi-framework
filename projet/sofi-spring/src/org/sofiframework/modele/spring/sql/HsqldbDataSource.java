/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.sql;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.hsqldb.jdbc.jdbcDataSource;

/**
 * Intialise un base de données in-memory HSQLDB afin de faire les test JUnit
 * pour hibernate sans être connecté à une base de données externe
 * 
 * @author Guillaume Poirier, Jean-Maxime Pelletier
 */
public class HsqldbDataSource implements DataSource {

  private DataSource dataSource;

  private String url = "jdbc:hsqldb:mem:testdb";

  private String encoding = "ISO-8859-1";

  /**
   * Initiliase l'environnement de tests avec le script données par le
   * <code>InputStream</code> passé en paramètre.
   * 
   * @param in
   *            Le stream à partir duquel lire le script d'initialisation.
   * @throws IOException
   *             si un problème se produit lors de la lecture de l'InputStream
   * @throws SQLException
   *             si un problème se produit lors de l'exécution du script
   */
  public void init(InputStream in) throws IOException, SQLException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    for (int read = in.read(); read != -1; read = in.read()) {
      buffer.write(read);
    }
    init(buffer.toString(getEncoding()));
  }

  /**
   * Initiliase l'environnement de tests avec le script passé en paramètre.
   * 
   * @param sql
   *            le script d'initialisation.
   * @throws SQLException
   *             si un problème se produit lors de l'exécution du script
   */
  public void init(String sql) throws SQLException {
    creerHsqldbDataSource();
    execute(sql);
  }

  /**
   * Initialise un DataSource pour accéder à la base de données HSQLDB
   */
  private void creerHsqldbDataSource() {
    jdbcDataSource ds = new jdbcDataSource();
    ds.setUser("sa");
    ds.setPassword("");
    ds.setDatabase(this.url);
    this.dataSource = ds;
  }

  /**
   * Ferme la base de données
   * 
   * @throws SQLException
   *             si une erreur SQL se produit lors de l'exécution de la
   *             fermeture de la base de données
   */
  public void tearDown() throws SQLException {
    execute("SHUTDOWN");
  }

  private void execute(String sql) throws SQLException {
    Connection conn = getConnection();
    try {
      Statement stmt = conn.createStatement();
      stmt.execute(sql);
      conn.commit();
    } finally {
      try {
        conn.close();
      } catch (SQLException e) {
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#getConnection()
   */
  @Override
  public Connection getConnection() throws SQLException {
    Connection conn = dataSource.getConnection();
    conn.setAutoCommit(false);
    return conn;
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#getConnection(java.lang.String,
   *      java.lang.String)
   */
  @Override
  public Connection getConnection(String username, String password)
      throws SQLException {
    Connection conn = dataSource.getConnection(username, password);
    conn.setAutoCommit(false);
    return conn;
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#getLoginTimeout()
   */
  @Override
  public int getLoginTimeout() throws SQLException {
    return dataSource.getLoginTimeout();
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#getLogWriter()
   */
  @Override
  public PrintWriter getLogWriter() throws SQLException {
    return dataSource.getLogWriter();
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#setLoginTimeout(int)
   */
  @Override
  public void setLoginTimeout(int seconds) throws SQLException {
    dataSource.setLoginTimeout(seconds);
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
   */
  @Override
  public void setLogWriter(PrintWriter out) throws SQLException {
    dataSource.setLogWriter(out);
  }

  @Override
  public boolean isWrapperFor(Class iface) throws SQLException {
    return iface.isInstance(this);
  }

  @Override
  public Object unwrap(Class iface) throws SQLException {
    Object objet = null;

    if (isWrapperFor(iface)) {
      objet = this;
    } else {
      throw new ClassCastException("L'objet " + this.getClass().getName()
          + " doit implémenter ou étendre " + iface.getName());
    }

    return objet;
  }

  /**
   * Obtenir l'encoding du fichier SQL de la base de données mémoire.
   * La valeur par défaut est ISO-8859-1
   * 
   * @return encoding
   */
  public String getEncoding() {
    return encoding;
  }

  /**
   * Modifier l'encoding du fichier sql pour
   * charger lors de l'instanciation de la base de données.
   * La valeur par défaut est ISO-8859-1
   * 
   * @param encoding Encoding du fichier sql de chargement
   * de la base de données
   */
  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }
  
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return Logger.getLogger(HsqldbDataSource.class.getName());
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}