/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.cache.service;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.springframework.beans.factory.InitializingBean;

/**
 * Implémentation du service de cache qui utilise EHCache.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceEhCacheImpl implements InitializingBean, ServiceCache {

  private String nom = null;

  private CacheManager manager = null;

  private Cache cache = null;

  @Override
  public void afterPropertiesSet() throws Exception {
    // Exposer le manager en MBean
    /*
     * try { MBeanServer mBeanServer =
     * ManagementFactory.getPlatformMBeanServer();
     * mBeanServer.registerMBean(manager, new ObjectName(this.nom == null ?
     * "cacheParDefaut" : this.nom)); } catch (Exception e) { // Alors le
     * serveur supporte pas les MBeans }
     */
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * ca.qc.gouv.msp.gde.modele.commun.service.CacheService#getValeur(java.lang
   * .Object)
   */
  @Override
  public Object getValeur(Object cle) {
    Element e = this.cache.get(cle);
    return e != null ? e.getObjectValue() : null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * ca.qc.gouv.msp.gde.modele.commun.service.CacheService#isEnCache(java.lang
   * .Object)
   */
  @Override
  public boolean isEnCache(Object cle) {
    boolean inCache = this.cache.isKeyInCache(cle);
    Element e = this.cache.get(cle);
    boolean notExpired = e != null && !this.cache.isExpired(e);
    return inCache && notExpired;
    //return this.getValeur(cle) != null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * ca.qc.gouv.msp.gde.modele.commun.service.CacheService#ajouter(java.lang
   * .Object, java.lang.Object)
   */
  @Override
  public void ajouter(Object cle, Object valeur) {
    this.cache.put(new Element(cle, valeur));
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public Cache getCache() {
    return cache;
  }

  public void setCache(Cache cache) {
    this.cache = cache;
  }

  public CacheManager getManager() {
    return manager;
  }

  public void setManager(CacheManager manager) {
    this.manager = manager;
  }
}
