/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.cache.service;

/**
 * Service qui permet de mettre des objets en cache.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface ServiceCache {

  /**
   * Ajouter une valeur dans le cache.
   * 
   * @param cle clé de la valeur
   * @param valeur valeur
   */
  void ajouter(Object cle, Object valeur);

  /**
   * Obtenir une valeur dans le cache.
   * 
   * @param cle Clé de la valeur
   * @return valeur
   */
  Object getValeur(Object cle);

  /**
   * Est-ce que la valeur est présente dans le cache.
   * 
   * @param cle clé de la valeur
   * @return vrai en cache faux sinon
   */
  boolean isEnCache(Object cle);
}
