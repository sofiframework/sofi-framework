/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.encryption;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Cette classe permet de passer des séquences encryptées binaires à du texte
 * lisible en base 64.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @deprecated utiliser Base64 de commons codec.
 */
@Deprecated
public class Base64 {
  /**
   * Permet d'obtenir des caractères lisibles à partir d'une séquence binaire.
   * @param sixbit séquence binaire
   * @return retourne le caractère correspondant à une chaîne binaire donnée
   */
  protected static char getChar(int sixbit) {
    if ((sixbit >= 0) && (sixbit <= 25)) {
      return (char) (65 + sixbit);
    }

    if ((sixbit >= 26) && (sixbit <= 51)) {
      return (char) (97 + (sixbit - 26));
    }

    if ((sixbit >= 52) && (sixbit <= 61)) {
      return (char) (48 + (sixbit - 52));
    }

    if (sixbit == 62) {
      return '+';
    }

    return (sixbit != 63) ? '?' : '/';
  }

  /**
   * Permet d'obtenir une séquence binaire basée sur un caractère donné.
   * @param c le caractère à convertir
   * @return la séquence binaire correspondant au caractère donné
   */
  protected static int getValue(char c) {
    if ((c >= 'A') && (c <= 'Z')) {
      return c - 65;
    }

    if ((c >= 'a') && (c <= 'z')) {
      return (c - 97) + 26;
    }

    if ((c >= '0') && (c <= '9')) {
      return (c - 48) + 52;
    }

    if (c == '+') {
      return 62;
    }

    if (c == '/') {
      return 63;
    }

    return (c != '=') ? (-1) : 0;
  }

  /**
   * Permet d'encoder une séquence d'octet.
   * @param raw la séquence d'octets
   * @return le texte corresondant à la séquence d'octets fournies
   */
  public static String encode(byte[] raw) {
    StringBuffer encoded = new StringBuffer();

    for (int i = 0; i < raw.length; i += 3) {
      encoded.append(encodeBlock(raw, i));
    }

    return encoded.toString();
  }

  /**
   * Permet d'obtenir une séquence de caractères basée sur une suite d'octet
   * ainsi qu'une position.
   * @param raw la séquence d'octets
   * @param offset la position
   * @return une séquence de caractère convertie
   */
  protected static char[] encodeBlock(byte[] raw, int offset) {
    int block = 0;
    int slack = raw.length - offset - 1;
    int end = (slack < 2) ? slack : 2;

    for (int i = 0; i <= end; i++) {
      byte b = raw[offset + i];

      int neuter = (b >= 0) ? ((int) (b)) : (b + 256);
      block += (neuter << (8 * (2 - i)));
    }

    char[] base64 = new char[4];

    for (int i = 0; i < 4; i++) {
      int sixbit = block >>> (6 * (3 - i)) & 0x3f;
      base64[i] = getChar(sixbit);
    }

    if (slack < 1) {
      base64[2] = '=';
    }

    if (slack < 2) {
      base64[3] = '=';
    }

    return base64;
  }

  /**
   * Permet de décoder une chaîne de caractère donnée à partir d'un texte
   * spécifique.
   * @param base64 Le texte à convertir en tableau de bytes
   * @return le tableau d'octet correspondant au texte fourni en paramètre
   */
  public static byte[] decode(String base64) {
    int pad = 0;

    for (int i = base64.length() - 1; base64.charAt(i) == '='; i--) {
      pad++;
    }

    int length = ((base64.length() * 6) / 8) - pad;
    byte[] raw = new byte[length];
    int rawindex = 0;

    for (int i = 0; i < base64.length(); i += 4) {
      int block = (getValue(base64.charAt(i)) << 18) +
          (getValue(base64.charAt(i + 1)) << 12) +
          (getValue(base64.charAt(i + 2)) << 6) + getValue(base64.charAt(i + 3));

      for (int j = 0; (j < 3) && ((rawindex + j) < raw.length); j++) {
        raw[rawindex + j] = (byte) ((block >> (8 * (2 - j))) & 0xff);
      }

      rawindex += 3;
    }

    return raw;
  }

  /**
   * Permet d'encoder un objet en base 64.
   * @param o l'objet à encoder
   * @return l'objet sérialisé en base 64
   */
  public static String objectToString(Object o) {
    if (o == null) {
      return null;
    }

    ByteArrayOutputStream baos = new ByteArrayOutputStream(32000);

    try {
      ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(
          baos));
      os.flush();
      os.writeObject(o);
      os.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return encode(baos.toByteArray());
  }

  /**
   * Permet de décoder une chaîne de caractères en objet.
   * @param s la chaîne de caractère
   * @return l'objet décodé correspondant à la chaîne de caractère fournie
   */
  public static Object stringToObject(String s) {
    if (s == null) {
      return null;
    }

    byte[] byteArray = decode(s);

    ByteArrayInputStream baos = new ByteArrayInputStream(byteArray);

    try {
      ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(baos));

      return is.readObject();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }
}
