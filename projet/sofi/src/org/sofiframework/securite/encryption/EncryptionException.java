/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.encryption;


/**
 * Exception spécifiant qu'il y a eu une erreur lors d'un encryption
 * ou d'une de-encryption.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class EncryptionException extends Exception {

  private static final long serialVersionUID = 7567508866338051362L;

  /**
   * Constructeur par défaut
   */
  public EncryptionException() {
    super();
  }

  /**
   * Constructeur avec un message d'erreur spécifique
   * @param msg le message d'erreur
   */
  public EncryptionException(String msg) {
    super(msg);
  }

  /**
   * Constructeur avec un message d'erreur spécifique, ainsi que la cause
   * de l'exception
   * @param msg le message d'erreur spécifique
   * @param cause la cause de l'exception
   */
  public EncryptionException(String msg, Throwable cause) {
    super(msg, cause);
  }

  /**
   * Constructeur avec une cause spécifique
   * @param cause la cause qui a causée l'exception
   */
  public EncryptionException(Throwable cause) {
    super(cause);
  }
}
