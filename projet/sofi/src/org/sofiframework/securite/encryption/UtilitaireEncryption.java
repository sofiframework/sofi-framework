/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.encryption;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import com.sun.crypto.provider.SunJCE;

/**
 * Cette classe utilitaire permet de d'encrypter ou décrypter du texte basé sur
 * l'algorithme SHA, DES ou 3DES CBC.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @author Rémi Mercier (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireEncryption {
  /** Variable désigant le type d'algorythme pour l'encryption */
  public static String DIGEST_ALGORITHM = "SHA";

  /** Variable désignant la clé pour l'encryption */
  public static String KEY_ALGORITHM = "DES";

  /**
   * Cette méthode permet de générer une clé basé sur l'algorithme DES
   * 
   * @return la clé généré
   * @throws java.lang.Exception
   *           si une survient lors de la génération de la clé
   */
  public static Key generateKey() throws Exception {
    return generateKey(KEY_ALGORITHM);
  }

  /**
   * Cette méthode permet de générer une clé basé sur un algorithme donné.
   * 
   * @param algorithm
   *          l'algorithme DES ou SHA
   * @return la clé généré
   * @throws org.sofiframework.securite.encryption.EncryptionException
   *           si une exception survient lors de la génération de la clé
   */
  public static Key generateKey(String algorithm) throws EncryptionException {
    try {
      Security.addProvider(new SunJCE());

      KeyGenerator generator = KeyGenerator.getInstance(algorithm);
      generator.init(56, new SecureRandom());

      Key key = generator.generateKey();

      return key;
    } catch (Exception e) {
      throw new EncryptionException(e);
    }
  }

  public static Key generateKey(String algorithm, String passe)
      throws EncryptionException {
    try {
      Security.addProvider(new SunJCE());

      KeySpec keySpec = new DESKeySpec(passe.getBytes());
      SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(
          keySpec);

      return key;
    } catch (Exception e) {
      throw new EncryptionException(e);
    }
  }

  /**
   * Cette méthode permet de décripter du texte.
   * 
   * @param key
   *          la clé utilisé lors de l'encryption
   * @param encryptedString
   *          le texte encrypté que l'on veut décrypter
   * @return le texte décrypté
   * @throws org.sofiframework.securite.encryption.EncryptionException
   *           si une erreur survient lors de la décryption
   */
  public static String decrypt(Key key, String encryptedString)
      throws EncryptionException {
    try {
      Security.addProvider(new SunJCE());

      Cipher cipher = Cipher.getInstance(key.getAlgorithm());
      cipher.init(Cipher.DECRYPT_MODE, key);

      byte[] encryptedBytes = Base64.decode(encryptedString);
      byte[] decryptedBytes = cipher.doFinal(encryptedBytes);

      String decryptedString = new String(decryptedBytes, "UTF8");

      return decryptedString;
    } catch (Exception e) {
      throw new EncryptionException(e);
    }
  }

  /**
   * Permet d'obtenir du texte encrypter de longueur constante basé sur
   * l'algorithme SHA.
   * <p>
   * Cette méthode enlève les valeurs parasites afin d'épurer le texte encrypté.
   * <p>
   * 
   * @param text
   *          le texte à épurer
   * @return le texte encrypté de longueur constante
   */
  public static String digest(String text) {
    return digest(DIGEST_ALGORITHM, text);
  }

  /**
   * Permet d'obtenir du texte encrypter de longueur constante basé sur un
   * algorithme donné.
   * <p>
   * Cette méthode enlève les valeurs parasites afin d'épurer le texte encrypté.
   * <p>
   * 
   * @param algorithm
   *          l'algorithme choisi. SHA ou DES
   * @param text
   *          le texte encrypté à épurer
   * @return le texte encrypté de longueur constante
   */
  public static String digest(String algorithm, String text) {
    MessageDigest mDigest = null;

    try {
      mDigest = MessageDigest.getInstance(algorithm);

      mDigest.update(text.getBytes("UTF-8"));
    } catch (NoSuchAlgorithmException nsae) {
      nsae.printStackTrace();
    } catch (UnsupportedEncodingException uee) {
      uee.printStackTrace();
    }

    byte[] raw = mDigest.digest();

    return Base64.encode(raw);
  }

  /**
   * Permet de comparer la version encryptée d'une chaîne de caractère avec une
   * version non encryptée pour vérifier si leur valeurs non encry^ptées sont
   * égales. L'algorythme utilisé est SHA.
   * 
   * @return Si les deux chaînes non encryptées sont égales
   * @param nonEncrypte
   *          Chaîne non encryptée
   * @param encrypte
   *          Chaîne déjà encryptée à l'aide d'un digest.
   */
  public static boolean isDigestsEquals(String encrypte, String nonEncrypte) {
    return encrypte.equals(UtilitaireEncryption.digest(nonEncrypte));
  }

  /**
   * Permet d'encrypter du texte.
   * <p>
   * 
   * @param key
   *          la clé utilisée pour encrypter
   * @param plainText
   *          le texte que l'on veut encrypté
   * @return le texte encrypté
   * @throws org.sofiframework.securite.encryption.EncryptionException
   *           si une erreur survient lors de l'encryption
   */
  public static String encrypt(Key key, String plainText)
      throws EncryptionException {
    try {
      Security.addProvider(new SunJCE());

      Cipher cipher = Cipher.getInstance(key.getAlgorithm());
      cipher.init(Cipher.ENCRYPT_MODE, key);

      byte[] decryptedBytes = plainText.getBytes("UTF8");
      byte[] encryptedBytes = cipher.doFinal(decryptedBytes);

      String encryptedString = Base64.encode(encryptedBytes);

      return encryptedString;
    } catch (Exception e) {
      throw new EncryptionException(e);
    }
  }

  /**
   * Permet d'encrypter du texte suivant l'algorythme 3DES CBC. Avec le mode
   * CBC, il est nécessaire en plus de la clé de fournir un vecteur
   * d'initialisation ; ce vecteur (comme la clé) doit être le même pour
   * encrypter et décrypter la donnée. Son contenu n'a pas d'importance ; cela
   * peut être par exemple : <br>
   * <code>IvParameterSpec IvParameters = new IvParameterSpec(
   * new byte[] { 12, 34, 56, 78, 90, 87, 65, 43 });</code>
   * <p>
   * <i>Pour plus d'information sur l'implémentation de la méthode, voir
   * http://www.quepublishing.com/articles/article.aspx?p=26343&seqNum=4</i>
   * 
   * @param plainText
   *          le texte à encrypter
   * @param key
   *          la clé pour encrypter le texte. Objet de type {@link SecretKey}.
   *          Elle doit être composé d'au moins 24 caractères sinon une
   *          exception est déclenchée.
   * @param ivParameter
   *          le vecteur d'initialisation. Objet de type {@link IvParameterSpec}
   *          .
   * @return un tableau de byte correspondant au texte encrypté.
   * @throws EncryptionException
   *           si une erreur survient lors de l'encryption.
   */
  public static byte[] encrypt3DES_CBC(String plainText, String key,
      IvParameterSpec ivParameter) throws EncryptionException {

    byte[] encrypted = null;
    try {
      // Create an array to hold the key
      byte[] encryptKey = key.getBytes();

      // Create a DESede key spec from the key
      DESedeKeySpec spec = new DESedeKeySpec(encryptKey);

      // Get the secret key factor for generating DESede keys
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

      // Generate a DESede SecretKey object
      SecretKey theKey = keyFactory.generateSecret(spec);

      // Create a DESede Cipher
      Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

      // Initialize the cipher and put it into encrypt mode
      cipher.init(Cipher.ENCRYPT_MODE, theKey, ivParameter);

      // Encrypt the data
      encrypted = cipher.doFinal(plainText.getBytes());

    } catch (Exception e) {
      throw new EncryptionException(e);
    }
    return encrypted;
  }

  /**
   * Permet de décrypter des données suivant l'algorythme 3DES CBC. Avec le mode
   * CBC, il est nécessaire en plus de la clé de fournir un vecteur
   * d'initialisation ; ce vecteur (comme la clé) doit être le même pour
   * encrypter et décrypter la donnée. Son contenu n'a pas d'importance ; cela
   * peut être par exemple : <br>
   * <code>IvParameterSpec IvParameters = new IvParameterSpec(
   * new byte[] { 12, 34, 56, 78, 90, 87, 65, 43 });</code>
   * <p>
   * <i>Pour plus d'information sur l'implémentation de la méthode, voir
   * http://www.quepublishing.com/articles/article.aspx?p=26343&seqNum=4</i>
   * 
   * @param encryptedData
   *          la donnée cryptée à décrypter.
   * @param key
   *          la clé pour décrypter le texte. Objet de type {@link SecretKey}.
   *          Elle doit être composé d'au moins 24 caractères sinon une
   *          exception est déclenchée.
   * @param ivParameter
   *          le vecteur d'initialisation. Objet de type {@link IvParameterSpec}
   *          .
   * @return un tableau de byte correspondant à la donnée décryptée.
   * @throws EncryptionException
   */
  public static byte[] decrypt3DES_CBC(byte[] encryptedData, String key,
      IvParameterSpec ivParameter) throws EncryptionException {

    byte[] decrypted = null;
    try {
      // Create an array to hold the key
      byte[] encryptKey = key.getBytes();

      // Create a DESede key spec from the key
      DESedeKeySpec spec = new DESedeKeySpec(encryptKey);

      // Get the secret key factor for generating DESede keys
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

      // Generate a DESede SecretKey object
      SecretKey theKey = keyFactory.generateSecret(spec);

      // Create a DESede Cipher
      Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

      // Initialize the cipher and put it in decrypt mode
      cipher.init(Cipher.DECRYPT_MODE, theKey, ivParameter);

      // Decrypt the data
      decrypted = cipher.doFinal(encryptedData);

    } catch (Exception e) {
      throw new EncryptionException(e);
    }
    return decrypted;
  }

  /**
   * Permet d'obtenir le format hexadécimal du hash MD5 de la donnée passée en
   * paramètre.
   * 
   * @param data
   * @return
   * @throws EncryptionException
   */
  public static String hashMD5ToHex(String data) throws EncryptionException {
    String hashedData = null;
    if (data != null) {
      try {
        byte[] hash = MessageDigest.getInstance("MD5").digest(data.getBytes());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.length; ++i) {
          sb.append(Integer.toHexString((hash[i] & 0xFF) | 0x100).substring(1,
              3));
        }
        hashedData = sb.toString();
      } catch (NoSuchAlgorithmException e) {
        throw new EncryptionException(e);
      }
    }
    return hashedData;
  }
}
