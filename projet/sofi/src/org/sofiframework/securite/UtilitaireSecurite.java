/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.sofiframework.securite.ldap.Entry;
import org.sofiframework.securite.ldap.LDAPConnection;
import org.sofiframework.securite.ldap.LDAPSearch;


/**
 * Classe offrant des services pour la sécurité LDAP.
 * <p>
 * Afin d'utiliser la classe, il faut la créer en lui fournissant les informations
 * du serveur LDAP : nom du serveur, port du serveur, nom d'utilisateur du compte
 * administrateur, mot de passe du compte administrateur et contexte.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireSecurite {
  /** Variable qui désigne le mot de passe utilisateur */
  private static final String UNICODE_PASSWORD = "userPassword";

  /** le nom du serveur d'authentification */
  private String nomServeur;

  /** le port de communication avec les serveur d'authentification */
  private int portServeur;

  /** le contexte dans lequel rechercher les utilisateur */
  private String contexte;

  /** le code d'utilisateur admin afin d'entrer sur le serveur d'authentification */
  private String nomUtilisateurAdmin;

  /** le mode de passe du compte administrateur sur le serveur d'authentification */
  private String motDePasseAdmin;

  /**
   * Constructeur de l'objet UtilitaireSecurite.
   * <p>
   * Contrairement à la majorité des classes Utilitaires de SOFI, celle-ci
   * doit être instantiée avant d'être utilisée. Le constructeur permet de saisir
   * les paramètres nécessaires à la connexion au serveur LDAP.
   * <p>
   * @param nomServeur le nom du serveur LDAP
   * @param portServeur le port du serveur LDAP
   * @param nomUtilisateurAdmin le nom de l'administrateur LDAP (ex: orcladmin)
   * @param motDePasseAdmin le mot de passe de l'administrateur LDAP
   * @param contexte le contexte dans lequel se retrouve les utilisateurs
   */
  public UtilitaireSecurite(String nomServeur, int portServeur,
      String nomUtilisateurAdmin, String motDePasseAdmin, String contexte) {
    this.setNomServeur(nomServeur);
    this.setPortServeur(portServeur);
    this.setNomUtilisateurAdmin(nomUtilisateurAdmin);
    this.setMotDePasseAdmin(motDePasseAdmin);
    this.setContexte(contexte);
  }

  /**
   * Vérifier le mot de passe d'un utilisateur.
   * <p>
   * @param nomUtilisateur le nom de l'utilisateur
   * @param motPasse le mot de passe de l'utilisateur
   * @return vrai si le mot de passe est correct, sinon faux
   */
  public boolean verifierMotDePasseUtilisateur(String nomUtilisateur,
      String motPasse) {
    try {
      LDAPConnection connexion = new LDAPConnection(nomServeur, portServeur,
          "cn=" + nomUtilisateur + "," + contexte, motPasse);
      DirContext dc = connexion.open();
      connexion.close(dc);
    } catch (Exception e) {
      e.printStackTrace();

      return false;
    }

    return true;
  }

  /**
   * Changer le mot de passe d'un utilisateur.
   * <p>
   * Si l'utilisateur n'a pas été trouvé ou si le nouveau mot de passe ne
   * respecte pas les règles du serveur, la méthode retournera faux.
   * <p>
   * @param nomUtilisateur le nom de l'utilisateur
   * @param nouveauMotPasse le nouveau mot de passe de l'utilisateur
   * @return vrai si le mot de passe a été changé avec succès, sinon faux
   */
  public boolean changerMotDePasseUtilisateur(String nomUtilisateur,
      String nouveauMotPasse) {
    try {
      LDAPConnection connexion = new LDAPConnection(nomServeur, portServeur,
          "cn=" + nomUtilisateurAdmin, motDePasseAdmin);
      DirContext ctx = connexion.open();
      //Attributes attrs = ctx.getAttributes("cn=" + nomUtilisateur + "," +
      //    contexte);

      BasicAttribute attribute = new BasicAttribute(UNICODE_PASSWORD);
      attribute.add(nouveauMotPasse);

      Attributes testAttrs = new BasicAttributes();
      testAttrs.put(attribute);

      ctx.modifyAttributes("cn=" + nomUtilisateur + "," + contexte,
          DirContext.REPLACE_ATTRIBUTE, testAttrs);

      ctx.close();
    } catch (Exception e) {
      return false;
    }

    return true;
  }

  /**
   * Obtenir l'entrée LDAP d'un utilisateur.
   * <p>
   * @param nomUtilisateur le nom de l'utilisateur
   * @return une entrée contenant les informations LDAP de l'utilisateur
   */
  public Entry obtenirEntreeUsager(String nomUtilisateur) {
    LDAPConnection lc = new LDAPConnection(nomServeur, portServeur,
        "cn=" + nomUtilisateurAdmin, motDePasseAdmin);

    try {
      DirContext dc = lc.open();

      Vector vecteur = LDAPSearch.search(lc, contexte, "sub",
          "(cn=" + nomUtilisateur + ")", new String[0]);

      lc.close(dc);

      return (Entry) vecteur.get(0);
    } catch (NamingException ne) {
      System.err.println("Error: " + ne.getMessage());

      return null;
    }
  }

  /**
   * Obtenir l'entrée LDAP d'un utilisateur.
   * <p>
   * @param contexteGroupes le contexte dans lequel les groupes se retrouvent
   * @param dnUtilisateur le chemin LDAP complet de l'utilisateur
   * @return une liste de chaines de caractères contenant les groupes de l'utilisateur
   */
  public ArrayList obtenirListeGroupes(String contexteGroupes,
      String dnUtilisateur) {
    LDAPConnection lc = new LDAPConnection(nomServeur, portServeur,
        "cn=" + nomUtilisateurAdmin, motDePasseAdmin);

    try {
      DirContext dc = lc.open();

      ArrayList listeGroupes = new ArrayList();
      Vector vecteurGroupes = LDAPSearch.search(lc, contexteGroupes, "sub",
          "(cn=*)", new String[0]);

      Iterator iterator = vecteurGroupes.iterator();

      while (iterator.hasNext()) {
        Entry entreeGroupe = (Entry) iterator.next();

        if (!entreeGroupe.getDN().startsWith(",")) {
          String nomGroupe = entreeGroupe.getDN().substring(3,
              entreeGroupe.getDN().indexOf(','));
          Vector vecteurMembres = (Vector) entreeGroupe.get("uniquemember");

          if (vecteurMembres != null) {
            Iterator iterateurMembres = vecteurMembres.iterator();

            while (iterateurMembres.hasNext()) {
              String membre = (String) iterateurMembres.next();

              if (membre.toLowerCase().equals(dnUtilisateur.toLowerCase())) {
                listeGroupes.add(nomGroupe);
              }
            }
          }
        }
      }

      lc.close(dc);

      return listeGroupes;
    } catch (NamingException ne) {
      System.err.println("Error: " + ne.getMessage());

      return null;
    }
  }

  /**
   * Permet de faire la recherche d'un utilisateur dans le répertoire LDAP pour
   * un attribut pcécis et une valeur précise.
   * @throws java.lang.Exception
   * @return Le nom d'utilisateur LDAP.
   * @param valeurRecherche la valeur de recherche.
   * @param critereRecherche le critère de recherche.
   */
  public String getUtilisateur(String critereRecherche, String valeurRecherche)
      throws Exception {
    DirContext srchContext = null;
    Hashtable srchEnv = new Hashtable();
    srchEnv.put("java.naming.factory.initial",
        "com.sun.jndi.ldap.LdapCtxFactory");
    srchEnv.put("java.naming.provider.url",
        "ldap://" + getNomServeur() + ":" + getPortServeur());

    if (nomUtilisateurAdmin != null) {
      srchEnv.put("java.naming.security.authentication", "simple");
      srchEnv.put("java.naming.security.principal",
          nomUtilisateurAdmin + "," + contexte);
      srchEnv.put("java.naming.security.credentials", motDePasseAdmin);
    }

    try {
      srchContext = new InitialDirContext(srchEnv);
    } catch (NamingException namEx) {
      namEx.printStackTrace();
    }

    String[] returnAttribute = new String[0];
    String filtreRecherche = critereRecherche + "=" + valeurRecherche;
    SearchControls srchControls = new SearchControls();
    srchControls.setSearchScope(2);
    srchControls.setReturningAttributes(returnAttribute);

    NamingEnumeration srchResponse = srchContext.search(contexte,
        filtreRecherche, srchControls);

    if (srchResponse.hasMore()) {
      SearchResult obj = (SearchResult) srchResponse.nextElement();

      return obj.getName();
    } else {
      return null;
    }
  }

  /**
   * Obtenir le nom du serveur.
   * <p>
   * @return le nom du serveur
   */
  public String getNomServeur() {
    return nomServeur;
  }

  /**
   * Fixer le nom du serveur.
   * <p>
   * @param newNomServeur le nom du serveur
   */
  public void setNomServeur(String newNomServeur) {
    nomServeur = newNomServeur;
  }

  /**
   * Obtenir le port du serveur.
   * <p>
   * @return le port du serveur
   */
  public int getPortServeur() {
    return portServeur;
  }

  /**
   * Fixer le port du serveur.
   * <p>
   * @param newPortServeur le port du serveur
   */
  public void setPortServeur(int newPortServeur) {
    portServeur = newPortServeur;
  }

  /**
   * Obtenir le contexte dans lequel rechercher les utilisateur.
   * <p>
   * @return le contexte dans lequel rechercher les utilisateur
   */
  public String getContexte() {
    return contexte;
  }

  /**
   * Fixer le contexte dans lequel rechercher les utilisateur.
   * <p>
   * @param newContexte le contexte dans lequel rechercher les utilisateur
   */
  public void setContexte(String newContexte) {
    contexte = newContexte;
  }

  /**
   * Fixer le code d'utilisateur de l'administrateur LDAP.
   * <p>
   * @param newNomUtilisateurAdmin le code d'utilisateur de l'administrateur LDAP
   */
  public void setNomUtilisateurAdmin(String newNomUtilisateurAdmin) {
    nomUtilisateurAdmin = newNomUtilisateurAdmin;
  }

  /**
   * Fixer le mot de passe de l'utilisateur de l'administrateur LDAP.
   * <p>
   * @param newMotDePasseAdmin le mot de passe de l'administrateur LDAP
   */
  public void setMotDePasseAdmin(String newMotDePasseAdmin) {
    motDePasseAdmin = newMotDePasseAdmin;
  }
}
