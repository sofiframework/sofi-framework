/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;


/**
 * Accès à ActiveDirectory pour vérifier les informations d'un utilisateur.
 * <p>
 * LDAP doit être activé sur le serveur pour pouvoir communiquer.
 * @author Steve Tremblay
 * @since SOFI 2.0
 *
 */
public class UtilitaireActiveDirectory {
  public static final int AUTHENTIFICATION_OK = 1;
  public static final int UTILISATEUR_NON_TROUVE = -1;
  public static final int MOT_PASSE_INCORRECT = -2;
  public static final int LOGIN_NON_PERMIS_A_CETTE_HEURE = -3;
  public static final int MOT_PASSE_EXPIRE = -4;
  public static final int COMPTE_DESACTIVE = -5;
  public static final int COMPTE_EXPIRE = -6;
  public static final int MOT_PASSE_A_CHANGER = -7;
  public static final int COMPTE_BARRE = -8;

  /**
   * Vérifier le mot de passe de l'utilisateur. Retourne une constante pour signifier le résultat.
   * <p>
   * La constante AUTHENTIFICATION_OK est retournée si tout est correct, sinon une constante d'erreur
   * correspondant à l'erreur est fournie.
   * @param nomServeurAD le nom du serveur Active Directory
   * @param port le port LDAP du serveur Active Directory
   * @param domaine le domaine du serveur Active Directory
   * @param codeUtilisateur le code de l'utilisateur a vérifier
   * @param motPasse le mot de passe à vérifier
   * @return une constante signifiant le résultat
   */
  public static int verifierMotDePasseUtilisateur(String nomServeurAD,
      int port, String domaine, String codeUtilisateur, String motPasse)
          throws NamingException {
    Properties env = new Properties();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.PROVIDER_URL, "ldap://" + nomServeurAD + ":" + port);
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    env.put(Context.SECURITY_PRINCIPAL, codeUtilisateur + "," + domaine);
    env.put(Context.SECURITY_CREDENTIALS, motPasse);

    try {
      new InitialDirContext(env).close();
    } catch (NamingException e) {
      String erreur = e.getExplanation();

      if (erreur.indexOf("data 525") != -1) {
        return UTILISATEUR_NON_TROUVE;
      } else if (erreur.indexOf("data 52e") != -1) {
        return MOT_PASSE_INCORRECT;
      } else if (erreur.indexOf("data 530") != -1) {
        return LOGIN_NON_PERMIS_A_CETTE_HEURE;
      } else if (erreur.indexOf("data 532") != -1) {
        return MOT_PASSE_EXPIRE;
      } else if (erreur.indexOf("data 533") != -1) {
        return COMPTE_DESACTIVE;
      } else if (erreur.indexOf("data 701") != -1) {
        return COMPTE_EXPIRE;
      } else if (erreur.indexOf("data 773") != -1) {
        return MOT_PASSE_A_CHANGER;
      } else if (erreur.indexOf("data 775") != -1) {
        return COMPTE_BARRE;
      } else {
        throw e;
      }
    }

    return 1;
  }
}
