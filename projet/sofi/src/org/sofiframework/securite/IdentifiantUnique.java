/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;


/**
 * Classe permettant d'extraire un identifiant unique pour un utilisateur.
 * <p>
 * Très utile afin d'implémenter un système d'authentification unique
 * personnalisé. L'identifiant unique sert alors de certificat authentification.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class IdentifiantUnique implements Serializable {

  private static final long serialVersionUID = 5294397235694581720L;

  /** un nombre random */
  private static Random myRand;

  /** un nombre généré aléatoirement qui est sécurisé */
  private static SecureRandom mySecureRand;

  /** l'identifiant s */
  private static String s_id;

  static {
    mySecureRand = new SecureRandom();

    long secureInitializer = mySecureRand.nextLong();
    myRand = new Random(secureInitializer);

    try {
      s_id = InetAddress.getLocalHost().toString();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
  }

  /** la valeur avant l'encryption MD5 */
  public String valueBeforeMD5 = "";

  /** la valeur après l'encryption MD5 */
  public String valueAfterMD5 = "";

  /**
   * Constructeur par défaut. Avec aucune spécification ce constructeur
   * offre une sécurité inférieure mais grande performance.
   */
  public IdentifiantUnique() {
    getIdentifiantUnique(false);
  }

  /**
   * Constructeur en passant l'identifiant de l'utilisateur.
   * @param ip l'adresse ip de l'utilisateur
   */
  public IdentifiantUnique(String identifiant) {
    // Spécifier l'IP du client;
    s_id = identifiant + s_id;
    getIdentifiantUnique(true);
  }

  /**
   * Constructeur avec option de sécurité. Si spécifier à true
   * ceci va permettre une forte encryption de la clé unique.
   * @param secure true si on veut une forte encryption, false autrement
   */
  public IdentifiantUnique(boolean secure) {
    getIdentifiantUnique(secure);
  }

  /**
   * Méthode pour générer un identifiant unique
   * @param secure true si on veut une forte encryption, false autrement
   */
  private void getIdentifiantUnique(boolean secure) {
    MessageDigest md5 = null;
    StringBuffer sbValueBeforeMD5 = new StringBuffer();

    try {
      md5 = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      System.out.println("Error: " + e);
    }

    try {
      long time = System.currentTimeMillis();
      long rand = 0;

      if (secure) {
        rand = mySecureRand.nextLong();
      } else {
        rand = myRand.nextLong();
      }

      sbValueBeforeMD5.append(s_id);
      sbValueBeforeMD5.append(":");
      sbValueBeforeMD5.append(Long.toString(time));
      sbValueBeforeMD5.append(":");
      sbValueBeforeMD5.append(Long.toString(rand));

      valueBeforeMD5 = sbValueBeforeMD5.toString();
      md5.update(valueBeforeMD5.getBytes());

      byte[] array = md5.digest();
      StringBuffer sb = new StringBuffer();

      for (int j = 0; j < array.length; ++j) {
        int b = array[j] & 0xFF;

        if (b < 0x10) {
          sb.append('0');
        }

        sb.append(Integer.toHexString(b));
      }

      valueAfterMD5 = sb.toString();
    } catch (Exception e) {
      System.out.println("Error:" + e);
    }
  }

  /**
   * Convertie l'identifiant unique en String
   * Example: C2FEEEACCFCD11D18B0500600806D9B6
   * @return retourne la string représentant l'identifiant généré
   */
  @Override
  public String toString() {
    String raw = valueAfterMD5.toUpperCase();
    StringBuffer sb = new StringBuffer();
    sb.append(raw.substring(0, 8));
    sb.append(raw.substring(8, 12));
    sb.append(raw.substring(12, 16));
    sb.append(raw.substring(16, 20));
    sb.append(raw.substring(20));

    return sb.toString();
  }

  /*
   * Classe de test
   */
  public static void main(String[] args) {
    for (int i = 0; i < 100; i++) {
      IdentifiantUnique identifiantUnique = new IdentifiantUnique(
          "172.11.95.11");
      System.out.println("Seeding String=" + identifiantUnique.valueBeforeMD5);
      System.out.println("rawGUID=" + identifiantUnique.valueAfterMD5);
      System.out.println("RandomGUID=" + identifiantUnique.toString());
    }
  }
}
