/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.ldap;

import java.util.Vector;

import javax.naming.CommunicationException;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.NoPermissionException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InvalidSearchFilterException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


/**
 * Cette classe offre des facilités de recherche dans un répertoire LDAP.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 */
public class LDAPSearch {
  /**
   * Cette méthode permet de rechercher dans un répertoire LDAP basé sur des
   * attributs spécifiques
   * @param lc La connection sur un répertoire LDAP
   * @param base le contexte de base
   * @param scope choisir l'un de ces choix (OBJECT_SCOPE, SUBTREE_SCOPE,
   * ONELEVEL_SCOPE)
   * @param filter permet de réduire la liste de retour
   * @param attributes Les identifiants des attributs à retourner
   * @return un vecteur contenant tous les entrés correspondants aux attributs
   * spécifiés ainsi que le filtre fourni.
   */
  public static Vector search(LDAPConnection lc, String base, String scope,
      String filter, String[] attributes) {
    Vector results = new Vector();

    DirContext dc = null;

    try {
      dc = lc.open();
    } catch (NamingException ne) {
      System.err.println("Error Opening Connection: " + ne.getMessage());

      return results;
    }

    SearchControls sc = new SearchControls();

    if (scope.equals("base")) {
      sc.setSearchScope(SearchControls.OBJECT_SCOPE);
    } else if (scope.equals("one")) {
      sc.setSearchScope(SearchControls.ONELEVEL_SCOPE);
    } else {
      sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
    }

    // Reduce data provided by the LDAP server by listing
    // only those attributes we want to return.
    if (attributes.length > 0) {
      sc.setReturningAttributes(attributes);
    }

    NamingEnumeration ne = null;

    try {
      ne = dc.search(base, filter, sc);

      // Use the NamingEnumeration object to cycle through
      // the result set.
      while (ne.hasMore()) {
        Entry entry = new Entry();
        SearchResult sr = (SearchResult) ne.next();
        String name = sr.getName();

        if ((base != null) && !base.equals("")) {
          entry.setDN(name + "," + base);
        } else {
          entry.setDN(name);
        }

        Attributes at = sr.getAttributes();
        NamingEnumeration ane = at.getAll();

        while (ane.hasMore()) {
          Attribute attr = (Attribute) ane.next();
          String attrType = attr.getID();
          NamingEnumeration values = attr.getAll();
          Vector vals = new Vector();

          //Another NamingEnumeration object, this time
          // to iterate through attribute values.
          while (values.hasMore()) {
            Object oneVal = values.nextElement();

            if (oneVal instanceof String) {
              vals.addElement(oneVal);
            } else {
              vals.addElement(new String((byte[]) oneVal));
            }
          }

          entry.put(attrType, vals);
        }

        results.addElement(entry);
      }

      lc.close(dc);

      // The search() method can throw a number of exceptions.
      // Here we just handle and print the exception.
      // In real life we might want to pass the exception along
      // to a piece of the software that might have a better
      // context for correcting or presenting the problem.
    } catch (InvalidSearchFilterException isfe) {
      System.err.println("Search Filter Invalid: " + filter);
      lc.close(dc);
    } catch (NameNotFoundException nnfe) {
      System.err.println("Object Not Found: " + base);
      lc.close(dc);
    } catch (NoPermissionException npe) {
      System.err.println("Search Failed: Permission Denied");
      lc.close(dc);
    } catch (CommunicationException ce) {
      System.err.println("Error Communicating with Server");
      lc.close(dc);
    } catch (NamingException nex) {
      System.err.println("Error: " + nex.getMessage());
      lc.close(dc);
    }

    return results;
  }
}
