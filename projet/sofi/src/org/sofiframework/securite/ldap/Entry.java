/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.ldap;

import java.util.Hashtable;


/**
 * Cette classe correspond à une entré dans un répertoire LDAP.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 */
public class Entry extends Hashtable {
  /**
   * 
   */
  private static final long serialVersionUID = -5305574314994867574L;
  /** le nom du noeud LDAP dans lequel on doit trouver l'entrée */
  private String dn = null;

  /**
   * Constructeur par défaut
   */
  public Entry() {
    super();
  }

  /**
   * Constructeur basé sur l'objet Entry.
   * <p>
   * @param entry l'objet possédant les données à copier.
   */
  public Entry(org.sofiframework.securite.ldap.Entry entry) {
    super(entry);
    setDN(entry.getDN());
  }

  /**
   * Fixer le nom du noeud LDAP dans lequel on doit trouver l'entrée.
   * <p>
   * @param c le nom du noeud LDAP dans lequel on doit trouver l'entrée
   */
  public void setDN(String c) {
    this.dn = c;
  }

  /**
   * Obtenir le nom du noeud LDAP dans lequel on doit trouver l'entrée.
   * <p>
   * @return le nom du noeud LDAP dans lequel on doit trouver l'entrée
   */
  public String getDN() {
    return dn;
  }
}
