/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;


/**
 * Cette classe permet d'établir une connection à un répertoire LDAP.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 */
public class LDAPConnection {
  /** le nom du serveur LDAP */
  private String host;

  /** le port de communication pour la communication LDAP */
  private int port;

  /** le nom du noeud qui contient l'élément */
  private String dn = null;

  /** le mot de passe pour accéder au noeud */
  private String password = null;

  /**
   * Constructeur par défaut
   */
  public LDAPConnection() {
  }

  /**
   * Constructeur fournissant les paramètres de connection au répertoire.
   * <p>
   * @param host l'adresse ip du serveur LDAP
   * @param port le port du serveur LDAP
   * @param dn le user pour se connecter
   * @param password le mot de passe
   */
  public LDAPConnection(String host, int port, String dn, String password) {
    this.host = host;
    this.port = port;
    this.dn = dn;
    this.password = password;
  }

  /**
   * Cette méthode permet d'obtenir le context sur le serveur LDAP basé sur
   * les paramètres de connection fournis lors de la création de l'objet.
   * @return le context sur les répertoires LDAP
   * @throws javax.naming.NamingException Si on problème survient lors de la
   * connection au serveur LDAP
   */
  public DirContext open() throws NamingException {
    Properties env = new Properties();
    env.put(Context.INITIAL_CONTEXT_FACTORY,
        "com.sun.jndi.ldap.LdapCtxFactory");

    env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + port);

    if (dn != null) {
      env.put(Context.SECURITY_PRINCIPAL, dn);
      env.put(Context.SECURITY_CREDENTIALS, password);
    }

    return new InitialDirContext(env);
  }

  /**
   * Cette méthode permet la déconnection à un répertoire LDAP.
   * @param dc Le contexte du répertoire de laquelle on veut se déconnecter
   */
  public void close(DirContext dc) {
    try {
      if (dc != null) {
        dc.close();
      }
    } catch (NamingException ne) {
      // Ignore exceptions when closing connection...
    }
  }
}
