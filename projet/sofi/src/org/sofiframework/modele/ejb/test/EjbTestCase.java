/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.ejb.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

import javax.ejb.EJBObject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.TestCase;

import org.sofiframework.exception.SOFIException;


public abstract class EjbTestCase extends TestCase {
  Context contexteEJB = null;
  int portRmiIDE = 23891;
  String protocole = "ormi://";
  String securityPrincipals = "admin";
  String securityCredentials = "welcome";
  String initialContextFactory =
      "com.evermind.server.rmi.RMIInitialContextFactory";

  public EjbTestCase(String sTestName) {
    super(sTestName);

    try {
      String url = getUrl();

      if (url.indexOf("ormi://") == -1) {
        url = protocole + getUrl();
      }

      System.out.println(url);

      setContexteEJB(getInitialContext(url));
    } catch (NamingException e) {
      e.printStackTrace();
    }
  }

  private Context getInitialContext(String url) throws NamingException {
    Hashtable env = new Hashtable();
    System.out.println(getSecurityPrincipals());
    System.out.println(getSecurityCredentials());
    env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());
    env.put(Context.SECURITY_PRINCIPAL, getSecurityPrincipals());
    env.put(Context.SECURITY_CREDENTIALS, getSecurityCredentials());
    env.put(Context.PROVIDER_URL, url);

    return new InitialContext(env);
  }

  public void setContexteEJB(Context contexteEJB) {
    this.contexteEJB = contexteEJB;
  }

  public Context getContexteEJB() {
    return contexteEJB;
  }

  public javax.ejb.EJBObject getRemote() {
    try {
      Method create = null;
      EJBObject ejb = null;
      javax.ejb.EJBHome home =
          (javax.ejb.EJBHome)PortableRemoteObject.narrow(getContexteEJB().lookup(getNomService()),
              getClasseHome());

      // Création du remote.
      // Obtenir la méthode de création du EJB.
      try {
        create = home.getClass().getMethod("create", new Class[] { });
      } catch (NoSuchMethodException e) {
        throw new SOFIException(e);
      } catch (Throwable t) {
        throw new SOFIException(null, t);
      }

      // Créer l'objet EJB.
      try {
        ejb = (EJBObject)create.invoke(home, new Object[] { });
      } catch (IllegalAccessException e) {
        throw new SOFIException(e);
      } catch (InvocationTargetException e) {
        throw new SOFIException(e);
      } catch (Throwable t) {
        throw new SOFIException(null, t);
      }

      return ejb;
    } catch (NamingException e) {
      throw new SOFIException(e.getMessage());
    }
  }

  /**
   * À implémenter afin de retourner l'url de l'EJB.
   */
  protected abstract String getUrl();

  /**
   * À implémenter afin de retourner le nom du service.
   */
  protected abstract String getNomService();

  /**
   * À implémenter afin de retrouner l'interface du Home de l'EJB.
   */
  protected abstract Class getClasseHome();

  /**
   * Retourne l'url afin d'appeler un EJB executé dans l'outil de développpement.
   * @return url afin d'appeler un EJB executé dans l'outil de développpement.
   */
  public String getUrlIDE() {
    StringBuffer url = new StringBuffer();
    url.append("localhost:");
    url.append(getPortRmiIDE());
    url.append("/current-workspace-app");

    return url.toString();
  }

  /**
   * Fixer le port rmi de l'outil de développement.
   * @param portRmiIDE le port rmi de l'outil de développement.
   */
  public void setPortRmiIDE(int portRmiIDE) {
    this.portRmiIDE = portRmiIDE;
  }

  /**
   * Retourne le port rmi de l'outil de développement.
   * @return  le port rmi de l'outil de développement.
   */
  public int getPortRmiIDE() {
    return portRmiIDE;
  }

  public void setSecurityCredentials(String securityCredentials) {
    this.securityCredentials = securityCredentials;
  }

  public String getSecurityCredentials() {
    return securityCredentials;
  }

  public void setProtocole(String protocole) {
    this.protocole = protocole;
  }

  public String getProtocole() {
    return protocole;
  }

  public void setSecurityPrincipals(String securityPrincipals) {
    this.securityPrincipals = securityPrincipals;
  }

  public String getSecurityPrincipals() {
    return securityPrincipals;
  }

  public void setInitialContextFactory(String initialContextFactory) {
    this.initialContextFactory = initialContextFactory;
  }

  public String getInitialContextFactory() {
    return initialContextFactory;
  }
}
