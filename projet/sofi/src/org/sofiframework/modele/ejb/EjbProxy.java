/*
 * $Id: EjbProxy.java 166 2005-05-26 13:23:48Z guillaume.poirier $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.ejb;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;


/**
 * Utilitaire pour permettre que chacun des appels de méthode à un EJB
 * soit fait sur une instance nouvellement créer à partir du Home, et qui
 * est libéré tout de suite après.
 * @author Guillaume Poirier (Nurun inc.)
 */
public class EjbProxy implements InvocationHandler {
  private static final Log log = LogFactory.getLog(EjbProxy.class);
  private EJBHome home;
  private Method createMethod;

  private EjbProxy(EJBHome home) {
    this.home = home;

    try {
      createMethod = home.getClass().getMethod("create", (Class<?>[]) null);
    } catch (NoSuchMethodException e) {
      throw new SOFIException("L'objet EJBHome ne possède pas de méthode create().", e);
    }

    if (!Modifier.isPublic(createMethod.getModifiers())) {
      throw new SOFIException("La méhode create() doit être public");
    }
  }

  /**
   * Implémentation du callback pour chaque méthode du proxy.  Un EJB est
   * créé à partir du home, l'appel de méthode lui est délégué, ensuite
   * le EJB est libéré.
   */
  @Override
  public Object invoke(Object proxy, Method method, Object[] args)
      throws Throwable {
    EJBObject ejb;

    try {
      // Création de l'instance du EJB
      ejb = (EJBObject) createMethod.invoke(home, (Object[]) null);
    } catch (InvocationTargetException e) {
      throw new SOFIException("Erreur lors de la création de l'instance EJB à partir du EJBHome",
          e);
    } catch (IllegalAccessException e) {
      // Le constructeur vérifie que la méthode est publique, donc cette
      // exception ne peut pas se produire.
      throw new AssertionError(e);
    }

    try {
      // Délègue l'appel de méthode à l'instance du EJB et retourne le résultat
      return method.invoke(ejb, args);
    } catch (InvocationTargetException e) {
      throw e.getTargetException();
    } finally {
      try {
        ejb.remove();
      } catch (Exception e) {
        log.error("Erreur pour supprimer une instance de EJB", e);
      }
    }
  }

  /**
   * Retourne l'interface implémenté par le EJB dans un array
   * pour la création du proxy.
   */
  private Class[] getEjbInterfaces() {
    Class api = createMethod.getReturnType();

    if (!api.isInterface()) {
      throw new SOFIException(
          "Le type de retour de la méthode create() de l'objet " +
          "EJBHome doit être une interface.");
    }

    return new Class[] { api };
  }

  /**
   * Créer un proxy EJB à partir d'un objet EJBHome dans le ClassLoader par défaut.
   * @param home EJBHome à partir duquel il faut créer les EJBs.
   * @return Le proxy.
   */
  public static Object createProxy(EJBHome home) {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();

    return createProxy(home, cl);
  }

  /**
   * Créer un proxy EJB à partir d'un objet EJBHome dans le ClassLoader spécifié.
   * @param home EJBHome à partir duquel il faut créer les EJBs.
   * @param cl Le classLoader dans lequel la classe du proxy doit être définit.
   * @return Le proxy.
   */
  public static Object createProxy(EJBHome home, ClassLoader cl) {
    EjbProxy handler = new EjbProxy(home);

    return Proxy.newProxyInstance(cl, handler.getEjbInterfaces(), handler);
  }

  /**
   * Créer un proxy EJB à partir d'un objet EJBHome dans le ClassLoader par défaut.
   * @param home EJBHome à partir duquel il faut créer les EJBs.
   * @param api L'interface que le proxy doit implémenté.
   * @return Le proxy.
   */
  public static Object createProxy(EJBHome home, Class api) {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();

    return createProxy(home, api, cl);
  }

  /**
   * Créer un proxy EJB à partir d'un objet EJBHome dans le ClassLoader spécifié.
   * @param home EJBHome à partir duquel il faut créer les EJBs.
   * @param api L'interface que le proxy doit implémenté.
   * @param cl Le classLoader dans lequel la classe du proxy doit être définit.
   * @return Le proxy.
   */
  public static Object createProxy(EJBHome home, Class api, ClassLoader cl) {
    return Proxy.newProxyInstance(cl, new Class[] { api }, new EjbProxy(home));
  }
}
