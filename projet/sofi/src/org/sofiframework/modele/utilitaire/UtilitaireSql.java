/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.utilitaire;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe utilitaire sur la génération de requête SQL.
 * <p>
 * @author Jean-Francois Brassard
 * @version SOFI 2.1
 */
public class UtilitaireSql {

  /**
   * Instance du singleton
   */
  private static UtilitaireSql instance = new UtilitaireSql();

  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory.getLog(UtilitaireSql.class);

  /**
   * Constructeur de l'utilitaire.
   */
  protected UtilitaireSql() {
    super();
  }

  /**
   * Retourne l'instance du singleton.
   */
  public static UtilitaireSql getInstance() {
    return instance;
  }

  /**
   * Retourne une condition Sql tout en ajoutant un séparateur AND
   * au début de la condition si nécessaire.
   * @conditionSqlEnTraitement La condition SQL en traitement.
   * @conditionSqlSupplementaire La condition SQL Supplémentaire a ajouter.
   * @since SOFI 2.0.4
   */
  public static void ajouterConditionSql(StringBuffer conditionSqlEnTraitement,
      String conditionSqlSupplementaire) {

    ajouterSeparateurConditionSqlAnd(conditionSqlEnTraitement);
    conditionSqlEnTraitement.append(conditionSqlSupplementaire);
  }

  /**
   * Retourne une condition Sql pour un attribut et une liste d'objet.
   * @param Attribut l'attribut donc la condition Sql doit être généré.
   * @param liste la liste inclut dans le IN
   * @return la condition In.
   */
  public static void ajouterConditionSqlIn(StringBuffer conditionEnTraitement,
      String Attribut, Object[] liste) {

    if (liste != null && liste.length > 0) {
      ajouterSeparateurConditionSqlAnd(conditionEnTraitement);

      conditionEnTraitement.append(Attribut);

      conditionEnTraitement.append(" IN (");

      for (int i = 0; i < liste.length; i++) {
        if (i > 0) {
          conditionEnTraitement.append(",");
        }

        if (liste instanceof String[]) {
          conditionEnTraitement.append("'");
        }

        conditionEnTraitement.append(liste[i]);

        if (liste instanceof String[]) {
          conditionEnTraitement.append("'");
        }
      }

      conditionEnTraitement.append(")");
    }
  }

  /**
   * Retourne une condition Sql en terminant avec un AND s'il y lieu.
   * @return la condition Sql avec un AND s'il y a lieu.
   */
  public static void ajouterSeparateurConditionSqlAnd(StringBuffer conditionEnTraitement) {
    if (conditionEnTraitement != null && conditionEnTraitement.length() > 0) {
      int position =
          conditionEnTraitement.toString().trim().lastIndexOf("AND");
      if (!(position > 0 &&
          (position + 3 >= conditionEnTraitement.toString().length()))) {
        conditionEnTraitement.append(" AND ");
      }
    }
  }
}
