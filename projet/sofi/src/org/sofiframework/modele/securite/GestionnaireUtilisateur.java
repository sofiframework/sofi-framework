/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.securite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;

/**
 * Gère l'accessibilité à l'utilisateur, donne accès à l'utilisateur en cours.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface GestionnaireUtilisateur {

  /**
   * Fixe l'utilisateur pour qu'il puisse être récupéré plus tard.
   * @param utilisateur Objet utilisateur en cours
   */
  void setUtilisateur(Utilisateur utilisateur);

  /**
   * Obtenir l'utilisateur en cours.
   * @return Objet Utilisateur en cours
   */
  Utilisateur getUtilisateur();

  /**
   * Libère les ressources occupés par le maintient de la référence à l'utilisateur en cours.
   */
  void libererUtilisateur();

  /**
   * Obtenir le code utilisateur de l'utilisateur en cours.
   * @return
   */
  public String getCodeUtilisateur();
}
