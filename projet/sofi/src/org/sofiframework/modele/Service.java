/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele;

import java.sql.SQLException;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;

/**
 * Décrit les méthodes qui doivent être supportées par une classe de service.
 * @author Jean-François Brassard
 * @since 3.0.2
 */
public interface Service {
  /**
   * Initiliase une transaction.
   */
  void initialiserTransaction();

  /**
   * Commit de la transaction en cours.
   */
  void commit();

  /**
   * Rollback de la transaction en cours.
   */
  void rollback();

  /**
   * Libère les ressources associées à la transaction en cours.
   */
  void libererTransaction();

  /**
   * Retourne la connexion de la base de données courante du modèle.
   * @return la connexion de la base de données courante du modèle.
   * @since SOFI 2.1
   * @throws SQLException l'exception SQL survenue.
   */
  public java.sql.Connection getConnexionBD()  throws SQLException;

  /**
   * Permet d'obtenir l'utilisateur en cours.
   * 
   * @return Utilisateur
   */
  public Utilisateur getUtilisateur() ;

  /**
   * Fixe l'utilisateur en cours
   * 
   * @param gestionnaireUtilisateur Gestionnaire des utilisateurs
   */
  public void setGestionnaireUtilisateur(GestionnaireUtilisateur gestionnaireUtilisateur) ;

  /**
   * Obtenir le gestionnaire des utilisateurs.
   * 
   * @return Gesitonnaire des utilisateurs
   */
  public GestionnaireUtilisateur getGestionnaireUtilisateur() ;

  /**
   * Obtenir l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * 
   * @return
   */
  public Utilisateur getUtilisateurEnCours() ;


  /**
   * Obtenir le code l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * 
   * @return
   */
  public String getCodeUtilisateurEnCours() ;
}
