/*
 * Copyright 2003-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.modele.entite;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Entité qui supporte les descriptions localisé des entités.
 * 
 * @author Jean-François Brassard
 * @since 3.0.2
 */
public abstract class EntiteDescriptionLocale extends EntiteTrace  {

  private static final long serialVersionUID = 5897243141907156984L;

  // Description localisé pour l'entité.
  private DescriptionLocaleMap descriptionLocale = new DescriptionLocaleMap();

  // Description localisé courte pour l'entité.
  private DescriptionLocaleMap descriptionCourteLocale = new DescriptionLocaleMap();

  // La liste des descriptions locale.
  private List<DescriptionLocale> descriptionLocaleList;


  /**
   * 
   * @param description
   */
  public void setDescriptionLocale(DescriptionLocaleMap descriptionLocale) {
    this.descriptionLocale = descriptionLocale;
  }

  /**
   * 
   * @return
   */
  public DescriptionLocaleMap getDescriptionLocale() {
    return descriptionLocale;
  }

  /**
   * 
   * @param description
   */
  public void setDescriptionCourteLocale(DescriptionLocaleMap descriptionCourteLocale) {
    this.descriptionCourteLocale = descriptionCourteLocale;
  }

  /**
   * 
   * @return
   */
  public DescriptionLocaleMap getDescriptionCourteLocale() {
    return descriptionCourteLocale;
  }

  /**
   * 
   * @param descriptionLocaleList
   */
  public void setDescriptionLocaleList(List<DescriptionLocale> descriptionLocaleList) {
    this.descriptionLocaleList = descriptionLocaleList;
    if (descriptionLocaleList != null) {
      for (int i = 0; i < descriptionLocaleList.size(); i++) {
        DescriptionLocale descriptionLocaleTmp = descriptionLocaleList.get(i);
        getDescriptionLocale().put(descriptionLocaleTmp.getCode(),
            descriptionLocaleTmp.getDescription());

        if (descriptionLocaleTmp.getDescriptionCourte() != null) {
          getDescriptionCourteLocale().put(descriptionLocaleTmp.getCode(),
              descriptionLocaleTmp.getDescriptionCourte());
        }
      }
    }
  }

  /**
   * 
   * @return
   */
  public List<DescriptionLocale> getDescriptionLocaleList() {
    return descriptionLocaleList;
  }

  /**
   * Prepare description by locale list to update.
   */
  public void prepareDescriptionLocaleList() {

    HashSet<String> localePresent = new HashSet<String>();

    for (int i=0;i<getDescriptionLocaleList().size();i++) {
      DescriptionLocale descriptionLocale = getDescriptionLocaleList().get(i);
      String value = getDescriptionLocale().get(descriptionLocale.getCode());
      String valueShort = getDescriptionCourteLocale().get(descriptionLocale.getCode());
      if ("".equals(value)) {
        getDescriptionLocaleList().remove(i);
      }else {
        descriptionLocale.setDescription(value);
        descriptionLocale.setDescriptionCourte(valueShort);
      }
      localePresent.add(descriptionLocale.getCode());
    }


    Iterator iter = getDescriptionLocale().values().iterator();
    while (iter.hasNext()) {
      String code = (String)iter.next();
      if (!localePresent.contains(code) || localePresent.size() == 0){
        DescriptionLocale descriptionLocale = new DescriptionLocale();
        descriptionLocale.setCode(code);
        descriptionLocale.setDescription(getDescriptionLocale().get(code));
        descriptionLocale.setDescriptionCourte(getDescriptionCourteLocale().get(code));
        descriptionLocale.setCleEtrangere(getId());
        getDescriptionLocaleList().add(descriptionLocale);
      }
    }

  }

}
