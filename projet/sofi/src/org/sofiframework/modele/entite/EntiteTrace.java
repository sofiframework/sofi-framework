/*
 * Copyright 2003-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.modele.entite;

import java.util.Date;

/**
 * Entité qui supporte les informations de traces qui
 * sont utilisés par chaque entité de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 * @since 3.0.2
 */
public abstract class EntiteTrace extends Entite implements Tracable {

  private static final long serialVersionUID = 7342023860959142240L;

  private String creePar;
  private String modifiePar;
  private Date dateCreation;
  private Date dateModification;
  private Integer version;


  @Override
  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  @Override
  public String getCreePar() {
    return creePar;
  }

  @Override
  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  @Override
  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  @Override
  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  @Override
  public String getModifiePar() {
    return modifiePar;
  }

  @Override
  public void setDateModification(java.util.Date dateModification) {
    this.dateModification = dateModification;
  }

  @Override
  public java.util.Date getDateModification() {
    return dateModification;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Integer getVersion() {
    return version;
  }

}
