package org.sofiframework.modele.entite;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.sofiframework.application.message.GestionLibelle;

/**
 * Map avec la description d'un élément x pour une locale.
 * <p>
 * 
 * @author jfbrassard
 * @since 3.0.2
 */
public class DescriptionLocaleMap extends HashMap<String, String> {

  private static final long serialVersionUID = -5427747639838355639L;

  /** Constructeur par défaut */
  public DescriptionLocaleMap() {
  }

  @Override
  public String get(Object key) {
    if (this.getHasDescription()) {
      Locale localeToApply = GestionLibelle.getInstance().getLocaleDisponible((String) key);
      return super.get(localeToApply.toString());
    }
    return "";
  }

  public boolean getHasDescription() {
    return !this.isEmpty();
  }

  /**
   * Obtenir la liste des langues disponibles
   * 
   * @return set de langues
   */
  public Set<String> getLangues() {
    return this.keySet();
  }

  /**
   * Obtenir la liste des descriptions.
   * 
   * @return collection de descriptions
   */
  public Collection<String> getDescriptions() {
    return this.values();
  }

  @Override
  public String toString() {
    StringBuffer b = new StringBuffer("{");
    for (Map.Entry<String, String> entry : this.entrySet()) {
      b.append(entry.getKey()).append(" : ").append(entry.getValue()).append(",");
    }
    return b.append("}").toString();
  }
}
