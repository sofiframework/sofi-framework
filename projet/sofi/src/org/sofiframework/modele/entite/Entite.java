package org.sofiframework.modele.entite;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Classe de base d'une entité d'affaires.
 * <p>
 * @author Jean-François Brassard
 * @since 3.0.2
 */

public class Entite extends ObjetTransfert {

  private static final long serialVersionUID = 1123201495178778304L;

  private Long id;
  
  public Entite() {
    super();
    if (getCle() == null) {
      setCle(new String[] { "id" });
    }
  }


  /**
   * 
   * @return
   */
  public Long getId() {
    return id;
  }

  /**
   * 
   * @param id
   */
  public void setId(Long id) {
    this.id = id;
  }


}
