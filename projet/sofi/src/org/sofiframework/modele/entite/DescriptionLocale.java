package org.sofiframework.modele.entite;


/**
 * Entite pour décrire une description par locale pour les applications qui
 * offre plusieurs localisation.
 * <p>
 * <author> Jean-François Brassard <jfbrassard@sofiframework.org>
 * @since 3.0.2
 *
 */

public class DescriptionLocale extends EntiteTrace {

  private static final long serialVersionUID = -1481858212988312443L;

  //
  private Long id;

  //
  private Object cleEtrangere;

  // Le code de la locale.
  private String code;

  // La description pour la locale.
  private String description;

  // La description pour la locale.
  private String descriptionCourte;

  /**
   * @param id the id to set
   */
  @Override
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the id
   */
  @Override
  public Long getId() {
    return id;
  }

  /**
   * @param cleEtrangere the cleEtrangere to set
   */
  public void setCleEtrangere(Object cleEtrangere) {
    this.cleEtrangere = cleEtrangere;
  }

  /**
   * @return the cleEtrangere
   */
  public Object getCleEtrangere() {
    return cleEtrangere;
  }

  /**
   * 
   * @param code
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * 
   * @return
   */
  public String getCode() {
    return code;
  }


  /**
   * 
   * @param description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * 
   * @return
   */
  public String getDescription() {
    return description;
  }


  /**
   * 
   * @param description
   */
  public void setDescriptionCourte(String descriptionCourte) {
    this.descriptionCourte = descriptionCourte;
  }

  /**
   * 
   * @return
   */
  public String getDescriptionCourte() {
    return descriptionCourte;
  }



}
