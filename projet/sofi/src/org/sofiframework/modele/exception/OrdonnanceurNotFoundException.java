/**
 * 
 */
package org.sofiframework.modele.exception;

/**
 * @author rmercier
 *
 */
public class OrdonnanceurNotFoundException extends ModeleException {

  /**
   * 
   */
  private static final long serialVersionUID = 6632335492864864194L;

  public OrdonnanceurNotFoundException(String message) {
    super(message);
  }

}
