/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.exception;

import org.sofiframework.exception.SOFIException;


/**
 * Exception de base pour les toutes exceptions causé
 * dans la couche du modele.
 * @author Jean-François Brassard (Nurun inc.)
 * @since SOFI 1.8 ModeleException hérite maintenant de SOFIException;
 */
public class ModeleException extends SOFIException {
  // Compatiblité avec SOFI 1.8.1 et inférieure
  private static final long serialVersionUID = 3594255454314979986L;
  private String codeErreur;
  private Object[] detail;

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * @param message le message de base.
   */
  public ModeleException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public ModeleException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * avec la cause.
   * @param cause la cause.
   */
  public ModeleException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public ModeleException(String message, Throwable cause, String codeErreur,
      Object[] detail) {
    super(message, cause);
    setCodeErreur(codeErreur);
    setDetail(detail);
  }

  public String getCodeErreur() {
    return codeErreur;
  }

  public void setCodeErreur(String codeErreur) {
    this.codeErreur = codeErreur;
  }

  public Object[] getDetail() {
    return detail;
  }

  public void setDetail(Object[] detail) {
    this.detail = detail;
  }
}
