/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.exception;


/**
 * Exception de base pour les toutes exceptions causé
 * dans le cas de concurrence de données.
 * @author Martin Guay
 */
public class ConcurrenceException extends ModeleException {
  /**
   * 
   */
  private static final long serialVersionUID = 3529942490421973915L;

  /**
   * Constructeur d'une exception spécifiant une erreur de concurrence
   * @param message le message de base.
   */
  public ConcurrenceException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur de concurrence
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public ConcurrenceException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur de concurrence
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public ConcurrenceException(String message, Throwable cause,
      String codeErreur, Object[] detail) {
    super(message, cause);
    setCodeErreur(codeErreur);
    setDetail(detail);
  }
}
