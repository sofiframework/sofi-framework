/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.distant;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.thread.Minuterie;


/**
 * Permet d'instancier des services distants de type EJB.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class ServiceDistantFactory {
  /**
   * Instance commune de journalisation.
   */
  protected static Log log = LogFactory.getLog(ServiceDistantFactory.class);

  /** Constructeur par défaut */
  public ServiceDistantFactory() {
  }

  /**
   * Obtenir une instance de service distant a partir du contexte initial.
   * AVERTISSEMENT : La classe Home du EJB doit absolument
   * posséder une méthode create.
   * @param contexteService
   * @return Service distant
   */
  public synchronized static EJBObject get(
      MetaDonneeServiceDistant contexteService)
          throws NamingException, ClassNotFoundException, NoSuchMethodException,
          IllegalAccessException, InvocationTargetException {
    Context contexte = null;
    Class classeEJBHome = null;
    EJBHome home = null;
    Method create = null;
    EJBObject ejb = null;

    log.warn("Creation du EJB " + contexteService.getNom());

    // Obtenir le contexte initial.
    try {
      contexte = contexteService.getInitialContext();
    } catch (NamingException e) {
      throw new SOFIException("Erreur lors de l'initialisation du contexte.", e);
    }

    // Obtenir la classe Home a partir de son nom.
    try {
      classeEJBHome = Class.forName(contexteService.getNomClasseHome());
    } catch (ClassNotFoundException e) {
      StringBuffer messException = new StringBuffer();
      messException.append("Class du Home du home non trouvé (");
      messException.append(contexteService.getNomClasseHome());
      messException.append(")");
      throw new SOFIException(messException.toString());
    } catch (Throwable t) {
      throw new SOFIException(t);
    }

    // Obtenir un objet home.
    try {
      home = (EJBHome) PortableRemoteObject.narrow(contexte.lookup(
          contexteService.getNom()), classeEJBHome);
    } catch (ClassCastException e) {
      StringBuffer messException = new StringBuffer();
      messException.append("Narrow de l'objet Home (");
      messException.append(contexteService.getNomClasseHome());
      messException.append(")");
      throw new SOFIException(messException.toString());
    } catch (Throwable t) {
      throw new SOFIException(t);
    }

    // Obtenir la méthode de création du EJB.
    try {
      create = home.getClass().getMethod("create", new Class[] {  });
    } catch (NoSuchMethodException e) {
      StringBuffer messException = new StringBuffer();
      messException.append("Méthode create du Home (");
      messException.append(contexteService.getNomClasseHome());
      messException.append(")");
      throw new SOFIException(messException.toString());
    } catch (Throwable t) {
      log.error("Erreur inatendue", t);
    }

    // Créer l'objet EJB.
    try {
      ejb = (EJBObject) create.invoke(home, new Object[] {  });
    } catch (IllegalAccessException e) {
      log.error("Invocation de la méthode create du Home (" +
          contexteService.getNomClasseHome() + ")");
    } catch (InvocationTargetException e) {
      log.error("Invocation de la méthode create du Home (" +
          contexteService.getNomClasseHome() + ")");
    } catch (Throwable t) {
      log.error("Erreur inatendue", t);
    }

    return ejb;
  }

  public synchronized static EJBObject getSecure(
      MetaDonneeServiceDistant contexteService) throws Exception {
    Minuterie minutable = new Minuterie();
    ServiceDistantFactoryExecutable traitement = new ServiceDistantFactoryExecutable();
    traitement.setContexteService(contexteService);

    // Si l'instanciation n'est pas faite en 15 secondes on a un probleme
    minutable.executer(traitement,
        Thread.currentThread().getContextClassLoader(), 15000L);

    EJBObject ejb = traitement.getEjb();

    if (ejb == null) {
      Exception e = traitement.getErreur();

      if (e != null) {
        throw e;
      } else {
        throw new SOFIException("Incapable d'instancier le EJB " +
            contexteService.getNom() +
            " la construction du EJB n'a retourné aucune exception.");
      }
    }

    return ejb;
  }

  /**
   * Obtenir un ou plusieurs services distants a partir d'une liste de contextes
   * @param contextesServices liste de contextes
   * @return un ou plusieurs services distants
   */
  public synchronized static Hashtable get(ArrayList contextesServices)
      throws Exception {
    Hashtable services = new Hashtable();

    for (Iterator i = contextesServices.iterator(); i.hasNext();) {
      MetaDonneeServiceDistant contexte = (MetaDonneeServiceDistant) i.next();
      EJBObject service = ServiceDistantFactory.get(contexte);
      services.put(contexte.getNom(), service);
    }

    return services;
  }
}
