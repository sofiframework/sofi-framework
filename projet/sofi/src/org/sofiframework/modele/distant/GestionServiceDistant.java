/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.distant;

import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import javax.ejb.EJBObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.base.RemoteProxy;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireException;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.UtilitaireXml;
import org.w3c.dom.Node;


/**
 * Centralise le services distants dans un singleton.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version SOFI 1.0
 */
public class GestionServiceDistant {
  /**
   * Instance commune de journalisation.
   */
  protected static Log log = LogFactory.getLog(GestionServiceDistant.class);

  /** Services distants en problème*/
  private static HashSet servicesDistantsEnProbleme = new HashSet();

  /**
   * Liste des services qui sont configurés dans le gestionnaire des service distant
   */
  private static Hashtable servicesDistantsConfigures = new Hashtable();

  /** La liste des contextes des services distants **/
  private static ArrayList listeContextes = null;

  /** La liste des services qui sont définis dans le fichier des services-distants.xml **/
  private static HashSet listeServicesDefinis = null;

  /**
   * Permet de construire les services distants à partir de la liste des contextes.
   * @throws org.sofiframework.exception.SOFIException
   */
  private static void construireServicesDistants() throws SOFIException {
    synchronized (servicesDistantsConfigures) {
      if (listeContextes != null) {
        // Construire un nouveau service distant pour tous les services qui n'existent pas déjà
        for (java.util.Iterator i = listeContextes.iterator(); i.hasNext();) {
          MetaDonneeServiceDistant contexte = (MetaDonneeServiceDistant) i.next();

          // Contruire un nouveau service distant
          if (!isServiceDistantConfigure(contexte.getNom())) {
            try {
              Object service = ServiceDistantFactory.getSecure(contexte);

              if (log.isWarnEnabled()) {
                log.warn("Service distant construit: " + service);
              }

              servicesDistantsConfigures.put(contexte.getNom(), service);
            } catch (Exception e) {
              // JMP On reessaie une derniere foi au cas ou ...
              // Faut pas mittrailler le serveur EJB on doit attendre un peu avant de réessayer 30 sec.
              // Surtout dans le cas ou le seveur est aussi en démarrage...
              if (log.isWarnEnabled()) {
                log.warn("On doit attendre avant de se réessayer ... ");
              }

              try {
                Thread.sleep(3000L);

                if (log.isWarnEnabled()) {
                  log.warn("2ieme essai de connexion de l'ejb" +
                      contexte.getNom());
                }

                Object service = ServiceDistantFactory.getSecure(contexte);
                servicesDistantsConfigures.put(contexte.getNom(), service);
              } catch (Exception e2) {
                log.error(e);
                throw new SOFIException(genererException(contexte, e2));
              }
            }

            // Retirer le service en problème si la connexion s'est fait.
            if (servicesDistantsEnProbleme.contains(contexte.getNom())) {
              servicesDistantsEnProbleme.remove(contexte.getNom());
            }
          }
        }
      }
    }
  }

  /**
   * Permet de construire les services distants contenu dans le fichier passé en paramètre.
   * Cette méthode est utilisée uniquement par le plugin des services distants.
   * @param fichierConfig
   * @throws org.sofiframework.exception.SOFIException
   */
  public static void construireServicesDistants(String fichierConfig)
      throws SOFIException {
    try {
      listeContextes = MetaDonneeServiceDistant.getContextes(fichierConfig);
      construireServicesDistants();
    } catch (SOFIException se) {
      throw se;
    } catch (Exception e) {
      new SOFIException(e);
    }
  }

  /**
   * Permet de contruire le service distant qui a son nom passé en paramètre.
   * @param nom
   * @throws org.sofiframework.exception.SOFIException
   */
  private static void construireServiceDistant(String nom)
      throws SOFIException {
    synchronized (servicesDistantsConfigures) {
      if (listeContextes != null) {
        // Construire un nouveau service distant pour tous les servuices qui n'existent pas déjà
        for (java.util.Iterator i = listeContextes.iterator(); i.hasNext();) {
          MetaDonneeServiceDistant contexte = (MetaDonneeServiceDistant) i.next();

          // Contruire un nouveau service distant
          if (!isServiceDistantConfigure(contexte.getNom()) &&
              (nom.compareTo(contexte.getNom()) == 0)) {
            try {
              Object service = ServiceDistantFactory.getSecure(contexte);

              if (log.isWarnEnabled()) {
                log.warn("Service distant construit: " + service);
              }

              servicesDistantsConfigures.put(contexte.getNom(), service);
            } catch (Exception e) {
              // JMP On reessaie une derniere foi au cas ou ...
              // Faut pas mittrailler le serveur EJB on doit attendre un peu avant de réessayer 30 sec.
              // Surtout dans le cas ou le seveur est aussi en démarrage...
              if (log.isWarnEnabled()) {
                log.warn("On doit attendre avant de se réessayer ... ");
              }

              try {
                Thread.sleep(3000L);

                if (log.isWarnEnabled()) {
                  log.warn("2ieme essai de connexion de l'ejb" +
                      contexte.getNom());
                }

                Object service = ServiceDistantFactory.getSecure(contexte);
                servicesDistantsConfigures.put(contexte.getNom(), service);
              } catch (Exception e2) {
                log.error(e);
                throw new SOFIException(genererException(contexte, e2));
              }
            }

            // Retirer le service en problème si la connexion s'est fait.
            if (servicesDistantsEnProbleme.contains(contexte.getNom())) {
              servicesDistantsEnProbleme.remove(contexte.getNom());
            }
          }
        }
      }
    }
  }

  /**
   * Libère les services distants instanciés.
   */
  public static void release() {
    synchronized (servicesDistantsConfigures) {
      try {
        for (Enumeration cles = servicesDistantsConfigures.keys();
            cles.hasMoreElements();) {
          Object objet = servicesDistantsConfigures.get(cles.nextElement());

          if (objet instanceof EJBObject) {
            ((EJBObject) objet).remove();
          }
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Obtenir un service distant à partir de son nom unique
   * @param nom nom du service distant
   * @return Objet EJB correspondant unn service distant demandé.
   */
  public static Object getServiceDistant(String nom) {
    if (!isServiceDistantConfigure(nom)) {
      log.warn(
          "Service distant non configuré, la configuration est en cours...");
      configurationServiceDistant(nom);
    }

    // Vérifier si le service distant est en problème
    // Si oui, tenter de le reconstruire.
    if (servicesDistantsEnProbleme.contains(nom)) {
      // Retirer l'ejb en cache.
      servicesDistantsConfigures.remove(nom);

      // Reconstruire l'ejb retiré en cache.
      configurationServiceDistant(nom);
    }

    Object o = servicesDistantsConfigures.get(nom);

    if (o == null) {
      if (log.isErrorEnabled()) {
        log.error(
            "Le service distant demandé est absent de la gestion des services distants.");
        log.error("Service demandé      : " + nom);
        log.error("Services disponibles : " +
            servicesDistantsConfigures.keySet());
      }

      try {
        // Retirer le service en cache.
        servicesDistantsConfigures.remove(nom);

        // Reconstruire l'ejb retiré en cache.
        configurationServiceDistant(nom);
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur lors du transtypage (casting) de l'objet en EJBObject.",
              e);
        }
      }
    } else if (log.isInfoEnabled()) {
      log.info("Classe du service demandé est : " + o.getClass().getName());

      Class[] interfaces = o.getClass().getInterfaces();
      log.info("Interfaces implémentées par le service distant ...");

      for (int i = 0; i < interfaces.length; i++) {
        log.info(i + " - " + interfaces[i].getName());
      }
    }

    return o;
  }

  /**
   * Configuration des services distants
   * <p>
   * L'attribut 'nomClasseModele' doit être configuré dans le fichier des paramètres systeme
   * ou encore l'attribut 'fichierConfigurationServiceEJB'.
   * @throws org.sofiframework.exception.SOFIException
   */
  static public void configurationServicesDistant() throws SOFIException {
    GestionServiceDistant.construireListeContextes();

    // Construire les services distants.
    GestionServiceDistant.construireServicesDistants();
  }

  /**
   * Configuration des services distants
   * <p>
   * L'attribut 'nomClasseModele' doit être configuré dans le fichier des paramètres systeme
   * ou encore l'attribut 'fichierConfigurationServiceEJB'.
   * @param nom
   * @throws org.sofiframework.exception.SOFIException
   */
  static private void configurationServiceDistant(String nom)
      throws SOFIException {
    GestionServiceDistant.construireListeContextes();

    // Construire les services distants.
    GestionServiceDistant.construireServiceDistant(nom);
  }

  /**
   * Spécifie si le service distant existe dans la liste
   * des services distants pouvant être configurés.
   * @throws org.sofiframework.exception.SOFIException
   * @return true si le service distant existe dans la liste
   * des services distants pouvant être configurés.
   * @param nom Le nom de service à valider.
   */
  static public boolean isServiceDistantExiste(String nom)
      throws SOFIException {
    GestionServiceDistant.construireListeContextes();

    if (listeServicesDefinis == null) {
      listeServicesDefinis = new HashSet();

      if (listeContextes != null) {
        for (Iterator iterateur = listeContextes.iterator();
            iterateur.hasNext();) {
          MetaDonneeServiceDistant contexte = (MetaDonneeServiceDistant) iterateur.next();
          listeServicesDefinis.add(contexte.getNom());
        }
      }
    }

    return listeServicesDefinis.contains(nom);
  }

  /**
   * Permet de contruire la liste des contextes.
   * @throws org.sofiframework.exception.SOFIException
   */
  synchronized static public void construireListeContextes()
      throws SOFIException {
    String nomClasseModele = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.NOM_CLASSE_MODELE);

    if (listeContextes == null) {
      String fichierConfigurationServiceEJB = GestionParametreSysteme.getInstance()
          .getString(ConstantesParametreSysteme.FICHIER_CONFIG_SERVICE_EJB);

      if (!UtilitaireString.isVide(fichierConfigurationServiceEJB)) {
        try {
          File fichier = null;

          if (fichierConfigurationServiceEJB.indexOf("..") != -1) {
            String emplacementWebInf = GestionParametreSysteme.getInstance()
                .getEmplacementWebInf();
            fichier = new File(emplacementWebInf, fichierConfigurationServiceEJB);

            if (emplacementWebInf == null) {
              fichierConfigurationServiceEJB = fichierConfigurationServiceEJB.substring(3);
              fichier = new File(emplacementWebInf,
                  fichierConfigurationServiceEJB);
            }
          } else {
            fichier = new File(fichierConfigurationServiceEJB);
          }

          if (fichier != null) {
            log.warn("Emplacement du fichier service distant utilisé : " +
                fichier.getPath());
          }

          FileInputStream inputStream = new FileInputStream(fichier);
          Node premierNode = UtilitaireXml.convertirFichierEnDocument(inputStream);
          inputStream.close();
          inputStream = null;

          // Construire la liste des contextes des services distants.
          // Obtenir tous les contextes qui sont dans le fichier de configuration
          try {
            listeContextes = MetaDonneeServiceDistant.getContextes(premierNode);
          } catch (Exception e) {
            new SOFIException(e);
          }
        } catch (SOFIException e) {
          throw e;
        } catch (Exception e) {
          throw new SOFIException(e);
        }
      }
    }
  }

  /**
   * Est-ce que le service distant est configuré?
   * @param nom du service
   * @return true si le service distant passé en paramètre est configuré.
   */
  public static boolean isServiceDistantConfigure(String nom) {
    return servicesDistantsConfigures.containsKey(nom);
  }

  private static String genererException(MetaDonneeServiceDistant contexte,
      Exception e) {
    StringBuffer exception = new StringBuffer();
    exception.append("Erreur de connexion du service distant suivant : \n");
    exception.append(contexte.getNom());
    exception.append(" à l'adresse suivante: ");
    exception.append(contexte.getProviderUrl());
    exception.append("\n");
    exception.append(UtilitaireException.getMessageExceptionAuComplet(e));

    return exception.toString();
  }

  /**
   * Spécifier que le service distant spécifier est en problème.
   * @param nomService le nom du service distant en problème.
   */
  public synchronized static void specifierServiceDistantEnProbleme(
      String nomService, RemoteException e) {
    servicesDistantsEnProbleme.add(nomService);

    StringBuffer messageErreur = new StringBuffer();
    messageErreur.append("Le service distant suivant est en problème : ");
    messageErreur.append(nomService);
    messageErreur.append(e.getMessage());
    throw new SOFIException(messageErreur.toString());
  }

  /**
   * Permet d'ajouter une instance de service distant dans le gestionnaire.
   * @param service Service
   * @param nom Nom du service
   */
  public static void ajouterServiceDistant(String nom, Object service) {
    if (listeServicesDefinis == null) {
      listeServicesDefinis = new HashSet();
    }

    listeServicesDefinis.add(nom);
    servicesDistantsConfigures.put(nom, service);
  }

  /**
   * Obtenir la version avec interface locale d'un service.
   * Si l'objet est un EJB, on utilise un proxy pour déléguer au ejb.
   * @return Service désiré
   * @param interfaceLocale Interface que l'on désire utiliser pour interroger le service.
   * @param nomService Nom du service désiré
   */
  public static Object getServiceLocal(String nomService, Class interfaceLocale) {
    Object service = null;
    Object inconnu = getServiceDistant(nomService);

    if (inconnu instanceof EJBObject) {
      service = RemoteProxy.creerProxy(inconnu, interfaceLocale);
    } else {
      service = inconnu;
    }

    return service;
  }
}
