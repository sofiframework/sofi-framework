/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.distant;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;

import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireXml;
import org.sofiframework.utilitaire.fichier.FichierTexte;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


/**
 * Objet d'affaires décrivant un contexte d'accès à un service distant EJB.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class MetaDonneeServiceDistant extends ObjetTransfert {
  private static final long serialVersionUID = -5682373736934539722L;

  /**
   * Nom de la balise qui délimite la liste des contextes de services.
   */
  public static final String NOM_LISTE_NOEUD_XML = "contextes-services-distants";

  /**
   * Nom de la balise qui délimite un contexte de service distant.
   */
  public static final String NOM_NOEUD_XML = "contexte-service-distant";

  /** Nom du EJB. */
  private String nom = "";

  /** Nom de classe du Home EJB. */
  private String nomClasseHome = "";

  /** Classe qui construit le context initial. */
  private String contextInitialFactory = "com.evermind.server.rmi.RMIInitialContextFactory";

  /** Nom d'usager pour l'accès au EJB. */
  private String securityPrincipal = "admin";

  /** Mot de passe pour l'accès au EJB. */
  private String securityCredential = "welcome";

  /** URL du EJB. */
  private String providerUrl = "ormi://localhost:23891/current-workspace-app";

  /** Constructeur par défaut */
  public MetaDonneeServiceDistant() {
  }

  /**
   * Obtenir un context initial pour la communication avec un service distant.
   * @return Contexte initial
   * @throws javax.naming.NamingException
   */
  public InitialContext getInitialContext() throws NamingException {
    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY, this.getContextInitialFactory());
    env.put(Context.SECURITY_PRINCIPAL, this.getSecurityPrincipal());
    env.put(Context.SECURITY_CREDENTIALS, this.getSecurityCredential());
    env.put(Context.PROVIDER_URL, this.getProviderUrl());
    env.put("dedicated.connection", "true");

    return new InitialContext(env);
  }

  /**
   * Popule le contexte avec un fichier XML.
   * @param resourceXML nom de la resource XML qui sert a populer l'objet de transfert.
   * @throws org.xml.sax.SAXException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  public void lireRessourceXml(String resourceXML) throws IOException {
    // Flux obtenu avec le nom de resource
    InputStream in = getClass().getResourceAsStream(resourceXML);

    try {
      lireXml(in);
    } finally {
      try {
        in.close();
      } catch (IOException e) {
      }
    }
  }

  /**
   * Popule le contexte avec un fichier XML.
   * @param resourceXML nom de la resource XML qui sert a populer l'objet de transfert.
   * @throws org.xml.sax.SAXException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  public void lireXml(Document /*XMLDocument*/ documentXML)
      throws SAXException, IOException, ParserConfigurationException {
    // Noeud d départ du document

    /*Node parent = documentXML.getFirstChild();*/
    Node parent = documentXML.getParentNode();

    this.lireXml(parent);
  }

  /**
   * Permet de produire une liste de contextes  à partir d'un fichier de configuration contenant
   * un ou plusieurs configurations de services distants
   * @param fichierConfiguration Chemin d'accès au fichier XML de configuration de services distants.
   * @return une liste de contexte pour configurer les services distants
   */
  public static ArrayList getContextes(String fichierConfiguration)
      throws SAXException, IOException, ParserConfigurationException {
    // Obtenir les noeuds qui sont des services distants
    Node parentNode = UtilitaireXml.convertirFichierEnDocument(new FichierTexte(
        fichierConfiguration, ""));
    ArrayList listeNoeuds = UtilitaireXml.getListeNodeDansUneNode(parentNode,
        NOM_LISTE_NOEUD_XML);

    //liste de contextes
    ArrayList listeContexte = new ArrayList();

    for (Iterator i = listeNoeuds.iterator(); i.hasNext();) {
      Node noeud = (Node) i.next();
      MetaDonneeServiceDistant contexte = new MetaDonneeServiceDistant();

      // Le nom de classe est remplacé par une version plus literale
      contexte.ajouterAliasXml(NOM_NOEUD_XML, MetaDonneeServiceDistant.class);
      contexte.lireXml(noeud);
      listeContexte.add(contexte);
    }

    return listeContexte;
  }

  public static ArrayList getContextes(Node parentNode)
      throws SAXException, IOException, ParserConfigurationException {
    // Obtenir les noeuds qui sont des services distants
    ArrayList listeNoeuds = UtilitaireXml.getListeNodeDansUneNode(parentNode,
        NOM_LISTE_NOEUD_XML);

    //liste de contextes
    ArrayList listeContexte = new ArrayList();

    for (Iterator i = listeNoeuds.iterator(); i.hasNext();) {
      Node noeud = (Node) i.next();
      MetaDonneeServiceDistant contexte = new MetaDonneeServiceDistant();

      // Le nom de classe est remplacé par une version plus literale
      contexte.ajouterAliasXml(NOM_NOEUD_XML, MetaDonneeServiceDistant.class);
      contexte.lireXml(noeud);
      listeContexte.add(contexte);
    }

    return listeContexte;
  }

  /**
   * Fixer le nom de classe de création des contextes de servides distants.
   * @param contextInitialFactory nom classe de création des servides
   */
  public void setContextInitialFactory(String contextInitialFactory) {
    this.contextInitialFactory = contextInitialFactory;
  }

  /**
   * Obtenir le nom de classe de création des contextes de servides distants.
   * @return nom de classe de création des contextes de servides distants
   */
  public String getContextInitialFactory() {
    return contextInitialFactory;
  }

  /**
   * Fixer le nom d'usager pour l'accès au EJB.
   * @param securityPrincipal nom d'usager pour l'accès au EJB.
   */
  public void setSecurityPrincipal(String securityPrincipal) {
    this.securityPrincipal = securityPrincipal;
  }

  /**
   * Obtenir le mot de passe pour l'accès au EJB.
   * @return le mot de passe pour l'accès au EJB.
   */
  public String getSecurityPrincipal() {
    return securityPrincipal;
  }

  /**
   * Fixer le mot de passe pour l'accès au EJB.
   * @param securityCredential Mot de passe pour l'accès au EJB
   */
  public void setSecurityCredential(String securityCredential) {
    this.securityCredential = securityCredential;
  }

  /**
   * Obtenir le mot de passe pour l'accès au EJB.
   * @return Mot de passe pour l'accès au EJB
   */
  public String getSecurityCredential() {
    return securityCredential;
  }

  /**
   * Fixer l'URL du EJB.
   * @param providerUrl URL du EJB
   */
  public void setProviderUrl(String providerUrl) {
    this.providerUrl = providerUrl;
  }

  /**
   * Obtenir l'URL du EJB
   * @return URL du EJB
   */
  public String getProviderUrl() {
    return providerUrl;
  }

  /**
   * Fixer le nom de l'EJB.
   * @param nom le nom de l'EJB
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le nom de l'EJB.
   * @return le nom de l'EJB
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de classe du Home EJB.
   * @param nomClasseHome le nom de classe du Home EJB
   */
  public void setNomClasseHome(String nomClasseHome) {
    this.nomClasseHome = nomClasseHome;
  }

  /**
   * Obtenir le nom de classe du Home EJB.
   * @return le nom de classe du Home EJB
   */
  public String getNomClasseHome() {
    return nomClasseHome;
  }
}
