/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.distant;

import java.lang.reflect.InvocationTargetException;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.ejb.EjbProxy;


public class ServiceDistantFactoryExecutable implements Runnable {
  /**
   * Instance commune de journalisation.
   */
  protected static Log log = LogFactory.getLog(ServiceDistantFactory.class);
  private EJBObject ejb = null;
  private MetaDonneeServiceDistant contexteService = null;
  private Exception erreur = null;

  public ServiceDistantFactoryExecutable() {
  }

  @Override
  public void run() {
    try {
      this.ejb = this.get(this.contexteService);
    } catch (Exception e) {
      erreur = e;
    }

    if (erreur != null) {
      this.ejb = null;
      this.contexteService = null;
    }
  }

  /**
   * Obtenir une instance de service distant a partir du contexte initial.
   * AVERTISSEMENT : La classe Home du EJB doit absolument
   * posséder une méthode create.
   * @param contexteService
   * @return Service distant
   */
  private EJBObject get(MetaDonneeServiceDistant contexteService)
      throws NamingException, ClassNotFoundException, NoSuchMethodException,
      IllegalAccessException, InvocationTargetException, SOFIException {
    Context contexte = null;
    Class classeEJBHome = null;
    EJBHome home = null;

    if (log.isWarnEnabled()) {
      log.warn("Creation du EJB " + contexteService.getNom());
    }

    // Obtenir le contexte initial.
    try {
      contexte = contexteService.getInitialContext();
    } catch (NamingException e) {
      throw new SOFIException("Erreur lors de l'initialisation du contexte.", e);
    }

    // Obtenir la classe Home a partir de son nom.
    try {
      classeEJBHome = Class.forName(contexteService.getNomClasseHome());
    } catch (ClassNotFoundException e) {
      StringBuffer messException = new StringBuffer();
      messException.append("Class du Home du home non trouvé (");
      messException.append(contexteService.getNomClasseHome());
      messException.append(")");
      throw new SOFIException(messException.toString(), e);
    } catch (Throwable t) {
      throw new SOFIException(t);
    }

    // Obtenir un objet home.
    try {
      home = (EJBHome) PortableRemoteObject.narrow(contexte.lookup(
          contexteService.getNom()), classeEJBHome);
    } catch (ClassCastException e) {
      StringBuffer messException = new StringBuffer();
      messException.append("Narrow de l'objet Home (");
      messException.append(contexteService.getNomClasseHome());
      messException.append(")");
      throw new SOFIException(messException.toString(), e);
    } catch (Throwable t) {
      throw new SOFIException(t);
    }

    return (EJBObject) EjbProxy.createProxy(home);
  }

  public void setContexteService(MetaDonneeServiceDistant contexteService) {
    this.contexteService = contexteService;
  }

  public MetaDonneeServiceDistant getContexteService() {
    return contexteService;
  }

  public void setEjb(EJBObject ejb) {
    this.ejb = ejb;
  }

  public EJBObject getEjb() {
    return ejb;
  }

  public void setErreur(Exception erreur) {
    this.erreur = erreur;
  }

  public Exception getErreur() {
    return erreur;
  }
}
