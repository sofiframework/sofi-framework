package org.sofiframework.modele.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Objet de filtrage de base lorsque sur des entités avec de la journalisation (trace) sur
 * les transactions.
 * @author jfbrassard
 * @since 3.0.2
 */

public class FiltreTrace extends ObjetFiltre {

  private static final long serialVersionUID = 9013433649352352040L;

  private String creePar;
  private String modifiePar;
  private Date dateCreation;
  private Date dateCreationDebut;
  private Date dateCreationFin;
  private Date dateModification;
  private Date dateModificationDebut;
  private Date dateModificationFin;
  private Integer version;

  /**
   * Constructeur par défaut.
   */
  public FiltreTrace() {
    ajouterIntervalle("dateCreationDebut", "dateCreationFin",
        "dateCreation");
    ajouterIntervalle("dateModificationDebut", "dateModificationFin",
        "dateModification");
  }

  /**
   * 
   * @param creePar
   */
  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  /**
   * 
   * @return
   */
  public String getCreePar() {
    return creePar;
  }

  /**
   * 
   * @param dateCreation
   */
  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  /**
   * 
   * @return
   */
  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  /**
   * 
   * @param modifiePar
   */
  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  /**
   * 
   * @return
   */
  public String getModifiePar() {
    return modifiePar;
  }

  /**
   * 
   * @param dateModification
   */
  public void setDateModification(java.util.Date dateModification) {
    this.dateModification = dateModification;
  }

  /**
   * 
   * @return
   */
  public java.util.Date getDateModification() {
    return dateModification;
  }

  /**
   * 
   * @param version
   */
  public void setVersion(Integer version) {
    this.version = version;
  }

  /**
   * 
   * @return
   */
  public Integer getVersion() {
    return version;
  }

  /**
   * @param dateCreationDebut the dateCreationDebut to set
   */
  public void setDateCreationDebut(Date dateCreationDebut) {
    this.dateCreationDebut = dateCreationDebut;
  }

  /**
   * @return the dateCreationDebut
   */
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }

  /**
   * @param dateCreationFin the dateCreationFin to set
   */
  public void setDateCreationFin(Date dateCreationFin) {
    this.dateCreationFin = dateCreationFin;
  }

  /**
   * @return the dateCreationFin
   */
  public Date getDateCreationFin() {
    return dateCreationFin;
  }

  /**
   * @param dateModificationDebut the dateModificationDebut to set
   */
  public void setDateModificationDebut(Date dateModificationDebut) {
    this.dateModificationDebut = dateModificationDebut;
  }

  /**
   * @return the dateModificationDebut
   */
  public Date getDateModificationDebut() {
    return dateModificationDebut;
  }

  /**
   * @param dateModificationFin the dateModificationFin to set
   */
  public void setDateModificationFin(Date dateModificationFin) {
    this.dateModificationFin = dateModificationFin;
  }

  /**
   * @return the dateModificationFin
   */
  public Date getDateModificationFin() {
    return dateModificationFin;
  }

}
