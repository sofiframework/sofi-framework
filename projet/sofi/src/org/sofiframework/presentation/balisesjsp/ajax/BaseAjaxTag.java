/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.ajax;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Balise Jsp de base pour les traitements Ajax.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.8
 */
public class BaseAjaxTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -5226728023019536885L;
  private String href = null;

  /**
   * Constructeur par défault.
   */
  public BaseAjaxTag() {
  }

  /**
   * Exécution de l'ouverture de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Exécution de la fin de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      String valeurELHref = EvaluateurExpression.evaluerString("href",
          getHref(), this, pageContext);
      setHref(valeurELHref);
    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    this.href = null;
    super.release();
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getHref() {
    return href;
  }
}
