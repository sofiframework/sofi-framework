/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.ajax;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;


/**
 * Balise Jsp qui permet de créer une fonction javascript
 * qui appelle un traitement Ajax et qui modifie l'affichage d'un DIV ou d'un element
 * de la page.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.5
 */
public class FonctionRetourValeurTag extends BaseAjaxTag {
  /**
   * 
   */
  private static final long serialVersionUID = -7227075459467300700L;
  private String nomFonction = null;
  private String nomDiv = null;
  private String nomElement = null;
  private String listeParametreHref = null;
  private String listeNomAttributHref = null;

  /**
   * Constructeur par défault.
   */
  public FonctionRetourValeurTag() {
  }

  /**
   * Exécution de l'ouverture de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    super.evaluerEL();
    evaluerEL();

    StringBuffer resultat = new StringBuffer();
    resultat.append("<script>");
    resultat.append(getNomFonction());
    resultat.append("() {");
    resultat.append(UtilitaireAjax.getLienAjax(getHref(), null, null));

    // Ecrire la fonction javascript dans la sortie en écriture
    TagUtils.getInstance().write(pageContext, resultat.toString());

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Exécution de la fin de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer();
    resultat.append(getParametreHref());

    resultat.append(UtilitaireAjax.traiterRequeteHttpGETAvecReponse(null,
        getNomDiv(), getNomElement()));

    resultat.append("</script>");

    // Ecrire la fonction javascript dans la sortie en écriture
    TagUtils.getInstance().write(pageContext, resultat.toString());

    return (EVAL_PAGE);
  }

  protected String getParametreHref() {
    StringBuffer resultat = new StringBuffer();

    StringTokenizer iterateurListeParam = new StringTokenizer(getListeParametreHref(),
        ",");
    int nbParametre = iterateurListeParam.countTokens();
    StringTokenizer iterateurListeNomAttribut = new StringTokenizer(getListeNomAttributHref(),
        ",");
    int nbNomAttribut = iterateurListeNomAttribut.countTokens();

    ArrayList listeParametre = new ArrayList();
    ArrayList listeNomAttribut = new ArrayList();

    while (iterateurListeParam.hasMoreTokens()) {
      String parametre = iterateurListeParam.nextToken();
      listeParametre.add(parametre);
    }

    while (iterateurListeNomAttribut.hasMoreTokens()) {
      String parametre = iterateurListeNomAttribut.nextToken();
      listeNomAttribut.add(parametre);
    }

    if (nbParametre != nbNomAttribut) {
      throw new SOFIException(
          "Le nombre de parametre doit etre égal au nombre d'attribut spécifié");
    }

    for (int i = 0; i < listeParametre.size(); i++) {
      resultat.append("lienAjax = lienAjax + '&");
      resultat.append(listeParametre.get(i));
      resultat.append("= '");
      resultat.append("+ document.getElementById('");
      resultat.append(listeNomAttribut.get(i));
      resultat.append("').value;\n");
    }

    return resultat.toString();
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  protected void evaluerEL() throws JspException {
    try {
      String valeurELHref = EvaluateurExpression.evaluerString("href",
          getHref(), this, pageContext);
      setHref(valeurELHref);
    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  public void setNomFonction(String nomFonction) {
    this.nomFonction = nomFonction;
  }

  public String getNomFonction() {
    return nomFonction;
  }

  public void setNomDiv(String nomDiv) {
    this.nomDiv = nomDiv;
  }

  public String getNomDiv() {
    return nomDiv;
  }

  public void setNomElement(String nomElement) {
    this.nomElement = nomElement;
  }

  public String getNomElement() {
    return nomElement;
  }

  public void setListeParametreHref(String listeParametreHref) {
    this.listeParametreHref = listeParametreHref;
  }

  public String getListeParametreHref() {
    return listeParametreHref;
  }

  public void setListeNomAttributHref(String listeNomAttributHref) {
    this.listeNomAttributHref = listeNomAttributHref;
  }

  public String getListeNomAttributHref() {
    return listeNomAttributHref;
  }
}
