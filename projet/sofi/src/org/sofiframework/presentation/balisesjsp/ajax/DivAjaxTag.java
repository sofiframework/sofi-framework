/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.ajax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.struts.controleur.RequestProcessorHelper;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise Jsp qui permet de traiter un div en mode Ajax.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.8
 */
public class DivAjaxTag extends BaseAjaxTag {
  /**
   * 
   */
  private static final long serialVersionUID = -3182380022988382692L;

  /** Le nombre de secondes nécessaire avant le rafraichissement du bloc */
  private String tempsRafraichissement = "";

  /**
   * Permet de signifier au div qu'il peut s'ouvrir et fermer à l'aide du href.
   * Vous devez par contre ajouter le code nécessaire dans votre action pour
   * implémenter qu'est-ce qui survient lorsque le div est ouvert ou fermé
   */
  private boolean ouvrirFermerDiv = false;

  /**
   * Permet de spécifier que le div doit être fermer par défaut.
   *
   * Valeur par défaut : false.
   */
  private String fermerParDefaut;

  /**
   * Permet de spécifier à la balise de toujours traiter le corps du DIV peut
   * importe si le DIV est ouvert ou fermé.
   */
  private boolean traiterCorps = false;

  /**
   * Traitement JavaScript a executé pendant le chargement ajax
   */
  private String ajaxJSPendantChargement;

  /**
   * Traitement JavaScript a executé après le chargement.
   */
  private String ajaxJSApresChargement;

  /**
   * L'identifiant du lien qui ouvre ou ferme une section.
   */
  private String styleIdLienSection;

  /**
   * Le style CSS du lien (spécifie par le paramètre 'idStyleClassSection'
   * lorsque la section est ouverte
   */
  private String styleClassIdLienSectionOuverte;

  /**
   * Le style CSS du lien (spécifie par le paramètre 'idStyleClassSection'
   * lorsque la section est fermée
   */
  private String styleClassIdLienSectionFermee;

  /**
   * Le nom de variable dont on désire connaitre l'état d'ouverture ou de
   * fermeture du div en cours.
   *
   * Valeur : true/false;
   */
  private String varOuverture = null;

  /**
   * Constructeur par défault.
   */
  public DivAjaxTag() {
  }

  /**
   * Exécution de l'ouverture de la balise.
   * 
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    initialiserDivPersistant();

    // HashMap ensembleDivOuvert = (HashMap)
    // pageContext.getSession().getAttribute("tableau_div_persistant");
    StringBuffer resultat = new StringBuffer();

    StringBuffer resultatScriptRafraichissement = new StringBuffer();

    boolean rafraichissement = (tempsRafraichissement != null) && (getHref() != null);
    boolean traiterCorps = (UtilitaireControleur.isDivOuvert(getId(), request).booleanValue()
        || isEffectuerTraiterCorps());

    if ((getHref() != null) && !getHref().equals("null")) {
      if ((rafraichissement && !isOuvrirFermerDiv()) || (isOuvrirFermerDiv() && traiterCorps)) {
        Boolean divOuvert = UtilitaireControleur.isDivOuvert(getId(), request);
        if ((isOuvrirFermerDiv() || !isDivFermerParDefaut()) && traiterCorps && divOuvert.booleanValue()) {
          Href href = new Href(getHref());
          href.ajouterParametre("divOuvert", "true");
          setHref(href.getUrlPourRedirection());
        }
        resultatScriptRafraichissement.append(UtilitaireAjax.getLienAjax(getHref(), null, null) + "\n");
        resultatScriptRafraichissement.append("traiterAffichageDivAjaxAsynchrone(lienAjax,'");
        resultatScriptRafraichissement.append(getId());
        resultatScriptRafraichissement.append("', null, null, ");

        String temps = getTempsRafraichissement();

        if ((temps == null) || temps.equals("")) {
          temps = "null";
        }

        resultatScriptRafraichissement.append(temps);

        resultatScriptRafraichissement.append(", false");

        if (isOuvrirFermerDiv()) {
          resultatScriptRafraichissement.append(", true");
        }

        resultatScriptRafraichissement.append(");");
        resultatScriptRafraichissement.append("\n");
      }
    }

    if (isOuvrirFermerDiv()) {
      if (UtilitaireControleur.isDivOuvert(getId(), request).booleanValue()) {
        if ((getStyleIdLienSection() != null) && (getStyleClassIdLienSectionOuverte() != null)) {
          resultatScriptRafraichissement.append("setStyleClass('");
          resultatScriptRafraichissement.append(getStyleIdLienSection());
          resultatScriptRafraichissement.append("','");
          resultatScriptRafraichissement.append(getStyleClassIdLienSectionOuverte());
          resultatScriptRafraichissement.append("');");
        }
      } else {
        if ((getStyleIdLienSection() != null) && (getStyleClassIdLienSectionFermee() != null)) {
          resultatScriptRafraichissement.append("setStyleClass('");
          resultatScriptRafraichissement.append(getStyleIdLienSection());
          resultatScriptRafraichissement.append("','");
          resultatScriptRafraichissement.append(getStyleClassIdLienSectionFermee());
          resultatScriptRafraichissement.append("');");
        }
      }
    }

    else {
      // Si script vide alors ne pas le traiter.
      resultat = new StringBuffer();
    }

    resultat.append("<div id=\"" + this.getId() + "\">\n");

    if (getVarOuverture() != null) {
      pageContext.getRequest().setAttribute(getVarOuverture(), UtilitaireControleur.isDivOuvert(getId(), request));
    } else {

      StringBuffer scriptsDiv = (StringBuffer) pageContext.getRequest()
          .getAttribute(ConstantesBaliseJSP.FONCTION_JS_ACCUMULE_BAS_PAGE);
      if (scriptsDiv == null) {
        scriptsDiv = new StringBuffer();

      }

      scriptsDiv.append(resultatScriptRafraichissement);

      // On accumule les scripts ajax de DIV pour mettre avant la fermeture du
      // body
      // dans la balise <sofi:javascript/>.
      pageContext.getRequest().setAttribute(ConstantesBaliseJSP.FONCTION_JS_ACCUMULE_BAS_PAGE, scriptsDiv);
      // Ecrire la fonction javascript dans la sortie en écriture
      TagUtils.getInstance().write(pageContext, resultat.toString());
    }

    if (traiterCorps || (!isOuvrirFermerDiv()
        && !((UtilitaireControleur.isDivOuvert(getId(), request) == null) && isDivFermerParDefaut()))) {
      return (EVAL_BODY_INCLUDE);
    } else {
      return (SKIP_BODY);
    }
  }

  /**
   * Cette méthode permet de générer le code HTML qui se placera dans le DIV
   * courant si le div est eclatable et persistant.
   */
  private void initialiserDivPersistant() {
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    if (UtilitaireControleur.isDivOuvert(getId(), request) == null
        || !UtilitaireControleur.isDivInitialise(getId(), request)) {
      if (isDivFermerParDefaut()) {
        RequestProcessorHelper.setEtatPersistanceDiv(getId(), Boolean.FALSE, request);
      } else {
        RequestProcessorHelper.setEtatPersistanceDiv(getId(), Boolean.TRUE, request);
      }
    }

  }

  /**
   * Exécution de la fin de la balise.
   * 
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer();

    if (getVarOuverture() == null) {
      resultat.append("</div>\n");
    }

    // Ecrire la fonction javascript dans la sortie en écriture
    TagUtils.getInstance().write(pageContext, resultat.toString());

    release();

    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * 
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  protected void evaluerEL() throws JspException {
    try {
      String id = EvaluateurExpression.evaluerString("id", getId(), this, pageContext);
      setId(id);
    } catch (JspException e) {
      throw e;
    }

    try {
      String styleIdLienSection = EvaluateurExpression.evaluerString("styleIdLienSection", getStyleIdLienSection(),
          this, pageContext);
      setStyleIdLienSection(styleIdLienSection);
    } catch (JspException e) {
      throw e;
    }

    try {
      String fermerParDefaut = EvaluateurExpression.evaluerString("fermerParDefaut", getFermerParDefaut(), this,
          pageContext);
      setFermerParDefaut(fermerParDefaut);
    } catch (JspException e) {
      throw e;
    }

    if ((getHref() != null) && (getTempsRafraichissement() != null)) {
      setHref(getHref() + "&ajax_rafraichir=true");
    }

    super.evaluerEL();
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    this.tempsRafraichissement = null;
    this.ajaxJSApresChargement = null;
    this.ajaxJSPendantChargement = null;
    super.release();
  }

  public void setTempsRafraichissement(String tempsRafraichissement) {
    this.tempsRafraichissement = tempsRafraichissement;
  }

  public String getTempsRafraichissement() {
    return tempsRafraichissement;
  }

  public void setOuvrirFermerDiv(boolean ouvrirFermerDiv) {
    this.ouvrirFermerDiv = ouvrirFermerDiv;
  }

  public boolean isOuvrirFermerDiv() {
    return ouvrirFermerDiv;
  }

  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  public void setStyleIdLienSection(String styleIdLienSection) {
    this.styleIdLienSection = styleIdLienSection;
  }

  public String getStyleIdLienSection() {
    return styleIdLienSection;
  }

  public void setStyleClassIdLienSectionOuverte(String styleClassIdLienSectionOuverte) {
    this.styleClassIdLienSectionOuverte = styleClassIdLienSectionOuverte;
  }

  public String getStyleClassIdLienSectionOuverte() {
    return styleClassIdLienSectionOuverte;
  }

  public void setStyleClassIdLienSectionFermee(String styleClassIdLienSectionFermee) {
    this.styleClassIdLienSectionFermee = styleClassIdLienSectionFermee;
  }

  public String getStyleClassIdLienSectionFermee() {
    return styleClassIdLienSectionFermee;
  }

  public boolean isDivFermerParDefaut() {
    if ((getFermerParDefaut() != null) && getFermerParDefaut().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setFermerParDefaut(String fermerParDefaut) {
    this.fermerParDefaut = fermerParDefaut;
  }

  public String getFermerParDefaut() {
    return fermerParDefaut;
  }

  public void setTraiterCorps(boolean traiterCorps) {
    this.traiterCorps = traiterCorps;
  }

  public boolean isEffectuerTraiterCorps() {
    return traiterCorps;
  }

  public void setVarOuverture(String varOuverture) {
    this.varOuverture = varOuverture;
  }

  public String getVarOuverture() {
    return varOuverture;
  }
}
