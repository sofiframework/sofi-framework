/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Afficher une date selon un format spécifique.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class DateELTag extends AffichageELTag {
  /**
   * 
   */
  private static final long serialVersionUID = -1640247802118209473L;
  /** Format a appliquer à la date */
  protected String format = GestionParametreSysteme.getInstance().getString("formatDate");

  /** Constructeur par défaut */
  public DateELTag() {
  }

  /**
   * Permet d'obtenir la valeur EL d'un parameter du tag.
   * Cette méthode est surchargée du tag BaseEL afin d'encapsuler le formatage de l'objet date.
   * @param nomParametre nom de la propriété de la classe qui recoit la faleur JSTL.
   * @param valeurParametre l'expression JSTL qui doit être interprétée.
   * @param classeParametre la classe de l'élément qui correspond à l'expression JSTL.
   * @return la date formatée
   */
  @Override
  public Object getValeurEL(String nomParametre, Object valeurParametre,
      Class classeParametre) {
    Date date = null;

    if (this.valeur != null) {
      if (EvaluateurExpression.isEvaluerEL(pageContext)) {
        date = (Date) super.getValeurEL("value", this.valeur, Date.class);
      } else {
        date = (Date) valeurParametre;
      }
    } else {
      date = new Date();
    }

    String dateFormatee = null;

    if (date != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(this.format);
      dateFormatee = sdf.format(date);
    }

    return dateFormatee;
  }

  /**
   * Fixer le format à appliquer à la date.
   * <p>
   * @param format le format à appliquer à la date
   * @see java.text.SimpleDateFormat
   */
  public void setFormat(String format) {
    this.format = format;
  }

  /**
   * Obtenir le format qui est utilisé pour formater la date.
   * <p>
   * @return le format utilisé
   */
  public String getFormat() {
    return format;
  }
}
