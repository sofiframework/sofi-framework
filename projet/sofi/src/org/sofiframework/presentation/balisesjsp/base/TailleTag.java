/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;

public class TailleTag extends TagSupport {

  /**
   * 
   */
  private static final long serialVersionUID = -4444865939748291016L;
  private static final String REQUEST = "request";
  private static final String SESSION = "session";
  private static final String APPLICATION = "application";

  private String var;
  private int scope;
  private Object valeur;


  @Override
  public int doStartTag() throws JspException {
    Object valeur = this.valeur;
    if (valeur instanceof String) {
      valeur = EvaluateurExpression.evaluer(
          "valeur", (String) valeur,
          Object.class, this, pageContext);
    }

    Integer taille = new Integer(getTaille(valeur));
    if (var != null) {
      if (scope == 0) {
        pageContext.setAttribute(var, taille);
      } else {
        pageContext.setAttribute(var, taille, scope);
      }
    } else {
      try {
        pageContext.getOut().print(taille);
      } catch (IOException e) {
        throw new JspException(e);
      }
    }
    return EVAL_PAGE;
  }

  private int getTaille(Object valeur) throws JspException {
    if (valeur == null) {
      return 0;
    } else if (valeur.getClass().isArray()) {
      return Array.getLength(valeur);
    } else if (valeur instanceof String) {
      return ((String) valeur).length();
    } else if (valeur instanceof Collection) {
      return ((Collection) valeur).size();
    } else if (valeur instanceof Map) {
      return ((Map) valeur).size();
    } else {
      throw new JspException(
          "Le type '" + valeur.getClass().getName()
          + "' n'est pas supporté.");
    }
  }

  @Override
  public void release() {
    var = null;
    scope = 0;
    valeur = null;
  }

  private static int getScope(String scope) {
    if (REQUEST.equalsIgnoreCase(scope)) {
      return PageContext.REQUEST_SCOPE;
    } else if (SESSION.equalsIgnoreCase(scope)) {
      return PageContext.SESSION_SCOPE;
    } else if (APPLICATION.equalsIgnoreCase(scope)) {
      return PageContext.APPLICATION_SCOPE;
    } else {
      return PageContext.PAGE_SCOPE;
    }
  }

  public void setValeur(Object valeur) {
    this.valeur = valeur;
  }


  public Object getValeur() {
    return valeur;
  }


  public void setScope(String scope) {
    this.scope = getScope(scope);
  }


  public void setVar(String var) {
    this.var = var;
  }


  public String getVar() {
    return var;
  }
}