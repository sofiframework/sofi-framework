/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise servant à délimiter une section de page sécurisée.
 * <p>
 * Cette balise ne possède qu'un identifiant qui sert  si l'utilisateur à droit à cette
 * section bien précise. Dans le cas où l'utilisateur n'a pas droit à cette section,
 * cette dernière n'est même pas traitée. Dans le cas où l'utilisateur à droit
 * à la section, mais seulement de manière "lecture seulement" alors la balise
 * place dans le contexte de page une variable indiquant que tout ce qui est
 * inclus à l'intérieur doit être en lecture seulement. La balise de fermeture
 * se charge d'enlever du contexte de la page la variable indiquant la lecture
 * seulement.
 * <p>
 * Voici un exemple rapide d'utilisation :<br>
 * Disons que la section de page JSP ressemble à ceci<br>
 * <code>
 * &nbsp;&nbsp;&lt;blocSecurite identifiant="exemple"&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="text" ... /&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;select ... /&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;button ... /&gt;<br>
 * &nbsp;&nbsp;&lt;/blocSecurite&gt;<br>
 * </code>
 * Tout d'abord, le traitement du début de la balise JSP ira vérifier si l'utilisateur
 * à droit ou pas au bloc de sécurité identifié par <code>exemple</code>. Dans le
 * cas ou l'utilisateur n'y a pas droit, alors le traitement de la balise fera un
 * SKIP_BODY ce qui arrêtera immédiatement le traitement de l'intérieur de celle-ci.
 * <p>
 * Dans le cas où l'utilisateur à droit au bloc, la balise vérifie si l'utilisateur à
 * droit au contenu de la balise en mode "lecture seulement". Dans le cas où on doit
 * traiter le mode "lecture seulement" la balise ira placer dans le contexte de page
 * la variable <code>modeLectureSeulement = new Boolean(true);</code>. Par la suite
 * il est donc du devoir de chacune des balises de gérer le fait que le contexte de
 * page possède cette variable. Lorsque la balise de fermeture de bloc de sécurité
 * est rencontrée, on enleve la variable <code>modeLectureSeulement</code> afin de
 * traiter le reste de la page HTML comme il se doit.
 * <p>
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.1
 */
public class BlocSecuriteTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -361464451947371406L;

  /**
   * Nom de la variable placée dans le PageContext qui détermine si les composant
   * doivent être en mode lecture seulement
   */
  public static final String MODE_LECTURE_SEULEMENT = "modeLectureSeulement";

  /** l'identifiant du bloc de sécurité */
  private String identifiant;

  /**
   * Le nom de variable temporaire qui loge si l'utilisateur a accès ou non au bloc de sécurité.
   * <p>
   * La valeur est un Boolean.
   */
  private String var;

  /**
   * Variable qui loge si l'utilisateur a accès au bloc en lecture seulement ou pas.
   * @since SOFI 2.0.1
   **/
  private String varLectureSeulement;

  /**
   * Permet de rendre la sécurité active ou pas
   * pour le bloc en question en fonction d'une condition
   * 
   * Exemple : securiteActive="${monForm.modeEdition}".
   */
  private String securiteActive;

  /** Constructeur par défaut */
  public BlocSecuriteTag() {
  }

  /**
   * Obtenir l'identifiant du bloc de sécurité.
   * <p>
   * @return l'identifiant du bloc de sécurité
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant du bloc de sécurité.
   * <p>
   * @param identifiant l'identifiant du bloc de sécurité
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * <p>
   * Vérifie si l'utilisateur à droit au bloc de sécurité et agit en conséquence.
   * <p>
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException Erreur lors du traitement de la balise JSP
   */
  @Override
  public int doStartTag() throws JspException {
    int directiveJsp = SKIP_BODY;

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    if (this.isValiderSecuriteUtilisateur()) {
      // Récupération de l'utilisateur présent dans la session
      Utilisateur utilisateur = this.getUtilisateur();

      if (utilisateur != null) {
        if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
          // Vérification si l'utilisateur à droit au composant
          boolean possedeDroitAcces = this.traiterPossedeDroitAcces(utilisateur);

          // L'utilisateur possède les droit d'accès
          if (possedeDroitAcces) {
            this.traiterLectureSeulement(utilisateur);
            directiveJsp = EVAL_BODY_INCLUDE;
          }
        } else {
          throw new JspException("Le composant de sécurité n'est pas configuré, donc impossible de traiter la balise BlocSecurite");
        }
      } else {
        throw new JspException("Erreur : Aucun utilisateur de configuré dans la session");
      }
    } else {
      directiveJsp = EVAL_BODY_INCLUDE;
    }

    return directiveJsp;
  }

  /**
   * Traite les droit d'accès à ce bloc de la page. Si un var est configuré,
   * placer le résultat dans le contexte de page.
   * @param utilisateur Utilisateur en cours
   * @return Si l'utilisateur à accès au bloc
   */
  private boolean traiterPossedeDroitAcces(Utilisateur utilisateur) {
    boolean possedeDroitAcces = GestionSecurite.getInstance()
        .isUtilisateurAccesObjetSecurisable(utilisateur, identifiant);

    if (getVar() != null) {
      this.pageContext.getRequest().setAttribute(
          getVar(), new Boolean(possedeDroitAcces));
    }

    return possedeDroitAcces;
  }

  /**
   * Traite si l'utilisateur à accès à au bloc seulement en lecture
   * ou bien aussi en modification.
   * @param utilisateur Utilisateur en cours
   */
  private void traiterLectureSeulement(Utilisateur utilisateur) {
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();

    boolean lectureSeulement = gestionSecurite
        .isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur, identifiant);

    // L'utilisateur peut le consulter seulement en lecture seule
    if (lectureSeulement) {
      this.pageContext.getRequest().setAttribute(
          MODE_LECTURE_SEULEMENT, Boolean.TRUE);
    }

    if (getVarLectureSeulement() != null) {
      this.pageContext.getRequest().setAttribute(
          getVarLectureSeulement(), new Boolean(lectureSeulement));
    }
  }

  /**
   * Obtenir l'utilisateur en cours
   * @return Utilisateur
   */
  private Utilisateur getUtilisateur() {
    return UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
  }

  /**
   * Complété la fin de la balise.
   * <p>
   * Enleve la variable <code>modeLectureSeulement</code> du contexte de page
   * afin de terminer le traitement comme il faut.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException Exception générique
   */
  @Override
  public int doEndTag() throws JspException {
    this.pageContext.getRequest().removeAttribute(MODE_LECTURE_SEULEMENT);
    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    this.setIdentifiant(
        EvaluateurExpression.evaluerString(
            "identifiant", this.identifiant, this, this.pageContext));

    this.setSecuriteActive(
        EvaluateurExpression.evaluerString(
            "securiteActive", this.securiteActive, this, this.pageContext));
  }

  /**
   * Fixer la variable qui loge  (boolean) si l'utilisateur a accès au bloc ou non.
   * @param varSecurise la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public String getVar() {
    return var;
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc ou non.
   * @return la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Fixer la variable qui loge (boolean) si l'utilisateur a accès au bloc en lecture seulement ou pas
   * @param varLectureSeulement
   */
  public void setVarLectureSeulement(String varLectureSeulement) {
    this.varLectureSeulement = varLectureSeulement;
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc en lecture seulement ou pas.
   * @return la variable qui loge si l'utilisateur a accès au bloc en lecture seulement ou pas.
   */
  public String getVarLectureSeulement() {
    return varLectureSeulement;
  }

  public String getSecuriteActive() {
    return securiteActive;
  }

  public void setSecuriteActive(String securiteActive) {
    this.securiteActive = securiteActive;
  }

  private boolean isValiderSecuriteUtilisateur() {
    return this.getSecuriteActive() == null || securiteActive.equals("true");
  }
}
