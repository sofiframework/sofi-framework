/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.base.afficheur.AfficheurPopup;
import org.sofiframework.presentation.balisesjsp.html.BaseBoutonTag;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireTypeMime;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;
import org.sofiframework.utilitaire.documentelectronique.UtilitaireDocumentElectronique;

/**
 *
 * AttacheTag permet d'afficher liste de fichier attache pour un domaine
 * de fichier précis et aussi d'appeler une fenêtre qui permet de traitement
 * d'ajouter des fichiers attachés.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.3
 */
public class FichierAttacheTag extends BaseBoutonTag {

  private static final long serialVersionUID = -6186324543192968019L;

  /**
   * Spécifie si la barre déroulante est affiche ou pas.
   */
  private boolean scrollbars = false;

  /**
   * Spécifie la largeur de la fenêtre flottante.
   */
  private Integer largeurFenetre = null;

  /**
   * Spécifie la hauteur de la fenêtre flottante.
   */
  private Integer hauteurFenetre = null;

  /**
   * Spécifie la hauteur de la fenêtre flottante.
   */
  private String nomFenetre = null;

  /**
   * Spécifie le type de domaine de fichier a traiter.
   */
  private String domaineFichier = null;

  /**
   * Le libelle qui doit être dans la fenêtre flottante de fichiers attachés.
   */
  private String libelleDomaineFichier = null;

  /**
   * L'adresse URL qui permet de faire la sélection du fichier.
   */
  private String hrefSelectionFichier = null;

  /**
   * URL qui permet de supprimer un fichier.
   */
  private String hrefSupprimerFichier = null;

  /**
   * Icone à afficher pour supprimer un fichier.
   */
  private String hrefIconeSupprimerFichier = null;

  /**
   * Message de confirmation a appliquer si on désire supprimer un fichier.
   */
  private String hrefSupprimerFichierMessageConfirmation = null;

  /**
   * Traiter le href pour Supprimer un fichier en mode ajax.
   */
  private boolean hrefSupprimerFichierAjax = false;

  /**
   * Spécifie si la barre déroulante est affiche ou pas.
   */
  private boolean scrollbarsSelectionFichier = false;

  /**
   * Spécifie la largeur de la fenêtre flottante.
   */
  private Integer largeurFenetreSelectionFichier = null;

  /**
   * Spécifie la hauteur de la fenêtre flottante.
   */
  private Integer hauteurFenetreSelectionFichier = null;

  /**
   * Le style de la la selection de fichier (de la liste).
   */
  private String styleClassSelectionFichier = null;

  /**
   * Indique si vous ne voulez qu'afficher la liste de fichier
   * en lecture seulement
   */
  private boolean readonly = false;

  /**
   * Indique si vous ne voulez qu'afficher la liste de fichier
   * en lecture seulement en appliquant les expressions EL.
   */
  private String readonlyEL = null;

  /**
   * Permet de spécifier le code HTML avec du JavaScript qui va permettre d'afficher d'afficher des boutons d'action tel
   * que la suppression ou le téléchargement après le nom du fichier.
   * L'url de l'action doit être spécifié dans ce décorateur.
   * La valeur de l'identifiant doit être spécifié par [ID] afin que la balise remplace par l'identifiant du document automatiquement.
   * @since SOFI 2.0.4
   */
  private String decorateurAction = null;

  /**
   * Permet de spécifier un message lorsque la liste de fichier attaché est vide.
   * @since SOFI 2.0.4
   */
  private String messageListeVide = null;

  /**
   * Le gabarit du résultat de la liste des fichiers attachés.
   * @since SOFI 2.0.4
   */
  private String gabaritListe = null;

  /**
   * Constructeur par défault.
   */
  public FichierAttacheTag() {
  }

  /**
   * Execute le début de la balise. Appel son parent.
   * @exception JspException si un exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {

    if (UtilitaireBaliseJSP.isValeurEL(getHrefSelectionFichier())) {
      setHrefSelectionFichier(EvaluateurExpression.evaluerString("hrefSelectionFichier",
          getHrefSelectionFichier(), this, pageContext));
    } else {
      setHrefSelectionFichier(UtilitaireBaliseJSP.genererUrl(pageContext,
          getHrefSelectionFichier()));
    }

    int retour = super.doStartTag();

    if (!UtilitaireString.isVide(readonlyEL) && "true".equals(readonlyEL.toLowerCase())) {
      this.readonly = true;
    }

    return retour;
  }

  /**
   * Process the end of this tag.  The default implementation does nothing.
   * @exception JspException if a JSP exception has occurred
   */
  @Override
  @SuppressWarnings("rawtypes")
  public int doEndTag() throws JspException {

    StringBuffer resultat = new StringBuffer();

    List liste = null;

    Object listeTmp =
        pageContext.getSession().getAttribute(getDomaineFichier());

    if (listeTmp instanceof ListeNavigation) {
      liste = ((ListeNavigation) listeTmp).getListe();
    } else {
      liste = (List) listeTmp;
    }

    if (!isReadonly()) {
      resultat.append("<div id=\"");
      resultat.append(getDomaineFichier());
      resultat.append("\">");
    }

    if (liste != null && liste.size() > 0) {
      String resultatListeFichierAttache =
          genererListeFichierAttache(pageContext.getSession(), liste);
      if (getGabaritListe() != null &&
          getGabaritListe().indexOf("[LISTE_FICHIER_ATTACHE]") != -1) {

        resultat.append(UtilitaireString.remplacerTous(getGabaritListe(),
            "[LISTE_FICHIER_ATTACHE]", resultatListeFichierAttache));
      } else {
        resultat.append(resultatListeFichierAttache);
      }
    } else {
      String decorateurEntreFichier =
          GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FICHIER_ATTACHE_DECORATEUR_ENTRE_FICHIER);
      Message message =
          UtilitaireMessage.get(getMessageListeVide(), null, pageContext);

      if (message != null && message.getMessage() != null) {
        if (!UtilitaireString.isVide(decorateurEntreFichier)) {
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[NOM_FICHIER]", message.getMessage());
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[VOLUME_FICHIER]", "");
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[ACTION]", "");
          resultat.append(decorateurEntreFichier);
        } else {
          resultat.append(message.getMessage());
        }
      }

    }

    if (!isReadonly()) {
      resultat.append("</div>");
    }

    TagUtils.getInstance().write(this.pageContext, resultat.toString());

    this.release();

    return (EVAL_PAGE);
  }

  /**
   * Générer une liste de fichier attaché avec son volume.
   * @return un résultat HTML qui affiche une liste de fichier attaché avec son volume.
   * @param liste la liste
   * @param session la session de l'utilisateur.
   */
  @SuppressWarnings("rawtypes")
  public String genererListeFichierAttache(HttpSession session, List liste) {

    String decorateurEntreFichierOriginal =
        GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FICHIER_ATTACHE_DECORATEUR_ENTRE_FICHIER);

    StringBuffer resultat = new StringBuffer();

    Iterator iterateur = liste.iterator();

    while (iterateur.hasNext()) {
      String decorateurEntreFichier = decorateurEntreFichierOriginal;

      DocumentElectronique document = (DocumentElectronique)iterateur.next();
      Href lienComplet = new Href(hrefSelectionFichier);

      lienComplet.ajouterParametre("identifiant", document.getIdentifiant());

      if ((hrefSelectionFichier != null) &&
          !"null".equals(hrefSelectionFichier)) {
        StringBuffer resultatLienFichier = new StringBuffer();
        resultatLienFichier.append("<a class=\"fichierAttacheTag\" href=\"");
        resultatLienFichier.append(lienComplet);
        resultatLienFichier.append("\"");

        if (document.isImage()) {
          resultatLienFichier.append(" target=\"new\"");
        }

        resultatLienFichier.append(">");

        String referenceImage = genererImage(document, this.isXhtml());
        resultatLienFichier.append(referenceImage);

        Libelle libelle =
            UtilitaireLibelle.getLibelle("sofi.libelle.commun.abreviation_kilo_octet",
                pageContext);

        if (((!UtilitaireString.isVide(decorateurEntreFichier) &&
            decorateurEntreFichier.indexOf("[VOLUME_FICHIER]") == -1)) ||
            UtilitaireString.isVide(decorateurEntreFichier)) {
          resultatLienFichier.append("&nbsp;");
          resultatLienFichier.append(document.getNomFichier());
          resultatLienFichier.append(" (");
          resultatLienFichier.append(document.getVolume());
          resultatLienFichier.append(libelle.getMessage());
          resultatLienFichier.append(")");
        } else {
          resultatLienFichier.append("&nbsp;");
          resultatLienFichier.append(document.getNomFichier());
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[VOLUME_FICHIER]",
                  document.getVolume() +
                  libelle.getMessage());
        }
        resultatLienFichier.append("</a>");

        if (!UtilitaireString.isVide(decorateurEntreFichier)) {
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[NOM_FICHIER]", resultatLienFichier.toString());
        } else {
          resultat.append(resultatLienFichier.toString());
        }
      } else {
        if (UtilitaireString.isVide(decorateurEntreFichier)) {
          resultat.append(document.getNomFichier());
        }
      }

      if (!StringUtils.isEmpty(hrefSupprimerFichier) &&
          getDecorateurAction() == null) {
        Message messageConfirmation = null;

        if (getHrefSupprimerFichierMessageConfirmation() != null) {
          messageConfirmation =
              UtilitaireMessage.get(this.getHrefSupprimerFichierMessageConfirmation(),
                  null, pageContext);
        }

        Href lienSupprimer = new Href(hrefSupprimerFichier);

        lienSupprimer.ajouterParametre("identifiant",
            document.getIdentifiant());
        lienSupprimer.ajouterParametre("domaineFichier", getDomaineFichier());
        resultat.append("&nbsp;<a href=\"");
        resultat.append("javascript:void(0);");

        if (messageConfirmation != null) {
          resultat.append("if (confirm('");
          resultat.append(UtilitaireString.convertirEnJavaScript(messageConfirmation.getMessage()));
          resultat.append("')) {");
        }

        if (isHrefSupprimerFichierAjax()) {
          resultat.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(lienSupprimer.toString(),
              getDomaineFichier(), null, null, null, null, pageContext));
        } else {
          resultat.append("window.location='");
          resultat.append(lienSupprimer);
          resultat.append("';");
        }

        if (messageConfirmation != null) {
          resultat.append("}");
        }

        resultat.append("\">");

        // L'attribut BORDER de la balise IMG est DEPRECATED selon le W3C.
        // La gestion du border s'effectue par CSS. Le plus simple étant de mettre
        // img{border:none;} dans sofi.css
        // REF: http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.7.3

        if (hrefIconeSupprimerFichier != null) {
          resultat.append("<img src=\"" + hrefIconeSupprimerFichier + "\"");
          resultat.append(this.getElementClose());
        } else {
          resultat.append("x");
        }

        resultat.append("</a>");
      }

      if (getDecorateurAction() != null) {
        String decorateur =
            UtilitaireString.remplacerTous(getDecorateurAction(), "[ID]",
                document.getIdentifiant().toString());

        if (!UtilitaireString.isVide(decorateurEntreFichier) &&
            decorateurEntreFichier.indexOf("[ACTION]") != -1) {
          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[ACTION]", decorateur);

          decorateurEntreFichier =
              UtilitaireString.remplacerTous(decorateurEntreFichier,
                  "[NOM_FICHIER]", document.getNomFichier());
        } else {
          resultat.append("&nbsp;");
          resultat.append(decorateur);
        }
      }

      if (UtilitaireString.isVide(decorateurEntreFichier)) {
        if (iterateur.hasNext()) {
          String separateur =
              GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FICHIER_ATTACHE_SEPARATEUR_ENTRE_FICHIER);

          if (StringUtils.isEmpty(separateur)) {
            separateur = " | ";
          }

          resultat.append(separateur);
        }
      } else {
        resultat.append(decorateurEntreFichier);
      }
    }

    return resultat.toString();
  }

  /**
   * Même traitement que genererImage(DocumentElectronique document) initial à
   * la différence que le code généré est conforme au W3C et que la syntaxe est
   * modifiée selon qu'il s'agit d'un document HTML ou XHTML.
   * <p>
   * @param document
   * @param indicateur d'un tag XHTML
   * @return un objet String qui contient le code HTML à afficher dans la page
   */
  private static String genererImage(DocumentElectronique document, boolean isXHTML) {
    StringBuffer resultat = new StringBuffer("<img src=\"images\\sofi\\");
    String iconeDefaut = UtilitaireTypeMime.getFichierIcone(UtilitaireDocumentElectronique.TYPE_CONTENU_DEFAUT);

    if (document.getCodeTypeMime() != null) {
      String icone = UtilitaireTypeMime.getFichierIcone(document.getCodeTypeMime());
      if(StringUtils.isEmpty(icone)) {
        icone = iconeDefaut;
      }
      resultat.append(icone);
    } else {
      resultat.append(iconeDefaut);
    }
    resultat.append("\"/>");

    return resultat.toString();
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected StringBuffer genererCodeHtmlComposant() throws JspException {
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    StringBuffer resultat = new StringBuffer();

    if (!isReadonly() && getHref() != null) {
      String aideContextuelle = null;
      Libelle libelleAide = null;
      Libelle libelleAffichage = null;
      Message libelleConfirmation = null;

      // Largeur et hauteur des fenêtres flottantes.
      int hauteurFenetre;
      int largeurFenetre;

      try {
        hauteurFenetre = getHauteurFenetre().intValue();
        largeurFenetre = getLargeurFenetre().intValue();
      } catch (Exception ex) {
        throw new JspException("Vous devez spécifier la hauteur et la largeur de la fenêtre");
      }

      try {
        // Prendre le bon libellé associé
        libelleAffichage =
            UtilitaireLibelle.getLibelle(getLibelle(), pageContext);

        if (getAideContextuelle() != null) {
          libelleAide =
              UtilitaireLibelle.getLibelle(this.getAideContextuelle(),
                  pageContext);
          aideContextuelle = libelleAide.getMessage();
        } else {
          // Vérifier si le libellé d'aide se trouve dans le libellé d'affichage
          if ((libelleAffichage != null) &&
              (libelleAffichage.getAideContextuelle() != null)) {
            aideContextuelle = libelleAffichage.getAideContextuelle();
          }
        }

        if (getMessageConfirmation() != null) {
          libelleConfirmation =
              UtilitaireMessage.get(this.getMessageConfirmation(), null,
                  pageContext);
        }
      } catch (Exception e) {
        throw new JspException(e);
      }

      resultat.append("<button type=\"button\" ");
      resultat.append("value=\"");
      resultat.append(libelleAffichage.getMessage());
      resultat.append("\"");

      if ((aideContextuelle != null) &&
          (aideContextuelle.trim().length() > 0)) {
        resultat.append(" title=\"");
        resultat.append(aideContextuelle);
        resultat.append("\"");
      }

      if (this.property != null) {
        resultat.append(" name=\"");
        resultat.append(property);
        resultat.append("\"");
      }

      if (this.accesskey != null) {
        resultat.append(" accesskey=\"");
        resultat.append(this.accesskey);
        resultat.append("\"");
      }

      if (this.tabindex != null) {
        resultat.append(" tabindex=\"");
        resultat.append(this.tabindex);
        resultat.append("\"");
      }

      if (this.id != null) {
        resultat.append(" id=\"");
        resultat.append(this.id);
        resultat.append("\"");
      }

      // Faire la gestion de l'affichage du composant
      if (this.getDisabled() || this.getReadonly()) {
        resultat.append(" disabled=\"disabled\"");
      }

      StringBuffer inactiverBouton = new StringBuffer();

      if (UtilitaireString.isVide(nomFormulaire)) {
        setInactiverBoutons(false);
      }

      inactiverBouton.append(";inactiverBouton(document.");
      inactiverBouton.append(nomFormulaire);
      inactiverBouton.append(", false);");

      if ((getHref() != null) && (getHref().trim().length() > 0)) {
        StringBuffer onClickBuffer = new StringBuffer();

        if (getOnclick() != null) {
          onClickBuffer.append(getOnclick());
        }

        Href lienComplet = new Href(getHref());
        lienComplet.ajouterParametre("domaineFichier", getDomaineFichier());
        lienComplet.ajouterParametre("libelleDomaineFichier",
            getLibelleDomaineFichier());

        if (getNomFenetre() == null) {
          setNomFenetre("fenetreFichierAttache");
        }

        String urlPopup =
            AfficheurPopup.getFonctionJS(lienComplet.getUrlPourRedirection(),
                largeurFenetre, hauteurFenetre, getNomFenetre(), false, false,
                false, false, false);

        onClickBuffer.append(urlPopup);

        if (libelleConfirmation != null) {
          onClickBuffer.append("};");
        }

        setOnclick(onClickBuffer.toString());
      }
    }

    return resultat;
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    scrollbars = false;
    largeurFenetre = null;
    hauteurFenetre = null;
    nomFenetre = null;
    domaineFichier = null;
    libelleDomaineFichier = null;
    hrefSelectionFichier = null;
    hrefSupprimerFichier = null;
    hrefIconeSupprimerFichier = null;
    hrefSupprimerFichierMessageConfirmation = null;
    hrefSupprimerFichierAjax = false;
    scrollbarsSelectionFichier = false;
    largeurFenetreSelectionFichier = null;
    hauteurFenetreSelectionFichier = null;
    styleClassSelectionFichier = null;
    readonly = false;
    readonlyEL = null;
    decorateurAction = null;
    messageListeVide = null;
    gabaritListe = null;
    super.release();
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  protected void evaluerEL() throws JspException {
    super.evaluerEL();

    try {
      String nomFenetre =
          EvaluateurExpression.evaluerString("nomFenetre", getNomFenetre(), this,
              pageContext);
      setNomFenetre(nomFenetre);
    } catch (JspException e) {
      throw e;
    }

    try {
      String domaineFichier =
          EvaluateurExpression.evaluerString("domaineFichier",
              getDomaineFichier(), this, pageContext);
      setDomaineFichier(domaineFichier);
    } catch (JspException e) {
      throw e;
    }

    try {
      String hrefSupprimerFichierMessageConfirmation =
          EvaluateurExpression.evaluerString("hrefSupprimerFichierMessageConfirmation",
              getHrefSupprimerFichierMessageConfirmation(), this, pageContext);
      setHrefSupprimerFichierMessageConfirmation(hrefSupprimerFichierMessageConfirmation);
    } catch (JspException e) {
      throw e;
    }

    if (UtilitaireBaliseJSP.isValeurEL(getHrefSupprimerFichier())) {
      setHrefSupprimerFichier(EvaluateurExpression.evaluerString("hrefSupprimerFichier",
          getHrefSupprimerFichier(), this, pageContext));
    } else {
      setHrefSupprimerFichier(UtilitaireBaliseJSP.genererUrl(pageContext,
          getHrefSupprimerFichier()));
    }

    if (UtilitaireBaliseJSP.isValeurEL(getHrefIconeSupprimerFichier())) {
      setHrefIconeSupprimerFichier(EvaluateurExpression.evaluerString("hrefIconeSupprimerFichier",
          getHrefIconeSupprimerFichier(), this, pageContext));
    } else {
      setHrefIconeSupprimerFichier(UtilitaireBaliseJSP.genererUrl(pageContext,
          getHrefIconeSupprimerFichier()));
    }

    if (getReadonlyEL() != null) {
      boolean readonly =
          EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
              this, pageContext);
      setReadonly(readonly);
    }

    String messageListeVide =
        EvaluateurExpression.evaluerString("messageListeVide",
            getMessageListeVide(), this, pageContext);

    setMessageListeVide(messageListeVide);

    String gabaritListe =
        EvaluateurExpression.evaluerString("gabaritListe", getGabaritListe(),
            this, pageContext);

    setGabaritListe(gabaritListe);

    try {
      String decorateurAction =
          EvaluateurExpression.evaluerString("decorateurAction",
              getDecorateurAction(), this, pageContext);
      setDecorateurAction(decorateurAction);
    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Obtenir si la barre déroulante est affiche ou pas.
   * @return si la barre déroulante est affiche ou pas
   */
  public boolean isScrollbars() {
    return (this.scrollbars);
  }

  /**
   * Fixer si la barre déroulante est affiche ou pas.
   * @param scrollbars si la barre déroulante est affiche ou pas
   */
  public void setScrollbars(boolean scrollbars) {
    this.scrollbars = scrollbars;
  }

  /**
   * Fixer la largeur de la fenêtre flottante.
   * @param largeurFenetre la largeur de la fenêtre flottante
   */
  public void setLargeurFenetre(Integer largeurFenetre) {
    this.largeurFenetre = largeurFenetre;
  }

  /**
   * Obtenir la largeur de la fenêtre flottante.
   * @return la largeur de la fenêtre flottante
   */
  public Integer getLargeurFenetre() {
    return largeurFenetre;
  }

  /**
   * Fixer la hauteur de la fenêtre flottante.
   * @param hauteurFenetre la hauteur de la fenêtre flottante
   */
  public void setHauteurFenetre(Integer hauteurFenetre) {
    this.hauteurFenetre = hauteurFenetre;
  }

  /**
   * Obtenir la hauteur de la fenêtre flottante.
   * @return la hauteur de la fenêtre flottante
   */
  public Integer getHauteurFenetre() {
    return hauteurFenetre;
  }

  public void setNomFenetre(String nomFenetre) {
    this.nomFenetre = nomFenetre;
  }

  public String getNomFenetre() {
    return nomFenetre;
  }

  public void setDomaineFichier(String domaineFichier) {
    this.domaineFichier = domaineFichier;
  }

  public String getDomaineFichier() {
    return domaineFichier;
  }

  public void setLibelleDomaineFichier(String libelleDomaineFichier) {
    this.libelleDomaineFichier = libelleDomaineFichier;
  }

  public String getLibelleDomaineFichier() {
    return libelleDomaineFichier;
  }

  public void setHrefSelectionFichier(String hrefSelectionFichier) {
    this.hrefSelectionFichier = hrefSelectionFichier;
  }

  public String getHrefSelectionFichier() {
    return hrefSelectionFichier;
  }

  public void setScrollbarsSelectionFichier(boolean scrollbarsSelectionFichier) {
    this.scrollbarsSelectionFichier = scrollbarsSelectionFichier;
  }

  public boolean isScrollbarsSelectionFichier() {
    return scrollbarsSelectionFichier;
  }

  public void setLargeurFenetreSelectionFichier(Integer largeurFenetreSelectionFichier) {
    this.largeurFenetreSelectionFichier = largeurFenetreSelectionFichier;
  }

  public Integer getLargeurFenetreSelectionFichier() {
    return largeurFenetreSelectionFichier;
  }

  public void setHauteurFenetreSelectionFichier(Integer hauteurFenetreSelectionFichier) {
    this.hauteurFenetreSelectionFichier = hauteurFenetreSelectionFichier;
  }

  public Integer getHauteurFenetreSelectionFichier() {
    return hauteurFenetreSelectionFichier;
  }

  public void setStyleClassSelectionFichier(String styleClassSelectionFichier) {
    this.styleClassSelectionFichier = styleClassSelectionFichier;
  }

  public String getStyleClassSelectionFichier() {
    return styleClassSelectionFichier;
  }

  @Override
  public void setReadonly(boolean readonly) {
    this.readonly = readonly;
  }

  public boolean isReadonly() {
    return readonly;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setHrefSupprimerFichier(String hrefSupprimerFichier) {
    this.hrefSupprimerFichier = hrefSupprimerFichier;
  }

  public String getHrefSupprimerFichier() {
    return hrefSupprimerFichier;
  }

  public void setHrefIconeSupprimerFichier(String hrefIconeSupprimerFichier) {
    this.hrefIconeSupprimerFichier = hrefIconeSupprimerFichier;
  }

  public String getHrefIconeSupprimerFichier() {
    return hrefIconeSupprimerFichier;
  }

  public void setHrefSupprimerFichierAjax(boolean hrefSupprimerFichierAjax) {
    this.hrefSupprimerFichierAjax = hrefSupprimerFichierAjax;
  }

  public boolean isHrefSupprimerFichierAjax() {
    return hrefSupprimerFichierAjax;
  }

  public void setHrefSupprimerFichierMessageConfirmation(String hrefSupprimerFichierMessageConfirmation) {
    this.hrefSupprimerFichierMessageConfirmation =
        hrefSupprimerFichierMessageConfirmation;
  }

  public String getHrefSupprimerFichierMessageConfirmation() {
    return hrefSupprimerFichierMessageConfirmation;
  }

  /**
   * Permet de fixer le code HTML avec du JavaScript qui va permettre d'afficher le ou les boutons d'actions
   * après le nom du fichier. L'url de l'action doit être spécifié dans ce décorateur.
   * La valeur de l'identifiant doit être spécifié par [ID] (ex. identiant=[ID]) afin que la balise remplace
   * par l'identifiant du document automatiquement.
   * @since SOFI 2.0.4
   * @param decorateurAction
   */
  public void setDecorateurAction(String decorateurAction) {
    this.decorateurAction = decorateurAction;
  }

  /**
   * Retourne le code HTML avec du JavaScript qui va permettre d'afficher  le ou les boutons d'actions
   * après le nom du fichier. L'url de l'action doit être spécifié dans ce décorateur.
   * La valeur de l'identifiant doit être spécifié par [ID] (ex. identiant=[ID]) afin que la balise remplace par l'identifiant
   * du document automatiquement.
   * @since SOFI 2.0.4
   * @return le résultat du composant qui permet d'appeler un action de supression de fichier attaché.
   */
  public String getDecorateurAction() {
    return decorateurAction;
  }

  /**
   * Permet de fixer un message lorsque la liste de fichier est vide.
   * @param messageListeVide message lorsque la liste de fichier est vide.
   * @since SOFI 2.0.4
   */
  public void setMessageListeVide(String messageListeVide) {
    this.messageListeVide = messageListeVide;
  }

  /**
   * Retourne un message lorsque la liste de fichier est vide.
   * @return un message lorsque la liste de fichier est vide.
   * @since SOFI 2.0.4
   */
  public String getMessageListeVide() {
    return messageListeVide;
  }

  /**
   * Fixer le gabarit du résultat de la liste des fichiers attachés.
   * @param gabaritListe le gabarit du résultat de la liste des fichiers attachés.
   * @since SOFI 2.0.4
   */
  public void setGabaritListe(String gabaritListe) {
    this.gabaritListe = gabaritListe;
  }

  /**
   * Retourne le gabarit du résultat de la liste des fichiers attachés.
   * @return le gabarit du résultat de la liste des fichiers attachés.
   * @since SOFI 2.0.4
   */
  public String getGabaritListe() {
    return gabaritListe;
  }
}
