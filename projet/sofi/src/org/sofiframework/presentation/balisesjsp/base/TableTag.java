/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.utilitaire.UtilitaireObjet;


/**
 * Tag qui permet d'afficher un tableau HTML et de gérer le nombre de colonnes.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 */
public class TableTag extends BaseELTag {
  /**
   * 
   */
  private static final long serialVersionUID = -2894837735671629859L;

  /** Constante pour représenter le nombre de colonnes du tableau */
  public final static String NOMBRE_COLONNES_TABLEAU_COURANT = "nombreColonnesTableauCourant";

  /** Constante pour représenter la position courante du champ */
  public final static String POSITION_COURANTE_DANS_TABLEAU = "positionCouranteDansTableau";

  /** La classe CSS du tableau */
  protected String styleClass;

  /** Le nombre d'espace à l'intérieur des cellules avant le contenu */
  protected String cellpadding;

  /** L'espace entre chaque cellule. */
  protected String cellspacing;

  /** Le nombre de colonnes de champs qui seront générées dans le tableau */
  protected String nbColonnes;

  /** Constructeur par défaut */
  public TableTag() {
  }

  /**
   * Génération de la balises de saisie.
   * <p>
   * Cette balise est utilisée pour afficher un libellé suivi d'une valeur JSTL provenant du langage EL.
   * Voici un exemple de résultat de cette balise :<BR><BR>
   * <i><b>Message : Ceci est mon message.</b></i><BR><BR>
   * La balise qui aurait pu générer ce résultat est la suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libellé="Message" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * Par défaut, cette balise s'affiche sur une seule ligne, cependant il est possible d'en afficher
   * deux par page avec l'utilisation des attributs <code>ouvrirLigne</code> et <code>fermerLigne</code>.
   * Cette balise supporte aussi l'utilisation de libellés dynamiques. Ainsi l'exemple plus haut
   * aurait aussi pu être de la forme suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libelle="org.sofiframework.message.erreur" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * <p>
   * @exception JspException si une exception JSP est lancé.
   * @return la valeur indiquant que faire avec le traitement restant de la page.
   */
  @Override
  public int doStartTag() throws JspException {
    String codeHtml = null;

    codeHtml = this.genererCodeHtmlComposant();

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    }

    if (this.getNbColonnes() == null) {
      this.setNbColonnes("1");
    }

    try {
      pageContext.getRequest().setAttribute(NOMBRE_COLONNES_TABLEAU_COURANT,
          Integer.valueOf(this.getNbColonnes()));
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Le paramètre nbColonnes doit être numérique.");
    }

    pageContext.getRequest().setAttribute(POSITION_COURANTE_DANS_TABLEAU,
        Integer.valueOf("1"));

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() {
    StringBuffer html = new StringBuffer("");

    html.append("<table");

    if (this.getCellpadding() != null) {
      html.append(" cellpadding=\"" + this.getCellpadding() + "\" ");
    }

    if (this.getCellspacing() != null) {
      html.append(" cellspacing=\"" + this.getCellspacing() + "\" ");
    }

    if (this.getStyleClass() != null) {
      html.append(" class=\"" + this.getStyleClass() + "\" ");
    }

    html.append(">");

    return html.toString();
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
    this.cellpadding = null;
    this.cellspacing = null;
    this.nbColonnes = null;
    this.styleClass = null;
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      resultat.append(valeur);
    }

    resultat.append("</table>");

    TagUtils.getInstance().write(pageContext, resultat.toString());

    pageContext.getRequest().removeAttribute(NOMBRE_COLONNES_TABLEAU_COURANT);
    pageContext.getRequest().removeAttribute(POSITION_COURANTE_DANS_TABLEAU);

    return (EVAL_PAGE);
  }

  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  public String getStyleClass() {
    return styleClass;
  }

  public void setCellpadding(String cellpadding) {
    this.cellpadding = cellpadding;
  }

  public String getCellpadding() {
    return cellpadding;
  }

  public void setCellspacing(String cellspacing) {
    this.cellspacing = cellspacing;
  }

  public String getCellspacing() {
    return cellspacing;
  }

  public void setNbColonnes(String nbColonnes) {
    this.nbColonnes = nbColonnes;
  }

  public String getNbColonnes() {
    return nbColonnes;
  }

  public static void gererOuvertureEtFermeturePourBalise(Object balise,
      PageContext contexte) {
    Integer nombreColonnesTableau = (Integer) contexte.getRequest()
        .getAttribute(TableTag.NOMBRE_COLONNES_TABLEAU_COURANT);

    if (nombreColonnesTableau != null) {
      Integer positionCouranteDansTableau = (Integer) contexte.getRequest()
          .getAttribute(TableTag.POSITION_COURANTE_DANS_TABLEAU);

      try {
        UtilitaireObjet.setPropriete(balise, "ouvrirLigne",
            Boolean.valueOf(positionCouranteDansTableau.intValue() == 1));
        UtilitaireObjet.setPropriete(balise, "fermerLigne",
            Boolean.valueOf(
                positionCouranteDansTableau.intValue() == nombreColonnesTableau.intValue()));
      } catch (Exception e) {
        System.out.println(
            "Probleme à fixer la valeur dans l'objet pour la gestion des lignes");
      }

      if (positionCouranteDansTableau.intValue() == nombreColonnesTableau.intValue()) {
        positionCouranteDansTableau = new Integer(1);
      } else {
        positionCouranteDansTableau = new Integer(positionCouranteDansTableau.intValue() +
            1);
      }

      contexte.getRequest().setAttribute(TableTag.POSITION_COURANTE_DANS_TABLEAU,
          positionCouranteDansTableau);
    }
  }
}
