/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageAvertissement;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.message.objetstransfert.MessageInformatif;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesMessage;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise d'affichage des messages informatifs, d'avertissement ou d'erreur.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 1.8 Support de plusieurs messages d'information ou d'avertissement.
 * Utilisation de la feuille de style de sofi pour les icones de type de message.
 * Support ajax ajouté.
 */
public class StatutTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -2778594406017459847L;

  /**
   * le lien relatif de l'icone d'alerte (avertissement).
   */
  static final private String ICONE_ALERTE = "message_icone_avertissement";

  /**
   * le style relatif de l'icone d'attention (erreur).
   */
  static final private String ICONE_ATTENTION = "message_icone_attention";

  /**
   * le style relatif de l'icone demandant un enregistement requis.
   */
  static final private String ICONE_ENREGISTREMENT = "message_icone_enregistrement";

  /**
   * le style relatif de l'icone d'information (information).
   */
  static final private String ICONE_INFORMATION = "message_icone_information";

  /**
   * Le nom d'attribut correspondant a une erreur.
   */
  private String nomAttribut;

  /**
   * Le style Id de la barre de statut.
   */
  private String styleId;

  /**
   * Afficher la barre de statut seulement s'il y a des messages d'associé.
   */
  private boolean afficherSeulementSiMessage;

  /**
   * Ne pas afficher le message d'erreur par défaut
   */
  private boolean exclureMessageErreurDefaut;

  /**
   * Nom de la variable dont le contenu de la réponse sera logé.
   */
  protected String var = null;

  /**
   * Le nom du formulaire dont se retrouve les messages.
   */
  private String nomFormulaire;

  /**
   * Spécifie si la barre de statut doit être traité en mode ajax.
   */
  private boolean ajax = false;

  /** Constructeur par défaut */
  public StatutTag() {
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    String resultat = null;
    this.pageContext.getRequest().setAttribute("BarreStatutActive", Boolean.TRUE);

    HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
    UtilitaireVelocity velocity = new UtilitaireVelocity();

    String nomFormulaire = getNomFormulaire();

    if (nomFormulaire == null) {
      nomFormulaire = (String) pageContext.getSession().getAttribute(Constantes.FORMULAIRE);
    }

    BaseForm formulaire = null;

    if (nomFormulaire != null) {
      formulaire = (BaseForm) pageContext.getRequest().getAttribute(nomFormulaire);

      if (formulaire == null) {
        formulaire = (BaseForm) pageContext.getSession().getAttribute(nomFormulaire);
      }
    }

    boolean messageErreurPresent = false;

    String iconeMessage = null;

    // La liste des messages a afficher dans la barre de statut.
    ArrayList listeMessages = new ArrayList();

    if (this.getNomAttribut() == null) {
      if (formulaire != null) {
        Map lesErreurs = formulaire.getLesErreurs();
        Map lesAvertissements = formulaire.getLesAvertissements();
        Map lesInformations = formulaire.getLesInformations();
        Map lesErreursGenerales = formulaire.getLesErreursGenerales();

        if ((lesErreursGenerales != null) && !lesErreursGenerales.isEmpty()) {
          Iterator iterateur = lesErreursGenerales.values().iterator();

          // Ajouter tous les messages générales dans la liste des messages.
          while (iterateur.hasNext()) {
            listeMessages.add(iterateur.next());
          }

          iconeMessage = ICONE_ATTENTION;
          messageErreurPresent = true;
        } else if ((lesErreurs != null) && !lesErreurs.isEmpty()) {
          MessageErreur premierErreur = (MessageErreur) lesErreurs.values()
              .iterator()
              .next();

          if (UtilitaireString.isVide(premierErreur.getFocus())) {
            Message message = UtilitaireMessage.get(premierErreur.getCleMessage(),
                request);

            // Ajouter le message dans la liste a afficher.
            listeMessages.add(message);
          } else {
            if (!isExclureMessageErreurDefaut()) {
              int nombreErreur = lesErreurs.size();
              String[] tableau = new String[] { Integer.toString(nombreErreur) };

              Message message = UtilitaireMessage.get(getCleMessageErreurGeneral(
                  formulaire, request), tableau, request);

              // Ajouter le message dans la liste a afficher.
              listeMessages.add(message);
              iconeMessage = ICONE_ATTENTION;
              messageErreurPresent = true;
            }
          }
        } else if ((lesAvertissements != null) && !lesAvertissements.isEmpty()) {
          // Si il y a des message d'avertissment ...
          Iterator iterateur = lesAvertissements.values().iterator();

          // Ajouter tous les messages d'avertissement dans la liste des messages.
          while (iterateur.hasNext()) {
            listeMessages.add(iterateur.next());
          }

          iconeMessage = ICONE_ALERTE;
        } else if ((lesInformations != null) && !lesInformations.isEmpty()) {
          // Si il y a des message d'information ...
          Iterator iterateur = lesInformations.values().iterator();

          // Ajouter tous les messages d'avertissement dans la liste des messages.
          while (iterateur.hasNext()) {
            listeMessages.add(iterateur.next());
          }

          iconeMessage = ICONE_INFORMATION;
        }
      }

      // Section modification
      // Si il n'y a pas d'erreurs, on doit placer le message de modification dans le formulaire
      if ((formulaire != null) && !formulaire.isNouveauFormulaire() &&
          !messageErreurPresent) {
        // Extraire si paramètre système.
        String cleMessageInformationModificationFormulaire = GestionParametreSysteme.getInstance()
            .getString(ConstantesParametreSysteme.CLE_MESSAGE_INFORMATION_MODIFICATION_FORMULAIRE);
        String cleClient = null;

        if (UtilitaireString.isVide(cleMessageInformationModificationFormulaire)) {
          cleClient = GestionMessage.getInstance().getFichierConfiguration().get(ConstantesMessage.AVERTISSEMENT_MODIFICATION_FORMULAIRE,
              request);
        } else {
          cleClient = GestionMessage.getInstance().getFichierConfiguration().get(cleMessageInformationModificationFormulaire,
              request);
        }

        Message messageModification = UtilitaireMessage.get(cleClient, request);
        velocity.ajouterAuContexte("iconeModification", ICONE_ENREGISTREMENT);
        velocity.ajouterAuContexte("messageModification",
            messageModification.getMessage());
      }

      if (!messageErreurPresent && (listeMessages.size() == 0)) {
        Message message = (Message) request.getSession().getAttribute(Constantes.MESSAGE_BARRE_STATUT);

        if (message != null) {
          if (MessageInformatif.class.isInstance(message)) {
            iconeMessage = ICONE_INFORMATION;
          }

          if (MessageErreur.class.isInstance(message)) {
            iconeMessage = ICONE_ATTENTION;
          }

          if (MessageAvertissement.class.isInstance(message)) {
            iconeMessage = ICONE_ALERTE;
          }

          if (iconeMessage == null) {
            iconeMessage = ICONE_INFORMATION;
          }

          // Ajouter le message dans la liste a afficher.
          listeMessages.add(message);
        }
      }

      velocity.ajouterAuContexte("nbListe", new Integer(listeMessages.size()));

      // Spécifier l'icone correspondant au(x) message(s) a afficher.
      velocity.ajouterAuContexte("iconeMessage", iconeMessage);

      // Spécifier la liste des message à afficher dans la barre de statut (messages).
      velocity.ajouterAuContexte("listeMessages", listeMessages);

      String gabarit = (String) GestionParametreSysteme.getInstance()
          .getParametreSysteme("velocity.gabarit.barre.statut");

      if (!isAjax()) {
        if (!UtilitaireString.isVide(getStyleId())) {
          velocity.ajouterAuContexte("styleId", getStyleId());
        } else {
          velocity.ajouterAuContexte("styleId", "message_erreur");
          setStyleId("message_erreur");
        }
      }

      if ((gabarit == null) || "".equals(gabarit)) {
        if (!isAjax()) {
          resultat = velocity.genererHTML(
              "org/sofiframework/presentation/velocity/gabarit/barreStatut.vm");
        } else {
          resultat = velocity.genererHTML(
              "org/sofiframework/presentation/velocity/gabarit/barre_statut_ajax.vm");
        }
      } else {
        resultat = velocity.genererHTML(gabarit);
      }
    } else {
      if (formulaire != null) {
        Map lesErreurs = formulaire.getLesErreurs();

        if (lesErreurs.containsKey(this.getNomAttribut())) {
          MessageErreur erreur = (MessageErreur) lesErreurs.get(this.getNomAttribut());
          resultat = "<span class=\"libelleErreur\">" + erreur.getMessage() +
              "</span>";
        } else {
          resultat = "";
        }
      }
    }

    if (isAfficherSeulementSiMessage() && (listeMessages.size() == 0)) {

      StringBuffer resultatVide = new StringBuffer();
      resultatVide.append("<div id=\"");
      resultatVide.append(getStyleId());
      resultatVide.append("\"><div class=\"clear\"></div></div>");

      TagUtils.getInstance().write(pageContext, resultatVide.toString());
    } else {
      if (UtilitaireString.isVide(getVar())) {
        TagUtils.getInstance().write(pageContext, resultat.toString());
      } else {
        pageContext.setAttribute(getVar(), resultat);
      }
    }

    if ((formulaire != null) && isAjax()) {
      formulaire.initialiserLesMessagesGeneraux();
    }

    request.getSession().removeAttribute(Constantes.MESSAGE_BARRE_STATUT);

    return Tag.EVAL_PAGE;
  }

  private String getCleMessageErreurGeneral(BaseForm formulaire,
      HttpServletRequest request) {
    String cleClient = GestionMessage.getInstance().getFichierConfiguration()
        .get(ConstantesMessage.ERREUR_BARRE_STATUT_NB_ERREUR,
            request);

    // Si clé de message spécifique pour l'application, nous l'utilisons.
    if (!"".equals(GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.CLE_MESSAGE_ERREUR_GENERAL))) {
      cleClient = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.CLE_MESSAGE_ERREUR_GENERAL);
    }

    // Si clé de message spécifique pour le formulaire, nous l'utilisons.
    if ((formulaire.getCleMessageErreurGeneral() != null) &&
        !"".equals(formulaire.getCleMessageErreurGeneral())) {
      cleClient = formulaire.getCleMessageErreurGeneral();
    }

    return cleClient;
  }

  @Override
  public void release() {
    setNomAttribut(null);
    setNomFormulaire(null);
    setExclureMessageErreurDefaut(false);
    setAfficherSeulementSiMessage(false);
    setStyleId(null);
    setAjax(false);
    setVar(null);
  }

  /**
   * Fixer le nom d'attribut qui peut être en erreur. En spécifiant
   * cette propriété, la barre de statut va seulement afficher le message correspondant
   * au nom attribut.
   * @param nomAttribut le nom d'attribut qui peut être en erreur
   */
  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  /**
   * Retourne le nom d'attribut qui peut être en erreur. En spécifiant
   * cette propriété, la barre de statut va seulement afficher le message correspondant
   * au nom attribut.
   * @return le nom d'attribut qui peut être en erreur.
   */
  public String getNomAttribut() {
    return nomAttribut;
  }

  /**
   * Fixer l'identifiant (id) associé au div de la barre de statut.
   * @param styleId l'identifiant (id) associé au div de la barre de statut.
   */
  public void setStyleId(String styleId) {
    this.styleId = styleId;
  }

  /**
   * Retourne l'identifiant (id) associé au div de la barre de statut.
   * @return l'identifiant (id) associé au div de la barre de statut.
   */
  public String getStyleId() {
    return styleId;
  }

  /**
   * Fixer true si la barre de statut doit s'afficher seulement lors qu'il y a des
   * messages à afficher.
   * @param afficherSeulementSiMessage true si la barre de statut doit s'afficher seulement lors
   * qu'il y a des messages à afficher.
   */
  public void setAfficherSeulementSiMessage(boolean afficherSeulementSiMessage) {
    this.afficherSeulementSiMessage = afficherSeulementSiMessage;
  }

  /**
   * Est-ce que la barre de statut doit s'afficher seulement lors qu'il y a des
   * messages à afficher.
   * @return true si la barre de statut doit s'afficher seulement lors qu'il y a des
   * messages à afficher.
   */
  public boolean isAfficherSeulementSiMessage() {
    return afficherSeulementSiMessage;
  }

  /**
   * Fixer true si vous désirez ne pas afficher le message d'erreur par défaut.
   * @param exclureMessageErreurDefaut true si vous désirez ne pas afficher le message d'erreur par défaut.
   */
  public void setExclureMessageErreurDefaut(boolean exclureMessageErreurDefaut) {
    this.exclureMessageErreurDefaut = exclureMessageErreurDefaut;
  }

  /**
   * Spécifie si on désire ne pas afficher le message d'erreur par défaut.
   * @return true si on désire ne pas afficher le message d'erreur par défaut.
   */
  public boolean isExclureMessageErreurDefaut() {
    return exclureMessageErreurDefaut;
  }

  /**
   * Fixer le nom du formulaire dont l'on désire extraire les
   * messages qui y sont associé.
   * @param nomFormulaire le nom du formulaire dont l'on désire extraire les
   * messages qui y sont associé.
   */
  public void setNomFormulaire(String nomFormulaire) {
    this.nomFormulaire = nomFormulaire;
  }

  /**
   * Retourne le nom du formulaire dont l'on désire extraire les
   * messages qui y sont associé.
   * @return le nom du formulaire dont l'on désire extraire les
   * messages qui y sont associé.
   */
  public String getNomFormulaire() {
    return nomFormulaire;
  }

  /**
   * Fixer le nom de la variable dont le contenu de la réponse sera logé.
   * @param var le nom de la variable dont le contenu de la réponse sera logé.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom de la variable dont le contenu de la réponse sera logé.
   * @return le nom de la variable dont le contenu de la réponse sera logé.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer true si vous désirez utilisé la barre de statut dans
   * un mode ajax.
   * @param ajax true si vous désirez utilisé la barre de statut dans
   * un mode ajax.
   */
  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  /**
   * Spécifie si on désire une barre de statut dans un mode ajax.
   * @return true si on désire une barre de statut dans un mode ajax.
   */
  public boolean isAjax() {
    return ajax;
  }
}
