/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;


/**
 * Tag de présetation de la compraison entre deux éléments de donnée.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class ComparaisonTag extends AffichageELTag {
  /**
   * 
   */
  private static final long serialVersionUID = -8755215002109426713L;

  /**
   * Gabarie utilisé pour former le html.
   */
  private static final String tag = "<balise class=\"styleClass\" title=\"titre\">donneePresentee</balise>&nbsp;";

  /**
   * Données qui sera présentée à l'écran.
   */
  private Object donneePresentee;

  /**
   * Donnée qui sera comparée à la donnée présentée.
   */
  private Object donneeComparee;

  /**
   * Style à utiliser lorsque les deux données sont différentes.
   */
  private String styleDifferent;

  /**
   * Style a utiliser lorsque les deux données sont identiques.
   */
  private String styleIdentique;

  /**
   * Type de balise à utiliser. Le type span est choisi par défaut.
   */
  private String balise = "span";

  /**
   * Il est possible de fournir un message personnalisé.
   * Le paramètre 0 est la donnée présentée.
   * La donnée 1 est la donnée comparée.
   */
  private String message = "{1}";

  /**
   * Permet d'obtenir la valeur EL d'un parameter du tag.
   * Cette méthode est surchargée du tag BaseEL afin d'encapsuler la comparaison.
   * @param nomParametre nom de la propriété de la classe qui recoit la faleur JSTL.
   * @param valeurParametre l'expression JSTL qui doit être interprétée.
   * @param classeParametre la classe de l'élément qui correspond à l'expression JSTL.
   * @return la balise de comparaison html avec la valeur des données comparées.
   */
  @Override
  public Object getValeurEL(String nomParametre, Object valeurParametre,
      Class classeParametre) {
    if (donneePresentee == null) {
      donneePresentee = "";
    }

    if (donneeComparee == null) {
      donneeComparee = "";
    }

    boolean different = !this.donneePresentee.equals(donneeComparee);

    String html = tag.replaceAll("balise", this.balise);

    if (!donneePresentee.equals("")) {
      html = html.replaceAll("donneePresentee", getAffichage(donneePresentee));
    } else {
      html = html.replaceAll("donneePresentee", "&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    if (different) {
      html = html.replaceAll("styleClass", this.styleDifferent);

      if (getAffichage(donneeComparee).length() == 0) {
        html = html.replaceAll("titre", "&nbsp;&nbsp;&nbsp;&nbsp;");
      } else {
        html = html.replaceAll("titre", this.getAffichageMessage());
      }
    } else {
      html = html.replaceAll("styleClass", this.styleIdentique);
      html = html.replaceAll("titre", "");
    }

    return html;
  }

  /**
   * Retourne le message affiché en titre avec son formatage final. Integre les données comparées dans le message.
   * @return le titre formaté.
   */
  private String getAffichageMessage() {
    MessageFormat format = new MessageFormat(message);

    return format.format(new Object[] {
        getAffichage(this.donneePresentee), getAffichage(this.donneeComparee)
    });
  }

  /**
   * Formatte un objet selon sa classe.
   * @param donnee objet à formater
   * @return objet formaté
   */
  private static String getAffichage(Object donnee) {
    if (donnee instanceof String) {
      return (String) donnee;
    } else if (donnee instanceof java.util.Date) {
      String formatDate = GestionParametreSysteme.getInstance().getString("formatDate");
      SimpleDateFormat sdf = new SimpleDateFormat(formatDate);

      return sdf.format((java.util.Date) donnee);
    } else {
      return donnee.toString();
    }
  }

  /**
   * Fixer la donné qui est présentée suite au libllé.
   * <p>
   * @param donneePresentee la donnée qui est présentée suite au libllé
   */
  public void setDonneePresentee(String donneePresentee) {
    this.donneePresentee = super.getValeurEL("donneePresentee",
        donneePresentee, Object.class);
  }

  /**
   * Obtenir la donné qui est présentée suite au libllé.
   * <p>
   * @return la donné qui est présentée suite au libllé
   */
  public Object getDonneePresentee() {
    return donneePresentee;
  }

  /**
   * Fixer la donnée qui sera comparée à la donnée présentée.
   * <p>
   * @param donneeComparee Donnée qui sera comparée à la donnée présentée
   */
  public void setDonneeComparee(String donneeComparee) {
    this.donneeComparee = super.getValeurEL("donneeComparee", donneeComparee,
        Object.class);
  }

  /**
   * Obtenir la donnée qui sera comparée à la donnée présentée.
   * <p>
   * @return Donnée qui sera comparée à la donnée présentée
   */
  public Object getDonneeComparee() {
    return donneeComparee;
  }

  /**
   * Fixer le style à utiliser lorsque les deux données sont différentes.
   * <p>
   * @param styleDifferent Style à utiliser lorsque les deux données sont différentes
   */
  public void setStyleDifferent(String styleDifferent) {
    this.styleDifferent = styleDifferent;
  }

  /**
   * Obtenir le style à utiliser lorsque les deux données sont différentes.
   * <p>
   * @return Style à utiliser lorsque les deux données sont différentes
   */
  public String getStyleDifferent() {
    return styleDifferent;
  }

  /**
   * Fixer le style a utiliser lorsque les deux données sont identiques.
   * <p>
   * @param styleIdentique Style a utiliser lorsque les deux données sont identiques
   */
  public void setStyleIdentique(String styleIdentique) {
    this.styleIdentique = styleIdentique;
  }

  /**
   * Obtenir le style a utiliser lorsque les deux données sont identiques.
   * <p>
   * @return Style a utiliser lorsque les deux données sont identiques
   */
  public String getStyleIdentique() {
    return styleIdentique;
  }

  /**
   * Fixer le type de balise à utiliser. Le type span est choisi par défaut.
   * <p>
   * @param balise Type de balise à utiliser. Le type span est choisi par défaut
   */
  public void setBalise(String balise) {
    this.balise = balise;
  }

  /**
   * Obtenir le type de balise a utiliser. Le type span est choisi par défaut.
   * <p>
   * @return Type de balise a utiliser. Le type span est choisi par défaut
   */
  public String getBalise() {
    return balise;
  }

  /**
   * Fixer le messag qui sera présenté comme titre (info-bulle)
   * Il est possible de fournir un message personnalisé.
   * Le paramètre 0 est la donnée présentée.
   * La donnée 1 est la donnée comparée.
   * <p>
   * @param message le message qui explique la comparaison
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Obtnir le message qui sera présenté comme titre (info-bulle)
   * Il est possible de fournir un message personnalisé.
   * Le paramètre 0 est la donnée présentée.
   * La donnée 1 est la donnée comparée.
   * <p>
   * @return le message qui explique la comparaison
   */
  public String getMessage() {
    return message;
  }
}
