/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.TagUtils;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.sofiframework.utilitaire.UtilitaireException;
import org.sofiframework.utilitaire.UtilitaireHttp;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise permettant d'importer du contenu dynamiquement via Http Client.
 * Cette balise permet d'inclure un url de base via un paramètre système et
 * permet d'ajouter un extension a l'appel de la page afin de traiter
 * le multilingue, ex: _fr, _en
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.8
 * @since SOFI 2.0.1 La propriété inclureCertificatDansUrl a été
 * ajouté afin de permettre d'inclure le certificat d'authentification
 * dans l'url de la page qui doit être inclus dynamiquement, ceci permet
 * d'appliquer le mode SSO (Authentification unique).
 */
public class ContenuDynamiqueTag extends TagSupport {

  private static final long serialVersionUID = -2045393889308019635L;


  /**
   * Le paramètre de remplacement dynamique pour fixer le code de
   * langue dans la structure de l'url externe.
   */
  public static final String PARAM_CODE_LANGUE = "[CODE_LANGUE]";


  /**
   * Le paramètre de remplacement dynamique pour fixer le nom de
   * la page dans la structure de l'url externe.
   */
  public static final String PARAM_PAGE = "[PAGE]";

  /**
   * Le paramètre de remplacement dynamique pour fixer l'extension de
   * la page dans la structure de l'url externe.
   */
  public static final String PARAM_PAGE_EXTENSION = "[PAGE_EXTENSION]";

  /**
   * La base de l'url a communiquer.
   * @since SOFI 2.0.2 si url de spécifier, on concatène baseUrl avec url.
   */
  private String baseUrl = null;

  // Spécifie seulement l'url de l'application sans la base de url.

  /**
   * Spécifie seulement l'url de l'application sans la base de url.
   * @since SOFI 2.0.2
   */
  private String url = null;

  // Le nom de la page a importer.
  private String page = null;

  // Est-ce que l'importation gère le multilingue.
  private boolean multilingue = false;

  // L'extension de la page à importer, ex: html, jsp, aspx
  private String pageExtension = null;

  // Inclure le certificat d'authentification dans l'url appelé.
  private boolean inclureCertificatDansUrl = false;

  // Permet de modifier tous les lien ajax dans le fragemnt de page pour spécier l'url de base.
  private boolean remplacerLienAjaxPourBaseUrl = false;

  // Spécifie l'url qui permet de faire de l'affichage de contenu dynamique pour
  // les liens qui sont spécifiés dans une zone de contenu dynamique.
  private String urlAffichageContenuDynamique;

  // Spécifier un nom de Cache pour meilleur performance.
  private String nomCache;
  
  private String cacheExpiration;

  // Spécifier l'indicateur de début du fragement a extraire de la page.
  private String fragmentIndicateurDebut;

  // Spécifier l'indicateur de fin du fragment a extraire de la page.
  private String fragmentIndicateurFin;
  
  // Le nom de la variable qui va loger le résultat du contenu extrait par la balise.
  private String var;
  
  // Extraire tout le contenu et non pas seulement le body.
  private boolean allContent = false;


  /** Constructeur par défaut */
  public ContenuDynamiqueTag() {
  }

  /**
   * Retourne le contenu d'un page dynamique appellé par une url.
   * @return le contenu d'un page dynamique appellé par une url.
   * @param contexte le contexte de la page.
   * @param inclureCertificatDansUrl Est-ce qu'on doit inclure le certificat d'authentication unique.
   * @param structureUrlExterne la structure de l'url externe a appliquer.
   * @param multilingue est-ce que le multiligue est supporté.
   * @param pageExtension l'extension de la page du contenu dynamique.
   * @param page le nom de la page.
   * @param baseUrl la base de l'url.
   */
  public static String getContenuDynamique(String urlComplet, String url, String page,
      String pageExtension, boolean multilingue, String structureUrlExterne,
      boolean inclureCertificatDansUrl, String nomCache, String cacheExpiration, String fragmentIndicateurDebut, String fragmentIndicateurFin, boolean allContent, PageContext contexte) {

    if (urlComplet == null && url != null) {
      urlComplet = url;
    }

    Locale localeUtilisateur = TagUtils.getInstance().getLocale(contexte);

    Href urlAvecCertificat = new Href(urlComplet);
    HttpServletRequest request = (HttpServletRequest) contexte.getRequest();

    if (inclureCertificatDansUrl) {
      String certificat = TagUtils.getInstance().getCertificat(request);

      if (certificat != null) {
        urlAvecCertificat.ajouterParametre(GestionSecurite.getInstance().getNomCookieCertificat(), certificat);
      } else {
        throw new SOFIException(
            "Le certificat n'est pas disponible, donc l'inclusion de la page ne peut ëtre complété.");
      }
    }

    return getContenuDynamique(urlAvecCertificat.getUrlPourRedirection(), url, page,
        pageExtension, multilingue, inclureCertificatDansUrl, localeUtilisateur,
        structureUrlExterne, nomCache, cacheExpiration, fragmentIndicateurDebut, fragmentIndicateurFin, allContent, request);
  }

  /**
   * Retourne le contenu d'un page dynamique appellé par une url.
   * @return le contenu d'un page dynamique appellé par une url.
   * @param structureUrlExterne la structure de l'url externe a appliquer.
   * @param localeUtilisateur le locale de l'utilisateur le contexte de la page.
   * @param inclureCertificatDansUrl Est-ce qu'on doit inclure le certificat d'authentication unique.
   * @param multilingue est-ce que le multiligue est supporté.
   * @param pageExtension l'extension de la page du contenu dynamique.
   * @param page le nom de la page.
   * @param baseUrl la base de l'url.
   */
  public static String getContenuDynamique(String baseUrl, String url, String page,
      String pageExtension, boolean multilingue,
      boolean inclureCertificatDansUrl, Locale localeUtilisateur,
      String structureUrlExterne, String nomCache, String cacheExpiration) {
    return getContenuDynamique(baseUrl, url, page,
        pageExtension, multilingue,
        inclureCertificatDansUrl, localeUtilisateur,
        structureUrlExterne, nomCache, cacheExpiration, null, null, false, null);
  }

  /**
   * Retourne le contenu d'un page dynamique appellé par une url.
   * @return le contenu d'un page dynamique appellé par une url.
   * @param structureUrlExterne la structure de l'url externe a appliquer.
   * @param localeUtilisateur le locale de l'utilisateur le contexte de la page.
   * @param inclureCertificatDansUrl Est-ce qu'on doit inclure le certificat d'authentication unique.
   * @param multilingue est-ce que le multiligue est supporté.
   * @param pageExtension l'extension de la page du contenu dynamique.
   * @param page le nom de la page.
   * @param baseUrl la base de l'url.
   * @param request Requête en cours qui doit être utilisée pour la copie de cookies.
   */
  public static String getContenuDynamique(String baseUrl, String url, String page,
      String pageExtension, boolean multilingue,
      boolean inclureCertificatDansUrl, Locale localeUtilisateur,
      String structureUrlExterne, String nomCache, String cacheExpiration,
      String fragmentIndicateurDebut, String fragmentIndicateurFin, boolean allContent, HttpServletRequest request) {
    // Vérifier l'existance dans les paramètres systèmes.
    String parametrePage = (String) GestionParametreSysteme.getInstance()
        .getParametreSysteme(page);
    if (parametrePage != null) {
      page = parametrePage;
    }

    StringBuffer resultatUrlComplet = new StringBuffer();
    resultatUrlComplet.append(url);

    if (url.substring(url.length()-1).indexOf("/") == -1 &&
        (pageExtension != null)) {
      resultatUrlComplet.append("/");
    }

    if (multilingue) {
      structureUrlExterne = UtilitaireString.remplacerTous(structureUrlExterne,
          PARAM_CODE_LANGUE, localeUtilisateur.getLanguage());
    }

    if ((page != null) && (pageExtension != null)) {
      structureUrlExterne = UtilitaireString.remplacerTous(structureUrlExterne, PARAM_PAGE, parametrePage);
      structureUrlExterne = UtilitaireString.remplacerTous(structureUrlExterne, PARAM_PAGE_EXTENSION, pageExtension);

      // Ajouter la structure modifié dans l'url.
      resultatUrlComplet.append(structureUrlExterne);
    }

    String contenuRetour = getHtml(resultatUrlComplet.toString(), nomCache, cacheExpiration, allContent, request);

    if (contenuRetour.indexOf(fragmentIndicateurDebut) != -1) {
      String[] retour  = UtilitaireString.substrings(contenuRetour, fragmentIndicateurDebut, fragmentIndicateurFin);
      contenuRetour = retour[0];
      if (baseUrl != null) {
        // Ré-écriture des url des images.
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "src=\"", "src=\"" + baseUrl);
        // Ré-écriture des url des liens en position relative au wiki.
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "href=\"/", "href=\"" + baseUrl + "/");
      }
    }
    
    return contenuRetour;
  }

  public static String getContenuDynamique(String baseUrl, String url, String page,
      String pageExtension, boolean multilingue,
      boolean inclureCertificatDansUrl, Locale localeUtilisateur,
      String structureUrlExterne) {
    return getContenuDynamique(baseUrl, url, page, pageExtension, multilingue, inclureCertificatDansUrl, localeUtilisateur, structureUrlExterne, null, null, null, null, false, null);
  }

  private static String getHtml(String url, String nomCache, String cacheExpiration, boolean allContent, HttpServletRequest request) {
    String contenuRetour = null;
    try {
      
      Long cacheExpirationMilliSeconds = null;
      


      // Utilisation de la cache
      ObjetCache objetCache = GestionCache.getInstance().getCache(nomCache, null);
      
      if (cacheExpiration != null && objetCache != null && objetCache.getDateCreation() != null) {
        cacheExpirationMilliSeconds = new Long(cacheExpiration) * 1000;
        java.util.Date now = new java.util.Date();
        if (now.getTime() > (objetCache.getDateCreation().getTime() + cacheExpirationMilliSeconds)) {
          objetCache = null;
        }
      }

      // Création de la définition si existe pas.
      GestionCache.getInstance().ajouterDefinitionObjetCache(nomCache);

      String contenuCache = null;

      boolean contenuCacheExiste = false;

      if (objetCache != null) {
        contenuCache = (String)objetCache.get(url);

        if (!UtilitaireString.isVide(contenuCache)){
          contenuCacheExiste = true;
        }

      }

      if (!contenuCacheExiste) {

        boolean memeServeur = false;
        if (request != null) {
          String base = getBaseUrl(request);
          memeServeur = url.startsWith(base);
        }
        contenuRetour = UtilitaireHttp.getContenuHtml(
            url, memeServeur ? request : null
            );

        if (!allContent) {
          contenuRetour = UtilitaireHttp.getBody(contenuRetour);
          contenuRetour = UtilitaireString.remplacerTous(contenuRetour.toString(), "\"../", "\"" + getBaseUrl(request) + "/");
        }

        GestionCache.getInstance().ajouterObjetCache(nomCache, url, contenuRetour);
      }else {
        contenuRetour = contenuCache;
      }
    } catch (Exception e) {
      StringBuffer contenuErreur = new StringBuffer();
      contenuErreur.append("<!--");
      contenuErreur.append(UtilitaireException.getMessageExceptionAuComplet(e));
      contenuErreur.append("-->\n");
      contenuRetour = contenuErreur.toString();
    }


    return contenuRetour;
  }

  private static String getBaseUrl(HttpServletRequest request) {
    String base = UtilitaireRequest.getUrlApplicationWeb(request);
    int indexWebInf = base.indexOf("/WEB-INF");
    /*
     * Il se peut que l'url contienne le WEB-INF
     * si le contenu est dans un Tiles. On ne désire
     * pas cette partie de l'url.
     */
    if (indexWebInf != -1) {
      base = base.substring(0, indexWebInf);
    }
    return base;
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    StringBuffer resultat = new StringBuffer();

    // Vérifier l'existance dans les paramètres systèmes.
    String parametreUrl = (String)GestionParametreSysteme.getInstance()
        .getParametreSysteme(getBaseUrl());

    if (UtilitaireString.isVide(parametreUrl)) {
      parametreUrl = getBaseUrl();
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(parametreUrl,
          parametreUrl);
    }

    if (parametreUrl != null) {
      setBaseUrl(parametreUrl);
    }

    // Vérifier si paramètre système.
    String parametreUrlAffichageContenuDynamique = GestionParametreSysteme.getInstance()
        .getString(getUrlAffichageContenuDynamique());

    if (UtilitaireString.isVide(parametreUrlAffichageContenuDynamique)) {
      parametreUrlAffichageContenuDynamique = getUrlAffichageContenuDynamique();
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(parametreUrlAffichageContenuDynamique,
          parametreUrlAffichageContenuDynamique);
    }

    if (parametreUrlAffichageContenuDynamique != null) {
      setUrlAffichageContenuDynamique(parametreUrlAffichageContenuDynamique);
    }

    // Vérifier l'existance dans les paramètres systèmes.
    String parametrePage = (String)GestionParametreSysteme.getInstance()
        .getParametreSysteme(getPage());

    if (UtilitaireString.isVide(parametrePage)) {
      parametrePage = getPage();
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(parametrePage,
          parametrePage);
    }

    String structureUrlExterneContenuDynamique = GestionParametreSysteme
        .getInstance().getString(
            ConstantesParametreSysteme.CONTENU_DYNAMIQUE_STRUCTURE__EXTERNE);

    if ((UtilitaireString.isVide(structureUrlExterneContenuDynamique)) &&
        (getPage() != null)) {
      throw new SOFIException(
          "Vous devez spécifier une valeur pour le paramètre système 'contenuDynamiqueStructureUrlExterne'.");
    }
    
    String cacheExpirationParam = GestionParametreSysteme.getInstance()
        .getString(ConstantesParametreSysteme.CONTENU_DYNAMIQUE_CACHE_EXPIRATION);
    
    if (!StringUtils.isEmpty(cacheExpirationParam)) {
      setCacheExpiration(cacheExpirationParam);
    }


    try {
      String urlValide = this.composerUrlComplet((HttpServletRequest) pageContext.getRequest());

      String contenu = getContenuDynamique(baseUrl, urlValide, getPage(),
          getPageExtension(), isMultilingue(),
          structureUrlExterneContenuDynamique, isInclureCertificatDansUrl(), getNomCache(), getCacheExpiration(), getFragmentIndicateurDebut(), getFragmentIndicateurFin(), isAllContent(),
          pageContext);

      if (isRemplacerLienAjaxPourBaseUrl()) {
        contenu = UtilitaireString.remplacerTous(contenu, ",lienAjax,",
            ",&#039;contenuDynamique.do?url=&#039; +escape(lienAjax),");
      }
      
      if (getVar() != null) {
        pageContext.setAttribute(getVar(), contenu);
      }else {
        resultat.append(contenu);
      }

     
    } catch (Exception e) {
      resultat.append(e.getMessage());
    }

    // Écrire la réponse au client.
    TagUtils.getInstance().write(pageContext, resultat.toString());

    return Tag.EVAL_PAGE;
  }

  private String composerUrlComplet(HttpServletRequest request) {
    StringBuffer urlComplet = new StringBuffer();

    boolean urlHttpExterne = getUrl() != null && getUrl().startsWith("http");

    if (!urlHttpExterne) {
      if (UtilitaireString.isVide(this.baseUrl)) {
        String base = getBaseUrl(request);
        urlComplet = new StringBuffer(base + "/");
      } else {
        urlComplet = new StringBuffer(this.getBaseUrl());
      }
    }

    if (getUrl() != null) {
      urlComplet.append(getUrl());
    }

    String urlValide = UtilitaireString.remplacerTous(urlComplet.toString(), "&amp;", "&");
    return urlValide;
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String baseUrl = EvaluateurExpression.evaluerString("baseUrl",
        getBaseUrl(), this, pageContext);

    if ((baseUrl != null) && baseUrl.equals(getBaseUrl())) {
      // Vérifier si paramètre système.
      baseUrl = GestionParametreSysteme.getInstance().getString(getBaseUrl());
    }

    setBaseUrl(baseUrl);

    String url = EvaluateurExpression.evaluerString("url", getUrl(), this,
        pageContext);

    if ((url != null) && url.equals(getUrl())) {
      // Vérifier si paramètre système.
      url = GestionParametreSysteme.getInstance().getString(getUrl());
    }

    if (!"".equals(url)) {
      setUrl(url);
    }

    String urlAffichageContenuDynamique = EvaluateurExpression.evaluerString("urlAffichageContenuDynamique",
        getUrlAffichageContenuDynamique(), this, pageContext);

    if ((urlAffichageContenuDynamique != null) &&
        urlAffichageContenuDynamique.equals(getUrlAffichageContenuDynamique())) {
      // Vérifier si paramètre système.
      urlAffichageContenuDynamique = GestionParametreSysteme.getInstance()
          .getString(getUrlAffichageContenuDynamique());
    }

    setUrlAffichageContenuDynamique(urlAffichageContenuDynamique);

    String page = EvaluateurExpression.evaluerString("page", getPage(), this,
        pageContext);
    setPage(page);

    String pageExtension = EvaluateurExpression.evaluerString("pageExtension",
        getPageExtension(), this, pageContext);
    setPageExtension(pageExtension);
  }

  @Override
  public int doEndTag() throws JspException {
    this.baseUrl = null;
    this.url = null;
    this.page = null;
    this.urlAffichageContenuDynamique = null;
    return super.doEndTag();
  }

  /**
   * Fixer l'url de base du contenu qui sera ajouté
   * dynamiquement à la page.
   * @param baseUrl l'url de base du contenu qui sera ajouté
   * dynamiquement à la page.
   * @since SOFI 2.0.2 si url de spécifier, on concatène baseUrl avec url.
   * Il est maintenant possible de spécifier directement un paramètre système.
   */
  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * Retourne l'url de base du contenu qui sera ajouté
   * dynamiquement à la page.
   * @return l'url de base du contenu qui sera ajouté
   * dynamiquement à la page.
   * @since SOFI 2.0.2 si url de spécifier, on concatène baseUrl avec url.
   * Il est maintenant possible de spécifier directement un paramètre système.
   */
  public String getBaseUrl() {
    return baseUrl;
  }

  /**
   * Fixer le nom de la page à inclure.
   * @param page le nom de la page a inclure.
   */
  public void setPage(String page) {
    this.page = page;
  }

  /**
   * Retourne le nom de la page à inclure.
   * @return le nom de la page à inclure.
   */
  public String getPage() {
    return page;
  }

  /**
   * Fixer true si le multilingue doit être
   * géré.
   * @param multilingue true si le multilingue doit être
   * géré.
   */
  public void setMultilingue(boolean multilingue) {
    this.multilingue = multilingue;
  }

  /**
   * Est-ce que le multilingue doit être géré.
   * @return true si le multilingue doit être géré.
   */
  public boolean isMultilingue() {
    return multilingue;
  }

  /**
   * Fixer l'extension de la page qui doit être inclus
   * dynamiquement.
   * @param pageExtension  l'extension de la page qui doit être inclus
   * dynamiquement.
   */
  public void setPageExtension(String pageExtension) {
    this.pageExtension = pageExtension;
  }

  /**
   * Retourne l'extension de la page à être
   * inclus dynamiquement.
   * @return  l'extension de la page à être
   * inclus dynamiquement.
   */
  public String getPageExtension() {
    return pageExtension;
  }

  /**
   * Fixer si on désire inclure le certificat
   * d'authentification dans l'appel de la page qui doit
   * être inclus dynamiquement.
   * @param inclureCertificatDansUrl true si on
   * désire inclure le certificat d'authentifion
   * dans l'appel de la page qui doit être inclus dynamiquement.
   */
  public void setInclureCertificatDansUrl(boolean inclureCertificatDansUrl) {
    this.inclureCertificatDansUrl = inclureCertificatDansUrl;
  }

  /**
   * Est-ce que l'on doit inclure le certificat d'authentification
   * dans l'appel de la page qui doit être inclus dynamiquement.
   * @return true si on doit inclure le certificat d'authentification
   * dans l'appel de la page qui doit être inclus dynamiquement.
   */
  public boolean isInclureCertificatDansUrl() {
    return inclureCertificatDansUrl;
  }

  /**
   * Retourne l'url spécifique de la page de l'application a appeler.
   * @param url l'url spécifique de la page de l'application a appeler.
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Fixer l'url spécifique de la page de l'application a appeler.
   * @return l'url spécifique de la page de l'application a appeler.
   */
  public String getUrl() {
    return url;
  }

  /**
   * Fixer true si vous désirez remplacer tous les lien ajax dans le fragemnt de page pour spécier l'url de base.
   * @param remplacerLienAjaxPourBaseUrl true pour remplacer tous les lien ajax dans le fragemnt de page pour spécier l'url de base.
   */
  public void setRemplacerLienAjaxPourBaseUrl(
      boolean remplacerLienAjaxPourBaseUrl) {
    this.remplacerLienAjaxPourBaseUrl = remplacerLienAjaxPourBaseUrl;
  }

  /**
   * Spécifie si vous désirez remplacer tous les lien ajax dans le fragemnt de page pour spécier l'url de base.
   * @return true si vous désirez remplacer tous les lien ajax dans le fragemnt de page pour spécier l'url de base.
   */
  public boolean isRemplacerLienAjaxPourBaseUrl() {
    return remplacerLienAjaxPourBaseUrl;
  }

  /**
   * Fixer l'url qui permet de faire de l'affichage de contenu dynamique pour
   * les liens qui sont spécifiés dans une zone de contenu dynamique.
   * @param urlAffichageContenuDynamique l'url qui permet de faire de l'affichage de contenu dynamique pour
   * les liens qui sont spécifiés dans une zone de contenu dynamique.
   */
  public void setUrlAffichageContenuDynamique(
      String urlAffichageContenuDynamique) {
    this.urlAffichageContenuDynamique = urlAffichageContenuDynamique;
  }

  /**
   * Retourne l'url qui permet de faire de l'affichage de contenu dynamique pour
   * les liens qui sont spécifiés dans une zone de contenu dynamique.
   * @return l'url qui permet de faire de l'affichage de contenu dynamique pour
   * les liens qui sont spécifiés dans une zone de contenu dynamique.
   */
  public String getUrlAffichageContenuDynamique() {
    return urlAffichageContenuDynamique;
  }

  /**
   * Retourne le nom de la cache pour le contenu.
   * @return le nom de la cache pour le contenu.
   * @since 3.0
   */
  public String getNomCache() {
    return nomCache;
  }

  /**
   * Fixer le nom de la cache pour le contenu.
   * @param nomCache le nom de la cache pour le contenu.
   * @since 3.0
   */
  public void setNomCache(String nomCache) {
    this.nomCache = nomCache;
  }

  /**
   * @param fragmentIndicateurDebut the fragmentIndicateurDebut to set
   */
  public void setFragmentIndicateurDebut(String fragmentIndicateurDebut) {
    this.fragmentIndicateurDebut = fragmentIndicateurDebut;
  }

  /**
   * @return the fragmentIndicateurDebut
   */
  public String getFragmentIndicateurDebut() {
    return fragmentIndicateurDebut;
  }

  /**
   * @param fragmentIndicateurFin the fragmentIndicateurFin to set
   */
  public void setFragmentIndicateurFin(String fragmentIndicateurFin) {
    this.fragmentIndicateurFin = fragmentIndicateurFin;
  }

  /**
   * @return the fragmentIndicateurFin
   */
  public String getFragmentIndicateurFin() {
    return fragmentIndicateurFin;
  }

  public String getVar() {
    return var;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public boolean isAllContent() {
    return allContent;
  }

  public void setAllContent(boolean allContent) {
    this.allContent = allContent;
  }

  public String getCacheExpiration() {
    return cacheExpiration;
  }

  public void setCacheExpiration(String cacheExpiration) {
    this.cacheExpiration = cacheExpiration;
  }
}
