/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.text.MessageFormat;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise qui affiche une version agrandie d'un fichier champ text ou textarea.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ZoomTexteTag extends javax.servlet.jsp.tagext.TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 8596682815322113386L;

  /**
   * Nom de l'id html du lien
   */
  private String idLien = null;

  /**
   * Nom du champ qui doit être associé au zoom.
   */
  private String nomChampTexte = null;

  /**
   * Largueur de la fenêtre (px)
   */
  private String largeurFenetre = null;

  /**
   * Hauteur de la fenêtre (px)
   */
  private String hauteurFenetre = null;

  /**
   * Position relative au champ texte ou doit apparaître la fenêtre (X)
   */
  private String positionX = null;

  /**
   * Position relative au champ texte ou doit apparaître la fenêtre (Y)
   */
  private String positionY = null;

  /**
   * Nombre de colonne du composant textarea (attribut html cols)
   */
  private String nombreColonne = null;

  /**
   * Nombre de lignes du composant textarea (attribut html rows)
   */
  private String nombreLigne = null;

  /**
   * Longueur Maximale en nombre de caractères qui peut être saisi
   * dans le champ textarea.
   */
  private String longueurMaximale = null;

  /**
   * Message (ou clé de message) qui doit être affiché par le composant zoom si
   * le texte saisi dépasse la longueur maximale permise.
   */
  private String messageTexteTropLong = null;

  /**
   * Libellé (ou clé de libellé) utilisé pour affiché le nombre de
   * caractères en temps réel saisi dans le champ.
   */
  private String libelleNombreCaractere = null;

  /**
   * Libellé (ou clé de libellé) du bouton qui modifie la valeur du champ texte.
   */
  private String libelleBoutonModifier = null;

  /**
   * Libellé (ou clé de libellé) du bouton qui annule la modification en cours.
   */
  private String libelleBoutonAnnuler = null;

  /**
   * Traitement JavaScript a appliquer lors de la soumission du texte.
   */
  private String traitementJSApresModification = null;

  /**
   * Spécifie le titre de la fenêtre lorsque traitement ajax.
   * @since SOFI 2.0.4
   */
  private String titreFenetreAjax;

  /**
   * Permet de spécifier du traitement JavaScript sur un évènement Onchange.
   * @since SOFI 2.1
   */
  private String traitementJSOnchange = null;

  /**
   * L'identifiant du DIV que doit être copier la valeur
   * du champ de saisie.
   * @since SOFI 2.1
   */
  private String idDivTexte = null;

  /**
   * Aide contextuelle sur le lien généré.
   * @since SOFI 2.1
   */
  private String aideContextuelle = null;

  /**
   * Constructeur
   */
  public ZoomTexteTag() {
    super();
    this.release();
  }

  /**
   * Début de la balise.
   */
  @Override
  public int doStartTag() throws JspException {
    int directive = EVAL_BODY_INCLUDE;

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      this.evaluerEL();
    }
    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    String nomFormulaireJS =
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            getNomChampTexte());

    StringBuffer resultatOnchange =
        UtilitaireBaliseJSP.genererJSFonctionModification(pageContext,
            nomFormulaire, nomFormulaireJS, getTraitementJSOnchange());

    if (getTraitementJSOnchange() != null) {
      setTraitementJSOnchange(getTraitementJSOnchange() +
          resultatOnchange.toString());
    } else {
      setTraitementJSOnchange(resultatOnchange.toString());
    }

    String nomChampComplet =
        UtilitaireBaliseNested.getIdentifiantUnique(nomChampTexte, pageContext);

    if (getIdDivTexte() != null) {
      String nomDivComplet =
          UtilitaireBaliseNested.getIdentifiantUnique(getIdDivTexte(),
              pageContext);

      String contenu =
          MessageFormat.format(";fixerValeurChampDansDiv({0},{1});",
              new Object[] { convertirParamJS(nomChampComplet),
              convertirParamJS(nomDivComplet) });

      if (getTraitementJSApresModification() != null) {
        setTraitementJSApresModification(UtilitaireString.convertirEnJavaScript(getTraitementJSApresModification()) +
            UtilitaireString.convertirEnJavaScript(contenu));
      } else {
        setTraitementJSApresModification(UtilitaireString.convertirEnJavaScript(contenu));
      }
    } else {
      setTraitementJSApresModification(UtilitaireString.convertirEnJavaScript(getTraitementJSApresModification()));
    }

    Libelle libelleAideContextuelle =
        UtilitaireLibelle.getLibelle(getAideContextuelle(), pageContext, null);

    StringBuffer resultat = new StringBuffer();

    String id = "";
    if (!UtilitaireString.isVide(getIdLien())){
      id = "id=\"" + getIdLien() + "\"";
    }

    resultat.append("<a " + id + " class = \"commentaireZoomTexte\" href=\"javascript:void(0);\"");

    if (!UtilitaireString.isVide(libelleAideContextuelle.getMessage())) {
      resultat.append(" title=\"");
      resultat.append(UtilitaireString.convertirEnHtml(libelleAideContextuelle.getMessage()));
      resultat.append("\"");
    }

    resultat.append(" onclick=\"creerZoomTexte({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, event, {12}, {13}, {14});\">");

    boolean lectureSeulement =
        UtilitaireBaliseJSP.isFormulaireEnLectureSeulement(pageContext, null);

    BaseForm formulaireCourant =
        UtilitaireBaliseNested.getFormulaireCourant(pageContext,
            nomChampComplet);

    if (!formulaireCourant.isModeAffichageSeulement() && !lectureSeulement) {
      String contenu =
          MessageFormat.format(resultat.toString(), new Object[] { "'" +
              nomChampComplet + "'", largeurFenetre, hauteurFenetre, positionX,
              positionY, nombreColonne, nombreLigne, longueurMaximale,
              this.getMessage(messageTexteTropLong),
              this.getLibelle(libelleNombreCaractere),
              this.getLibelle(libelleBoutonModifier),
              this.getLibelle(libelleBoutonAnnuler), getJSApresModification(),
              this.getLibelle(titreFenetreAjax), getJSOnChange() });

      TagUtils.getInstance().write(pageContext, contenu);
    } else {
      directive = SKIP_BODY;
    }

    return directive;
  }

  /**
   * Exécute la fin de la balise.
   */
  @Override
  public int doEndTag() throws JspException {
    String nomChampComplet =
        UtilitaireBaliseNested.getIdentifiantUnique(nomChampTexte, pageContext);
    BaseForm formulaire =
        UtilitaireBaliseNested.getFormulaireCourant(pageContext,
            nomChampComplet);

    if (!formulaire.isModeAffichageSeulement()) {
      TagUtils.getInstance().write(pageContext, "</a>");
    }

    this.release();
    return (EVAL_PAGE);
  }

  private String getMessage(String cleMessage) {
    return "'" +
        UtilitaireString.convertirEnJavaScript(UtilitaireMessage.get(cleMessage,
            null, pageContext).getMessage()) + "'";
  }

  private String getLibelle(String cleLibelle) {
    if (cleLibelle != null) {
      return "'" +
          UtilitaireString.convertirEnJavaScript(UtilitaireLibelle.getLibelle(cleLibelle,
              this.pageContext).getMessage()) + "'";
    } else {
      return "null";
    }
  }

  private String convertirParamJS(String nom) {
    if (nom != null) {
      return "'" + nom + "'";
    } else {
      return "null";
    }
  }

  private String getJSApresModification() {
    return "'" +
        UtilitaireString.convertirEnJavaScript(getTraitementJSApresModification()) +
        "'";
  }

  private String getJSOnChange() {
    return "'" +
        UtilitaireString.convertirEnJavaScript(getTraitementJSOnchange()) + "'";
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    this.nomChampTexte = null;

    this.largeurFenetre = "410";
    this.hauteurFenetre = "215";
    this.positionX = "0";
    this.positionY = "0";

    this.nombreColonne = "60";
    this.nombreLigne = "10";
    this.longueurMaximale = null;
    this.messageTexteTropLong = null;

    this.libelleNombreCaractere = null;
    this.libelleBoutonModifier = null;
    this.libelleBoutonAnnuler = null;
    this.traitementJSApresModification = null;
    this.titreFenetreAjax = null;
    this.traitementJSOnchange = null;
    this.idDivTexte = null;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  private void evaluerEL() throws JspException {
    String idLien = EvaluateurExpression.evaluerString("idLien", getIdLien(),
        this, pageContext);
    setIdLien(idLien);

    String traitementJSApresModification =
        EvaluateurExpression.evaluerString("traitementJSApresModification",
            getTraitementJSApresModification(), this, pageContext);
    setTraitementJSApresModification(traitementJSApresModification);

    String titreFenetreAjax =
        EvaluateurExpression.evaluerString("titreFenetreAjax",
            getTitreFenetreAjax(), this, pageContext);
    setTitreFenetreAjax(titreFenetreAjax);

    String traitementJSOnchange =
        EvaluateurExpression.evaluerString("traitementJSOnchange",
            getTraitementJSOnchange(), this, pageContext);
    setTraitementJSOnchange(traitementJSOnchange);
  }

  public void setNomChampTexte(String nomChampTexte) {
    this.nomChampTexte = nomChampTexte;
  }

  public String getNomChampTexte() {
    return nomChampTexte;
  }

  public void setLargeurFenetre(String largeurFenetre) {
    this.largeurFenetre = largeurFenetre;
  }

  public String getLargeurFenetre() {
    return largeurFenetre;
  }

  public void setHauteurFenetre(String hauteurFenetre) {
    this.hauteurFenetre = hauteurFenetre;
  }

  public String getHauteurFenetre() {
    return hauteurFenetre;
  }

  public void setPositionX(String positionX) {
    this.positionX = positionX;
  }

  public String getPositionX() {
    return positionX;
  }

  public void setPositionY(String positionY) {
    this.positionY = positionY;
  }

  public String getPositionY() {
    return positionY;
  }

  public void setNombreColonne(String nombreColonne) {
    this.nombreColonne = nombreColonne;
  }

  public String getNombreColonne() {
    return nombreColonne;
  }

  public void setNombreLigne(String nombreLigne) {
    this.nombreLigne = nombreLigne;
  }

  public String getNombreLigne() {
    return nombreLigne;
  }

  public void setLongueurMaximale(String longueurMaximale) {
    this.longueurMaximale = longueurMaximale;
  }

  public String getLongueurMaximale() {
    return longueurMaximale;
  }

  public void setMessageTexteTropLong(String messageTexteTropLong) {
    this.messageTexteTropLong = messageTexteTropLong;
  }

  public String getMessageTexteTropLong() {
    return messageTexteTropLong;
  }

  public void setLibelleNombreCaractere(String libelleNombreCaractere) {
    this.libelleNombreCaractere = libelleNombreCaractere;
  }

  public String getLibelleNombreCaractere() {
    return libelleNombreCaractere;
  }

  public void setLibelleBoutonModifier(String libelleBoutonModifier) {
    this.libelleBoutonModifier = libelleBoutonModifier;
  }

  public String getLibelleBoutonModifier() {
    return libelleBoutonModifier;
  }

  public void setLibelleBoutonAnnuler(String libelleBoutonAnnuler) {
    this.libelleBoutonAnnuler = libelleBoutonAnnuler;
  }

  public String getLibelleBoutonAnnuler() {
    return libelleBoutonAnnuler;
  }

  public void setTraitementJSApresModification(String traitementJSApresModification) {
    this.traitementJSApresModification = traitementJSApresModification;
  }

  public String getTraitementJSApresModification() {
    return traitementJSApresModification;
  }

  public void setTitreFenetreAjax(String titreFenetreAjax) {
    this.titreFenetreAjax = titreFenetreAjax;
  }

  public String getTitreFenetreAjax() {
    return titreFenetreAjax;
  }

  public void setTraitementJSOnchange(String traitementJSOnchange) {
    this.traitementJSOnchange = traitementJSOnchange;
  }

  public String getTraitementJSOnchange() {
    return traitementJSOnchange;
  }

  /**
   * Fixer l'identifiant du DIV que doit être copier la valeur
   * du champ de saisie.
   * @since SOFI 2.1
   * @param idDivTexte l'identifiant du DIV que doit être copier la valeur
   * du champ de saisie.
   */
  public void setIdDivTexte(String idDivTexte) {
    this.idDivTexte = idDivTexte;
  }

  /**
   * Retourne l'identifiant du DIV que doit être copier la valeur
   * du champ de saisie.
   * @since SOFI 2.1
   * @return l'identifiant du DIV que doit être copier la valeur
   * du champ de saisie.
   */
  public String getIdDivTexte() {
    return idDivTexte;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setIdLien(String idLien) {
    this.idLien = idLien;
  }

  public String getIdLien() {
    return idLien;
  }
}
