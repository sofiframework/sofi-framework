/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise permettant de générer les importations de fichier JavaScript.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.8
 */
public class JavaScriptTag
extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 5491252551874802199L;
  // La source du fichier JavaScript.
  private String src = null;
  private boolean appliquerOnLoad = false;
  private String onLoad = null;

  /** Constructeur par défaut */
  public JavaScriptTag() {
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag()
      throws JspException {
    evaluerEL();

    StringBuffer resultat = new StringBuffer();

    Boolean initialisationPageChargement =
        (Boolean)pageContext.getRequest().getAttribute("initialisationPageChargement");

    if (initialisationPageChargement == null) {
      // L'attribut TYPE de la balise SCRIPT est obligatoire.
      // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
      resultat.append("\n<script type=\"text/javascript\">");
      resultat.append("var sofiChargementPageComplete = 'false';");
      resultat.append("</script>\n");
      pageContext.getRequest().setAttribute("initialisationPageChargement",
          Boolean.TRUE);
    }

    if (getSrc() != null) {
      resultat.append("<script type=\"text/javascript\" src=\"");
      resultat.append(getSrc());

      String dateDeploiement =
          GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.DATE_DEPLOIEMENT);

      if (!UtilitaireString.isVide(dateDeploiement)) {
        resultat.append("?dateDeploiement=");
        resultat.append(dateDeploiement);
      }

      resultat.append("\">");
    }

    if (getOnLoad() != null) {
      StringBuffer fonctionOnloadPersonnalise =
          (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE);

      if (fonctionOnloadPersonnalise == null) {
        fonctionOnloadPersonnalise = new StringBuffer();
        fonctionOnloadPersonnalise.append(getOnLoad());
        pageContext.getRequest().setAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE,
            fonctionOnloadPersonnalise);
      }
      else {
        fonctionOnloadPersonnalise.append(getOnLoad());
        pageContext.getRequest().setAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE,
            fonctionOnloadPersonnalise);
      }
    }

    if (isAppliquerOnLoad()) {
      resultat.append("\n<script>\n");

      StringBuffer scriptsDiv =
          (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_JS_ACCUMULE_BAS_PAGE);

      if (scriptsDiv != null) {
        resultat.append(scriptsDiv);
      }


    }

    boolean fonctionOnloadATraiter = false;

    if (isAppliquerOnLoad() &&
        (pageContext.getRequest().getAttribute(Constantes.FORMULAIRE_EN_COURS) ==
        null)) {
      resultat.append("window.onload= function(){");

      resultat.append(UtilitaireBaliseJSP.getFonctionJSOnLoad());

      if (pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE) !=
          null) {
        StringBuffer fonctionOnloadPersonnalise =
            (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE);
        resultat.append(fonctionOnloadPersonnalise.toString());
      }

      resultat.append("}");
      fonctionOnloadATraiter = true;
    }

    if ((getSrc() != null) || (isAppliquerOnLoad())) {
      if (isAppliquerOnLoad()) {
        BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

        if (formulaire != null) {
          formulaire.initialiserLesMessages();
        }

        if (fonctionOnloadATraiter) {
          resultat.append("\n");
        }

        resultat.append("sofiChargementPageComplete = 'true';");
      }

      resultat.append("\n</script>\n");
    }

    // Écrire la réponse au client.
    TagUtils.getInstance().write(pageContext, resultat.toString());

    return Tag.EVAL_PAGE;
  }

  @Override
  public int doEndTag()
      throws JspException {
    return super.doEndTag();
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL()
      throws JspException {
    String valeur =
        EvaluateurExpression.evaluerString("src", getSrc(), this, pageContext);
    setSrc(valeur);

    String onLoad =
        EvaluateurExpression.evaluerString("onLoad", getOnLoad(), this,
            pageContext);
    setOnLoad(onLoad);
  }

  public void setSrc(String src) {
    this.src = src;
  }

  public String getSrc() {
    return src;
  }

  public void setAppliquerOnLoad(boolean appliquerOnLoad) {
    this.appliquerOnLoad = appliquerOnLoad;
  }

  public boolean isAppliquerOnLoad() {
    return appliquerOnLoad;
  }

  public void setOnLoad(String onLoad) {
    this.onLoad = onLoad;
  }

  public String getOnLoad() {
    return onLoad;
  }
}
