/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * PopupFieldTag permet d'afficher un fenetre flottante tout en fesant un appel
 * a une action spécifique ou une page JSP ou HTML.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.0 Support Ajax.
 */
public class PopupTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -6850298420468580175L;

  /**
   * Lien hypertexte URI de la fenetre de la liste de valeurs.
   */
  private String href = null;

  /**
   * Spécifie si la barre déroulante est affiche ou pas.
   */
  private String scrollbars = null;

  /**
   * Spécifie la largeur de la fenêtre flottante.
   */
  private String largeurfenetre = null;

  /**
   * Spécifie la hauteur de la fenêtre flottante.
   */
  private String hauteurfenetre = null;

  /**
   * Spécifie le nom de la fenêtre flottante.
   */
  private String nomfenetre = null;

  /**
   * Spécifie le nom du bean qui contient les paramètres.
   */
  private String name = null;

  /**
   * Spécifie le nom de l'action a appeler avec l'url.
   */
  private String action = null;

  /**
   * Spoécifie le nom de paramètre a générer dans l'url.
   */
  private String paramId = null;

  /**
   * Spécifie le nom du paramètre.
   */
  private String paramName = null;

  /**
   * Propriété du bean.
   */
  private String paramProperty = null;

  /**
   * Scope du paramètre.
   */
  private String paramScope = null;

  /**
   * La propriété du bean à utiliser.
   */
  private String property = null;

  /**
   * The scope of the bean specified by the name property, if any.
   */
  private String scope = null;

  /**
   * Spécifie la classe du stylesheet .css.
   */
  private String styleClass = null;

  /**
   * Le nom de la valeur qui est insérer la déclaration de l'appel du popup.
   */
  private String var = null;

  /**
   * Indique si l'adresse de la fenêtre appel une application sur le même domaine.
   */
  private boolean adresseSurMemeDomaine;

  /**
   * Indique si on désire la barre d'outil.
   */
  private boolean barreOutil;

  /**
   * Indique si on désire la barre de menu.
   */
  private boolean barreMenu;

  /**
   * Indique si on désire la barre d'adresse.
   */
  private boolean barreAdresse;

  /**
   * Indique si on désire maximiser la fenêtre.
   */
  private boolean maximiser;

  /**
   * Indique que le fenêtre flottante doit être en mode ajax.
   */
  private boolean ajax;

  /**
   * Ajustement de la position Y de la fenêtre flottante
   * en mode ajax.
   */
  private String ajustementPositionX;

  /**
   * Ajustement de la position Y de la fenêtre flottante
   * en mode ajax.
   */
  private String ajustementPositionY;

  /**
   * Spécifie l'identifiant de la fenêtre.
   */
  private String styleId;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  private String nomParametreValeur = null;

  /**
   * L'id de la propriété dont on désire la valeur.
   */
  private String idProprieteValeur = null;

  /**
   * Traitement ajax a appliquer après le chargement
   * du popup ajax.
   */
  private String ajaxJSApresChargement;

  /**
   * Spécifie le titre de la fenêtre lorsque traitement ajax.
   * @since SOFI 2.1
   */
  private String titreFenetreAjax;

  /**
   * Spécifie l'aide contextuelle sur le lien de l'appel de la fenête.
   * @since SOFI 2.1
   */
  private String aideContextuelle;

  /**
   * Constructeur par défault.
   */
  public PopupTag() {
  }

  /**
   * Obtenir le lien hypertexte URI de la fenetre de la liste de valeurs.
   * @return le lien hypertexte URI de la fenetre de la liste de valeurs
   */
  public String getHref() {
    return (this.href);
  }

  /**
   * Fixer le lien hypertexte URI de la fenetre de la liste de valeurs.
   * @param href le lien hypertexte URI de la fenetre de la liste de valeurs
   */
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Obtenir si la barre déroulante est affiche ou pas.
   * @return si la barre déroulante est affiche ou pas
   */
  public String getScrollbars() {
    return (this.scrollbars);
  }

  /**
   * Fixer si la barre déroulante est affiche ou pas.
   * @param scrollbars si la barre déroulante est affiche ou pas
   */
  public void setScrollbars(String scrollbars) {
    this.scrollbars = scrollbars;
  }

  /**
   * Obtenir la largeur de la fenêtre flottante.
   * @return la largeur de la fenêtre flottante
   */
  public String getLargeurfenetre() {
    return (this.largeurfenetre);
  }

  /**
   * Fixer la largeur de la fenêtre flottante.
   * @param largeurfenetre la largeur de la fenêtre flottante
   */
  public void setLargeurfenetre(String largeurfenetre) {
    this.largeurfenetre = largeurfenetre;
  }

  /**
   * Obtenir la hauteur de la fenêtre flottante.
   * @return la hauteur de la fenêtre flottante
   */
  public String getHauteurfenetre() {
    return (this.hauteurfenetre);
  }

  /**
   * Fixer la hauteur de la fenêtre flottante.
   * @param hauteurfenetre la hauteur de la fenêtre flottante
   */
  public void setHauteurfenetre(String hauteurfenetre) {
    this.hauteurfenetre = hauteurfenetre;
  }

  /**
   * Obtenir la hauteur de la fenêtre flottante.
   * @return la hauteur de la fenêtre flottante
   */
  public String getNomfenetre() {
    return (this.nomfenetre);
  }

  /**
   * Fixer la hauteur de la fenêtre flottante.
   * @param nomfenetre la hauteur de la fenêtre flottante
   */
  public void setNomfenetre(String nomfenetre) {
    this.nomfenetre = nomfenetre;
  }

  /**
   * Obtenir le nom du bean qui contient les paramètres.
   * @return le nom du bean qui contient les paramètres
   */
  public String getName() {
    return (this.name);
  }

  /**
   * Fixer le nom du bean qui contient les paramètres.
   * @param name le nom du bean qui contient les paramètres
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Obtenir le nom de l'action a appeler avec l'url.
   * @return le nom de l'action a appeler avec l'url
   */
  public String getAction() {
    return (this.action);
  }

  /**
   * Fixer le nom de l'action a appeler avec l'url.
   * @param action le nom de l'action a appeler avec l'url
   */
  public void setAction(String action) {
    this.action = action;
  }

  /**
   * Obtenir le nom de paramètre a générer dans l'url.
   * @return le nom de paramètre a générer dans l'url
   */
  public String getParamId() {
    return (this.paramId);
  }

  /**
   * Fixer le nom de paramètre a générer dans l'url.
   * @param paramId le nom de paramètre a générer dans l'url
   */
  public void setParamId(String paramId) {
    this.paramId = paramId;
  }

  /**
   * Obtenir le nom du paramètre.
   * @return le nom du paramètre
   */
  public String getParamName() {
    return (this.paramName);
  }

  /**
   * Fixer le nom du paramètre.
   * @param paramName le nom du paramètre
   */
  public void setParamName(String paramName) {
    this.paramName = paramName;
  }

  /**
   * Obtenir la propriété du bean.
   * @return la propriété du bean
   */
  public String getParamProperty() {
    return (this.paramProperty);
  }

  /**
   * Fixer la propriété du bean.
   * @param paramProperty la propriété du bean
   */
  public void setParamProperty(String paramProperty) {
    this.paramProperty = paramProperty;
  }

  /**
   * Obtenir paramètre le scope du paramètre.
   * @return le paramètre scope du paramètre
   */
  public String getParamScope() {
    return (this.paramScope);
  }

  /**
   * Fixer le paramètre scope du paramètre.
   * @param paramScope le paramètre scope du paramètre
   */
  public void setParamScope(String paramScope) {
    this.paramScope = paramScope;
  }

  /**
   * Obtenir la propriété du bean à utiliser.
   * @return la propriété du bean à utiliser
   */
  public String getProperty() {
    return (this.property);
  }

  /**
   * Fixer la propriété du bean à utiliser.
   * @param property la propriété du bean à utiliser
   */
  public void setProperty(String property) {
    this.property = property;
  }

  /**
   * Obtenir le scope.
   * @return le scope
   */
  public String getScope() {
    return (this.scope);
  }

  /**
   * Fixer le scope.
   * @param scope le scope
   */
  public void setScope(String scope) {
    this.scope = scope;
  }

  /**
   * Fixer le style css a appliquer au lien.
   * @param styleClass le style css a appliquer au lien
   */
  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  /**
   * Obtenir le style css a appliquer au lien.
   * @return le style css a appliquer au lien
   */
  public String getStyleClass() {
    return styleClass;
  }

  /**
   * Obtenir le nom de la valeur qui est insérer la déclaration de l'appel du popup.
   * @return le nom de la valeur qui est insérer la déclaration de l'appel du popup
   */
  public String getVar() {
    return (this.var);
  }

  /**
   * Fixer le nom de la valeur qui est insérer la déclaration de l'appel du popup.
   * @param var le nom de la valeur qui est insérer la déclaration de l'appel du popup
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne true si l'adresse de la fenêtre est sur le même domaine
   * que l'application qui génère la lien de la fenêtre flottante.
   * @return true si l'adresse de la fenêtre est sur le même domaine
   */
  public boolean isAdresseSurMemeDomaine() {
    return adresseSurMemeDomaine;
  }

  /**
   * Fixer true si l'adresse de la fenêtre est sur le même domaine
   * que l'application qui génère la lien de la fenêtre flottante.
   * @param adresseSurMemeDomaine si l'adresse de la fenêtre est sur le même domaine
   */
  public void setAdresseSurMemeDomaine(boolean adresseSurMemeDomaine) {
    this.adresseSurMemeDomaine = adresseSurMemeDomaine;
  }

  /**
   * Execute le début de la balise. Appel son parent.
   * @exception JspException si un exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    setAjaxJSApresChargement(UtilitaireString.convertirEnJavaScript(getAjaxJSApresChargement()));

    if (isAjax()) {
      Href nouveauHref = new Href(getHref());
      href = nouveauHref.getUrlPourRedirection();

      if (getAjustementPositionX() == null) {
        setAjustementPositionX("0");
        setAjustementPositionY("0");
      }
    }

    StringBuffer buffer = new StringBuffer();

    if (getVar() == null) {
      buffer.append(genererDebutLienAppelPopup());
    }

    if (!isAjax()) {
      buffer.append(genererFonctionPopup());
    } else {
      buffer.append(genererFonctionPopupAjax());
    }

    if (getVar() == null) {
      buffer.append("\"");
      buffer.append(">");
    }

    if (getVar() != null) {
      this.pageContext.setAttribute(getVar(), buffer);
    } else {
      // Print this field to our output writer
      JspWriter writer = this.pageContext.getOut();

      try {
        writer.print(buffer.toString());
      } catch (IOException e) {
        throw new JspException();
      }
    }

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Fin de la balise.
   * @exception JspException lancé si une erreur JSP est lancé.
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer results = new StringBuffer();

    if (getVar() == null) {
      results.append("</a>");
    }

    // Print this field to our output writer
    JspWriter writer = pageContext.getOut();

    try {
      writer.print(results.toString());
    } catch (IOException e) {
      throw new JspException(e.toString());
    }

    return (EVAL_PAGE);
  }

  /**
   *
   * @return
   */
  protected String genererDebutLienAppelPopup() {
    StringBuffer buffer = new StringBuffer();

    buffer.append("<a ");

    if (getStyleClass() != null) {
      buffer.append("class=\"");
      buffer.append(getStyleClass());
      buffer.append("\"");
    }

    if (getStyleId() != null) {
      buffer.append(" id=\"");
      buffer.append(getStyleId());
      buffer.append("\"");
    }

    if (this.getAideContextuelle() != null) {
      Libelle libelleAide =
          UtilitaireLibelle.getLibelle(this.getAideContextuelle(), pageContext);
      buffer.append(" title=\"");
      buffer.append(libelleAide.getMessage());
      buffer.append("\"");
    }

    buffer.append(" href=\"javascript:void(0)\" onClick=\"javascript:");

    return buffer.toString();
  }

  protected String genererFonctionPopup() throws JspException {
    StringBuffer popupBuffer = new StringBuffer();

    if (!isMaximiser()) {
      popupBuffer.append("popup('");
    } else {
      popupBuffer.append("popupMaximise('");
    }

    String href = getHref().trim();

    String url = null;
    String forward = null;
    String page = null;
    String action = null;
    String anchor = null;

    // Identify the parameters we will add to the completed URL
    Map params =
        TagUtils.getInstance().computeParameters(pageContext, paramId, paramName,
            paramProperty, paramScope, name, property, scope, false);

    try {
      url =
          TagUtils.getInstance().computeURL(pageContext, forward, href, page, action,
              null, params, anchor, false);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(pageContext, e);
      throw new JspException(e.toString());
    }

    if (isAdresseSurMemeDomaine()) {
      popupBuffer.append("../");
    }

    popupBuffer.append(url);

    popupBuffer.append("','");

    String nomFenetre = "popup";

    if (getNomfenetre() != null) {
      nomFenetre = getNomfenetre();
    }

    popupBuffer.append(nomFenetre);

    if (!isMaximiser()) {
      popupBuffer.append("','");
      popupBuffer.append(getLargeurfenetre());
      popupBuffer.append("','");
      popupBuffer.append(getHauteurfenetre());
    }

    popupBuffer.append("','");
    popupBuffer.append("status=1,resizable=1");

    if (isBarreOutil()) {
      popupBuffer.append(",toolbar=1");
    }

    if (isBarreMenu()) {
      popupBuffer.append(",menubar=1");
    }

    if (isBarreAdresse()) {
      popupBuffer.append(",location=1");
    }

    popupBuffer.append(",scrollbars=");

    if ((this.scrollbars == null) ||
        this.scrollbars.trim().toLowerCase().equals("true")) {
      popupBuffer.append("1");
    } else {
      popupBuffer.append("0");
    }

    popupBuffer.append("')");
    popupBuffer.append(";");

    return popupBuffer.toString();
  }

  protected String genererFonctionPopupAjax() throws JspException {
    StringBuffer popupBuffer = new StringBuffer();

    popupBuffer.append(UtilitaireAjax.getLienAjax("lienAjax", getHref(),
        getNomParametreValeur(), getIdProprieteValeur(), pageContext));

    popupBuffer.append("genererFenetreFlottanteAJAX('");

    String nomFenetre = "popup";

    if (getNomfenetre() != null) {
      nomFenetre = getNomfenetre();
    }

    popupBuffer.append(nomFenetre);
    popupBuffer.append("','");

    popupBuffer.append(getStyleId());
    popupBuffer.append("',");

    String href = getHref().trim();

    String url = null;
    String forward = null;
    String page = null;
    String action = null;
    String anchor = null;

    // Identify the parameters we will add to the completed URL
    Map params =
        TagUtils.getInstance().computeParameters(pageContext, paramId, paramName,
            paramProperty, paramScope, name, property, scope, false);

    try {
      url =
          TagUtils.getInstance().computeURL(pageContext, forward, href, page, action,
              null, params, anchor, false);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(pageContext, e);
      throw new JspException(e.toString());
    }

    if (isAdresseSurMemeDomaine() && !ajax) {
      popupBuffer.append("../");
    }

    popupBuffer.append("lienAjax");

    popupBuffer.append(",'");
    popupBuffer.append(getLargeurfenetre());
    popupBuffer.append("','");
    popupBuffer.append(getHauteurfenetre());
    popupBuffer.append("','");
    popupBuffer.append(getAjustementPositionX());
    popupBuffer.append("','");
    popupBuffer.append(getAjustementPositionY());

    popupBuffer.append("',event");

    if (getAjaxJSApresChargement() != null || getTitreFenetreAjax() != null) {
      popupBuffer.append(",'");
      if (getTitreFenetreAjax() != null) {
        Libelle libelle =
            UtilitaireLibelle.getLibelle(getTitreFenetreAjax(), pageContext,
                null);

        String fonctionTitre =
            "setTitreFenetre('" + UtilitaireString.convertirEnJavaScript(libelle.getMessage()) +
            "');";
        popupBuffer.append(UtilitaireString.convertirEnJavaScript(fonctionTitre));
      }
      if (getAjaxJSApresChargement() != null) {
        popupBuffer.append(getAjaxJSApresChargement());
      }
      popupBuffer.append("'");
    }

    popupBuffer.append(");");

    return popupBuffer.toString();
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      String valeurELHref =
          EvaluateurExpression.evaluerString("href", getHref(), this,
              pageContext);

      setHref(valeurELHref);

      String nomFenetre =
          EvaluateurExpression.evaluerString("nomfenetre", getNomfenetre(), this,
              pageContext);
      setNomfenetre(nomFenetre);

      String styleClass =
          EvaluateurExpression.evaluerString("styleClass", getStyleClass(), this,
              pageContext);
      setStyleClass(styleClass);

      String ajaxJSApresChargement =
          EvaluateurExpression.evaluerString("ajaxJSApresChargement",
              getAjaxJSApresChargement(), this, pageContext);
      setAjaxJSApresChargement(ajaxJSApresChargement);

      String titreFenetreAjax =
          EvaluateurExpression.evaluerString("titreFenetreAjax",
              getTitreFenetreAjax(), this, pageContext);
      setTitreFenetreAjax(titreFenetreAjax);
    } catch (JspException e) {
      throw e;
    }
  }

  public boolean isBarreOutil() {
    return barreOutil;
  }

  public void setBarreOutil(boolean barreOutil) {
    this.barreOutil = barreOutil;
  }

  public boolean isBarreMenu() {
    return barreMenu;
  }

  public void setBarreMenu(boolean barreMenu) {
    this.barreMenu = barreMenu;
  }

  public boolean isBarreAdresse() {
    return barreAdresse;
  }

  public void setBarreAdresse(boolean barreAdresse) {
    this.barreAdresse = barreAdresse;
  }

  public boolean isMaximiser() {
    return maximiser;
  }

  public void setMaximiser(boolean maximiser) {
    this.maximiser = maximiser;
  }

  /**
   * Fixer true si on désire la fonctionnalité ajax.
   * @param ajax true si on désire la fonctionnalité ajax.
   */
  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  /**
   * Est-ce qu'on désire la fonctinnalité ajax du lien.
   * @return true si désire la fonctinnalité ajax du lien.
   */
  public boolean isAjax() {
    return ajax;
  }

  public void setAjustementPositionX(String ajustementPositionX) {
    this.ajustementPositionX = ajustementPositionX;
  }

  public String getAjustementPositionX() {
    return ajustementPositionX;
  }

  public void setAjustementPositionY(String ajustementPositionY) {
    this.ajustementPositionY = ajustementPositionY;
  }

  public String getAjustementPositionY() {
    return ajustementPositionY;
  }

  public void setStyleId(String styleId) {
    this.styleId = styleId;
  }

  public String getStyleId() {
    return styleId;
  }

  /**
   * Fixer true si le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @param nomParametreValeur
   */
  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  /**
   * Retourne le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @return le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   */
  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  /**
   * Fixer le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   * <p>
   * Il est possible de spécifier plusieurs propriété correspondant
   * au paramètre spécifié en séparant les propriétés de virgule.
   * @param idProprieteValeur le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   */
  public void setIdProprieteValeur(String idProprieteValeur) {
    this.idProprieteValeur = idProprieteValeur;
  }

  /**
   * Retourne le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @return le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   */
  public String getIdProprieteValeur() {
    return idProprieteValeur;
  }

  /**
   * Fixer le traitement JavaScript a executé après le chargement.
   * @param ajaxJSApresChargement le traitement JavaScript a executé après le chargement.
   */
  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé après le chargement.
   * @return le traitement JavaScript a executé après le chargement.
   */
  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  /**
   * Fixer le titre de la fenêtre ajax.
   * @param titreFenetreAjax le titre de la fenêtre ajax.
   * @since SOFI 2.1
   */
  public void setTitreFenetreAjax(String titreFenetreAjax) {
    this.titreFenetreAjax = titreFenetreAjax;
  }

  /**
   * Retourne le titre de la fenêtre ajax.
   * @return le titre de la fenêtre ajax.
   * @since SOFI 2.1
   */
  public String getTitreFenetreAjax() {
    return titreFenetreAjax;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }
}
