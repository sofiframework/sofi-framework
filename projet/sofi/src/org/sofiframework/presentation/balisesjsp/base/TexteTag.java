/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise Jsp qui permet de traiter du texte tel que la conversion des
 * retour de chariot en changement de ligne HTML.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 2.0
 */
public class TexteTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -7072997602091710032L;

  /**
   * La valeur du texte a traiter.
   */
  private String valeur;

  /**
   * Permet de convertir les retours de chariot en saut de ligne HTML.
   */
  private boolean convertirRetourChariotEnSautLigneHTML;

  /**
   * Permet de spécifier un ombre de carateres maximal qui est admissibles
   * avant de spécifier un retour de chariot HTML.
   */
  private String sautLigneNombreCararactereMaximal;

  /**
   * Permet de spécifier le caractère séparateur avant d'appliquer un saut de ligne
   * HTML.
   */
  private String sautLigneCararactereSeparateur;

  /**
   * Le nom de variable temporaire qui loge le texte.
   */
  private String var;

  /**
   * Permet de convertir le texte en format HTML.
   */
  private boolean convertirEnHtml;

  /**
   * Constructeur par défault.
   */
  public TexteTag() {
  }

  /**

  /**
   * Exécution de l'ouverture de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    String texte = valeur.toString();

    if (valeur != null) {
      if (isConvertirRetourChariotEnSautLigneHTML()) {
        texte = UtilitaireString.modifierRetourChariotEnChangementLigneWeb(valeur.toString());
      }

      // Traitement de la chaine de caractère si trop longue selon la spécification
      // de la colonne.
      if (getSautLigneNombreCararactereMaximal() != null) {
        texte = UtilitaireString.traiterChaineCaractereTropLongueAvecSautLigneHTML(texte,
            new Integer(getSautLigneNombreCararactereMaximal()),
            getSautLigneCararactereSeparateur());
      }

      if (getVar() == null) {
        if (isConvertirEnHtml()) {
          texte = UtilitaireString.convertirEnHtml(texte);
        }

        TagUtils.getInstance().write(pageContext, texte);
      } else {
        this.pageContext.setAttribute(getVar(), texte);
      }
    }

    return (SKIP_BODY);
  }

  /**
   * Exécution de la fin de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    return (EVAL_PAGE);
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String valeur = EvaluateurExpression.evaluerString("valeur", getValeur(),
        this, pageContext);
    setValeur(valeur);
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getValeur() {
    return valeur;
  }

  public void setConvertirRetourChariotEnSautLigneHTML(
      boolean convertirRetourChariotEnSautLigneHTML) {
    this.convertirRetourChariotEnSautLigneHTML = convertirRetourChariotEnSautLigneHTML;
  }

  public boolean isConvertirRetourChariotEnSautLigneHTML() {
    return convertirRetourChariotEnSautLigneHTML;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getVar() {
    return var;
  }

  public void setSautLigneNombreCararactereMaximal(
      String sautLigneNombreCararactereMaximal) {
    this.sautLigneNombreCararactereMaximal = sautLigneNombreCararactereMaximal;
  }

  public String getSautLigneNombreCararactereMaximal() {
    return sautLigneNombreCararactereMaximal;
  }

  public void setSautLigneCararactereSeparateur(
      String sautLigneCararactereSeparateur) {
    this.sautLigneCararactereSeparateur = sautLigneCararactereSeparateur;
  }

  public String getSautLigneCararactereSeparateur() {
    return sautLigneCararactereSeparateur;
  }

  public void setConvertirEnHtml(boolean convertirEnHtml) {
    this.convertirEnHtml = convertirEnHtml;
  }

  public boolean isConvertirEnHtml() {
    return convertirEnHtml;
  }
}
