/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;


/**
 * Tag qui pemet de fermer automatiuement une fenêtre enfant si on ferme le parent.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class FermeturePopupTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 8113451454618957038L;
  /**
   * Délais de la fermeture.
   */
  private String timeout;

  /** Constructeur par défaut */
  public FermeturePopupTag() {
  }

  /**
   * Execute le début de la balise. Appel son parent.
   * <p>
   * @exception JspException si un exception JSP est lancé.
   */
  @Override
  public int doStartTag() throws JspException {
    StringBuffer js = new StringBuffer();
    // SP - Non conforme au W3C sans l'attribut type="..."
    //js.append("\n<script>");
    js.append("\n<script type=\"text/javascript\">");
    js.append("\n   function checkLocation() {");
    js.append("\n      try {");
    js.append("\n         window.opener.location.href;");
    js.append("\n      } catch(ex) {");
    js.append("\n         window.close();");
    js.append("\n      }");
    js.append("\n      setTimeout('checkLocation()', ");
    js.append(timeout);
    js.append(");");
    js.append("\n   }");
    js.append("\n   checkLocation();");
    js.append("\n</script>\n");

    // Print this field to our output writer
    JspWriter writer = this.pageContext.getOut();

    try {
      writer.print(js.toString());
    } catch (IOException e) {
      throw new JspException();
    }

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Execute la fin de la balise.
   * @exception JspException si un exception JSP est lancé.
   */
  @Override
  public int doEndTag() throws JspException {
    return (EVAL_PAGE);
  }

  /**
   * Libération de  l'objet pour une exécution subséquante.
   */
  @Override
  public void release() {
  }

  /**
   * Obtenir le délais de la fermeture.
   * @return le délais de la fermeture
   */
  public String getTimeout() {
    return timeout;
  }

  /**
   * Fixer le délais de la fermeture.
   * @param newTimeout le délais de la fermeture
   */
  public void setTimeout(String newTimeout) {
    timeout = newTimeout;
  }

  /**
   * Fixer le délais de la fermeture.
   * @param expiration le délais de la fermeture
   */
  public void setExpiration(String expiration) {
    timeout = expiration;
  }
}
