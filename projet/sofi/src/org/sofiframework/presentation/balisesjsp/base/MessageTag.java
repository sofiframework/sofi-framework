/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;



/**
 * Tag permettant d'afficher des message provenant d'une source de message
 * en spécifiant la langue et l'application.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class MessageTag extends BaseMessageTag {
  /**
   * 
   */
  private static final long serialVersionUID = -7618661456522284615L;

  /**
   * Obtenir la valeur du paramtre sans évaluation supplémentaire.
   * <p>
   * @return la valeur du paramtre sans évaluation supplémentaire
   */
  public String getValeurParametre(String parametreName, String parametre) {
    return parametre;
  }
}
