/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.constante.Constantes;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise permet d'afficher une message automatiquement dans un popup javascript,
 * tout en spécifiant le champ en erreur avec un focus sur le champ.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class MessagePopupTag extends BaseMessageTag {
  /**
   * 
   */
  private static final long serialVersionUID = -3623428332139036753L;

  /**
   * Constructeur par défault.
   */
  public MessagePopupTag() {
  }

  /**
   * Execute le début de la balise.
   * @exception JspException si un exception JSP est lancé.
   */
  @Override
  public int doStartTag() throws JspException {
    super.doStartTag();
    StringBuffer results = new StringBuffer();
    if (getIdentifiant() == null) {

      Message message =
          (Message)pageContext.getRequest().getAttribute(Constantes.PREMIER_MESSAGE_ERREUR);

      if (message != null) {
        results.append("<script type=\"text/javascript\">");

        if (message.getMessage() != null) {
          results.append("</script><script type=\"text/javascript\">function verifierErreur() {");
          results.append("alert(\"");
          results.append(message.getMessage());
          results.append("\");}");
          results.append("verifierErreur();");
        }
      }

    } else {
      results.append("alert('");
      results.append(UtilitaireString.convertirEnJavaScript(message.getMessage()));
      results.append("');");
    }
    if (getVar() == null) {
      // Print this field to our output writer
      JspWriter writer = pageContext.getOut();

      try {
        writer.print(results.toString());
      } catch (IOException e) {
        throw new JspException(e.toString());
      }
    } else {
      this.pageContext.getRequest().setAttribute(getVar(), results);
    }
    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Traitement de la fin de la balise.
   * @exception JspException si une exception JSP est traité.
   */
  @Override
  public int doEndTag() throws JspException {
    if (getIdentifiant() == null) {

      StringBuffer results = new StringBuffer();
      results.append("</script>");

      // Écrire le contenu
      JspWriter writer = pageContext.getOut();

      try {
        writer.print(results.toString());
      } catch (IOException e) {
        throw new JspException(e.toString());
      }
    }

    return (EVAL_PAGE);
  }

  /**
   * Libérationdes resources acquises.
   */
  @Override
  public void release() {
    super.release();
  }
}
