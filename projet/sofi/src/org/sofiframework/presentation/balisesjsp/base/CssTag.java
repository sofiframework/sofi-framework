/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise permettant de générer les importations de fichier CSS.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.8
 */
public class CssTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -3550576246235652867L;

  // La source du fichier CSS.
  private String href = null;

  // Le media du fichier CSS
  private String media = null;

  /** Constructeur par défaut */
  public CssTag() {
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    StringBuffer resultat = new StringBuffer();

    resultat.append("<link href=\"");
    resultat.append(getHref());

    resultat.append(UtilitaireBaliseJSP.getParametreUrlDateDeploiement(true));

    resultat.append("\" rel=\"stylesheet\" type=\"text/css\"");

    if (!UtilitaireString.isVide(getMedia())) {
      resultat.append(" media=\"" + getMedia() + "\"");
    }

    resultat.append(">");

    // Écrire la réponse au client.
    TagUtils.getInstance().write(pageContext, resultat.toString());

    return Tag.EVAL_PAGE;
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String valeur = EvaluateurExpression.evaluerString("href", getHref(), this,
        pageContext);
    setHref(valeur);

    valeur = EvaluateurExpression.evaluerString("media", getMedia(), this,
        pageContext);
    setMedia(valeur);
  }

  @Override
  public int doEndTag() throws JspException {
    return super.doEndTag();
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getHref() {
    return href;
  }

  public String getMedia() {
    return media;
  }

  public void setMedia(String media) {
    this.media = media;
  }
}
