/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;

/**
 * Balise permet d'affiche un propriété d'une valeur d'un objet cache.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.2
 */
public class CacheTag extends javax.servlet.jsp.tagext.TagSupport implements BaliseValeurCache {
  private static final long serialVersionUID = -1714913125326071337L;

  /**
   * Spécifie le nom de la cache a utiliser.
   */
  protected String cache;

  /**
   * Valeur du sous-cache qui doit être retrouvé.
   * 
   * @since SOFI 2.0.3
   */
  protected Object sousCache;

  /**
   * Nom de classe de filtre qui doit être utilisée.
   * 
   * @since SOFI 2.0.3
   */
  protected String filtre;

  /**
   * Spécifie l'aide contextuelle sur le libellé.
   */
  protected String valeur;

  /**
   * Spéficie la propriete de la valeur de l'objet cache si celle-ci n'est pas un String.
   */
  protected String proprieteAffichage;

  /**
   * Le nom de variable temporaire qui loge le message.
   */
  private String var;

  /**
   * Spécifie le séparateur des codes de valeurs à traiter par la cache, si plusieurs.
   * 
   * @since SOFI 2.0
   */
  private String separateurCacheMultiple;

  /**
   * Spécifie le séparateur des valeurs des codes à traiter par la cache, si plusieurs.
   * 
   * @since SOFI 2.0
   */
  private String separateurValeurCacheMultiple;

  private String codeClient;

  /** Constructeur par défaut */
  public CacheTag() {
  }

  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }
    return SKIP_BODY;
  }

  /**
   * Execute le début de la balise.
   * <p>
   * 
   * @exception JspException
   *              si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    String nomCache = getCache();

    // Utilisation de la cache
    ObjetCache objetCache = GestionCache.getInstance().getCache(nomCache, sousCache, codeClient);
    Map collection = null;

    if (objetCache != null) {
      collection = UtilitaireBaliseJSP.appliquerFiltreCache(objetCache.getCollection(), this.filtre, this, pageContext);

      if (getValeur() != null) {
        ArrayList listeValeurCacheATraiter = new ArrayList();

        if (getSeparateurCacheMultiple() != null) {
          StringTokenizer listeCache = new StringTokenizer(getValeur(), getSeparateurCacheMultiple());

          while (listeCache.hasMoreTokens()) {
            listeValeurCacheATraiter.add(listeCache.nextToken());
          }
        } else {
          listeValeurCacheATraiter.add(getValeur());
        }

        int compteurNbCache = 1;

        StringBuffer etiquetteComplete = new StringBuffer();

        // Parcourir tous les codes de valeurs a traiter.
        for (Iterator i = listeValeurCacheATraiter.iterator(); i.hasNext();) {
          // Accéder â la valeur du code
          Object cle = i.next();
          Object valeur = collection.get(cle);
          if (valeur == null) {
            valeur = cle;
          }

          // Traiter l'étiquette de la cache.
          String etiquette = null;
          boolean traiterLibelle = true;

          if (String.class.isInstance(valeur)) {
            etiquette = (String) valeur;
          } else if (DomaineValeur.class.isInstance(valeur)) {
            DomaineValeur domaineValeur = (DomaineValeur) valeur;
            Locale locale = UtilitaireBaliseJSP.getLocale(pageContext);

            if ((domaineValeur.getListeDescriptionLangue() != null)
                && (domaineValeur.getListeDescriptionLangue().size() > 0)) {
              etiquette = (String) domaineValeur.getListeDescriptionLangue().get(locale.toString());
              traiterLibelle = false;
            } else {
              etiquette = domaineValeur.getDescription();
            }
          } else {
            if (getProprieteAffichage() != null) {
              try {
                Object prop = PropertyUtils.getProperty(valeur, getProprieteAffichage()).toString();
                if (prop != null) {
                  etiquette = prop.toString();
                }
              } catch (Exception ex) {
                throw new JspException("Erreur pour obtenir la valeur de la propriété " + getProprieteAffichage(), ex);
              }
            } else {
              throw new SOFIException(
                  "Vous devez spécifier la propriété 'proprieteAffichage' de la balise sofi:cache afin d'accéder à une proprité d'un objet de transfert.");
            }
          }

          if (traiterLibelle) {
            // Appliquer la gestion des libellés.
            try {
              Libelle libelle = UtilitaireLibelle.getLibelle(etiquette, pageContext);
              etiquette = libelle.getMessage();
            } catch (Exception e) {
              etiquette = "";
            }
          }

          etiquetteComplete.append(etiquette);

          if ((getSeparateurValeurCacheMultiple() != null) && (compteurNbCache < listeValeurCacheATraiter.size())) {
            etiquetteComplete.append(" ");
            etiquetteComplete.append(getSeparateurValeurCacheMultiple());
            etiquetteComplete.append(" ");
          }

          compteurNbCache++;
        }

        if (getVar() != null) {
          this.pageContext.setAttribute(getVar(), etiquetteComplete.toString());
        } else {
          if (etiquetteComplete != null) {
            TagUtils.getInstance().write(pageContext, etiquetteComplete.toString());
          }
        }
      } else {
        if ((getVar() != null) && (collection.size() > 0)) {
          this.pageContext.setAttribute(getVar(), collection);
        }
      }
    }

    return (EVAL_PAGE);
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    this.cache = null;
    this.valeur = null;
    this.proprieteAffichage = null;
    this.var = null;
    this.sousCache = null;
    this.filtre = null;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * 
   * @throws javax.servlet.jsp.JspException
   */
  private void evaluerEL() throws JspException {
    try {
      String cache = EvaluateurExpression.evaluerString("cache", getCache(), this, pageContext);
      setCache(cache);

      if (getSousCache() != null) {
        sousCache = EvaluateurExpression.evaluer("sousCache", getSousCache().toString(), Object.class, this,
            pageContext);
      }

      Object valeur = EvaluateurExpression.evaluer("valeur", getValeur(), Object.class, this, pageContext);
      if (valeur != null) {
        setValeur(valeur.toString());
      }
    } catch (JspException e) {
      throw e;
    }
  }

  public void setCache(String cache) {
    this.cache = cache;
  }

  public String getCache() {
    return cache;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getValeur() {
    return valeur;
  }

  public void setProprieteAffichage(String proprieteAffichage) {
    this.proprieteAffichage = proprieteAffichage;
  }

  public String getProprieteAffichage() {
    return proprieteAffichage;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getVar() {
    return var;
  }

  public void setSeparateurCacheMultiple(String separateurCacheMultiple) {
    this.separateurCacheMultiple = separateurCacheMultiple;
  }

  public String getSeparateurCacheMultiple() {
    return separateurCacheMultiple;
  }

  public void setSeparateurValeurCacheMultiple(String separateurValeurCacheMultiple) {
    this.separateurValeurCacheMultiple = separateurValeurCacheMultiple;
  }

  public String getSeparateurValeurCacheMultiple() {
    return separateurValeurCacheMultiple;
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  @Override
  public Object getSousCache() {
    return sousCache;
  }

  public void setFiltre(String filtre) {
    this.filtre = filtre;
  }

  public String getFiltre() {
    return filtre;
  }

  @Override
  public String getNomCache() {
    return this.getCache();
  }

  @Override
  public Object getValeurCache() {
    return this.getValeur();
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
