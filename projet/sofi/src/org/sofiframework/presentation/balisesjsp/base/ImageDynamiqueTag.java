/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;
import org.sofiframework.utilitaire.documentelectronique.UtilitaireDocumentElectronique;


/**
 * Balise de présentation d'une image dynamique (DocumentElectronique).
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class ImageDynamiqueTag extends BaseELTag {
  /**
   * 
   */
  private static final long serialVersionUID = 7044143337123413103L;

  /**
   * L'instance de journalisation pour cette classe
   */
  static private Log log = LogFactory.getLog(ImageDynamiqueTag.class);

  /**
   * Objet de type org.sofiframework.utilitaire.documentelectronique.
   * DocumentElectronique qui sera utilisé pour générer l'image dynamique.
   */
  private DocumentElectronique valeur = null;

  /**
   * La valeur binaire de l'image à traiter en EL.
   */
  private String valeurBinaire;

  /**
   * Le document binaire a traiter.
   */
  private byte[] documentBinaire;

  /** Constructeur par défaut */
  public ImageDynamiqueTag() {
  }

  /**
   * Traitement effectué lors de l'ouverture de la balise.
   * @return directive qui doit être effectuée suite à la fin du traitement.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerValeur();

    UtilitaireDocumentElectronique utilDE = null;

    if (documentBinaire != null) {
      utilDE = new UtilitaireDocumentElectronique((HttpServletRequest) this.pageContext.getRequest(),
          (HttpServletResponse) this.pageContext.getResponse(),
          documentBinaire, "image");
    } else {
      // Retrouver le document à partir de la référence.
      DocumentElectronique document = (DocumentElectronique) this.getValeurEL("valeur",
          valeur, DocumentElectronique.class);

      if (document == null) {
        throw new SOFIException("Le document n'as pas été fixé pour affichage");
      }

      if (document.getNomFichier() == null) {
        document.setNomFichier("image");
      }

      utilDE = new UtilitaireDocumentElectronique((HttpServletRequest) this.pageContext.getRequest(),
          (HttpServletResponse) this.pageContext.getResponse(),
          document.getValeurDocument(), document.getNomFichier());
    }

    try {
      utilDE.ecrireContenuFichierAuClient();
    } catch (IOException e) {
      log.error(e);
    }

    return (EVAL_PAGE);
  }

  /**
   * Évaluer le ou les valeurs spécifier dans l'attribut valeurBinaire.
   * <p>
   * Pour que ça fonctionne de manière optimale, il faut placer les byte[] à afficher dans une variable de session
   * ou encore une arraylist de byte[] comme dans l'exemple ci-dessous :<br>
   * <b><i>&lt;c:set var="imageSession" value="${image.contenuImage}" scope="session"/&gt;</i></b><br>
   * où la variable ${image.contenuImage} est un tableau de byte (byte[]).<br><br>
   * Cette image est affichée dans la page en utilisant la balise suivante<br>
   * <b><i>&lt;sofi:imageDynamique valeurBinaire="imageSession"/&gt;</i></b>
   */
  synchronized public void evaluerValeur() throws JspException {
    if (getValeurBinaire() != null) {
      Object valeur = pageContext.getSession().getAttribute(getValeurBinaire());

      if (valeur instanceof List) {
        // Indice de l'image a extraire de la liste.
        String index = pageContext.getRequest().getParameter("index");

        // La liste d'image a afficher.
        List listeImage = (List) valeur;

        // Le compteur permettant d'acceder à la bonne image a afficher.
        Integer compteur = (Integer) pageContext.getSession().getAttribute("COMPTEUR_LISTE_IMAGE");

        if (compteur == null) {
          compteur = new Integer(1);
        } else {
          compteur = new Integer(compteur.intValue() + 1);
        }

        pageContext.getSession().setAttribute("COMPTEUR_LISTE_IMAGE", compteur);

        // Extraire le document de la liste.
        byte[] image = (byte[]) listeImage.get((new Integer(index)).intValue());

        // Fixer le document binaire à traiter.
        setDocumentBinaire(image);

        if (compteur.intValue() == listeImage.size()) {
          // Si la dernière image de la liste, on doit initialiser le compteur et la liste.
          pageContext.getSession().setAttribute("COMPTEUR_LISTE_IMAGE",
              new Integer(0));
          pageContext.getSession().setAttribute(getValeurBinaire(), null);
        }
      } else {
        byte[] image = (byte[]) pageContext.getSession().getAttribute(getValeurBinaire());
        setDocumentBinaire(image);
        pageContext.getSession().removeAttribute(getValeurBinaire());
      }
    }
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  /**
   * Fixer la valeur.
   * @param valeur la valeur
   */
  public void setValeur(DocumentElectronique valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir la valeur.
   * @return la valeur
   */
  public DocumentElectronique getValeur() {
    return valeur;
  }

  public void setValeurBinaire(String valeurBinaire) {
    this.valeurBinaire = valeurBinaire;
  }

  public String getValeurBinaire() {
    return valeurBinaire;
  }

  public void setDocumentBinaire(byte[] documentBinaire) {
    this.documentBinaire = documentBinaire;
  }

  public byte[] getDocumentBinaire() {
    return documentBinaire;
  }
}
