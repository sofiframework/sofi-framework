/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.Href;


/**
 * Balise permettant d'affichage à l'utilisateur qu'il y a de l'aide de disponible
 * pour le composant d'interface qu'il visualise.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.6
 * @see org.sofiframework.presentation.balisesjsp.base.PopupTag
 */
public class AideTag extends PopupTag {

  /**
   * 
   */
  private static final long serialVersionUID = -4682525660319932777L;

  /**
   * L'instance de journalisation pour cette classe.
   */
  private static final Log log = LogFactory.getLog(AideTag.class);

  /**
   * Le du composant du référentiel pouvant avoir de l'aide.
   */
  private String nomComposantReferentiel;

  /** Constructeur par défaut */
  public AideTag() {
  }

  /**
   * Execute le début de la balise.
   * <p>
   * @exception JspException si un exception JSP est lancé.
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    Aide aide = null;
    ObjetSecurisable objetSecurisable = null;

    if (getVar() == null) {
      GestionAide.getInstance().setInfoPopupAide(isMaximiser(), getParamId(),
          getParamName(), getParamProperty(), getParamScope(), getName(),
          getProperty(), getScope(), isAdresseSurMemeDomaine(), getNomfenetre(),
          getLargeurfenetre(), getHauteurfenetre(), getScrollbars(),
          isBarreOutil(), isBarreAdresse(), isBarreAdresse());
      GestionAide.getInstance().setAjax(isAjax());
    }

    if (getNomComposantReferentiel() == null) {
      String adresseCourante = UtilitaireBaliseJSP.getAdresseActionCourante(pageContext.getRequest());
      String ancienneAdresseAvantRedirect = (String) pageContext.getSession()
          .getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE_AVANT_REDIRECT);
      objetSecurisable = GestionSecurite.getInstance()
          .getObjetSecurisableParAdresse(adresseCourante);

      if ((ancienneAdresseAvantRedirect != null) && (objetSecurisable == null)) {
        objetSecurisable = GestionSecurite.getInstance()
            .getObjetSecurisableParAdresse(ancienneAdresseAvantRedirect);
      }

      if (objetSecurisable != null) {
        pageContext.getSession().setAttribute("SOFI_DERNIER_OBJET_DEMANDE",
            objetSecurisable);
      } else {
        objetSecurisable = (ObjetSecurisable) pageContext.getSession()
            .getAttribute("SOFI_DERNIER_OBJET_DEMANDE");
      }
    } else {
      objetSecurisable = GestionSecurite.getInstance().getObjetSecurisable(getNomComposantReferentiel());
    }

    if (objetSecurisable != null) {
      try {
        aide = GestionAide.getInstance().getAide(objetSecurisable,
            UtilitaireBaliseJSP.getLocale(pageContext));
      } catch (Exception e) {
        if (log.isWarnEnabled()) {
          log.warn(e);
        }
      }
    }

    if (aide != null) {
      this.pageContext.setAttribute("objetSecurisableAvecAide", Boolean.TRUE);

      if (getVar() == null) {
        Href lienAide = new Href(GestionAide.getInstance().getUrlAide());
        lienAide.ajouterParametre("sofi_seq_objet_java_aide",
            objetSecurisable.getSeqObjetSecurisable());

        setHref(lienAide.toString());

        StringBuffer buffer = new StringBuffer();
        buffer.append(genererDebutLienAppelPopup());

        if (!isAjax()) {
          buffer.append(UtilitaireBaliseJSP.genererFonctionPopupAide(
              getHref(), pageContext));
        } else {
          buffer.append(genererFonctionPopupAjax());
        }

        buffer.append("\"");
        buffer.append(">");

        JspWriter writer = this.pageContext.getOut();

        try {
          writer.print(buffer.toString());
        } catch (IOException e) {
          throw new JspException();
        }

        return EVAL_BODY_INCLUDE;
      } else {
        if (aide != null) {
          this.pageContext.setAttribute(getVar(), aide);

          if (log.isInfoEnabled()) {
            log.info("Proposition de l'aide suivante :" + aide.getCleAide());
          }
        }
        return (EVAL_PAGE);
      }
    } else {
      /*
       * On doit signaler à la méthode de fin de tag qu'on ne doit pas faire la fin du tag A.
       */
      this.pageContext.setAttribute("objetSecurisableAvecAide", Boolean.FALSE);

      if (getVar() != null) {
        this.pageContext.setAttribute(getVar(), null);
      }

      return SKIP_BODY;
    }
  }

  /**
   * Execute la fin de la balise.
   * @exception JspException si un exception JSP est lancé.
   */
  @Override
  public int doEndTag() throws JspException {
    Boolean objetAvecAide = (Boolean) this.pageContext.getAttribute("objetSecurisableAvecAide");
    if (objetAvecAide.booleanValue()) {
      super.doEndTag(); //Méthode qui ecrit le </a> dans la réponse.
    }
    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public void evaluerEL() throws JspException {
    super.evaluerEL();
    String nomComposantReferentiel = EvaluateurExpression.evaluerString("nomComposantReferentiel",
        getNomComposantReferentiel(), this, pageContext);
    setNomComposantReferentiel(nomComposantReferentiel);
  }

  /**
   * Libération de  l'objet pour une exécution subséquante.
   */
  @Override
  public void release() {
  }

  public void setNomComposantReferentiel(String nomComposantReferentiel) {
    this.nomComposantReferentiel = nomComposantReferentiel;
  }

  public String getNomComposantReferentiel() {
    return nomComposantReferentiel;
  }
}
