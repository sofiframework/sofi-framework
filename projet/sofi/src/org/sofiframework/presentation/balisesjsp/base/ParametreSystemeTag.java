/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Balise permettant d'extraire un paramètre système.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.8.2
 */
public class ParametreSystemeTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -2126255514147803389L;
  private static final String REQUEST = "request";
  private static final String SESSION = "session";
  private static final String APPLICATION = "application";
  private String var;
  private int scope;
  private String code;
  private String client;

  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    String valeur = GestionParametreSysteme.getInstance().getString(getCode(), getClient());

    if (var != null) {
      if (scope == 0) {
        pageContext.setAttribute(var, valeur);
      } else {
        pageContext.setAttribute(var, valeur, scope);
      }
    } else {
      try {
        pageContext.getOut().print(valeur);
      } catch (IOException e) {
        throw new JspException(e);
      }
    }

    return EVAL_PAGE;
  }

  @Override
  public void release() {
    var = null;
    scope = 0;
    code = null;
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String code = EvaluateurExpression.evaluerString("code", getCode(), this,
        pageContext);
    setCode(code);
  }

  private static int getScope(String scope) {
    if (REQUEST.equalsIgnoreCase(scope)) {
      return PageContext.REQUEST_SCOPE;
    } else if (SESSION.equalsIgnoreCase(scope)) {
      return PageContext.SESSION_SCOPE;
    } else if (APPLICATION.equalsIgnoreCase(scope)) {
      return PageContext.APPLICATION_SCOPE;
    } else {
      return PageContext.PAGE_SCOPE;
    }
  }

  public void setScope(String scope) {
    this.scope = getScope(scope);
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getVar() {
    return var;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }
}
