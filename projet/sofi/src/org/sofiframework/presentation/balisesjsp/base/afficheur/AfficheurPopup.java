/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base.afficheur;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe d'afficheur pour des éléments qui appelle une fenêtre flottante.
 * @author Jean-François Brassard
 * @version SOFI 1.1
 */
public class AfficheurPopup {
  /**
   * Méthode permettant l'affichage d'un élément HTML appelant une fenêtre flottante.
   * @param url l'adresse web a appellé
   * @param titre le titre qui appelle la fenêtre flottante.
   * @param styleClass le style du titre
   * @param largeurFenetre la largeur de la fenêtre de la fenêtre flottante appellé.
   * @param hauteurFenetre la hauteur de la fenêtre de la fenêtre flottante appellé.
   * @param nomFenetre le nom de la fenêtre flottante.
   * @param barreDeroulante true si vous désirez une barre déroulante.
   * @param barreOutil true si vous désirez une barre d'outil.
   * @param barreMenu true si vous désirez une barre de menu.
   * @param barreAdresse true si vous désirez une barre d'adresse.
   * @param barreStatut true si vous désirez une barre de statut.
   * @return le résultat d'affichage de l'élément HMTL
   */
  static public String affichage(String url, String titre, String styleClass,
      int largeurFenetre, int hauteurFenetre, String nomFenetre,
      boolean barreDeroulante, boolean barreOutil, boolean barreMenu,
      boolean barreAdresse, boolean barreStatut) {
    StringBuffer resultat = new StringBuffer();

    resultat.append("<a ");

    if (styleClass != null) {
      resultat.append("class =\"");
      resultat.append(styleClass);
      resultat.append("\"");
    }

    resultat.append(" href=\"javascript:void(0)\" onClick=\"javascript:");
    resultat.append(AfficheurPopup.getFonctionJS(url, largeurFenetre,
        hauteurFenetre, nomFenetre, barreDeroulante, barreOutil, barreMenu,
        barreAdresse, barreStatut));

    resultat.append("\">");
    resultat.append(titre);
    resultat.append("</a>");

    return resultat.toString();
  }

  /**
   * Retourne la fonction JS qui permet d'appeler une fenetre flottante.
   * @return la fonction JS qui permet d'appeler une fenetre flottante.
   * @param barreStatut true si vous désirez afficher la barre de statut.
   * @param barreAdresse true si vous désirez afficher la barre de statut.
   * @param barreMenu true si vous désirez afficher la barre de statut.
   * @param barreOutil true si vous désirez afficher la barre de statut.
   * @param barreDeroulante true si vous désirez afficher la barre de statut.
   * @param nomFenetre le nom de la fenêtre
   * @param hauteurFenetre la hauteur de la fenêtre.
   * @param largeurFenetre
   * @param url
   */
  public static String getFonctionJS(String url, int largeurFenetre,
      int hauteurFenetre, String nomFenetre, boolean barreDeroulante,
      boolean barreOutil, boolean barreMenu, boolean barreAdresse,
      boolean barreStatut) {
    StringBuffer resultat = new StringBuffer();

    resultat.append("popup('");
    resultat.append(url);

    if (url.indexOf("?") != -1) {
      resultat.append("&SOFIignorerRetour=true");
    } else {
      resultat.append("?SOFIignorerRetour=true");
    }

    resultat.append("','");

    String nomFenetreDefaut = "popup";

    if (UtilitaireString.isVide(nomFenetre)) {
      nomFenetre = nomFenetreDefaut;
    }

    resultat.append(nomFenetre);
    resultat.append("','");
    resultat.append(largeurFenetre);
    resultat.append("','");
    resultat.append(hauteurFenetre);
    resultat.append("','");
    resultat.append("resizable=1");

    if (barreStatut) {
      resultat.append(",status=1");
    }

    if (barreOutil) {
      resultat.append(",toolbar=1");
    }

    if (barreMenu) {
      resultat.append(",menubar=1");
    }

    if (barreAdresse) {
      resultat.append(",location=1");
    }

    if (barreDeroulante) {
      resultat.append(",scrollbars=1");
    }

    resultat.append("');");

    return resultat.toString();
  }
}
