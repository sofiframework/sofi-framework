/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Tag qui permet d'afficher un libellé et une valeur JSTL.
 * <p>
 * Si aucune information sur le libellé n'est fournie, seulement la
 * valeur sera afichée.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 */
public class AffichageELTag extends BaseELTag {
  /**
   * 
   */
  private static final long serialVersionUID = -1992668327388274927L;

  /** le libellé à afficher avant le champ valeur */
  protected String libelle;

  /** Titre de l'ensemble valeur et libellé */
  protected String titre;

  /** la valeur EL, c'est-à-dire la valeur JSTL à évaluer (Ex : ${listeAffichage.message}) */
  protected Object valeur;

  /**
   * Spécifie si on désire fermer le ligne automatiquement.
   * La balise <tr> s'ajoute automatiquement après le composant d'interface.
   */
  private boolean fermerLigne = true;

  /**
   * Spécifie si on désire ouvrir une nouvelle ligne.
   * L'ouverture de la balise tr est ajoutée automatiquement si la propriété
   * n'est pas ajoutée au tag.
   */
  private boolean ouvrirLigne = true;

  /**
   * Spécifie que la conversion en HTML doit être faite.
   * @since SOFI 2.1
   */
  private

  boolean convertirEnHtml = true;

  /** Constructeur par défaut */
  public AffichageELTag() {
  }

  /**
   * Fixer le titre de l'ensemble valeur et libellé.
   * <p>
   * @param titre le titre de l'ensemble valeur et libellé
   */
  public void setTitre(String titre) {
    this.titre = titre;
  }

  /**
   * Obtenir le titre de l'ensemble valeur et libellé.
   * <p>
   * @return le titre de l'ensemble valeur et libellé
   */
  public String getTitre() {
    return titre;
  }

  /**
   * Fixer le libellé à afficher avant le champ valeur.
   * <p>
   * @param libelle le libellé à afficher avant le champ valeur
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir le libellé à afficher avant le champ valeur.
   * <p>
   * @return le libellé à afficher avant le champ valeur
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Fixer la valeur EL, c'est-à-dire la valeur JSTL à évaluer.
   * <p>
   * @param valeur la valeur EL, c'est-à-dire la valeur JSTL à évaluer
   */
  public void setValeur(Object valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir la valeur EL, c'est-à-dire la valeur JSTL à évaluer.
   * <p>
   * @return la valeur EL, c'est-à-dire la valeur JSTL à évaluer
   */
  public Object getValeur() {
    return valeur;
  }

  /**
   * Obtenir la valeur qui spécifie si on désire fermer le ligne automatiquement..
   * <p>
   * @return la valeur qui spécifie si on désire fermer le ligne automatiquement.
   */
  public boolean getFermerLigne() {
    return fermerLigne;
  }

  /**
   * Fixer la valeur qui spécifie si on désire fermer le ligne automatiquement..
   * <p>
   * @param fermerLigne la valeur qui spécifie si on désire fermer le ligne automatiquement.
   */
  public void setFermerLigne(boolean fermerLigne) {
    this.fermerLigne = fermerLigne;
  }

  /**
   * Obtenir la valeur qui spécifie si on désire ouvrir une nouvelle ligne.
   * <p>
   * @return la valeur qui spécifie si on désire ouvrir une nouvelle ligne.
   */
  public boolean getOuvrirLigne() {
    return ouvrirLigne;
  }

  /**
   * Fixer la valeur qui spécifie si on désire ouvrir une nouvelle ligne.
   * <p>
   * @param ouvrirLigne la valeur qui spécifie si on désire ouvrir une nouvelle ligne.
   */
  public void setOuvrirLigne(boolean ouvrirLigne) {
    this.ouvrirLigne = ouvrirLigne;
  }

  /**
   * Génération de la balises de saisie.
   * <p>
   * Cette balise est utilisée pour afficher un libellé suivi d'une valeur JSTL provenant du langage EL.
   * Voici un exemple de résultat de cette balise :<BR><BR>
   * <i><b>Message : Ceci est mon message.</b></i><BR><BR>
   * La balise qui aurait pu générer ce résultat est la suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libellé="Message" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * Par défaut, cette balise s'affiche sur une seule ligne, cependant il est possible d'en afficher
   * deux par page avec l'utilisation des attributs <code>ouvrirLigne</code> et <code>fermerLigne</code>.
   * Cette balise supporte aussi l'utilisation de libellés dynamiques. Ainsi l'exemple plus haut
   * aurait aussi pu être de la forme suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libelle="org.sofiframework.message.erreur" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * <p>
   * @exception JspException si une exception JSP est lancé.
   * @return la valeur indiquant que faire avec le traitement restant de la page.
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    String codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {
        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant();
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(pageContext, codeHtml);
    }

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() {
    StringBuffer html = new StringBuffer("");

    if (getLibelle() != null && getOuvrirLigne()) {
      html.append("<tr>");
    }

    if (getLibelle() != null) {
      html.append("<th scope=\"row\">");

      // Ajouter le bout de code qui représente le libellé
      html.append(UtilitaireLibelle.getLibelle(libelle, pageContext,
          null).getMessage());
      html.append("</th><td>");
    }

    // Afficher la valeur EL
    Object valeurEL = this.getValeurEL("value", getValeur(), Object.class);

    if (valeurEL != null) {
      if (isConvertirEnHtml()) {
        html.append(UtilitaireString.convertirEnHtml(valeurEL.toString()));
      } else {
        html.append(valeurEL.toString());
      }
    }

    return html.toString();
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      resultat.append(valeur);
    }

    // Fermeture de la colonne si libellé de spécifier.
    if (getLibelle() != null) {
      resultat.append("</td>");

      if (getFermerLigne()) {
        resultat.append("</tr>");
      }
    }

    TagUtils.getInstance().write(pageContext, resultat.toString());

    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      if (getLibelle() != null) {
        String libelle =
            EvaluateurExpression.evaluerString("libelle", getLibelle(), this,
                pageContext);
        setLibelle(libelle);
      }
    } catch (Exception e) {
    }
  }

  public void setConvertirEnHtml(boolean convertirEnHtml) {
    this.convertirEnHtml = convertirEnHtml;
  }

  public boolean isConvertirEnHtml() {
    return convertirEnHtml;
  }
}
