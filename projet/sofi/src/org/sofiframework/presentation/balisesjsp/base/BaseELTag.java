/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Tag qui permet de gérer les expressions JSTL.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class BaseELTag extends javax.servlet.jsp.tagext.BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 6310675058095506170L;

  /** Constructeur par défaut */
  public BaseELTag() {
  }

  /**
   * Permet d'obtenir la valeur EL d'un parameter du tag
   * @param nomParametre Nom de le la propriété qui contient le texte JSTL.
   * @param valeurParametre valeur texte de la propriété JSTL
   * @param classeParametre Classe de l'objet qui doit être retrouvé
   * grâce à l'expression JSTL
   * @return la valeur de l'objet traité par les expressions JSTL.
   */
  public Object getValeurEL(String nomParametre, Object valeurParametre, Class classeParametre) {
    if (valeurParametre != null) {
      if (EvaluateurExpression.isEvaluerEL(pageContext)) {
        try {
          return ExpressionEvaluatorManager.evaluate(nomParametre,
              (String) valeurParametre, classeParametre, this, this.pageContext);
        } catch (Exception ex) {
        }
      }
    }
    return valeurParametre;
  }
}
