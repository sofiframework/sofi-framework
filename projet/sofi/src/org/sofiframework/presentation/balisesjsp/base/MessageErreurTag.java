/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Balise affichant un message d'erreur pour une propriété spécique.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.3
 * @since SOFI 2.1
 * Support des formulaires imbriqués, possibile de spécifier un icone avec
 * aide contextuelle spécifiant le message d'erreur.
 */
public class MessageErreurTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -3442685858204319489L;

  /**
   * Le nom d'attribut correspondant a une erreur.
   */
  private String nomAttribut;

  /**
   * Le style CSS.
   */
  private String styleClass;

  /**
   * Le nom de variable temporaire qui loge le message.
   */
  private String var = null;

  /**
   * Ajouter un icone avec aide contextuelle lorsqu'un
   * message d'erreur est trouvé.
   */
  private String iconeAvecAideContextuelle;

  /** Constructeur par défaut */
  public MessageErreurTag() {
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    // Traiter les expressions EL.
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    StringBuffer resultat = new StringBuffer();

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    MessageErreur messageErreur = null;

    String nomAttribut = getNomAttribut();

    String nomAttributNested =
        UtilitaireBaliseNested.getIdentifiantUnique(getNomAttribut(),
            pageContext);

    if (formulaire != null) {
      Map lesErreurs = formulaire.getLesErreurs();

      boolean erreur = lesErreurs.containsKey(nomAttribut);
      boolean erreurNested = lesErreurs.containsKey(nomAttributNested);

      if (erreur || erreurNested) {

        String attributEnErreur = null;

        if (erreur) {
          attributEnErreur = nomAttribut;
        } else {
          attributEnErreur = nomAttributNested;
        }
        messageErreur =
            UtilitaireBaliseJSP.getMessageErreur(formulaire, attributEnErreur,
                pageContext);

        if (getNomAttribut() != null) {
          if (!isAfficherIconeAvecAideContextuelle()) {
            resultat.append("<span class=\"");

            if (getStyleClass() == null) {
              resultat.append("libelle_message_erreur");
            } else {
              resultat.append(getStyleClass());
            }

            resultat.append("\">");
            resultat.append(messageErreur.getMessage());
            resultat.append("</span>");
          } else {
            // Pour que le titre du champ de saisie soit convertie en texte du gestion message
            resultat.append(UtilitaireBaliseJSP.genererChampInvisiblePourFocus(attributEnErreur +
                "_erreur",
                messageErreur.getMessage()));
          }
        }
      }
    }

    if (messageErreur != null) {
      if (getVar() != null) {
        this.pageContext.setAttribute(getVar(), messageErreur.getMessage());
      } else {
        TagUtils.getInstance().write(pageContext, resultat.toString());
      }
    }

    return Tag.EVAL_PAGE;
  }

  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  public String getNomAttribut() {
    return nomAttribut;
  }

  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  public String getStyleClass() {
    return styleClass;
  }

  @Override
  public int doEndTag() throws JspException {
    setNomAttribut(null);
    setStyleClass(null);
    setVar(null);

    return super.doEndTag();
  }

  /**
   * Fixer le nom de la variable temporaire qui loge le message d'erreur.
   * @param var le nom de la variable temporaire qui loge le message d'erreur.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom de la variable temporaire qui loge le message d'erreur.
   * @return le nom de la variable temporaire qui loge le message d'erreur.
   */
  public String getVar() {
    return var;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    try {
      String var =
          EvaluateurExpression.evaluerString("var", getVar(), this, pageContext);
      setVar(var);

      String iconeAvecAideContextuelle =
          EvaluateurExpression.evaluerString("iconeAvecAideContextuelle",
              getIconeAvecAideContextuelle(),
              this, pageContext);
      setIconeAvecAideContextuelle(iconeAvecAideContextuelle);

      String nomAttribut =
          EvaluateurExpression.evaluerString("nomAttribut", getNomAttribut(),
              this, pageContext);
      setNomAttribut(nomAttribut);
    } catch (JspException e) {
      throw e;
    }
  }

  public void setIconeAvecAideContextuelle(String iconeAvecAideContextuelle) {
    this.iconeAvecAideContextuelle = iconeAvecAideContextuelle;
  }

  public String getIconeAvecAideContextuelle() {
    return iconeAvecAideContextuelle;
  }

  public boolean isAfficherIconeAvecAideContextuelle() {
    if ((getIconeAvecAideContextuelle() != null) &&
        getIconeAvecAideContextuelle().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }
}
