/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.constante.Constantes;


/**
 * Balise Jsp qui permet de savoir quelle version de fureteur Internet est
 * utilisée.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class TypeNavigateurTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -5259118544034479170L;

  /**
   * Spécifie si le navigateur utilisé est Internet Explorer 5.5 et plus
   */
  protected String ie_55 = null;

  /**
   * Spécifie si le navigateur utilisé est Mozilla (Moteur de type Gecko)
   */
  protected String mozilla = null;

  /**
   * Constructeur par défault.
   */
  public TypeNavigateurTag() {
  }

  /**
   * Obtenir si le navigateur utilisé est Internet Explorer 5.5 et plus
   * @return si le navigateur utilisé est Internet Explorer 5.5 et plus
   */
  public String getIe5_5() {
    return (this.ie_55);
  }

  /**
   * Fixer si le navigateur utilisé est Internet Explorer 5.5 et plus
   * @param ie_55 si le navigateur utilisé est Internet Explorer 5.5 et plus
   */
  public void setIe5_5(String ie_55) {
    this.ie_55 = ie_55;
  }

  /**
   * Obtenir si le navigateur utilisé est Mozilla (Moteur de type Gecko)
   * @return si le navigateur utilisé est Mozilla (Moteur de type Gecko)
   */
  public String getMozilla() {
    return (this.mozilla);
  }

  /**
   * Fixer si le navigateur utilisé est Mozilla (Moteur de type Gecko)
   * @param mozilla si le navigateur utilisé est Mozilla (Moteur de type Gecko)
   */
  public void setMozilla(String mozilla) {
    this.mozilla = mozilla;
  }

  /**
   * Exécution de l'ouverture de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    if (valider(pageContext.getSession())) {
      return (EVAL_BODY_INCLUDE);
    } else {
      return (SKIP_BODY);
    }
  }

  /**
   * Exécution de la fin de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    return (EVAL_PAGE);
  }

  /**
   * Verifier si le type de navigateur est selon demandé
   */
  private boolean valider(javax.servlet.http.HttpSession session) {
    int typeNavigateurEnTraitement = ((Integer) session.getAttribute(Constantes.TYPE_NAVIGATEUR)).intValue();
    ArrayList listeNavigateurAVerifier = new ArrayList();
    int typeNavigateurAValider = 0;

    if ((getMozilla() != null) && !getMozilla().equals("")) {
      typeNavigateurAValider = Constantes.MOZILLA;
      listeNavigateurAVerifier.add(new Integer(Constantes.IE_55));
    }

    if ((getIe5_5() != null) && !getIe5_5().equals("")) {
      typeNavigateurAValider = Constantes.IE_55;
      listeNavigateurAVerifier.add(new Integer(Constantes.MOZILLA));
    }

    if (listeNavigateurAVerifier.size() == 1) {
      switch (typeNavigateurAValider) {
      case Constantes.IE_55: {
        if (typeNavigateurEnTraitement == Constantes.IE_55) {
          return true;
        }
      }

      break;

      case Constantes.MOZILLA: {
        if (typeNavigateurEnTraitement == Constantes.MOZILLA) {
          return true;
        }
      }

      break;

      default:
        return false;
      }
    } else {
      Iterator iterateur = listeNavigateurAVerifier.iterator();

      while (iterateur.hasNext()) {
        Integer valeur = (Integer) iterateur.next();

        if (valeur.intValue() == typeNavigateurEnTraitement) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    super.release();
  }
}
