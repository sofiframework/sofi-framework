/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.Tag;

import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;

/**
 * Tag de base d'affichage de messages. Classe abstraite qui permet l'implantation d'un mécanisme d'évaluation
 * d'expression
 * <p>
 * 
 * @author Steve Tremblay (Nurun inc.)
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BaseMessageTag extends javax.servlet.jsp.tagext.TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 7096381666609309168L;

  /**
   * Identifiant unique du message.
   */
  private String identifiant;

  /**
   * Code de l'application qui est propriétaire du message.
   */
  private String application;

  /**
   * Paramètre 1 à incorporer au message.
   */
  private String parametre1;

  /**
   * Paramètre 2 à incorporer au message.
   */
  private String parametre2;

  /**
   * Paramètre 3 à incorporer au message.
   */
  private String parametre3;

  /**
   * Paramètre 4 à incorporer au message.
   */
  private String parametre4;

  /**
   * Paramètre 5 à incorporer au message.
   */
  private String parametre5;

  /**
   * Module qui est propriétaire du message.
   */
  private String module;

  /**
   * Le nom de variable temporaire qui loge le message.
   */
  private String var;

  /**
   * Le lien hypertexte de confirmation associé à l'identifiant du message.
   */
  private String hrefConfirmation;

  /**
   * Le code de client pour lequel on veut le libellé.
   */
  private String codeClient;

  /**
   * Le message en cours.
   */
  protected Message message;

  /** Constructeur par défaut */
  public BaseMessageTag() {
  }

  /**
   * Obtenir l'identifiant unique du message.
   * 
   * @return l'identifiant unique du message
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant unique du message.
   * 
   * @param identifiant
   *          l'identifiant unique du message
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Execute le début de la balise. Appel son parent.
   * 
   * @exception JspException
   *              si un exception JSP est lancé.
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    if (getHrefConfirmation() != null) {
      // Traitement afin d'associer un url avec l'identifiant du message afin
      // d'automatiser les messages de confirmations.
      // Fabriquer le lien URL
      if (UtilitaireBaliseJSP.isValeurEL(getHrefConfirmation())) {
        setHrefConfirmation(EvaluateurExpression.evaluerString("hrefConfirmation", getHrefConfirmation(), this,
            pageContext));
      } else {
        setHrefConfirmation(UtilitaireBaliseJSP.genererUrl(pageContext, getHrefConfirmation()));
      }

      LinkedHashMap listeMessageAvecHrefConfirmation = UtilitaireBaliseJSP
          .getListeMessageAvecHrefConfirmation(pageContext);
      listeMessageAvecHrefConfirmation.put(identifiant, getHrefConfirmation());
    }

    Object[] args = new Object[5];
    args[0] = parametre1;
    args[1] = parametre2;
    args[2] = parametre3;
    args[3] = parametre4;
    args[4] = parametre5;

    message = UtilitaireMessage.get(identifiant, args, this.pageContext, codeClient);

    return (Tag.EVAL_PAGE);
  }

  /**
   * Execute la fin de la balise.
   * 
   * @exception JspException
   *              if a JSP exception has occurred
   */
  @Override
  public int doEndTag() throws JspException {
    if (getVar() != null) {
      this.pageContext.setAttribute(getVar(), message.getMessage());
    } else {
      if (getHrefConfirmation() == null) {
        // Ecrire le contenu dans le writer.
        JspWriter writer = this.pageContext.getOut();

        try {
          writer.print(message.getMessage());
        } catch (IOException e) {
          throw new JspException();
        }
      }
    }

    return (EVAL_PAGE);
  }

  /**
   * Initialisation des propriétés qui pourraient influancer l'exécution de la balise.
   */
  @Override
  public void release() {
    message = null;
    super.release();
  }

  /**
   * Obtenir le code de l'application qui est propriétaire du message.
   * 
   * @return le code de l'application qui est propriétaire du message
   */
  public String getApplication() {
    return application;
  }

  /**
   * Fixer le code de l'application qui est propriétaire du message
   * 
   * @param newApplication
   *          le code de l'application qui est propriétaire du message
   */
  public void setApplication(String newApplication) {
    application = newApplication;
  }

  /**
   * Obtenir le paramètre 1 à incorporer au message.
   * 
   * @return le paramètre 1 à incorporer au message
   */
  public String getParametre1() {
    return parametre1;
  }

  /**
   * Fixer le paramètre 1 à incorporer au message.
   * 
   * @param parametre1
   *          le paramètre 1 à incorporer au message
   */
  public void setParametre1(String parametre1) {
    this.parametre1 = parametre1;
  }

  /**
   * Obtenir le paramètre 2 à incorporer au message.
   * 
   * @return le paramètre 2 à incorporer au message
   */
  public String getParametre2() {
    return parametre2;
  }

  /**
   * Fixer le paramètre 2 à incorporer au message.
   * 
   * @param parametre2
   *          le paramètre 2 à incorporer au message
   */
  public void setParametre2(String parametre2) {
    this.parametre2 = parametre2;
  }

  /**
   * Obtenir le paramètre 3 à incorporer au message.
   * 
   * @return le paramètre 3 à incorporer au message
   */
  public String getParametre3() {
    return parametre3;
  }

  /**
   * Fixer le paramètre 3 à incorporer au message.
   * 
   * @param parametre3
   *          le paramètre 3 à incorporer au message
   */
  public void setParametre3(String parametre3) {
    this.parametre3 = parametre3;
  }

  /**
   * Obtenir le paramètre 4 à incorporer au message.
   * 
   * @return le paramètre 4 à incorporer au message
   */
  public String getParametre4() {
    return parametre4;
  }

  /**
   * Fixer le paramètre 4 à incorporer au message.
   * 
   * @param parametre4
   *          le paramètre 4 à incorporer au message
   */
  public void setParametre4(String parametre4) {
    this.parametre4 = parametre4;
  }

  /**
   * Obtenir le paramètre 5 à incorporer au message.
   * 
   * @return le paramètre 5 à incorporer au message
   */
  public String getParametre5() {
    return parametre5;
  }

  /**
   * Fixer le paramètre 5 à incorporer au message.
   * 
   * @param parametre5
   *          le paramètre 5 à incorporer au message
   */
  public void setParametre5(String parametre5) {
    this.parametre5 = parametre5;
  }

  /**
   * Obtenir le module qui est propriétaire du message.
   * 
   * @return le module qui est propriétaire du message
   */
  public String getModule() {
    return module;
  }

  /**
   * Fixer le module qui est propriétaire du message.
   * 
   * @param module
   *          le module qui est propriétaire du message
   */
  public void setModule(String module) {
    this.module = module;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * 
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    try {
      String parametre1 = EvaluateurExpression.evaluerString("parametre1", getParametre1(), this, pageContext);
      setParametre1(parametre1);

      String parametre2 = EvaluateurExpression.evaluerString("parametre2", getParametre2(), this, pageContext);
      setParametre2(parametre2);

      String parametre3 = EvaluateurExpression.evaluerString("parametre3", getParametre3(), this, pageContext);
      setParametre3(parametre3);

      String parametre4 = EvaluateurExpression.evaluerString("parametre4", getParametre4(), this, pageContext);
      setParametre4(parametre4);

      String parametre5 = EvaluateurExpression.evaluerString("parametre5", getParametre5(), this, pageContext);
      setParametre5(parametre5);

      String identifiant = EvaluateurExpression.evaluerString("identifiant", getIdentifiant(), this, pageContext);
      setIdentifiant(identifiant);
    } catch (JspException e) {
      throw e;
    }
  }

  public String getVar() {
    return var;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getHrefConfirmation() {
    return hrefConfirmation;
  }

  public void setHrefConfirmation(String hrefConfirmation) {
    this.hrefConfirmation = hrefConfirmation;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
