/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Tag qui permet d'afficher un libellé et une valeur JSTL.
 * <p>
 * Si aucune information sur le libellé n'est fournie, seulement la
 * valeur sera afichée.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class FieldsetTag extends BaseELTag {
  protected static Log log = LogFactory.getLog(FieldsetTag.class);
  private static final long serialVersionUID = 1354208494546068337L;

  /** Titre de du tableau */
  private String titre;

  /** La classe CSS du tableau */
  private String styleClass;

  /** Variable qui loge si l'utilisateur a accès au bloc ou non.
   * @since SOFI 2.0.1
   **/
  private String varSecurise;

  /**
   * Variable qui loge si l'utilisateur a accès au bloc en lecture seulement ou pas.
   * @since SOFI 2.0.1
   **/
  private String varSecuriseLectureSeulement;

  /**
   * La classe qui permet de faire appliquer de la sécurité applicative
   * personnalisé sur le fieldset.
   * @since SOFI 2.0.4
   */
  private String classeSecuriteApplicative;

  /** Constructeur par défaut */
  public FieldsetTag() {
  }

  /**
   * Fixer le titre de l'ensemble valeur et libellé.
   * <p>
   * @param titre le titre de l'ensemble valeur et libellé
   */
  public void setTitre(String titre) {
    this.titre = titre;
  }

  /**
   * Obtenir le titre de l'ensemble valeur et libellé.
   * <p>
   * @return le titre de l'ensemble valeur et libellé
   */
  public String getTitre() {
    return titre;
  }

  /**
   * Génération de la balises de saisie.
   * <p>
   * Cette balise est utilisée pour afficher un libellé suivi d'une valeur JSTL provenant du langage EL.
   * Voici un exemple de résultat de cette balise :<BR><BR>
   * <i><b>Message : Ceci est mon message.</b></i><BR><BR>
   * La balise qui aurait pu générer ce résultat est la suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libellé="Message" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * Par défaut, cette balise s'affiche sur une seule ligne, cependant il est possible d'en afficher
   * deux par page avec l'utilisation des attributs <code>ouvrirLigne</code> et <code>fermerLigne</code>.
   * Cette balise supporte aussi l'utilisation de libellés dynamiques. Ainsi l'exemple plus haut
   * aurait aussi pu être de la forme suivante :<BR><BR>
   * <i><b>&lt;sofi:affichageEL libelle="org.sofiframework.message.erreur" valeur="${objetMessage.message}"/&gt;</b></i><BR><BR>
   * <p>
   * @exception JspException si une exception JSP est lancé.
   * @return la valeur indiquant que faire avec le traitement restant de la page.
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }
    String codeHtml = null;
    boolean lectureSeulement = false;

    boolean securise = false;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.getTitre())) {

        if (UtilitaireBaliseJSP.isAccesComposant(getClasseSecuriteApplicative(),
            pageContext)) {
          // Générer le code représentant le composant HTML
          codeHtml = this.genererCodeHtmlComposant();
          lectureSeulement =
              gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
                  this.getTitre());

          if (!lectureSeulement) {
            // Valider la sécurité applicative personnalisé.
            lectureSeulement =
                UtilitaireBaliseJSP.isAccesComposantLectureSeulement(getClasseSecuriteApplicative(),
                    pageContext);
          }
        }
      } else {
        securise = true;
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);

      // L'utilisateur peut le consulter seulement en lecture seule
      if (lectureSeulement) {
        this.pageContext.getRequest().setAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT,
            new Boolean(true));
      }

      if (getVarSecurise() != null) {
        this.pageContext.getRequest().setAttribute(getVarSecurise(),
            new Boolean(securise));
      }

      if (getVarSecuriseLectureSeulement() != null) {
        this.pageContext.getRequest().setAttribute(getVarSecuriseLectureSeulement(),
            new Boolean(lectureSeulement));
      }

      return (EVAL_BODY_INCLUDE);
    } else {
      if (getVarSecurise() != null) {
        this.pageContext.getRequest().setAttribute(getVarSecurise(),
            new Boolean(securise));
      }

      return (SKIP_BODY);
    }
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() {
    StringBuffer html = new StringBuffer("");

    html.append("<fieldset ");

    if (getStyleClass() != null) {
      html.append("class=\"");
      html.append(getStyleClass());
      html.append("\"");
    }

    html.append(">");
    html.append("<legend>" +
        UtilitaireLibelle.getLibelle(this.getTitre(), pageContext,
            null).getMessage() + "</legend>");

    return html.toString();
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
    this.styleClass = null;
    this.titre = null;
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {

    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if ((!UtilitaireBaliseJSP.isSecuriteActif(pageContext)
        || !gestionSecurite.isSecuriteActif()
        || gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur, this.getTitre()))
        && UtilitaireBaliseJSP.isAccesComposant(getClasseSecuriteApplicative(), pageContext)) {

      StringBuffer resultat = new StringBuffer("");

      // Traiter le body de la balise.
      if (bodyContent != null) {
        String valeur = bodyContent.getString();

        if (valeur == null) {
          valeur = "";
        }

        resultat.append(valeur);
      }

      resultat.append("</fieldset>");
      TagUtils.getInstance().write(pageContext, resultat.toString());
    }

    this.pageContext.getRequest().removeAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT);

    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  private void evaluerEL() throws JspException {
    try {
      String classeSecuriteApplicative =
          EvaluateurExpression.evaluerString("classeSecuriteApplicative",
              getClasseSecuriteApplicative(), this, pageContext);
      setClasseSecuriteApplicative(classeSecuriteApplicative);

      String titre =
          EvaluateurExpression.evaluerString("titre", getTitre(), this,
              pageContext);
      setTitre(titre);

      String styleClass =
          EvaluateurExpression.evaluerString("styleClass", getStyleClass(), this,
              pageContext);
      setStyleClass(styleClass);
    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Fixer la classe CSS a utilsé pour le fieldset.
   * @param styleClass la classe CSS a utilsé pour le fieldset.
   */
  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  /**
   * La classe CSS a utilsé pour le fieldset.
   * @return classe CSS a utilsé pour le fieldset.
   */
  public String getStyleClass() {
    return styleClass;
  }

  /**
   * Fixer la variable qui loge  (boolean) si l'utilisateur a accès au bloc ou non.
   * @param varSecurise la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public void setVarSecurise(String varSecurise) {
    this.varSecurise = varSecurise;
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc ou non.
   * @return la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public String getVarSecurise() {
    return varSecurise;
  }

  /**
   * Fixer la variable qui loge (boolean) si l'utilisateur a accès au bloc en lecture seulement ou pas
   * @param varSecuriseLectureSeulement
   */
  public void setVarSecuriseLectureSeulement(String varSecuriseLectureSeulement) {
    this.varSecuriseLectureSeulement = varSecuriseLectureSeulement;
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc en lecture seulement ou pas.
   * @return la variable qui loge si l'utilisateur a accès au bloc en lecture seulement ou pas.
   */
  public String getVarSecuriseLectureSeulement() {
    return varSecuriseLectureSeulement;
  }

  /**
   * Permet de fixer la classe qui permet d'appliquer de la sécurité
   * applicative sur le fielset en traitement.
   * @param classeSecuriteApplicative  la classe qui permet d'appliquer de la sécurité
   * applicative sur le fielset en traitement.
   * @since SOFI 2.0.4
   */
  public void setClasseSecuriteApplicative(String classeSecuriteApplicative) {
    this.classeSecuriteApplicative = classeSecuriteApplicative;
  }

  /**
   * Retourne la classe qui permet d'appliquer de la sécurité
   * applicative sur le fielset en traitement.
   * @return la classe qui permet d'appliquer de la sécurité
   * applicative sur le fielset en traitement.
   * @since SOFI 2.0.4
   */
  public String getClasseSecuriteApplicative() {
    return classeSecuriteApplicative;
  }
}
