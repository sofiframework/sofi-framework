/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.base;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.html.BaseFormulaireTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise permet d'affiche un libellé.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class LibelleTag extends BaseMessageTag {
  /**
   * 
   */
  private static final long serialVersionUID = 7414856058083031287L;

  /**
   * Spécifie l'aide contextuelle sur le libellé.
   */
  protected String title;

  /**
   * Spéficie si le libellé représente un attribut obligatoire
   */
  protected boolean obligatoire;

  /**
   * Spécifie le nom de la liste, si le libellé fait référence à un liste d'objet (formulaire) imbriqués.
   */
  protected String nomListe;

  /**
   * Spécifie le nom de l'attribut qui correspond au libellé.
   */
  protected String nomAttribut;

  /**
   * styleClass.
   */
  protected String styleClass;

  /**
   * Spécifier l'aide contextuelle seulement.
   */
  protected boolean aideContextuelleSeulement = false;

  /**
   * Spécifier si le libellé est associé à un champ multibox.
   */
  protected boolean libelleMultibox = false;

  /**
   * Spécifie si vous désirez une icone d'aide après le libellé.
   */
  protected String iconeAide = null;

  /**
   * Spécifie si vous désirez que l'icone d'aide soit position à gauche du libellé.
   */
  protected boolean iconeAideAGauche = false;

  /**
   * Faire le focus sur le libellé si message d'erreur.
   */
  protected boolean focusSiErreur = false;

  /** Affichage de l'indicateur obligatoire du champ de saisie. **/
  protected String affichageIndObligatoire = null;

  /** Spécifier un suffixe à lq correspondance de l'identifiant (id) à associer **/
  protected String suffixeId;

  /** Constructeur par défaut */
  public LibelleTag() {
  }

  /**
   * Obtenir l'aide contextuelle sur le libellé.
   * 
   * @return l'aide contextuelle sur le libellé
   */
  public String getTitle() {
    return title;
  }

  /**
   * Fixer l'aide contextuelle sur le libellé.
   * 
   * @param newTitle
   *          l'aide contextuelle sur le libellé
   */
  public void setTitle(String newTitle) {
    title = newTitle;
  }

  /**
   * Obtenir si le libellé représente un attribut obligatoire.
   * 
   * @return si le libellé représente un attribut obligatoire
   */
  public boolean getObligatoire() {
    return obligatoire;
  }

  /**
   * Fixer si le libellé représente un attribut obligatoire.
   * 
   * @param newObligatoire
   *          si le libellé représente un attribut obligatoire
   */
  public void setObligatoire(boolean newObligatoire) {
    obligatoire = newObligatoire;
  }

  /**
   * Obtenir le nom de la liste, si le libellé fait référence à un liste d'objet (formulaire) imbriqués.
   * 
   * @return liste
   */
  public String getNomListe() {
    return nomListe;
  }

  /**
   * Fixer le nom de la liste, si le libellé fait référence à un liste d'objet (formulaire) imbriqués.
   * 
   * @param newNomListe
   *          liste
   */
  public void setNomListe(String newNomListe) {
    nomListe = newNomListe;
  }

  /**
   * Fixer le nom du formulaire imbrique, si le libellé fait référence à un liste d'objet (formulaire) imbriqués.
   * 
   * @param nomFormulaireImbrique
   *          liste
   */
  public void setNomFormulaireImbrique(String nomFormulaireImbrique) {
    nomListe = nomFormulaireImbrique;
  }

  /**
   * Obtenir le nom de l'attribut qui correspond au libellé.
   * 
   * @return le nom de l'attribut qui correspond au libellé
   */
  public String getNomAttribut() {
    return nomAttribut;
  }

  /**
   * Fixer le nom de l'attribut qui correspond au libellé.
   * 
   * @param newAttribut
   *          le nom de l'attribut qui correspond au libellé
   */
  public void setNomAttribut(String newAttribut) {
    nomAttribut = newAttribut;
  }

  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    /**
     * Si paramètre la propriété iconeAide est null et qu'il existe un paramètre système, alors prendre la valeur de
     * celui-ci.
     * 
     * @since SOFI 2.0.3
     */
    if (this.iconeAide == null) {
      Boolean iconeAideDefaut = GestionParametreSysteme.getInstance().getBoolean(
          ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE);

      if (iconeAideDefaut != null) {
        setIconeAide(iconeAideDefaut.toString());
      }
    }

    return SKIP_BODY;
  }

  /**
   * Execute le début de la balise.
   * <p>
   * 
   * @exception JspException
   *              si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    Object[] parametres = new Object[5];
    parametres[0] = getParametre1();
    parametres[1] = getParametre2();
    parametres[2] = getParametre3();
    parametres[3] = getParametre4();
    parametres[4] = getParametre5();

    StringBuffer resultat = new StringBuffer();

    if (isIconeAideAGauche()
        && (isAfficherIconeAide() || ((getIconeAide() == null) && UtilitaireBaliseJSP.isAideEnligne(getIdentifiant(),
            pageContext)))) {
      UtilitaireBaliseJSP.genererIcodeAide(getIdentifiant(), getNomAttribut(), resultat, pageContext);
      resultat.append("&nbsp;");
    }

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(this.pageContext);

    String referenceFormulaireImbrique = UtilitaireBaliseNested.getReferenceFormulaireImbrique(pageContext, null);

    if (getObligatoire()) {
      formulaire.ajouterAttributObligatoire(referenceFormulaireImbrique, getNomAttribut());
    }

    StringBuffer nomAttributComplet = new StringBuffer();

    if ((getNomListe() == null) && (referenceFormulaireImbrique != null)) {
      nomAttributComplet.append(referenceFormulaireImbrique);
      nomAttributComplet.append(".");

      nomAttributComplet.append(getNomAttribut());
      setNomAttribut(nomAttributComplet.toString());
    } else {
      if (getNomListe() != null) {
        if (referenceFormulaireImbrique != null) {
          referenceFormulaireImbrique = referenceFormulaireImbrique + "." + getNomListe();
        } else {
          referenceFormulaireImbrique = getNomListe();
        }
      }
    }

    MessageErreur erreur = null;

    if (!UtilitaireString.isVide(getNomAttribut())) {
      if (UtilitaireString.isVide(getNomListe())) {
        erreur = UtilitaireBaliseJSP.getMessageErreur(formulaire, getNomAttribut(), this.pageContext);
      } else {
        erreur = formulaire.getMessageErreur(this.pageContext.getSession(), referenceFormulaireImbrique,
            getNomAttribut());
      }

      if (erreur != null) {
        this.setStyleClass(BaseFormulaireTag.STYLE_LIBELLE_ERREUR);

        if (!GestionParametreSysteme.getInstance()
            .getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_SUR_LIBELLE_ERREUR).booleanValue()) {
          setTitle("");
        } else {
          this.setTitle(erreur.getMessage());
        }
      } else {
        if ((getStyleClass() != null) && getStyleClass().equals(BaseFormulaireTag.STYLE_LIBELLE_ERREUR)) {
          this.setStyleClass(null);
        }
      }
    }

    if (isAfficherIconeAide() && !UtilitaireBaliseJSP.isAideEnligne(getIdentifiant(), pageContext)) {
      setTitle("");
    }

    if (getAideContextuelleSeulement() && !isAfficherIconeAide()) {
      this.title = getIdentifiant();
      setIdentifiant(null);
    }

    int noLigneSelection = 0;
    String nomAttributFocus = getNomAttribut();

    if ((erreur != null) && (erreur.getNomListe() != null)) {
      noLigneSelection = -1;
      nomAttributFocus = erreur.getFocus();
    }

    if (getNomListe() == null) {
      noLigneSelection = -1;
    }

    if (isLibelleMultibox()) {
      Integer no = UtilitaireBaliseJSP.getCompteurBoiteACocherMultibox(getNomAttribut(), false, pageContext);

      if (no != null) {
        nomAttributFocus = nomAttributFocus + no.toString();
      }
    }

    if (getSuffixeId() != null) {
      nomAttributFocus = nomAttributFocus + getSuffixeId();
    }

    if (getVar() == null) {
      if (!getAideContextuelleSeulement()) {
        if (erreur != null) {
          if (isFocusSiErreur()) {
            resultat.append(UtilitaireBaliseJSP.genererChampInvisiblePourFocus(getNomAttribut(), erreur.getMessage()));
          }
        }

        if (!UtilitaireString.isVide(getNomListe()) || (referenceFormulaireImbrique != null)) {

          // Si le libellé est en relation avec une liste imbriqués d'objets (NESTED)
          UtilitaireBaliseJSP.genererLibelle(referenceFormulaireImbrique, nomAttributFocus, noLigneSelection,
              this.title, getIdentifiant(), getStyleClass(), this.obligatoire, isAffichageIndObligatoire(), parametres,
              getCodeClient(), this.pageContext, resultat);
        } else {
          UtilitaireBaliseJSP.genererLibelle(nomAttributFocus, this.title, getIdentifiant(), getStyleClass(),
              this.obligatoire, isAffichageIndObligatoire(), parametres, getCodeClient(), this.pageContext, resultat);
        }

        if (!isIconeAideAGauche()
            && (isAfficherIconeAide() || ((getIconeAide() == null) && UtilitaireBaliseJSP.isAideEnligne(
                getIdentifiant(), pageContext)))) {
          UtilitaireBaliseJSP.genererIcodeAide(getIdentifiant(), getNomAttribut(), resultat, pageContext);
        }
      } else {
        UtilitaireBaliseJSP.genererIcodeAide(getIdentifiant(), getNomAttribut(), resultat, pageContext);
      }

      // Ecrire la fonction javascript dans la sortie en écriture
      TagUtils.getInstance().write(pageContext, resultat.toString());
    } else {
      if (isIconeAideAGauche()
          && (isAfficherIconeAide() || ((getIconeAide() == null) && UtilitaireBaliseJSP.isAideEnligne(getIdentifiant(),
              pageContext)))) {
        UtilitaireBaliseJSP.genererIcodeAide(getIdentifiant(), getNomAttribut(), resultat, pageContext);
      }

      Libelle libelle = UtilitaireLibelle.getLibelle(getIdentifiant(), pageContext, parametres, getCodeClient());

      resultat.append(libelle.getMessage());

      if (!isIconeAideAGauche()
          && (isAfficherIconeAide() || ((getIconeAide() == null) && UtilitaireBaliseJSP.isAideEnligne(getIdentifiant(),
              pageContext)))) {
        UtilitaireBaliseJSP.genererIcodeAide(getIdentifiant(), getNomAttribut(), resultat, pageContext);
      }

      this.pageContext.getRequest().setAttribute(getVar(), resultat.toString());
    }

    setNomAttribut(null);

    return (EVAL_PAGE);
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    this.title = null;
  }

  /**
   * Retourne le style de feuille de style du libellé
   * 
   * @param le
   *          style de feuille de style du libellé
   */
  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  /**
   * Retourne le style de feuille de style du libellé
   * 
   * @return le style de feuille de style du libellé
   */
  public String getStyleClass() {
    return styleClass;
  }

  /**
   * Retourne si la balise est pour seulement affiché un aide contextuelle
   * 
   * @return true si si la balise est pour seulement affiché un aide contextuelle
   */
  public boolean getAideContextuelleSeulement() {
    return aideContextuelleSeulement;
  }

  /**
   * Fixer si la balise est pour seulement affiché un aide contextuelle
   * 
   * @param aideContextuelleSeulement
   *          true si si la balise est pour seulement affiché un aide contextuelle
   */
  public void setAideContextuelleSeulement(boolean aideContextuelleSeulement) {
    this.aideContextuelleSeulement = aideContextuelleSeulement;
  }

  /**
   * Fixer l'aide contextuelle sur le libellé.
   * 
   * @param newTitle
   *          l'aide contextuelle sur le libellé
   */
  public void setAideContextuelle(String aideContextuelle) {
    setTitle(aideContextuelle);
  }

  /**
   * Obtenir l'aide contextuelle sur le libellé.
   * 
   * @return l'aide contextuelle sur le libellé
   */
  public String getAideContextuelle() {
    return title;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * 
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public void evaluerEL() throws JspException {
    super.evaluerEL();

    String styleClass = EvaluateurExpression.evaluerString("styleClass", getStyleClass(), this, pageContext);
    setStyleClass(styleClass);

    if (getAffichageIndObligatoire() != null) {
      String affichageObligatoire = EvaluateurExpression.evaluerString("affichageIndObligatoire",
          String.valueOf(getAffichageIndObligatoire()), this, pageContext);
      setAffichageIndObligatoire(affichageObligatoire);
    }

    String nomAttribut = EvaluateurExpression.evaluerString("nomAttribut", getNomAttribut(), this, pageContext);
    setNomAttribut(nomAttribut);

    String nomListe = EvaluateurExpression.evaluerString("nomListe", getNomListe(), this, pageContext);
    setNomListe(nomListe);

    String aideContextuelle = EvaluateurExpression.evaluerString("aideContextuelle", getAideContextuelle(), this,
        pageContext);
    setAideContextuelle(aideContextuelle);
  }

  public void setLibelleMultibox(boolean libelleMultibox) {
    this.libelleMultibox = libelleMultibox;
  }

  public boolean isLibelleMultibox() {
    return libelleMultibox;
  }

  public void setIconeAide(String iconeAide) {
    this.iconeAide = iconeAide;
  }

  public String getIconeAide() {
    return iconeAide;
  }

  public boolean isAfficherIconeAide() {
    if ((getIconeAide() != null) && getIconeAide().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setFocusSiErreur(boolean focusSiErreur) {
    this.focusSiErreur = focusSiErreur;
  }

  public boolean isFocusSiErreur() {
    return focusSiErreur;
  }

  /**
   * Fixer true si on veut l'affichage de l'indicateur obligatoire.
   * 
   * @param affichageIndObligatoire
   *          true si on veut l'affichage de l'indicateur obligatoire.
   */
  public void setAffichageIndObligatoire(String affichageIndObligatoire) {
    this.affichageIndObligatoire = affichageIndObligatoire;
  }

  /**
   * Retourne true si on veut l'affichage de l'indicateur obligatoire.
   * 
   * @return true si on veut l'affichage de l'indicateur obligatoire.
   */
  public String getAffichageIndObligatoire() {
    return affichageIndObligatoire;
  }

  /**
   * Est-ce que l'affichage de l'indicateur obligatoire est requis.
   */
  private boolean isAffichageIndObligatoire() {
    if ((getAffichageIndObligatoire() != null) && getAffichageIndObligatoire().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setSuffixeId(String suffixeId) {
    this.suffixeId = suffixeId;
  }

  public String getSuffixeId() {
    return suffixeId;
  }

  public void setIconeAideAGauche(boolean iconeAideAGauche) {
    this.iconeAideAGauche = iconeAideAGauche;
  }

  public boolean isIconeAideAGauche() {
    return iconeAideAGauche;
  }

}
