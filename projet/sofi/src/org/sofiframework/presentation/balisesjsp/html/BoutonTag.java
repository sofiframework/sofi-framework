/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise JSP représentant un bouton qui ne possède qu'un libellé ainsi qu'un lien href.
 * <p>
 * On peut spécifier facultativement un message de confirmation avant le traitement
 * appelé par le bouton.
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.0
 */
public class BoutonTag extends BaseBoutonTag {
  /**
   * 
   */
  private static final long serialVersionUID = -6061069601993475019L;

  /** Constructeur par défaut */
  public BoutonTag() {
  }

  /**
   * Exécute le début de la balise.
   * <p>
   * @throws JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    super.doStartTag();

    return doEndTag();
  }

  /**
   * Traiter la fermeture de la balise.
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    // rien faire..
    return super.doEndTag();
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected StringBuffer genererCodeHtmlComposant() throws JspException {
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    StringBuffer resultat = new StringBuffer();

    String aideContextuelle = null;
    Libelle libelleAide = null;
    Libelle libelleAffichage = null;
    Message libelleConfirmation = null;

    try {
      // Prendre le bon libellé associé
      libelleAffichage = UtilitaireLibelle.getLibelle(getLibelle(), pageContext);

      if (this.getAideContextuelle() != null) {
        libelleAide = UtilitaireLibelle.getLibelle(this.getAideContextuelle(),
            pageContext);
        aideContextuelle = libelleAide.getMessage();
      } else {
        // Vérifier si le libellé d'aide se trouve dans le libellé d'affichage
        if ((libelleAffichage != null) &&
            (libelleAffichage.getAideContextuelle() != null)) {
          aideContextuelle = libelleAffichage.getAideContextuelle();
        }
      }

      if (getMessageConfirmation() != null) {
        libelleConfirmation = UtilitaireMessage.get(this.getMessageConfirmation(),
            null, pageContext);
      }
    } catch (Exception e) {
      throw new JspException(e);
    }

    if (src != null) {
      resultat.append("<input type=\"image\"");
      resultat.append(" src=\"");
      resultat.append(src);
      resultat.append("\"");
      // L'attribut BORDER de la balise IMG est DEPRECATED selon le W3C.
      // La gestion du border s'effectue par CSS. Le plus simple étant de mettre
      // img{border:none;} dans sofi.css
      // REF: http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.7.3
      //      resultat.append(" border=\"0\"");
    } else {
      resultat.append("<button type=\"button\" ");
      resultat.append("value=\"");
      resultat.append(libelleAffichage.getMessage());
      resultat.append("\"");
    }

    if ((aideContextuelle != null) && (aideContextuelle.trim().length() > 0)) {
      resultat.append(" title=\"");
      resultat.append(aideContextuelle);
      resultat.append("\"");
    }

    if (this.property != null) {
      resultat.append(" name=\"");
      resultat.append(property);
      resultat.append("\"");
    }

    if (this.accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(this.accesskey);
      resultat.append("\"");
    }

    if (this.tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(this.tabindex);
      resultat.append("\"");
    }

    if (this.id != null) {
      resultat.append(" id=\"");
      resultat.append(this.id);
      resultat.append("\"");
    }

    // Le code suivant génère 2 fois l'attribut DISABLED lorsque
    // getDisabled() = true, car un de ses parents gère déjà le contenu de
    // l'attribut DISABLED selon la valeur de getDisabled().
    // On ajoute donc l'attribut DISABLED seulement si getReadonly() = true et
    // que getDisabled() = false.
    // Faire la gestion de l'affichage du composant
    //    if (this.getDisabled() || this.getReadonly()) {
    //      resultat.append(" disabled=\"true\"");
    //    }
    if (this.getReadonly() && (!this.getDisabled())) {
      // Le W3C recommande de ne pas utiliser des valeurs booléennes pour les attributs.
      // REF: http://www.w3.org/TR/xhtml1/#C_10
      // Le W3C suggère d'utiliser le nom de l'attribut comme valeur.
      // La présence de l'attribut implique TRUE alors que son absence implique FALSE.
      // REF: http://www.w3.org/TR/xhtml1/#h-4.5
      resultat.append(" disabled=\"disabled\"");
    }

    StringBuffer inactiverBouton = new StringBuffer();

    if (UtilitaireString.isVide(nomFormulaire)) {
      setInactiverBoutons(false);
    }

    inactiverBouton.append("inactiverBouton(document.");
    inactiverBouton.append(nomFormulaire);
    inactiverBouton.append(", false);");

    StringBuffer onClickBuffer = new StringBuffer();

    if ((getHref() != null) && (getHref().trim().length() > 0)) {
      if (getOnclick() != null) {
        onClickBuffer.append(getOnclick());
      }

      if (libelleConfirmation != null) {
        if (isActiverConfirmationModification() && activerModeTransactionnel()) {
          onClickBuffer.append(
              "if (document.forms[0].indModification.value == '0' || ( document.forms[0].indModification.value == '1' && confirm('");
        } else {
          onClickBuffer.append("if (confirm('");
        }

        onClickBuffer.append(UtilitaireString.convertirEnJavaScript(
            libelleConfirmation.getMessage()));

        if (isActiverConfirmationModification() && activerModeTransactionnel()) {
          onClickBuffer.append("'))) {");
        } else {
          onClickBuffer.append("')) {");
        }
      }

      if (!isHrefJS()) {
        onClickBuffer.append("document.location.href = '");
      }

      onClickBuffer.append(getHref());

      if (!isHrefJS()) {
        if (isNotifierModificationFormulaire()) {
          onClickBuffer.append("&indModification=1");
        }

        onClickBuffer.append("';");
      }

      if (isActiverFonctionInactiverBoutons()) {
        onClickBuffer.append(inactiverBouton);
      }

      if (libelleConfirmation != null) {
        onClickBuffer.append("};");
      }

      onClickBuffer.append(" return false;");
      setOnclick(onClickBuffer.toString());
    }

    return resultat;
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
  }
}
