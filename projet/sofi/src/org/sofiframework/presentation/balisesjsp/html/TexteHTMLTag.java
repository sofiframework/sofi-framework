/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;


/**
 * Pour plus d'information sur le TinyMCE
 * http://tinymce.moxiecode.com/tinymce/
 *
 * Pour avoir la liste des boutons que l'on peut ajouter
 * http://tinymce.moxiecode.com/tinymce/docs/reference_buttons.html
 */
public class TexteHTMLTag extends TextareaTag {
  /**
   * 
   */
  private static final long serialVersionUID = -6052662723600742204L;

  /** Spécifie le langage de l'éditeur. */
  protected String langage = "fr_ca";

  /** Spécifie le theme à utiliser pour l'éditeur. */
  protected String theme = "advanced";

  /** Spécifie la location de la barre de menu. */
  protected String locationMenu = "top";

  /** Spécifie l'alignement de la barre de menu. */
  protected String alignementMenu = null;

  /** Spécifie les boutons contenus dans la ligne 1 de la barre de menu. */
  protected String boutonsMenuLigne1 = "bold,italic,underline,strikethrough,forecolor,separator,link,unlink,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo";

  /** Spécifie les boutons contenus dans la ligne 2 de la barre de menu. */
  protected String boutonsMenuLigne2 = "";

  /** Spécifie les boutons contenus dans la ligne 3 de la barre de menu. */
  protected String boutonsMenuLigne3 = "";

  /** Spécifie les options d'initialisation supplémentaires de l'éditeur. */
  protected String optionsSupplementaires = null;

  /** Spécifie les valeurs des options d'initialisation supplémentaires de l'éditeur. */
  protected String optionsSupplementairesValeurs = null;

  /** Permet d'ajouter une deuxième ligne de boutons par défaut */
  protected boolean afficherBoutonsMenuLigne2ParDefaut = false;
  protected String fichierCss;

  /**
   * Fixer le langage de l'éditeur.
   * <p>
   * @param langage Le langage de l'éditeur
   */
  public void setLangage(String langage) {
    this.langage = langage;
  }

  /**
   * Obtenir le langage de l'éditeur.
   * <p>
   * @return Le langage de l'éditeur
   */
  public String getLangage() {
    return langage;
  }

  /**
   * Fixer le theme à utiliser pour l'éditeur.
   * <p>
   * @param theme Le theme à utiliser pour l'éditeur
   */
  public void setTheme(String theme) {
    this.theme = theme;
  }

  /**
   * Obtenir le theme à utiliser pour l'éditeur.
   * <p>
   * @return Le theme à utiliser pour l'éditeur
   */
  public String getTheme() {
    return theme;
  }

  /**
   * Fixer la location de la barre de menu.
   * <p>
   * @param locationMenu La location de la barre de menu
   */
  public void setLocationMenu(String locationMenu) {
    this.locationMenu = locationMenu;
  }

  /**
   * Obtenir la location de la barre de menu.
   * <p>
   * @return La location de la barre de menu
   */
  public String getLocationMenu() {
    return locationMenu;
  }

  /**
   * Fixer l'alignement de la barre de menu.
   * <p>
   * @param alignementMenu L'alignement de la barre de menu
   */
  public void setAlignementMenu(String alignementMenu) {
    this.alignementMenu = alignementMenu;
  }

  /**
   * Obtenir l'alignement de la barre de menu.
   * <p>
   * @return L'alignement de la barre de menu
   */
  public String getAlignementMenu() {
    return alignementMenu;
  }

  /**
   * Fixer les boutons de la ligne 1 de la barre de menu.
   * <p>
   * @param boutonsMenuLigne1 Les boutons de la première ligne de la barre de menu
   */
  public void setBoutonsMenuLigne1(String boutonsMenuLigne1) {
    this.boutonsMenuLigne1 = boutonsMenuLigne1;
  }

  /**
   * Obtenir les boutons de la ligne 1 de la barre de menu.
   * <p>
   * @return les boutons de la ligne 1 de la barre de menu
   */
  public String getBoutonsMenuLigne1() {
    return boutonsMenuLigne1;
  }

  /**
   * Fixer les boutons de la ligne 2 de la barre de menu.
   * <p>
   * @param boutonsMenuLigne2 Les boutons de la deuxième ligne de la barre de menu
   */
  public void setBoutonsMenuLigne2(String boutonsMenuLigne2) {
    this.boutonsMenuLigne2 = boutonsMenuLigne2;
  }

  /**
   * Obtenir les boutons de la ligne 2 de la barre de menu.
   * <p>
   * @return les boutons de la ligne 2 de la barre de menu
   */
  public String getBoutonsMenuLigne2() {
    return boutonsMenuLigne2;
  }

  /**
   * Fixer les boutons de la ligne 3 de la barre de menu.
   * <p>
   * @param boutonsMenuLigne3 Les boutons de la troisième ligne de la barre de menu
   */
  public void setBoutonsMenuLigne3(String boutonsMenuLigne3) {
    this.boutonsMenuLigne3 = boutonsMenuLigne3;
  }

  /**
   * Obtenir les boutons de la ligne 3 de la barre de menu.
   * <p>
   * @return les boutons de la ligne 3 de la barre de menu
   */
  public String getBoutonsMenuLigne3() {
    return boutonsMenuLigne3;
  }

  /**
   * Fixer les options d'initialisation supplémentaires de l'éditeur.
   * S'il y a plus d'une option, elles doivent être séparées par une virgule.
   * <p>
   * @param optionsSupplementaires Les options d'initialisation supplémentaires
   */
  public void setOptionsSupplementaires(String optionsSupplementaires) {
    this.optionsSupplementaires = optionsSupplementaires;
  }

  /**
   * Obtenir les options d'initialisation supplémentaires de l'éditeur.
   * S'il y a plus d'une option, elles sont séparées par une virgule.
   * <p>
   * @return Les options d'initialisation supplémentaires
   */
  public String getOptionsSupplementaires() {
    return optionsSupplementaires;
  }

  /**
   * Fixer la valeur des options d'initialisation supplémentaires de l'éditeur.
   * S'il y a plus d'une valeur, elles doivent être séparées par une virgule.
   * <p>
   * @param optionsSupplementairesValeurs La valeur des options d'initialisation supplémentaires de l'éditeur
   */
  public void setOptionsSupplementairesValeurs(
      String optionsSupplementairesValeurs) {
    this.optionsSupplementairesValeurs = optionsSupplementairesValeurs;
  }

  /**
   * Obtenir la valeur des options d'initialisation supplémentaires de l'éditeur.
   * S'il y a plus d'une valeur, elles sont séparées par une virgule.
   * <p>
   * @return La valeur des options d'initialisation supplémentaires de l'éditeur
   */
  public String getOptionsSupplementairesValeurs() {
    return optionsSupplementairesValeurs;
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un objet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected String genererCodeHtmlComposant() throws JspException {
    // Initialiser la ligne avec des options par défaut
    if (isAfficherBoutonsMenuLigne2ParDefaut()) {
      this.boutonsMenuLigne2 = "image,code,hr,removeformat,sub,sup,backcolor,charmap,visualaid,anchor,separator";
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
        property);

    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
    String contextPath = request.getContextPath();

    StringBuffer resultat = new StringBuffer("");

    // Inclusion des fichers javascript pour l'éditeur
    resultat.append(
        "\n<!-- Importation et initialisation pour l'éditeur HTML -->");
    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    //    resultat.append("\n<script language=\"javascript\" type=\"text/javascript\" src=\"");
    resultat.append("\n<script type=\"text/javascript\" src=\"");
    resultat.append(contextPath);
    resultat.append("/script/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>");
    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    //    resultat.append("\n<script language=\"javascript\" type=\"text/javascript\" src=\"");
    resultat.append("\n<script type=\"text/javascript\" src=\"");
    resultat.append(contextPath);
    resultat.append(
        "/script/tinymce/jscripts/tiny_mce/langs/fr_ca.js\"></script>");

    // Inclusion de la fonction indiquant la modification du formulaire
    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    //    resultat.append("\n<script language=\"javascript\" type=\"text/javascript\">function modifierEtatFormulaire(inst) {");
    resultat.append("\n<script type=\"text/javascript\">function modifierEtatFormulaire(inst) {");
    resultat.append(UtilitaireBaliseJSP.genererNotifierFormulaire(
        nomFormulaireJS));
    resultat.append("}</script>");

    // Inclusion de la fonction d'initialisation de l'éditeur avec les options spécifiées
    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    //    resultat.append("\n<script language=\"javascript\" type=\"text/javascript\">");
    resultat.append("\n<script type=\"text/javascript\">");
    resultat.append("\ntinyMCE.init({");

    resultat.append("\n\tlanguage : \"");
    resultat.append(getLangage());
    resultat.append("\",");

    resultat.append("\n\tmode : \"exact\",");
    resultat.append("\n\telements : \"");
    resultat.append(property);
    resultat.append("\",");

    resultat.append("\n\ttheme : \"");
    resultat.append(getTheme());
    resultat.append("\",");

    resultat.append("\n\ttheme_");
    resultat.append(getTheme());
    resultat.append("_layout_manager : \"SimpleLayout\",");

    if (this.getLocationMenu() != null) {
      resultat.append("\n\ttheme_");
      resultat.append(getTheme());
      resultat.append("_toolbar_location : \"");
      resultat.append(getLocationMenu());
      resultat.append("\",");
    }

    if (this.getAlignementMenu() != null) {
      resultat.append("\n\ttheme_");
      resultat.append(getTheme());
      resultat.append("_toolbar_align : \"");
      resultat.append(getAlignementMenu());
      resultat.append("\",");
    }

    if (getFichierCss() != null) {
      resultat.append("\n\tcontent_css : \"");
      resultat.append(getFichierCss());
      resultat.append("\",");
      setBoutonsMenuLigne2(getBoutonsMenuLigne2() + ",styleselect");
    } else {
      setBoutonsMenuLigne2(getBoutonsMenuLigne2() +
          ",fontselect,fontsizeselect");
    }

    if (getBoutonsMenuLigne1() != null) {
      resultat.append("\n\ttheme_");
      resultat.append(getTheme());
      resultat.append("_buttons1 : \"");
      resultat.append(getBoutonsMenuLigne1());
      resultat.append("\",");
    }

    if (getBoutonsMenuLigne2() != null) {
      resultat.append("\n\ttheme_");
      resultat.append(getTheme());
      resultat.append("_buttons2 : \"");
      resultat.append(getBoutonsMenuLigne2());
      resultat.append("\",");
    }

    if (getBoutonsMenuLigne3() != null) {
      resultat.append("\n\ttheme_");
      resultat.append(getTheme());
      resultat.append("_buttons3 : \"");
      resultat.append(getBoutonsMenuLigne3());
      resultat.append("\",");
      resultat.append("\n\tplugins : \"table\",");
    }

    // Traiter si optionsSupplementaires et optionsSupplementairesValeurs sont spécifié
    if (getOptionsSupplementaires() != null) {
      StringTokenizer listeOptions = new StringTokenizer(getOptionsSupplementaires(),
          ",");
      StringTokenizer listeValeurs = new StringTokenizer(getOptionsSupplementairesValeurs(),
          ",");

      if (listeOptions.countTokens() != listeValeurs.countTokens()) {
        throw new SOFIException(
            "ERREUR Balise listeValeurs : Le nombre d'options supplémentaires (optionsSupplementaires) doit être égale au nombre de valeurs (optionsSupplementairesValeurs)");
      }

      while (listeOptions.hasMoreTokens()) {
        String option = listeOptions.nextToken();
        String valeur = listeValeurs.nextToken();

        resultat.append("\n\t");
        resultat.append(option);
        resultat.append(" : \"");
        resultat.append(valeur);
        resultat.append("\",");
      }
    }

    //resultat.append("\n\tdocument_base_url : \"").append(contextPath).append("\",\n");
    resultat.append(
        "\n\tonchange_callback : \"modifierEtatFormulaire\"});\n</script>\n\n");

    // Générer le code Html du textarea
    resultat.append(super.genererCodeHtmlComposant());

    return resultat.toString();
  }

  /**
   * Fixer si l'on désire une deux^ème ligne de boutons
   * @param afficherBoutonsMenuLigne2ParDefaut si l'on désire les boutons
   */
  public void setAfficherBoutonsMenuLigne2ParDefaut(
      boolean afficherBoutonsMenuLigne2ParDefaut) {
    this.afficherBoutonsMenuLigne2ParDefaut = afficherBoutonsMenuLigne2ParDefaut;
  }

  /**
   * Obtenir si l'on désire une deuxième ligne de boutons par defaut.
   * @return Si l'on désire les boutons
   */
  public boolean isAfficherBoutonsMenuLigne2ParDefaut() {
    return afficherBoutonsMenuLigne2ParDefaut;
  }

  /**
   * Fixer un emplacement d'un fichier css.
   * @param fichierCss l'emplacement d'un fichier css.
   */
  public void setFichierCss(String fichierCss) {
    this.fichierCss = fichierCss;
  }

  /**
   * Retourne un emplacement d'un fichier css.
   * @return un emplacement d'un fichier css.
   */
  public String getFichierCss() {
    return fichierCss;
  }
}
