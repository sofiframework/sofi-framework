/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.Locale;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireExpressionReguliere;
import org.sofiframework.utilitaire.UtilitaireNombre;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.exception.ExpressionReguliereFormatInvalideException;

/**
 * Classe de base pour les mutiples balises type de champ de saisie.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient en lecture seulement.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.balisesjsp.html.TextTag
 * @see org.sofiframework.presentation.balisesjsp.html.PasswordTag
 */
public abstract class BaseFieldTag extends BaseFormulaireTag {
  /**
   * 
   */
  private static final long serialVersionUID = 3998957766590356754L;
  private boolean nombreDefautZero = false;
  private String nombreNegatifActif = null;
  private boolean nombreNegatif = false;
  private String placeholder = null;

  /**
   * Permet d'insérer un attribut HTML autocomplete sur le champs pour enlever la completion du base du navigateur.
   */
  private String autocomplete;

  /** Constructeur par défaut */
  public BaseFieldTag() {
    super();
  }

  /**
   * Méthode qui sert à générer le contenu de la balise à afficher.
   * <p>
   * 
   * @param resultat
   *          le code à inscrire à l'intérieur de la balise
   * @throws JspException
   *           si une exception JSP est lancé
   */
  @Override
  protected void ecrireBalise(StringBuffer resultat) throws JspException {
    String nomFormulaireJS = null;

    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    if (isTraitementTransactionnel()) {
      nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext, property);
    }

    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(this.pageContext);

    Libelle libelle = UtilitaireLibelle.getLibelle(getLibelle(), this.pageContext, null);

    if (libelle.getPlaceholder() != null && StringUtils.isEmpty(getPlaceholder())) {
      setPlaceholder(libelle.getPlaceholder());
    }

    resultat.append("<input type=\"");
    resultat.append(this.type);
    resultat.append("\" name=\"");

    if (this.indexed) {
      prepareIndex(resultat, this.name);
    }

    resultat.append(this.property);
    resultat.append("\"");

    if (this.accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(this.accesskey);
      resultat.append("\"");
    }

    if (this.accept != null) {
      resultat.append(" accept=\"");
      resultat.append(this.accept);
      resultat.append("\"");
    }

    if (getSize() != null) {
      resultat.append(" size=\"");
      resultat.append(getSize());
      resultat.append("\"");
    }

    if (this.tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(this.tabindex);
      resultat.append("\"");
    }

    if (getStyleId() == null) {
      resultat.append(" id=\"");
      resultat.append(this.property);
      resultat.append("\"");
    }

    if (StringUtils.isNotBlank(getPlaceholder())) {
      resultat.append(" placeholder=\"");
      Libelle libellePlaceHolder = UtilitaireLibelle.getLibelle(this.placeholder, pageContext, null);
      resultat.append(libellePlaceHolder.getMessage());
      resultat.append("\"");

    }

    // Si le format en nombre est désiré, fixer le format accepte.
    if (isFormatterEnNombre()) {
      if (getFormat() == null) {
        setFormat("###");
      }

      String grandeuxMax = getMaxlength();

      if (grandeuxMax == null) {
        grandeuxMax = getSize();
      }

      setMaxlength(getSize());

      StringBuffer limiteChampTexte = new StringBuffer();

      if (!getReadonly() && !getDisabled()) {
        StringBuffer formatterNombre = new StringBuffer();
        int nbDecimal = 0;

        if (getFormat().indexOf(".") != -1) {
          nbDecimal = getFormat().length() - getFormat().indexOf(".") - 1;
        }

        formatterNombre.append("formatterNombre(this,");
        formatterNombre.append(nbDecimal);
        formatterNombre.append(",");
        formatterNombre.append(isNombreDefautZero());
        formatterNombre.append(",");
        formatterNombre.append(grandeuxMax);
        formatterNombre.append(",");

        if (isNombreNegatif()) {
          formatterNombre.append("true");
        } else {
          formatterNombre.append("false");
        }

        formatterNombre.append(",'");
        formatterNombre.append(UtilitaireBaliseJSP.getLocale(pageContext).getLanguage());
        formatterNombre.append("');");
        setOnchange(formatterNombre.toString() + getOnchange());
      }

      if (getOnchange() != null) {
        setOnchange(getOnchange() + limiteChampTexte.toString());
      }

      if (getStyleClass() != null) {
        setStyleClass(getStyleClass() + " saisie_alignement_droite");
      } else {
        setStyleClass("saisie_alignement_droite");
      }

      if (getStyleClass() == null) {
        setStyleClassLectureSeulement("saisie_alignement_droite_lectureSeulement");
      }
    }

    boolean attributFormulaire = true;

    try {
      Class typeClass = UtilitaireObjet.getClasse(baseForm, getProperty());

      if (typeClass == null) {
        attributFormulaire = false;
      }
    } catch (Exception e) {
      attributFormulaire = false;
    }

    if ((baseForm != null) && baseForm.isTransactionnel() && attributFormulaire && isTraitementTransactionnel()) {
      UtilitaireBaliseJSP.specifierGestionModification(pageContext, nomFormulaire, nomFormulaireJS, getOnchange(),
          resultat);
      setOnchange(null);
    }

    String onBlur = getOnblur();
    setOnblur(null);

    // Faire la gestion du mode lecture seulement
    if (this.getDisabled() || this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.styleClassLectureSeulement, getStyleClass(), resultat);

      if (!GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.READONLY_DISABLED_DISTINCT)
          .booleanValue()) {
        setDisabled(false);
        setReadonly(true);
      }
    } else {

      UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(), getStyleClassFocus(), getOnfocus(), onBlur, resultat);
    }

    resultat.append(" value=\"");

    if (redisplay || !"password".equals(this.type)) {
      Object value = null;

      if (this.value != null) {
        resultat.append(this.value);
      } else {
        try {
          if (baseForm != null) {
            value = UtilitaireObjet.getValeurAttribut(baseForm, this.property);
          } else {
            value = TagUtils.getInstance().lookup(this.pageContext, this.name, this.property, null);
          }
        } catch (Exception e) {

        }
      }

      // Traiter si format.
      String format = getFormat();

      if ((value == null) || "".equals(value)) {
        if (getValeurDefaut() != null) {
          value = getValeurDefaut();
        } else {
          value = "";
        }
      }

      if ((format != null) && !UtilitaireString.isVide((String) value)) {
        try {
          Locale localeUtilisateur = UtilitaireSession.getLocale(pageContext.getSession());

          if ((getFormat().substring(0, 1).indexOf("^") == -1)) {
            // Traiter les format numérique.
            String valeurFormatte = UtilitaireNombre.getValeurFormate(getFormat(), value, localeUtilisateur,
                (getReadonly() || getDisabled()));

            value = valeurFormatte;
          } else {
            /**
             * @since SOFI 2.0.1 Traiter les expressions régulière pour le format de saisie.
             * @author Jean-François Brassard
             */
            try {
              value = UtilitaireExpressionReguliere.getValeurFormatte(getFormat(), getFormatSaisie(), (String) value);
            } catch (ExpressionReguliereFormatInvalideException e) {
              // Laisser la valeur saisie, car en erreur.
            }
          }
        } catch (Exception e) {
          // Conversion impossible, donc prendre la valeur saisie par l'utilisateur.
        }
      }

      resultat.append(TagUtils.getInstance().filter(value.toString()));
    }

    resultat.append("\"");

    if (this.maxlength != null) {
      resultat.append(" maxlength=\"");
      resultat.append(this.maxlength);
      resultat.append("\"");
    }

    if ("false".equals(this.getAutocomplete())) {
      resultat.append(" AUTOCOMPLETE=\"false\" ");
    }

    // Traitement Struts de base.
    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(getElementClose());
  }

  /**
   * Méthode qui sert à libérer les ressources sytème utilisées
   */
  @Override
  public void release() {
    super.release();
    setFormatterEnNombre(false);
    setPlaceholder(null);
  }

  /**
   * 
   * @param nombreDefautZero
   */
  public void setNombreDefautZero(boolean nombreDefautZero) {
    this.nombreDefautZero = nombreDefautZero;
  }

  /**
   * 
   * @return
   */
  public boolean isNombreDefautZero() {
    return nombreDefautZero;
  }

  /**
   * Évaluer les expressionn régulières
   */
  @Override
  public void evaluerEL() throws JspException {
    super.evaluerEL();

    if (getNombreNegatifActif() != null) {
      boolean nombreNegatif = EvaluateurExpression.evaluerBoolean("nombreNegatifActif", getNombreNegatifActif(), this,
          pageContext);
      setNombreNegatif(nombreNegatif);
    }
  }

  /**
   * 
   * @param nombreNegatifActif
   */
  public void setNombreNegatifActif(String nombreNegatifActif) {
    this.nombreNegatifActif = nombreNegatifActif;
  }

  /**
   * 
   * @return
   */
  public String getNombreNegatifActif() {
    return nombreNegatifActif;
  }

  /**
   * 
   * @param nombreNegatif
   */
  public void setNombreNegatif(boolean nombreNegatif) {
    this.nombreNegatif = nombreNegatif;
  }

  /**
   * 
   * @return
   */
  public boolean isNombreNegatif() {
    return nombreNegatif;
  }

  /**
   * 
   * @return
   */
  public String getAutocomplete() {
    return autocomplete;
  }

  /**
   * 
   * @param autocomplete
   */
  public void setAutocomplete(String autocomplete) {
    this.autocomplete = autocomplete;
  }

  /**
   * @param placeholder
   *          the placeholder to set
   */
  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  /**
   * @return the placeholder
   */
  public String getPlaceholder() {
    return placeholder;
  }
}
