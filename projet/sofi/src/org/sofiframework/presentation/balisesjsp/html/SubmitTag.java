/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise de présentation d'un bouton submit (forme image ou bouton).
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class SubmitTag extends BaseBoutonTag {
  /**
   * 
   */
  private static final long serialVersionUID = -101130414773367154L;
  private String fonctionApresSoumission;
  private boolean soumissionFichier = false;

  /**
   * Variable pour rendre le champ de type file obligatoire
   * en javascript et valide que le fichier est existant
   * et que le navigateur y a acces pour le telecharger
   * vers l'amont.
   *
   * NOTE. Ceci fonctionne pour formulaire avec un
   * champ seulement.
   *
   * @since 2.0.5
   */
  private String fichier;

  /**
   *
   * @since 2.0.5
   */
  private String messageFichierObligatoire;

  /**
   * @since 2.0.5
   */
  private String messageFichierErreurPasAccesOuPasExistant;


  /** Constructeur par defaut */
  public SubmitTag() {
  }

  /**
   * Exécute la fin de la balise.
   * <p>
   * @throws JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    if (!UtilitaireString.isVide(getValue())) {
      setLibelle(getValue());
    }

    if (!UtilitaireString.isVide(getValue())) {
      setAideContextuelle(getTitle());
    }

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);
    boolean affichageSeulement = false;
    if (formulaire != null) {
      affichageSeulement = formulaire.isModeAffichageSeulement();
    }

    if (!affichageSeulement) {
      super.doStartTag();
    }

    traitementFonctionApresSoumission();

    return (EVAL_PAGE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected StringBuffer genererCodeHtmlComposant() throws JspException {
    StringBuffer resultat = new StringBuffer();

    // Extraire le formulaire courant.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    Message libelleConfirmation = null;

    if (getMessageConfirmation() != null) {
      libelleConfirmation =
          UtilitaireMessage.get(this.getMessageConfirmation(), null,
              pageContext);
    }

    boolean image = false;

    if (src != null) {
      image = true;
      resultat.append("<input type=\"image\"");
      resultat.append(" src=\"");
      resultat.append(src);
      resultat.append("\"");
      // L'attribut BORDER ne fait pas parti de la balise INPUT|IMAGE selon le W3C.
      // REF: http://www.w3.org/TR/REC-html40/interact/forms.html#edef-INPUT
      // resultat.append(" border=\"0\"");
      // Le W3C recommande d'utiliser l'attribut ALT dans la balise INPUT|IMAGE.
      // REF: http://www.w3.org/TR/REC-html40/interact/forms.html#input-control-types
      resultat.append(" alt=\"Bouton soumission.\"");
    } else {
      if (!UtilitaireString.isVide(getHref())) {
        resultat.append("<button type=\"button\"");
      } else {
        resultat.append("<button type=\"submit\"");
      }
    }

    try {
      StringBuffer inactiverBouton = new StringBuffer();

      inactiverBouton.append("inactiverBouton(document.");
      inactiverBouton.append(nomFormulaire);
      if (!UtilitaireString.isVide(getHref())) {
        inactiverBouton.append(", false);");
      } else {
        inactiverBouton.append(", true);");
      }
      if ((getHref() != null) && (getHref().trim().length() > 0)) {
        String fonctionSoumissionJS = "";
        StringBuffer onClickBuffer = new StringBuffer();

        StringBuffer jsOnclickPersonnalise = (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONCLICK_PERSONALISE);
        if (jsOnclickPersonnalise != null) {
          onClickBuffer.append(jsOnclickPersonnalise);
        }

        if (isSoumissionFichier()) {
          fonctionSoumissionJS =
              genererSoumissionFormulaireTelechargement(pageContext, getHref(),
                  nomFormulaire,
                  isNotifierModificationFormulaire(),
                  image, getSrcAlt(),
                  null,
                  inactiverBouton.toString());
        } else {
          fonctionSoumissionJS =
              UtilitaireBaliseJSP.genererSoumissionFormulaire(pageContext,
                  getHref(),
                  nomFormulaire,
                  isNotifierModificationFormulaire(),
                  image,
                  getSrcAlt(),
                  null);

          if (getOnclick() != null) {
            onClickBuffer.append(getOnclick());
          }
        }


        onClickBuffer.append(fonctionSoumissionJS);

        if (libelleConfirmation != null) {
          onClickBuffer.append("if (confirm('");
          onClickBuffer.append(UtilitaireString.convertirEnJavaScript(libelleConfirmation.getMessage()));
          onClickBuffer.append("')) {");
        }

        if (!isSoumissionFichier()) {
          onClickBuffer.append(inactiverBouton);
        }

        onClickBuffer.append(pageContext.getAttribute("nomFonctionSoumissionJS"));

        onClickBuffer.append("();");

        if (libelleConfirmation != null) {
          onClickBuffer.append("};");
        }

        onClickBuffer.append("return false;");
        setOnclick(onClickBuffer.toString());
      } else {
        if (isActiverFonctionInactiverBoutons()) {
          StringBuffer onClickBuffer = new StringBuffer();

          StringBuffer jsOnclickPersonnalise = (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONCLICK_PERSONALISE);
          if (jsOnclickPersonnalise != null) {
            onClickBuffer.append(jsOnclickPersonnalise);
          }

          if (getOnclick() != null) {
            onClickBuffer.append(getOnclick());
            onClickBuffer.append(";");
          }

          onClickBuffer.append(inactiverBouton.toString());
          setOnclick(onClickBuffer.toString());
        }
      }
    } catch (Exception ex) {
      throw new JspException(ex);
    }

    try {
      Libelle libelle =
          UtilitaireLibelle.getLibelle(this.getLibelle(), pageContext, null);

      if (libelle != null) {
        this.setLibelle(libelle.getMessage());
      }

      // Traiter si aide contextuelle spécifier dans la balise.
      if (getAideContextuelle() != null) {
        Libelle aideContextuelle =
            UtilitaireLibelle.getLibelle(getAideContextuelle(), pageContext,
                null);

        if (libelle != null) {
          if (!formulaire.isFormulaireEnErreur()) {
            setAideContextuelle(aideContextuelle.getMessage());
          } else {
            setAideContextuelle(null);
          }
        }
      } else {
        // Traiter l'aide contextuelle si associer au libellé
        if ((libelle != null) && (!UtilitaireString.isVide(libelle.getAideContextuelle()))) {
          if (formulaire == null || !formulaire.isFormulaireEnErreur()) {
            setAideContextuelle(libelle.getAideContextuelle());
          } else {
            setAideContextuelle(null);
          }
        }
      }
    } catch (Exception ex) {
      throw new JspException(ex);
    }

    if (property != null) {
      resultat.append(" name=\"");
      resultat.append(property);

      if (indexed) {
        prepareIndex(resultat, null);
      }

      resultat.append("\"");
    }

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    resultat.append(" value=\"");
    resultat.append(getLibelle());
    resultat.append("\"");
    setTitle(getAideContextuelle());

    return resultat;
  }

  private String genererSoumissionFormulaireTelechargement(PageContext contexte,
      String lien,
      String nomFormulaire,
      boolean modificationFormulaire,
      boolean image,
      String imageInactive,
      String messageConfirmation,
      String inactiverBouton) throws JspException {
    HttpServletRequest request = (HttpServletRequest)contexte.getRequest();
    StringBuffer resultat = new StringBuffer();

    String noSoumissionsFormulaire =
        (String)contexte.getSession().getAttribute("noSoumissionFormulaire");

    if (noSoumissionsFormulaire == null) {
      noSoumissionsFormulaire = "1";
      UtilitaireControleur.setAttributTemporairePourAction("noSoumissionFormulaire",
          noSoumissionsFormulaire,
          request);
    } else {
      int valeurNoSoumissionFormulaire =
          Integer.valueOf(noSoumissionsFormulaire).intValue() + 1;
      UtilitaireControleur.setAttributTemporairePourAction("noSoumissionFormulaire",
          String.valueOf(valeurNoSoumissionFormulaire),
          request);
      noSoumissionsFormulaire = String.valueOf(valeurNoSoumissionFormulaire);
    }

    if (nomFormulaire == null) {
      nomFormulaire = "forms[0]";
    }

    StringBuffer nomFonctionJS = new StringBuffer();
    nomFonctionJS.append("fonctionSubmit");
    nomFonctionJS.append(noSoumissionsFormulaire);

    contexte.setAttribute("nomFonctionSoumissionJS", nomFonctionJS);

    StringBuffer soumissionFormJS = new StringBuffer();
    soumissionFormJS.append("document.");
    soumissionFormJS.append(nomFormulaire);
    soumissionFormJS.append(".action ='");
    soumissionFormJS.append(lien);
    soumissionFormJS.append("';");

    if (modificationFormulaire) {
      soumissionFormJS.append("document.");
      soumissionFormJS.append(nomFormulaire);
      soumissionFormJS.append(".indModification.value = 1;");
    }

    //resultat.append("\n<script type=\"text/javascript\">");
    if (messageConfirmation != null) {
      // Si message de confirmation, ouvrir la condition.
      resultat.append("if (confirm('");
      resultat.append(UtilitaireString.convertirEnJavaScript(messageConfirmation));
      resultat.append("')) {");
    } else {
      resultat.append("function ");
      resultat.append(nomFonctionJS);
      resultat.append("() {");
    }

    resultat.append("var champNomFichier = null;");
    resultat.append("var formulaireEnCours = ").append("document.").append(nomFormulaire).append("; ");
    resultat.append("for (var i=0;i<formulaireEnCours.elements.length && champNomFichier == null;i++){  if (formulaireEnCours.elements[i].type == 'file'){ champNomFichier = formulaireEnCours.elements[i]; }  } ");
    resultat.append("if (!champNomFichier.value.length){ ");

    Message libelleFichierObligatoire = null;

    if (getMessageFichierObligatoire() != null) {
      libelleFichierObligatoire =
          UtilitaireMessage.get(this.getMessageFichierObligatoire(), null,
              pageContext);
    }

    resultat.append("alert('" +
        UtilitaireString.convertirEnJavaScript(libelleFichierObligatoire.getMessage()) +
        "'); ");
    resultat.append("return false; ");
    resultat.append("}else{ ");

    resultat.append("try{ ");

    resultat.append(getOnclick());

    if (image) {
      resultat.append(soumissionFormJS);
      resultat.append("    setTimeout('");
      resultat.append("document.").append(nomFormulaire).append(".submit();', 10);");
    } else {
      resultat.append(soumissionFormJS);
      resultat.append("document.");
      resultat.append(nomFormulaire);
      resultat.append(".submit();");
    }

    if (isActiverFonctionInactiverBoutons()) {
      resultat.append(inactiverBouton);
    }
    resultat.append("}catch(err){ ");
    resultat.append("if (err.number == '-2147024891'){ ");

    Message libelleFichierErreurPasAccesOuPasExistant = null;

    if (getMessageFichierErreurPasAccesOuPasExistant() != null) {
      libelleFichierErreurPasAccesOuPasExistant =
          UtilitaireMessage.get(this.getMessageFichierErreurPasAccesOuPasExistant(),
              null, pageContext);
    }

    resultat.append("cacherMessageAttente();");
    resultat.append("reactiverBoutonInactive();");
    resultat.append("alert('" +
        UtilitaireString.convertirEnJavaScript(libelleFichierErreurPasAccesOuPasExistant.getMessage()) +
        "'); ");

    resultat.append("} ");
    resultat.append("return false; ");
    resultat.append("} ");

    resultat.append("}");
    resultat.append("}");

    return resultat.toString();
  }

  /**
   * Traiter la fin de la balise Input.
   * @exception JspException si une exception JSP est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    super.doEndTag();

    return (EVAL_PAGE);
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du submit.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  protected void evaluerEL() throws JspException {
    if (getFonctionApresSoumission() != null) {
      this.fonctionApresSoumission =
          EvaluateurExpression.evaluerString("fonctionApresSoumission",
              getFonctionApresSoumission(),
              this, pageContext);
    }

    if (getFichier() != null) {
      this.fichier =
          EvaluateurExpression.evaluerString("fichier", getFichier(), this,
              pageContext);
    }

    if (getMessageFichierObligatoire() != null) {
      this.fichier =
          EvaluateurExpression.evaluerString("fichier", getFichier(), this,
              pageContext);
    }

    super.evaluerEL();
  }

  private void traitementFonctionApresSoumission() {
    String href = getHref();

    if ((href != null) && (fonctionApresSoumission != null)) {
      HashMap listeFonctionsApresSoumission =
          (HashMap)GestionParametreSysteme.getInstance().getParametreSysteme("listeFonctionsApresSoumission");

      if (listeFonctionsApresSoumission == null) {
        listeFonctionsApresSoumission = new HashMap();
        GestionParametreSysteme.getInstance().ajouterParametreSysteme("listeFonctionsApresSoumission",
            listeFonctionsApresSoumission);
      }

      if (href.startsWith("/")) {
        href = href.substring(1);
      }

      listeFonctionsApresSoumission.put("href", fonctionApresSoumission);
    }

    setFonctionApresSoumission(fonctionApresSoumission);
  }

  /**
   * Retourne la fonction JS a appelé après le traitement de soumission
   * de formulaire.
   * @return la fonction JS a appelé après le traitement de soumission de formulaire.
   */
  public String getFonctionApresSoumission() {
    return fonctionApresSoumission;
  }

  /**
   * Fixer la fonction JS a appelé après le traitement de soumission
   * de formulaire.
   * @param fonctionApresSoumission la fonction JS a appelé après le traitement de soumission de formulaire.
   */
  public void setFonctionApresSoumission(String fonctionApresSoumission) {
    this.fonctionApresSoumission = fonctionApresSoumission;
  }

  /**
   * Definir si le boutton
   * de soumission est utliser pour
   * soumettre un fichier.
   * Valeur suppore : "true / false"
   * Le EL est supporte
   *
   * La valeur true a pour effet de rendre le champ fichier obligatoire
   * en javascript et valide que le fichier est existant
   * et que le navigateur y a acces pour le telecharger
   * vers l'amont.
   *
   * NOTE. Ceci fonctionne pour formulaire avec seulement un
   * champ fichier.
   *
   * @param fichier
   * @since 2.0.5
   */
  public void setFichier(String fichier) {
    this.fichier = fichier;
  }

  /**
   * Permet de savoir si le boutton
   * de soumission est utliser pour
   * soumettre un fichier.
   * Valeur supporte : "true / false"
   * Le EL est supporte.
   *
   * La valeur true a pour effet de rendre le champ fichier obligatoire
   * en javascript et valide que le fichier est existant
   * et que le navigateur y a acces pour le telecharger
   * vers l'amont.
   *
   * NOTE. Ceci fonctionne pour formulaire avec seulement un
   * champ fichier.
   *
   * @return fichier
   * @since 2.0.5
   */
  public String getFichier() {
    return fichier;
  }

  public boolean isSoumissionFichier() {
    if (!UtilitaireString.isVide(fichier)) {
      soumissionFichier = Boolean.valueOf(fichier).booleanValue();
    }

    return soumissionFichier;
  }

  /**
   * Definir l'identifiant du message pour expliquer que
   * le fichier est obligatoire.
   *
   * Le EL est supporte.
   * @param messageFichierObligatoire
   * @since 2.0.5
   */
  public void setMessageFichierObligatoire(String messageFichierObligatoire) {
    this.messageFichierObligatoire = messageFichierObligatoire;
  }

  /**
   * Definir l'identifiant du message pour expliquer que
   * le fichier est obligatoire.
   *
   * Le EL est supporte.
   * @return messageFichierOblibatoire
   * @since 2.0.5
   */
  public String getMessageFichierObligatoire() {
    return messageFichierObligatoire;
  }

  /**
   * Definir l'identifiant du message pour expliquer qu'une
   * erreur est survenue et que vous n'avez acces au fichier ou que
   * le fichier n'est pas existant.
   *
   * Le EL est supporte.
   * @param messageFichierErreurPasAccesOuPasExistant
   * @since 2.0.5
   */
  public void setMessageFichierErreurPasAccesOuPasExistant(String messageFichierErreurPasAccesOuPasExistant) {
    this.messageFichierErreurPasAccesOuPasExistant =
        messageFichierErreurPasAccesOuPasExistant;
  }

  /**
   * Obtenir l'identifiant du message pour expliquer qu'une
   * erreur est survenue et que vous n'avez acces au fichier ou que
   * le fichier n'est pas existant.
   *
   * Le EL est supporte.
   * @return messageFichierErreurPasAccesOuPasExistant
   * @since 2.0.5
   */
  public String getMessageFichierErreurPasAccesOuPasExistant() {
    return messageFichierErreurPasAccesOuPasExistant;
  }

}
