/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.ButtonTag;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise JSP de base pour représenter un bouton
 * <p>
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.1
 */
public abstract class BaseBoutonTag extends ButtonTag {

  private static final long serialVersionUID = 6506764353831295789L;

  /**
   * Le libelle à afficher sur le bouton.
   */
  private String libelle;

  /**
   * Le message de confirmation;
   */
  private String messageConfirmation;

  /**
   * L'aide contextuelle à afficher sur le bouton.
   * Les codes de l'aide contextuelle pour accès dans un bundle ou dans la BD sont supportés
   */
  private String aideContextuelle;

  /**
   * Utiliser pour spécifier un chemin d'accès à une image.
   * Remplace le bouton si l'image est spécifiée
   */
  protected String src = null;

  /**
   * Utiliser pour spécifier un chemin d'accès à une image.
   * Remplace le bouton si l'image est spécifiée.
   */
  protected String srcAlt = null;

  /**
   * Le libelle correspondant au message d'attente.
   */
  protected String libelleMessageAttente = null;

  /**
   * La position X du message d'attente.
   */
  protected String positionXMessageAttente = null;

  /**
   * La position Y du message d'attente.
   */
  protected String positionYMessageAttente = null;

  /**
   * Le style id a cacher.
   */
  protected String styleIdACacher = null;

  /**
   * le lien HTML appelé lors d'un click sur le bouton
   * Les valeurs EL style JSTL sont acceptées
   */
  private String href;

  /**
   * Le bouton est représenté par une image.
   */
  private boolean image;

  /**
   * Spécifie que le formulaire à été modidifié
   */
  private boolean notifierModificationFormulaire;

  /** la valeur qui indique si l'on doit toujours garder le boutons du formulaire actif */
  protected boolean inactiverBoutons = true;

  /**
   * Le href inclus du javascript
   */
  private boolean hrefJS = false;

  /**
   * Est-ce que le bouton effectue un traitement essentiellement transactionnel.
   */
  protected String transactionnel = null;

  /**
   * Permet de rendre inactif le bouton.
   */
  protected String disabledEL;

  /**
   * Activer seulement si formulaire modifié.
   */
  protected String activerSiFormulaireModifie = null;

  /**
   * Inactiver seulement si formulaire modifié.
   */
  protected String inactiverSiFormulaireModifie = null;

  /**
   * Actif même si le formulaire associé est en lecture seulement.
   */
  protected String actifSiFormulaireLectureSeulement = null;

  /**
   * Spécifie si le formulaire doit traiter les validations et populer l'objet de transfert.
   */
  protected String ignorerValidation = null;

  /**
   * Spécifie si le formulaire doit traiter les validations des champs obligatoires.
   */
  protected String ignorerValidationObligatoire = null;

  /**
   * Spécifie si le bouton doit appeler, sur le clique, la fonction de confirmation
   * des modifications apportées au formulaire.
   */
  private String activerConfirmationModificationFormulaire;

  /**
   * Le nom de la variable qui va logé le résultat de la balise bouton.
   * @since SOFI 2.0.4
   */
  private String var;


  /** Constructeur par défaut */
  public BaseBoutonTag() {
  }

  /**
   * Obtenir le libelle à afficher sur le bouton.
   * <p>
   * @return le libelle à afficher sur le bouton
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Fixer le libelle à afficher sur le bouton.
   * <p>
   * @param libelle le libelle à afficher sur le bouton
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir le message de confirmation à afficher lorsque le bouton est appuyer.
   * <p/>
   * @return le message de confirmation à afficher lorsque le bouton est appuyer
   */
  public String getMessageConfirmation() {
    return messageConfirmation;
  }

  /**
   * Fixer le message de confirmation à afficher lorsque le bouton est appuyer.
   * <p/>
   * @param messageConfirmation le message de confirmation à afficher lorsque le bouton est appuyer
   */
  public void setMessageConfirmation(String messageConfirmation) {
    this.messageConfirmation = messageConfirmation;
  }

  /**
   * Obtenir le libelle du message d'attente à afficher.
   * <p>
   * @return le libelle du message d'attente à afficher.
   */
  public String getLibelleMessageAttente() {
    return libelleMessageAttente;
  }

  /**
   * Fixer le libelle du message d'attente à afficher.
   * <p>
   * @param libelle le libelle du message d'attente à afficher.
   */
  public void setLibelleMessageAttente(String libelleMessageAttente) {
    this.libelleMessageAttente = libelleMessageAttente;
  }

  /**
   * Obtenir le libelle à afficher sur le bouton.
   * <p>
   * @return le libelle à afficher sur le bouton
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer le libelle à afficher sur le bouton.
   * <p>
   * @param libelle le libelle à afficher sur le bouton
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Fixer le chemin d'accès vers l'image qui sert à soumettre le formulaire.
   * <p>
   * @param src le chemin d'accès vers l'image qui sert à soumettre le formulaire
   */
  public void setSrc(String src) {
    this.src = src;
  }

  /**
   * Obtenir le chemin d'accès vers l'image qui sert à soumettre le formulaire.
   * <p>
   * @return le chemin d'accès vers l'image qui sert à soumettre le formulaire
   */
  public String getSrc() {
    return src;
  }

  /**
   * Fixer le chemin d'accès vers l'image alternative qui sert à soumettre le formulaire.
   * <p>
   * @param srcAlt le chemin d'accès vers l'image alternative qui sert à soumettre le formulaire
   */
  public void setSrcAlt(String srcAlt) {
    this.srcAlt = srcAlt;
  }

  /**
   * Fixer le chemin d'accès vers l'image alternative qui sert à soumettre le formulaire.
   * <p>
   * @return le chemin d'accès vers l'image alternative qui sert à soumettre le formulaire
   */
  public String getSrcAlt() {
    return srcAlt;
  }

  /**
   * Obtenir le lien HTML appelé lors d'un click sur le bouton.
   * <p>
   * @return le lien HTML appelé lors d'un click sur le bouton
   */
  public String getHref() {
    return href;
  }

  /**
   * Fixer le lien HTML appelé lors d'un click sur le bouton.
   * <p>
   * @param le lien HTML appelé lors d'un click sur le bouton
   */
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Exécute le début de la balise.
   * <p>
   * @throws JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();

    if (!UtilitaireString.isVide(getLibelleMessageAttente())) {
      // Traitement du message d'attente.
      UtilitaireBaliseJSP.setProchainNomMessageAttente(pageContext.getRequest());
    }

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }else {
      if ((getHref() != null) && (getHref().indexOf("javascript:") != -1)) {
        setHrefJS(true);
      }
    }

    if (getStyleId() == null) {
      setStyleId(UtilitaireBaliseJSP.getNomProprieteVide(pageContext));
    } else {
      if (pageContext.getRequest().getAttribute("StyleId" + getStyleId()) !=
          null) {
        setStyleId(getStyleId() + "1");
      } else {
        pageContext.getRequest().setAttribute("StyleId" + getStyleId(),
            Boolean.TRUE);
      }
    }

    // Ajouter paramètre qui permet d'ignorer les validations fait au submit.
    if ((getIgnorerValidation() != null) &&
        getIgnorerValidation().toLowerCase().equals("true")) {
      Href nouveauHref = new Href(getHref());
      nouveauHref.ajouterParametre("ignorerValidation", "true");
      setHref(nouveauHref.getUrlPourRedirection());
    }

    if ((getIgnorerValidationObligatoire() != null) &&
        getIgnorerValidationObligatoire().toLowerCase().equals("true")) {
      Href nouveauHref = new Href(getHref());
      nouveauHref.ajouterParametre("ignorerValidationObligatoire", "true");
      setHref(nouveauHref.getUrlPourRedirection());
    }

    if (isActiverConfirmationModification()) {
      setOnclick(ConstantesBaliseJSP.CODE_CONFIRMER_MODIFICATION_FORMULAIRE +
          ((getOnclick() == null) ? "" : getOnclick()));
    }

    traiterActiverSiFormulaireModifie();
    traiterInactiverSiFormulaireModifie();

    StringBuffer codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    boolean afficherBouton = false;

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      // L'utilisateur à droit de voir le composant
      if (!UtilitaireBaliseJSP.isCacherLienInterdit(href, request)
          && gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
              this.libelle)) {
        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setDisabled(true);
        } else {
          BaseForm baseForm =
              UtilitaireBaliseJSP.getBaseForm(this.pageContext);
          boolean blocSecurite =
              (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) !=
              null) || ((baseForm != null) && baseForm.isModeLectureSeulement());

          if (UtilitaireString.isVide(getDisabledEL()) && blocSecurite) {
            if (activerModeTransactionnel() && !possedeActifSiFormulaireLectureSeulement()) {
              this.setDisabled(true);
            } else {
              this.setDisabled(false);
            }
          }
        }

        afficherBouton = true;
      }
    } else {
      afficherBouton = true;
    }

    specifierStyleBouton();

    if (afficherBouton) {
      // Générer le code représentant le composant HTML
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      if (!UtilitaireString.isVide(codeHtml.toString())) {
        codeHtml.append(prepareEventHandlers());
        codeHtml.append(prepareStyles());
        codeHtml.append(getElementClose());
      }

      if (getVar() == null) {
        TagUtils.getInstance().write(pageContext, codeHtml.toString());
      } else {
        pageContext.setAttribute(getVar(), codeHtml.toString());
      }
    }

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Traiter la fermeture de la balise.
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    this.release();

    return (EVAL_PAGE);
  }

  /**
   * Permet d'inactiver un bouton lorsque le client a appuyé une fois sur le bouton.
   * <p>
   * Évite la double-soumission.
   * <p>
   * @return le code HTML qui représente la fonction Javascript à ajouter dans la page
   */

  /**
   * Permet d'inactiver un bouton lorsque le client a appuyé une fois sur une image.
   * <p>
   * Évite la double-soumission.
   * <p>
   * @param nomFormulaire le nom du formulaire à soumettre
   * @return le code HTML qui représente la fonction Javascript à ajouter dans la page
   */
  public String getFonctionInactiverImage(String nomFormulaire) {
    StringBuffer fonction = new StringBuffer();
    boolean jsEcrit = pageContext.getAttribute("jsEcrit") != null;

    if (!jsEcrit) {
      fonction.append("\n<input type=\"hidden\" name=\"navigue\" value=\"0\"/>");
      fonction.append("\n<script type=\"text/javascript\">");
      fonction.append("\nfunction clickImage(theform, image) {");

      if (this.srcAlt != null) {
        fonction.append("\n  image.src = '").append(this.srcAlt).append("';");
      }

      fonction.append("\n  if (theform.navigue.value == '0') {");
      fonction.append("\n    theform.navigue.value = '1';");
      fonction.append("\n    setTimeout(\"document.").append(nomFormulaire).append(".submit();\", 10);");
      fonction.append("\n  }");

      fonction.append("\n}");
      fonction.append("\n</script>\n");

      pageContext.setAttribute("jsEcrit", Boolean.TRUE);
    }

    return fonction.toString();
  }

  /**
   * Retourne le lien sur une image
   * <p>
   * @return le code HTML représentant l'image
   * @throws JspException si une exception JSP est lancé
   */
  public String getLienImage() throws JspException {
    StringBuffer resultat = new StringBuffer();
    resultat.append("\n<img src=\"").append(this.src).append("\"");

    if (property != null) {
      resultat.append(" name=\"");
      resultat.append(property);

      if (indexed) {
        prepareIndex(resultat, null);
      }

      resultat.append("\"");
    }

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(getElementClose());

    return resultat.toString();
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  protected abstract StringBuffer genererCodeHtmlComposant() throws JspException;

  /**
   * Méthode qui doit être ré-écrite afin de décorer après le composants d'interface.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  protected StringBuffer decorerApresComposant() throws JspException {
    return null;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      if (getMessageConfirmation() != null) {
        String messageConfirmation =
            EvaluateurExpression.evaluerString("messageConfirmation",
                getMessageConfirmation(), this, pageContext);
        setMessageConfirmation(messageConfirmation);
      }

      if (getAideContextuelle() != null) {
        String aideContextuelle =
            EvaluateurExpression.evaluerString("aideContextuelle",
                getAideContextuelle(), this, pageContext);
        setAideContextuelle(aideContextuelle);
      }

      if (getLibelle() != null) {
        String libelle =
            EvaluateurExpression.evaluerString("libelle", getLibelle(), this,
                pageContext);
        setLibelle(libelle);
      }

      //@todo s'assurer que ca fonctionne tout de meme en JSP 2.0
      if ((getHref() != null) && (getHref().indexOf("javascript:") != -1) &&
          (getHref().indexOf("${") != -1)) {
        int positionEL = getHref().indexOf("${");

        String hrefSansJS = getHref().substring(positionEL);
        setHref(hrefSansJS.substring(0, hrefSansJS.indexOf("}") + 1));
        setHrefJS(true);
      }

      if (UtilitaireBaliseJSP.isValeurEL(getHref())) {
        setHref(EvaluateurExpression.evaluerString("href", getHref(), this,
            pageContext));
      } else {
        setHref(UtilitaireBaliseJSP.genererUrl(pageContext, getHref()));
      }

      setOnclick(EvaluateurExpression.evaluerString("onclick", getOnclick(),
          this, pageContext));

      if (getDisabledEL() != null) {
        boolean disabled =
            EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
                this, pageContext);
        setDisabled(disabled);
      }

      if (getIgnorerValidation() != null) {
        setIgnorerValidation(EvaluateurExpression.evaluerString("ignorerValidation",
            getIgnorerValidation(), this, pageContext));
      }

      if (getIgnorerValidationObligatoire() != null) {
        setIgnorerValidationObligatoire(EvaluateurExpression.evaluerString("ignorerValidationObligatoire",
            getIgnorerValidationObligatoire(), this, pageContext));
      }

      if (getActifSiFormulaireLectureSeulement() != null) {
        String actifSiFormulaireLectureSeulement =
            EvaluateurExpression.evaluerString("actifSiFormulaireLectureSeulement",
                getActifSiFormulaireLectureSeulement(), this, pageContext);
        setActifSiFormulaireLectureSeulement(actifSiFormulaireLectureSeulement);
      }

      if (getInactiverSiFormulaireModifie() != null) {
        setInactiverSiFormulaireModifie(EvaluateurExpression.evaluerString("inactiverSiFormulaireModifie",
            getInactiverSiFormulaireModifie(), this, pageContext));
      }

      if (getActiverSiFormulaireModifie() != null) {
        String activerSiFormulaireModifie =
            EvaluateurExpression.evaluerString("activerSiFormulaireModifie",
                getActiverSiFormulaireModifie(), this, pageContext);
        setActiverSiFormulaireModifie(activerSiFormulaireModifie);
      }

      if (getActiverConfirmationModificationFormulaire() != null) {
        setActiverConfirmationModificationFormulaire(EvaluateurExpression.evaluerString("activerConfirmationModificationFormulaire",
            getActiverConfirmationModificationFormulaire(), this,
            pageContext));
      }
    } catch (JspException e) {
      throw e;
    }
  }

  public void setStyleIdACacher(String styleIdACacher) {
    this.styleIdACacher = styleIdACacher;
  }

  public String getStyleIdACacher() {
    return styleIdACacher;
  }

  public void setPositionXMessageAttente(String positionXMessageAttente) {
    this.positionXMessageAttente = positionXMessageAttente;
  }

  public String getPositionXMessageAttente() {
    return positionXMessageAttente;
  }

  public void setPositionYMessageAttente(String positionYMessageAttente) {
    this.positionYMessageAttente = positionYMessageAttente;
  }

  public String getPositionYMessageAttente() {
    return positionYMessageAttente;
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    libelle = null;

    messageConfirmation = null;
    setAideContextuelle(null);
    src = null;
    positionXMessageAttente = null;
    positionYMessageAttente = null;
    styleIdACacher = null;
    image = false;
    setTitle(null);
    transactionnel = null;
    setHrefJS(false);
    setActiverSiFormulaireModifie(null);
    setIgnorerValidation(null);
    setTransactionnel(null);
    inactiverBoutons = true;
    setOnclick(null);
    setHref(null);
    setStyleId(null);

    super.release();
  }

  /**
   * Est-ce que le bouton est représenté par une image.
   * @return true si le bouton est représenté par une image.
   */
  public boolean isImage() {
    return image;
  }

  /**
   * Fixer true si le bouton est représenté par une image.
   * @param image true si le bouton est représenté par une image.
   */
  public void setImage(boolean image) {
    this.image = image;
  }

  /**
   * Est-ce que vous désirez notifier que le formulaire à été modifié
   * lors du traitement de l'action du bouton.
   * @return true si vous désirez notifier que le formulaire à été modifié
   */
  public boolean isNotifierModificationFormulaire() {
    return notifierModificationFormulaire;
  }

  /**
   * Fixer true si vous désirez notifier que le formulaire à été modifié
   * lors du traitement de l'action du bouton.
   * @param notifierModificationFormulaire true si vous désirez notifier que le formulaire à été modifié
   */
  public void setNotifierModificationFormulaire(boolean notifierModificationFormulaire) {
    this.notifierModificationFormulaire = notifierModificationFormulaire;
  }

  /**
   * Est-ce que lors de la sélection du boutons, tous les autres boutons
   * du formulaire se désactive?
   * <p>
   * true par défaut.
   * @return true si tous les autres boutons du formulaire se désactive lors de la sélection du bouton.
   */
  public boolean getInactiverBoutons() {
    return inactiverBoutons;
  }

  /**
   * Fixer true si si tous les autres boutons du formulaire se désactive lors
   * de la sélection du bouton.
   * @param inactiverBoutons
   */
  public void setInactiverBoutons(boolean inactiverBoutons) {
    this.inactiverBoutons = inactiverBoutons;
  }

  /**
   * Est-ce que le href fixé est du javascript?
   * @return true si le href fixé est du javascript
   */
  protected boolean isHrefJS() {
    return hrefJS;
  }

  /**
   * Fixer true si le href fixé est un javascript
   * @param hrefJS true si le href fixé est un javascript
   */
  protected void setHrefJS(boolean hrefJS) {
    this.hrefJS = hrefJS;
  }

  /**
   * Spécifier le style des boutons.
   */
  private void specifierStyleBouton() {
    if (getSrc() == null) {
      if (getStyleClass() == null) {
        // Traiter si aucun style de spécifier, alors prendre le défaut de SOFI.
        setStyleClass("bouton");

        if (!getDisabled()) {
          setStyleClass("bouton");
        } else {
          setStyleClass("boutondisabled");
        }
      } else {
        // Appliquer le style spécifier.
        if (getDisabled()) {
          StringBuffer styleDisabled = new StringBuffer(getStyleClass());
          styleDisabled.append("disabled");
          setStyleClass(styleDisabled.toString());
        }
      }


    }

    if (!UtilitaireString.isVide(getLibelleMessageAttente())) {
      Message messageAttente =
          UtilitaireMessage.get(getLibelleMessageAttente(), null, pageContext);

      // @since SOFI 2.0.2
      // Support si la clé est un libellé.
      if ((getLibelleMessageAttente().indexOf(".") != -1) &&
          getLibelleMessageAttente().equals(messageAttente.getMessage())) {
        // Vérifier le message est un libellé.
        messageAttente =
            UtilitaireLibelle.getLibelle(getLibelleMessageAttente(),
                pageContext, null);
      }

      // Traitement du message d'attente.
      StringBuffer onClickBuffer = new StringBuffer();
      onClickBuffer.append(UtilitaireBaliseJSP.genererEvenementMessageAttente(pageContext,
          UtilitaireString.convertirEnJavaScript(messageAttente.getMessage()),
          getStyleId(), getPositionXMessageAttente(),
          getPositionYMessageAttente()));

      if (!UtilitaireString.isVide(getStyleIdACacher())) {
        onClickBuffer.append("hide('");
        onClickBuffer.append(getStyleIdACacher());
        onClickBuffer.append("');");
      }

      setOnclick(onClickBuffer.toString());
    }
  }

  public void setTransactionnel(String transactionnel) {
    this.transactionnel = transactionnel;
  }

  public String getTransactionnel() {
    return transactionnel;
  }

  public boolean activerModeTransactionnel() {
    if ((transactionnel != null) &&
        transactionnel.toLowerCase().equals("false")) {
      return false;
    } else {
      return true;
    }
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  @Override
  protected String getElementClose() {
    // Prendre le bon libellé associé
    Libelle libelleAffichage = UtilitaireLibelle.getLibelle(getLibelle(), pageContext);
    StringBuffer fermeture = new StringBuffer(">");
    if (getLibelle() != null) {
      fermeture.append("<span>");
      fermeture.append(libelleAffichage.getMessage());
      fermeture.append("</span>");

    }
    if (src != null) {
      fermeture.append("</input>");
    }else {
      fermeture.append("</button>");
    }

    return fermeture.toString();
  }

  public void setActiverSiFormulaireModifie(String activerSiFormulaireModifie) {
    this.activerSiFormulaireModifie = activerSiFormulaireModifie;
  }

  public String getActiverSiFormulaireModifie() {
    return activerSiFormulaireModifie;
  }

  public boolean isTraitementActiverSiFormulaireModifie() {
    if ((getActiverSiFormulaireModifie() != null) &&
        getActiverSiFormulaireModifie().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setInactiverSiFormulaireModifie(String inactiverSiFormulaireModifie) {
    this.inactiverSiFormulaireModifie = inactiverSiFormulaireModifie;
  }

  public String getInactiverSiFormulaireModifie() {
    return inactiverSiFormulaireModifie;
  }

  public boolean isTraitementInactiverSiFormulaireModifie() {
    if ((getInactiverSiFormulaireModifie() != null) &&
        getInactiverSiFormulaireModifie().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  private void traiterInactiverSiFormulaireModifie() {
    if (isTraitementInactiverSiFormulaireModifie()) {
      List listeBoutonInactive =
          (List)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_INACTIVE);

      if (listeBoutonInactive == null) {
        listeBoutonInactive = new ArrayList();
        pageContext.getRequest().setAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_INACTIVE,
            listeBoutonInactive);
      }

      listeBoutonInactive.add(getStyleId());
    }
  }

  private void traiterActiverSiFormulaireModifie() {
    if (isTraitementActiverSiFormulaireModifie()) {
      List listeBoutonActive =
          (List)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_ACTIVE);

      if (listeBoutonActive == null) {
        listeBoutonActive = new ArrayList();
        pageContext.getRequest().setAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_ACTIVE,
            listeBoutonActive);
      }

      listeBoutonActive.add(getStyleId());
    }
  }

  public void setActifSiFormulaireLectureSeulement(String actifSiFormulaireLectureSeulement) {
    this.actifSiFormulaireLectureSeulement = actifSiFormulaireLectureSeulement;
  }

  public String getActifSiFormulaireLectureSeulement() {
    return actifSiFormulaireLectureSeulement;
  }

  /**
   * Est-ce que le bouton doit être actif même si le formulaire associé est
   * en lecture seulement.
   */
  public boolean possedeActifSiFormulaireLectureSeulement() {
    if ((getActifSiFormulaireLectureSeulement() != null) &&
        getActifSiFormulaireLectureSeulement().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setIgnorerValidation(String ignorerValidation) {
    this.ignorerValidation = ignorerValidation;
  }

  public String getIgnorerValidation() {
    return ignorerValidation;
  }

  public void setIgnorerValidationObligatoire(String ignorerValidationObligatoire) {
    this.ignorerValidationObligatoire = ignorerValidationObligatoire;
  }

  public String getIgnorerValidationObligatoire() {
    return ignorerValidationObligatoire;
  }

  public boolean isActiverConfirmationModification() {
    if ((getActiverConfirmationModificationFormulaire() != null) &&
        getActiverConfirmationModificationFormulaire().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public String getActiverConfirmationModificationFormulaire() {
    return activerConfirmationModificationFormulaire;
  }

  public void setActiverConfirmationModificationFormulaire(String activerConfirmationModificationFormulaire) {
    this.activerConfirmationModificationFormulaire =
        activerConfirmationModificationFormulaire;
  }

  /**
   * Fixer le nom de la variable dont le résultat de la balise sera logé.
   * @param var le nom de la variable dont le résultat de la balise sera logé.
   * @since SOFI 2.0.4
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom la variable dont le résultat de la balise sera logé.
   * @since SOFI 2.0.4
   * @return le nom la variable dont le résultat de la balise sera logé.
   */
  public String getVar() {
    return var;
  }

  public boolean isActiverFonctionInactiverBoutons() {
    return inactiverBoutons;
  }
}
