/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

/**
 * Interface qui doit être implémentée pour décorer la
 * valeur et l'étiquette d'un élément de la liste
 * d'option de tag Select.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface SelectDecorator {

  /**
   * Obtenir l'étiquette de l'option. Cette valeur
   * est affiché à l'écran dans la liste déroulante.
   * @param objet option
   * @return Étiquette formattée
   */
  String getEtiquette(Object objet);

  /**
   * Obtenir la valeur de l'option qui sera
   * utilisée pour identifier l'option.
   * @param objet option
   * @return Valeur de l'option
   */
  String getValeur(Object objet);

}
