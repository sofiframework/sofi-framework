/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.base.TableTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireNombre;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Objet de base pour les balises de formulaire.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BaseFormulaireTag extends org.apache.struts.taglib.html.BaseFieldTag {
  /**
   * 
   */
  private static final long serialVersionUID = -2904392448043012952L;

  /** Variable qui identifie le style à utiliser pour les champs de saisie qui sont en erreur */
  public static final String STYLE_SAISIE_ERREUR = "saisieErreur";

  /** Variable qui identifie le style à utiliser pour les libellés des champs en erreur */
  public static final String STYLE_LIBELLE_ERREUR = "libelleErreur";

  /** Spécifie la classe css pour un attribut en lecture seulement */
  protected String styleClassLectureSeulement = null;

  /** Spécifie la classe css pour un attribut sélectionné */
  protected String styleClassFocus = null;

  /** Spécifie le libellé */
  protected String libelle = null;

  /** Spécifie si le champ de saisie est obligatoire */
  protected boolean obligatoire = false;

  /**
   * Spécifie si on désire fermer le ligne.
   * La fermeture de la balise tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  protected boolean fermerLigne = true;

  /** Spécifie si le format du champ de saisie accepte en expression régulière. **/
  protected String format = null;

  /** Spécifie si le format de sortie du champ de saisie pour traitement à la logique d'affaire.
      Implique l'utilisation du formatter d'expression régulière.
   **/
  protected String formatSaisie = null;

  /** Spécifie si le format de sortie du champ de saisie pour traitement à la logique d'affaire.
      Implique l'utilisation du formatter d'expression régulière.
   **/
  protected String formatSortie = null;

  /** Le message d'erreur a appliquer lors d'un echec de l'application du format **/
  protected String formatMessageErreur = null;

  /**
   * Spécifie si on désire ouvrir une nouvelle ligne.
   * L'ouverture de la balise tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  protected boolean ouvrirLigne = true;

  /**
   * Le style a spécifier à la ligne (TR).
   */
  protected String ligneStyleClass = null;

  /**
   * Permet d'inactiver un champ de saisie avec l'aide des expressions EL.
   */
  protected String disabledEL = null;

  /**
   * Permet de mettre en lecture seulement avec l'aide des expressions EL.
   */
  protected String readonlyEL = null;

  /**
   * Permet de spécifier que le champ de saisie est obligatoire avec l'aide des expressions EL.
   */
  protected String obligatoireEL = null;

  /** Spécifie si on désire générer un ligne HTML automatique, par défaut c'est OUI **/
  protected boolean genererLigne = true;

  /** Ajout d'un icone aide avec aide contextuelle **/
  protected String iconeAide = null;

  /**
   * Spécifie si vous désirez que l'icone d'aide soit position à gauche du libellé.
   */
  protected String iconeAideAGauche = null;

  /** Afficher seulemenet la valeur du formulaire et non le champ de saisie **/
  protected Object affichageSeulement = null;

  /** Affichage de l'indicateur obligatoire du champ de saisie. **/
  protected Object affichageIndObligatoire = null;

  /** Formater en nombre le champ de saisie **/
  protected boolean formatterEnNombre = false;

  /** Formater en valeur monétaire lors d'un affichage seulement */
  protected boolean valeurMonetaire = false;

  /** Spécifie la valeur par défaut */
  protected String valeurDefaut = null;

  /** Href lors d'un traitement ajax **/
  protected String href = null;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  protected String nomParametreValeur = null;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax.
   */
  protected String divRetourAjax = null;

  /**
   * L'identifiant de la page dont l'on désire modifier la valeur dans
   * un traitement ajax.
   */
  protected String idRetourAjax = null;

  /**
   * Spécifie qu'il va avoir plusieurs identifiant de retour dans la
   * réponse avec l'aide d'un document XML
   */
  protected boolean idRetourMultipleAjax = false;

  /**
   * Traitement JavaScript a executé pendant le chargement ajax
   */
  protected String ajaxJSPendantChargement;

  /**
   * Traitement JavaScript a executé après le chargement.
   */
  protected String ajaxJSApresChargement;

  /**
   * Traitement JavaScript Prérequis au chargement ajax.
   */
  protected String ajaxJSPreRequis;

  /**
   * Spécifie si le select est dans contexte transactionnel tel que l'application
   * de notification de modification de formulaire.
   * @since SOFI 2.0.2
   */
  private String transactionnel = "true";
  private String aideContextuelle = "";



  /** Constructeur par défaut */
  public BaseFormulaireTag() {
    this.doReadonly = true;
  }

  /*
   * Permet de savoir s'il faut afficher de l'aide en ligne sur le champ.
   */

  private boolean isAideEnLigne(String aideContextuelle,
      PageContext pageContext) {
    return (UtilitaireBaliseJSP.isAideEnligne(aideContextuelle, pageContext) &&
        (isAffichageIconeAide() || (getIconeAide() == null)));
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @return le style à utiliser dans le cas où le champ possède le focus
   */
  public String getStyleClassFocus() {
    return (this.styleClassFocus);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @param styleClassFocus le style à utiliser dans le cas où le champ possède le focus
   */
  public void setStyleClassFocus(String styleClassFocus) {
    this.styleClassFocus = styleClassFocus;
  }

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * @param libelle le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @return la valeur qui indique si le champ est obligatoire ou pas
   */
  public boolean getObligatoire() {
    return (this.obligatoire);
  }

  /**
   * Fixer la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @param obligatoire la valeur qui indique si le champ est obligatoire ou pas
   */
  public void setObligatoire(boolean obligatoire) {
    this.obligatoire = obligatoire;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public boolean getFermerLigne() {
    return (this.fermerLigne);
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @param fermerLigne la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public void setFermerLigne(boolean fermerLigne) {
    this.fermerLigne = fermerLigne;
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @param ouvrirLigne la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public void setOuvrirLigne(boolean ouvrirLigne) {
    this.ouvrirLigne = ouvrirLigne;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public boolean getOuvrirLigne() {
    return ouvrirLigne;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @param styleClassLectureSeulement le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Execute le début de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    if (getDisabledEL() != null) {
      boolean disabled =
          EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
              this, pageContext);
      setDisabled(disabled);
    }

    if (getReadonlyEL() != null) {
      boolean readonly =
          EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
              this, pageContext);
      setReadonly(readonly);
    }

    if (getObligatoireEL() != null) {
      boolean obligatoire =
          EvaluateurExpression.evaluerBoolean("obligatoireEL",
              String.valueOf(getObligatoireEL()), this, pageContext);
      setObligatoire(obligatoire);
    }

    /**
     * Si paramètre la propriété iconeAide est null et qu'il existe un paramètre système,
     * alors prendre la valeur de celui-ci.
     * @since SOFI 2.0.3
     */
    if (this.iconeAide == null) {
      Boolean iconeAideDefaut =
          GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE);

      if (iconeAideDefaut != null) {
        setIconeAide(iconeAideDefaut.toString());
      }
    }

    /**
     * Si paramètre la propriété iconeAideAGauche est null et qu'il existe un paramètre système,
     * alors prendre la valeur de celui-ci.
     * @since SOFI 2.1
     */
    if (this.iconeAideAGauche == null) {
      Boolean iconeAideAGaucheDefaut =
          GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE_A_GAUCHE);

      if (iconeAideAGaucheDefaut != null) {
        setIconeAideAGauche(iconeAideAGaucheDefaut.toString());
      }
    }

    // Fixer l'évènement pour la liste de valeur s'il y a lieu.
    String urlListeValeurOnchange =
        (String)pageContext.getAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONCHANGE);
    String urlListeValeurOnkeyup =
        (String)pageContext.getAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONKEYUP);

    if (urlListeValeurOnchange != null) {
      urlListeValeurOnchange =
          ListeValeursTag.remplacerParametreProperty(urlListeValeurOnchange,
              getProperty(), pageContext);
      setOnchange(urlListeValeurOnchange);
    }

    if (urlListeValeurOnkeyup != null) {
      urlListeValeurOnkeyup =
          ListeValeursTag.remplacerParametreProperty(urlListeValeurOnkeyup,
              getProperty(), pageContext);
      setOnkeyup(urlListeValeurOnkeyup);
    }

    if (getDisabled()) {
      setObligatoire(false);
    }

    if (getLibelle() != null) {
      TableTag.gererOuvertureEtFermeturePourBalise(this, pageContext);
    }

    String codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {
        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setDisabled(true);
        } else {
          boolean blocSecurite = (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null)
              || this.isFormulaireEnLectureSeulement();
          boolean isLectureSeulement = UtilitaireString.isVide(getDisabledEL())
              && UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
          if (isLectureSeulement) {
            this.setDisabled(true);
          }
        }

        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant();
      } else {
        this.pageContext.setAttribute("CHAMP_SECURISE", Boolean.TRUE);
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    }

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Déterminer si le formulaire courant est en lecture seulement.
   * On doit vérifier le formulaire parent si il existe. Si oui,
   * on utilise
   *
   * @return true, le formulaire doit etre affiché en lecture seule.
   * False, le formulaire sera éditable.
   */
  protected boolean isFormulaireEnLectureSeulement() {
    BaseForm formulaire =
        UtilitaireBaliseNested.getFormulaireCourant(pageContext, getProperty());
    boolean lectureSeule = false;

    if (formulaire != null) {
      lectureSeule = formulaire.isModeLectureSeulement();

      if ((formulaire.getFormulaireParent() != null) && !lectureSeule) {
        lectureSeule =
            formulaire.getFormulaireParent().isModeLectureSeulement();
      }
    }

    return lectureSeule;
  }

  /**
   * Déterminer si le formulaire courant est en affichage seulement.
   * On doit vérifier si le formulaire parent
   * @returntrue, le formulaire doit etre affiché en affichage seulement.
   * False, le formulaire sera éditable.
   */
  protected boolean isFormulaireEnAffichageSeulement() {
    return UtilitaireBaliseJSP.isFormulaireCourantEnAffichageSeulement(pageContext,
        getProperty());
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  protected String genererCodeHtmlComposant() throws JspException {
    // Accès au formulaire
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    // Si le formulaire est en affichage seulement tous les composants doivent l'être
    if (baseForm != null) {
      if (isFormulaireEnAffichageSeulement() && getAffichageSeulement() == null) {
        setAffichageSeulement(Boolean.TRUE);
      }

      // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
      // L'attribut ne devrait pas être traité si c'est readonly
      if (!isAffichage() && !getDisabled()) {
        baseForm.ajouterAttributATraiter(getProperty());
      } else {
        this.setObligatoire(false);
        this.setAffichageIndObligatoire(Boolean.FALSE);
      }
    }

    if (getStyleId() == null) {
      this.setStyleId(getProperty());
    }

    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    if (getObligatoire()) {
      formulaire.ajouterAttributObligatoire(nomFormulaireImbrique,
          getProperty());
    }

    if (getTitle() != null) {
      aideContextuelle = getTitle();
    } else {
      aideContextuelle = getLibelle();
    }

    // Traiter le format du champ de saisie.
    try {
      formulaire.ajouterFormat(nomFormulaireImbrique, getProperty(),
          getFormat(), getFormatSaisie(), getFormatSortie(),
          getFormatMessageErreur());
    } catch (Exception e) {
    }

    StringBuffer resultat = new StringBuffer("");

    if (getProperty() == null) {
      this.property = getLibelle();
    }

    MessageErreur erreur = null;

    if (getProperty() != null) {
      erreur =
          UtilitaireBaliseJSP.getMessageErreur(baseForm, this.property, this.pageContext);
    }

    Boolean estPositioneeHautChamp = Boolean.FALSE;
    String titre = null;

    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();

      // Permet de verifier si le message d'erreur de saisie
      // doit être positionné en bas du champ. False est la valeur par défaut et signifie que
      // les messages apparaîtront en nicetitle au survol du champ.
      estPositioneeHautChamp =
          GestionParametreSysteme.getInstance().getBoolean(Constantes.POSITION_MESSAGE_ERREUR_SAISIE);

      if (estPositioneeHautChamp == null) {
        estPositioneeHautChamp = Boolean.FALSE;
      }
    }

    Libelle libelle = null;
    String styleLibelle = null;

    this.setStyleClass(getStyleClass());
    this.setStyleClassFocus(getStyleClassFocus());

    if (erreur != null) {
      if (getStyleClass() != null) {
        this.setStyleClass(STYLE_SAISIE_ERREUR + " " + getStyleClass());
      } else {
        this.setStyleClass(STYLE_SAISIE_ERREUR);
      }

      this.setStyleClassFocus(STYLE_SAISIE_ERREUR);
      styleLibelle = STYLE_LIBELLE_ERREUR;
    } else {
      libelle =
          UtilitaireLibelle.getLibelle(this.getTitle(), pageContext, null);
      titre = libelle.getMessage();
    }

    // Pour que le titre du champ de saisie soit convertie en texte du gestion message
    if ((titre != null) && (estPositioneeHautChamp.booleanValue() == false)) {
      titre = UtilitaireString.convertirEnHtml(titre);
      this.setTitle(titre);
    }

    String titreErreur = null;

    if (erreur != null) {
      if (!GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_SUR_LIBELLE_ERREUR).booleanValue()) {
        setTitle("");
      }

      titreErreur = titre;
    }

    if (isAffichageIconeAide() &&
        !isAideEnLigne(aideContextuelle, pageContext)) {
      setTitle("");
    }

    boolean iconeFixe = false;

    // Générer le libéllé.
    if (!UtilitaireString.isVide(getLibelle())) {
      String nomAttribut = getProperty();

      if (isAffichage()) {
        nomAttribut = null;
      }

      if (isGenererLigne()) {
        if (getOuvrirLigne()) {
          resultat.append("<tr");

          if (getLigneStyleClass() != null) {
            resultat.append(" class=\"");
            resultat.append(getLigneStyleClass());
            resultat.append("\" ");
          }

          resultat.append(">");
        }

        // Ajouter le bout de code qui affiche le libellé
        if (isAffichage()) {
          resultat.append("<th scope=\"row\" class=\"libelle_formulaire\">");
          setObligatoire(false);
          nomAttribut = "null";
        } else {
          resultat.append("<th scope=\"row\">");
        }

        if (isAffichageIconeAideAGauche() &&
            (isAffichageIconeAide() || isAideEnLigne(aideContextuelle,
                pageContext))) {
          UtilitaireBaliseJSP.genererIcodeAide(getLibelle(), getProperty(),
              resultat, pageContext);
          resultat.append("&nbsp;");
          iconeFixe = true;
        }

        if (nomFormulaireImbrique != null) {
          UtilitaireBaliseJSP.genererLibelle(nomFormulaireImbrique,
              nomAttribut, -1, getTitle(), getLibelle(), styleLibelle,
              getObligatoire(), isAffichageIndObligatoire(), pageContext,
              resultat);
        } else {
          UtilitaireBaliseJSP.genererLibelle(nomAttribut, getTitle(),
              this.libelle, styleLibelle, this.obligatoire,
              isAffichageIndObligatoire(), this.pageContext, resultat);
        }

        resultat.append("</th><td>");
      } else {
        if (nomFormulaireImbrique != null) {
          UtilitaireBaliseJSP.genererLibelle(nomFormulaireImbrique,
              nomAttribut, -1, getTitle(), getLibelle(), styleLibelle,
              getObligatoire(), isAffichageIndObligatoire(), pageContext,
              resultat);
        } else {
          UtilitaireBaliseJSP.genererLibelle(nomAttribut, getTitle(),
              this.libelle, styleLibelle, this.obligatoire,
              isAffichageIndObligatoire(), this.pageContext, resultat);
        }

        resultat.append("&nbsp;");
      }
    } else {
      if (!isAffichage()) {
        if (isAffichageIconeAideAGauche() &&
            (isAffichageIconeAide() || isAideEnLigne(aideContextuelle,
                pageContext))) {
          UtilitaireBaliseJSP.genererIcodeAide(aideContextuelle, getProperty(),
              resultat, pageContext);
          resultat.append("&nbsp;");
          iconeFixe = true;
        }
      }

      if (getObligatoire()) {
        UtilitaireBaliseJSP.ajouterAttributObligatoire(nomFormulaireImbrique,
            getProperty(), pageContext);
      }
    }

    if (!UtilitaireString.isVide(titreErreur)) {
      setTitle(titreErreur);
    }

    // Ajouter le contenu HTML de la balise
    if (!isAffichage()) {
      // Si Traitement Ajax.
      if (getHref() != null) {
        // Fixer le nom de propriété comme nom de paramètre pour extraire la valeur.
        if (getNomParametreValeur() == null) {
          setNomParametreValeur(getProperty());
        }

        if (isIdRetourMultipleAjax()) {
          setDivRetourAjax("REPONSE_MULTIPLE");
        }

        if (getIdRetourAjax() != null) {
          setDivRetourAjax(getIdRetourAjax());
        }

        String appelAjax =
            UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(getHref(),
                getDivRetourAjax(), getNomParametreValeur(), getStyleId(),
                getAjaxJSPendantChargement(), getAjaxJSApresChargement(),
                pageContext);

        String onChangeCourant = getOnchange();

        if (onChangeCourant != null) {
          setOnchange(onChangeCourant + appelAjax);
        } else {
          setOnchange(appelAjax);
        }
      }

      this.ecrireBalise(resultat);
    } else {
      if (this.value == null) {
        Object value = null;

        try {
          value =
              TagUtils.getInstance().lookup(this.pageContext, this.name, this.property,
                  null);
        } catch (Exception e) {
          value = UtilitaireObjet.getValeurAttribut(baseForm, getProperty());
        }

        if ((value == null) || "".equals(value)) {
          if (getValeurDefaut() != null) {
            value = getValeurDefaut();
          } else {
            value = "";
          }
        }

        this.value = value.toString();
      }

      if (isFormatterEnNombre() && (getFormat() == null)) {
        setFormat("###");
      }

      // Si le format en nombre est désiré, fixer le format accepte.
      if ((format != null) &&
          (getFormat().substring(0, 1).indexOf("^") == -1) &&
          !UtilitaireString.isVide(value)) {
        if (getFormat().indexOf("$") != -1) {
          setValeurMonetaire(true);
        }

        try {
          Locale localeUtilisateur =
              UtilitaireSession.getLocale(pageContext.getSession());

          String valeurFormatte =
              UtilitaireNombre.getValeurFormate(getFormat(), value,
                  localeUtilisateur, true);

          this.value = valeurFormatte;
        } catch (Exception e) {
          // Conversion impossible, donc prendre la valeur saisie par l'utilisateur.
        }
      }

      resultat.append("<span id=\"");
      resultat.append(getProperty());
      resultat.append("\" ");

      if (!UtilitaireString.isVide(this.getStyleClass())) {
        resultat.append(" class=\"").append(this.getStyleClass()).append("\"");
      }

      resultat.append(">");

      if (!iconeFixe && isAffichageIconeAideAGauche() &&
          (isAffichageIconeAide() || isAideEnLigne(aideContextuelle,
              pageContext))) {
        UtilitaireBaliseJSP.genererIcodeAide(aideContextuelle, getProperty(),
            resultat, pageContext);
        resultat.append("&nbsp;");
      }

      resultat.append(UtilitaireString.convertirEnHtml(value));

      if (isAffichage()) {
        resultat.append("</span>");
      }
    }

    if (erreur != null) {
      // Si true, on place le message d'erreur en bas de la balise
      if (estPositioneeHautChamp.booleanValue()) {
        resultat.append("<span class=\"libelleErreur\">" + titre + "</span>");
      }
    }

    return resultat.toString();
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    if (!isAffichageIconeAideAGauche() &&
        (isAffichageIconeAide() || isAideEnLigne(aideContextuelle,
            pageContext))) {
      UtilitaireBaliseJSP.genererIcodeAide(aideContextuelle, getProperty(),
          resultat, pageContext);
    }

    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      resultat.append(valeur);
    }

    // Fermeture de la colonne si libellé de spécifier.
    if (genererLigne && (pageContext.getAttribute("CHAMP_SECURISE") == null)) {
      if (getLibelle() != null) {
        resultat.append("</td>");

        if (getFermerLigne()) {
          resultat.append("</tr>");
        }
      }
    }

    TagUtils.getInstance().write(pageContext, resultat.toString());

    //Libérer les ressources
    release();

    return (EVAL_PAGE);
  }

  /**
   * Ecrit la balise personnalisée.
   * <p>
   * @param resultat le contenu de la balise à écrire
   * @throws JspException Exception générique
   */
  protected abstract void ecrireBalise(StringBuffer resultat) throws JspException;

  /**
   * Méthode qui sert à libérer les ressources sytème utilisées
   */
  @Override
  public void release() {
    super.release();
    setStyleClassFocus(null);
    setStyleClass(null);
    setGenererLigne(true);
    setAffichageSeulement(null);
    setOnkeyup(null);
    setOnkeydown(null);
    setObligatoire(false);
    setNomParametreValeur(null);
    setAjaxJSApresChargement(null);
    setAjaxJSPendantChargement(null);
    setAjaxJSPreRequis(null);
    setAffichageIndObligatoire(null);
    setDivRetourAjax(null);
    setFormat(null);
    setFormatMessageErreur(null);
    setIdRetourMultipleAjax(false);
    setIdRetourAjax(null);
    setIconeAide(null);
    setFormatSaisie(null);
    setFermerLigne(true);
    setGenererLigne(true);
    setLibelle(null);
    setNomParametreValeur(null);
    setFormatSortie(null);
    setDisabledEL(null);
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String valeur =
        EvaluateurExpression.evaluerString("value", getValue(), this,
            pageContext);
    setValue(valeur);

    String valeurDefaut =
        EvaluateurExpression.evaluerString("valeurDefaut", getValeurDefaut(),
            this, pageContext);
    setValeurDefaut(valeurDefaut);

    if (getLibelle() != null) {
      String libelle =
          EvaluateurExpression.evaluerString("libelle", String.valueOf(getLibelle()),
              this, pageContext);
      setLibelle(libelle);
    }

    if (getProperty() != null) {
      String property =
          EvaluateurExpression.evaluerString("property", String.valueOf(getProperty()),
              this, pageContext);
      setProperty(property);
    }

    String href =
        EvaluateurExpression.evaluerString("href", getHref(), this, pageContext);
    setHref(href);

    String format =
        EvaluateurExpression.evaluerString("format", getFormat(), this,
            pageContext);
    setFormat(format);

    String formatSaisie =
        EvaluateurExpression.evaluerString("formatSaisie", getFormatSaisie(),
            this, pageContext);
    setFormatSaisie(formatSaisie);

    String formatSortie =
        EvaluateurExpression.evaluerString("formatSortie", getFormatSortie(),
            this, pageContext);
    setFormatSortie(formatSortie);

    String formatMessageErreur =
        EvaluateurExpression.evaluerString("formatMessageErreur",
            getFormatMessageErreur(), this, pageContext);
    setFormatMessageErreur(formatMessageErreur);

    if (getAffichageIndObligatoire() != null) {
      String affichageObligatoire =
          EvaluateurExpression.evaluerString("affichageIndObligatoire",
              (String)getAffichageIndObligatoire(), this, pageContext);
      setAffichageIndObligatoire(affichageObligatoire);
    }


    String affichageSeulement =
        EvaluateurExpression.evaluerString("affichageSeulement",
            (String)getAffichageSeulement(), this, pageContext);

    setAffichageSeulement(affichageSeulement);

    if (getOnchange() != null) {
      String onChange =
          EvaluateurExpression.evaluerString("onchange", String.valueOf(getOnchange()),
              this, pageContext);
      setOnchange(onChange);
    }

    if (getOnkeyup() != null) {
      String onKeyUp =
          EvaluateurExpression.evaluerString("onkeyup", String.valueOf(getOnkeyup()),
              this, pageContext);
      setOnkeyup(onKeyUp);
    }

    if (getOnblur() != null) {
      String onblur =
          EvaluateurExpression.evaluerString("onblur", String.valueOf(getOnblur()),
              this, pageContext);
      setOnblur(onblur);
    }

    if (getOnkeydown() != null) {
      String onkeydown =
          EvaluateurExpression.evaluerString("onkeydown", String.valueOf(getOnkeydown()),
              this, pageContext);
      setOnkeydown(onkeydown);
    }

    if (getSize() != null) {
      String size =
          EvaluateurExpression.evaluerString("size", String.valueOf(getSize()),
              this, pageContext);
      setSize(size);
    }

    if (getMaxlength() != null) {
      String maxLength =
          EvaluateurExpression.evaluerString("maxLength", String.valueOf(getMaxlength()),
              this, pageContext);
      setMaxlength(maxLength);
    }

    if (getDivRetourAjax() != null) {
      String divRetourAjax =
          EvaluateurExpression.evaluerString("divRetourAjax",
              String.valueOf(getDivRetourAjax()), this, pageContext);
      setDivRetourAjax(divRetourAjax);
    }

    if (getAjaxJSApresChargement() != null) {
      String ajaxJSApresChargement =
          EvaluateurExpression.evaluerString("ajaxJSApresChargement",
              String.valueOf(getAjaxJSApresChargement()), this, pageContext);
      setAjaxJSApresChargement(ajaxJSApresChargement);
    }

    if (getIconeAide() != null) {
      String iconeAide =
          EvaluateurExpression.evaluerString("iconeAide", String.valueOf(getIconeAide()),
              this, pageContext);
      setIconeAide(iconeAide);
    }
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getFormat() {
    return format;
  }

  public void setFormatSortie(String formatSortie) {
    this.formatSortie = formatSortie;
  }

  public String getFormatSortie() {
    return formatSortie;
  }

  public void setFormatMessageErreur(String formatMessageErreur) {
    this.formatMessageErreur = formatMessageErreur;
  }

  public String getFormatMessageErreur() {
    return formatMessageErreur;
  }

  public void setFormatSaisie(String formatSaisie) {
    this.formatSaisie = formatSaisie;
  }

  public String getFormatSaisie() {
    return formatSaisie;
  }

  public void setLigneStyleClass(String ligneStyleClass) {
    this.ligneStyleClass = ligneStyleClass;
  }

  public String getLigneStyleClass() {
    return ligneStyleClass;
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setObligatoireEL(String obligatoireEL) {
    this.obligatoireEL = obligatoireEL;
  }

  public String getObligatoireEL() {
    return obligatoireEL;
  }

  public void setGenererLigne(boolean genererLigne) {
    this.genererLigne = genererLigne;
  }

  public boolean isGenererLigne() {
    return genererLigne;
  }

  /**
   * Fixer true si vous désirez l'aide d'aide a gauche du libellé.
   * @param iconeAideAGauche true si vous désirez l'aide d'aide a gauche du libellé.
   */
  public void setIconeAideAGauche(String iconeAideAGauche) {
    this.iconeAideAGauche = iconeAideAGauche;
  }

  /**
   * Est-ce que l'icone d'aide doit être fixé a gauche du libellé.
   * @return true si l'icone d'aide doit être fixé a gauche du libellé.
   */
  public String getIconeAideAGauche() {
    return iconeAideAGauche;
  }

  public boolean isAffichageIconeAideAGauche() {
    return (getIconeAideAGauche() != null) &&
        getIconeAideAGauche().toString().toLowerCase().equals("true");
  }

  /**
   * Afficher une icone d'aide lorsque aide contextuelle.
   * @param iconeAide true si afficher une icone d'aide lorsque aide contextuelle.
   */
  public void setIconeAide(String iconeAide) {
    this.iconeAide = iconeAide;
  }

  /**
   * Indique si afficher une icone d'aide lorsque aide contextuelle.
   * @return true si afficher une icone d'aide lorsque aide contextuelle.
   */
  public String getIconeAide() {
    return iconeAide;
  }

  /**
   * Indique si afficher une icone d'aide lorsque aide contextuelle.
   * @return true si afficher une icone d'aide lorsque aide contextuelle.
   */
  private boolean isAffichageIconeAide() {
    return (getIconeAide() != null) &&
        getIconeAide().toString().toLowerCase().equals("true");
  }

  public void setAffichageSeulement(Object affichageSeulement) {
    this.affichageSeulement = affichageSeulement;
  }

  public Object getAffichageSeulement() {
    return affichageSeulement;
  }

  /**
   * Est-ce que le champ de saisie est en affichage seulement.
   * @return true si le champ de saisie est affichage seulement.
   */
  public boolean isAffichage() {
    return (getAffichageSeulement() != null) &&
        getAffichageSeulement().toString().toLowerCase().equals("true");
  }

  /**
   * Fixer true si on veut l'affichage de l'indicateur obligatoire.
   * @param affichageIndObligatoire true si on veut l'affichage de l'indicateur obligatoire.
   */
  public void setAffichageIndObligatoire(Object affichageIndObligatoire) {
    this.affichageIndObligatoire = affichageIndObligatoire;
  }

  /**
   * Retourne true si on veut l'affichage de l'indicateur obligatoire.
   * @return true si on veut l'affichage de l'indicateur obligatoire.
   */
  public Object getAffichageIndObligatoire() {
    return affichageIndObligatoire;
  }

  /**
   * Est-ce que l'affichage de l'indicateur obligatoire est requis.
   */
  private boolean isAffichageIndObligatoire() {
    if ((getAffichageIndObligatoire() != null) &&
        getAffichageIndObligatoire().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer true si vous désirez formatter la valeur du champ en nombre.
   * @param formatterEnNombre true si vous désirez formatter la valeur du champ en nombre
   */
  public void setFormatterEnNombre(boolean formatterEnNombre) {
    this.formatterEnNombre = formatterEnNombre;
  }

  /**
   * Retourne true si vous désirez formatter la valeur du champ en nombre.
   * @return true si vous désirez formatter la valeur du champ en nombre.
   */
  public boolean isFormatterEnNombre() {
    return formatterEnNombre;
  }

  /**
   * Fixer la valeur monétaire doit être appliqué.
   * @param valeurMonetaire la valeur monétaire doit être appliqué
   */
  public void setValeurMonetaire(boolean valeurMonetaire) {
    this.valeurMonetaire = valeurMonetaire;
  }

  /**
   * Est-ce que la valeur monétaire doit être appliqué.
   * @return true si la valeur monétaire doit être appliqué
   */
  public boolean isValeurMonetaire() {
    return valeurMonetaire;
  }

  /**
   * Fixer une valeur par défaut.
   * @param valeurDefaut une valeur par défaut.
   */
  public void setValeurDefaut(String valeurDefaut) {
    this.valeurDefaut = valeurDefaut;
  }

  /**
   * Retourne la valeur par défaut.
   * @return la valeur par défaut.
   */
  public String getValeurDefaut() {
    return valeurDefaut;
  }

  /**
   * Fixer le href ajax a executé
   * @param href le href ajax a executé
   */
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Retourne le href ajax a executé.
   * @return le href ajax a executé.
   */
  public String getHref() {
    return href;
  }

  /**
   * Fixer le nom de parametre qui correspond à la valeur selectionné.
   * @param nomParametreValeur le nom de parametre qui correspond à la valeur selectionné.
   */
  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  /**
   * Retourne le nom de parametre qui correspond à la valeur selectionné.
   * @return le nom de parametre qui correspond à la valeur selectionné.
   */
  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  /**
   * Fixer le div que l'on doit utiliser pour référer la réponse ajax.
   * @param divRetourAjax le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public void setDivRetourAjax(String divRetourAjax) {
    this.divRetourAjax = divRetourAjax;
  }

  /**
   * Retourne le div que l'on doit utiliser pour référer la réponse ajax.
   * @return le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public String getDivRetourAjax() {
    return divRetourAjax;
  }

  /**
   * Fixer le traitement JavaScript a executé pendant le chargement ajax
   * @param ajaxJSPendantChargement le traitement JavaScript a executé pendant le chargement ajax
   */
  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé pendant le chargement ajax
   * @return le traitement JavaScript a executé pendant le chargement ajax
   */
  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  /**
   * Fixer le traitement JavaScript a executé après le chargement.
   * @param ajaxJSApresChargement le traitement JavaScript a executé après le chargement.
   */
  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé après le chargement.
   * @return le traitement JavaScript a executé après le chargement.
   */
  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  /**
   * Fixer le traitement JavaScript Prérequis au chargement ajax.
   * @param ajaxJSPreRequis le traitement JavaScript Prérequis au chargement ajax.
   */
  public void setAjaxJSPreRequis(String ajaxJSPreRequis) {
    this.ajaxJSPreRequis =
        UtilitaireString.convertirEnJavaScript(ajaxJSPreRequis);
  }

  /**
   * Retourne le traitement JavaScript Prérequis au chargement ajax.
   * @return le traitement JavaScript Prérequis au chargement ajax.
   */
  public String getAjaxJSPreRequis() {
    return ajaxJSPreRequis;
  }

  /**
   * Fixer l'identifiant dont l'on désire modifier la valeur en ajax.
   * @param idRetourAjax l'identifiant dont l'on désire modifier la valeur en ajax.
   */
  public void setIdRetourAjax(String idRetourAjax) {
    this.idRetourAjax = idRetourAjax;
  }

  /**
   * Retourne l'identifiant dont l'on désire modifier la valeur en ajax.
   * @return l'identifiant dont l'on désire modifier la valeur en ajax.
   */
  public String getIdRetourAjax() {
    return idRetourAjax;
  }

  /**
   * Spécifie si plusieurs id de la page va être modifié en ajax.
   * @param idRetourMultipleAjax true si plusieurs id de la page va être modifié en ajax.
   */
  public void setIdRetourMultipleAjax(boolean idRetourMultipleAjax) {
    this.idRetourMultipleAjax = idRetourMultipleAjax;
  }

  /**
   * Fixer true si plusieurs id de la page va être modifié en ajax.
   * @return true si plusieurs id de la page va être modifié en ajax.
   */
  public boolean isIdRetourMultipleAjax() {
    return idRetourMultipleAjax;
  }

  /**
   * Fixer si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   * @param transationnel true si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   */
  public void setTransactionnel(String transactionnel) {
    this.transactionnel = transactionnel;
  }

  /**
   * Retourne true  si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   * @return true  si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   */
  public String getTransactionnel() {
    return transactionnel;
  }

  /**
   * Retourne true  si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   * @return true  si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   */
  public boolean isTraitementTransactionnel() {
    if ((getTransactionnel() != null) &&
        getTransactionnel().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }
}
