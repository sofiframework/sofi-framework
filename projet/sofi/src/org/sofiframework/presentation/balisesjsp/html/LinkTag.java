/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise JSP permetttant la création d'un lien hypertexte.
 *
 * @author Jean-François Brassard
 * @version SOFI 1.0
 * @since SOFI 1.8 Fonctionnalité ajax disponible.
 * Support de section éclatable en mode ajax.
 * Permet de faire appel à la barre de statut en mode ajax.
 * Permet d'alerter l'utilisateur qu'il tente d'accéder à un autre service, mais
 * qu'il va perdre les données de son formulaire transactionnel s'il n'enregistre
 * pas au préalable.
 */
public class LinkTag extends org.apache.struts.taglib.html.LinkTag {
  /**
   * 
   */
  private static final long serialVersionUID = -7787977969195196019L;
  private String libelle = null;
  private String aideContextuelle = null;
  private String messageConfirmation = null;
  private boolean ajax = false;
  private String fonctionJS;
  private String var;
  private String ajaxJSPendantChargement;
  private String ajaxJSApresChargement;
  private String ajaxJSPreRequis;

  /**
   * Spécifie que le formulaire à été modifié
   */
  private boolean notifierModificationFormulaire;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax.
   */
  private String divRetourAjax = null;

  /**
   * L'identifiant de la page dont l'on désire modifier la valeur dans
   * un traitement ajax.
   */
  private String idRetourAjax = null;

  /**
   * Spécifie qu'il va avoir plusieurs identifiant de retour dans la
   * réponse avec l'aide d'un document XML
   */
  protected boolean idRetourMultipleAjax = false;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  private String nomParametreValeur = null;

  /**
   * L'id de la propriété dont on désire la valeur.
   */
  private String idProprieteValeur = null;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax à la barre des messages.
   */
  private String divBarreStatutAjax = null;

  /**
   * Est-ce que la barre de statut est ajax.
   */
  private boolean barreStatutAjax;

  /**
   * Spécifie si on désire faire ajouter un message de confirmation
   * que le formulaire a été modifié et que si l'utilisateur poursuit
   * alors les dernières modifications seront perdues.
   */
  private boolean activerConfirmationModificationFormulaire;

  /**
   * Spécifie si la section appelé en ajax est éclatable, donc peut s'offrir
   * et se fermer.
   */
  private boolean ajaxEclatable;

  /**
   * Permet d'instruire au tag div que son propre contenu est persistant dans le
   * temps (tout au long de la session de l'utilisateur). Pour utiliser cette
   * fonctionnalité, il faut au minimum qu'un lien HREF soit impliqué pour
   * éclater ou non le contenu du DIV.
   */
  private boolean ajaxEclatablePersistant = false;

  /**
   * Ne pas traiter la fermeture automatique lors d'un lien ajax éclatable.
   */
  private boolean ajaxEclatableFermetureNonAutomatique = false;

  /**
   * Spécifie le style CSS a appliquer lorsque la section éclatable est
   * ouverte.
   */
  private String ajaxStyleClassSectionOuverte;

  /**
   * Spécifie le style CSS a appliquer lorsque la section éclatable est
   * fermée.
   */
  private String ajaxStyleClassSectionFermee;

  /**
   * Spécifie si vous désiez mémoriser l'adresse affiché avant d'appeler
   * un nouveau traitement par ce lien. Permet ainsi de faciliter le retour
   * à la tâche précédente.
   */
  private boolean memoriserHrefActuel;

  /**
   * Spécifie de générer un lien hypertexte seulement s'il y a un adresse
   * de retour de disponible. Pour ce faire, il doit avoir au problème un appel
   * de sofi-html:link avec le paramètre memoriserHrefActurl à true.
   * @toto renomer pour appliquerHrefRetour;
   */
  private boolean appliquerAdresseRetour;

  /**
   * Permet de spécifier le href présentement en cours avec les paramètres d'associés.
   */
  private boolean appliquerHrefAvecParametres;


  // Inclure le certificat d'authentification dans l'url appelé.
  private boolean inclureCertificatDansUrl = false;

  /**
   * Génération d'un début du lien hypertexte.
   * @exception JspException si une exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    // Ajouter des paramètres par défaut s'il y a lieu.
    init();

    // Style si div ouvert ou fermé à traiter.
    if ((getAjaxStyleClassSectionFermee() != null) &&
        (UtilitaireControleur.isDivOuvert(getDivRetourAjax(), request) !=
        null)) {
      if (UtilitaireControleur.isDivOuvert(getDivRetourAjax(),
          request).booleanValue()) {
        setStyleClass(getAjaxStyleClassSectionOuverte());
      } else {
        setStyleClass(getAjaxStyleClassSectionFermee());
      }
    }

    String codeHtml = null;
    boolean skipBody = false;

    // Traitement des ancres
    if (linkName != null) {
      codeHtml =
          MessageFormat.format("<a name=\"{0}\">", new Object[] { linkName });
    } else if ((getHref() != null) && !isAucunUrlPourAppliquerRetour()) {
      // Vérifier la sécurité lié au bouton
      GestionSecurite gestionSecurite = GestionSecurite.getInstance();

      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        Utilisateur utilisateur =
            UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

        // L'utilisateur à droit de voir le composant
        if (!UtilitaireBaliseJSP.isCacherLienInterdit(href, request)) {
          if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
              this.libelle)) {
            // Générer le code représentant le composant HTML
            codeHtml = this.genererCodeHtmlComposant();
          }
        } else {
          skipBody = true;
        }
      } else {
        // Si on a pas de composant de sécurité, alors on ne la traite pas et on
        // affiche le composant comme le développeur la programme
        codeHtml = this.genererCodeHtmlComposant();
      }
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    } else {
      if (getVar() != null) {
        String varLink = (String)pageContext.getAttribute("varLink");
        pageContext.getRequest().setAttribute(getVar(), varLink);
      }
    }

    if (isAppliquerHrefAvecParametres() && (getVar() != null)) {
      Href avantDernierHrefAvecParametres =
          (Href)pageContext.getSession().getAttribute(Constantes.DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE);
      pageContext.setAttribute(getVar(),
          avantDernierHrefAvecParametres.getUrlPourRedirection());
    }

    return (skipBody ? SKIP_BODY : EVAL_BODY_INCLUDE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() throws JspException {
    /**
     * @since SOFI 2.0
     * Cette fonctionnalité permet de retenir si
     * on doit executer l'action AJAX afin de populer le DIV. Ceci ce fait dans
     * l'unique cas où le div est présent dans l'ensemble de DIV.
     */
    if (isAjaxEclatablePersistant()) {
      setHref(getHref() + "&divPersistant=" + getDivRetourAjax());
    }

    if (isNotifierModificationFormulaire()) {
      setHref(getHref() + "&indModification=1");
    }

    Message messageConfirmation = null;

    if (getMessageConfirmation() != null) {
      messageConfirmation =
          UtilitaireMessage.get(this.getMessageConfirmation(), null,
              pageContext);
    }

    // Génération du début du lien
    StringBuffer resultat = new StringBuffer();
    StringBuffer fonctionJS = new StringBuffer();

    resultat.append("<a href=\"");

    if ((messageConfirmation == null) && !isAjax() && (this.href != null)) {
      if (isActiverConfirmationModificationFormulaire()) {
        resultat.append("javascript:go('");
      }


      resultat.append(calculateURL());


      if (isActiverConfirmationModificationFormulaire()) {
        resultat.append("');");
      }

      resultat.append("\"");
    } else {
      resultat.append("javascript:");
    }

    StringBuffer onClickBuffer = new StringBuffer();

    if (messageConfirmation != null && !isBarreStatutAjax()) {
      if (getOnclick() != null) {
        onClickBuffer.append(getOnclick());
      }

      fonctionJS.append("if (confirm('");
      fonctionJS.append(UtilitaireString.convertirEnJavaScript(messageConfirmation.getMessage()));
      fonctionJS.append("')) {");

      if (isAjax()) {
        if (getAjaxJSPreRequis() != null) {
          fonctionJS.append(getAjaxJSPreRequis());
        }

        String url = calculateURL();

        fonctionJS.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(getHref(),
            getDivRetourAjax(),
            getNomParametreValeur(),
            getStyleId(),
            getAjaxJSPendantChargement(),
            getAjaxJSApresChargement(),
            pageContext));

        if (getFonctionJS() != null) {
          fonctionJS.append(getFonctionJS());
        }
      } else {
        fonctionJS.append("window.location='");
        fonctionJS.append(calculateURL());
        fonctionJS.append("';");
      }

      fonctionJS.append("}");
    } else {
      if (isAjax()) {

        if (messageConfirmation != null) {

          fonctionJS.append("if (confirm('");
          fonctionJS.append(UtilitaireString.convertirEnJavaScript(messageConfirmation.getMessage()));
          fonctionJS.append("')) {");

        }

        if (isIdRetourMultipleAjax()) {
          setDivRetourAjax("REPONSE_MULTIPLE");
        }

        if (getIdRetourAjax() != null) {
          setDivRetourAjax(getIdRetourAjax());
        }

        StringBuffer lienAjaxComplet = new StringBuffer();

        if (getAjaxJSPreRequis() != null) {
          fonctionJS.append(getAjaxJSPreRequis());
        }

        if (isActiverConfirmationModificationFormulaire()) {
          fonctionJS.append("if (messageModificationFormulaire && document.forms[0].indModification.value == '1') {");

          fonctionJS.append("if (confirm(messageModificationFormulaire)) {");
          fonctionJS.append("display('modificationFormulaire', false);display('messageFormulaire', true);");

          Href href = new Href(getHref());
          href.ajouterParametre("indModification", "0");
          setHref(href.getUrlPourRedirection());
        } else {
          Href href = new Href(getHref());

          setHref(href.getUrlPourRedirection());
        }

        if (isBarreStatutAjax()) {
          String actionBarreStatut =
              GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.ACTION_BARRE_STATUT_AJAX);

          if (actionBarreStatut.equals("")) {
            // Spécifier un message par défaut.
            actionBarreStatut = "barreStatutAjax.do?methode=afficher";
          }

          String divBarreStatutAjax = getDivBarreStatutAjax();

          if (getDivBarreStatutAjax() == null) {
            // Spécifie l'identifiant du div par défaut de la barre de statut.
            divBarreStatutAjax = "message_erreur";
          }

          lienAjaxComplet.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchroneAvecMessage(getHref(),
              getDivRetourAjax(),
              getNomParametreValeur(),
              getIdProprieteValeur(),
              actionBarreStatut,
              divBarreStatutAjax,
              getAjaxJSPendantChargement(),
              getAjaxJSApresChargement(),
              pageContext));
        } else {
          lienAjaxComplet.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(getHref(),
              getDivRetourAjax(),
              getNomParametreValeur(),
              getIdProprieteValeur(),
              getAjaxJSPendantChargement(),
              getAjaxJSApresChargement(),
              pageContext));
        }

        if (getVar() != null) {
          setFonctionJS(lienAjaxComplet.toString());
        }

        if (isActiverConfirmationModificationFormulaire()) {
          fonctionJS.append("document.forms[0].indModification.value = '0';");
          fonctionJS.append(lienAjaxComplet.toString());
          fonctionJS.append("}");
          fonctionJS.append("}");
          fonctionJS.append("else  {");
          fonctionJS.append(lienAjaxComplet.toString());
          fonctionJS.append("}");
        } else {
          fonctionJS.append(lienAjaxComplet.toString());
        }

        if (messageConfirmation != null) {

          fonctionJS.append("}");
        }
      }
    }

    if (getVar() != null) {
      if (fonctionJS.toString().length() == 0) {
        pageContext.setAttribute("varLink", getHref());
      } else {
        pageContext.setAttribute("varLink", fonctionJS.toString());
      }

      return null;
    }

    //    Correctif SOFI 1.8 Ne plus insérer les fonctions jS sur le onclick mais sur le HREF
    //    if (fonctionJS.length() > 0) {
    //      onClickBuffer.append(fonctionJS);
    //    }
    if (onClickBuffer.length() > 1) {
      setOnclick(onClickBuffer.toString());
    }

    if (fonctionJS.length() > 0) {
      // Correctif SOFI 1.8 Ne plus insérer les fonctions jS sur le onclick mais sur le HREF
      resultat.append(fonctionJS);
      resultat.append("\"");
    }

    if (target != null) {
      resultat.append(" target=\"");
      resultat.append(target);
      resultat.append("\"");
    }

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    Libelle libelle =
        UtilitaireLibelle.getLibelle(getLibelle(), pageContext, null);

    if (UtilitaireString.isVide(getAideContextuelle())) {
      if (libelle.getAideContextuelle() != null) {
        resultat.append(" title=\"");
        resultat.append(libelle.getAideContextuelle());
        resultat.append("\"");
      }
    } else {
      Libelle libelleAideContextuelle =
          UtilitaireLibelle.getLibelle(getAideContextuelle(), pageContext, null);

      if (!UtilitaireString.isVide(libelleAideContextuelle.getMessage())) {
        resultat.append(" title=\"");
        resultat.append(libelleAideContextuelle.getMessage());
        resultat.append("\"");
      }
    }

    resultat.append(prepareStyles());
    resultat.append(prepareEventHandlers());
    resultat.append(">");

    if ((libelle != null) && (libelle.getMessage() != null)) {
      resultat.append(libelle.getMessage());
    }

    return resultat.toString();
  }

  /**
   * Traiter la fermeture de la balise.
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    if ((getVar() == null) &&
        (!isAppliquerAdresseRetour() || (isAppliquerAdresseRetour() &&
            !isAucunUrlPourAppliquerRetour()))) {
      StringBuffer results = new StringBuffer();

      if (text != null) {
        results.append(text);
      }

      results.append("</a>");

      TagUtils.getInstance().write(pageContext, results.toString());
    }

    this.release();

    return (EVAL_PAGE);
  }

  /**
   * Libérer les ressouces.
   */
  @Override
  public void release() {
    super.release();
    libelle = null;
    href = null;
    ajaxEclatable = false;
    ajaxJSApresChargement = null;
    ajaxJSPendantChargement = null;
    ajaxJSPreRequis = null;
    ajaxStyleClassSectionFermee = null;
    ajaxStyleClassSectionOuverte = null;
    ajaxEclatableFermetureNonAutomatique = false;
    ajaxEclatablePersistant = false;
  }

  /**
   * Fixer le libellé
   * @param libelle le libellé
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Retourne le libellé du lien hypertexte
   * @return le libellé du lien hypertexte
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Fixer l'aide contextuelle
   * @param aideContextuelle  l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne l'aide contextuelle
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  private void init() {
    String urlRetour =
        (String)pageContext.getSession().getAttribute(Constantes.ADRESSE_ACTION_RETOUR);

    if (urlRetour != null) {
      urlRetour = UtilitaireString.remplacerTous(urlRetour, "$P",
          "&");
    }

    if (isAppliquerAdresseRetour() && (urlRetour != null)) {
      href = urlRetour;
    }

    if (getHref() != null) {
      Href hrefAvecParam = new Href(href);

      if (isAjaxEclatableFermetureNonAutomatique()) {
        hrefAvecParam.ajouterParametre(ConstantesBaliseJSP.AJAX_ECLATABLE_FERMETURE_NON_AUTOMATIQUE,
            "true");
      }

      if (isAjaxEclatable()) {
        hrefAvecParam.ajouterParametre("sofi_eclatable", "true");
      }

      if (isMemoriserHrefActuel()) {
        hrefAvecParam.ajouterParametre("sofi_memoriser_href_actuel", "true");
        Href dernierHrefAvecParametres =
            (Href)pageContext.getSession().getAttribute(Constantes.DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE);


        if (dernierHrefAvecParametres.getListeParametres().containsKey("sofi_url_retour")) {
          dernierHrefAvecParametres.supprimerParametre("sofi_url_retour");
        }

        String urlComplet = dernierHrefAvecParametres.toString();

        urlComplet = UtilitaireString.encoderUrl(urlComplet);

        hrefAvecParam.ajouterParametre("sofi_url_retour", urlComplet);
      }

      if (isAppliquerAdresseRetour()) {
        hrefAvecParam.ajouterParametre("sofi_lien_retour", "true");
      }

      setHref(hrefAvecParam.getUrlPourRedirection());
    }

    if (getStyleId() == null) {
      setStyleId(UtilitaireBaliseJSP.getNomProprieteVide(pageContext));
    }

    if ((getAjaxStyleClassSectionOuverte() != null) &&
        (getAjaxStyleClassSectionFermee() != null) &&
        (getDivRetourAjax() != null)) {
      StringBuffer ajaxJSApres = new StringBuffer();

      if (ajaxJSApresChargement != null) {
        ajaxJSApres.append(ajaxJSApresChargement);
      }

      if ((ajaxJSApresChargement != null) &&
          (ajaxJSApresChargement.indexOf(";") == -1)) {
        ajaxJSApres.append(";");
      }

      ajaxJSApres.append("setStyleClassSectionEclatable('");
      ajaxJSApres.append(getStyleId());
      ajaxJSApres.append("','");
      ajaxJSApres.append(getDivRetourAjax());
      ajaxJSApres.append("','");
      ajaxJSApres.append(getAjaxStyleClassSectionOuverte());
      ajaxJSApres.append("','");
      ajaxJSApres.append(getAjaxStyleClassSectionFermee());
      ajaxJSApres.append("');");
      setAjaxJSApresChargement(ajaxJSApres.toString());
    } else {
      setAjaxJSApresChargement(ajaxJSApresChargement);
    }

    if (getIdProprieteValeur() != null) {
      setIdProprieteValeur(NestedPropertyHelper.getAdjustedProperty((HttpServletRequest)pageContext.getRequest(),
          getIdProprieteValeur()));
    }
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du lien hypertexte.
   * @throws javax.servlet.jsp.JspException
   */
  private void evaluerEL() throws JspException {
    try {
      String valeurELHref =
          EvaluateurExpression.evaluerString("href", getHref(), this,
              pageContext);

      setHref(valeurELHref);

      String libelle =
          EvaluateurExpression.evaluerString("libelle", getLibelle(), this,
              pageContext);
      setLibelle(libelle);

      String styleCSS =
          EvaluateurExpression.evaluerString("styleClass", getStyleClass(), this,
              pageContext);
      setStyleClass(styleCSS);

      String styleId =
          EvaluateurExpression.evaluerString("styleId", getStyleId(), this,
              pageContext);

      setStyleId(styleId);

      String divAjaxRetour =
          EvaluateurExpression.evaluerString("divAjaxRetour", getDivRetourAjax(),
              this, pageContext);
      setDivRetourAjax(divAjaxRetour);

      if (getMessageConfirmation() != null) {
        String messageConfirmation =
            EvaluateurExpression.evaluerString("messageConfirmation",
                getMessageConfirmation(), this,
                pageContext);
        setMessageConfirmation(messageConfirmation);
      }

      String ajaxJSPendantChargement =
          EvaluateurExpression.evaluerString("ajaxJSPendantChargement",
              getAjaxJSPendantChargement(), this,
              pageContext);
      setAjaxJSPendantChargement(ajaxJSPendantChargement);

      String ajaxJSApresChargement =
          EvaluateurExpression.evaluerString("ajaxJSApresChargement",
              getAjaxJSApresChargement(), this,
              pageContext);
      setAjaxJSApresChargement(ajaxJSApresChargement);

      String ajaxJSPreRequis =
          EvaluateurExpression.evaluerString("ajaxJSPreRequis",
              getAjaxJSPreRequis(), this,
              pageContext);
      setAjaxJSPreRequis(ajaxJSPreRequis);

      String aideContextuelle =
          EvaluateurExpression.evaluerString("aideContextuelle",
              getAideContextuelle(), this,
              pageContext);
      setAideContextuelle(aideContextuelle);
    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Fixer le message de confirmation a appliquer lors de l'appel du lien.
   * @param messageConfirmation le message de confirmation a appliquer lors de l'appel du lien.
   */
  public void setMessageConfirmation(String messageConfirmation) {
    this.messageConfirmation = messageConfirmation;
  }

  /**
   * Retourne le message de confirmation a appliquer lors de l'appel du lien.
   * @return le message de confirmation a appliquer lors de l'appel du lien.
   */
  public String getMessageConfirmation() {
    return messageConfirmation;
  }

  /**
   * Fixer true si on désire la fonctionnalité ajax.
   * @param ajax true si on désire la fonctionnalité ajax.
   */
  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  /**
   * Est-ce qu'on désire la fonctinnalité ajax du lien.
   * @return true si désire la fonctinnalité ajax du lien.
   */
  public boolean isAjax() {
    return ajax;
  }

  /**
   * Fixer une fonction javascript lors d'un traitement ajax.
   * @param fonctionJS une fonction javascript lors d'un traitement ajax.
   */
  public void setFonctionJS(String fonctionJS) {
    this.fonctionJS = fonctionJS;
  }

  /**
   * Retourne une fonction javascript lors d'un traitement ajax.
   * @return une fonction javascript lors d'un traitement ajax.
   */
  public String getFonctionJS() {
    return fonctionJS;
  }

  /**
   * Fixer le code généré par la balise dans un variable temporaire afin
   * de réutilisation.
   * @param var le code généré par la balise dans un variable temporaire afin
   * de réutilisation.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le code généré par la balise dans un variable temporaire afin
   * de réutilisation.
   * @return le code généré par la balise dans un variable temporaire afin
   * de réutilisation.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer le div qui permet d'afficher la barre de statut en ajax.
   * @param divBarreStatutAjax le div qui permet d'afficher la barre de statut en ajax.
   */
  public void setDivBarreStatutAjax(String divBarreStatutAjax) {
    this.divBarreStatutAjax = divBarreStatutAjax;
  }

  /**
   * Retourne le div qui permet d'afficher la barre de statut en ajax.
   * @return le div qui permet d'afficher la barre de statut en ajax.
   */
  public String getDivBarreStatutAjax() {
    return divBarreStatutAjax;
  }

  /**
   * Fixer true si on désire appliquer la barre de statut
   * en ajax après le traitement du lien.
   * @param barreStatutAjax true si on désire appliquer la barre de statut
   * en ajax après le traitement du lien.
   */
  public void setBarreStatutAjax(boolean barreStatutAjax) {
    this.barreStatutAjax = barreStatutAjax;
  }

  /**
   * Retourne si on désire appliquer la barre de statut
   * en ajax après le traitement du lien.
   * @return true  si on désire appliquer la barre de statut
   * en ajax après le traitement du lien.
   */
  public boolean isBarreStatutAjax() {
    return barreStatutAjax;
  }

  /**
   * Fixer le div que l'on doit utiliser pour référer la réponse ajax.
   * @param divRetourAjax le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public void setDivRetourAjax(String divRetourAjax) {
    this.divRetourAjax = divRetourAjax;
  }

  /**
   * Retourne le div que l'on doit utiliser pour référer la réponse ajax.
   * @return le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public String getDivRetourAjax() {
    return divRetourAjax;
  }

  /**
   * Fixer le traitement JavaScript a executé pendant le chargement ajax
   * @param ajaxJSPendantChargement le traitement JavaScript a executé pendant le chargement ajax
   */
  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé pendant le chargement ajax
   * @return le traitement JavaScript a executé pendant le chargement ajax
   */
  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  /**
   * Fixer le traitement JavaScript a executé après le chargement.
   * @param ajaxJSApresChargement le traitement JavaScript a executé après le chargement.
   */
  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé après le chargement.
   * @return le traitement JavaScript a executé après le chargement.
   */
  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  /**
   * Fixer le traitement JavaScript Prérequis au chargement ajax.
   * @param ajaxJSPreRequis le traitement JavaScript Prérequis au chargement ajax.
   */
  public void setAjaxJSPreRequis(String ajaxJSPreRequis) {
    this.ajaxJSPreRequis = ajaxJSPreRequis;
  }

  /**
   * Retourne le traitement JavaScript Prérequis au chargement ajax.
   * @return le traitement JavaScript Prérequis au chargement ajax.
   */
  public String getAjaxJSPreRequis() {
    return ajaxJSPreRequis;
  }

  /**
   * Fixer true si vous désirez qu'il appel un message d'avertissement
   * afin d'alerter l'utilisateur qu'il tente de changer de service après avoir
   * modifié un formulaire transactionnel.
   * @param avertisementFormulaireModifie true si vous désirez afficher un
   * message d'avertisssement lorsque l'utilisateur tente d'aller vers
   * un autre service après avoir modifié un formulaire transactionel.
   */
  public void setActiverConfirmationModificationFormulaire(boolean activerConfirmationModificationFormulaire) {
    this.activerConfirmationModificationFormulaire =
        activerConfirmationModificationFormulaire;
  }

  /**
   * Indique si vous désirez qu'il appel un message d'avertissement
   * afin d'alerter l'utilisateur qu'il tente de changer de service après avoir
   * modifié un formulaire transactionnel.
   * @return true si vous désirez qu'il appel un message d'avertissement
   * afin d'alerter l'utilisateur qu'il tente de changer de service après avoir
   * modifié un formulaire transactionnel.
   */
  public boolean isActiverConfirmationModificationFormulaire() {
    return activerConfirmationModificationFormulaire;
  }

  /**
   * Fixer true si vous désirez un traitement de section éclatable.
   * @param ajaxEclatable true si vous désirez un traitement de section éclatable.
   */
  public void setAjaxEclatable(boolean ajaxEclatable) {
    this.ajaxEclatable = ajaxEclatable;
  }

  /**
   * Est-ce que le composant doit appliquer la notion de section éclatable.
   * @return true si le composant doit appliquer la notion de section éclatable.
   */
  public boolean isAjaxEclatable() {
    return ajaxEclatable;
  }

  /**
   * Fixer le style CSS a appliquer lorsque la section est ouverte.
   * @param ajaxStyleClassOuvert le style CSS a appliquer lorsque la section est ouverte.
   */
  public void setAjaxStyleClassSectionOuverte(String ajaxStyleClassSectionOuverte) {
    this.ajaxStyleClassSectionOuverte = ajaxStyleClassSectionOuverte;
  }

  /**
   * Retourne le style CSS a appliquer lorsque la section est ouverte.
   * @return le style CSS a appliquer lorsque la section est ouverte.
   */
  public String getAjaxStyleClassSectionOuverte() {
    return ajaxStyleClassSectionOuverte;
  }

  /**
   * Fixer le style CSS a appliquer lorsque la section est fermée.
   * @param ajaxStyleClassOuvert le style CSS a appliquer lorsque la section est fermée.
   */
  public void setAjaxStyleClassSectionFermee(String ajaxStyleClassSectionFermee) {
    this.ajaxStyleClassSectionFermee = ajaxStyleClassSectionFermee;
  }

  /**
   * Retourne le style CSS a appliquer lorsque la section est fermée.
   * @return le style CSS a appliquer lorsque la section est fermée.
   */
  public String getAjaxStyleClassSectionFermee() {
    return ajaxStyleClassSectionFermee;
  }

  /**
   * Fixer le style id de la balise HTML à modifier avec la valeur de retour de la fonction ajax.
   * @param idRetourAjax le style id de la balise HTML à modifier avec la valeur de retour de la fonction ajax.
   */
  public void setIdRetourAjax(String idRetourAjax) {
    this.idRetourAjax = idRetourAjax;
  }

  /**
   * Retourne le style id de la balise HTML à modifier avec la valeur de retour de la fonction ajax.
   * @return le style id de la balise HTML à modifier avec la valeur de retour de la fonction ajax.
   */
  public String getIdRetourAjax() {
    return idRetourAjax;
  }

  /**
   * Fixer true s'il y a plusieurs identifiants de balise HTML à traiter dans la réponse.
   * @param idRetourMultipleAjax true s'il y a plusieurs identifiants de balise HTML à traiter dans la réponse.
   */
  public void setIdRetourMultipleAjax(boolean idRetourMultipleAjax) {
    this.idRetourMultipleAjax = idRetourMultipleAjax;
  }

  /**
   * Est-ce qu'il y a plusieurs identifiants de balise HTML à traiter dans la réponse.
   * @return true s'il y a plusieurs identifiants de balise HTML à traiter dans la réponse.
   */
  public boolean isIdRetourMultipleAjax() {
    return idRetourMultipleAjax;
  }

  /**
   * Fixer true si le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @param nomParametreValeur
   */
  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  /**
   * Retourne le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @return le nom de paramètre qui est associé à la valeur
   * sélectionnée.
   */
  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  /**
   * Fixer le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   * <p>
   * Il est possible de spécifier plusieurs propriété correspondant
   * au paramètre spécifié en séparant les propriétés de virgule.
   * @param idProprieteValeur le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   */
  public void setIdProprieteValeur(String idProprieteValeur) {
    this.idProprieteValeur = idProprieteValeur;
  }

  /**
   * Retourne le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   * <p>
   * Il est possible de spécifier plusieurs paramètre en séparant
   * les paramètres de virgule.
   * @return le style id de la propriété dont on désire extraire la valeur
   * et l'associer la valeur au nom de paramètre spécifié.
   */
  public String getIdProprieteValeur() {
    return idProprieteValeur;
  }

  /**
   * Fixer true si on désire mémoriser l'adresse actuellement consulté afin d'offrir la
   * possibilté de retour vers la taches précedent lors que vous spécifié la propriété
   * appliquerAdresseRetour. (L'url est logé dans la variable de session
   * nommé (SOFIadresseActionRetour).
   * @param memoriserHrefActuel true si on désire mémoriser l'adresse actuellement consulté
   */
  public void setMemoriserHrefActuel(boolean memoriserHrefActuel) {
    this.memoriserHrefActuel = memoriserHrefActuel;
  }

  /**
   * Retourne true si on désire mémoriser l'adresse actuellement consulté afin d'offrir la
   * possibilté de retour vers la taches précedent lors que vous spécifié la propriété
   * appliquerAdresseRetour. (L'url est logé dans la variable de session
   * nommé (SOFIadresseActionRetour).
   * @return  true si on désire mémoriser l'adresse actuellement consulté
   */
  public boolean isMemoriserHrefActuel() {
    return memoriserHrefActuel;
  }

  /**
   * Fixer true si vous désirez générer un lien hypertexte seulement s'il y a un adresse
   * de retour de disponible. Pour ce faire, il doit avoir au problème un appel
   * de sofi-html:link avec le paramètre memoriserHrefActuel à true.
   * @param appliquerAdresseRetour true si vous désirez générer un lien hypertexte avec l'adresse de retour.
   */
  public void setAppliquerAdresseRetour(boolean appliquerAdresseRetour) {
    this.appliquerAdresseRetour = appliquerAdresseRetour;
  }

  /**
   * Retourne true si vous désirez générer un lien hypertexte seulement s'il y a un adresse
   * de retour de disponible. Pour ce faire, il doit avoir au problème un appel
   * de sofi-html:link avec le paramètre memoriserHrefActuel à true.
   * @return true si vous désirez générer un lien hypertexte avec l'adresse de retour.
   */
  public boolean isAppliquerAdresseRetour() {
    return appliquerAdresseRetour;
  }

  /**
   * Fixer true si on désire que le tag div spécifié par la propriété
   * divAjaxRetour offre la persistance du contenu oui ou non.
   * @param ajaxEclatablePersistant true si on désire que le tag div spécifié par la propriété
   * divAjaxRetour offre la persistance du contenu oui ou non.
   */
  public void setAjaxEclatablePersistant(boolean ajaxEclatablePersistant) {
    this.ajaxEclatablePersistant = ajaxEclatablePersistant;
  }

  /**
   * Retourne true si on désire que le tag div spécifié par la propriété
   * divAjaxRetour offre la persistance du contenu oui ou non.
   * @return true si on désire que le tag div spécifié par la propriété
   * divAjaxRetour offre la persistance du contenu oui ou non.
   */
  public boolean isAjaxEclatablePersistant() {
    return ajaxEclatablePersistant;
  }

  /**
   * Fixer true si on désire ne pas traiter la fermeture automatique lors d'un lien ajax éclatable.
   * Ceci permet de traiter l'action Java, le controleur n'exécutera pas automatiquement la fermeture.
   * @param ajaxEclatableFermetureNonAutomatique
   */
  public void setAjaxEclatableFermetureNonAutomatique(boolean ajaxEclatableFermetureNonAutomatique) {
    this.ajaxEclatableFermetureNonAutomatique =
        ajaxEclatableFermetureNonAutomatique;
  }

  /**
   * Retourne true si on désire ne pas traiter la fermeture automatique lors d'un lien ajax éclatable.
   * Ceci permet de traiter l'action Java, le controleur n'exécutera pas automatiquement la fermeture.
   * @return true si on désire ne pas traiter la fermeture automatique lors d'un lien ajax éclatable.
   * Ceci permet de traiter l'action Java, le controleur n'exécutera pas automatiquement la fermeture.
   */
  public boolean isAjaxEclatableFermetureNonAutomatique() {
    return ajaxEclatableFermetureNonAutomatique;
  }

  /**
   * Est-ce que c'est un traitement pour spécifier un lien hypertexte avec l'adresse de retour et qu'il
   * n'existe pas d'url a spécifier.
   * @return true si c'est un traitement pour spécifier un lien hypertexte avec l'adresse de retour.
   */
  private boolean isAucunUrlPourAppliquerRetour() {
    String urlRetour =
        (String)pageContext.getSession().getAttribute(Constantes.ADRESSE_ACTION_RETOUR);

    if (isAppliquerAdresseRetour() && (urlRetour == null)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer true si on désire spécifier le href présentement en cours
   * avec les paramètres d'associés.
   * @param appliquerHrefAvecParametres true si on désire spécifier le href présentement en cours
   * avec les paramètres d'associés.
   */
  public void setAppliquerHrefAvecParametres(boolean appliquerHrefAvecParametres) {
    this.appliquerHrefAvecParametres = appliquerHrefAvecParametres;
  }

  /**
   * Retourne si on désire spécifier le href présentement en cours
   * avec les paramètres d'associés.
   * @return true si on désire spécifier le href présentement en cours
   * avec les paramètres d'associés.
   */
  public boolean isAppliquerHrefAvecParametres() {
    return appliquerHrefAvecParametres;
  }

  @Override
  protected String calculateURL() throws JspException {
    // Identify the parameters we will add to the completed URL
    Map params =
        TagUtils.getInstance().computeParameters(pageContext, paramId, paramName,
            paramProperty, paramScope, name,
            property, scope, transaction);

    // if "indexed=true", add "index=x" parameter to query string
    // * @since Struts 1.1
    if (indexed) {
      int indexValue = getIndexValue();

      //calculate index, and add as a parameter
      if (params == null) {
        params = new HashMap(); //create new HashMap if no other params
      }

      if (indexId != null) {
        params.put(indexId, Integer.toString(indexValue));
      } else {
        params.put("index", Integer.toString(indexValue));
      }
    }

    String url = null;

    try {
      url =
          TagUtils.getInstance().computeURLWithCharEncoding(pageContext, forward,
              href, page, action,
              module, params,
              anchor, false,
              useLocalEncoding);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(pageContext, e);
      throw new JspException(messages.getMessage("rewrite.url", e.toString()));
    }

    if (inclureCertificatDansUrl) {
      Href urlAvecCertificat = new Href(url);
      String certificat =
          UtilitaireControleur.getCertificat((HttpServletRequest)pageContext.getRequest());

      if (certificat != null) {
        urlAvecCertificat.ajouterParametre(GestionSecurite.getInstance().getNomCookieCertificat(),
            certificat);
      }

      url = urlAvecCertificat.getUrlPourRedirection();
    }
    return url;
  }

  public void setNotifierModificationFormulaire(boolean notifierModificationFormulaire) {
    this.notifierModificationFormulaire = notifierModificationFormulaire;
  }

  public boolean isNotifierModificationFormulaire() {
    return notifierModificationFormulaire;
  }

  /**
   * Fixer si on désire inclure le certificat
   * d'authentification dans l'appel de la page qui doit
   * être inclus dynamiquement.
   * @param inclureCertificatDansUrl true si on
   * désire inclure le certificat d'authentifion
   * dans l'appel de la page qui doit être inclus dynamiquement.
   */
  public void setInclureCertificatDansUrl(boolean inclureCertificatDansUrl) {
    this.inclureCertificatDansUrl = inclureCertificatDansUrl;
  }

  /**
   * Est-ce que l'on doit inclure le certificat d'authentification
   * dans l'appel de la page qui doit être inclus dynamiquement.
   * @return true si on doit inclure le certificat d'authentification
   * dans l'appel de la page qui doit être inclus dynamiquement.
   */
  public boolean isInclureCertificatDansUrl() {
    return inclureCertificatDansUrl;
  }
}
