/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;


/**
 * FileTag permet d'afficher un champ texte avec un bouton pouvant
 * d'extraire un fichier d'un poste client.
 * <p>
 * Offre toutes les fonctionnalités de BaseFieldTag.
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.6
 * @see org.sofiframework.presentation.struts.form.BaseForm
 * @see org.sofiframework.presentation.balisesjsp.html.BaseFieldTag
 */
public class FileTag extends BaseFieldTag {

  private static final long serialVersionUID = -3132967976576836512L;

  private String type = null;
  private String tailleMaximale = null;
  private String messageErreur = null;

  /** Constructeur pour cette balise */
  public FileTag() {
    super();
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setTailleMaximale(String tailleMaximale) {
    this.tailleMaximale = tailleMaximale;
  }

  public String getTailleMaximale() {
    return tailleMaximale;
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected String genererCodeHtmlComposant() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    setTransactionnel("false");

    // Accès au formulaire
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    // Ajout dans la liste à traiter dans le formulaire.
    baseForm.ajouterAttributATraiter(getProperty());
    super.type = "file";

    if ((getType() != null) || (getTailleMaximale() != null)) {
      // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
      String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
          getProperty());

      if ((nomFormulaireImbrique == null) &&
          (getProperty().indexOf(".") != -1)) {
        nomFormulaireImbrique = getProperty().substring(0,
            getProperty().lastIndexOf("."));
      }

      // Traiter le format du champ de saisie.
      UtilitaireBaliseJSP.traiterAttributFormat(getProperty(),
          nomFormulaireImbrique, getType(), getTailleMaximale(),
          getMessageErreur(), pageContext);
    }

    return super.genererCodeHtmlComposant();
  }

  public void setMessageErreur(String messageErreur) {
    this.messageErreur = messageErreur;
  }

  public String getMessageErreur() {
    return messageErreur;
  }

  /**
   * Évaluer les expressionn régulières
   */
  @Override
  public void evaluerEL() throws JspException {
    super.evaluerEL();

    if (getTailleMaximale() != null) {
      String tailleMaximale = EvaluateurExpression.evaluerString("tailleMaximale",
          getTailleMaximale(), this, pageContext);
      setTailleMaximale(tailleMaximale);
    }

    if (getMessageErreur() != null) {
      String messageErreur = EvaluateurExpression.evaluerString("tailleMaximaleMessageErreur",
          getMessageErreur(), this, pageContext);
      setMessageErreur(messageErreur);
    }

    if (getType() != null) {
      String type = EvaluateurExpression.evaluerString("type", getType(), this,
          pageContext);
      setType(type);
    }
  }
}
