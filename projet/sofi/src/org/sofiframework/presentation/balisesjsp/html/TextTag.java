/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;



/**
 * TextTag permet d'afficher un champ texte en plus de traiter
 * automatiquement si l'utilisateur fait une modification sur ce champ texte.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.form.BaseForm
 * @see org.sofiframework.presentation.balisesjsp.html.BaseFieldTag
 */
public class TextTag extends BaseFieldTag {
  /**
   * 
   */
  private static final long serialVersionUID = 3296402789612311203L;

  /** Constructeur pour cette balise */
  public TextTag() {
    super();
    this.type = "text";
  }
}
