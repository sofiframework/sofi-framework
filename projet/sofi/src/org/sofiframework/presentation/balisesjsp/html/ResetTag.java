/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise JSP permetttant la création d'un lien hypertexte.
 *
 * @author Jean-François Brassard
 * @version SOFI 1.0
 */
public class ResetTag extends BaseBoutonTag {
  /**
   * 
   */
  private static final long serialVersionUID = 6868398401076207744L;

  /**
   * Génération d'un début du lien hypertexte.
   * @exception JspException si une exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    super.doStartTag();

    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected StringBuffer genererCodeHtmlComposant() throws JspException {
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    StringBuffer resultat = new StringBuffer("<input type=\"reset\"");

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    Libelle libelle = UtilitaireLibelle.getLibelle(getLibelle(), pageContext,
        null);

    if (UtilitaireString.isVide(getAideContextuelle())) {
      if (libelle.getAideContextuelle() != null) {
        resultat.append(" title=\"");
        resultat.append(libelle.getAideContextuelle());
        resultat.append("\"");
      }
    } else {
      Libelle aideContextuelle = UtilitaireLibelle.getLibelle(getAideContextuelle(),
          pageContext, null);
      resultat.append(" title=\"");
      resultat.append(aideContextuelle.getMessage());
      resultat.append("\"");
    }

    Message libelleConfirmation = null;

    if (getMessageConfirmation() != null) {
      libelleConfirmation = UtilitaireMessage.get(this.getMessageConfirmation(),
          null, pageContext);
    }

    // Pas conforme au W3C. L'identifiant et les attributs d'une balise doivent
    // être en minuscules.
    //resultat.append(" onClick = \"");
    resultat.append(" onclick = \"");

    if (getOnclick() != null) {
      resultat.append(getOnclick());
    }

    if (getHref() != null) {
      StringBuffer inactiverBouton = new StringBuffer();
      inactiverBouton.append("inactiverBouton(document.");
      inactiverBouton.append(nomFormulaire);
      inactiverBouton.append(", false);");

      if (getMessageConfirmation() != null) {
        resultat.append("if (confirm('");
        resultat.append(libelleConfirmation.getMessage());
        resultat.append("')) {");
      }

      resultat.append("document.location.href = '");
      resultat.append(getHref());
      resultat.append("';");
      resultat.append(inactiverBouton);

      if (getMessageConfirmation() != null) {
        resultat.append("};");
      }
    } else {
      resultat.append("document.");
      resultat.append(UtilitaireBaliseJSP.getNomFormulaire(pageContext));
      resultat.append(".indModification.value = '0';");
      resultat.append("display('modificationFormulaire', false);");
      resultat.append("display('messageFormulaire', true);");
    }

    resultat.append("\"");

    resultat.append(" value=\"");

    resultat.append(libelle.getMessage());

    resultat.append("\"");

    return resultat;
  }

  /**
   * Traiter la fermeture de la balise.
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    // rien faire..
    return (EVAL_PAGE);
  }

  /**
   * Libérer les ressouces.
   */
  @Override
  public void release() {
    super.release();
  }
}
