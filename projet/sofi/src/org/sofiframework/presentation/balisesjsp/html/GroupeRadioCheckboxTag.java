/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.balisesjsp.base.BaseELTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Tag qui permet d'afficher un groupe de boutons radio ou de checkbox.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @since SOFI 2.0
 */
public class GroupeRadioCheckboxTag extends BaseELTag {
  /**
   * 
   */
  private static final long serialVersionUID = 5621814472526437770L;

  /** Constante pour représenter le nombre de colonnes du tableau */
  public final static String NOMBRE_COLONNES_GROUPE_COURANT = "nombreColonnesGroupeCourant";

  /** Constante pour représenter la position courante du champ */
  public final static String POSITION_COURANTE_DANS_GROUPE = "positionCouranteDansGroupe";

  /** Constante pour l'étiquette dans le contexte de page */
  public final static String ATTRIBUT_ETIQUETTE = "sofiEtiquetteGroupeRadioCheckboxTag";

  /** Constante pour la valeur dans le contexte de page */
  public final static String ATTRIBUT_VALEUR = "sofiValeurGroupeRadioCheckboxTag";

  /** Constante pour la propriété dans le contexte de page */
  public final static String ATTRIBUT_PROPRIETE = "sofiProprieteGroupeRadioCheckboxTag";

  /** La collection qui va contenir les objets à afficher */
  protected Object collection;

  /** Itérateur pour parcourir la collection */
  private Iterator iterateur;

  /** Le nom de la propriété pour l'étiquette */
  private String proprieteEtiquette;

  /** Le nom de la propriété pour la valeur */
  private String proprieteValeur;

  /** Le nom de la propriété du formulaire associée */
  private String property;

  /** La classe CSS du tableau */
  protected String styleClass;

  /** Le nombre d'espace à l'intérieur des cellules avant le contenu */
  protected String cellpadding;

  /** L'espace entre chaque cellule. */
  protected String cellspacing;

  /** Le nombre de colonnes de champs qui seront générées dans le tableau */
  protected String nbColonnes;

  /** La cache contenant les valeurs possibles */
  protected String cache;

  /** Constructeur par défaut */
  public GroupeRadioCheckboxTag() {
  }

  /**
   * Génération de la balise.
   * <p>
   * @exception JspException si une exception JSP est lancé.
   * @return la valeur indiquant que faire avec le traitement restant de la page.
   */
  @Override
  public int doStartTag() throws JspException {

    String codeHtml = null;

    codeHtml = this.genererCodeHtmlComposant();

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    }

    // Mettre dans le contexte le nom de la propriété pour les balises
    pageContext.getSession().setAttribute(ATTRIBUT_PROPRIETE, this.getProperty());

    if (this.getNbColonnes() == null) {
      this.setNbColonnes("1");
    }

    try {
      pageContext.getRequest().setAttribute(NOMBRE_COLONNES_GROUPE_COURANT,
          Integer.valueOf(this.getNbColonnes()));
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Le paramètre nbColonnes doit être numérique.");
    }

    pageContext.getRequest().setAttribute(POSITION_COURANTE_DANS_GROUPE,
        Integer.valueOf("1"));

    Collection listeObjets = this.getListe();
    iterateur = listeObjets.iterator();

    if (iterateur.hasNext()) {
      Object objet = iterateur.next();
      String etiquette = null;
      String valeur = null;

      try {
        etiquette = PropertyUtils.getProperty(objet, proprieteEtiquette)
            .toString();
      } catch (Exception e) {
        throw new JspException(
            "L'attribut indiqué comme propriété de l'étiquette est introuvable.");
      }

      try {
        valeur = PropertyUtils.getProperty(objet, proprieteValeur).toString();
      } catch (Exception e) {
        throw new JspException(
            "L'attribut indiqué comme propriété de la valeur est introuvable.");
      }

      pageContext.getSession().setAttribute(ATTRIBUT_ETIQUETTE, etiquette);
      pageContext.getSession().setAttribute(ATTRIBUT_VALEUR, valeur);

      return (EVAL_BODY_INCLUDE);
    } else {
      return (SKIP_BODY);
    }
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() {
    StringBuffer html = new StringBuffer("");

    // Accès au formulaire pour vérifier s'il y a un div d'erreur à créer.
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(this.pageContext);
    MessageErreur erreur = UtilitaireBaliseJSP.getMessageErreur(formulaire,
        this.property, this.pageContext);

    String titre = null;

    // S'il y a une erreur, afficher un div pour l'encadré d'erreur
    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();

      // Pour que le titre du champ de saisie soit convertie en texte du gestion message
      html.append(UtilitaireBaliseJSP.genererChampInvisiblePourFocus(
          getProperty(), titre));
      html.append("<div class=\"cadreErreur\">");
    }

    html.append("<table");

    if (this.getCellpadding() != null) {
      html.append(" cellpadding=\"" + this.getCellpadding() + "\" ");
    }

    if (this.getCellspacing() != null) {
      html.append(" cellspacing=\"" + this.getCellspacing() + "\" ");
    }

    if (this.getStyleClass() != null) {
      html.append(" class=\"" + this.getStyleClass() + "\" ");
    }

    html.append(">");

    return html.toString();
  }

  /**
   * Libérer les ressources acquises.
   */
  @Override
  public void release() {
    super.release();
    this.setCache(null);
    this.setCellpadding(null);
    this.setCellspacing(null);
    this.setCollection(null);
    this.setNbColonnes(null);
    this.setProperty(null);
    this.setProprieteEtiquette(null);
    this.setProprieteValeur(null);
    this.setStyleClass(null);
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      resultat.append(valeur);
    }

    resultat.append("</table>");

    pageContext.getRequest().removeAttribute(NOMBRE_COLONNES_GROUPE_COURANT);
    pageContext.getRequest().removeAttribute(POSITION_COURANTE_DANS_GROUPE);

    pageContext.getSession().removeAttribute(ATTRIBUT_ETIQUETTE);
    pageContext.getSession().removeAttribute(ATTRIBUT_VALEUR);
    pageContext.getSession().removeAttribute(ATTRIBUT_PROPRIETE);

    //pageContext.getRequest().removeAttribute(NOMBRE_COLONNES_TABLEAU_COURANT);
    //pageContext.getRequest().removeAttribute(POSITION_COURANTE_DANS_TABLEAU);
    // Accès au formulaire pour vérifier s'il y a un div d'erreur à créer.
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(this.pageContext);
    MessageErreur erreur = UtilitaireBaliseJSP.getMessageErreur(formulaire,
        this.property, this.pageContext);

    if (erreur != null) {
      resultat.append("</div>");
    }

    TagUtils.getInstance().write(pageContext, resultat.toString());

    return (EVAL_PAGE);
  }

  public void setCollection(Object collection) {
    this.collection = collection;
  }

  public Object getCollection() {
    return collection;
  }

  /**
   * Permet d'application l'expression JSTL (EL) à la balise à l'attribut collection.
   * <p>
   * @return Une liste d'objet à utiliser pour construire la balise select.
   */
  private Collection getListe() {
    try {
      if (this.getCollection() != null) {
        Collection listeRetour = (Collection) evaluer("collection", collection);

        try {
          String index = UtilitaireBaliseNested.getIndexListeNested(pageContext);

          String nomCollection = collection.toString();

          if ((index != null) && (listeRetour == null)) {
            StringBuffer collectionImbrique = new StringBuffer(nomCollection.substring(
                0, nomCollection.length() - 1));
            collectionImbrique.append(index);
            collectionImbrique.append("}");
            listeRetour = (Collection) evaluer("collection",
                collectionImbrique.toString());
          }
        } catch (Exception e) {
        }

        return listeRetour;
      } else {
        try {
          ArrayList listeRetour = new ArrayList();

          // Utilisation de la cache
          ObjetCache cache = this.getObjetCache();

          Iterator iterEtiquette = cache.iterateurValeurs();
          Iterator iterValeur = cache.iterateurCles();

          // Traiter l'étiquette de la cache.
          String etiquette = null;
          boolean traiterLibelle = true;

          while (iterEtiquette.hasNext()) {
            if (!UtilitaireString.isVide(getProprieteEtiquette())) {
              ObjetTransfert objetTransfert = (ObjetTransfert) iterEtiquette.next();
              etiquette = (String) objetTransfert.getPropriete(getProprieteEtiquette());
            } else {
              Object valeurEtiquette = iterEtiquette.next();

              if (DomaineValeur.class.isInstance(valeurEtiquette)) {
                DomaineValeur domaineValeur = (DomaineValeur) valeurEtiquette;
                Locale locale = UtilitaireBaliseJSP.getLocale(pageContext);

                if ((domaineValeur.getListeDescriptionLangue() != null) &&
                    (domaineValeur.getListeDescriptionLangue().size() > 0)) {
                  etiquette = (String) domaineValeur.getListeDescriptionLangue()
                      .get(locale.getLanguage());
                  traiterLibelle = false;
                } else {
                  etiquette = domaineValeur.getDescription();
                }
              } else {
                etiquette = (String) valeurEtiquette;
              }
            }

            if (traiterLibelle) {
              // Appliquer la gestion des libellés.
              Libelle libelle = UtilitaireLibelle.getLibelle(etiquette,
                  pageContext);
              etiquette = libelle.getMessage();
            }

            // Traiter la valeur de la cache.
            Object valeur = null;

            if (!UtilitaireString.isVide(getProprieteValeur())) {
              ObjetTransfert objetTransfert = (ObjetTransfert) iterValeur.next();
              valeur = objetTransfert.getPropriete(getProprieteValeur());
            } else {
              valeur = iterValeur.next();
            }

            ObjetCleValeur objetCleValeur = new ObjetCleValeur(valeur, etiquette);
            listeRetour.add(objetCleValeur);
          }

          this.setProprieteEtiquette("objetValeur");
          this.setProprieteValeur("objetCle");

          return listeRetour;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return new ArrayList();
  }

  @Override
  public int doAfterBody() throws JspException {
    // Render the output from this iteration to the output stream
    if (bodyContent != null) {
      TagUtils.getInstance().writePrevious(pageContext, bodyContent.getString());
      bodyContent.clearBody();
    }

    if (iterateur.hasNext()) {
      Object objet = iterateur.next();

      String etiquette = null;
      String valeur = null;

      try {
        etiquette = PropertyUtils.getProperty(objet, proprieteEtiquette)
            .toString();
      } catch (Exception e) {
        throw new JspException(
            "L'attribut indiqué comme propriété de l'étiquette est introuvable.");
      }

      try {
        valeur = PropertyUtils.getProperty(objet, proprieteValeur).toString();
      } catch (Exception e) {
        throw new JspException(
            "L'attribut indiqué comme propriété de la valeur est introuvable.");
      }

      pageContext.getSession().setAttribute(ATTRIBUT_ETIQUETTE, etiquette);
      pageContext.getSession().setAttribute(ATTRIBUT_VALEUR, valeur);

      if (objet == null) {
        pageContext.removeAttribute(id);
      } else {
        pageContext.setAttribute(id, objet);
      }

      return (EVAL_BODY_AGAIN);
    } else {
      return (SKIP_BODY);
    }
  }

  public static boolean verifierOuvrirLigne(PageContext contexte) {
    Integer nombreColonnesTableau = (Integer) contexte.getRequest()
        .getAttribute(NOMBRE_COLONNES_GROUPE_COURANT);

    boolean valeurRetour = false;

    if (nombreColonnesTableau != null) {
      Integer positionCouranteDansTableau = (Integer) contexte.getRequest()
          .getAttribute(POSITION_COURANTE_DANS_GROUPE);

      try {
        valeurRetour = (positionCouranteDansTableau.intValue() == 1);

        //UtilitaireObjet.setPropriete(balise, "fermerLigne", Boolean.valueOf(positionCouranteDansTableau.intValue() == nombreColonnesTableau.intValue()));
      } catch (Exception e) {
        System.out.println(
            "Probleme à fixer la valeur dans l'objet pour la gestion des lignes");
      }
    }

    return valeurRetour;
  }

  public static boolean verifierFermerLigne(PageContext contexte) {
    Integer nombreColonnesTableau = (Integer) contexte.getRequest()
        .getAttribute(NOMBRE_COLONNES_GROUPE_COURANT);

    boolean valeurRetour = false;

    if (nombreColonnesTableau != null) {
      Integer positionCouranteDansTableau = (Integer) contexte.getRequest()
          .getAttribute(POSITION_COURANTE_DANS_GROUPE);

      try {
        valeurRetour = (positionCouranteDansTableau.intValue() == nombreColonnesTableau.intValue());
      } catch (Exception e) {
        System.out.println(
            "Probleme à fixer la valeur dans l'objet pour la gestion des lignes");
      }

      if (positionCouranteDansTableau.intValue() == nombreColonnesTableau.intValue()) {
        positionCouranteDansTableau = new Integer(1);
      } else {
        positionCouranteDansTableau = new Integer(positionCouranteDansTableau.intValue() +
            1);
      }

      contexte.getRequest().setAttribute(POSITION_COURANTE_DANS_GROUPE,
          positionCouranteDansTableau);
    }

    return valeurRetour;
  }

  /**
   * Obtenir l'objet cache pour contruire la liste des options du composant.
   * @return  L'objet cache correspondant
   */
  protected ObjetCache getObjetCache() {
    return GestionCache.getInstance().getCache(this.getCache());
  }

  public void setProprieteEtiquette(String proprieteEtiquette) {
    this.proprieteEtiquette = proprieteEtiquette;
  }

  public String getProprieteEtiquette() {
    return proprieteEtiquette;
  }

  public void setProprieteValeur(String proprieteValeur) {
    this.proprieteValeur = proprieteValeur;
  }

  public String getProprieteValeur() {
    return proprieteValeur;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public String getProperty() {
    return property;
  }

  public void setStyleClass(String styleClass) {
    this.styleClass = styleClass;
  }

  public String getStyleClass() {
    return styleClass;
  }

  public void setCellpadding(String cellpadding) {
    this.cellpadding = cellpadding;
  }

  public String getCellpadding() {
    return cellpadding;
  }

  public void setCellspacing(String cellspacing) {
    this.cellspacing = cellspacing;
  }

  public String getCellspacing() {
    return cellspacing;
  }

  public void setNbColonnes(String nbColonnes) {
    this.nbColonnes = nbColonnes;
  }

  public String getNbColonnes() {
    return nbColonnes;
  }

  public void setCache(String cache) {
    this.cache = cache;
  }

  public String getCache() {
    return cache;
  }

  private Object evaluer(String nom, Object valeur) throws JspException {
    if (valeur instanceof String) {
      return ExpressionEvaluatorManager.evaluate(nom, valeur.toString(),
          Object.class, this, this.pageContext);
    } else {
      return valeur;
    }
  }
}
