/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.Globals;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.InfoMultibox;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * MultiboxTag est un champ de saisie de type boite à coché.
 * <p>
 * Il se différencie de la base CheckboxTag car il assume que l'attribut correspond à un
 * tableau (n'importe qu'elle type de primitif ou de String), et le checkbox
 * est initialisé à "checked" si la valeur est listé dans l'attribut "value" si
 * présent dans les valeurs retourné par la propriété.
 * <p>
 * Il permet de notifier si l'utilisateur fait une modification sur ce
 * champ texte.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 1.8 Traitement Ajax disponible.
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class MultiboxTag extends org.apache.struts.taglib.html.MultiboxTag {
  /**
   * 
   */
  private static final long serialVersionUID = -4859363181925471747L;

  /** Spécifie la classe css pour un attribut en lecture seulement */
  protected String styleClassLectureSeulement = null;

  /** Spécifie la classe css pour un attribut sélectionné */
  protected String styleClassFocus = null;

  /** Spécifie le libellé */
  protected String libelle = null;
  protected String disabledEL = null;
  protected String readonlyEL = null;

  /** Href lors d'un traitement ajax **/
  protected String href = null;

  /**
   * Spécifier un début d'un groupe de radio bouton.
   */
  private String groupeDebut;

  /**
   * Spécifie une fin d'un groupe de radio bouton.
   */
  private String groupeFin;

  /**
   * Obtenir le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @return le style à utiliser dans le cas où le champ possède le focus
   */
  public String getStyleClassFocus() {
    return (this.styleClassFocus);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @param styleClassFocus le style à utiliser dans le cas où le champ possède le focus
   */
  public void setStyleClassFocus(String styleClassFocus) {
    this.styleClassFocus = styleClassFocus;
  }

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * @param libelle le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @param styleClassLectureSeulement le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Execute le début de la balise.
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si un exception JSP est lancé.
   */
  @Override
  public int doEndTag() throws JspException {
    String codeHtml = null;

    // Gestion des propriétés et valeurs selon le groupe multibox si présent
    String libelleGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_ETIQUETTE);
    String valeurGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_VALEUR);
    String proprieteGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_PROPRIETE);

    if ((libelleGroupe != null) && (valeurGroupe != null)) {
      this.setLibelle(libelleGroupe);
      this.setValue(valeurGroupe);
      this.setProperty(proprieteGroupe);
    }

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      Utilisateur utilisateur =
          UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {
        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setDisabled(true);
        } else {
          BaseForm baseForm =
              UtilitaireBaliseJSP.getBaseForm(this.pageContext);
          boolean blocSecurite =
              (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) !=
              null) || ((baseForm != null) && baseForm.isModeLectureSeulement());

          boolean isLectureSeulement = UtilitaireString.isVide(getDisabledEL())
              && UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
          if (isLectureSeulement) {
            this.setDisabled(true);
          }
        }

        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant();
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(pageContext, codeHtml);
    }

    this.release();

    return (EVAL_PAGE);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    // Accès au formulaire.
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(this.pageContext);

    if (formulaire.isModeAffichageSeulement()) {
      setDisabled(true);
    }

    if (!getDisabled() && !getReadonly()) {
      // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
      formulaire.ajouterAttributATraiter(getProperty());
    }

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    String nomFormulaireJS =
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            property);

    MessageErreur erreur =
        UtilitaireBaliseJSP.getMessageErreur(formulaire, this.property,
            this.pageContext);

    Boolean estPositioneeHautChamp = Boolean.FALSE;
    String titre = null;

    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();

      // Permet de verifier si le message d'erreur de saisie
      // doit être positionné en bas du champ. False est la valeur par défaut et signifie que
      // les messages apparaîtront en nicetitle au survol du champ.
      estPositioneeHautChamp =
          GestionParametreSysteme.getInstance().getBoolean(Constantes.POSITION_MESSAGE_ERREUR_SAISIE);

      if (estPositioneeHautChamp == null) {
        estPositioneeHautChamp = Boolean.FALSE;
      }
    }

    this.setStyleClass("checkbox " + getStyleClass());

    StringBuffer resultat = new StringBuffer();

    // Pour que le titre du champ de saisie soit convertie en texte du gestion message
    if (erreur != null) {
      titre = UtilitaireString.convertirEnHtml(titre);

      if ((getGroupeDebut() != null) && getGroupeDebut().equals("true")) {
        resultat.append(UtilitaireBaliseJSP.genererChampInvisiblePourFocus(getProperty(),
            titre));
        resultat.append("<div class=\"cadreErreur\">");
      }

      setTitle(titre);
    }

    Integer no =
        UtilitaireBaliseJSP.getCompteurBoiteACocherMultibox(getProperty(), true,
            pageContext);
    boolean selectionne = this.isSelectionne(formulaire);

    //    if (!formulaire.isModeAffichageSeulement()) {
    resultat.append("<input type=\"checkbox\"");
    resultat.append(" name=\"");
    resultat.append(this.property);
    resultat.append("\"");

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    resultat.append(" id=\"");
    resultat.append(getProperty());
    resultat.append(no);
    resultat.append("\"");

    UtilitaireBaliseJSP.specifierGestionModification(pageContext,
        nomFormulaire, nomFormulaireJS, getOnchange(), resultat);

    // Faire la gestion de l'affichage en mode disabled
    if (formulaire.isModeLectureSeulement() || this.getDisabled() ||
        this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(getStyleClassLectureSeulement(),
          getStyleClass(), resultat);
      setDisabled(true);
    }

    org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
        getStyleClassFocus(), getOnfocus(), getOnblur(), resultat);

    resultat.append(" value=\"");

    String value = this.getValeurEL();

    if (value == null) {
      value = this.constant;
    }

    if (value == null) {
      JspException e =
          new JspException(messages.getMessage("multiboxTag.value"));
      pageContext.setAttribute(Globals.EXCEPTION_KEY, e,
          PageContext.REQUEST_SCOPE);
      throw e;
    }

    resultat.append(TagUtils.getInstance().filter(value));
    resultat.append("\"");

    String onClickCourant = getOnclick();

    // Traitement Ajax
    if (getHref() != null) {
      setOnclick(null);

      HashMap listeInfoListeCouranteMultibox =
          (HashMap)pageContext.getSession().getAttribute("listeInfoListeCouranteMultibox");

      if ((listeInfoListeCouranteMultibox != null) &&
          (listeInfoListeCouranteMultibox.get(getProperty()) != null)) {
        InfoMultibox infoListeCouranteMultibox =
            (InfoMultibox)listeInfoListeCouranteMultibox.get(getProperty());
        HashSet listeSelectionnee =
            infoListeCouranteMultibox.getListeSelectionnee();

        if (listeSelectionnee.contains(value)) {
          selectionne = true;
        } else {
          selectionne = false;
        }

        Href lien = new Href(getHref());

        lien.ajouterParametre(infoListeCouranteMultibox.getNomParametreRequete(),
            value);

        if (onClickCourant != null) {
          setOnclick(onClickCourant +
              UtilitaireAjax.traiterRequeteHttpGETAsynchrone(lien));
        } else {
          setOnclick(UtilitaireAjax.traiterRequeteHttpGETAsynchrone(lien));
        }
      }
    }

    if (selectionne) {
      resultat.append(" checked=\"checked\"");
    }

    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(getElementClose());

    //    } else {
    //      if (selectionne) {
    //        resultat.append(
    //          "<span class=\"checkbox_affichage_seulement_oui\">&nbsp;&nbsp;&nbsp;</span>");
    //      } else {
    //        resultat.append(
    //          "<span class=\"checkbox_affichage_seulement_non\">&nbsp;&nbsp;&nbsp;</span>");
    //      }
    //    }
    UtilitaireBaliseJSP.genererLibelle(this.property, no, this.getTitle(),
        this.libelle, this.pageContext, resultat);

    if (pageContext.getRequest().getAttribute(GroupeRadioCheckboxTag.NOMBRE_COLONNES_GROUPE_COURANT) !=
        null) {
      resultat.append("</td>");

      if (GroupeRadioCheckboxTag.verifierFermerLigne(pageContext)) {
        resultat.append("</tr>");
      }
    }

    if (erreur != null) {
      if ((getGroupeFin() != null) && getGroupeFin().equals("true")) {
        resultat.append("</div>");
      }
    }

    return resultat.toString();
  }

  private boolean isSelectionne(BaseForm form) throws JspException {
    String[] values = null;
    boolean selectionne = false;

    if (form != null) {
      try {
        values = BeanUtils.getArrayProperty(form, property);

        if (values == null) {
          values = new String[0];
        }
      } catch (IllegalAccessException e) {
        throw new JspException(messages.getMessage("getter.access", property,
            name), e);
      } catch (InvocationTargetException e) {
        Throwable t = e.getTargetException();
        throw new JspException(messages.getMessage("getter.result", property,
            t.toString()), e);
      } catch (NoSuchMethodException e) {
        throw new JspException(messages.getMessage("getter.method", property,
            name), e);
      }

      int i = 0;

      while (i < values.length) {
        if (value.equals(values[i])) {
          selectionne = true;

          break;
        }

        i++;
      }
    }

    return selectionne;
  }

  private String getValeurEL() {
    try {
      return EvaluateurExpression.evaluerString("value", this.value, this,
          pageContext);
    } catch (JspException e) {
      return value;
    }
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      if (getValue() != null) {
        String valeur =
            EvaluateurExpression.evaluerString("value", getValue(), this,
                pageContext);
        setValue(valeur);
      }

      if (getLibelle() != null) {
        String libelle =
            EvaluateurExpression.evaluerString("libelle", getLibelle(), this,
                pageContext);
        setLibelle(libelle);
      }

      if (getProperty() != null) {
        String property =
            EvaluateurExpression.evaluerString("property", String.valueOf(getProperty()),
                this, pageContext);
        setProperty(property);
      }

      if (getDisabledEL() != null) {
        boolean disabled =
            EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
                this, pageContext);
        setDisabled(disabled);
      }

      if (getReadonlyEL() != null) {
        boolean readonly =
            EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
                this, pageContext);
        setDisabled(readonly);
      }

      String valeurELHref =
          EvaluateurExpression.evaluerString("href", getHref(), this,
              pageContext);
      setHref(valeurELHref);

      if (getGroupeDebut() != null) {
        String groupeDebut =
            EvaluateurExpression.evaluerString("groupeDebut", getGroupeDebut(),
                this, pageContext);
        setGroupeDebut(groupeDebut.toLowerCase());
      }

      if (getGroupeFin() != null) {
        String groupeFin =
            EvaluateurExpression.evaluerString("groupeFin", getGroupeFin(), this,
                pageContext);
        setGroupeFin(groupeFin.toLowerCase());
      }
    } catch (JspException e) {
      throw e;
    }
  }

  @Override
  public void release() {
    this.styleClassLectureSeulement = null;
    this.styleClassFocus = null;
    this.libelle = null;
    this.disabledEL = null;
    this.readonlyEL = null;
    this.href = null;
    super.release();
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getHref() {
    return href;
  }

  public void setGroupeDebut(String groupeDebut) {
    this.groupeDebut = groupeDebut;
  }

  public String getGroupeDebut() {
    return groupeDebut;
  }

  public void setGroupeFin(String groupeFin) {
    this.groupeFin = groupeFin;
  }

  public String getGroupeFin() {
    return groupeFin;
  }
}
