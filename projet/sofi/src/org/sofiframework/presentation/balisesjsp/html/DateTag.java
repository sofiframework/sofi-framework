/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.util.ResponseUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise JSP générant un champ texte suivi d'un icone calendrier qui appel
 * un calendrier javascript lorsqu'on clic dessus.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class DateTag extends BaseFormulaireTag {
  /**
   * 
   */
  private static final long serialVersionUID = 2028398523928285822L;

  /** Spécifie si la balise date inclus les heures et minutes */
  protected boolean heure = false;

  /** Spécifie si l'icone calendrier inclus */
  protected String iconeCalendrier = null;

  /** Spécifie si la sélection d'une date se fait en un seul clic */
  protected boolean unClic = true;
  private String aideContextuelle = "";

  private String placeholder = null;

  /** Constructeur par défaut */
  public DateTag() {
    super();
  }

  /**
   * Obtenir la valeur qui spécifie si l'heure doit apparaître dans la balise.
   * <p>
   * @return la valeur qui spécifie si l'heure doit apparaître dans la balise
   */
  public boolean getHeure() {
    return (this.heure);
  }

  /**
   * Fixer la valeur qui spécifie si l'heure doit apparaître dans la balise.
   * <p>
   * @param heure la valeur qui spécifie si l'heure doit apparaître dans la balise
   */
  public void setHeure(boolean heure) {
    this.heure = heure;
  }

  /**
   * Obtenir la valeur qui spécifie si l'icône du calendrier doit être affiché.
   * <p>
   * @return la valeur qui spécifie si l'icône du calendrier doit être affiché
   */
  public String getIconeCalendrier() {
    return (this.iconeCalendrier);
  }

  /**
   * Fixer la valeur qui spécifie si l'icône du calendrier doit être affiché.
   * <p>
   * @param iconeCalendrier la valeur qui spécifie si l'icône du calendrier doit être affiché
   */
  public void setIconeCalendrier(String iconeCalendrier) {
    this.iconeCalendrier = iconeCalendrier;
  }

  /**
   * Est-ce que l'on désire afficher l'icone de calendrier.
   * @return true si on désire afficher l'icone de calendrier.
   */
  public boolean isAfficherIconeCalendrier() {
    return (getIconeCalendrier() != null) &&
        getIconeCalendrier().toString().toLowerCase().equals("true");
  }

  /**
   * Obtenir la valeur qui identifie si le calendrier doit s'ouvrir suite à un clic ou deux.
   * <p>
   * @return la valeur qui identifie si le calendrier doit s'ouvrir suite à un clic ou deux
   */
  public boolean getUnClic() {
    return (this.unClic);
  }

  /**
   * Fixer la valeur qui identifie si le calendrier doit s'ouvrir suite à un clic ou deux.
   * <p>
   * @param unClic la valeur qui identifie si le calendrier doit s'ouvrir suite à un clic ou deux
   */
  public void setUnClic(boolean unClic) {
    this.unClic = unClic;
  }

  /**
   * Méthode qui sert à générer le contenu de la balise à afficher.
   * <p>
   * @param resultat le code à inscrire à l'intérieur de la balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  protected void ecrireBalise(StringBuffer resultat) throws JspException {
    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    // Accès au formulaire.
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    // Préparation de la méthode javascript qui indique si le formulaire
    // a été modifié
    String nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
        property);

    String valeurDuChampATraiter = null;
    String valeurDate = "";

    Libelle libelle = UtilitaireLibelle.getLibelle(getLibelle(), this.pageContext, null);

    if (libelle.getPlaceholder() != null && StringUtils.isEmpty(getPlaceholder())) {
      setPlaceholder(libelle.getPlaceholder());
    }

    if (value != null) {
      resultat.append(ResponseUtils.filter(value));
    } else {
      try {
        valeurDuChampATraiter = (String) TagUtils.getInstance().lookup(this.pageContext,
            this.name, this.property, null);
      } catch (Exception e) {
        valeurDuChampATraiter = (String) UtilitaireObjet.getValeurAttribut(baseForm,
            getProperty());
      }

      if (!getHeure() && (valeurDuChampATraiter != null) &&
          (valeurDuChampATraiter.length() >= 10)) {
        valeurDate = valeurDuChampATraiter.substring(0, 10);
      } else {
        valeurDate = valeurDuChampATraiter;
      }

      if (valeurDate == null) {
        valeurDate = "";
      }
    }

    if (!UtilitaireString.isVide(nomFormulaireJS) &&
        ((!getDisabled() && !getReadonly() && (iconeCalendrier == null)) ||
            ((iconeCalendrier != null) && iconeCalendrier.equals("true")))) {
      resultat.append("<script type=\"text/javascript\">");
      resultat.append("function notifier");
      resultat.append(nomFormulaireJS);
      resultat.append(getProperty());
      resultat.append("(valeur) {");
      resultat.append("if (valeur != '");
      resultat.append(valeurDate);
      resultat.append("') {");
      resultat.append("notifier");
      resultat.append(nomFormulaireJS);
      resultat.append("();}}</script>");
    }

    resultat.append("<input type=\"text");
    resultat.append("\" name=\"");
    resultat.append(property);
    resultat.append("\"");

    if (getAccesskey() != null) {
      resultat.append(" accesskey=\"");
      resultat.append(getAccesskey());
      resultat.append("\"");
    }

    if (accept != null) {
      resultat.append(" accept=\"");
      resultat.append(accept);
      resultat.append("\"");
    }

    if (maxlength != null) {
      resultat.append(" maxlength=\"");
      resultat.append(maxlength);
      resultat.append("\"");
      resultat.append(" size=\"");
      resultat.append(maxlength);
      resultat.append("\"");
    } else { //Spécifier 10 comme défaut
      resultat.append(" maxlength=\"");

      if (getHeure()) {
        resultat.append(16);
      } else {
        resultat.append(10);
      }

      resultat.append("\"");

      resultat.append(" size=\"");

      if (getHeure()) {
        resultat.append(16);
      } else {
        resultat.append(10);
      }

      resultat.append("\"");
    }

    if (this.getTabindex() != null) {
      resultat.append(" tabindex=\"");
      resultat.append(this.getTabindex());
      resultat.append("\"");
    }

    // Pas conforme au W3C. Une balise doit avoir un seul ID.
    // Le resultat.append(prepareStyles()); plus bas en spécifie déjà un.
    //    resultat.append(" id=\"");
    //    resultat.append(getProperty());
    //    resultat.append("\"");

    if ((!getReadonly() && !getDisabled()) || !isAfficherIconeCalendrier()) {
      StringBuffer onBlur = new StringBuffer();

      if (!UtilitaireString.isVide(getOnblur())){
        onBlur.append(getOnblur());
      }

      if (!UtilitaireString.isVide(nomFormulaireJS)){
        onBlur.append("notifier");
        onBlur.append(nomFormulaireJS);
        onBlur.append(getProperty());
        onBlur.append("(this.value);");
      }

      UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
          getStyleClassFocus(), getOnfocus(), onBlur.toString(), resultat);

      resultat = UtilitaireBaliseJSP.specifierGestionModification(pageContext,
          nomFormulaire, nomFormulaireJS, getOnchange(), resultat);
    }

    // Spécifier le mode lectureSeulement
    if (baseForm.isModeLectureSeulement() || this.getDisabled() ||
        this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.getStyleClassLectureSeulement(),
          getStyleClass(), resultat);
    }

    resultat.append(" value=\"");

    resultat.append(ResponseUtils.filter(valeurDate));

    resultat.append("\"");

    if (StringUtils.isNotBlank(getPlaceholder())) {
      resultat.append(" placeholder=\"");
      Libelle libellePlaceHolder =
          UtilitaireLibelle.getLibelle(this.placeholder, pageContext, null);
      resultat.append(libellePlaceHolder.getMessage());
      resultat.append("\"");
    }

    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());

    // Laisse le taglib parent gérer la fermeture de la balise selon la valeur
    // de l'attribut xhtml de la balise <html:html> de la page JSP
    //resultat.append("/>");
    resultat.append(this.getElementClose());

    afficherCalendrier(resultat, nomFormulaire, getDisabled(), getReadonly(), this.getAlt(),
        getProperty(), getIconeCalendrier(), getHeure(), pageContext);
  }

  /**
   * Méthode qui sert à afficher l'icône de calendrier pour sélectionner une date.
   * <p>
   * @param resultat le contenu de la balise à afficher
   * @param nomFormulaire le nom du formulaire situé dans la page
   * @param disabled valeur identifiant si le calendrier est actif ou pas
   * @param readOnly valeur identifiant si le champ de saisie est en lecture seule ou pas
   * @throws JspException Exception générique
   */
  static public void afficherCalendrier(StringBuffer resultat,
      String nomFormulaire, boolean disabled, boolean readOnly, String alt, String property,
      String iconeCalendrier, boolean heure, PageContext pageContext)
          throws JspException {
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);
    String idCalendrier = property + "1";

    if (!baseForm.isModeLectureSeulement() &&
        ((!disabled && !readOnly && (iconeCalendrier == null)) ||
            ((iconeCalendrier != null) && iconeCalendrier.equals("true")))) {

      resultat.append("&nbsp;<a ");

      resultat.append(" class=\"iconeDate\" href=\"javascript:void(0);\" onclick=\"");
      resultat.append("showCalendar('");
      resultat.append(property);
      resultat.append("','");
      resultat.append(idCalendrier);
      resultat.append("',");

      if (heure) {
        resultat.append("'%Y-%m-%d %H:%M',true");
      } else {
        resultat.append("'%Y-%m-%d', false");
      }
      resultat.append(");\" ");

      resultat.append("id=\"");
      resultat.append(idCalendrier);
      resultat.append("\"");

      if (alt != null) {
        resultat.append(" alt=\"").append(alt).append("\"");
      }
      resultat.append(">");

      // L'attribut BORDER de la balise IMG est DEPRECATED selon le W3C.
      // La gestion du border s'effectue par CSS. Le plus simple étant de mettre
      // img{border:none;} dans sofi.css
      // REF: http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.7.3
      //resultat.append("<img border=\"0\"  ");
      resultat.append("&nbsp;<img");

      // Utilisation du paramètre iconeCalendrier plutôt l'icone codé en dur
      //resultat.append("src=\"images/commun/icone_calendrier.gif\" ");
      if (UtilitaireString.isVide(iconeCalendrier)){
        resultat.append(" src=\"images/sofi/icone_calendrier.gif\"");
      } else {
        resultat.append(" src=\"");
        resultat.append(iconeCalendrier);
        resultat.append("\"");
      }

      // L'attribut ALT de la balise IMG est obligatoire.
      // REF: http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.8
      resultat.append(" alt=\"Calendrier\"");

      String styleCssImage = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.CSS_IMAGE_CHAMP_DATE);
      if (styleCssImage != null) {
        resultat.append("class=\"").append(styleCssImage).append("\" ");
      } else {
        resultat.append("style=\"cursor: pointer;\" ");
      }

      resultat.append("/></a>");
    }
  }

  /**
   * Méthode qui sert à libérer les ressources sytème utilisées
   */
  @Override
  public void release() {
    super.release();
    setPlaceholder(null);
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * @param placeholder the placeholder to set
   */
  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  /**
   * @return the placeholder
   */
  public String getPlaceholder() {
    return placeholder;
  }
}
