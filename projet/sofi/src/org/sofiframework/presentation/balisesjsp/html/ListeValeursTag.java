/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * ListeValeursFieldTag permet d'afficher un champ texte suivi d'une loupe, permettant d'appeller une fenêtre avec une
 * liste de valeur correspondant a ce qui est saisie dans le champ texte.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.0 Ajout de la fonctionnalité permettant l'ouverture d'une fenêtre Ajax.
 */
public class ListeValeursTag extends BaseFormulaireTag {
  private static final long serialVersionUID = -6467092826225948473L;

  /** Variable qui identifie le code HTML de début à utiliser pour afficher l'icône de la loupe */
  private static final String IMAGE_LOUPE_DEBUT = "<img src=\"{0}\" width=\"19\" height=\"19\" border=\"0\" class=\"icone_loupe\" alt=\"Liste de valeurs\"";

  /** Source de l'image de recherche */
  private String srcImage = "images/sofi/icone_loupe.gif";

  /** Variable qui identifie le code HTML de fin à utiliser pour afficher l'icône de la loupe */
  private static final String IMAGE_LOUPE_FIN = "/>";

  /** la valeur indiquant si on doit afficher des barres de défillement ou pas */
  protected String scrollbars = null;

  /** la largeur de la fenêtre flottante */
  protected String largeurfenetre = null;

  /** la hauteur de la fenêtre flottante */
  protected String hauteurfenetre = null;

  /** le nom du paramètre passé au contrôleur par l'URL appelé */
  protected String nomvaleur = null;

  /** le nom de la fenêtre flottante (il ne fait jamais mettre le charactère ' dans ce champ) */
  protected String nomfenetre = null;

  /** la deuxième propriété à afficher et/ou rechercher */
  private String deuxiemePropriete;

  /** la grandeur de la deuxième propriété */
  private String deuxiemeSize;

  /** la longueur maximale de la deuxième propriété */
  private String deuxiemeMaxlength;

  /**
   * la valeur qui indique si le deuxième champs doit afficher une erreur lorsque le composant possède une erreur.
   */
  private boolean deuxiemeElementPossedeErreur = false;

  /**
   * Le ou les paramètres dont on désire avoir dans l'url qui appel la fenêtre de la liste de valeurs.
   */
  private String paramId;

  /**
   * Le ou les propriétés qui retourne les valeurs des paramètres associés.
   */
  private String paramProperty;

  /**
   * Le ou les paramètres dépendants dont on désire avoir dans l'url qui appel la fenêtre de la liste de valeurs.
   */
  private String paramIdDependant;

  /**
   * Le ou les propriétés dépendant qui retourne les valeurs des paramètres associés.
   */
  private String paramPropertyDependant;

  /**
   * Le nom du formulaire dont les valeurs sélection dans la liste de valeur doit être retourné.
   */
  private String formulaireValeurRetour;

  /**
   * Le ou les attributs dont la liste de valeur doit retourner après la sélection d'un item dans la liste.
   */
  private String attributRetour;

  /**
   * Spécifie si on désire que le focus s'arrête sur la loupe ou passe au champ suivant lorsque l'utilisateur navigue
   * dans le formulaie en utilisant la touche TAB. Détermine si un ancre sera utilisé.
   */
  private boolean focusChampSuivant = false;

  /** Variable contenant le href de la fenetre **/
  private String hrefFenetre;

  /**
   * Ajustement de la position X de l'emplacement de la fenêtre ajax.
   */
  private String ajustementPositionX;

  /**
   * Ajustement de la position Y de l'emplacement de la fenêtre ajax.
   */
  private String ajustementPositionY;

  /**
   * Détermine si on désire avoir le style de liste de valeur AJAX ou le style traditionnel avec un popup. La valeur par
   * défaut est Ajax à false pour concerver le comportement original.
   */
  private boolean ajax = false;

  /**
   * Le nom de la variable qui loge le traitement javascript d'une liste de valeurs pour x élément(s).
   */
  private String var;

  /**
   * L'identifiant de la liste de valeurs
   */
  private String identifiant;

  /**
   * Spécifie si on doit appliquer le filtre doit se faire sur l'évènement onchange.
   */
  private boolean activerFiltreSurEvenementOnchange;

  /**
   * Spécifie si on doit appliquer le filtre sur l'évènement onkeyup.
   */
  private boolean activerFiltreSurEvenementOnkeyup;

  /**
   * Spécifie une fonctionnalité JavaScript lors de la liste de valeur ajax a traiter lorsqu'il y a modification au
   * formulaire.
   */
  private String ajaxJSModificationFormulaire;

  /**
   * Spécifie si on désire avoir l'icone de la loupe de recherche. Valeur possible : true/false. Défaut = true;
   */
  private Object iconeLoupe = null;

  /**
   * Spécifie si on désire lancer une nouvelle recherche automatiquement au clic de l'icone de la loupe.
   */
  private Object iconeLoupeNouvelleRecherche;

  /**
   * Spécifie le titre de la fenêtre lorsque traitement ajax.
   * 
   * @since SOFI 2.0.4
   */
  private String titreFenetreAjax;

  private String placeholder = null;

  /** Constructeur par défaut. */
  public ListeValeursTag() {
    super();
  }

  /**
   * Obtenir Lien hypertexte URI à appeler lors d'un click sur l'icône de la loupe.
   * <p>
   * 
   * @return Lien hypertexte URI à appeler lors d'un click sur l'icône de la loupe
   */
  @Override
  public String getHref() {
    return (this.href);
  }

  /**
   * Fixer Lien hypertexte URI à appeler lors d'un click sur l'icône de la loupe.
   * <p>
   * 
   * @param href
   *          Lien hypertexte URI à appeler lors d'un click sur l'icône de la loupe
   */
  @Override
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Obtenir la valeur indiquant si on doit afficher des barres de défillement ou pas.
   * <p>
   * 
   * @return la valeur indiquant si on doit afficher des barres de défillement ou pas
   */
  public String getScrollbars() {
    return (this.scrollbars);
  }

  /**
   * Fixer la valeur indiquant si on doit afficher des barres de défillement ou pas.
   * <p>
   * 
   * @param scrollbars
   *          la valeur indiquant si on doit afficher des barres de défillement ou pas
   */
  public void setScrollbars(String scrollbars) {
    this.scrollbars = scrollbars;
  }

  /**
   * Obtenir la largeur de la fenêtre flottante.
   * <p>
   * 
   * @return la largeur de la fenêtre flottante
   */
  public String getLargeurfenetre() {
    return (this.largeurfenetre);
  }

  /**
   * Fixer la largeur de la fenêtre flottante.
   * <p>
   * 
   * @param largeurfenetre
   *          la largeur de la fenêtre flottante
   */
  public void setLargeurfenetre(String largeurfenetre) {
    this.largeurfenetre = largeurfenetre;
  }

  /**
   * Obtenir la hauteur de la fenêtre flottante.
   * <p>
   * 
   * @return la hauteur de la fenêtre flottante
   */
  public String getHauteurfenetre() {
    return (this.hauteurfenetre);
  }

  /**
   * Fixer la hauteur de la fenêtre flottante.
   * <p>
   * 
   * @param hauteurfenetre
   *          la hauteur de la fenêtre flottante
   */
  public void setHauteurfenetre(String hauteurfenetre) {
    this.hauteurfenetre = hauteurfenetre;
  }

  /**
   * Obtenir le nom du paramètre passé au contrôleur par l'URL appelé.
   * <p>
   * 
   * @return le nom du paramètre passé au contrôleur par l'URL appelé
   */
  public String getNomvaleur() {
    return (this.nomvaleur);
  }

  /**
   * Fixer le nom du paramètre passé au contrôleur par l'URL appelé.
   * <p>
   * 
   * @param nomvaleur
   *          le nom du paramètre passé au contrôleur par l'URL appelé
   */
  public void setNomvaleur(String nomvaleur) {
    this.nomvaleur = nomvaleur;
  }

  /**
   * Obtenir le nom de la fenêtre flottante (il ne fait jamais mettre le charactère ' dans ce champ).
   * <p>
   * 
   * @return le nom de la fenêtre flottante (il ne fait jamais mettre le charactère ' dans ce champ)
   */
  public String getNomfenetre() {
    if (UtilitaireString.isVide(this.nomfenetre)) {
      this.nomfenetre = "popup";
    }

    return (this.nomfenetre);
  }

  /**
   * Fixer le nom de la fenêtre flottante (il ne fait jamais mettre le charactère ' dans ce champ).
   * <p>
   * 
   * @param nomfenetre
   *          le nom de la fenêtre flottante (il ne fait jamais mettre le charactère ' dans ce champ)
   */
  public void setNomfenetre(String nomfenetre) {
    this.nomfenetre = nomfenetre;
  }

  /**
   * Obtenir la deuxième propriété à afficher et/ou rechercher.
   * <p>
   * 
   * @return la deuxième propriété à afficher et/ou rechercher
   */
  public String getDeuxiemePropriete() {
    return deuxiemePropriete;
  }

  /**
   * Fixer la deuxième propriété à afficher et/ou rechercher.
   * <p>
   * 
   * @param deuxiemePropriete
   *          la deuxième propriété à afficher et/ou rechercher
   */
  public void setDeuxiemePropriete(String deuxiemePropriete) {
    this.deuxiemePropriete = deuxiemePropriete;
  }

  /**
   * Obtenir la grandeur de la deuxième propriété.
   * <p>
   * 
   * @return la grandeur de la deuxième propriété
   */
  public String getDeuxiemeSize() {
    return deuxiemeSize;
  }

  /**
   * Fixer la grandeur de la deuxième propriété.
   * <p>
   * 
   * @param deuxiemeSize
   *          la grandeur de la deuxième propriété
   */
  public void setDeuxiemeSize(String deuxiemeSize) {
    this.deuxiemeSize = deuxiemeSize;
  }

  /**
   * Obtenir la longueur maximale de la deuxième propriété.
   * <p>
   * 
   * @return la longueur maximale de la deuxième propriété
   */
  public String getDeuxiemeMaxlength() {
    return deuxiemeMaxlength;
  }

  /**
   * Fixer la longueur maximale de la deuxième propriété.
   * <p>
   * 
   * @param deuxiemeMaxlength
   *          la longueur maximale de la deuxième propriété
   */
  public void setDeuxiemeMaxlength(String deuxiemeMaxlength) {
    this.deuxiemeMaxlength = deuxiemeMaxlength;
  }

  /**
   * Obtenir la valeur qui indique si le deuxième champs doit afficher une erreur lorsque le composant possède une
   * erreur.
   * <p>
   * 
   * @return la valeur qui indique si le deuxième champs doit afficher une erreur lorsque le composant possède une
   *         erreur
   */
  public boolean getDeuxiemeElementPossedeErreur() {
    return deuxiemeElementPossedeErreur;
  }

  /**
   * Retourne les identifiants des paramètres de url appelant la liste de valeurs. Chacun des identifiants peuvent être
   * séparé d'une virgule.
   * 
   * @return les identifiants au paramètres.
   */
  public String getParamId() {
    return paramId;
  }

  /**
   * Fixer les identifiants des paramètres de url appelant la liste de valeurs. Si plusieurs identifiant, séparer les
   * d'une virgule.
   * 
   * @param paramId
   */
  public void setParamId(String paramId) {
    this.paramId = paramId;
  }

  /**
   * Retourne les propriétés des paramètres de url appelant la liste de valeurs. Chacune des propriétés peuvent être
   * séparé d'une virgule.
   * 
   * @return les propriétés au paramètres.
   */
  public String getParamProperty() {
    return paramProperty;
  }

  /**
   * Fixer les propriétés des paramètres de url appelant la liste de valeurs. Si plusieurs propriétés, séparer les d'une
   * virgule.
   * 
   * @param paramProperty
   */
  public void setParamProperty(String paramProperty) {
    this.paramProperty = paramProperty;
  }

  /**
   * Fixer la valeur qui indique si le deuxième champs doit afficher une erreur lorsque le composant possède une erreur.
   * <p>
   * 
   * @param deuxiemeElementPossedeErreur
   *          la valeur qui indique si le deuxième champs doit afficher une erreur lorsque le composant possède une
   *          erreur
   */
  public void setDeuxiemeElementPossedeErreur(boolean deuxiemeElementPossedeErreur) {
    this.deuxiemeElementPossedeErreur = deuxiemeElementPossedeErreur;
  }

  /**
   * Obtenir la valeur qui spécifie si on désire passer le focus au champ suivant.
   * <p>
   * 
   * @return la valeur qui spécifie si on désire passer le focus au champ suivant.
   */
  public boolean getFocusChampSuivant() {
    return focusChampSuivant;
  }

  /**
   * Fixer la valeur qui spécifie si on désire passer le focus au champ suivant.
   * <p>
   * 
   * @param focusChampSuivant
   *          la valeur qui spécifie si on désire passer le focus au champ suivant.
   */
  public void setFocusChampSuivant(boolean focusChampSuivant) {
    this.focusChampSuivant = focusChampSuivant;
  }

  /*
   * Débute le traitement de la balise Jsp. <p>
   * 
   * @exception JspException si une exception JSP est lancé
   * 
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */

  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    if (getAjaxJSModificationFormulaire() != null) {
      pageContext.setAttribute(ConstantesBaliseJSP.FONCTION_JS_APRES_MODIFICATION_FORMULAIRE,
          getAjaxJSModificationFormulaire());
    }

    // Récupérer certaines variables utilises
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    if (getHref() != null) {
      setHrefFenetre(getHref());
      setHref(null);
    }

    if (getVar() != null) {
      if (isAjax()) {
        pageContext.setAttribute(getVar(), ajouterLoupeAjax(baseForm));
      } else {
        pageContext.setAttribute(getVar(), ajouterLoupeJavascript(baseForm));
      }

      return (SKIP_BODY);
    }

    /**
     * Si paramètre la propriété iconeAide est null et qu'il existe un paramètre système, alors prendre la valeur de
     * celui-ci.
     * 
     * @since SOFI 2.1
     */
    if (this.iconeLoupeNouvelleRecherche == null) {
      Object listeValeurIconeLoupe = GestionParametreSysteme.getInstance().getParametreSysteme(
          ConstantesParametreSysteme.LISTE_VALEUR_ICONE_LOUPE_NOUVELLE_RECHERCHE);

      if (listeValeurIconeLoupe != null) {
        setIconeLoupeNouvelleRecherche(listeValeurIconeLoupe.toString());
      } else {
        setIconeLoupeNouvelleRecherche("true");
      }
    }

    if (this.getTitreFenetreAjax() != null) {
      Libelle libelle = UtilitaireLibelle.getLibelle(this.getTitreFenetreAjax(), pageContext, null);
      String titre = libelle.getMessage();
      setTitreFenetreAjax(titre);
    }

    super.doStartTag();

    // Continue processing this page
    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Méthode qui sert à générer le contenu de la balise à afficher.
   * <p>
   * 
   * @param resultat
   *          le code à inscrire à l'intérieur de la balise
   * @throws JspException
   *           si une exception JSP est lancé
   */
  @Override
  protected void ecrireBalise(StringBuffer resultat) throws JspException {
    // Ajouter le(s) champ(s) de saisie
    this.ajouterInputTexte(resultat, this.getTitle(), this.property, this.cols, this.maxlength, true);

    if ((this.deuxiemePropriete != null) && (this.deuxiemePropriete.length() > 0)) {
      resultat.append("&nbsp;&nbsp;");
      this.ajouterInputTexte(resultat, this.getTitle(), this.deuxiemePropriete, this.deuxiemeSize,
          this.deuxiemeMaxlength, this.deuxiemeElementPossedeErreur);
    }
  }

  /**
   * Méthode qui sert à produire le bout de code qui génère un textField
   * <p>
   * 
   * @param resultat
   *          le StringBuffer auquel il faut ajouter le composant
   * @param titre
   *          le titre à affiche dans la zone de text en cas d'erreur
   * @param propriete
   *          la propriete à afficher dans la zone de texte
   * @param grandeur
   *          la grandeur du champs
   * @param grandeurSaisie
   *          le nombre de charactères que l'on peut saisir
   * @param afficherErreur
   *          valeur qui indique si l'on doit afficher les erreurs dans champ de saisie ou si on laisse le tout en
   *          format normal
   */
  private void ajouterInputTexte(StringBuffer resultat, String titre, String propriete, String grandeur,
      String grandeurSaisie, boolean afficherErreur) throws JspException {
    // Accès au formulaire en cours.
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(this.pageContext);

    String nomFormulaireJS = null;

    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    if (isTraitementTransactionnel()) {
      nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext, property);
    }

    Libelle libelle = UtilitaireLibelle.getLibelle(getLibelle(), this.pageContext, null);

    if (libelle.getPlaceholder() != null && StringUtils.isEmpty(getPlaceholder())) {
      setPlaceholder(libelle.getPlaceholder());
    }

    String valeurDuChampATraiter = null;
    resultat.append("<input type=\"text");
    resultat.append("\" name=\"");
    resultat.append(propriete);
    resultat.append("\"");

    if (getAccesskey() != null) {
      resultat.append(" accesskey=\"");
      resultat.append(getAccesskey());
      resultat.append("\"");
    }

    if (accept != null) {
      resultat.append(" accept=\"");
      resultat.append(accept);
      resultat.append("\"");
    }

    if (maxlength != null) {
      resultat.append(" maxlength=\"");
      resultat.append(grandeurSaisie);
      resultat.append("\"");
    }

    if (cols != null) {
      resultat.append(" size=\"");
      resultat.append(grandeur);
      resultat.append("\"");
    }

    if (this.getTabindex() != null) {
      resultat.append(" tabindex=\"");
      resultat.append(this.getTabindex());
      resultat.append("\"");
    }

    if ((titre != null) && afficherErreur) {
      resultat.append(" title=\"");
      resultat.append(titre);
      resultat.append("\"");
    }

    if (StringUtils.isNotBlank(getPlaceholder())) {
      resultat.append(" placeholder=\"");
      Libelle libellePlaceHolder = UtilitaireLibelle.getLibelle(this.placeholder, pageContext, null);
      resultat.append(libellePlaceHolder.getMessage());
      resultat.append("\"");
    }

    /*
     * resultat.append(" id=\""); resultat.append(propriete); resultat.append("\"");
     */

    // Fixer l'évènement onchange ou onckeyup pour appel de la liste de valeur.
    if (!getReadonly() && !getDisabled()
        && (isActiverFiltreSurEvenementOnchange() || isActiverFiltreSurEvenementOnkeyup())) {
      String appelJS = getUrlLoupeAjax(baseForm, null, true, false).toString();

      // Fixer l'url de la loupe dans le contexte de la page afin d'etre répuré par les autres critères
      // de la liste de valeurs.
      if (isActiverFiltreSurEvenementOnchange()) {
        pageContext.setAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONCHANGE, appelJS);
        appelJS = remplacerParametreProperty(appelJS, getProperty(), pageContext);
        setOnchange(appelJS);
      } else {
        pageContext.setAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONKEYUP, appelJS);
        appelJS = remplacerParametreProperty(appelJS, getProperty(), pageContext);
        setOnkeyup(appelJS);
      }
    }

    if (isTraitementTransactionnel()) {
      UtilitaireBaliseJSP.specifierGestionModification(this.pageContext, nomFormulaire, nomFormulaireJS, getOnchange(),
          resultat);
    }

    // Gestion de l'affichage en mode Readonly
    if (baseForm.isModeLectureSeulement() || this.getDisabled() || this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.getStyleClassLectureSeulement(), getStyleClass(), resultat);

      if (!GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.READONLY_DISABLED_DISTINCT)
          .booleanValue()) {
        setDisabled(false);
        setReadonly(true);
      }
    } else {

      if (afficherErreur) {
        UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(), this.getStyleClassFocus(), getOnfocus(), getOnblur(),
            resultat);
      } else {
        UtilitaireBaliseJSP.specifierStyleFocus(null, null, getOnfocus(), getOnblur(), resultat);
      }
    }

    resultat.append(" value=\"");

    try {
      valeurDuChampATraiter = UtilitaireObjet.getValeurAttribut(baseForm, propriete).toString();
    } catch (Exception e) {

    }

    if (valeurDuChampATraiter != null) {
      resultat.append(TagUtils.getInstance().filter(valeurDuChampATraiter));
    } else {
      resultat.append("");
    }

    resultat.append("\"");
    resultat.append(prepareEventHandlers());

    if (!afficherErreur) {
      String style1Temp = this.getStyleClass();
      String style2Temp = this.getStyleClassFocus();
      this.setStyleClass(null);
      this.setStyleClassFocus(null);
      resultat.append(prepareStyles());
      this.setStyleClass(style1Temp);
      this.styleClassFocus = style2Temp;
    } else {
      resultat.append(prepareStyles());
    }

    resultat.append("/>");
  }

  /**
   * Méthode qui sert à produire le bout de code qui génère un la loupe de la liste de valeur
   * <p>
   * 
   * @param resultat
   *          le StringBuffer auquel il faut ajouter le composant
   * @throws javax.servlet.jsp.JspException
   */
  private void ajouterLoupe(StringBuffer resultat) throws JspException {
    // Accès au formulaire principal.
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    boolean securise = UtilitaireBaliseJSP.isBlocSecuriseEnLectureSeulement(pageContext)
        || this.isFormulaireEnAffichageSeulement() || baseForm.isModeLectureSeulement();

    if (!securise) {
      // Ajout du paramètre <a href>LOUPE</a>
      if (isAjax()) {
        resultat.append(ajouterLoupeAjax(baseForm));
      } else {
        resultat.append(ajouterLoupeJavascript(baseForm));
      }
    }
  }

  private StringBuffer ajouterLoupeAjax(BaseForm baseForm) throws JspException {
    StringBuffer resultat = new StringBuffer();

    if (isAfficherIconeLoupe() || (!getReadonly() && !getDisabled())) {
      if (getVar() == null) {
        resultat.append("<a href=\"javascript:void(0);\" onclick=\"");
      }

      // L'identifiant de l'image de la loupe.
      String idImage = UtilitaireBaliseJSP.getNomProprieteVide(pageContext);

      resultat.append(getUrlLoupeAjax(baseForm, idImage, false, true));

      if (isActiverFiltreSurEvenementOnchange() || isActiverFiltreSurEvenementOnkeyup()) {
        // Le nom du formulaire en traitement.
        String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

        // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
        String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, getProperty());
        String attributAInitialiser = getParamProperty();

        if (!UtilitaireString.isVide(getAttributRetour())) {
          attributAInitialiser = getAttributRetour();
        }

        StringTokenizer listeParamProperty = new StringTokenizer(attributAInitialiser, ",");

        while (listeParamProperty.hasMoreTokens()) {
          String paramProperty = listeParamProperty.nextToken();
          resultat.append("document.forms[");

          if (nomFormulaire != null) {
            resultat.append("'");
            resultat.append(nomFormulaire);
            resultat.append("'");
          } else {
            resultat.append("0");
          }

          resultat.append("].elements['");

          if (nomFormulaireImbrique == null) {
            resultat.append(paramProperty.trim());
            resultat.append("'].value='';");
          } else {
            resultat.append(nomFormulaireImbrique);
            resultat.append(".");
            resultat.append(paramProperty.trim());
            resultat.append("'].value='';");
          }
        }
      }

      // Ajouter le focus sur le premier lien de la liste ajax
      resultat.append("traitementApresInitListeValeur();");

      // resultat.append("return false;");
      if (getVar() == null) {
        String src = IMAGE_LOUPE_DEBUT.replaceAll("[{][0][}]", this.getSrcImage());

        String cssChampIconeListeValeur = GestionParametreSysteme.getInstance().getString(
            ConstantesParametreSysteme.CSS_IMAGE_LISTE_VALEUR);
        if (cssChampIconeListeValeur != null) {
          src += " class=\"" + cssChampIconeListeValeur + "\" ";
        } else {
          src += " align=\"middle\" ";
        }
        resultat.append("\">").append(src).append(" id = \"").append(idImage).append("\"").append(IMAGE_LOUPE_FIN)
        .append("</a>");
      }
    }
    return resultat;
  }

  private String getUrlLoupeAjax(BaseForm form, String idImage, boolean filtrage, boolean iconeLoupe)
      throws JspException {
    StringBuffer resultatUrl = new StringBuffer();

    resultatUrl.append("url = '").append(this.getURL());

    String fonctionJSApresModification = (String) pageContext
        .getAttribute(ConstantesBaliseJSP.FONCTION_JS_APRES_MODIFICATION_FORMULAIRE);

    if (fonctionJSApresModification != null) {
      String idUnique = UtilitaireBaliseJSP.getNomProprieteVide(pageContext);
      resultatUrl.append("&idAjaxJSModificationFormulaire=");
      resultatUrl.append(idUnique);
      UtilitaireControleur.setAttributTemporairePourAction(idUnique, getAjaxJSModificationFormulaire(),
          (HttpServletRequest) pageContext.getRequest());

      this.setAjaxJSApresChargement(UtilitaireString.convertirEnJavaScript(fonctionJSApresModification));
    }

    resultatUrl.append(this.getListeParametreURLJavascript(filtrage, iconeLoupe)).append(" + '&ajax=true'").append(";");

    if (!iconeLoupe) {
      resultatUrl.append("if (this.value != ''){");
    }

    resultatUrl.append("genererFenetreListeValeurAJAX('").append(getNomfenetre());
    resultatUrl.append("','");

    if ((getVar() != null) || (idImage == null)) {
      resultatUrl.append(getProperty());
      resultatUrl.append("'");
      resultatUrl.append(",'");
      resultatUrl.append("[PROPERTY]");
    } else {
      resultatUrl.append(getProperty());
      resultatUrl.append("','");
      resultatUrl.append(idImage);
    }

    resultatUrl.append("'");
    resultatUrl.append(",").append("url, ");
    resultatUrl.append(this.getLargeurfenetre());
    resultatUrl.append(", ");
    resultatUrl.append(this.getHauteurfenetre()).append(",");
    resultatUrl.append(getAjustementPositionX()).append(",");
    resultatUrl.append(getAjustementPositionY()).append(",event");

    resultatUrl.append(",'");
    resultatUrl.append(UtilitaireString.convertirEnJavaScript("fireOnClickEvent('jsListeNavigation');"));

    if (getAjaxJSApresChargement() != null || getTitreFenetreAjax() != null) {
      if (getTitreFenetreAjax() != null) {
        String fonctionTitre = "setTitreFenetre('" + UtilitaireString.convertirEnJavaScript(getTitreFenetreAjax())
            + "');";
        resultatUrl.append(UtilitaireString.convertirEnJavaScript(fonctionTitre));
      }
      if (getAjaxJSApresChargement() != null) {
        resultatUrl.append(getAjaxJSApresChargement());
      }
    }

    resultatUrl.append("'");
    resultatUrl.append(");");

    if (!iconeLoupe) {
      resultatUrl.append("};");
    }

    if (getIdentifiant() != null) {
      StringBuffer scriptJSInitialiserIdentifiant = new StringBuffer();

      // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
      String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, getProperty());
      scriptJSInitialiserIdentifiant.append("document.getElementById('");

      StringBuffer idAttribut = new StringBuffer();

      if (nomFormulaireImbrique != null) {
        idAttribut.append(nomFormulaireImbrique);
        idAttribut.append(".");
      }

      idAttribut.append(getIdentifiant());
      scriptJSInitialiserIdentifiant.append(idAttribut);
      scriptJSInitialiserIdentifiant.append("').value='';");

      if (!iconeLoupe) {
        if (!getProperty().equals(identifiant)) {
          resultatUrl.append(scriptJSInitialiserIdentifiant);
        } else {
          resultatUrl.append("[TRAITEMENT_INITALISER_IDENTIFIANT]");
          pageContext.setAttribute("listeValeurTraitementJSInitialiserIdentifiant",
              scriptJSInitialiserIdentifiant.toString());
          pageContext.setAttribute("listeValeurIdentifiant", idAttribut.toString());
        }
      }
    }

    /**
     * Activation du debuggeur Ajax si spécifié dans le fichier des paramètres développeur. Nom de param : debugAjax
     * (true/false)
     */
    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultatUrl.append("setDebugAjax(true);");
    }

    if (!isActiverFiltreSurEvenementOnkeyup()) {
      // Ajouter le focus sur le premier lien de la liste ajax
      resultatUrl.append("focusLienListeNavigation();");
    }

    return resultatUrl.toString();
  }

  public static String remplacerParametreProperty(String urlLoupe, String nomAttributEnTraitement, PageContext contexte) {
    urlLoupe = UtilitaireString.remplacerTous(urlLoupe, "[PROPERTY]", nomAttributEnTraitement);

    // Accès au répertoire de correspondance entre les propriétés est les identifiants
    // de paramètre dans l'url.
    HashMap correspondancePropertyId = getCorrespondanceIdProperty(contexte);
    urlLoupe = UtilitaireString.remplacerTous(urlLoupe, "[PARAM_ID]",
        (String) correspondancePropertyId.get(nomAttributEnTraitement));

    String listeValeurTraitementJSInitialiserIdentifiant = (String) contexte
        .getAttribute("listeValeurTraitementJSInitialiserIdentifiant");
    String listeValeurIdentifiant = (String) contexte.getAttribute("listeValeurIdentifiant");

    if ((listeValeurTraitementJSInitialiserIdentifiant != null)
        && !nomAttributEnTraitement.equals(listeValeurIdentifiant)) {
      urlLoupe = UtilitaireString.remplacerTous(urlLoupe, "[TRAITEMENT_INITALISER_IDENTIFIANT]",
          listeValeurTraitementJSInitialiserIdentifiant);
    } else {
      urlLoupe = UtilitaireString.remplacerTous(urlLoupe, "[TRAITEMENT_INITALISER_IDENTIFIANT]", "");
    }

    return urlLoupe;
  }

  /**
   * Contruire une loupe style Javascript pour rechercher et obtenir les résultats.
   * 
   * @throws javax.servlet.jsp.JspException
   *           Erreur JSP survenu lors de l'exécution
   * @param baseForm
   *          Formulaire en cours
   * @return le resultat de retour.
   */
  private StringBuffer ajouterLoupeJavascript(BaseForm baseForm) throws JspException {
    StringBuffer resultat = new StringBuffer();

    if (isAfficherIconeLoupe() || (!getReadonly() && !getDisabled())) {

      if (getVar() == null) {
        resultat.append("&nbsp;");

        if (getFocusChampSuivant()) {
          resultat.append(IMAGE_LOUPE_DEBUT);
        } else {
          resultat.append("<a href=\"javascript:void(0)\"");
        }

        resultat.append(" onClick=\"javascript:");
      }

      resultat.append("popup('");

      resultat.append(this.getURL());
      resultat.append(this.getListeParametreURLJavascript(true, true));

      resultat.append(", '");

      resultat.append(this.getNomfenetre());

      resultat.append("', '");
      resultat.append(getLargeurfenetre());
      resultat.append("', '");
      resultat.append(getHauteurfenetre());
      resultat.append("', '");

      resultat.append("scrollbars=");

      if ((this.scrollbars == null) || this.scrollbars.trim().toLowerCase().equals("true")) {
        resultat.append("1");
      } else {
        resultat.append("0");
      }

      resultat.append("'");

      if (getVar() == null) {
        resultat.append(");\">");

        if (!getFocusChampSuivant()) {
          resultat.append(IMAGE_LOUPE_DEBUT);
          resultat.append(IMAGE_LOUPE_FIN);
          resultat.append("</a>");
        }
      }
    }

    int positionSrcImage = resultat.indexOf("{0}");
    resultat.replace(positionSrcImage, positionSrcImage + 3, this.getSrcImage());

    return resultat;
  }

  /**
   * Obtenir l'URL en le complétant et en interprétant le JSTL.
   * 
   * @throws javax.servlet.jsp.JspException
   *           Erreur JSP survenu lors du traitement
   * @return URL complété et interprété JSTL.
   */
  private String getURL() throws JspException {
    String url = null;

    try {
      String lienComplet = (String) UtilitaireBaliseJSP.getValeurEL("hrefFenetre", getHrefFenetre(), String.class,
          this, this.pageContext);

      url = TagUtils.getInstance().computeURL(this.pageContext, null, lienComplet, null, null, null, new HashMap(),
          null, false);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(pageContext, e);
      throw new JspException(messages.getMessage("rewrite.url", e.toString()));
    }

    return url;
  }

  /**
   * Retourne la liste des parametres qui doit être inclus dans la requête qui fait appel à la fenêtre avec la liste de
   * valeurs.
   * 
   * @return les paramètres de l'url de l'appel de la fenêtre de la liste de valeurs.
   */
  private String getListeParametreURLJavascript(boolean filtrage, boolean iconeLoupe) {
    StringBuffer js = null;

    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, getProperty());

    /*
     * Si on a un évènement ajax sur les champs on doit produire un gabarit qui sera réutilisé lors du traitement dans
     * les balises.
     */
    boolean evenementAjaxSurChamp = isActiverFiltreSurEvenementOnchange() || isActiverFiltreSurEvenementOnkeyup();

    /*
     * Si on a seulement les propriétés directement spécifiées avec "property"
     */
    if (getParamProperty() == null) {
      js = genererProprietesStatiques(nomFormulaire);
    } else {
      js = genererProprietesDynamiques(nomFormulaire, nomFormulaireImbrique, false);

      if (evenementAjaxSurChamp && isLancerRechercheSurIconeLoupe()) {
        if (iconeLoupe && (getVar() == null)) {
          js = new StringBuffer("'");
        } else {
          js = genererGabaritEvenementSurChamp(nomFormulaire);
        }

        if (!StringUtils.isEmpty(getParamIdDependant())) {
          StringBuffer extraProprietes = genererProprietesDependantDynamiques(nomFormulaire, nomFormulaireImbrique);
          js.append("+ '");
          js.append(extraProprietes);
        }
      } else {
        js = genererProprietesDynamiques(nomFormulaire, nomFormulaireImbrique, false);
      }

      js.append("+'&attributRetour=");
      js.append(genererListeAttributRetourFenetre(nomFormulaireImbrique));

      if (!isTraitementTransactionnel()) {
        js.append("'");
        js.append("+'&transactionnel=false");
      }

      if (iconeLoupe) {
        js.append("&").append(ConstantesBaliseJSP.INITIALISER_PARAMETRES_LISTE_NAVIGATION).append("=true");
      }

      js.append("&formulaireValeurRetour=").append(nomFormulaire).append("'");

    }

    return js.toString();
  }

  /**
   * Obtenir le javascript des propriétés lorsqu'elles sont spécifiés de facon statique avec les attributs de balises
   * "property" et "deuxiemePropriete".
   * 
   * @return
   * @param nomFormulaire
   *          Nom du formulaire en cours
   */
  private StringBuffer genererProprietesStatiques(String nomFormulaire) {
    StringBuffer js = new StringBuffer();

    js.append("&valeur=").append("' + escape(document.forms['").append(nomFormulaire).append("'].elements['")
    .append(property);

    if (this.deuxiemePropriete != null) {
      js.append("'].value) + '&valeur2=' + escape(document.forms['").append(nomFormulaire).append("'].elements['")
      .append(this.deuxiemePropriete);
    }

    js.append("'].value)");

    return js;
  }

  /**
   * Obtenir le javascript des propriétés dynamiques spécifiés par les attributs de balise "paramId" et "paramProperty".
   * 
   * @return
   * @param evenementAjaxSurChamp
   *          Si un évènement AJAX (onchange ou onKeyUp) doit être ajouté aux balises incluses dans la liste de valeur.
   * @param nomFormulaireImbrique
   *          Nom du formulaire Nested
   * @param nomFormulaire
   *          NOm du formulaire en cours
   */
  private StringBuffer genererProprietesDynamiques(String nomFormulaire, String nomFormulaireImbrique,
      boolean evenementAjaxSurChamp) {
    StringBuffer js = new StringBuffer();

    // Traiter si paramId et paramProperty sont spécifié
    StringTokenizer listeParamId = new StringTokenizer(getParamId(), ",");
    StringTokenizer listeParamProperty = new StringTokenizer(getParamProperty(), ",");

    if (listeParamId.countTokens() != listeParamProperty.countTokens()) {
      throw new SOFIException("ERREUR Balise listeValeurs : Le nombre de paramètres de paramId "
          + "doit être égale au nombre de paramètre de paramProperty");
    }

    HashMap correspondancePropertyId = new HashMap();

    for (int i = 0; listeParamId.hasMoreTokens(); i++) {
      String paramId = UtilitaireString.supprimerTousLesBlancs(listeParamId.nextToken());
      String paramProperty = UtilitaireString.supprimerTousLesBlancs(listeParamProperty.nextToken());

      if (nomFormulaireImbrique != null) {
        paramProperty = nomFormulaireImbrique + "." + paramProperty;
      }

      // Ajouter la correspondance entre la propriété et l'identifiant du paramètre dans l'url.
      correspondancePropertyId.put(paramProperty, paramId);

      if (!evenementAjaxSurChamp) {
        if (i > 0) {
          js.append(" + '");
        }

        js.append(genererJavascriptGetValeur(nomFormulaire, paramId, paramProperty));
      }
    }

    pageContext.setAttribute(ConstantesBaliseJSP.LISTE_VALEURS_CORRESPONDANCE_PROPERTY_ID, correspondancePropertyId);

    return js;
  }

  /**
   * Obtenir le javascript des propriétés dependant dynamiques spécifiés par les attributs de balise "paramIdDependant"
   * et "paramPropertyDependant".
   * 
   * @return
   * @param nomFormulaireImbrique
   *          Nom du formulaire Nested
   * @param nomFormulaire
   *          NOm du formulaire en cours
   */
  private StringBuffer genererProprietesDependantDynamiques(String nomFormulaire, String nomFormulaireImbrique) {
    StringBuffer js = new StringBuffer();

    // Traiter si paramId et paramProperty sont spécifié
    StringTokenizer listeParamId = new StringTokenizer(getParamIdDependant(), ",");
    StringTokenizer listeParamProperty = new StringTokenizer(getParamPropertyDependant(), ",");

    if (listeParamId.countTokens() != listeParamProperty.countTokens()) {
      throw new SOFIException("ERREUR Balise listeValeurs : Le nombre de paramètres de paramId "
          + "doit être égale au nombre de paramètre de paramProperty");
    }

    for (int i = 0; listeParamId.hasMoreTokens(); i++) {
      String paramId = UtilitaireString.supprimerTousLesBlancs(listeParamId.nextToken());
      String paramProperty = UtilitaireString.supprimerTousLesBlancs(listeParamProperty.nextToken());

      if (nomFormulaireImbrique != null) {
        paramProperty = nomFormulaireImbrique + "." + paramProperty;
      }

      if (i > 0) {
        js.append(" + '");
      }

      js.append(genererJavascriptGetValeur(nomFormulaire, paramId, paramProperty));
    }

    return js;
  }

  /**
   * Obtenir le javascript qui est utilisé pour le retour des attributs lors du clique de l'utilisateur d'une des ligne
   * de la liste.
   * 
   * @return
   * @param nomFormulaireImbrique
   */
  private StringBuffer genererListeAttributRetourFenetre(String nomFormulaireImbrique) {
    StringBuffer listeAttributRetour = new StringBuffer();

    /*
     * La liste des attributs peut etre basé su les paramProperties ou sur attributRetour. Si un attributRetour est
     * spécifié cest lui quie st utilisé sinon c'est attributRetour. AttributRetour est utilisé si on doit retourner
     * plus de paramètres quil est spécifié pour appeler la fenetre ou si ils sont différents.
     */
    String attributs = (getAttributRetour() != null) ? getAttributRetour() : getParamProperty();

    if ((nomFormulaireImbrique != null) && (attributs != null)) {
      for (StringTokenizer tk = new StringTokenizer(attributs, ","); tk.hasMoreTokens();) {
        String attribut = tk.nextToken().trim();

        // Ajouter des virgules seulement s'il y a déjà un paramètre dans la liste.
        if (listeAttributRetour.length() > 0) {
          listeAttributRetour.append(",");
        }

        String nomParent = getNestedProperty();

        // Ajouter le nom de l'attribut retour.
        if ((nomParent != null) && (paramProperty.indexOf(nomParent) == -1)) {
          // Traiter les formulaires parents.
          listeAttributRetour.append(nomParent);
          listeAttributRetour.append('.');
        }

        // Ajouter le nom de l'attribut retour.
        listeAttributRetour.append(attribut);
      }
    } else {
      listeAttributRetour.append(attributs);
    }

    return listeAttributRetour;
  }

  private String getNestedProperty() {
    return NestedPropertyHelper.getCurrentProperty((HttpServletRequest) pageContext.getRequest());
  }

  private StringBuffer genererGabaritEvenementSurChamp(String nomFormulaire) {
    StringBuffer js = new StringBuffer();
    js.append("&[PARAM_ID]=' + escape(document.forms[");

    if (nomFormulaire != null) {
      js.append("'").append(nomFormulaire).append("'");
    } else {
      js.append("0");
    }

    js.append("].elements['[PROPERTY]'].value)");

    return js;
  }

  private StringBuffer genererJavascriptGetValeur(String nomFormulaire, String paramId, String paramProperty) {
    StringBuffer js = new StringBuffer();
    js.append("&").append(paramId.trim()).append("=' + escape(document.forms[");

    if (nomFormulaire != null) {
      js.append("'").append(nomFormulaire).append("'");
    } else {
      js.append("0");
    }

    js.append("].elements['").append(paramProperty.trim()).append("'].value)");

    return js;
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * 
   * @throws JspException
   *           si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    if ((getVar() == null) && isAfficherIconeLoupe()) {
      // Vérifier la sécurité lié au bouton
      GestionSecurite gestionSecurite = GestionSecurite.getInstance();
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

      // Si la sécurité est actif.
      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // L'utilisateur à droit de voir le composant.
        if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur, this.libelle)) {
          this.ajouterLoupe(resultat);
        }
      } else {
        this.ajouterLoupe(resultat);
      }

      TagUtils.getInstance().write(pageContext, resultat.toString());
    }

    pageContext.setAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONKEYUP, null);
    pageContext.setAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONCHANGE, null);

    pageContext.setAttribute(ConstantesBaliseJSP.FONCTION_JS_APRES_MODIFICATION_FORMULAIRE, null);

    return super.doEndTag();
  }

  /**
   * Évaluer les expressionn régulières
   */
  @Override
  public void evaluerEL() throws JspException {
    super.evaluerEL();

    String ajaxJSModificationFormulaire = EvaluateurExpression.evaluerString("ajaxJSModificationFormulaire",
        getAjaxJSModificationFormulaire(), this, pageContext);
    setAjaxJSModificationFormulaire(ajaxJSModificationFormulaire);

    if (iconeLoupe != null) {
      String isIcone = EvaluateurExpression.evaluerString("iconeLoupe", (String) getIconeLoupe(), this, pageContext);
      setIconeLoupe(isIcone);
    }

    if (this.srcImage != null) {
      String src = EvaluateurExpression.evaluerString("srcImage", this.getSrcImage(), this, pageContext);
      this.setSrcImage(src);
    }
  }

  /**
   * Méthode qui sert à libérer les ressources utilisées
   */
  @Override
  public void release() {
    super.release();
    this.setScrollbars(null);
    this.setLargeurfenetre(null);
    this.setHauteurfenetre(null);
    this.setNomvaleur(null);
    this.setNomfenetre(null);
    this.setDeuxiemePropriete(null);
    this.setDeuxiemeSize(null);
    this.setDeuxiemeMaxlength(null);
    this.setDeuxiemeElementPossedeErreur(false);
    this.setParamId(null);
    this.setParamProperty(null);
    this.setFormulaireValeurRetour(null);
    this.setAttributRetour(null);
    this.setFocusChampSuivant(false);
    this.setHrefFenetre(null);
    this.setAjustementPositionX(null);
    this.setAjustementPositionY(null);
    this.setAjax(false);
    this.setVar(null);
    this.setIdentifiant(null);
    this.setActiverFiltreSurEvenementOnchange(false);
    this.setActiverFiltreSurEvenementOnkeyup(false);
    this.setAjaxJSModificationFormulaire(null);
    this.setIconeLoupe(null);
    this.setIconeLoupeNouvelleRecherche(null);
    this.setPlaceholder(null);
  }

  public void setFormulaireValeurRetour(String formulaireValeurRetour) {
    this.formulaireValeurRetour = formulaireValeurRetour;
  }

  public String getFormulaireValeurRetour() {
    return formulaireValeurRetour;
  }

  public void setAttributRetour(String attributRetour) {
    this.attributRetour = attributRetour;
  }

  public String getAttributRetour() {
    return attributRetour;
  }

  /**
   * Fixer si le style de liste de valeur AJAX ou dans une fenêtre flottante de type navigateur.
   * 
   * @param ajax
   *          Si true la liste sera AJAX, sinon ouverture dans une fenêtre flottante de type navigateur.
   */
  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  /**
   * Obtenir si le style de liste de valeur sera AJAX ou dans une fenêtre flottante de type navigateur.
   * 
   * @return Si true la liste sera AJAX, sinon ouverture dans une fenêtre flottante de type navigateur.
   */
  public boolean isAjax() {
    return ajax;
  }

  /**
   * Fixer le href de la fenêtre
   * 
   * @param hrefFenetre
   *          href de la fenêtre
   */
  public void setHrefFenetre(String hrefFenetre) {
    this.hrefFenetre = hrefFenetre;
  }

  /**
   * Retourne le href de la fenêtre.
   * 
   * @return le href de la fenêtre.
   */
  public String getHrefFenetre() {
    return hrefFenetre;
  }

  /**
   * Fixer l'ajustement de la position X de la fenêtre ajax.
   * 
   * @param ajustementPositionX
   *          l'ajustement de la position X de la fenêtre ajax.
   */
  public void setAjustementPositionX(String ajustementPositionX) {
    this.ajustementPositionX = ajustementPositionX;
  }

  /**
   * Retourne l'ajustement de la position X de la fenêtre ajax.
   * 
   * @return l'ajustement de la position X de la fenêtre ajax.
   */
  public String getAjustementPositionX() {
    return ajustementPositionX;
  }

  /**
   * Fixer l'ajustement de la position Y de la fenêtre ajax.
   * 
   * @param ajustementPositionY
   *          l'ajustement de la position Y de la fenêtre ajax.
   */
  public void setAjustementPositionY(String ajustementPositionY) {
    this.ajustementPositionY = ajustementPositionY;
  }

  /**
   * Retourne l'ajustement de la position Y.
   * 
   * @return l'ajustement de la position Y.
   */
  public String getAjustementPositionY() {
    return ajustementPositionY;
  }

  /**
   * Fixer le nom de la variable qui va loger le traitement javascript pour appeler une liste de valeur.
   * 
   * @param var
   *          le nom de variable qui va loger le traitement javascript pour appeler une liste de valeur.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom de la variable qui va loger le traitement javascript pour appeler une liste de valeur.
   * 
   * @return
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer l'identifiant de la liste de valeurs
   * 
   * @param identifiant
   *          l'identifiant de la liste de valeurs
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Retourne l'identifiant de la liste de valeurs
   * 
   * @return l'identifiant de la liste de valeurs
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer true si vous désirez activer le filtre sur l'évènement onchange des champs de la liste de valeurs.
   * 
   * @param activerFiltreSurEvenementOnchange
   *          true si vous désirez activer le filtre sur l'évènement onchange des champs de la liste de valeurs.
   */
  public void setActiverFiltreSurEvenementOnchange(boolean activerFiltreSurEvenementOnchange) {
    this.activerFiltreSurEvenementOnchange = activerFiltreSurEvenementOnchange;
  }

  /**
   * Retourne true si vous désirez activer le filtre sur l'évènement onchange des champs de la liste de valeurs.
   * 
   * @return true si vous désirez activer le filtre sur l'évènement onchange des champs de la liste de valeurs.
   */
  public boolean isActiverFiltreSurEvenementOnchange() {
    return activerFiltreSurEvenementOnchange;
  }

  /**
   * Fixer true si vous désirez activer le filtre sur l'évènement onkeyup des champs de la liste de valeurs.
   * 
   * @param activerFiltreSurEvenementOnkeyup
   *          true si vous désirez activer le filtre sur l'évènement onkeyup des champs de la liste de valeurs.
   */
  public void setActiverFiltreSurEvenementOnkeyup(boolean activerFiltreSurEvenementOnkeyup) {
    this.activerFiltreSurEvenementOnkeyup = activerFiltreSurEvenementOnkeyup;
  }

  /**
   * Retourne true si vous désirez activer le filtre sur l'évènement onkeyup des champs de la liste de valeurs.
   * 
   * @return true si vous désirez activer le filtre sur l'évènement onkeyup des champs de la liste de valeurs.
   */
  public boolean isActiverFiltreSurEvenementOnkeyup() {
    return activerFiltreSurEvenementOnkeyup;
  }

  /**
   * Retourne le tableau de correspondance entre les identifiants des paramètre et les propriétés.
   * 
   * @return le tableau de correspondance entre les identifiants des paramètre et les propriétés.
   * @param contexte
   *          le contexte de la page.
   */
  public static HashMap getCorrespondanceIdProperty(PageContext contexte) {
    return (HashMap) contexte.getAttribute(ConstantesBaliseJSP.LISTE_VALEURS_CORRESPONDANCE_PROPERTY_ID);
  }

  /**
   * Fixer une fonctionnalité JavaScript a traiter s'il y a modification au formulaire.
   * 
   * @param ajaxJSModificationFormulaire
   *          la fonctionnalité JavaScript a tratier s'il y a modification au formulaire.
   */
  public void setAjaxJSModificationFormulaire(String ajaxJSModificationFormulaire) {
    this.ajaxJSModificationFormulaire = ajaxJSModificationFormulaire;
  }

  /**
   * Retourne une fonctionnalité JavaScript a traiter s'il y a modification au formulaire.
   * 
   * @return une fonctionnalité JavaScript a traiter s'il y a modification au formulaire.
   */
  public String getAjaxJSModificationFormulaire() {
    return ajaxJSModificationFormulaire;
  }

  /**
   * Fixer si on désire l'icone de la loupe de recherche.
   * 
   * @param iconeLoupe
   *          false si on désire ne pas afficher la loupe de recherche.
   */
  public void setIconeLoupe(Object iconeLoupe) {
    this.iconeLoupe = iconeLoupe;
  }

  /**
   * Retoure false si on désire ne pas afficher la loupe de recherche.
   * 
   * @return false si on désire ne pas afficher la loupe de recherche.
   */
  public Object getIconeLoupe() {
    return iconeLoupe;
  }

  /**
   * Est-ce que l'icone loupe doit être affichée.
   * 
   * @return
   */
  public boolean isAfficherIconeLoupe() {
    return (iconeLoupe != null) && getIconeLoupe().toString().toLowerCase().equals("true");
  }

  /**
   * Fixer la source de l'image utilisée pour la recheche.
   * 
   * @param srcImage
   *          Chemin de l'image
   */
  public void setSrcImage(String srcImage) {
    this.srcImage = srcImage;
  }

  /**
   * Obtenir la source de l'image utilisée pour la recheche.
   * 
   * @return Chemin de l'image
   */
  public String getSrcImage() {
    return srcImage;
  }

  /**
   * Fixer le titre de la fenêtre Ajax.
   * 
   * @param titreFenetreAjax
   *          le titre de la fenêtre Ajax.
   * @since SOFI 2.0.4
   */
  public void setTitreFenetreAjax(String titreFenetreAjax) {
    this.titreFenetreAjax = titreFenetreAjax;
  }

  /**
   * Retourne le titre de la fenêtre Ajax.
   * 
   * @return le titre de la fenêtre Ajax.
   * @since SOFI 2.0.4
   */
  public String getTitreFenetreAjax() {
    return titreFenetreAjax;
  }

  /**
   * Fixer true si on désire automatiquement lancer une nouvelle recherche au clic de l'icone de la loupe.
   * 
   * @param iconeLoupeNouvelleRecherche
   *          true si on désire automatiquement lancer une nouvelle recherche au clic de l'icone de la loupe.
   * @since SOFI 2.1
   */
  public void setIconeLoupeNouvelleRecherche(Object iconeLoupeNouvelleRecherche) {
    this.iconeLoupeNouvelleRecherche = iconeLoupeNouvelleRecherche;
  }

  /**
   * Retourne true si on désire automatiquement lancer une nouvelle recherche au clic de l'icone de la loupe.
   * 
   * @return true si on désire automatiquement lancer une nouvelle recherche au clic de l'icone de la loupe.
   * @since SOFI 2.1
   */
  public Object getIconeLoupeNouvelleRecherche() {
    return iconeLoupeNouvelleRecherche;
  }

  /**
   * Est-ce que l'icone loupe doit être affichée.
   * 
   * @return
   */
  public boolean isLancerRechercheSurIconeLoupe() {
    return (iconeLoupeNouvelleRecherche != null) && iconeLoupeNouvelleRecherche.toString().toLowerCase().equals("true");
  }

  /**
   * @param placeholder
   *          the placeholder to set
   */
  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  /**
   * @return the placeholder
   */
  public String getPlaceholder() {
    return placeholder;
  }

  /**
   * @return the paramIdDependant
   */
  public String getParamIdDependant() {
    return paramIdDependant;
  }

  /**
   * @param paramIdDependant
   *          the paramIdDependant to set
   */
  public void setParamIdDependant(String paramIdDependant) {
    this.paramIdDependant = paramIdDependant;
  }

  /**
   * @return the paramPropertyDependant
   */
  public String getParamPropertyDependant() {
    return paramPropertyDependant;
  }

  /**
   * @param paramPropertyDependant
   *          the paramPropertyDependant to set
   */
  public void setParamPropertyDependant(String paramPropertyDependant) {
    this.paramPropertyDependant = paramPropertyDependant;
  }
}
