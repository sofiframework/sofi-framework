/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * RadioTag est un champ de saisie de type radio.
 * <p>
 * Un seul choix possible parmis une liste de choix.
 * <p>
 * Il permet de notifier si l'utilisateur fait une modification sur ce
 * champ de saisie.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 1.8 Traitement Ajax disponible.
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class RadioTag extends org.apache.struts.taglib.html.RadioTag {
  /**
   * 
   */
  private static final long serialVersionUID = 7196306349169541344L;

  /** Spécifie la classe css pour un attribut en lecture seulement */
  private String styleClassLectureSeulement = null;

  /** Spécifie le libellé */
  private String libelle = null;
  private String disabledEL = null;
  private String readonlyEL = null;

  /** Href lors d'un traitement ajax **/
  private String href = null;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  private String nomParametreValeur = null;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax.
   */
  private String divRetourAjax = null;

  /**
   * L'identifiant de la page dont l'on désire modifier la valeur dans
   * un traitement ajax.
   */
  private String idRetourAjax = null;

  /**
   * Spécifie qu'il va avoir plusieurs identifiant de retour dans la
   * réponse avec l'aide d'un document XML
   */
  private boolean idRetourMultipleAjax = false;

  /**
   * Traitement JavaScript a executé pendant le chargement ajax
   */
  private String ajaxJSPendantChargement;

  /**
   * Traitement JavaScript a executé après le chargement.
   */
  private String ajaxJSApresChargement;

  /**
   * Traitement JavaScript Prérequis au chargement ajax.
   */
  private String ajaxJSPreRequis;

  /**
   * Spécifier un début d'un groupe de radio bouton.
   */
  private String groupeDebut;

  /**
   * Spécifie une fin d'un groupe de radio bouton.
   */
  private String groupeFin;

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * @param libelle le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @param styleClassLectureSeulement le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Générer le html pour l'élement &lt;input type="radio"&gt;.
   * <p>
   * @param serverValue La valeur du serveur utilisé pour l'attribut de la balise <code>value</code> et envoyé au serveur lors de la soumission du formulaire.
   * @param checkedValue Si la valeur du serveur est égale à la valeur de l'élément le radio bouton est sélectionné.
   * @return L'élément bouton radio.
   * @throws JspException si une exception JSP est traité
   */
  @Override
  protected String renderRadioElement(String serverValue,
      String checkedValue) throws JspException {
    String codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      Utilisateur utilisateur =
          UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {

        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setDisabled(true);
        } else {
          BaseForm baseForm =
              UtilitaireBaliseJSP.getBaseForm(this.pageContext);
          boolean blocSecurite =
              (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) !=
              null) ||
              ((baseForm != null) && baseForm.isModeLectureSeulement());
          boolean isLectureSeulement =
              UtilitaireString.isVide(getDisabledEL()) &&
              UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
          if (isLectureSeulement) {
            this.setDisabled(true);
          }
        }

        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant(serverValue, checkedValue);
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant(serverValue, checkedValue);
    }

    if (codeHtml == null) {
      codeHtml = "";
    }

    return codeHtml;
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant(String serverValue,
      String checkedValue) throws JspException {

    // Accès au formulaire.
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(this.pageContext);

    if (formulaire.isModeAffichageSeulement()) {
      setDisabled(true);
    } else {
      if (!getDisabled() && !getReadonly()) {
        // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
        formulaire.ajouterAttributATraiter(getProperty());
      }
    }

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    String nomFormulaireJS =
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            property);

    MessageErreur erreur =
        UtilitaireBaliseJSP.getMessageErreur(formulaire, this.property,
            this.pageContext);
    Boolean estPositioneeHautChamp = Boolean.FALSE;
    String titre = null;

    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();

      // Permet de verifier si le message d'erreur de saisie
      // doit être positionné en bas du champ. False est la valeur par défaut et signifie que
      // les messages apparaîtront en nicetitle au survol du champ.
      estPositioneeHautChamp =
          GestionParametreSysteme.getInstance().getBoolean(Constantes.POSITION_MESSAGE_ERREUR_SAISIE);

      if (estPositioneeHautChamp == null) {
        estPositioneeHautChamp = Boolean.FALSE;
      }
    }

    Libelle libelle = null;
    this.setStyleClass(getStyleClass());

    if (erreur == null) {
      libelle =
          UtilitaireLibelle.getLibelle(this.getTitle(), pageContext, null);
      titre = libelle.getMessage();
    }

    StringBuffer resultat = new StringBuffer();

    String titreNonEscapeHtml = titre;

    // Pour que le titre du champ de saisie soit convertie en texte du gestion message
    if ((erreur != null) && (estPositioneeHautChamp.booleanValue() == false)) {
      titre = UtilitaireString.convertirEnHtml(titre);

      if ((getGroupeDebut() != null) && getGroupeDebut().equals("true")) {
        resultat.append(UtilitaireBaliseJSP.genererChampInvisiblePourFocus(getProperty(),
            titre));
        resultat.append("<div class=\"cadreErreur\">");
      }

      setTitle(titre);
    }

    if (pageContext.getRequest().getAttribute(GroupeRadioCheckboxTag.NOMBRE_COLONNES_GROUPE_COURANT) !=
        null) {
      if (GroupeRadioCheckboxTag.verifierOuvrirLigne(pageContext)) {
        resultat.append("<TR>");
      }

      resultat.append("<TD>");
    }

    resultat.append("<input type=\"radio\"");
    resultat.append(" name=\"");

    // @since Struts 1.1
    if (indexed) {
      prepareIndex(resultat, name);
    }

    resultat.append(this.property);
    resultat.append("\"");

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    // Partir un compteur pour le nombre de boite à cocher du même nom
    String attributCompteurBoite = "ind" + getProperty();
    Integer no = (Integer)pageContext.getAttribute(attributCompteurBoite);

    if (no == null) {
      no = new Integer(1);
      pageContext.setAttribute(attributCompteurBoite, no);
    } else {
      no = new Integer((no.intValue() + 1));
      pageContext.setAttribute(attributCompteurBoite, no);
    }

    resultat =
        UtilitaireBaliseJSP.specifierGestionModification(pageContext, nomFormulaire,
            nomFormulaireJS,
            getOnchange(),
            resultat);

    // Faire la gestion du mode inactif
    if (formulaire.isModeLectureSeulement() || this.getDisabled() ||
        this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.styleClassLectureSeulement,
          getStyleClass(), resultat);
    }

    resultat.append(" value=\"");
    resultat.append(getValue());
    resultat.append("\"");

    if (serverValue.trim().equalsIgnoreCase(checkedValue)) {
      resultat.append(" checked=\"checked\"");
    }

    this.setStyleId(getProperty() + no);

    String onClickCourant = getOnclick();

    // Si Traitement Ajax.
    if (getHref() != null) {
      setOnclick(null);

      // Fixer le nom de propriété comme nom de paramètre pour extraire la valeur.
      if (getNomParametreValeur() == null) {
        setNomParametreValeur(getProperty());
      }

      if (isIdRetourMultipleAjax()) {
        setDivRetourAjax("REPONSE_MULTIPLE");
      }

      if (getIdRetourAjax() != null) {
        setDivRetourAjax(getIdRetourAjax());
      }

      String appelAjax =
          UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(getHref(),
              getDivRetourAjax(),
              getNomParametreValeur(),
              getStyleId(),
              getAjaxJSPendantChargement(),
              getAjaxJSApresChargement(),
              pageContext);

      if (onClickCourant != null) {
        setOnclick(onClickCourant + appelAjax);
      } else {
        setOnclick(appelAjax);
      }
    }

    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(getElementClose());

    if (erreur != null) {
      setTitle(titreNonEscapeHtml);
    }

    resultat.append("&nbsp;");

    // Générer le bout de code qui affiche le libellé
    UtilitaireBaliseJSP.genererLibelle(this.property, no, this.getTitle(),
        this.libelle, this.pageContext,
        resultat);

    if (pageContext.getRequest().getAttribute(GroupeRadioCheckboxTag.NOMBRE_COLONNES_GROUPE_COURANT) !=
        null) {
      resultat.append("</td>");

      if (GroupeRadioCheckboxTag.verifierFermerLigne(pageContext)) {
        resultat.append("</tr>");
      }
    }

    if (erreur != null) {
      // Si true, on place le message d'erreur en bas de la balise
      if (estPositioneeHautChamp.booleanValue()) {
        resultat.append("<span class=\"libelleErreur\">" + titre + "</span>");
      }

      if ((getGroupeFin() != null) && getGroupeFin().equals("true")) {
        resultat.append("</div>");
      }
    }

    return resultat.toString();
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      if (getValue() != null) {
        String valeur =
            EvaluateurExpression.evaluerString("value", getValue(), this,
                pageContext);
        setValue(valeur);
      }

      if (getLibelle() != null) {
        String libelle =
            EvaluateurExpression.evaluerString("libelle", String.valueOf(getLibelle()),
                this, pageContext);
        setLibelle(libelle);
      }

      if (getProperty() != null) {
        String property =
            EvaluateurExpression.evaluerString("property", String.valueOf(getProperty()),
                this, pageContext);
        setProperty(property);
      }

      if (getDisabledEL() != null) {
        boolean disabled =
            EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
                this, pageContext);
        setDisabled(disabled);
      }

      if (getReadonlyEL() != null) {
        boolean readonly =
            EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
                this, pageContext);
        setReadonly(readonly);
      }

      String valeurELHref =
          EvaluateurExpression.evaluerString("href", getHref(), this,
              pageContext);
      setHref(valeurELHref);

      if (getGroupeDebut() != null) {
        String groupeDebut =
            EvaluateurExpression.evaluerString("groupeDebut", getGroupeDebut(),
                this, pageContext);
        setGroupeDebut(groupeDebut.toLowerCase());
      }

      if (getGroupeFin() != null) {
        String groupeFin =
            EvaluateurExpression.evaluerString("groupeFin", getGroupeFin(), this,
                pageContext);
        setGroupeFin(groupeFin.toLowerCase());
      }

      if (getOnclick() != null) {
        String onclick =
            EvaluateurExpression.evaluerString("onclick", String.valueOf(getOnclick()),
                this, pageContext);
        setOnclick(onclick);
      }

      if (getDivRetourAjax() != null) {
        String divRetourAjax =
            EvaluateurExpression.evaluerString("divRetourAjax",
                String.valueOf(getDivRetourAjax()),
                this, pageContext);
        setDivRetourAjax(divRetourAjax);
      }

      if (getAjaxJSApresChargement() != null) {
        String ajaxJSApresChargement =
            EvaluateurExpression.evaluerString("ajaxJSApresChargement",
                String.valueOf(getAjaxJSApresChargement()),
                this, pageContext);
        setAjaxJSApresChargement(ajaxJSApresChargement);
      }
    } catch (JspException e) {
      throw e;
    }
  }

  @Override
  public int doStartTag() throws JspException {

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }
    // Gestion des propriétés et valeurs selon le groupe multibox si présent
    String libelleGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_ETIQUETTE);
    String valeurGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_VALEUR);
    String proprieteGroupe =
        (String)pageContext.findAttribute(GroupeRadioCheckboxTag.ATTRIBUT_PROPRIETE);

    if ((libelleGroupe != null) && (valeurGroupe != null)) {
      this.setLibelle(libelleGroupe);
      this.setValue(valeurGroupe);
      this.setProperty(proprieteGroupe);
    }

    String radioTag = renderRadioElement(serverValue(), currentValue());

    TagUtils.getInstance().write(pageContext, radioTag);

    this.text = null;

    return (EVAL_BODY_BUFFERED);
  }

  @Override
  public int doEndTag() throws JspException {
    // Non conforme au W3C. La balise INPUT ne peut pas se fermer par </input>.
    // La balise est fermé correctement dans la fonction genererCodeHtmlComposant().
    // TagUtils.getInstance().write(this.pageContext, "</input>");

    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }
    release();
    return (EVAL_PAGE);
  }

  private String serverValue() throws JspException {
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(this.pageContext);
    Object serverValue =
        UtilitaireObjet.getValeurAttribut(baseForm, this.property);
    return (serverValue == null) ? "" : serverValue.toString();
  }

  private String currentValue() throws JspException {
    return (this.value == null) ? "" : this.value;
  }

  /**
   * Méthode qui sert à libérer les ressources sytème utilisées
   */
  @Override
  public void release() {
    super.release();
    setStyleClass(null);
    setOnkeyup(null);
    setOnkeydown(null);
    setNomParametreValeur(null);
    setAjaxJSApresChargement(null);
    setAjaxJSPendantChargement(null);
    setAjaxJSPreRequis(null);
    setDivRetourAjax(null);
    setIdRetourMultipleAjax(false);
    setIdRetourAjax(null);
    setLibelle(null);
    setNomParametreValeur(null);
    setDisabledEL(null);
    setDivRetourAjax(null);
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  /**
   * Fixer le href ajax a executé
   * @param href le href ajax a executé
   */
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Retourne le href ajax a executé.
   * @return le href ajax a executé.
   */
  public String getHref() {
    return href;
  }

  /**
   * Fixer le nom de parametre qui correspond à la valeur selectionné.
   * @param nomParametreValeur le nom de parametre qui correspond à la valeur selectionné.
   */
  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  /**
   * Retourne le nom de parametre qui correspond à la valeur selectionné.
   * @return le nom de parametre qui correspond à la valeur selectionné.
   */
  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  /**
   * Fixer le div que l'on doit utiliser pour référer la réponse ajax.
   * @param divRetourAjax le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public void setDivRetourAjax(String divRetourAjax) {
    this.divRetourAjax = divRetourAjax;
  }

  /**
   * Retourne le div que l'on doit utiliser pour référer la réponse ajax.
   * @return le div que l'on doit utiliser pour référer la réponse ajax.
   */
  public String getDivRetourAjax() {
    return divRetourAjax;
  }

  /**
   * Fixer le traitement JavaScript a executé pendant le chargement ajax
   * @param ajaxJSPendantChargement le traitement JavaScript a executé pendant le chargement ajax
   */
  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé pendant le chargement ajax
   * @return le traitement JavaScript a executé pendant le chargement ajax
   */
  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  /**
   * Fixer le traitement JavaScript a executé après le chargement.
   * @param ajaxJSApresChargement le traitement JavaScript a executé après le chargement.
   */
  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  /**
   * Retourne le traitement JavaScript a executé après le chargement.
   * @return le traitement JavaScript a executé après le chargement.
   */
  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  /**
   * Fixer le traitement JavaScript Prérequis au chargement ajax.
   * @param ajaxJSPreRequis le traitement JavaScript Prérequis au chargement ajax.
   */
  public void setAjaxJSPreRequis(String ajaxJSPreRequis) {
    this.ajaxJSPreRequis = ajaxJSPreRequis;
  }

  /**
   * Retourne le traitement JavaScript Prérequis au chargement ajax.
   * @return le traitement JavaScript Prérequis au chargement ajax.
   */
  public String getAjaxJSPreRequis() {
    return ajaxJSPreRequis;
  }

  public void setIdRetourAjax(String idRetourAjax) {
    this.idRetourAjax = idRetourAjax;
  }

  public String getIdRetourAjax() {
    return idRetourAjax;
  }

  public void setIdRetourMultipleAjax(boolean idRetourMultipleAjax) {
    this.idRetourMultipleAjax = idRetourMultipleAjax;
  }

  public boolean isIdRetourMultipleAjax() {
    return idRetourMultipleAjax;
  }

  public void setGroupeDebut(String groupeDebut) {
    this.groupeDebut = groupeDebut;
  }

  public String getGroupeDebut() {
    return groupeDebut;
  }

  public void setGroupeFin(String groupeFin) {
    this.groupeFin = groupeFin;
  }

  public String getGroupeFin() {
    return groupeFin;
  }
}
