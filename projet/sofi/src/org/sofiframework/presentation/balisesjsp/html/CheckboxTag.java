/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * CheckboxTag est un champ de saisie de type boite à coché.
 * <p>
 * Il permet de notifier si l'utilisateur fait une modification sur ce
 * champ de saisie.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 1.8 Traitement Ajax disponible.
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class CheckboxTag extends org.apache.struts.taglib.html.CheckboxTag {

  private static final long serialVersionUID = 4352743795057881594L;

  /** Variable qui identifie le style à utiliser pour les champs de saisie qui sont en erreur */
  public static final String STYLE_SAISIE_ERREUR = "saisieErreur";

  /** Variable qui identifie le style à utiliser pour les libellés des champs en erreur */
  public static final String STYLE_LIBELLE_ERREUR = "libelleErreur";

  /** Spécifie la classe css pour un attribut en lecture seulement */
  protected String styleClassLectureSeulement = null;

  /** Spécifie la classe css pour un attribut sélectionné */
  protected String styleClassFocus = null;

  /** Spécifie le libellé */
  protected String libelle = null;

  /** Spécifie si le libellé est associé avec la boite à cocher **/
  protected boolean libelleAssocie = false;

  /**
   * Spécifie si on désire fermer le ligne.
   * La fermeture de la balise tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  protected boolean fermerLigne = true;

  /**
   * Spécifie si on désire ouvrir une nouvelle ligne.
   * L'ouverture de la balise tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  protected boolean ouvrirLigne = true;

  /** Spécifie si le champ de saisie est obligatoire */
  protected boolean obligatoire = false;

  /** Spécifier un style sur le ligne HTML **/
  protected String ligneStyleClass = null;

  /** Spécifie si on désire générer un ligne HTML automatique, par défaut c'est OUI **/
  protected boolean genererLigne = true;
  protected String disabledEL = null;
  protected String readonlyEL = null;

  /** Href lors d'un traitement ajax **/
  protected String href = null;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  protected String nomParametreValeur = null;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax.
   */
  protected String divRetourAjax = null;

  /**
   * L'identifiant de la page dont l'on désire modifier la valeur dans
   * un traitement ajax.
   */
  protected String idRetourAjax = null;

  /**
   * Spécifie qu'il va avoir plusieurs identifiant de retour dans la
   * réponse avec l'aide d'un document XML
   */
  protected boolean idRetourMultipleAjax = false;

  /**
   * Traitement JavaScript a executé pendant le chargement ajax
   */
  protected String ajaxJSPendantChargement;

  /**
   * Traitement JavaScript a executé après le chargement.
   */
  protected String ajaxJSApresChargement;

  /**
   * Traitement JavaScript Prérequis au chargement ajax.
   */
  protected String ajaxJSPreRequis;

  /**
   * Obtenir le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @return le style à utiliser dans le cas où le champ possède le focus
   */
  public String getStyleClassFocus() {
    return (this.styleClassFocus);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @param styleClassFocus le style à utiliser dans le cas où le champ possède le focus
   */
  public void setStyleClassFocus(String styleClassFocus) {
    this.styleClassFocus = styleClassFocus;
  }

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * @param libelle le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @param styleClassLectureSeulement le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Obtenir le nom de la boite à cocher.
   * <p>
   * @return le nom de la boite à cocher
   */
  @Override
  public String getName() {
    return (this.name);
  }

  /**
   * Fixer le nom de la boite à cocher.
   * <p>
   * @param name le nom de la boite à cocher
   */
  @Override
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public boolean getFermerLigne() {
    return (this.fermerLigne);
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @param fermerLigne la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public void setFermerLigne(boolean fermerLigne) {
    this.fermerLigne = fermerLigne;
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @param ouvrirLigne la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public void setOuvrirLigne(boolean ouvrirLigne) {
    this.ouvrirLigne = ouvrirLigne;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public boolean getOuvrirLigne() {
    return ouvrirLigne;
  }

  /**
   * Est-ce que le libellé est associé avec la boite à cocher
   * @return true si le libellé est associé avec la boite à cocher
   */
  public boolean isLibelleAssocie() {
    return libelleAssocie;
  }

  /**
   * Fixer true si le libellé est associé avec la boite à cocher.
   * @param libelleAssocie true si le libellé est associé avec la boite à cocher
   */
  public void setLibelleAssocie(boolean libelleAssocie) {
    this.libelleAssocie = libelleAssocie;
  }

  /**
   * Début de la gestion de la balise JSP.
   * <p>
   * Cette méthode vérifie la sécurité du composant et gère la génération du
   * composant HTML en conséquence.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException si une exception JSP est traité
   */
  @Override
  public int doStartTag() throws JspException {
    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    String codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (UtilitaireBaliseJSP.isFormulaireCourantEnAffichageSeulement(pageContext,
        getProperty())) {
      this.setDisabled(true);
      this.setObligatoire(false);
    }

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {
        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setDisabled(true);
        } else {
          BaseForm baseForm =
              UtilitaireBaliseJSP.getBaseForm(this.pageContext);
          boolean blocSecurite =
              (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null)
              || ((baseForm != null) && baseForm.isModeLectureSeulement());
          boolean isLectureSeulement = UtilitaireString.isVide(getDisabledEL())
              && UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
          if (isLectureSeulement) {
            this.setDisabled(true);
          }
        }

        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant();
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    }

    return (EVAL_BODY_BUFFERED);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant() throws JspException {
    // Accès au formulaire
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    if (formulaire != null && formulaire.isModeAffichageSeulement()) {
      setDisabled(true);
    }

    if (formulaire != null && (!getDisabled() && !getReadonly())) {
      // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
      formulaire.ajouterAttributATraiter(getProperty());
    }

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    if (formulaire != null && getObligatoire()) {
      formulaire.ajouterAttributObligatoire(nomFormulaireImbrique,
          getProperty());
    }

    MessageErreur erreur =
        UtilitaireBaliseJSP.getMessageErreur(formulaire, this.property,
            this.pageContext);
    String titre = null;

    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();
    }

    String styleLibelle = null;

    this.setStyleClass("checkbox " + getStyleClass());
    this.setStyleClassFocus(getStyleClassFocus());

    if (erreur != null) {
      this.setStyleClass(STYLE_SAISIE_ERREUR);
      this.setStyleClassFocus(STYLE_SAISIE_ERREUR);
      styleLibelle = STYLE_LIBELLE_ERREUR;
    } else {
      Libelle objLibelle = null;
      objLibelle =
          UtilitaireLibelle.getLibelle(this.getTitle(), pageContext, null);
      titre = objLibelle.getMessage();
    }

    setTitle(titre);

    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    String nomFormulaireJS =
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            property);

    StringBuffer resultat = new StringBuffer("");

    //Libelle libelle = UtilitaireLibelle.getLibelle(this.getLibelle(), pageContext, null);
    // Générer le libéllé.
    if (libelle != null) {
      if (isGenererLigne()) {
        if (getOuvrirLigne()) {
          resultat.append("<tr");

          if (getLigneStyleClass() != null) {
            resultat.append(" class=\"");
            resultat.append(getLigneStyleClass());
            resultat.append("\" ");
          }

          resultat.append(">");
        }

        // Ajouter le bout de code qui affiche le libellé
        resultat.append("<th scope=\"row\">");

        if (!isLibelleAssocie()) {
          UtilitaireBaliseJSP.genererLibelle(this.property, titre,
              this.libelle, styleLibelle, getObligatoire(), false,
              this.pageContext, resultat);
        }

        resultat.append("</th><td>");
      } else {
        if (!isLibelleAssocie()) {
          UtilitaireBaliseJSP.genererLibelle(this.property, titre,
              this.libelle, styleLibelle, getObligatoire(), false,
              this.pageContext, resultat);
        }
      }
    }

    Object result = null;

    try {
      result =
          TagUtils.getInstance().lookup(pageContext, name, property, null);
    } catch (Exception e) {
      if (formulaire != null) {
        result = UtilitaireObjet.getValeurAttribut(formulaire, property);
      }
    }

    if (result == null) {
      result = "";
    }

    //    if (!(result instanceof String)) {
    //      result = result.toString();
    //    }

    //String checked = (String) result;

    //    if (!formulaire.isModeAffichageSeulement()) {
    resultat.append("<input type=\"checkbox\"");
    resultat.append(" name=\"");

    // * @since Struts 1.1
    if (indexed) {
      prepareIndex(resultat, name);
    }

    resultat.append(this.property);
    resultat.append("\"");

    if (accesskey != null) {
      resultat.append(" accesskey=\"");
      resultat.append(accesskey);
      resultat.append("\"");
    }

    if (tabindex != null) {
      resultat.append(" tabindex=\"");
      resultat.append(tabindex);
      resultat.append("\"");
    }

    resultat =
        UtilitaireBaliseJSP.specifierGestionModification(pageContext, nomFormulaire,
            nomFormulaireJS, getOnchange(), resultat);

    // Faire la gestion du disabled
    if (formulaire != null &&  (formulaire.isModeLectureSeulement() || this.getDisabled() ||
        this.getReadonly())) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.getStyleClassLectureSeulement(),
          getStyleClass(), resultat);
    }

    resultat.append(" value=\"");

    if (value == null) {
      resultat.append("on");
    } else {
      resultat.append(value);
    }

    resultat.append("\"");

    if (result != null) {
      boolean checked = false;
      if (result instanceof String[]) {
        String[] results = (String[]) result;
        for (int i = 0; i < results.length && !checked; i++) {
          checked = this.isSelectionne(results[i]);
        }
      } else if (result instanceof String) {
        checked = this.isSelectionne((String) result);
      } else {
        checked = this.isSelectionne(result.toString());
      }

      if (checked) {
        resultat.append(" checked=\"checked\"");
      }
    }

    if (StringUtils.isEmpty(this.getStyleId())) {
      setStyleId(getProperty());
    }

    String onClickCourant = getOnclick();

    // Si Traitement Ajax.
    if (getHref() != null) {
      setOnclick(null);

      // Fixer le nom de propriété comme nom de paramètre pour extraire la valeur.
      if (getNomParametreValeur() == null) {
        setNomParametreValeur(getProperty());
      }

      if (isIdRetourMultipleAjax()) {
        setDivRetourAjax("REPONSE_MULTIPLE");
      }

      if (getIdRetourAjax() != null) {
        setDivRetourAjax(getIdRetourAjax());
      }

      String appelAjax =
          UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(getHref(),
              getDivRetourAjax(), getNomParametreValeur(), getStyleId(),
              getAjaxJSPendantChargement(), getAjaxJSApresChargement(),
              pageContext);

      if (onClickCourant != null) {
        setOnclick(onClickCourant + appelAjax);
      } else {
        setOnclick(appelAjax);
      }
    }

    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(getElementClose());

    //    } else {
    //      if (isSelectionne(checked)) {
    //        resultat.append(
    //          "<span class=\"checkbox_affichage_seulement_oui\">&nbsp;&nbsp;&nbsp;</span>");
    //      } else {
    //        resultat.append(
    //          "<span class=\"checkbox_affichage_seulement_non\">&nbsp;&nbsp;&nbsp;</span>");
    //      }
    //    }
    if ((getLibelle() != null) && isLibelleAssocie()) {
      resultat.append("&nbsp;");
      UtilitaireBaliseJSP.genererLibelle(this.property, null, this.libelle,
          null, false, false, this.pageContext, resultat);
    }

    return resultat.toString();
  }

  private boolean isSelectionne(String checked) {
    if (checked.equalsIgnoreCase(value) || checked.equalsIgnoreCase("true") ||
        checked.equalsIgnoreCase("yes") || checked.equalsIgnoreCase("on")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    StringBuffer resultat = new StringBuffer("");

    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      resultat.append(valeur);
    }

    // Fermeture de la colonne si libellé de spécifier.
    if ((getLibelle() != null) && isGenererLigne()) {
      resultat.append("</td>");

      if (getFermerLigne()) {
        resultat.append("</tr>");
      }
    }

    TagUtils.getInstance().write(pageContext, resultat.toString());

    this.release();

    return (SKIP_BODY);
  }

  @Override
  public void release() {
    super.release();
    this.accesskey = null;
    this.ajaxJSApresChargement = null;
    this.ajaxJSPendantChargement = null;
    this.ajaxJSPreRequis = null;
    this.disabledEL = null;
    this.divRetourAjax = null;
    this.fermerLigne = true;
    this.genererLigne = true;
    this.href = null;
    this.id = null;
    this.idRetourAjax = null;
    this.idRetourMultipleAjax = false;
    this.libelle = null;
    this.libelleAssocie = false;
    this.ligneStyleClass = null;
    this.obligatoire = false;
    this.ouvrirLigne = true;
    this.readonlyEL = null;
    this.styleClassFocus = null;
    this.styleClassLectureSeulement = null;
  }

  /**
   * Obtenir la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @return la valeur qui indique si le champ est obligatoire ou pas
   */
  public boolean getObligatoire() {
    return (this.obligatoire);
  }

  /**
   * Fixer la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @param obligatoire la valeur qui indique si le champ est obligatoire ou pas
   */
  public void setObligatoire(boolean obligatoire) {
    this.obligatoire = obligatoire;
  }

  public void setLigneStyleClass(String ligneStyleClass) {
    this.ligneStyleClass = ligneStyleClass;
  }

  public String getLigneStyleClass() {
    return ligneStyleClass;
  }

  public void setGenererLigne(boolean genererLigne) {
    this.genererLigne = genererLigne;
  }

  public boolean isGenererLigne() {
    return genererLigne;
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      if (getDisabledEL() != null) {
        boolean disabled =
            EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
                this, pageContext);
        setDisabled(disabled);
      }

      if (getReadonlyEL() != null) {
        boolean readonly =
            EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
                this, pageContext);
        setReadonly(readonly);
      }

      if (getOnclick() != null) {
        String onclick =
            EvaluateurExpression.evaluerString("onclick", String.valueOf(getOnclick()),
                this, pageContext);
        setOnclick(onclick);
      }

      if (getLibelle() != null) {
        String libelle =
            EvaluateurExpression.evaluerString("libelle", String.valueOf(getLibelle()),
                this, pageContext);
        setLibelle(libelle);
      }

      if (getProperty() != null) {
        String property =
            EvaluateurExpression.evaluerString("property", String.valueOf(getProperty()),
                this, pageContext);
        setProperty(property);
      }
    } catch (JspException e) {
      throw e;
    }
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getHref() {
    return href;
  }

  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  public void setDivRetourAjax(String divRetourAjax) {
    this.divRetourAjax = divRetourAjax;
  }

  public String getDivRetourAjax() {
    return divRetourAjax;
  }

  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  public void setAjaxJSPreRequis(String ajaxJSPreRequis) {
    this.ajaxJSPreRequis = ajaxJSPreRequis;
  }

  public String getAjaxJSPreRequis() {
    return ajaxJSPreRequis;
  }

  public void setIdRetourAjax(String idRetourAjax) {
    this.idRetourAjax = idRetourAjax;
  }

  public String getIdRetourAjax() {
    return idRetourAjax;
  }

  public void setIdRetourMultipleAjax(boolean idRetourMultipleAjax) {
    this.idRetourMultipleAjax = idRetourMultipleAjax;
  }

  public boolean isIdRetourMultipleAjax() {
    return idRetourMultipleAjax;
  }
}
