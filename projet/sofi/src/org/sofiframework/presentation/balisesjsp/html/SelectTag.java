/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.BaseHandlerTag;
import org.apache.struts.taglib.html.Constants;
import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.balisesjsp.base.BaliseValeurCache;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.base.TableTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * SelectTag permet d'afficher un champ de saisie dans une liste en plus de
 * traiter automatiquement si l'utilisateur fait une modification sur ce champ
 * texte.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * Permet de générer une liste depuis des valeurs résidant dans le singleton
 * GestionCache.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 1.6 Traitement Ajax disponible.
 * @see org.sofiframework.application.cache.GestionCache
 */
public class SelectTag extends BaseHandlerTag implements BaliseValeurCache {
  /**
   * 
   */
  private static final long serialVersionUID = -4234788307948781645L;

  protected static Log log = LogFactory.getLog(SelectTag.class);

  /**
   * Variable qui identifie le style à utiliser pour les champs de saisie qui
   * sont en erreur
   */
  public static final String STYLE_SAISIE_ERREUR = "saisieErreur";

  /**
   * Variable qui identifie le style à utiliser pour les libellés des champs en
   * erreur
   */
  public static final String STYLE_LIBELLE_ERREUR = "libelleErreur";

  /** Spécifie la classe css pour un attribut en lecture seulement */
  private String styleClassLectureSeulement = null;

  /** Spécifie la classe css pour un attribut sélectionné */
  private String styleClassFocus = null;

  /** Spécifie le libellé */
  private String libelle = null;

  /** Spécifie si le champ de saisie est obligatoire */
  private boolean obligatoire = false;

  /** Spécifie le nom de la collection d'objet de transfert */
  private Object collection = null;

  /**
   * Spécifie le nom le propriété correspondant à l'étiquette d'un contenu de la
   * liste déroulante
   */
  private String proprieteEtiquette = null;

  /**
   * Spécifie le nom de propritété de la valeur d'un contenu de la liste
   * déroulante
   */
  private String proprieteValeur = null;

  /**
   * Spécifie le nom de propritété de l'info-bulle d'un contenu de la liste
   * déroulante
   */
  private String proprieteTitle = null;

  /** Spécifie le nom de la cache à utiliser pour cette liste déroulante */
  private String cache = null;

  /**
   * @since SOFI 2.0.3
   */
  private Object sousCache = null;

  /**
   * @since SOFI 2.0.3
   */
  private String filtre = null;

  /**
   * Activer la sélection d'une valeur par défaut de l'objet cache.
   * 
   * @since SOFI 2.0.3
   */
  private Object activerCleDefaut = null;

  /**
   * Spécifie si on désire fermer le ligne. La fermeture de la balise tr est
   * ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  private boolean fermerLigne = true;

  /**
   * Spécifie si on désire ouvrir une nouvelle ligne. L'ouverture de la balise
   * tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  private boolean ouvrirLigne = true;

  /** Spécifier si on désire une ligne vide comme première option */
  private Object ligneVide = null;

  /** Spécifier la valeur de la ligne vide */
  private String ligneVideValeur = null;

  /** Spécifier le libellé de la ligne vide */
  private String ligneVideLibelle = null;

  /**
   * Permet de présenter le libellé de ligne vide si la valeur du SelectTag en
   * affichage seulement est la valeur null ou vide.
   * 
   * @since SOFI 2.0.2
   */
  private String ligneVideLibelleEnAffichageSeulement = null;

  /** Le nom attribut dont le libellé sera associé **/
  private String nomAttribut;

  /**
   * L'adresse a appelé lors d'un changement de valeur du select
   */
  private String href;

  /**
   * Le style a appliqué sur la ligne comprenant le select.
   */
  private String ligneStyleClass = null;

  /**
   * Permet d'inactiver un champ de saisie avec l'aide des expressions EL.
   */
  private String disabledEL = null;

  /**
   * Permet de mettre en lecture seulement avec l'aide des expressions EL.
   */
  private String readonlyEL = null;

  /**
   * Spécifie si on désire générer un ligne HTML automatique, par défaut c'est
   * OUI
   **/
  private boolean genererLigne = true;

  /**
   * Le nom de la propriété du formulaire s'il y a lieu.
   */
  private String property = null;

  /**
   * La constante logant le nom du formulaire en traitement.
   */
  private String name = Constants.BEAN_KEY;

  /**
   * Indicateur qui spécifie si on peut sélectionner plusieurs élément dans le
   * select.
   */
  private String multiple = null;

  /**
   * Combien d'élément doit être affiché à l'utilisateur.
   */
  private String size = null;

  /**
   * La valeur par défaut.
   */
  private String value = null;

  /**
   * Appliquer la liste deroulante en ajax.
   */
  private boolean ajax = false;

  /**
   * Le div que l'on doit utiliser pour référer la réponse ajax.
   */
  private String divRetourAjax = null;

  /**
   * L'identifiant de la page dont l'on désire modifier la valeur dans un
   * traitement ajax.
   */
  private String idRetourAjax = null;

  /**
   * Spécifie qu'il va avoir plusieurs identifiant de retour dans la réponse
   * avec l'aide d'un document XML
   */
  private boolean idRetourMultipleAjax = false;

  /**
   * Le nom de parametre qui correspond à la valeur selectionné.
   */
  private String nomParametreValeur = null;

  /**
   * Le nom d'attribut de destination du retour du traitement ajax.
   */
  private String nomAttributDestinationAjax = null;

  /**
   * Traitement JavaScript a executé pendant le chargement ajax
   */
  private String ajaxJSPendantChargement;

  /**
   * Traitement JavaScript a executé après le chargement.
   */
  private String ajaxJSApresChargement;

  /** Afficher seulemenet la valeur du formulaire et non le champ de saisie **/
  private Object affichageSeulement = null;

  /** Affichage de l'indicateur obligatoire du champ de saisie. **/
  private Object affichageIndObligatoire = null;

  /**
   * Spécifie si le formulaire doit traiter les validations et populer l'objet
   * de transfert.
   * <p>
   * Défaut : false;
   */
  private Object ignorerValidation = null;

  /**
   * Spécifie si le select est dans contexte transactionnel tel que
   * l'application de notification de modification de formulaire.
   * 
   * @since SOFI 2.0.1
   */
  private String transactionnel = "true";

  /**
   * Nom de la variable dont sera logé le traitement de modification d'un
   * élément de la liste déroulante. Permet de réutiliser un évènement ajax par
   * exemple pour remplir une autre liste déroulante.
   * 
   * @since SOFI 2.0.1
   */
  private String var;

  /**
   * Nom de la classe de décoration qui doit être utilisé pour produire les
   * libellés et valeurs des options du select.
   */
  private String classeDecorator;

  /**
   * Objet de décoration instanciée pour produire les libellés et valeurs des
   * options du select. Cette instance est produite à partir du nom de classe
   * classeDecorator.
   * 
   * @since SOFI 2.0.3
   */
  private SelectDecorator decorator;

  /**
   * Est un select qui participe à un composant multi choix en tant que select
   * source.
   * 
   * @since SOFI 2.0.3
   */
  private Object selectMultiChoixSource = null;

  /**
   * Est un select qui participe à un composant multi choix en tant que select
   * destination.
   * 
   * @since SOFI 2.0.3
   */
  private Object selectMultiChoixDestination = null;

  /**
   * Ajout d'un icone aide avec aide contextuelle.
   * 
   * @since SOFI 2.0.3
   */
  private String iconeAide = null;

  /**
   * Spécifie si vous désirez que l'icone d'aide soit position à gauche du
   * libellé.
   * 
   * @since SOFI 2.0.3
   */
  private String iconeAideAGauche = null;

  /**
   * Determine s'il faut afficher tous les éléments de la cache ou seulement les
   * actifs.
   */
  private boolean actifsSeulement = true;

  private String codeClient;

  /**
   * Spécifie quel propriété doit être valider pour définir un optgroup.
   */
  private String proprieteOptGroup = null;
  
  /**
   * Spécifie si la description locale est utilisée ou non.
   * 
   * @since SOFI 3.2
   */
  private Object utiliserDescriptionLocale = null;

  /**
   * Obtenir le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * 
   * @return le style à utiliser dans le cas où le champ possède le focus
   */
  public String getStyleClassFocus() {
    return (this.styleClassFocus);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * 
   * @param styleClassFocus
   *          le style à utiliser dans le cas où le champ possède le focus
   */
  public void setStyleClassFocus(String styleClassFocus) {
    this.styleClassFocus = styleClassFocus;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * 
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * 
   * @param styleClassLectureSeulement
   *          le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une fermeture de balise tr
   * après le champ.
   * <p>
   * 
   * @return la valeur qui indique si on doit ajouter une fermeture de balise tr
   *         après le champ
   */
  public boolean getFermerLigne() {
    return (this.fermerLigne);
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une fermeture de balise tr
   * après le champ.
   * <p>
   * 
   * @param fermerLigne
   *          la valeur qui indique si on doit ajouter une fermeture de balise
   *          tr après le champ
   */
  public void setFermerLigne(boolean fermerLigne) {
    this.fermerLigne = fermerLigne;
  }

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * 
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * 
   * @param libelle
   *          le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * 
   * @return la valeur qui indique si le champ est obligatoire ou pas
   */
  public boolean getObligatoire() {
    return (this.obligatoire);
  }

  /**
   * Fixer la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * 
   * @param obligatoire
   *          la valeur qui indique si le champ est obligatoire ou pas
   */
  public void setObligatoire(boolean obligatoire) {
    this.obligatoire = obligatoire;
  }

  /**
   * Obtenir le nom de la collection d'objet de transfert.
   * <p>
   * 
   * @return le nom de la collection d'objet de transfert
   */
  public Object getCollection() {
    return collection;
  }

  /**
   * Fixer le nom de la collection d'objet de transfert.
   * <p>
   * 
   * @param collection
   *          le nom de la collection d'objet de transfert
   */
  public void setCollection(Object collection) {
    this.collection = collection;
  }

  /**
   * Obtenir le nom le propriété correspondant à l'étiquette d'un contenu de la
   * liste déroulante.
   * <p>
   * 
   * @return le nom le propriété correspondant à l'étiquette d'un contenu de la
   *         liste déroulante
   */
  public String getProprieteEtiquette() {
    return proprieteEtiquette;
  }

  /**
   * Fixer le nom le propriété correspondant à l'étiquette d'un contenu de la
   * liste déroulante.
   * <p>
   * 
   * @param proprieteEtiquette
   *          le nom le propriété correspondant à l'étiquette d'un contenu de la
   *          liste déroulante
   */
  public void setProprieteEtiquette(String proprieteEtiquette) {
    this.proprieteEtiquette = proprieteEtiquette;
  }

  /**
   * Obtenir le nom de propritété de la valeur d'un contenu de la liste
   * déroulante.
   * <p>
   * 
   * @return le nom de propritété de la valeur d'un contenu de la liste
   *         déroulante
   */
  public String getProprieteValeur() {
    return proprieteValeur;
  }

  /**
   * Fixer le nom de propritété de la valeur d'un contenu de la liste
   * déroulante.
   * <p>
   * 
   * @param proprieteValeur
   *          le nom de propritété de la valeur d'un contenu de la liste
   *          déroulante
   */
  public void setProprieteValeur(String proprieteValeur) {
    this.proprieteValeur = proprieteValeur;
  }

  /**
   * Obtenir le nom de la cache à utiliser pour cette liste déroulante.
   * <p>
   * 
   * @return le nom de la cache à utiliser pour cette liste déroulante
   */
  public String getCache() {
    return cache;
  }

  /**
   * Fixer le nom de la cache à utiliser pour cette liste déroulante.
   * <p>
   * 
   * @param cache
   *          le nom de la cache à utiliser pour cette liste déroulante
   */
  public void setCache(String cache) {
    this.cache = cache;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une ouverture de balise tr
   * avant le champ.
   * <p>
   * 
   * @return la valeur qui indique si on doit ajouter une ouverture de balise tr
   *         avant le champ
   */
  public boolean getOuvrirLigne() {
    return ouvrirLigne;
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une ouverture de balise tr
   * avant le champ.
   * <p>
   * 
   * @param ouvrirLigne
   *          la valeur qui indique si on doit ajouter une ouverture de balise
   *          tr avant le champ
   */
  public void setOuvrirLigne(boolean ouvrirLigne) {
    this.ouvrirLigne = ouvrirLigne;
  }

  public Object getLigneVide() {
    return ligneVide;
  }

  /**
   * Spécifier si on désire une ligne vide comme première option.
   * <p>
   * 
   * @return Spécifier si on désire une ligne vide comme première option.
   */
  public boolean isAfficherLigneVide() {
    if ((getLigneVide() != null)
        && getLigneVide().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer Spécifier si on désire une ligne vide comme première option..
   * <p>
   * 
   * @param ligneVide
   *          Spécifier si on désire une ligne vide comme première option.
   */
  public void setLigneVide(Object ligneVide) {
    this.ligneVide = ligneVide;
  }

  /**
   * Retourne la valeur de la ligne vide
   * <p>
   * 
   * @return la valeur de la ligne vide
   */
  public String getLigneVideValeur() {
    return ligneVideValeur;
  }

  /**
   * Fixer la valeur de la ligne vide
   * <p>
   * 
   * @param ligneVideValeur
   *          la valeur de la ligne vide.
   */
  public void setLigneVideValeur(String ligneVideValeur) {
    this.ligneVideValeur = ligneVideValeur;
  }

  /**
   * Retourne le libelle de la ligne vide
   * <p>
   * 
   * @return le libelle de la ligne vide
   */
  public String getLigneVideLibelle() {
    return ligneVideLibelle;
  }

  /**
   * Fixer le libelle de la ligne vide
   * <p>
   * 
   * @param ligneVideLibelle
   *          le libelle de la ligne vide.
   */
  public void setLigneVideLibelle(String ligneVideLibelle) {
    this.ligneVideLibelle = ligneVideLibelle;
  }

  /**
   * Retourne le nom d'attribut dont le libellé est associé
   * 
   * @return le nom d'attribut dont le libellé est associé
   */
  public String getNomAttribut() {
    return nomAttribut;
  }

  /**
   * Fixer le nom d'attribut dont le libellé est associé
   * 
   * @param nomAttribut
   *          le nom d'attribut dont le libellé est associé
   */
  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  /**
   * Permet d'application l'expression JSTL (EL) à la balise à l'attribut
   * collection.
   * <p>
   * 
   * @return Une liste d'objet à utiliser pour construire la balise select.
   */
  private ArrayList getListe() {
    ArrayList listeRetour = null;

    try {
      if (EvaluateurExpression.isEvaluerEL(pageContext)
          && (getCollection() != null)) {
        listeRetour = (ArrayList) ExpressionEvaluatorManager.evaluate(
            "collection", collection.toString(), ArrayList.class, this,
            this.pageContext);
      } else {
        listeRetour = (ArrayList) collection;
      }

      String index = UtilitaireBaliseNested.getIndexListeNested(pageContext);

      if ((index != null) && (listeRetour == null) && (collection != null)) {
        StringBuffer collectionImbrique = new StringBuffer(collection
            .toString().substring(0, collection.toString().length() - 1));
        collectionImbrique.append(index);
        collectionImbrique.append("}");
        listeRetour = (ArrayList) ExpressionEvaluatorManager.evaluate(
            "collection", collectionImbrique.toString(), ArrayList.class, this,
            this.pageContext);
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour obtenir la collection spécifiée " + collection);
      }
    }

    return (listeRetour != null) ? listeRetour : new ArrayList();
  }

  /**
   * Créer le début d'une balise SELECT
   * <p>
   * 
   * @return le code HTML qui crée la balise SELECT
   * @throws JspException
   *           Exception générique
   * @since Struts 1.1
   */
  protected String renderSelectStartElement() throws JspException {
    String codeHtml = null;

    try {
      // Vérifier la sécurité lié au bouton
      GestionSecurite gestionSecurite = GestionSecurite.getInstance();
      Utilisateur utilisateur = UtilitaireControleur
          .getUtilisateur(this.pageContext.getSession());

      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // L'utilisateur à droit de voir le composant
        if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
            this.libelle)) {
          // L'utilisateur a droit au composant en lecture seulement
          if (gestionSecurite
              .isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
                  this.libelle)) {
            this.setReadonly(true);
          } else {
            BaseForm baseForm = UtilitaireBaliseJSP
                .getBaseForm(this.pageContext);
            boolean blocSecurite = (this.pageContext.getRequest().getAttribute(
                BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null)
                || ((baseForm != null) && baseForm.isModeLectureSeulement());
            boolean isLectureSeulement = UtilitaireString
                .isVide(getDisabledEL())
                && UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
            if (isLectureSeulement) {
              this.setReadonly(true);
            }
          }

          // Générer le code représentant le composant HTML
          codeHtml = this.genererCodeHtmlComposant(null);
        } else {
          this.pageContext.setAttribute("CHAMP_SECURISE", Boolean.TRUE);
        }
      } else {
        // Si on a pas de composant de sécurité, alors on ne la traite pas et on
        // affiche le composant comme le développeur la programme
        codeHtml = this.genererCodeHtmlComposant(null);
      }

      if ((codeHtml == null) || (getVar() != null)) {
        codeHtml = "";
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour générer le début de la balise Select.", e);
      }
    }

    return codeHtml;
  }

  /**
   * Déterminer si le formulaire courant est en affichage seulement. On doit
   * vérifier si le formulaire parent
   * 
   * @returntrue, le formulaire doit etre affiché en affichage seulement. False,
   *              le formulaire sera éditable.
   */
  protected boolean isFormulaireEnAffichageSeulement() {
    return UtilitaireBaliseJSP.isFormulaireCourantEnAffichageSeulement(
        pageContext, getProperty());
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le
   * composant.
   * <p>
   * 
   * @param appelFonctionJS
   *          Le nom de la fonction JS a appelé si appel d'une soumission de
   *          formulaire lors d'une sélection d'un item de la liste.
   * @return un onjet String qui contient le code HTML à afficher dans la page
   *         pour obtenir ce composant
   * @throws JspException
   *           une erreur lors du traitement de la balise
   */
  private String genererCodeHtmlComposant(String appelFonctionJS)
      throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    // String urlListeValeurOnkeyup = (String)
    // pageContext.getAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONKEYUP);
    // Fixer l'évènement pour la liste de valeur s'il y a lieu.
    String urlListeValeurOnchange = (String) pageContext
        .getAttribute(ConstantesBaliseJSP.LISTE_VALEURS_URL_ONCHANGE);

    if (urlListeValeurOnchange != null) {
      urlListeValeurOnchange = ListeValeursTag.remplacerParametreProperty(
          urlListeValeurOnchange, getProperty(), pageContext);
      setOnchange(urlListeValeurOnchange);
    }

    if (isIgnorerValidation()) {
      Href nouveauHref = new Href(getHref());
      nouveauHref.ajouterParametre("ignorerValidation", "true");
      setHref(nouveauHref.getUrlPourRedirection());
    }

    if (getProperty() == null) {
      setProperty(UtilitaireBaliseJSP.getNomProprieteVide(pageContext));
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    // Si le formulaire est en affichage seulement tous les composants doivent
    // l'être
    if (formulaire != null) {
      if (isFormulaireEnAffichageSeulement()) {
        setAffichageSeulement("true");
      }
      if (formulaire.isModeAffichageSeulement() || isAffichage()) {
        this.setObligatoire(false);
        this.setAffichageIndObligatoire(Boolean.FALSE);
      }
    }

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested
        .getNomFormulaireImbrique(pageContext, getProperty());

    if (getObligatoire()) {
      formulaire.ajouterAttributObligatoire(nomFormulaireImbrique,
          getProperty());
    }

    if ((nomFormulaireImbrique != null)
        && (getNomAttributDestinationAjax() != null)) {
      setNomAttributDestinationAjax(UtilitaireBaliseNested
          .getIdentifiantUnique(getNomAttributDestinationAjax(), pageContext));
    }

    if ((getHref() != null) && !isAjax()) {
      if (UtilitaireString.isVide(getNomParametreValeur())) {
        appelFonctionJS = UtilitaireBaliseJSP.genererSoumissionFormulaire(
            pageContext, getHref(), nomFormulaire,
            formulaire.isTransactionnel(), false, null, null);
      } else {
        StringBuffer fonctionNavigationJS = new StringBuffer();
        fonctionNavigationJS.append("window.location ='");
        fonctionNavigationJS.append(getHref());
        // Non conforme au W3C. Les & ne sont pas tolérés en XHTML.
        // REF: http://www.w3.org/TR/xhtml1/#C_12
        // fonctionNavigationJS.append("&");
        fonctionNavigationJS.append("&amp;");
        fonctionNavigationJS.append(getNomParametreValeur());
        fonctionNavigationJS.append("=' + ");
        fonctionNavigationJS.append("document.getElementById('");
        fonctionNavigationJS.append(getProperty());
        fonctionNavigationJS.append("').value");
        appelFonctionJS = fonctionNavigationJS.toString();
      }
    }

    StringBuffer resultat = new StringBuffer("");

    Libelle libelle = null;
    String styleLibelle = null;

    String titre = null;

    Boolean estPositioneeHautChamp = null;
    String nomFormulaireJS = null;

    // Accès au formulaire
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    MessageErreur erreur = null;

    if (getProperty().indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) == -1) {
      if (!getDisabled() && !getReadonly() && !isAffichage()
          && getVar() == null) {
        // Ajouter l'attribut à traiter si le formulaire traite seulement les
        // attributs de la JSP.
        baseForm.ajouterAttributATraiter(getProperty());
      }

      if (isTraitementTransactionnel()) {
        nomFormulaireJS = UtilitaireBaliseJSP
            .genererFonctionIndModificationFormulaire(pageContext, property);
      }

      erreur = UtilitaireBaliseJSP.getMessageErreur(baseForm, this.property,
          this.pageContext);

      if (erreur != null) {
        // On obtient le message d'erreur
        titre = erreur.getMessage();

        // Permet de verifier si le message d'erreur de saisie
        // doit être positionné en bas du champ. False est la valeur par défaut
        // et signifie que
        // les messages apparaîtront en nicetitle au survol du champ.
        estPositioneeHautChamp = GestionParametreSysteme.getInstance()
            .getBoolean(Constantes.POSITION_MESSAGE_ERREUR_SAISIE);

        if (estPositioneeHautChamp == null) {
          estPositioneeHautChamp = Boolean.FALSE;
        }
      }

      if (erreur != null) {
        StringBuffer styleClassComplet = new StringBuffer("selectErreur");

        if (getStyleClass() != null) {
          styleClassComplet.append(" ");
          styleClassComplet.append(getStyleClass());
        }

        this.setStyleClass(styleClassComplet.toString());
        this.setStyleClassFocus(styleClassComplet.toString());
        styleLibelle = "libelleErreur";
        titre = erreur.getMessage();
      } else {
        libelle = UtilitaireLibelle.getLibelle(this.getTitle(), pageContext,
            null);
        titre = libelle.getMessage();
      }
    } else {
      libelle = UtilitaireLibelle
          .getLibelle(this.getTitle(), pageContext, null);
      titre = libelle.getMessage();
    }

    // Pour que le titre du champ de saisie soit convertie en texte du gestion
    // message
    if ((titre != null) && (estPositioneeHautChamp.booleanValue() == false)) {
      this.setTitle(titre);
    }

    String titreErreur = null;

    if (erreur != null) {
      if (!GestionParametreSysteme
          .getInstance()
          .getBoolean(
              ConstantesParametreSysteme.AIDE_CONTEXTUELLE_SUR_LIBELLE_ERREUR)
          .booleanValue()) {
        setTitle("");
      }

      titreErreur = titre;
    }

    // Générer le libéllé.
    if (getLibelle() != null) {
      if (getOuvrirLigne()) {
        if (isGenererLigne()) {
          resultat.append("<tr");

          if (getLigneStyleClass() != null) {
            resultat.append(" class=\"");
            resultat.append(getLigneStyleClass());
            resultat.append("\" ");
          }

          resultat.append(">");
        }
      }

      // Générer le bout de code HTML qui affiche le libellé
      if (isGenererLigne()) {
        // Ajouter le bout de code qui affiche le libellé
        if (isAffichage()) {
          resultat.append("<th scope=\"row\" class=\"libelle_formulaire\">");
          setObligatoire(false);
          nomAttribut = "null";
        } else {
          resultat.append("<th scope=\"row\">");
        }
      }

      if (UtilitaireString.isVide(getNomAttribut())) {
        setNomAttribut(this.property);
      }

      if (isAffichageIconeAideAGauche()
          && (isAffichageIconeAide() || isAideEnLigne(getLibelle(), pageContext))) {
        UtilitaireBaliseJSP.genererIcodeAide(getLibelle(), getProperty(),
            resultat, pageContext);
        resultat.append("&nbsp;");
      }

      if (nomFormulaireImbrique != null) {
        UtilitaireBaliseJSP.genererLibelle(nomFormulaireImbrique,
            this.property, -1, getTitle(), getLibelle(), styleLibelle,
            getObligatoire(), isAffichageIndObligatoire(), pageContext,
            resultat);
      } else {
        UtilitaireBaliseJSP.genererLibelle(this.property, getTitle(),
            this.libelle, styleLibelle, this.obligatoire,
            isAffichageIndObligatoire(), this.pageContext, resultat);
      }

      if (genererLigne) {
        resultat.append("</th><td>");
      } else {
        resultat.append("&nbsp;");
      }
    } else {
      if (getObligatoire()) {
        UtilitaireBaliseJSP.ajouterAttributObligatoire(nomFormulaireImbrique,
            getProperty(), pageContext);
      }
    }

    setTitle(titreErreur);

    Object valeurChamp = this.getValeurFormulaire();

    if ((getProperty().indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) != -1)
        || (!formulaire.isModeAffichageSeulement() && !isAffichage())) {
      resultat.append("<select name=\"");

      // * @since Struts 1.1
      if (this.indexed) {
        prepareIndex(resultat, name);
      }

      resultat.append(property);
      resultat.append("\"");

      if (accesskey != null) {
        resultat.append(" accesskey=\"");
        resultat.append(accesskey);
        resultat.append("\"");
      }

      if (multiple != null) {
        resultat.append(" multiple=\"multiple\"");
      }

      if (size != null) {
        resultat.append(" size=\"");
        resultat.append(size);
        resultat.append("\"");
      }

      if (tabindex != null) {
        resultat.append(" tabindex=\"");
        resultat.append(tabindex);
        resultat.append("\"");
      }

      if (getProperty().indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) == -1) {
        this.setStyleId(UtilitaireBaliseNested.getIdentifiantUnique(
            getProperty(), pageContext));
      } else {
        if (getDivRetourAjax() != null) {
          this.setStyleId(UtilitaireBaliseNested.getIdentifiantUnique(
              getProperty(), pageContext) + getDivRetourAjax());
        }
      }

      StringBuffer onChangeBuffer = new StringBuffer();

      String onChangeCourant = getOnchange();

      if (onChangeCourant != null) {
        onChangeBuffer.append(onChangeCourant);
      }

      setOnchange(null);

      if ((getHref() != null) && !isAjax()) {
        onChangeBuffer.append(appelFonctionJS);

        if (getNomParametreValeur() == null) {
          onChangeBuffer.append(pageContext
              .getAttribute("nomFonctionSoumissionJS"));
          onChangeBuffer.append("();");
        }
      }

      if ((getHref() != null) && isAjax()) {
        setOnchange(null);

        if (getNomParametreValeur() == null) {
          setNomParametreValeur(getProperty());
        }

        if (isIdRetourMultipleAjax()) {
          setDivRetourAjax("REPONSE_MULTIPLE");
        }

        if (getIdRetourAjax() != null) {
          setDivRetourAjax(getIdRetourAjax());
        }

        if (getNomAttributDestinationAjax() != null) {
          Href href = new Href(getHref());

          if (nomFormulaireImbrique != null) {
            href.ajouterParametre("index",
                UtilitaireBaliseNested.getIndexListeNested(pageContext));
          }

          String traitemenentApresAjax = null;

          if (divRetourAjax != null) {
            traitemenentApresAjax = UtilitaireAjax
                .traiterAffichageDivAjaxAsynchrone(getHref(),
                    getDivRetourAjax(), getNomParametreValeur(), getStyleId(),
                    getAjaxJSPendantChargement(), getAjaxJSApresChargement(),
                    pageContext);

            if (!UtilitaireString.isVide(getAjaxJSApresChargement())) {
              traitemenentApresAjax = ";" + getAjaxJSApresChargement();
            }

            setAjaxJSApresChargement(traitemenentApresAjax);
          }

          String proprieteCombo = "combo." + getNomAttributDestinationAjax();

          String appelAjax = UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(
              href.getUrlPourRedirection(), proprieteCombo,
              getNomParametreValeur(), UtilitaireBaliseNested
                  .getIdentifiantUnique(getProperty(), pageContext),
              getAjaxJSPendantChargement(), getAjaxJSApresChargement(),
              pageContext);
          onChangeBuffer.append(appelAjax);
        } else {
          String appelAjax = UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(
              getHref(), getDivRetourAjax(), getNomParametreValeur(),
              getStyleId(), getAjaxJSPendantChargement(),
              getAjaxJSApresChargement(), pageContext);
          onChangeBuffer.append(appelAjax);
        }
      }

      if (getProperty().indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) == -1) {
        if (baseForm.isTransactionnel() && isTraitementTransactionnel()) {
          UtilitaireBaliseJSP.specifierGestionModification(pageContext,
              nomFormulaire, nomFormulaireJS, onChangeBuffer.toString(),
              resultat);
        }

        if (onChangeBuffer.toString().length() != 0) {
          setOnchange(onChangeBuffer.toString());
        }

        // Faire la gestion du mode inactif
        if (this.getDisabled() || this.getReadonly()) {
          setReadonly(false);
          setDisabled(true);
          UtilitaireBaliseJSP.specifierLectureSeulement(
              this.styleClassLectureSeulement, getStyleClass(), resultat);
        } else {
          // @todo probleme avec IE 7.0
          // UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
          // getStyleClassFocus(), getOnfocus(), getOnblur(), resultat);
        }
      } else {
        setOnchange(onChangeBuffer.toString());
      }

      if (getVar() != null) {
        pageContext.getRequest().setAttribute(getVar(), onChangeBuffer);
      }

      resultat.append(prepareEventHandlers());
      resultat.append(prepareStyles());
      resultat.append(">");

      String valeurSelectionne = genererListeOptions(pageContext, resultat,
          valeurChamp);

      // Fermeture de la balise de la liste déroulante.
      resultat.append("</select>");

      if (getReadonly() || getDisabled()) {
        resultat.append("<input type=\"hidden\" name=\"" + this.property
            + "\" value=\"" + valeurSelectionne + "\"/>");
      }
    } else {
      StringBuffer options = new StringBuffer();
      String valeurSelectionne = genererListeOptions(pageContext, options,
          valeurChamp);

      if (!UtilitaireString.isVide(valeurSelectionne)) {
        String valuePart = "\"" + valeurSelectionne + "\">";
        int indexDebut = options.indexOf(valuePart) + valuePart.length();
        int indexFin = options.indexOf("<", indexDebut);
        String etiquetteSelectionne = options.substring(indexDebut, indexFin);

        resultat.append(etiquetteSelectionne);
      } else if (!UtilitaireString.isVide(this
          .getLigneVideLibelleEnAffichageSeulement())
          && this.getLigneVideLibelleEnAffichageSeulement().equals("true")) {
        // Ajouter le libelle de ligne vide si présent
        if (isAfficherLigneVide() || (getLigneVideLibelle() != null)) {
          Libelle ligneVideLibelle = null;
          ligneVideLibelle = UtilitaireLibelle.getLibelle(
              getLigneVideLibelle(), pageContext);
          if (ligneVideLibelle != null) {
            resultat.append(ligneVideLibelle.getMessage());
          }
        }
      }
    }

    if (erreur != null) {
      // Si true, on place le message d'erreur en bas de la balise
      if (estPositioneeHautChamp.booleanValue()) {
        resultat.append("<span class=\"libelleErreur\">" + titre + "</span>");
      }
    }

    return resultat.toString();
  }

  public Object getValeurFormulaire() {
    Object valeurFormulaire = this.getValeurChamp(this.getProperty());

    if (getProperty().indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) == -1) {
      Object bean = pageContext.findAttribute(name);
      if (bean == null) {
        bean = UtilitaireBaliseJSP.getBaseForm(pageContext);
      }

      valeurFormulaire = UtilitaireObjet.getValeurAttribut(bean, property);

      if ((valeurFormulaire == null) && (value != null)) {
        valeurFormulaire = value;
      }
    } else {
      valeurFormulaire = value;
    }

    return valeurFormulaire;
  }

  private Object getValeurChamp(String nomPropriete) {
    Object valeurChamp = null;

    if (nomPropriete.indexOf(ConstantesBaliseJSP.PROPRIETE_VIDE) == -1) {
      Object bean = pageContext.findAttribute(name);

      if (bean == null) {
        bean = UtilitaireBaliseJSP.getBaseForm(pageContext);
      }

      valeurChamp = UtilitaireObjet.getValeurAttribut(bean, nomPropriete);

      if ((valeurChamp == null) && (value != null)) {
        valeurChamp = value;
      }
    } else {
      valeurChamp = value;
    }

    return valeurChamp;
  }

  /**
   * Execute le début de la balises JSP
   * <p>
   * 
   * @throws JspException
   *           si un exception JSP est lancé.
   */
  @Override
  public int doStartTag() throws JspException {
    try {
      if (this.classeDecorator != null) {
        this.decorator = (SelectDecorator) Thread.currentThread()
            .getContextClassLoader().loadClass(this.classeDecorator)
            .newInstance();
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error(
            "Erreur lors de l'instanciation du décorateur de balise Select.", e);
      }
    }

    if (getLibelle() != null) {
      TableTag.gererOuvertureEtFermeturePourBalise(this, pageContext);
    }

    /**
     * Si paramètre la propriété iconeAide est null et qu'il existe un paramètre
     * système, alors prendre la valeur de celui-ci.
     * 
     * @since SOFI 2.0.3
     */
    if (this.iconeAide == null) {
      Boolean iconeAideDefaut = GestionParametreSysteme.getInstance()
          .getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE);

      if (iconeAideDefaut != null) {
        setIconeAide(iconeAideDefaut.toString());
      }
    }

    /**
     * Si paramètre la propriété iconeAideAGauche est null et qu'il existe un
     * paramètre système, alors prendre la valeur de celui-ci.
     * 
     * @since SOFI 2.1
     */
    if (this.iconeAideAGauche == null) {
      Boolean iconeAideAGaucheDefaut = GestionParametreSysteme.getInstance()
          .getBoolean(
              ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE_A_GAUCHE);

      if (iconeAideAGaucheDefaut != null) {
        setIconeAideAGauche(iconeAideAGaucheDefaut.toString());
      }
    }

    if ((getNomAttributDestinationAjax() != null) && (getHref() == null)) {
      throw new SOFIException("Vous devez spécifier les attributs href");
    }

    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested
        .getNomFormulaireImbrique(pageContext, getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    TagUtils.getInstance().write(pageContext, renderSelectStartElement());

    return (EVAL_BODY_BUFFERED);
  }

  /**
   * Execute la fin de la balise.
   * <p>
   * 
   * @throws JspException
   *           si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette
   *         balise
   */
  @Override
  public int doEndTag() throws JspException {
    // retirer l'attribut dans la portée de la page.
    pageContext.removeAttribute(Constants.SELECT_KEY);

    StringBuffer resultat = new StringBuffer();

    if (bodyContent != null) {
      resultat.append(bodyContent.getString());
    }

    if (!isAffichageIconeAideAGauche()
        && (isAffichageIconeAide() || isAideEnLigne(getLibelle(), pageContext))
        && (pageContext.getAttribute("CHAMP_SECURISE") == null)) {
      UtilitaireBaliseJSP.genererIcodeAide(getLibelle(), getProperty(),
          resultat, pageContext);
    }

    // Fermeture de la colonne si libellé de spécifier.
    if (getLibelle() != null) {
      resultat.append("</td>");

      if (getFermerLigne()) {
        resultat.append("</tr>");
      }
    }

    TagUtils.getInstance().write(pageContext, resultat.toString());
    nomAttribut = null;
    cache = null;
    libelle = null;
    ligneVideLibelle = null;
    ligneVideValeur = null;
    ligneVide = null;
    collection = null;
    affichageIndObligatoire = null;
    ajax = false;
    disabledEL = null;
    divRetourAjax = null;
    fermerLigne = true;
    genererLigne = true;
    href = null;
    value = null;
    property = null;
    readonlyEL = null;
    nomAttributDestinationAjax = null;
    nomParametreValeur = null;
    styleClassFocus = null;
    styleClassLectureSeulement = null;
    affichageSeulement = null;
    selectMultiChoixSource = null;
    selectMultiChoixDestination = null;
    sousCache = null;
    activerCleDefaut = null;
    filtre = null;

    super.release();

    this.pageContext.setAttribute("CHAMP_SECURISE", null);

    return (EVAL_PAGE);
  }

  /**
   * Permet de générer la liste d'options dans la liste déroulante.
   * <p>
   * Cette liste peut résulter d'un liste dans une porté du servlet ou encore
   * être en cache et gérer par GestionCache.
   * <p>
   * 
   * @param pageContext
   *          le context de la page en cours
   * @param resultat
   *          le contenu de la balise à afficher
   * @param valeurChamp
   *          la valeur du champ a afficher dans la balise
   * @see org.sofiframework.application.cache.GestionCache
   */
  @SuppressWarnings("rawtypes")
  public String genererListeOptions(PageContext pageContext,
      StringBuffer resultat, Object valeurBalise) throws JspException {
    String valeurChamp = null;
    String valeurSelectionnee = "";

    HashSet listeSelectionne = this.getListeSelectionne(valeurBalise);
    if (listeSelectionne.isEmpty()) {
      valeurChamp = (String) valeurBalise;
    }

    boolean valeurVide = UtilitaireString.isVide(valeurChamp);

    if (isAfficherLigneVide() || (getLigneVideLibelle() != null)) {
      Libelle libelle = null;

      if (getLigneVideLibelle() != null) {
        libelle = UtilitaireLibelle.getLibelle(getLigneVideLibelle(),
            pageContext);
      }

      String etiquette = (libelle == null) || (libelle.getMessage() == null) ? ""
          : libelle.getMessage();

      ecrireOption(resultat, etiquette, "", valeurVide, null);

    }

    if (utiliseCollection()) {
      valeurSelectionnee = populerListe(valeurChamp, valeurBalise,
          listeSelectionne, resultat, null);
    }

    if (utiliseCache()) {
      valeurSelectionnee = populerCache(valeurChamp, valeurBalise,
          listeSelectionne, resultat, getValeurParDefaut());
    }

    return valeurSelectionnee;
  }

  private Object getValeurParDefaut() {
    ObjetCache objetCache = GestionCache.getInstance().getCache(getCache(),
        sousCache);
    return objetCache.getCleDefaut();
  }

  private boolean utiliseCollection() {
    return (getCollection() != null) && (getListe().size() != 0);
  }

  private boolean utiliseCache() {
    return (getCache() != null);
  }

  private HashSet getListeSelectionne(Object valeur) {
    HashSet listeSelectionne = new HashSet();

    if ((valeur != null)
        && valeur.getClass().getName().equals("[Ljava.lang.String;")) {
      String[] tableauSelectionne = (String[]) valeur;

      for (int i = 0; i < tableauSelectionne.length; i++) {
        listeSelectionne.add(tableauSelectionne[i].toString());
      }
    }

    return listeSelectionne;
  }

  private String populerListe(String valeurChamp, Object valeurBalise,
      HashSet listeSelectionne, StringBuffer resultat, List listeEnfants) {

    List listeAtraiter = null;

    if (listeEnfants != null) {
      listeAtraiter = listeEnfants;
    } else {
      listeAtraiter = getListe();
    }
    String valeurSelectionnee = "";

    for (Iterator i = listeAtraiter.iterator(); i.hasNext();) {
      Object objet = i.next();
      String etiquette = getEtiquette(objet);
      String valeurString = getValeur(objet);
      String infoBulle = getInfoBulle(objet);

      // Appliquer la gestion des libellés.
      Libelle libelle = UtilitaireLibelle.getLibelle(etiquette, pageContext);

      // Si il y a un libelle l'utilisé comme valeur
      if ((libelle != null) && (libelle.getMessage() != null)) {
        etiquette = libelle.getMessage();
      }

      if (valeurChamp == null) {
        valeurChamp = "";
      }

      boolean ecrireOption = this.isEcrireOption(valeurString);
      boolean champDestination = this.isMultiChoixDestination();

      if (ecrireOption) {
        
        if (getProprieteOptGroup() != null) {
          listeEnfants = (List) UtilitaireObjet.getValeurAttribut(objet,
              getProprieteOptGroup());
        }
        if (listeEnfants != null && !listeEnfants.isEmpty()) {

          resultat.append("<optgroup label=\"");
          resultat.append(etiquette);
          resultat.append("\">");

          populerListe(valeurChamp, valeurBalise, listeSelectionne, resultat,
              listeEnfants);

          resultat.append("</optgroup>\n");

        } else {

          if ((valeurChamp != null)
              && (valeurString.equals(valeurChamp) || listeSelectionne
                  .contains(valeurString))) {
            ecrireOption(resultat, etiquette, valeurString, !champDestination,
                infoBulle);
            valeurSelectionnee = valeurString;
          } else {
            /*
             * Si on est la destination on fait seulement écrire les valeurs qui
             * sont sélectionnées.
             */
            if (!champDestination) {
              // Déterminer si on est pas une des sélection de la destination
              ecrireOption(resultat, etiquette, valeurString, false, infoBulle);
            }
          }
        }
      }
    }

    return valeurSelectionnee;
  }

  private boolean isEcrireOption(String valeur) {
    boolean ecrireOption = true;
    /*
     * Si on est une source et que la valeur se retrouve dans la destination on
     * ne l'ecrit pas.
     */
    if (this.isMultiChoixSource() && this.isValeurDestination(valeur)) {
      ecrireOption = false;
    }
    return ecrireOption;
  }

  private boolean isValeurDestination(String valeur) {
    boolean isDestination = false;

    String nomAttributDestination = (String) pageContext
        .getAttribute(SelectMultiChoixTag.NOM_ATTRIBUT_SELECT_MULTI_CHOIX_DESTINATION
            + getProperty());
    Object valeurDestination = this.getValeurChamp(nomAttributDestination);

    HashSet listeSelection = getListeSelectionne(valeurDestination);

    return listeSelection.contains(valeur)
        || valeurDestination.toString().equals(valeur);
  }

  private String populerCache(String valeurChamp, Object valeurBalise,
      HashSet listeSelectionne, StringBuffer resultat, Object valeurDefaut)
      throws JspException {
    String valeurSelectionnee = "";
    String nomCache = getCache();

    // Utilisation de la cache
    ObjetCache objetCache = GestionCache.getInstance().getCache(nomCache,
        sousCache, codeClient);

    Map collection = null;

    /*
     * On retire de la collection les éléments EntreeCache inactifs s'il y a en
     * a; mais on conserve la valeur de la propriété peu importe.
     */
    if (this.actifsSeulement) {
      collection = objetCache.getCollectionSansInactifs(valeurChamp);
    } else {
      collection = objetCache.getCollection();
    }
    /*
     * On peut appliquer un filtre a une collection
     * 
     * @since SOFI 2.0.3
     */
    collection = UtilitaireBaliseJSP.appliquerFiltreCache(collection,
        this.filtre, this, pageContext);

    Iterator iterEtiquette = collection.values().iterator();
    Iterator iterCle = collection.keySet().iterator();

    // Traiter l'étiquette de la cache.
    String etiquette = null;
    boolean traiterLibelle = true;
    String infoBulle = null;

    while (iterEtiquette.hasNext()) {
      Object valeurEtiquette = iterEtiquette.next();

      // La propriété de l'objet est identifiée
      if (!UtilitaireString.isVide(getProprieteEtiquette())
          && (getListe().size() == 0)) {
        etiquette = getEtiquette(valeurEtiquette);
        // Si la description locale est utilisée, il faut pas traiter libelle
        if (isAfficherDescriptionLocale()) {
          traiterLibelle = false;
        }
        // Si il s'agit d'un domaine de valeur
      } else if (DomaineValeur.class.isInstance(valeurEtiquette)) {
        DomaineValeur domaineValeur = (DomaineValeur) valeurEtiquette;
        Locale locale = UtilitaireBaliseJSP.getLocale(pageContext);

        if ((domaineValeur.getListeDescriptionLangue() != null)
            && (domaineValeur.getListeDescriptionLangue().size() > 0)) {
          etiquette = (String) domaineValeur.getListeDescriptionLangue().get(
              locale.toString());
          traiterLibelle = false;
        } else {
          etiquette = domaineValeur.getDescription();
        }
        // Si il s'agit d'une string
      } else {
        etiquette = (String) valeurEtiquette;
      }

      if (traiterLibelle) {
        // Appliquer la gestion des libellés.
        Libelle libelle = UtilitaireLibelle.getLibelle(etiquette, pageContext);
        etiquette = libelle.getMessage();
      }

      // Traiter la valeur de la cache.
      Object valeur = iterCle.next();

      if (!UtilitaireString.isVide(getProprieteValeur())
          && valeur instanceof ObjetTransfert) {
        ObjetTransfert objetTransfert = (ObjetTransfert) valeur;
        valeur = getPropriete(objetTransfert, getProprieteValeur());
      }

      // Traiter l'info-bulle si present
      if (!UtilitaireString.isVide(getProprieteTitle())
          && DomaineValeur.class.isInstance(valeurEtiquette)) {
        DomaineValeur domaineValeur = (DomaineValeur) valeurEtiquette;
        Locale locale = UtilitaireBaliseJSP.getLocale(pageContext);

        if ((domaineValeur.getListeDescriptionLangue() != null)
            && (domaineValeur.getListeDescriptionLangue().size() > 0)) {
          infoBulle = (String) domaineValeur.getListeDescriptionLangue().get(
              locale.getLanguage());
        } else {
          infoBulle = domaineValeur.getDescription();
        }
      }

      /*
       * On doit convertir la valeur par défaut pour aller chercher la propriété
       * valeur si jamais elle n'est pas null, si la valeur par défaut n'est pas
       * déjà une string.
       */
      if (!UtilitaireString.isVide(getProprieteValeur())
          && valeurDefaut != null && !(valeurDefaut instanceof String)) {
        valeurDefaut = getPropriete(valeurDefaut, getProprieteValeur());
      }

      if (valeurChamp == null) {
        valeurChamp = "";
      }

      boolean ecrireOption = this.isEcrireOption(valeur.toString());
      // Si il s'agit du champ destination d'un selectMultiChoix
      boolean champDestination = this.isMultiChoixDestination();

      if (ecrireOption) {
        boolean selectionne = (valeurChamp != null)
            && (valeur != null)
            && (valeur.toString().equals(valeurChamp) || listeSelectionne
                .contains(valeur.toString()));

        boolean defaut = UtilitaireString.isVide(valeurChamp)
            && traiterCleDefaut() && (valeurDefaut != null) && (valeur != null)
            && valeur.equals(valeurDefaut) && !this.isMultiChoixSource();

        if (selectionne || defaut) {
          // On sélectionne les options si il ne s'agit pas du champ destination
          // d'un selectMultiChoix
          ecrireOption(resultat, etiquette, valeur.toString(),
              !champDestination, infoBulle);
          valeurSelectionnee = valeur.toString();
        } else {
          /*
           * Si on est la destination, on fait seulement écrire les valeurs qui
           * sont sélectionnées.
           */
          if (!champDestination) {
            ecrireOption(resultat, etiquette,
                valeur != null ? valeur.toString() : "", false, infoBulle);
          }
        }
      }
    }

    return valeurSelectionnee;
  }

  /**
   * Obtenir la valeur d'une propriété de l'objet de transfert.
   * 
   * @param objet
   *          Objet de transfert
   * @param propriete
   *          nom de la propriété
   * @return valeur de la propriété
   */
  private Object getPropriete(Object objet, String propriete) {
    Object valeur = null;

    if (propriete.indexOf("[") != -1) {
      valeur = UtilitaireObjet.evaluerExpression(objet, propriete);
    } else {
      valeur = UtilitaireObjet.getValeurAttribut(objet, propriete);
    }

    return valeur;
  }

  /**
   * Obtenir la valeur de l'étiquette pour un objet.
   * 
   * @param objet
   *          item de la liste
   * @return étiquette
   */
  private String getEtiquette(Object objet) {
    String etiquette = null;

    if (this.decorator != null) {
      OptionDecore decore = new OptionDecore(objet);
      etiquette = decore.getEtiquette();
    } else if (objet instanceof String) {
      etiquette = (String) objet;
    } else {
      Object propriete = getPropriete(objet, getProprieteEtiquette());

      if (propriete instanceof java.lang.String) {
        etiquette = (String) propriete;
      } else if (propriete instanceof java.util.Map) {
        Locale locale = UtilitaireBaliseJSP.getLocale(pageContext);
        // Si la propriété est un objet de type Map on prend l'élément du Map
        // qui correspond à la langue de l'utilisateur
        etiquette = (String) ((Map) propriete).get(locale.getLanguage()
            .toLowerCase());
      } else if (propriete instanceof java.util.Date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
            UtilitaireBaliseJSP.getLocale(pageContext));
        etiquette = sdf.format(propriete.toString());
      } else {
        if (propriete != null) {
          etiquette = propriete.toString();
        }
      }
    }

    return etiquette;
  }

  /**
   * Obtenir la valeur à utiliser pour l'objet courant.
   * 
   * @param objet
   *          Item dont on veut obtenir la propriété valeur
   * @return valeur en chaîne de caractères
   */
  private String getValeur(Object objet) {
    String valeur = null;

    if (this.decorator != null) {
      OptionDecore decore = new OptionDecore(objet);
      valeur = decore.getValeur();
    } else {
      Object propriete = getPropriete(objet, getProprieteValeur());
      if (propriete instanceof String) {
        valeur = (String) propriete;
      } else {
        if (propriete instanceof java.util.Date) {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
              UtilitaireBaliseJSP.getLocale(pageContext));
          valeur = sdf.format(propriete.toString());
        } else {
          valeur = propriete.toString();
        }
      }
    }

    return valeur;
  }

  /**
   * Obtenir l'info-bulle à utiliser pour l'objet courant.
   * 
   * @param objet
   *          Item dont on veut obtenir la propriété valeur
   * @return valeur en chaîne de caractères
   */
  private String getInfoBulle(Object objet) {
    String valeur = null;

    if (!UtilitaireString.isVide(getProprieteTitle())) {
      Object propriete = getPropriete(objet, getProprieteTitle());
      if (propriete instanceof String) {
        valeur = (String) propriete;
      } else {
        if (propriete instanceof java.util.Date) {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
              UtilitaireBaliseJSP.getLocale(pageContext));
          valeur = sdf.format(propriete.toString());
        } else {
          valeur = propriete.toString();
        }
      }
    }

    return valeur;
  }

  /**
   * Obtenir l'objet cache pour contruire la liste des options du cmposant.
   * 
   * @return Liste d'objet
   */
  protected ObjetCache getObjetCache(String nomCache) {
    return GestionCache.getInstance().getCache(nomCache);
  }

  /**
   * Ajoute une option à la liste déroulante.
   * <p>
   * 
   * @param resultat
   *          le contenu de la balise à afficher
   * @param etiquette
   *          l'étiquette à afficher
   * @param valeur
   *          la valeur de l'option à écrire
   * @param isSelectionne
   *          valeur identifiant si la valeur est la valeur sélectionnée ou pas
   * @param title
   *          Valeur afficher dans une info-bulle
   */
  public void ecrireOption(StringBuffer resultat, String etiquette,
      String valeur, boolean isSelectionne, String title) {
    if (isSelectionne) {
      // Pas conforme au W3C. Un attribut doit posséder une valeur en XHTML.
      // REF: http://www.w3.org/TR/xhtml1/#h-4.5
      // resultat.append("<option selected value=\"");
      resultat.append("<option selected=\"selected\" value=\"");
    } else {
      resultat.append("<option value=\"");
    }

    resultat.append(valeur);
    if (!UtilitaireString.isVide(title)) {
      resultat.append("\" title=\"");
      resultat.append(title);
    }
    resultat.append("\">");
    resultat.append(etiquette);
    resultat.append("</option>\n");
  }

  /**
   * Retourne le lien hypertexte a appeler lors d'une nouvelle sélection de la
   * liste déroulante.
   * 
   * @return le lien hypertexte a appeler lors d'une nouvelle sélection de la
   *         liste déroulante.
   */
  public String getHref() {
    return href;
  }

  /**
   * Fixer le lien hypertexte a appeler lors d'une nouvelle sélection de la
   * liste déroulante.
   * 
   * @param href
   *          le lien hypertexte a appeler lors d'une nouvelle sélection de la
   *          liste déroulante.
   */
  public void setHref(String href) {
    this.href = href;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * 
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    try {
      String valeurELHref = EvaluateurExpression.evaluerString("href",
          getHref(), this, pageContext);

      setHref(valeurELHref);

      if (getDisabledEL() != null) {
        boolean disabled = EvaluateurExpression.evaluerBoolean("disabledEL",
            getDisabledEL(), this, pageContext);
        setDisabled(disabled);
      }

      if (getReadonlyEL() != null) {
        boolean readonly = EvaluateurExpression.evaluerBoolean("readonlyEL",
            String.valueOf(getReadonlyEL()), this, pageContext);
        setDisabled(readonly);
      }

      if (getAffichageIndObligatoire() != null) {
        String affichageObligatoire = EvaluateurExpression.evaluerString(
            "affichageIndObligatoire",
            String.valueOf(getAffichageIndObligatoire()), this, pageContext);
        setAffichageIndObligatoire(affichageObligatoire);
      }

      if (getValue() != null) {
        String value = EvaluateurExpression.evaluerString("value",
            String.valueOf(getValue()), this, pageContext);
        setValue(value);
      }

      if (getLigneVideLibelle() != null) {
        String ligneVideLibelle = EvaluateurExpression.evaluerString(
            "ligneVideLibelle", String.valueOf(getLigneVideLibelle()), this,
            pageContext);
        setLigneVideLibelle(ligneVideLibelle);
      }

      if (getOnchange() != null) {
        String onChange = EvaluateurExpression.evaluerString("onchange",
            String.valueOf(getOnchange()), this, pageContext);
        setOnchange(onChange);
      }

      if (getIgnorerValidation() != null) {
        setIgnorerValidation(EvaluateurExpression.evaluerString(
            "ignorerValidation", (String) getIgnorerValidation(), this,
            pageContext));
      }

      if (getLibelle() != null) {
        String libelle = EvaluateurExpression.evaluerString("libelle",
            String.valueOf(getLibelle()), this, pageContext);
        setLibelle(libelle);
      }

      if (getProperty() != null) {
        String property = EvaluateurExpression.evaluerString("property",
            String.valueOf(getProperty()), this, pageContext);
        setProperty(property);
      }

      if (getCache() != null) {
        String cache = EvaluateurExpression.evaluerString("cache",
            String.valueOf(getCache()), this, pageContext);
        setCache(cache);
      }

      if (getAffichageSeulement() != null) {
        String affichageSeulement = EvaluateurExpression.evaluerString(
            "affichageSeulement", (String) getAffichageSeulement(), this,
            pageContext);

        setAffichageSeulement(affichageSeulement.toLowerCase());
      }

      if (getDivRetourAjax() != null) {
        String divRetour = EvaluateurExpression.evaluerString("divRetourAjax",
            String.valueOf(getDivRetourAjax()), this, pageContext);
        setDivRetourAjax(divRetour);
      }

      if (getIdRetourAjax() != null) {
        String idRetour = EvaluateurExpression.evaluerString("idRetourAjax",
            String.valueOf(getIdRetourAjax()), this, pageContext);
        setIdRetourAjax(idRetour);
      }

      if (getLigneVide() != null) {
        String ligneVide = EvaluateurExpression.evaluerString("ligneVide",
            String.valueOf(getLigneVide()), this, pageContext);
        setLigneVide(ligneVide);
      }

      String ajaxJSPendantChargement = EvaluateurExpression.evaluerString(
          "ajaxJSPendantChargement", getAjaxJSPendantChargement(), this,
          pageContext);
      setAjaxJSPendantChargement(ajaxJSPendantChargement);

      String ajaxJSApresChargement = EvaluateurExpression.evaluerString(
          "ajaxJSApresChargement", getAjaxJSApresChargement(), this,
          pageContext);
      setAjaxJSApresChargement(ajaxJSApresChargement);

      if (activerCleDefaut != null) {
        activerCleDefaut = new Boolean(EvaluateurExpression.evaluerBoolean(
            "activerCleDefaut", (String) getActiverCleDefaut(), this,
            pageContext));
      }

    } catch (JspException e) {
      throw e;
    }
  }

  /**
   * Déterminer si on doit activer le traitement de la valeur par défaut pour
   * l'objet cache de la balise.
   * 
   * @return true, utiliser la valeur par défaut, false sinon
   */
  private boolean traiterCleDefaut() {
    // Le paramètre doit être présent et
    // on doit le fixer à "true"
    return (activerCleDefaut != null) && (activerCleDefaut instanceof Boolean)
        && ((Boolean) activerCleDefaut).booleanValue();
  }

  public void setLigneStyleClass(String ligneStyleClass) {
    this.ligneStyleClass = ligneStyleClass;
  }

  public String getLigneStyleClass() {
    return ligneStyleClass;
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setGenererLigne(boolean genererLigne) {
    this.genererLigne = genererLigne;
  }

  public boolean isGenererLigne() {
    return genererLigne;
  }

  /**
   * Fixer true si on veut l'affichage de l'indicateur obligatoire.
   * 
   * @param affichageIndObligatoire
   *          true si on veut l'affichage de l'indicateur obligatoire.
   */
  public void setAffichageIndObligatoire(Object affichageIndObligatoire) {
    this.affichageIndObligatoire = affichageIndObligatoire;
  }

  /**
   * Retourne true si on veut l'affichage de l'indicateur obligatoire.
   * 
   * @return true si on veut l'affichage de l'indicateur obligatoire.
   */
  public Object getAffichageIndObligatoire() {
    return affichageIndObligatoire;
  }

  /**
   * Est-ce que l'affichage de l'indicateur obligatoire est requis.
   */
  private boolean isAffichageIndObligatoire() {
    if ((getAffichageIndObligatoire() != null)
        && getAffichageIndObligatoire().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public String getName() {
    return (this.name);
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public String getProperty() {
    return property;
  }

  public String getMultiple() {
    return (this.multiple);
  }

  public void setMultiple(String multiple) {
    this.multiple = multiple;
  }

  public String getSize() {
    return (this.size);
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getValue() {
    return (this.value);
  }

  public void setValue(String value) {
    this.value = value;
  }

  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  public boolean isAjax() {
    return ajax;
  }

  public void setDivRetourAjax(String divRetourAjax) {
    if (!UtilitaireString.isVide(divRetourAjax)) {
      setAjax(true);
      this.divRetourAjax = divRetourAjax;
    }
  }

  public String getDivRetourAjax() {
    return divRetourAjax;
  }

  public void setNomParametreValeur(String nomParametreValeur) {
    this.nomParametreValeur = nomParametreValeur;
  }

  public String getNomParametreValeur() {
    return nomParametreValeur;
  }

  public void setNomAttributDestinationAjax(String nomAttributDestinationAjax) {
    if (!UtilitaireString.isVide(nomAttributDestinationAjax)) {
      setAjax(true);
      this.nomAttributDestinationAjax = nomAttributDestinationAjax;
    }
  }

  public String getNomAttributDestinationAjax() {
    return nomAttributDestinationAjax;
  }

  public void setIdRetourAjax(String idRetourAjax) {
    this.idRetourAjax = idRetourAjax;
  }

  public String getIdRetourAjax() {
    return idRetourAjax;
  }

  public void setIdRetourMultipleAjax(boolean idRetourMultipleAjax) {
    this.idRetourMultipleAjax = idRetourMultipleAjax;
  }

  public boolean isIdRetourMultipleAjax() {
    return idRetourMultipleAjax;
  }

  public void setIgnorerValidation(Object ignorerValidation) {
    this.ignorerValidation = ignorerValidation;
  }

  public Object getIgnorerValidation() {
    return ignorerValidation;
  }

  public boolean isIgnorerValidation() {
    if ((getIgnorerValidation() != null)
        && getIgnorerValidation().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setAjaxJSPendantChargement(String ajaxJSPendantChargement) {
    this.ajaxJSPendantChargement = ajaxJSPendantChargement;
  }

  public String getAjaxJSPendantChargement() {
    return ajaxJSPendantChargement;
  }

  public void setAjaxJSApresChargement(String ajaxJSApresChargement) {
    this.ajaxJSApresChargement = ajaxJSApresChargement;
  }

  public String getAjaxJSApresChargement() {
    return ajaxJSApresChargement;
  }

  public void setAffichageSeulement(Object affichageSeulement) {
    this.affichageSeulement = affichageSeulement;
  }

  public Object getAffichageSeulement() {
    return affichageSeulement;
  }

  /**
   * Est-ce que le champ de saisie est en affichage seulement.
   * 
   * @return true si le champ de saisie est affichage seulement.
   */
  public boolean isAffichage() {
    if ((getAffichageSeulement() != null)
        && getAffichageSeulement().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer le nom de la variable dont sera logé le traitement de modification
   * d'un élément de la liste déroulante. Permet de réutiliser un évènement ajax
   * par exemple pour remplir une autre liste déroulante.
   * 
   * @param var
   *          le nom de la variable dont sera logé le traitement de modification
   *          d'un élément de la liste déroulante.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom de la variable dont sera logé le traitement de modification
   * d'un élément de la liste déroulante. Permet de réutiliser un évènement ajax
   * par exemple pour remplir une autre liste déroulante.
   * 
   * @return le nom de la variable dont sera logé le traitement de modification
   *         d'un élément de la liste déroulante.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer si la balise est transationnel et qu'elle doit appliquer l'évènement
   * de modification de formulaire.
   * 
   * @param transactionnel
   *          true si la balise est transationnel et qu'elle doit appliquer
   *          l'évènement de modification de formulaire.
   */
  public void setTransactionnel(String transactionnel) {
    this.transactionnel = transactionnel;
  }

  /**
   * Retourne true si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   * 
   * @return true si la balise est transationnel et qu'elle doit appliquer
   *         l'évènement de modification de formulaire.
   */
  public String getTransactionnel() {
    return transactionnel;
  }

  /**
   * Permet de présenter le libellé de ligne vide si la valeur du SelectTag en
   * affichage seulement est la valeur null ou vide.
   * 
   * @param afficherLibelleLigneVideEnAffichageSeulement
   */
  public void setLigneVideLibelleEnAffichageSeulement(
      String ligneVideLibelleEnAffichageSeulement) {
    this.ligneVideLibelleEnAffichageSeulement = ligneVideLibelleEnAffichageSeulement;
  }

  /**
   * Permet de présenter le libellé de ligne vide si la valeur du SelectTag en
   * affichage seulement est la valeur null ou vide.
   * 
   * @return true affiche le libelle, false sinon
   */
  public String getLigneVideLibelleEnAffichageSeulement() {
    return ligneVideLibelleEnAffichageSeulement;
  }

  /**
   * Retourne true si la balise est transationnel et qu'elle doit appliquer
   * l'évènement de modification de formulaire.
   * 
   * @return
   */
  public boolean isTraitementTransactionnel() {
    if ((getTransactionnel() != null)
        && getTransactionnel().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne la classe qui permet de décorer le contenu de la liste déroulante.
   * 
   * @return la classe qui permet de décorer le contenu de la liste déroulante.
   * @since SOFI 2.0.3
   */
  public String getClasseDecorator() {
    return classeDecorator;
  }

  /**
   * Fixer la classe qui permet de décorer le contenu de la liste déroulante.
   * 
   * @param classeDecorator
   *          la classe qui permet de décorer le contenu de la liste déroulante.
   * @since SOFI 2.0.3
   */
  public void setClasseDecorator(String classeDecorator) {
    this.classeDecorator = classeDecorator;
  }

  /**
   * Fixer true si vous désirez l'aide d'aide a gauche du libellé.
   * 
   * @param iconeAideAGauche
   *          true si vous désirez l'aide d'aide a gauche du libellé.
   */
  public void setIconeAideAGauche(String iconeAideAGauche) {
    this.iconeAideAGauche = iconeAideAGauche;
  }

  /**
   * Est-ce que l'icone d'aide doit être fixé a gauche du libellé.
   * 
   * @return true si l'icone d'aide doit être fixé a gauche du libellé.
   */
  public String getIconeAideAGauche() {
    return iconeAideAGauche;
  }

  public boolean isAffichageIconeAideAGauche() {
    return (getIconeAideAGauche() != null)
        && getIconeAideAGauche().toString().toLowerCase().equals("true");
  }

  /**
   * Afficher une icone d'aide lorsque aide contextuelle.
   * 
   * @param iconeAide
   *          true si afficher une icone d'aide lorsque aide contextuelle.
   */
  public void setIconeAide(String iconeAide) {
    this.iconeAide = iconeAide;
  }

  /**
   * Indique si afficher une icone d'aide lorsque aide contextuelle.
   * 
   * @return true si afficher une icone d'aide lorsque aide contextuelle.
   */
  public String getIconeAide() {
    return iconeAide;
  }

  public Object getSelectMultiChoixSource() {
    return selectMultiChoixSource;
  }

  public void setSelectMultiChoixSource(Object selectMultiChoixSource) {
    this.selectMultiChoixSource = selectMultiChoixSource;
  }

  public Object getSelectMultiChoixDestination() {
    return selectMultiChoixDestination;
  }

  public void setSelectMultiChoixDestination(Object selectMultiChoixDestination) {
    this.selectMultiChoixDestination = selectMultiChoixDestination;
  }

  private boolean isMultiChoixSource() {
    return selectMultiChoixSource != null
        && selectMultiChoixSource.toString().toLowerCase().equals("true");
  }

  private boolean isMultiChoixDestination() {
    return selectMultiChoixDestination != null
        && selectMultiChoixDestination.toString().toLowerCase().equals("true");
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  @Override
  public Object getSousCache() {
    return sousCache;
  }

  public void setFiltre(String filtre) {
    this.filtre = filtre;
  }

  public String getFiltre() {
    return filtre;
  }

  /**
   * Indique si afficher une icone d'aide lorsque aide contextuelle.
   * 
   * @return true si afficher une icone d'aide lorsque aide contextuelle.
   */
  private boolean isAffichageIconeAide() {
    return (getIconeAide() != null)
        && getIconeAide().toString().toLowerCase().equals("true");
  }

  /*
   * Permet de savoir s'il faut afficher de l'aide en ligne sur le champ.
   */

  private boolean isAideEnLigne(String aideContextuelle, PageContext pageContext) {
    return (UtilitaireBaliseJSP.isAideEnligne(aideContextuelle, pageContext) && (isAffichageIconeAide() || (getIconeAide() == null)));
  }

  @Override
  public String getNomCache() {
    return this.getCache();
  }

  @Override
  public Object getValeurCache() {
    return this.getValeurFormulaire();
  }

  public void setActiverCleDefaut(Object activerCleDefaut) {
    this.activerCleDefaut = activerCleDefaut;
  }

  public Object getActiverCleDefaut() {
    return activerCleDefaut;
  }

  public boolean isActifsSeulement() {
    return actifsSeulement;
  }

  public void setActifsSeulement(boolean actifsSeulement) {
    this.actifsSeulement = actifsSeulement;
  }

  /**
   * Décoration d'un objet pour représenter la valeur et l'étiquette d'un
   * élément de la liste des optionds de la balise Select.
   */
  class OptionDecore {

    /**
     * Objet qui doit être affiché comme une option.
     */
    private Object option;

    public OptionDecore(Object option) {
      this.option = option;
    }

    public String getEtiquette() {
      return decorator.getEtiquette(this.option);
    }

    public String getValeur() {
      return decorator.getValeur(this.option);
    }
  }

  public String getProprieteTitle() {
    return proprieteTitle;
  }

  public void setProprieteTitle(String proprieteTitle) {
    this.proprieteTitle = proprieteTitle;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * Retourne la propriété doit être valider pour définir un optgroup.
   * 
   * @return la propriété doit être valider pour définir un optgroup.
   */
  public String getProprieteOptGroup() {
    return proprieteOptGroup;
  }

  /**
   * Fixer la propriété doit être valider pour définir un optgroup.
   * 
   * @param proprieteOptGroup
   *          la propriété doit être valider pour définir un optgroup.
   */
  public void setProprieteOptGroup(String proprieteOptGroup) {
    this.proprieteOptGroup = proprieteOptGroup;
  }

  /**
   * @return the utiliserDescriptionLocale
   */
  public Object getUtiliserDescriptionLocale() {
    return utiliserDescriptionLocale;
  }
  
  /**
   * Spécifier si on désire utiliser la description locale.
   * <p>
   * 
   * @return Spécifier si on désire utiliser la description locale.
   */
  public boolean isAfficherDescriptionLocale() {
    if ((getUtiliserDescriptionLocale() != null)
        && getUtiliserDescriptionLocale().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param utiliserDescriptionLocale the utiliserDescriptionLocale to set
   */
  public void setUtiliserDescriptionLocale(Object utiliserDescriptionLocale) {
    this.utiliserDescriptionLocale = utiliserDescriptionLocale;
  }

}
