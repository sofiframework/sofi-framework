/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;


/**
 * Balise permettant d'inclure un composant d'edition de contenu riche en HTML.
 * Le composant utilise est CKEditor. Pour plus d'information sur le CKEditor
 * http://www.ckeditor.com
 *
 * @author jfbrassard
 * @version 3.0
 */
public class CKEditorTag extends TextareaTag {


  /**
   * 
   */
  private static final long serialVersionUID = -9112242866337334189L;

  /** Spécifie le langage de l'éditeur. */
  private String langage = "fr_ca";

  /** Spécifie les boutons à utiliser avec le composants*/
  private String boutonsMenu = "['Source','-','Bold', 'Italic', '-', 'NumberedList', 'BulletedList','-','Outdent','Indent', '-', 'Link', 'Unlink', '-','TextColor','BGColor']";

  /** Gabarit Velocity a utiliser **/
  private String gabaritVelocity = "/org/sofiframework/presentation/velocity/gabarit/ckeditor.vm";

  /**
   * Fixer le langage de l'éditeur.
   * <p>
   * @param langage Le langage de l'éditeur
   */
  public void setLangage(String langage) {
    this.langage = langage;
  }

  /**
   * Obtenir le langage de l'éditeur.
   * <p>
   * @return Le langage de l'éditeur
   */
  public String getLangage() {
    return langage;
  }


  /**
   * Fixer les boutons de la ligne 1 de la barre de menu.
   * <p>
   * @param boutonsMenuLigne1 Les boutons de la première ligne de la barre de menu
   */
  public void setBoutonsMenu(String boutons) {
    this.boutonsMenu = boutons;
  }

  /**
   * Obtenir les boutons de la barre de menu.
   * <p>
   * @return les boutons de la barre de menu
   */
  public String getBoutonsMenu() {
    return boutonsMenu;
  }



  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un objet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  @Override
  protected String genererCodeHtmlComposant() throws JspException {

    setStyleClass("jquery_ckeditor " + getProperty());

    if (getReadonly()) {
      setAffichageSeulement(Boolean.TRUE);
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
        property);

    StringBuffer resultat = new StringBuffer();

    UtilitaireVelocity velocity = new UtilitaireVelocity();
    velocity.ajouterAuContexte("formulaireEnTraitement", nomFormulaireJS);
    velocity.ajouterAuContexte("boutonsMenu", getBoutonsMenu());

    resultat.append(velocity.generer(getGabaritVelocity()));

    // Générer le code Html du textarea
    resultat.append(super.genererCodeHtmlComposant());

    return resultat.toString();
  }

  /**
   * @param gabaritVelocity the gabaritVelocity to set
   */
  public void setGabaritVelocity(String gabaritVelocity) {
    this.gabaritVelocity = gabaritVelocity;
  }

  /**
   * @return the gabaritVelocity
   */
  public String getGabaritVelocity() {
    return gabaritVelocity;
  }

  @Override
  public void release() {
    super.release();
  }

}
