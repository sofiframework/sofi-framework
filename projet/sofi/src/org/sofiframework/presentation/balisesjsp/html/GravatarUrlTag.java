/**
 * 
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.securite.encryption.EncryptionException;
import org.sofiframework.securite.encryption.UtilitaireEncryption;

/**
 * @author remi.mercier
 * 
 */
public class GravatarUrlTag extends TagSupport {

  private static final String GRAVATAR_HTTP = "http://www.gravatar.com/avatar/";
  private static final String GRAVATAR_HTTPS = "https://secure.gravatar.com/avatar/";

  private String courriel;
  private String var;
  private boolean https = false;
  private Integer size;

  /**
   * 
   */
  private static final long serialVersionUID = 3101191923071920173L;

  @Override
  public void release() {
    courriel = null;
    var = null;
    https = false;
    super.release();
  }

  @Override
  public int doStartTag() throws JspException {
    if (StringUtils.isNotEmpty(courriel)) {
      String hash = null;
      try {
        hash = UtilitaireEncryption.hashMD5ToHex(courriel);
      } catch (EncryptionException e) {
        throw new JspException(e);
      }

      String url = null;
      try {
        url = construireUrl(hash);
      } catch (UnsupportedEncodingException e1) {
        throw new JspException(e1);
      }

      try {
        if (testUrl(url)) {
          if (StringUtils.isNotEmpty(var)) {
            pageContext.setAttribute(var, url);
          } else {
            try {
              pageContext.getOut().write(url);
            } catch (IOException e) {
              throw new JspException(e);
            }
          }
        }
      } catch (IOException e) {
        throw new JspException(e);
      }
    }
    return EVAL_PAGE;
  }

  private String construireUrl(String hash) throws UnsupportedEncodingException {
    StringBuilder sb = new StringBuilder();
    if (https) {
      sb.append(GRAVATAR_HTTPS);
    } else {
      sb.append(GRAVATAR_HTTP);
    }

    sb.append(hash);
    sb.append("?d=404");

    if (size != null) {
      sb.append("&s=").append(size);
    }

    return sb.toString();
  }

  private boolean testUrl(String path) throws IOException {
    boolean testOk = false;
    URL u = new URL(path);
    HttpURLConnection conn = (HttpURLConnection) u.openConnection();
    conn.setRequestMethod("HEAD");
    testOk = conn.getResponseCode() == 200;
    return testOk;
  }

  public String getCourriel() {
    return courriel;
  }

  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  public String getVar() {
    return var;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public boolean getHttps() {
    return https;
  }

  public void setHttps(boolean https) {
    this.https = https;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

}
