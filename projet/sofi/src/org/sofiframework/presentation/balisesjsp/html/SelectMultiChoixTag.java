/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.presentation.balisesjsp.base.BaseELTag;
import org.sofiframework.utilitaire.UtilitaireString;

public class SelectMultiChoixTag extends BaseELTag {

  private static final long serialVersionUID = 5188248370143855114L;

  private String libelleChampSource = null;
  private String nomChampSource = null;
  private String nomChampDestination = null;
  private String libelleChampDestination = null;
  private String width = null;
  private String heigth = null;

  public static final String NOM_ATTRIBUT_SELECT_MULTI_CHOIX_SOURCE = "selectMultiChoixSource";
  public static final String NOM_ATTRIBUT_SELECT_MULTI_CHOIX_DESTINATION = "selectMultiChoixDestination";

  @Override
  public int doStartTag() throws JspException {
    /**
     * On doit donner les nom des champs qui participent au picklist aux deux
     * select.
     */
    pageContext.setAttribute(
        NOM_ATTRIBUT_SELECT_MULTI_CHOIX_DESTINATION + nomChampSource, nomChampDestination);
    pageContext.setAttribute(
        NOM_ATTRIBUT_SELECT_MULTI_CHOIX_SOURCE + nomChampDestination, nomChampSource);

    // Évaluer les balises JSP incluses
    return (EVAL_BODY_INCLUDE);
  }

  @Override
  public int doEndTag() throws JspException {
    Libelle libelleSource =
        UtilitaireLibelle.getLibelle(this.getLibelleChampSource(), pageContext, null);
    Libelle libelleDestination =
        UtilitaireLibelle.getLibelle(this.getLibelleChampDestination(), pageContext, null);

    // Ajouter le js de création du picklist au onload de la page
    this.getOnload()
    .append("createMovableOptions('")
    .append(nomChampSource).append("','")
    .append(nomChampDestination).append("', ")
    .append(width).append(", ")
    .append(heigth).append(", '")
    .append(UtilitaireString.remplacerTous(libelleSource.getMessage(), "'", "\\\\'")).append("', '")
    .append(UtilitaireString.remplacerTous(libelleDestination.getMessage(), "'", "\\\\'")).append("'); ");

    // Ajouter le js de création du picklist au onsubmit du formulaire
    StringBuffer onsubmit = this.getOnClick();

    if (onsubmit.indexOf("multipleSelectOnSubmit") == -1) {
      onsubmit.append("multipleSelectOnSubmit();");
    }

    // Nettoyer les variables
    pageContext.removeAttribute(
        NOM_ATTRIBUT_SELECT_MULTI_CHOIX_DESTINATION + nomChampSource);
    pageContext.removeAttribute(
        NOM_ATTRIBUT_SELECT_MULTI_CHOIX_SOURCE + nomChampDestination);

    // Toujours évaluer le reste de la page
    return (EVAL_PAGE);
  }

  /**
   * Obtenir le onload
   */
  private StringBuffer getOnload() {
    return this.getStringBufferRequest(
        ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE,
        pageContext);
  }

  /**
   * Obtenir le onsubmit
   */
  private StringBuffer getOnClick() {
    return this.getStringBufferRequest(
        ConstantesBaliseJSP.FONCTION_ONCLICK_PERSONALISE,
        pageContext);
  }

  /**
   * Obtenir une variable StringBuffer de la request, si elle n'existe pas la créer
   * @param nom
   * @param pageContext
   * @return
   */
  private StringBuffer getStringBufferRequest(String nom, PageContext pageContext) {
    StringBuffer buffer = (StringBuffer) pageContext.getRequest().getAttribute(nom);

    /*
     * Créer le buffer si il n'existe pas.
     */
    if (buffer == null) {
      buffer = new StringBuffer();
      pageContext.getRequest().setAttribute(nom, buffer);
    }

    return buffer;
  }

  public String getLibelleChampSource() {
    return libelleChampSource;
  }

  public void setLibelleChampSource(String libelleChampSource) {
    this.libelleChampSource = libelleChampSource;
  }

  public String getNomChampSource() {
    return nomChampSource;
  }

  public void setNomChampSource(String nomChampSource) {
    this.nomChampSource = nomChampSource;
  }

  public String getNomChampDestination() {
    return nomChampDestination;
  }

  public void setNomChampDestination(String nomChampDestination) {
    this.nomChampDestination = nomChampDestination;
  }

  public String getLibelleChampDestination() {
    return libelleChampDestination;
  }

  public void setLibelleChampDestination(String libelleChampDestination) {
    this.libelleChampDestination = libelleChampDestination;
  }

  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
  }

  public String getHeigth() {
    return heigth;
  }

  public void setHeigth(String heigth) {
    this.heigth = heigth;
  }
}
