/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * Balise JSP permetttant la création d'un champ de formulaire caché.
 *
 * @author Jean-François Brassard
 * @version SOFI 1.3
 */
public class HiddenTag extends org.apache.struts.taglib.html.HiddenTag {
  /**
   * 
   */
  private static final long serialVersionUID = -8796995661314563584L;

  /** Constructeur pour cette balise */
  public HiddenTag() {
    super();
    this.type = "hidden";
  }

  /**
   * Génération d'un début du lien hypertexte.
   * @exception JspException si une exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    // Accès au formulaire
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    if (!UtilitaireBaliseJSP.isFormulaireEnLectureSeulement(pageContext,
        getProperty()) && !baseForm.isModeAffichageSeulement()) {
      // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
      baseForm.ajouterAttributATraiter(getProperty());
    }

    if (getStyleId() == null) {
      setStyleId(getProperty());
    }

    // Traiter le champ de saisie par struts
    super.doStartTag();

    // S'il y a une valeur séparé de spécifié
    if (!write) {
      return (EVAL_BODY_BUFFERED);
    }

    // Calculer la valeur a être affiché séparément.
    // * @since Struts 1.1
    String results = null;

    if (value != null) {
      results = TagUtils.getInstance().filter(value);
    } else {
      Object value =
          TagUtils.getInstance().lookup(pageContext, name, property, null);

      if (value == null) {
        results = "";
      } else {
        results = TagUtils.getInstance().filter(value.toString());
      }
    }

    TagUtils.getInstance().write(pageContext, results);

    return (EVAL_BODY_BUFFERED);
  }

  /**
   * Traiter la fermeture de la balise.
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    release();

    return (EVAL_PAGE);
  }

  /**
   * Préparer l'affichage de la valeur du champ hidden
   *
   * @param results le résultat à afficher
   */
  @Override
  protected void prepareValue(StringBuffer results) throws JspException {
    // Accès au formulaire
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    results.append(" value=\"");

    if (value != null) {
      results.append(this.formatValue(value));
    } else {
      Object value = UtilitaireObjet.getValeurAttribut(baseForm, getProperty());

      results.append(this.formatValue(value));
    }

    results.append('"');
  }

  /**
   * Libérer les ressouces.
   */
  @Override
  public void release() {
    super.release();
  }

  // Laisse la classe parent gérer la fermeture de la balise selon la valeur
  // de l'attribut xhtml de la balise <html:html> de struts-html
  //  protected String getElementClose() {
  //    return "/>";
  //  }

  public void evaluerEL() throws JspException {
    if (getStyleId() != null) {
      String id =
          EvaluateurExpression.evaluerString("styleId", getStyleId(), this,
              pageContext);
      setStyleId(id);
    }

    if (getValue() != null) {
      String value =
          EvaluateurExpression.evaluerString("value", getValue(), this,
              pageContext);
      setValue(value);
    }
  }
}
