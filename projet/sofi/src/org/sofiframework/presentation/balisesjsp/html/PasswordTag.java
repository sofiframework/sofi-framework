/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;



/**
 * PasswordTag permet d'afficher un champ texte de type mot de passe.
 * <p>
 * Le champ de saisie ne sera pas visible en texte clair.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class PasswordTag extends BaseFieldTag {
  /**
   * 
   */
  private static final long serialVersionUID = -5161844249803854106L;

  /** Constructeur pour cette balise */
  public PasswordTag() {
    super();
    this.type = "password";
  }
}
