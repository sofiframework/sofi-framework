/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.base.TableTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * TextareaTag permet d'afficher un champ texte en plus de traiter
 * automatiquement si l'utilisateur fait une modification sur ce champ texte.
 * <p>
 * De plus, si le formulaire est en mode de lecture seulement le champ devient
 * en lecture seulement.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class TextareaTag extends org.apache.struts.taglib.html.TextareaTag {
  /**
   * 
   */
  private static final long serialVersionUID = 5360806346291072828L;

  /** Variable qui identifie le style à utiliser pour les champs de saisie qui sont en erreur */
  public static final String STYLE_SAISIE_ERREUR = "saisieErreur";

  /** Variable qui identifie le style à utiliser pour les libellés des champs en erreur */
  public static final String STYLE_LIBELLE_ERREUR = "libelleErreur";

  /** Spécifie la classe css pour un attribut en lecture seulement */
  protected String styleClassLectureSeulement = null;

  /** Spécifie la classe css pour un attribut sélectionné. */
  protected String styleClassFocus = null;

  /** Spécifie le libellé */
  protected String libelle = null;

  /** Spécifie si le champ de saisie est obligatoire */
  protected Object obligatoire = null;

  /**
   * Spécifie si on désire ouvrir une nouvelle ligne.
   * L'ouverture de la balise tr est ajoutée automatiquement si la propriété n'est pas ajoutée au tag.
   */
  protected boolean ouvrirLigne = true;

  /**
   * Spécifie si on désire fermer la ligne automatiquement.
   * La balise &gt;tr&lt; s'ajoute automatiquement après le composant d'interface.
   */
  protected boolean fermerLigne = true;

  /**
   * Style sur une ligne d'un tableau.
   */
  protected String ligneStyleClass = null;

  /** Spécifie le nom de l'attribut qui indique le nombre de caractère
   * encore disponible à la zone de texte.
   */
  protected String nomAttributCaractereRestant = null;

  /**
   * Permet d'inactiver un champ de saisie avec l'aide des expressions EL.
   */
  protected String disabledEL = null;

  /**
   * Permet de mettre en lecture seulement avec l'aide des expressions EL.
   */
  protected String readonlyEL = null;

  /** Ajout d'un icone aide avec aide contextuelle **/
  protected String iconeAide = null;

  /**
   * Spécifie si vous désirez que l'icone d'aide soit position à gauche du libellé.
   * @since SOFI 2.1
   */
  private String iconeAideAGauche = null;

  /** Affichage de l'indicateur obligatoire du champ de saisie. **/
  protected Object affichageIndObligatoire = null;

  /** Afficher seulemenet la valeur du formulaire et non le champ de saisie **/
  protected Object affichageSeulement = null;

  /** Permet d'inscrire un libelle d'aide directement dans le champs. Fonctionnalité HTML 5 **/
  private String placeholder = null;

  /**
   * Obtenir le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @return le style à utiliser dans le cas où le champ possède le focus
   */
  public String getStyleClassFocus() {
    return (this.styleClassFocus);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ possède le focus.
   * <p>
   * @param styleClassFocus le style à utiliser dans le cas où le champ possède le focus
   */
  public void setStyleClassFocus(String styleClassFocus) {
    this.styleClassFocus = styleClassFocus;
  }

  /**
   * Obtenir le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @return le style à utiliser dans le cas où le champ est en lecture seule
   */
  public String getStyleClassLectureSeulement() {
    return (this.styleClassLectureSeulement);
  }

  /**
   * Fixer le style à utiliser dans le cas où le champ est en lecture seule.
   * <p>
   * @param styleClassLectureSeulement le style à utiliser dans le cas où le champ est en lecture seule
   */
  public void setStyleClassLectureSeulement(String styleClassLectureSeulement) {
    this.styleClassLectureSeulement = styleClassLectureSeulement;
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @param ouvrirLigne la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public void setOuvrirLigne(boolean ouvrirLigne) {
    this.ouvrirLigne = ouvrirLigne;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une ouverture de balise tr avant le champ
   */
  public boolean getOuvrirLigne() {
    return ouvrirLigne;
  }

  /**
   * Obtenir la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @return la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public boolean getFermerLigne() {
    return (this.fermerLigne);
  }

  /**
   * Fixer la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ.
   * <p>
   * @param fermerLigne la valeur qui indique si on doit ajouter une fermeture de balise tr après le champ
   */
  public void setFermerLigne(boolean fermerLigne) {
    this.fermerLigne = fermerLigne;
  }

  /**
   * Obtenir le libellé associé au champ.
   * <p>
   * @return le libellé associé au champ
   */
  public String getLibelle() {
    return (this.libelle);
  }

  /**
   * Fixer le libellé associé au champ.
   * <p>
   * @param libelle le libellé associé au champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Obtenir la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @return la valeur qui indique si le champ est obligatoire ou pas
   */
  public Object getObligatoire() {
    return (this.obligatoire);
  }

  /**
   * Fixer la valeur qui indique si le champ est obligatoire ou pas.
   * <p>
   * @param obligatoire la valeur qui indique si le champ est obligatoire ou pas
   */
  public void setObligatoire(Object obligatoire) {
    this.obligatoire = obligatoire;
  }

  /**
   * Execute le début de la balise.
   * <p>
   * @throws JspException si un exception JSP est lancé.
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    if (getLibelle() != null) {
      TableTag.gererOuvertureEtFermeturePourBalise(this, pageContext);
    }

    /**
     * Si paramètre la propriété iconeAide est null et qu'il existe un paramètre système,
     * alors prendre la valeur de celui-ci.
     * @since SOFI 2.0.3
     */
    if (this.iconeAide == null) {
      Boolean iconeAideDefaut =
          GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_ICONE_AIDE);

      if (iconeAideDefaut != null) {
        setIconeAide(iconeAideDefaut.toString());
      }
    }

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      // Évaluer les expressions régulières (EL)
      evaluerEL();
    }

    if (getDisabled()) {
      setObligatoire("false");
    }

    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext,
            getProperty());

    if ((formulaire != null) && (nomFormulaireImbrique != null)) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    String codeHtml = null;

    // Vérifier la sécurité lié au bouton
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();

    if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
      Utilisateur utilisateur =
          UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
      // L'utilisateur à droit de voir le composant
      if (gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
          this.libelle)) {

        // L'utilisateur a droit au composant en lecture seulement
        if (gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
            this.libelle)) {
          this.setReadonly(true);
        } else {
          BaseForm baseForm =
              UtilitaireBaliseJSP.getBaseForm(this.pageContext);
          boolean blocSecurite =
              (this.pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null)
              || ((baseForm != null) && baseForm.isModeLectureSeulement());
          boolean isLectureSeulement = UtilitaireString.isVide(getDisabledEL())
              && UtilitaireString.isVide(getReadonlyEL()) && blocSecurite;
          if (isLectureSeulement) {
            this.setReadonly(true);
          }
        }

        // Générer le code représentant le composant HTML
        codeHtml = this.genererCodeHtmlComposant();
      }
    } else {
      // Si on a pas de composant de sécurité, alors on ne la traite pas et on
      // affiche le composant comme le développeur la programme
      codeHtml = this.genererCodeHtmlComposant();
    }

    // Écrire le champ dans la réponse.
    if (codeHtml != null) {
      TagUtils.getInstance().write(this.pageContext, codeHtml);
    }

    this.release();

    return (EVAL_BODY_BUFFERED);
  }

  /**
   * Méthode qui sert à générer le code HTML qui servira à afficher le composant.
   * <p>
   * @return un onjet String qui contient le code HTML à afficher dans la page pour
   * obtenir ce composant
   * @throws JspException une erreur lors du traitement de la balise
   */
  protected String genererCodeHtmlComposant() throws JspException {
    // Accès au formulaire
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    Libelle libelle = UtilitaireLibelle.getLibelle(getLibelle(), this.pageContext, null);

    if (libelle.getPlaceholder() != null && StringUtils.isEmpty(getPlaceholder())) {
      setPlaceholder(libelle.getPlaceholder());
    }

    boolean affichageSeulement =
        (UtilitaireBaliseJSP.isFormulaireCourantEnAffichageSeulement(pageContext,
            getProperty()) || isAffichageSeulement());
    ;
    if (affichageSeulement) {
      this.setObligatoire("false");
    } else {
      if (!getDisabled()) {
        // Ajouter l'attribut à traiter si le formulaire traite seulement les attributs de la JSP.
        formulaire.ajouterAttributATraiter(getProperty());
      }
    }

    // Prendre les paramètres de base pour la majorité des balises
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    String nomFormulaireJS =
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            property);

    String nomFormulaireImbrique =
        UtilitaireBaliseNested.getReferenceFormulaireImbrique(pageContext,
            getProperty());

    if (isObligatoire()) {
      formulaire.ajouterAttributObligatoire(nomFormulaireImbrique,
          getProperty());
    }

    MessageErreur erreur =
        UtilitaireBaliseJSP.getMessageErreur(formulaire, this.property,
            this.pageContext);

    // Permet de verifier si le message d'erreur de saisie
    // doit être positionné en bas du champ. False est la valeur par défaut et signifie que
    // les messages apparaîtront en nicetitle au survol du champ.
    Boolean estPositioneeHautChamp =
        GestionParametreSysteme.getInstance().getBoolean(Constantes.POSITION_MESSAGE_ERREUR_SAISIE);

    if (estPositioneeHautChamp == null) {
      estPositioneeHautChamp = Boolean.FALSE;
    }

    String titre = null;

    StringBuffer resultat = new StringBuffer("");

    String styleLibelle = null;

    if (erreur != null) {
      // On obtient le message d'erreur
      titre = erreur.getMessage();
    }

    this.setStyleClass(getStyleClass());
    this.setStyleClassFocus(getStyleClassFocus());


    // Appliquer le maximum de la zone de texte spécifie.
    if (getMaxlength() != null) {
      StringBuffer limiteChampTexte = new StringBuffer();

      if (!UtilitaireString.isVide(getOnkeyup())) {
        limiteChampTexte.append(getOnkeyup());
      }

      if (getNomAttributCaractereRestant() != null) {
        limiteChampTexte.append("limiterChampTexteAvecCompteur(this,this.form.");
        limiteChampTexte.append(getNomAttributCaractereRestant());
        limiteChampTexte.append(",");
        limiteChampTexte.append(getMaxlength());
        limiteChampTexte.append(");");
      } else {
        limiteChampTexte.append("limiterChampTexte(this,");
        limiteChampTexte.append(getMaxlength());
        limiteChampTexte.append(");");
      }

      setOnkeyup(limiteChampTexte.toString());
    }

    if (erreur != null) {
      if (getStyleClass() != null) {
        this.setStyleClass(STYLE_SAISIE_ERREUR + " " + getStyleClass());
      } else {
        this.setStyleClass(STYLE_SAISIE_ERREUR);
      }

      this.setStyleClassFocus(STYLE_SAISIE_ERREUR);
      styleLibelle = STYLE_LIBELLE_ERREUR;
      titre = erreur.getMessage();
    } else {
      libelle =
          UtilitaireLibelle.getLibelle(this.getTitle(), pageContext, null);
      titre = libelle.getMessage();
    }

    String titreErreur = null;

    if (erreur != null) {
      if (!GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_SUR_LIBELLE_ERREUR).booleanValue()) {
        setTitle("");
      }

      titreErreur = titre;
    }

    // Pour que le titre du champ de saisie soit convertie en texte du gestion message
    if ((titre != null) && (estPositioneeHautChamp.booleanValue() == false)) {
      this.setTitle(titre);
    }

    // Générer le libéllé.
    if (getLibelle() != null) {
      if (getOuvrirLigne()) {
        resultat.append("<tr");

        if (getLigneStyleClass() != null) {
          resultat.append(" class=\"");
          resultat.append(getLigneStyleClass());
          resultat.append("\" ");
        }

        resultat.append(">");
      }

      // Ajouter le bout de code qui affiche le libellé
      if (affichageSeulement) {
        resultat.append("<th scope=\"row\" class=\"libelle_formulaire\">");
        setObligatoire("false");
      } else {
        resultat.append("<th scope=\"row\">");
      }

      if (isAffichageIconeAideAGauche() &&
          (isAffichageIconeAide() || isAideEnLigne(getLibelle(),
              pageContext))) {
        UtilitaireBaliseJSP.genererIcodeAide(getLibelle(), getProperty(),
            resultat, pageContext);
        resultat.append("&nbsp;");
      }

      if (nomFormulaireImbrique != null) {
        UtilitaireBaliseJSP.genererLibelle(nomFormulaireImbrique,
            this.property, -1, getTitle(), getLibelle(), styleLibelle,
            isObligatoire(), isAffichageIndObligatoire(), pageContext,
            resultat);
      } else {
        UtilitaireBaliseJSP.genererLibelle(this.property, getTitle(),
            this.libelle, styleLibelle, isObligatoire(),
            isAffichageIndObligatoire(), this.pageContext, resultat);
      }

      resultat.append("</th><td>");
    } else {
      if (isObligatoire()) {
        UtilitaireBaliseJSP.ajouterAttributObligatoire(nomFormulaireImbrique,
            getProperty(), pageContext);
      }
    }

    setTitle(titreErreur);

    if (!affichageSeulement) {
      resultat.append("<textarea");

      resultat.append(" name=\"");

      // @since Struts 1.1
      if (indexed) {
        prepareIndex(resultat, name);
      }

      resultat.append(property);
      resultat.append("\"");

      if (accesskey != null) {
        resultat.append(" accesskey=\"");
        resultat.append(accesskey);
        resultat.append("\"");
      }

      if (tabindex != null) {
        resultat.append(" tabindex=\"");
        resultat.append(tabindex);
        resultat.append("\"");
      }

      if (cols != null) {
        resultat.append(" cols=\"");
        resultat.append(cols);
        resultat.append("\"");
      }

      if (rows != null) {
        resultat.append(" rows=\"");
        resultat.append(rows);
        resultat.append("\"");
      }

      if (getStyleId() == null) {
        resultat.append(" id=\"");
        resultat.append(this.property);
        resultat.append("\"");
      }

      if (StringUtils.isNotBlank(getPlaceholder())) {
        resultat.append(" placeholder=\"");
        Libelle libellePlaceHolder =
            UtilitaireLibelle.getLibelle(this.placeholder, pageContext, null);
        resultat.append(libellePlaceHolder.getMessage());
        resultat.append("\"");
      }

      resultat =
          UtilitaireBaliseJSP.specifierGestionModification(pageContext, nomFormulaire,
              nomFormulaireJS, getOnchange(), resultat);

      if (pageContext.getRequest().getAttribute(Constantes.IND_MODIFICATION_FORMULAIRE) !=
          null) {
        setOnchange(null);
      }

      StringBuffer fonctionJsWord = new StringBuffer();
      fonctionJsWord.append("traiterWord('");
      fonctionJsWord.append(this.property);
      fonctionJsWord.append("');");

      if (getOnblur() != null) {
        setOnblur(getOnblur() + fonctionJsWord.toString());
      } else {
        setOnblur(fonctionJsWord.toString());
      }

      // Faire la gestion du mode lecture seulement
      if (this.getDisabled() || this.getReadonly()) {
        if (!GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.READONLY_DISABLED_DISTINCT).booleanValue()) {
          setDisabled(false);
          setReadonly(true);
        }

        UtilitaireBaliseJSP.specifierLectureSeulement(this.styleClassLectureSeulement,
            getStyleClassFocus(), resultat);
      } else {
        UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
            getStyleClassFocus(), getOnfocus(), getOnblur(), resultat);
      }

      setOnblur(null);

      resultat.append(prepareEventHandlers());
      resultat.append(prepareStyles());
      resultat.append(">");

      resultat.append(renderData());

      resultat.append("</textarea>");
    } else {
      Object valeur = getValue();

      if (getProperty() != null) {
        try {
          valeur =
              TagUtils.getInstance().lookup(this.pageContext, this.name, this.property,
                  null);
        } catch (Exception e) {
          valeur =
              UtilitaireObjet.getValeurAttribut(formulaire, getProperty());
        }
      }

      if (valeur != null) {
        String texte = StringEscapeUtils.escapeHtml(valeur.toString());
        texte = UtilitaireString.remplacerTous(texte, "&lt;br/&gt;", "<br/>");
        texte =
            UtilitaireString.modifierRetourChariotEnChangementLigneWeb(texte);
        resultat.append(texte);
      }
    }

    if (erreur != null) {
      // Si true, on place le message d'erreur en bas de la balise
      if (estPositioneeHautChamp.booleanValue()) {
        resultat.append("<span class=\"libelleErreur\">" + titre + "</span>");
      }
    }

    if (isAffichageIconeAideAGauche() &&
        (isAffichageIconeAide() || isAideEnLigne(getLibelle(),
            pageContext))) {
      UtilitaireBaliseJSP.genererIcodeAide(getLibelle(), getProperty(),
          resultat, pageContext);
    }

    // Fermeture de la colonne si le libellé est spécifié.
    if (getLibelle() != null) {
      resultat.append("</td>");

      // Fermeture de la ligne
      if (getFermerLigne()) {
        resultat.append("</tr>");
      }
    }

    return resultat.toString();
  }

  public void setLigneStyleClass(String ligneStyleClass) {
    this.ligneStyleClass = ligneStyleClass;
  }

  public String getLigneStyleClass() {
    return ligneStyleClass;
  }

  public void setNomAttributCaractereRestant(String nomAttributCaractereRestant) {
    this.nomAttributCaractereRestant = nomAttributCaractereRestant;
  }

  public String getNomAttributCaractereRestant() {
    return nomAttributCaractereRestant;
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    if (getDisabledEL() != null) {
      boolean disabled =
          EvaluateurExpression.evaluerBoolean("disabledEL", getDisabledEL(),
              this, pageContext);
      setDisabled(disabled);
    }

    if (getReadonlyEL() != null) {
      boolean readonly =
          EvaluateurExpression.evaluerBoolean("readonlyEL", String.valueOf(getReadonlyEL()),
              this, pageContext);
      setReadonly(readonly);
    }

    if (getAffichageIndObligatoire() != null) {
      String affichageObligatoire =
          EvaluateurExpression.evaluerString("affichageIndObligatoire",
              String.valueOf(getAffichageIndObligatoire()), this, pageContext);
      setAffichageIndObligatoire(affichageObligatoire);
    }

    if (getStyleId() != null) {
      String styleId =
          EvaluateurExpression.evaluerString("styleId", String.valueOf(getStyleId()),
              this, pageContext);
      setStyleId(styleId);
    }

    if (getProperty() != null) {
      String property =
          EvaluateurExpression.evaluerString("property", getProperty(), this,
              pageContext);

      setProperty(property);
    }

    if (getAffichageSeulement() != null) {
      String affichageSeulement =
          EvaluateurExpression.evaluerString("affichageSeulement",
              (String)getAffichageSeulement(), this, pageContext);

      setAffichageSeulement(affichageSeulement.toLowerCase());
    }

    if (getValue() != null) {
      String valeur =
          EvaluateurExpression.evaluerString("value", getValue(), this,
              pageContext);

      setValue(valeur);
    }

    if (getLibelle() != null) {
      String libelle =
          EvaluateurExpression.evaluerString("libelle", getLibelle(),
              this, pageContext);

      setLibelle(libelle);
    }

    if (getObligatoire() != null) {
      String obligatoire =
          EvaluateurExpression.evaluerString("obligatoire", (String)getObligatoire(),
              this, pageContext);

      setObligatoire(obligatoire);
    }
  }

  @Override
  protected String renderData() throws JspException {
    String data = this.value;

    if (data == null) {
      try {
        data = this.lookupProperty(this.name, this.property);
      } catch (Exception e) {
        BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);
        data =
            (String)UtilitaireObjet.getValeurAttribut(baseForm, getProperty());
      }
    }

    return (data == null) ? "" : TagUtils.getInstance().filter(data);
  }

  /**
   * Méthode qui sert à libérer les ressources sytème utilisées
   */
  @Override
  public void release() {
    super.release();
    setStyleClassFocus(null);
    setStyleClass(null);
    setOnkeyup(null);
    setOnkeydown(null);
    setPlaceholder(null);
    setAffichageSeulement(null);
    setObligatoire(false);
    setAffichageIndObligatoire(null);
    setIconeAide(null);
    setFermerLigne(true);
    setLibelle(null);
    setDisabledEL(null);
    setReadonlyEL(null);
  }

  public void setDisabledEL(String disabledEL) {
    this.disabledEL = disabledEL;
  }

  public String getDisabledEL() {
    return disabledEL;
  }

  public void setReadonlyEL(String readonlyEL) {
    this.readonlyEL = readonlyEL;
  }

  public String getReadonlyEL() {
    return readonlyEL;
  }

  public void setIconeAide(String iconeAide) {
    this.iconeAide = iconeAide;
  }

  public String getIconeAide() {
    return iconeAide;
  }

  private boolean isAffichageIconeAide() {
    return (getIconeAide() != null) &&
        getIconeAide().toString().toLowerCase().equals("true");
  }

  /**
   * Fixer true si vous désirez l'aide d'aide a gauche du libellé.
   * @param iconeAideAGauche true si vous désirez l'aide d'aide a gauche du libellé.
   */
  public void setIconeAideAGauche(String iconeAideAGauche) {
    this.iconeAideAGauche = iconeAideAGauche;
  }

  /**
   * Est-ce que l'icone d'aide doit être fixé a gauche du libellé.
   * @return true si l'icone d'aide doit être fixé a gauche du libellé.
   */
  public String getIconeAideAGauche() {
    return iconeAideAGauche;
  }

  public boolean isAffichageIconeAideAGauche() {
    return (getIconeAideAGauche() != null) &&
        getIconeAideAGauche().toString().toLowerCase().equals("true");
  }

  /**
   * Fixer true si on veut l'affichage de l'indicateur obligatoire.
   * @param affichageIndObligatoire true si on veut l'affichage de l'indicateur obligatoire.
   */
  public void setAffichageIndObligatoire(Object affichageIndObligatoire) {
    this.affichageIndObligatoire = affichageIndObligatoire;
  }

  /**
   * Retourne true si on veut l'affichage de l'indicateur obligatoire.
   * @return true si on veut l'affichage de l'indicateur obligatoire.
   */
  public Object getAffichageIndObligatoire() {
    return affichageIndObligatoire;
  }

  /**
   * Est-ce que l'affichage de l'indicateur obligatoire est requis.
   */
  private boolean isAffichageIndObligatoire() {
    if ((getAffichageIndObligatoire() != null) &&
        getAffichageIndObligatoire().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setAffichageSeulement(Object affichageSeulement) {
    this.affichageSeulement = affichageSeulement;
  }

  public Object getAffichageSeulement() {
    return affichageSeulement;
  }

  /**
   * Est-ce que un mode d'affichage seulement.
   */
  private boolean isAffichageSeulement() {
    if ((getAffichageSeulement() != null) &&
        getAffichageSeulement().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  private boolean isObligatoire() {
    if ((getObligatoire() != null) &&
        getObligatoire().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /*
   * Permet de savoir s'il faut afficher de l'aide en ligne sur le champ.
   */

  private boolean isAideEnLigne(String aideContextuelle,
      PageContext pageContext) {
    return (UtilitaireBaliseJSP.isAideEnligne(aideContextuelle, pageContext) &&
        (isAffichageIconeAide() || (getIconeAide() == null)));
  }

  /**
   * @param placeholder the placeholder to set
   */
  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  /**
   * @return the placeholder
   */
  public String getPlaceholder() {
    return placeholder;
  }
}
