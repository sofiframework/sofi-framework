/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.struts.Globals;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.struts.form.SommaireValidationForm;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise JSP encapsulant toute la logique de gestion d'un formulaire.
 * <p>
 * @author Jean-François (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormTag extends org.apache.struts.taglib.html.FormTag {
  /**
   * 
   */
  private static final long serialVersionUID = -4361603081638251392L;

  /** Spécifie si les modifications faites au formulaire doivent être indiqué à l'utilisateur */
  protected boolean transactionnel = false;

  /** Appel de fonction javascript lors du onload de la page **/
  protected String onload = null;

  /**
   * Le focus est fait automatiquement sur le premier champ du formulaire disponible.
   */
  private Object focusAutomatique = null;

  /**
   * La clé du message d'avertisssement qui sera affiché lorsque le
   * formulaire transactionnel a été modifié et qu'un autre lien de menu
   * est sélectionné.
   */
  private String cleMessageAvertisementFormulaireModifie = null;

  /**
   * Spécifie si on doit permettre l'auto complétion des formulaires.
   * Fonctionnalité offerte par les différents navigateurs.
   */
  private boolean autocomplete = false;

  /**
   * La clé du message d'erreur général à utiliser pour ce formulaire.
   * @since SOFI 2.0.2
   */
  private String cleMessageErreurGeneral = null;

  /**
   * Spécifier que le formulaire doit être utilisé en mode ajax.
   * @since SOFI 2.1
   */
  private boolean ajax = false;

  /**
   * Est-ce que le formulaire est transactionnel
   * <p>
   * @return true pour spécifier que le formulaire est transactionnel.
   */
  public boolean isTransactionnel() {
    return this.transactionnel;
  }

  /**
   * Spécifier si le formulaire est transactionnel
   * <p>
   * @param transactionnel
   */
  public void setTransactionnel(boolean transactionnel) {
    this.transactionnel = transactionnel;
  }

  /**
   * Obtenir une variable StringBuffer de la request, si elle n'existe pas la créer
   * @param nom
   * @param pageContext
   * @return
   */
  private StringBuffer getStringBufferRequest(String nom,
      PageContext pageContext) {
    StringBuffer buffer =
        (StringBuffer)pageContext.getRequest().getAttribute(nom);

    /*
     * Créer le buffer si il n'existe pas.
     */
    if (buffer == null) {
      buffer = new StringBuffer();
      pageContext.getRequest().setAttribute(nom, buffer);
    }

    return buffer;
  }

  /**
   * Obtenir le onsubmit
   */
  private StringBuffer getOnsubmitBuffer() {
    return this.getStringBufferRequest(ConstantesBaliseJSP.FONCTION_ONSUBMIT_PERSONALISE,
        pageContext);
  }

  /**
   * Traite le début de la balise du formulaire.f
   *
   * @exception JspException si une exception est lancé
   */
  @Override
  public int doStartTag() throws JspException {

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    // Créer l'approprier élément "form" basé sur les paramètres spécifié
    StringBuffer results = new StringBuffer();

    if (getOnsubmit() != null) {
      this.getOnsubmitBuffer().append(getOnsubmit());
      this.setOnsubmit(null);
    }

    if (getAction() == null) {
      pageContext.getSession().setAttribute(Constantes.FORMULAIRE, null);
      results.append("<form>");
    } else {
      // Extraire le nom du formulaire
      this.lookup();

      if (!ajax) {
        if (getFocusAutomatique() != null) {
          setFocusAutomatique(getFocusAutomatique().toString().toLowerCase());
        } else {
          if (UtilitaireString.isVide(GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FORMULAIRE_FOCUS_AUTOMATIQUE))) {
            setFocusAutomatique("false");
          } else {
            String parametreFocusAutomatique =
                GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FORMULAIRE_FOCUS_AUTOMATIQUE).toLowerCase();

            if ((getFocusAutomatique() != null) && !isEffectuerFocusAutomatique()) {
              setFocusAutomatique("false");
            } else {
              setFocusAutomatique(parametreFocusAutomatique);
            }
          }
        }

        // @since SOFI 1.8
        // La propriété focus n'est plus utilisé, a moins que la proprité
        // focusAutomatique soit fixé à false. Un script général est utilisé pour trouver.
        // le premier champ dans la page et faire un focus automatiquement.
        if (isEffectuerFocusAutomatique()) {
          setFocus(null);
        }

        results.append(this.renderFormStartElement());

        results.append(this.renderToken());
      }

      String nomFormulaire = Constants.FORM_KEY;

      // Emmagasiner la balise dans les attributs de la page.
      pageContext.setAttribute(nomFormulaire, this, PageContext.REQUEST_SCOPE);

      this.initFormBean();
    }

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    if (formulaire != null) {
      // Fixer la clé du message d'erreur général s'il y lieu.
      if (!UtilitaireString.isVide(getCleMessageErreurGeneral())) {
        formulaire.setCleMessageErreurGeneral(getCleMessageErreurGeneral());
      }
      // Fixer le formulaire en cours dans la request.
      pageContext.getRequest().setAttribute(Constantes.FORMULAIRE_EN_COURS,
          formulaire);
    }

    if (!isAjax()) {

      String nomFormulairePourValidation =
          UtilitaireBaliseJSP.getNomFormulaire(pageContext) +
          Constantes.LISTE_ATTRIBUT_VALIDATION_FORMULAIRE;


      // Empèche une erreur javascript si dans IE
      if (formulaire.isModeAffichageSeulement()) {
        this.focus = null;
      }

      formulaire.initialiserListeAttributATraiter();

      if (formulaire.isTraiterAttributsAfficheSeulement()) {
        formulaire.initialiserAttributObligatoire();
      }

      formulaire.setTransactionnel(isTransactionnel());

      if (isTransactionnel()) {
        if (formulaire.isModeLectureSeulement()) {
          setFocus(null);
          pageContext.getRequest().setAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT,
              new Boolean(true));
        } else {
          pageContext.getRequest().setAttribute(Constantes.IND_MODIFICATION_FORMULAIRE,
              Boolean.TRUE);
        }
      } else {
        if (formulaire != null) {
          formulaire.setModeLectureSeulement(false);
          formulaire.setFormulaireModifie(false);
        }
      }

      // Spécifier que c'est un nouveau formulaire et permettre l'ajout des champs obligatoire.
      if ((GestionParametreSysteme.getInstance().getParametreSysteme(nomFormulairePourValidation) ==
          null) || !formulaire.isAttributsObligatoireEnCache()) {
        pageContext.setAttribute(nomFormulairePourValidation,
            new SommaireValidationForm());
      }

      // Emmagasiner le nom de l'adresse de l'action du formulaire
      pageContext.getRequest().setAttribute(Constantes.ADRESSE_ACTION_FORMULAIRE,
          getAction());
      TagUtils.getInstance().write(pageContext, results.toString());

      if (formulaire != null) {
        UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
            null);
      }
    }


    return (EVAL_BODY_INCLUDE);
  }

  /**
   * Traiter la fin de la balise de formualire.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException si une exception JSP est traité
   */
  @Override
  public int doEndTag() throws JspException {

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    try {
      if (!ajax) {
        // Écrire le contenu au client.
        JspWriter writer = pageContext.getOut();
        StringBuffer resultat = new StringBuffer();

        if ((formulaire != null) && !formulaire.isModeLectureSeulement() &&
            formulaire.isNouveauFormulaire()) {
          resultat.append("<input type=\"hidden\" id=\"nouveauFormulaire\" name=\"nouveauFormulaire\" value=\"true\"");
          resultat.append("/>");
          formulaire.setNouveauFormulaire(false);
        }

        resultat.append(getResultatScriptFermeture());
        writer.print(resultat.toString());
      }


    } catch (IOException e) {
      throw new JspException(messages.getMessage("common.io", e.toString()));
    }

    if (!ajax) {
      if (formulaire != null) {
        // Effacer les messages affichés dans le formulaire.
        formulaire.initialiserLesMessages();
      }

      setCleMessageAvertisementFormulaireModifie(null);
    }

    // Continuer à traiter la page
    return (EVAL_PAGE);
  }

  private StringBuffer getResultatScriptFermeture() throws JspException {

    // Le nom du formulaire en traitement.
    String nomFormulaire =
        org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP.getNomFormulaire(pageContext);

    // Initialiser l'indicateur qui spécifie que le formulaire a été initialisé
    pageContext.getSession().setAttribute(Constantes.NOM_DERNIER_FORMULAIRE,
        null);

    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);
    StringBuffer resultat = new StringBuffer();
    StringBuffer resultatOnLoad = new StringBuffer();

    if ((formulaire != null) && !formulaire.isModeLectureSeulement() &&
        formulaire.isNouveauFormulaire()) {
      resultat.append("<input type=\"hidden\" id=\"nouveauFormulaire\" name=\"nouveauFormulaire\" value=\"true\"");
      resultat.append("/>");
      formulaire.setNouveauFormulaire(false);
    }

    // Fermer la balise de fin du formulaire courrant.
    resultat.append("</form>");

    if (isTransactionnel()) {
      // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
      // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
      // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
      resultat.append("<script type=\"text/javascript\">");
      resultat.append("var aucunIndModificationFormulaire = true");
      resultat.append("</script>");
    }

    // Traitement pour afficher un message popup et un focus sur le permier
    // atttribut en erreur.
    Message message =
        (Message)pageContext.getRequest().getAttribute(Constantes.PREMIER_MESSAGE_ERREUR);

    boolean messageConfirmationExiste = false;

    boolean focusPourErreur = false;

    if (pageContext.getRequest().getAttribute("focusErreurTraite") == null) {
      if ((formulaire != null) && (message != null)) {
        if (MessageErreur.class.isInstance(message) && (message != null)) {
          MessageErreur messageErreur = (MessageErreur)message;

          if (messageErreur.getFocus() != null) {
            resultatOnLoad.append("focusErreur('");
            resultatOnLoad.append(messageErreur.getFocus());
            resultatOnLoad.append("');");
            focusPourErreur = true;
          }
        }
      } else {
        if ((formulaire != null) &&
            (formulaire.getLesConfirmations() != null) &&
            (formulaire.getLesConfirmations().size() > 0)) {
          messageConfirmationExiste = true;

          // Traiter les messages de confirmations.
          Iterator iterateur =
              formulaire.getLesConfirmations().keySet().iterator();

          if (iterateur.hasNext()) {
            String cle = (String)iterateur.next();
            String hrefRetour =
                (String)UtilitaireBaliseJSP.getListeMessageAvecHrefConfirmation(pageContext).get(cle);

            if (hrefRetour != null) {
              // Extraire de message de confirmation.
              Message messageConfirmation =
                  (Message)formulaire.getLesConfirmations().get(cle);

              //Message messageConfirmation = (Message)formulaire.getLesConfirmations().get(cle);
              String fonctionJS =
                  UtilitaireBaliseJSP.genererSoumissionFormulaire(pageContext,
                      hrefRetour,
                      nomFormulaire,
                      false, false,
                      null,
                      messageConfirmation.getMessage());
              resultatOnLoad.append(fonctionJS);
            }
          }
        }
      }
    }

    if ((formulaire != null) && (getFocus() != null) && !focusPourErreur) {
      resultat.append(renderFocusJavascript());
    }

    if (getOnload() != null) {
      resultatOnLoad.append(getOnload());
    }

    if ((formulaire != null) &&
        ((formulaire.getLesConfirmations() == null) || (formulaire.getLesConfirmations().size() ==
        0)) &&
        (pageContext.getRequest().getParameter("ajax") == null)) {
      // Traiter le javascript du focus de base requis
      if (!formulaire.isModeAffichageSeulement() &&
          (pageContext.getRequest().getAttribute("focusErreurTraite") ==
          null) && (message == null) && isEffectuerFocusAutomatique()) {
        if ((pageContext.getRequest().getAttribute("SOFI_ACTION_POST") ==
            null) || formulaire.isTransactionnel()) {
          // Ajouter l'appel de fonction qui permet de faire le focus sur le premier champ dans la page
          resultatOnLoad.append("focusPremierChampFormulaire();");
        }
      }

      // Si formulaire transactionnel, ajouter dans le onload le traitement JS qui fixe le message d'avertissement
      // que le formulaire a été modifié et que les données seront perdu si un élément de menu a été sélectionné.
      if (formulaire.isTransactionnel()) {
        String cleMessage = null;

        if (getCleMessageAvertisementFormulaireModifie() == null) {
          cleMessage =
              GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.CLE_MESSAGE_AVERTISSEMENT_FORMULAIRE_MODIFIE);
        } else {
          cleMessage = getCleMessageAvertisementFormulaireModifie();
        }

        if (!cleMessage.equals("")) {
          Message messageAvertissement =
              UtilitaireMessage.getMessage(cleMessage, UtilitaireBaliseJSP.getLocale(pageContext).getLanguage(),
                  null);
          resultatOnLoad.append("setMessageModificationFormulaire('");
          resultatOnLoad.append(messageAvertissement.getMessage());
          resultatOnLoad.append("');");
        }
      }
    }

    // Ecrire OnLoad
    this.ecrireOnload(resultatOnLoad, resultat, pageContext);

    // Ecrire le OnSubmit
    this.ecrireOnsubmit(resultat, pageContext);

    if (formulaire != null) {
      String ancienFormulaire =
          (String)pageContext.getSession().getAttribute("ancienFormulaire");

      if ((ancienFormulaire != null) &&
          !ancienFormulaire.equals(nomFormulaire)) {
        formulaire.setFormulaireModifie(false);
      }

      pageContext.getSession().setAttribute("ancienFormulaire", nomFormulaire);

      if (!formulaire.isModeLectureSeulement() && formulaire.isModifie() &&
          !formulaire.isMessageDansFormulaire() &&
          ((ancienFormulaire == null) ||
              ancienFormulaire.equals(nomFormulaire))) {
        // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
        // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
        // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
        resultat.append("<script type=\"text/javascript\">");
        resultat.append(UtilitaireBaliseJSP.genererNotifierFormulaire(nomFormulaire));
        resultat.append("</script>");
      }

      // Supprimer les attributs qui ont été crée pour le traitement
      pageContext.removeAttribute(Constants.BEAN_KEY,
          PageContext.REQUEST_SCOPE);
      pageContext.removeAttribute(Constants.FORM_KEY,
          PageContext.REQUEST_SCOPE);

      pageContext.getRequest().removeAttribute(Constantes.ADRESSE_ACTION_FORMULAIRE);

      // Traiter la fonction qui permet d'activer le ou les boutons demandés
      List listeBoutonAActive =
          (List)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_ACTIVE);
      // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
      // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
      // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
      resultat.append("<script type=\"text/javascript\">");
      resultat.append("\nfunction ");
      resultat.append(ConstantesBaliseJSP.NOM_FONCTION_ACTIVER_BOUTONS);
      resultat.append("() {\n");

      if (!formulaire.isModeLectureSeulement() &&
          (listeBoutonAActive != null)) {
        resultat.append("if (document.");
        resultat.append(nomFormulaire);
        resultat.append(".indModification.value == '1') {");

        Iterator iterateur = listeBoutonAActive.iterator();

        while (iterateur.hasNext()) {
          String id = (String)iterateur.next();
          resultat.append("setBoutonInactif(");
          resultat.append("false,'");
          resultat.append(id);
          resultat.append("');\n");
        }

        resultat.append("}");
      }

      resultat.append("}\n</script>\n");
    }

    // Traiter la fonction qui permet d'inactiver le ou les boutons demandés
    List listeBoutonAInActive =
        (List)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.LISTE_BOUTON_A_INACTIVE);

    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
    resultat.append("\n<script type=\"text/javascript\">");
    resultat.append("\nfunction ");
    resultat.append(ConstantesBaliseJSP.NOM_FONCTION_INACTIVER_BOUTONS);
    resultat.append("() {\n");

    if ((formulaire != null) && !formulaire.isModeLectureSeulement() &&
        (listeBoutonAInActive != null)) {
      resultat.append("if (document.");
      resultat.append(nomFormulaire);
      resultat.append(".indModification.value == '1') {");

      Iterator iterateur = listeBoutonAInActive.iterator();

      while (iterateur.hasNext()) {
        String id = (String)iterateur.next();
        resultat.append("setBoutonInactif(");
        resultat.append("true,'");
        resultat.append(id);
        resultat.append("');\n");
      }

      resultat.append("}");
    }

    resultat.append("}\n</script>\n");

    if (!messageConfirmationExiste) {
      // Supprimer le premier message
      pageContext.getSession().setAttribute(Constantes.PREMIER_MESSAGE_ERREUR,
          null);
    }

    return resultat;
  }

  private void ecrireOnload(StringBuffer resultatOnLoad, StringBuffer resultat,
      PageContext context) {
    String fonctionJavaScriptOnload =
        UtilitaireBaliseJSP.getFonctionJSOnLoad();

    if (!UtilitaireString.isVide(fonctionJavaScriptOnload)) {
      resultatOnLoad.append(fonctionJavaScriptOnload);
      resultatOnLoad.append(";");
    }

    resultatOnLoad.append(ConstantesBaliseJSP.NOM_FONCTION_ACTIVER_BOUTONS);
    resultatOnLoad.append("();");
    resultatOnLoad.append(ConstantesBaliseJSP.NOM_FONCTION_INACTIVER_BOUTONS);
    resultatOnLoad.append("();");

    if (pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE) !=
        null) {
      resultatOnLoad.append((StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONLOAD_PERSONALISE));
    }

    if ((resultatOnLoad.length() > 0) &&
        (pageContext.getRequest().getParameter("ajax") == null)) {
      // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
      // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
      // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
      resultat.append("<script type=\"text/javascript\">\n");
      resultat.append("window.onload=function(){NiceTitles.autoCreation();");
      resultat.append("setTimeout(\"");
      resultat.append(resultatOnLoad);
      resultat.append("\", 100);");
      resultat.append("}");
      resultat.append("\n</script>");
    }
  }

  private void ecrireOnsubmit(StringBuffer resultat, PageContext pageContext) {
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    StringBuffer fonctionOnsubmit =
        (StringBuffer)pageContext.getRequest().getAttribute(ConstantesBaliseJSP.FONCTION_ONSUBMIT_PERSONALISE);
    if ((fonctionOnsubmit != null && fonctionOnsubmit.length() > 0) &&
        (pageContext.getRequest().getParameter("ajax") == null)) {
      /*
       * document.contactForm.onsubmit = function() {multipleSelectOnSubmit();};
       * */


      // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
      // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
      // resultat.append("<script type=\"text/javascript\" language=\"JavaScript\">");
      resultat.append("<script type=\"text/javascript\">\n");
      resultat.append("document.").append(nomFormulaire).append(".onsubmit = function() {");
      resultat.append(fonctionOnsubmit);
      resultat.append("};");
      resultat.append("\n</script>");
    }
  }

  /**
   * Generates the opening <code>&lt;form&gt;</code> element with appropriate
   * attributes.
   * @since Struts 1.1
   */
  @Override
  protected String renderFormStartElement() {
    StringBuffer results = new StringBuffer("<form");

    // render attributes
    renderName(results);

    renderAttribute(results, "method",
        (getMethod() == null) ? "post" : getMethod());
    renderAction(results);
    renderAttribute(results, "accept-charset", getAcceptCharset());
    renderAttribute(results, "class", getStyleClass());
    renderAttribute(results, "enctype", getEnctype());
    renderAttribute(results, "onreset", getOnreset());
    //renderAttribute(results, "onsubmit", getOnsubmit());
    renderAttribute(results, "style", getStyle());
    renderAttribute(results, "target", getTarget());

    if (isAutocomplete()) {
      renderAttribute(results, "autocomplete", "on");
    }

    // L'attribut AUTOCOMPLETE n'est pas un attribut valide de la balise FORM.
    // Il est préférable de l'ignorer lorsqu'il n'est pas exigé pour ne pas
    // qu'il se retrouve dans la balise.
    // REF: http://www.w3.org/TR/REC-html40/interact/forms.html#edef-FORM
    // else {
    //   renderAttribute(results, "autocomplete", "off");
    // }

    // Hook for additional attributes
    renderOtherAttributes(results);

    results.append(">");

    return results.toString();
  }

  /**
   * Renders the name of the form.  If XHTML is set to true, the name will
   * be rendered as an 'id' attribute, otherwise as a 'name' attribute.
   */
  @Override
  protected void renderName(StringBuffer results) {
    renderAttribute(results, "name", beanName);

    if (getStyleId() == null) {
      renderAttribute(results, "id", beanName);
    } else {
      renderAttribute(results, "id", getStyleId());
    }
  }

  /**
   * Renders the action attribute
   */
  @Override
  protected void renderAction(StringBuffer results) {
    HttpServletResponse response =
        (HttpServletResponse)this.pageContext.getResponse();

    results.append(" action=\"");
    results.append(response.encodeURL(TagUtils.getInstance().getActionMappingURL(this.action,
        this.pageContext)));

    results.append("\"");
  }

  /**
   * 'Hook' to enable this tag to be extended and
   *  additional attributes added.
   */
  @Override
  protected void renderOtherAttributes(StringBuffer results) {
  }

  /**
   * Generates a hidden input field with token information, if any. The
   * field is added within a div element for HTML 4.01 Strict compliance.
   * @return A hidden input field containing the token.
   * @since Struts 1.1
   */
  @Override
  protected String renderToken() {
    StringBuffer results = new StringBuffer();
    HttpSession session = pageContext.getSession();

    if (session != null) {
      String token =
          (String)session.getAttribute(Globals.TRANSACTION_TOKEN_KEY);

      if (token != null) {
        results.append("<div><input type=\"hidden\" name=\"");
        results.append(Constants.TOKEN_KEY);
        results.append("\" value=\"");
        results.append(token);

        if (isXhtml()) {
          results.append("\" />");
        } else {
          results.append("\">");
        }

        results.append("</div>");
      }
    }

    return results.toString();
  }

  /**
   * Renders attribute="value" if not null
   */
  @Override
  protected void renderAttribute(StringBuffer results, String attribute,
      String value) {
    if (value != null) {
      results.append(" ");
      results.append(attribute);
      results.append("=\"");
      results.append(value);
      results.append("\"");
    }
  }

  /**
   * Returns true if this tag should render as xhtml.
   */
  private boolean isXhtml() {
    return TagUtils.getInstance().isXhtml(this.pageContext);
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    String onload =
        EvaluateurExpression.evaluerString("onload", getOnload(), this,
            pageContext);
    setOnload(onload);

    String action =
        EvaluateurExpression.evaluerString("action", getAction(), this,
            pageContext);
    setAction(action);

    String focus =
        EvaluateurExpression.evaluerString("focus", getFocus(), this,
            pageContext);
    setFocus(focus);

    if (getFocusAutomatique() != null) {
      Object focusAutomatique =
          EvaluateurExpression.evaluerString("focusAutomatique",
              (String)getFocusAutomatique(), this,
              pageContext);
      setFocusAutomatique(((String)focusAutomatique).toLowerCase());
    }

    String cleMessageAvertisementFormulaireModifie =
        EvaluateurExpression.evaluerString("cleMessageAvertisementFormulaireModifie",
            getCleMessageAvertisementFormulaireModifie(),
            this, pageContext);

    setCleMessageAvertisementFormulaireModifie(cleMessageAvertisementFormulaireModifie);

    String cleMessageMessageGeneral =
        EvaluateurExpression.evaluerString("cleMessageMessageGeneral",
            getCleMessageErreurGeneral(), this,
            pageContext);

    setCleMessageErreurGeneral(cleMessageMessageGeneral);
  }

  public void setOnload(String onload) {
    this.onload = onload;
  }

  public String getOnload() {
    return onload;
  }

  public void setFocusAutomatique(Object focusAutomatique) {
    this.focusAutomatique = focusAutomatique;
  }

  public Object getFocusAutomatique() {
    return focusAutomatique;
  }

  /**
   * Est-ce que le focus doit être fait automatiquement sur le
   * premier champ disponible dans le formulaire.
   * @return true si le focus doit être fait automatiquement sur le
   * premier champ disponible dans le formulaire.
   */
  public boolean isEffectuerFocusAutomatique() {
    if ((getFocusAutomatique() != null) &&
        getFocusAutomatique().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setCleMessageAvertisementFormulaireModifie(String cleMessageAvertisementFormulaireModifie) {
    this.cleMessageAvertisementFormulaireModifie =
        cleMessageAvertisementFormulaireModifie;
  }

  public String getCleMessageAvertisementFormulaireModifie() {
    return cleMessageAvertisementFormulaireModifie;
  }

  public void setAutocomplete(boolean autocomplete) {
    this.autocomplete = autocomplete;
  }

  public boolean isAutocomplete() {
    return autocomplete;
  }

  /**
   * Retourne la clé du message d'erreur général à utiliser pour le formulaire.
   * @return la clé du message d'erreur général à utiliser pour le formulaire.
   */
  public String getCleMessageErreurGeneral() {
    return cleMessageErreurGeneral;
  }

  /**
   * Fixer la clé du message d'erreur général à utiliser pour ce formulaire.
   * @param cleMessageErreurGeneral la clé du message d'erreur général à utiliser pour le formulaire.
   */
  public void setCleMessageErreurGeneral(String cleMessageErreurGeneral) {
    this.cleMessageErreurGeneral = cleMessageErreurGeneral;
  }

  /**
   * Permet de spécifier que le formulaire est utilisé en mode ajax.
   * @param ajax true si le formulaire est utilisé en mode ajax.
   */
  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  /**
   * Retourne true si le formulaire est utilisé en mode ajax.
   * @return true si le formulaire est utilisé en mode ajax.
   */
  public boolean isAjax() {
    return ajax;
  }

  @Override
  public void release() {
    setFocus(null);
    setFocusAutomatique(null);
    super.release();
  }
}
