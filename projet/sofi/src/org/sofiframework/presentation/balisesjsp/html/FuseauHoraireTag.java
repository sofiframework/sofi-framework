/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.html;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.cache.ListeFuseauHoraire;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise qui affiche une liste de fuseaux horaires. Les fuseaux horaires peuvent être d'un seul pays si le paramètre
 * "paysFuseauHoraire" spécifie une liste de pays. Les descriptions de chaque fuseaux doivent être disponibles par le
 * service de libellés. Le clé de libellé de chaque fuseau est composée d'un paramètre "libelleFuseauHoraire" et de
 * l'identifiant du fuseau. Lors d'une première utilisation simplement faire afficher une page avec le composant. En
 * affichant la source de la page, il est ensuite possible d'ajouter les libellés dans la base de données.
 */
public class FuseauHoraireTag extends SelectTag {
  /**
   * 
   */
  private static final long serialVersionUID = -4140839590754100469L;

  /**
   * Obtenir la liste des fuseaux qui sera utilisée par le composant
   */
  private static ListeFuseauHoraire listeFuseau = null;

  static {
    listeFuseau = new ListeFuseauHoraire();
    listeFuseau.chargerDonnees();
  }

  private String libelleFuseauHoraire = null;

  /**
   * Constructeur par défaut
   */
  public FuseauHoraireTag() {
    setCache("listeTimeZone");
  }

  /**
   * Obtenir l'objet cache. La liste des fuseaux horaires.
   * 
   * @return Liste de fuseaux horaires utilisés pour construire les options.
   * 
   * @param nomCache
   *          Nom de l'objet cache (n'est pas utilisée par ce composant)
   */
  @Override
  protected ObjetCache getObjetCache(String nomCache) {
    return listeFuseau;
  }

  /**
   * Compose la fonction javascript nécessaire pour indiquer à l'utilisateur son fuseau horaire.
   * 
   * @throws javax.servlet.jsp.JspException
   *           Erreur survenu lors de l'exécution du traitement
   * @return Directive de fin du tag
   */
  @Override
  public int doEndTag() throws JspException {
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(pageContext.getSession());
    StringBuffer message = new StringBuffer();
    String libelle = null;

    if ((libelleFuseauHoraire != null) && (utilisateur != null)) {
      libelle = GestionLibelle.getInstance()
          .get(libelleFuseauHoraire, utilisateur.getLangue(), (Object) null, null).getMessage();
    }

    if (libelle != null) {
      message.append(libelle);
    }

    // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
    // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
    // message.append("<script type=\"text/javascript\" language=\"JavaScript\">\n");
    message.append("<script type=\"text/javascript\">\n");
    message.append("ecrireFuseauHoraire();\n");
    message.append("</script>\n");

    TagUtils.getInstance().write(pageContext, message.toString());

    return super.doEndTag();
  }

  /**
   * Fixer le code de libellé qui correspond au message du fuseau horaire à droite du champs.
   * 
   * @param libelleFuseauHoraire
   *          Code de libellé
   */
  public void setLibelleFuseauHoraire(String libelleFuseauHoraire) {
    this.libelleFuseauHoraire = libelleFuseauHoraire;
  }

  /**
   * Obtenir le code de libellé qui correspond au message du fuseau horaire à droite du champs.
   * 
   * @return Code de libellé
   */
  public String getLibelleFuseauHoraire() {
    return libelleFuseauHoraire;
  }
}
