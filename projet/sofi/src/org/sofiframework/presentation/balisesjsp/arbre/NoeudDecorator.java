/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.arbre;

import java.util.Collection;

/**
 * Décorateur utilisé pour l'affichage d'un objet par la balise arbre.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface NoeudDecorator {

  /**
   * Méthode qui détermine si le noeud en cours possède des noeuds enfant ou pas.
   * <p>
   * @return la valeur qui indique si le noeud en cours est une feuille ou pas
   */
  boolean isFeuille(Object noeud);

  /**
   * Méthode qui sert à récupérer la liste de tous les noeuds enfant du noeud courant.
   * <p>
   * @return la collection qui contient tous les noeuds enfants du noeud en cours
   */
  Collection getListeEnfants(Object noeud);

  /**
   * Méthode qui sert à retourner l'identifiant unique du noeud en cours.
   * <p>
   * Cette méthode est utilisée afin de déterminer le noeud sur lequel est placé le focus.
   * <p>
   * @return la donnée qui représente l'identifiant unique du noeud.
   */
  String getIdentifiantNoeud(Object noeud);
}
