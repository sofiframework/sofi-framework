/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.arbre;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;


/**
 * Balise permettant de générer un arbre HTML.
 * <p>
 * @author : Guillaume Poirier, Nurun inc.
 * @author : Pierre-Frédérick Duret, Nurun inc.
 * @author : Jean-Maxime Pelletier
 */
public class ArbreTag extends BodyTagSupport {

  /**
   * 
   */
  private static final long serialVersionUID = 6769808213278045477L;

  private Log log = LogFactory.getLog(ArbreTag.class);

  private String var;
  private Object racine;
  private Object liste;
  private JspWriter out;
  private Stack pile;
  private Iterator itCourant;
  protected Object noeud;
  private int seq;
  private String classeDecorator;

  private NoeudDecorator decorator;

  public void setRacine(Object racine) {
    this.racine = racine;
  }

  public void setListe(Object liste) {
    this.liste = liste;
  }

  public void setVar(String var) {
    this.var = var;
  }

  private void evaluerEL() throws JspException {
    this.racine = UtilitaireBaliseJSP.getValeurEL("racine", racine.toString(), Object.class, this, pageContext);
    this.liste = UtilitaireBaliseJSP.getValeurEL("liste", liste.toString(), Collection.class, this, pageContext);
  }

  @Override
  public int doStartTag() throws JspException {
    this.pile = new Stack();
    this.seq = 0;

    try {
      this.out = pageContext.getOut();

      if (EvaluateurExpression.isEvaluerEL(pageContext)) {
        this.evaluerEL();
      }

      if (this.liste == null) {
        if (this.racine == null) {
          throw new JspException("L'attribut racine ou liste doit être spécifié.");
        }
        this.liste = Collections.singletonList(racine);
      }

      if (this.classeDecorator != null) {
        this.decorator = (NoeudDecorator) Thread.currentThread()
            .getContextClassLoader().loadClass(this.classeDecorator).newInstance();
      }

      this.itCourant = ((Collection) liste).iterator();

      if (this.itCourant.hasNext()) {
        this.ouvrirListe(true);
        this.suivant();
        return EVAL_BODY_BUFFERED;
      } else {
        return SKIP_BODY;
      }
    } catch (IOException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur d'entrée/sortie du tag Arbre doStartTag.", e);
      }
      throw new JspException(e);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur du tag Arbre doStartTag.", e);
      }
      throw (e instanceof JspException) ? (JspException) e : new JspException(e);
    }
  }

  @Override
  public int doAfterBody() throws JspException {
    try {
      NoeudArbre noeudConverti = this.getNoeudConverti();

      this.bodyContent.writeOut(out);
      this.bodyContent.clear();

      if (!noeudConverti.isFeuille()) {
        this.ouvrirListe(false);
        this.pile.push(itCourant);
        this.itCourant = noeudConverti.getListeEnfants().iterator();
      }

      if (itCourant.hasNext()) {
        this.suivant();
        return EVAL_BODY_BUFFERED;
      } else {
        while (!itCourant.hasNext()) {
          this.fermerItem();
          this.fermerListe();

          if (!pile.isEmpty()) {
            this.itCourant = (Iterator) this.pile.pop();
          } else {
            return SKIP_BODY;
          }
        }

        this.fermerItem();
        this.suivant();
        return EVAL_BODY_BUFFERED;
      }
    } catch (IOException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur d'entrée/sortie du tag Arbre doAfterBody.", e);
      }
      throw new JspException(e);
    } catch (RuntimeException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur Runtime du tag Arbre doAfterBody.", e);
      }
      throw e;
    }
  }

  @Override
  public int doEndTag() throws JspException {
    this.release();
    return super.doEndTag();
  }

  /**
   * Réinitialiser les propriétés de la balise.
   */
  @Override
  public void release() {
    this.var = null;
    this.racine = null;
    this.liste = null;
    this.out = null;
    this.pile = null;
    this.itCourant = null;
    this.noeud = null;
    this.seq = 0;
  }

  private void suivant() throws IOException, JspException {
    this.noeud = itCourant.next();

    if (noeud == null) {
      throw new NullPointerException("L'arbre contient un noeud null.");
    }

    if (!(this.noeud instanceof NoeudArbre) && this.classeDecorator == null) {
      throw new JspException("Les noeuds de l'arbre doivent implémenter " +
          "l'interface noeud ou bien vous devez " +
          "fournir une classe de décorateur qui " +
          "implémente NoeudDecorator.");
    }

    this.ouvrirItem();
    this.pageContext.setAttribute(this.var, this.noeud);
  }

  /**
   * Permet d'ouverture de la liste des noeuds de l'arbre.
   * @param racine Si il s'agit de la racine de l'arbre (du premier niveau).
   * @throws IOException Erreur d'entrée/sortie
   */
  protected void ouvrirListe(boolean racine) throws IOException {
    if (racine) {
      this.out.print("<ul id=\"");
      this.out.print(getId());
      this.out.println("\">");
    } else {
      this.out.println("<ul>");
    }
  }

  /**
   * Fermeture de la liste des noeauds de l'arbre.
   * @throws IOException Erreur d'entrée/sortie
   */
  protected void fermerListe() throws IOException {
    this.out.println("</ul>");
  }

  /**
   * Ouverture d'un item de l'arbre.
   * @throws IOException Erreur d'entrée/sortie
   */
  protected void ouvrirItem() throws IOException {
    NoeudArbre noeudConverti = this.getNoeudConverti();
    String id = noeudConverti.getIdentifiantNoeud();

    if (id == null) {
      this.out.println("<li>");
    } else {
      this.out.print("<li id=\"");
      this.out.print(getId());
      this.out.print("_");
      this.out.print(id);
      this.out.print("\" value=\"");
      this.out.print(getId());
      this.out.println("\">");
    }
  }

  /**
   * Fermeture d'un noeud de l'abre.
   * @throws IOException Erreur d'entrée/sortie
   */
  protected void fermerItem() throws IOException {
    this.out.println("</li>");
  }

  protected NoeudArbre getNoeudConverti() {
    return (this.noeud instanceof NoeudArbre)
        ? (NoeudArbre) this.noeud : new NoeudDecore(this.noeud);
  }

  public String getClasseDecorator() {
    return classeDecorator;
  }

  public void setClasseDecorator(String classeDecorator) {
    this.classeDecorator = classeDecorator;
  }

  /**
   * Décoration d'un objet pour représenter un noeud de l'arbre.
   */
  class NoeudDecore extends NoeudArbre {

    /**
     * 
     */
    private static final long serialVersionUID = 5542952487150940577L;
    /**
     * Noeud de l'arbre.
     */
    private Object noeud;

    public NoeudDecore(Object noeud) {
      super();
      this.noeud = noeud;
    }

    @Override
    public String getIdentifiantNoeud() {
      return decorator.getIdentifiantNoeud(this.noeud);
    }

    @Override
    public Collection getListeEnfants() {
      return decorator.getListeEnfants(this.noeud);
    }

    @Override
    public boolean isFeuille() {
      return decorator.isFeuille(this.noeud);
    }
  }
}
