/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.displaytag.decorator.TableDecorator;
import org.sofiframework.constante.Constantes;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.balisesjsp.utilitaire.InfoMultibox;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Décorateur pour la liste de navigation.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class ListeNavigationDecorateur extends TableDecorator {
  /**
   * Génère une boite à cocher multiple.
   * @return String une boite à cocher.
   */
  public String getMultibox() {
    StringBuffer resultat = new StringBuffer();

    String compteurLigne = String.valueOf(getListIndex());
    String ancienCompteurLigne = (String) this.getPageContext().getAttribute("compteurLigneListeNavigation");
    Integer compteurColonne = (Integer) this.getPageContext().getAttribute("compteurColonneMultibox");

    if ((ancienCompteurLigne == null) ||
        !compteurLigne.equals(ancienCompteurLigne)) {
      compteurColonne = new Integer(0);
    } else {
      compteurColonne = new Integer(compteurColonne.intValue() + 1);
    }

    this.getPageContext().setAttribute("compteurColonneMultibox",
        compteurColonne);
    this.getPageContext().setAttribute("compteurLigneListeNavigation",
        compteurLigne);

    ColonneMultiboxTag baliseMultiboxTag = (ColonneMultiboxTag) this.getPageContext()
        .getAttribute(ColonneMultiboxTag.COLONNE_MULTIBOX +
            compteurColonne);

    String nomAttributMultibox = baliseMultiboxTag.getNomAttributMultibox();
    String nomValeurMultibox = baliseMultiboxTag.getNomValeurMultibox();
    HashSet listeSelectionnee = baliseMultiboxTag.getListeSelectionnee();
    String proprieteActivation = baliseMultiboxTag.getProprieteActivation();

    String onClick = baliseMultiboxTag.getOnClick();

    ObjetTransfert objetTransfert = (ObjetTransfert) getCurrentRowObject();

    Object valeurPourMultibox = objetTransfert.getPropriete(nomValeurMultibox);

    Boolean actif = new Boolean(true);

    if (proprieteActivation != null) {
      actif = (Boolean) objetTransfert.getPropriete(proprieteActivation);
    }

    // Ajout JFB afin d'emmagasiner les informations de la liste courante pour traitement des boites a cocher multiple
    HashMap listeInfoListeCouranteMultibox = (HashMap) this.getPageContext()
        .getSession()
        .getAttribute("listeInfoListeCouranteMultibox");

    if (compteurLigne.equals("0") && (compteurColonne.intValue() == 0)) {
      if (listeInfoListeCouranteMultibox == null) {
        listeInfoListeCouranteMultibox = new HashMap();
      }

      this.getPageContext().getSession().setAttribute("listeInfoListeCouranteMultibox",
          listeInfoListeCouranteMultibox);
    }

    InfoMultibox infoListeCouranteMultibox = null;

    if (listeInfoListeCouranteMultibox.get(nomAttributMultibox) != null) {
      infoListeCouranteMultibox = (InfoMultibox) listeInfoListeCouranteMultibox.get(nomAttributMultibox);
    } else {
      infoListeCouranteMultibox = new InfoMultibox();
      infoListeCouranteMultibox.setListeSelectionnee(listeSelectionnee);
      infoListeCouranteMultibox.setNomAttribut(nomAttributMultibox);
      listeInfoListeCouranteMultibox.put(nomAttributMultibox,
          infoListeCouranteMultibox);
    }

    if (listeSelectionnee == null) {
      listeSelectionnee = infoListeCouranteMultibox.getListeSelectionnee();
    }

    infoListeCouranteMultibox.ajouterIdentifiantListeCourante(valeurPourMultibox);

    // Fin JFB
    resultat.append("<input type=\"checkbox\"");
    resultat.append(" name=\"");
    resultat.append(nomAttributMultibox);
    resultat.append("\"");
    resultat.append(" value=\"");
    resultat.append(valeurPourMultibox.toString());
    resultat.append("\"");

    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(this.getPageContext());

    if (baliseMultiboxTag.getAdresseBalise() == null) {
      String[] valeurs = null;

      try {
        valeurs = BeanUtils.getArrayProperty(baseForm, nomAttributMultibox);

        if (valeurs == null) {
          valeurs = new String[0];
        }
      } catch (IllegalAccessException e) {
      } catch (InvocationTargetException e) {
      } catch (NoSuchMethodException e) {
      }

      if (getPageContext().getSession().getAttribute("listeInfoListeCouranteMultiboxTraite") == null) {
        for (int i = 0; i < valeurs.length; i++) {
          if (valeurPourMultibox.toString().equals(valeurs[i])) {
            resultat.append(" checked=\"checked\"");

            if (listeSelectionnee != null) {
              listeSelectionnee.add(valeurPourMultibox.toString());
            }

            break;
          } else {
            if ((listeSelectionnee != null) &&
                (getPageContext().getRequest().getAttribute("consulterListeNavigationAvecAncienParam") != null)) {
              listeSelectionnee.remove(valeurPourMultibox.toString());
            }
          }
        }
      }
    } else {
      // Traitement si l'utilisation d'une requête HTTP GET
      Href lien = baliseMultiboxTag.getAdresseBalise();
      lien.ajouterParametre(infoListeCouranteMultibox.getNomParametreRequete(),
          valeurPourMultibox);
      // Non conforme au W3C. Les attributs doivent être en minuscules.
      // REF: http://www.w3.org/TR/xhtml1/#h-4.2
      //      resultat.append(" onClick=\"");
      resultat.append(" onclick=\"");
      if (!StringUtils.isEmpty(baliseMultiboxTag.getIdValeurRetour())) {
        resultat.append(UtilitaireAjax.traiterRequeteHttpGETAvecReponse(lien,baliseMultiboxTag.getIdValeurRetour()));
      }else {
        resultat.append(UtilitaireAjax.traiterRequeteHttpGETAsynchrone(lien));
      }

      if (!UtilitaireString.isVide(baliseMultiboxTag.getOnClick())) {
        resultat.append(baliseMultiboxTag.getOnClick());
      }

      resultat.append("\"");
    }

    if ((listeSelectionnee != null) &&
        listeSelectionnee.contains(valeurPourMultibox.toString())) {
      resultat.append(" checked=\"checked\"");
    }

    if (!actif.booleanValue()) {
      // Pas conforme au W3C. Un attribut doit posséder une valeur en XHTML.
      // REF: http://www.w3.org/TR/xhtml1/#h-4.5
      //      resultat.append(" disabled ");
      resultat.append(" disabled=\"disabled\" ");
    } else {
      if ((baliseMultiboxTag.getHref() == null) &&
          !UtilitaireString.isVide(onClick)) {
        // Non conforme au W3C. Les attributs doivent être en minuscules.
        // REF: http://www.w3.org/TR/xhtml1/#h-4.2
        //        resultat.append(" onClick=\"");
        resultat.append(" onclick=\"");
        resultat.append(onClick);
        resultat.append("\"");
      }
    }

    // Fermeture de la balise selon la valeur de l'attribut xhtml de la balise
    // <html:html> de la page JSP
    //    resultat.append("/>");
    resultat.append(UtilitaireBaliseJSP.getElementClose(this.getPageContext()));

    return resultat.toString();
  }

  /**
   * Retourne l'objet de transfert courant pour la rangée courante
   * de la liste.
   * @return l'objet de transfert associé à la rangée courante.
   */
  public ObjetTransfert getObjetTransfert() {
    return (ObjetTransfert) getCurrentRowObject();
  }

  /**
   * Retourne la balise de la colonne en traitement de la liste.
   * @param nomProprieteColonne le nom de la propriété de la colonne
   * @return la balise colonne.
   */
  public ColonneTag getBaliseColonne(String nomProprieteColonne) {
    return (ColonneTag) getPageContext().getAttribute(ColonneTag.COLONNE +
        nomProprieteColonne);
  }

  /**
   * Retourne le modèle de l'application.
   * @return le modèle de l'application.
   */
  public Object getModele() {
    return getPageContext().getRequest().getAttribute(Constantes.MODELE);
  }
}
