/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.util.HashSet;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.tags.el.ExpressionEvaluator;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Balise permettant de traiter une colonne décorant une boite à cocher
 * d'une liste de navigation page par page.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class ColonneMultiboxTag extends ColonneTag {

  private static final long serialVersionUID = 6292081223810195345L;

  public static String COLONNE_MULTIBOX = "colonneMultiboxTag";

  protected HashSet listeSelectionnee;
  protected String nomListeSelectionnee;
  protected String nomAttributMultibox;
  protected String nomValeurMultibox;
  protected String proprieteActivation;
  protected String onClick;
  protected String idValeurRetour;

  private Log log = LogFactory.getLog(ColonneMultiboxTag.class);

  public String getNomListeSelectionnee() {
    return nomListeSelectionnee;
  }

  public void setNomListeSelectionnee(String nomListeSelectionnee) {
    this.nomListeSelectionnee = nomListeSelectionnee;
  }

  public HashSet getListeSelectionnee() {
    return listeSelectionnee;
  }

  public void setListeSelectionnee(HashSet listeSelectionnee) {
    this.listeSelectionnee = listeSelectionnee;
  }

  public String getNomAttributMultibox() {
    return nomAttributMultibox;
  }

  public void setNomAttributMultibox(String nomAttributMultibox) {
    super.setProperty("multibox");
    this.nomAttributMultibox = nomAttributMultibox;
  }

  public String getNomValeurMultibox() {
    return nomValeurMultibox;
  }

  public void setNomValeurMultibox(String nomValeurMultibox) {
    this.nomValeurMultibox = nomValeurMultibox;
  }

  public String getProprieteActivation() {
    return proprieteActivation;
  }

  public void setProprieteActivation(String proprieteActivation) {
    this.proprieteActivation = proprieteActivation;
  }

  public String getOnClick() {
    return onClick;
  }

  public void setOnClick(String onClick) {
    this.onClick = onClick;
  }
  
  public String getIdValeurRetour() {
    return idValeurRetour;
  }

  public void setIdValeurRetour(String idValeurRetour) {
    this.idValeurRetour = idValeurRetour;
  }

  @Override
  public int doStartTag() throws JspException {
    setConvertirEnHtml(false);

    if (EvaluateurExpression.isEvaluerEL(pageContext) &&
        (getNomListeSelectionnee() != null)) {
      if (getNomListeSelectionnee().toString().indexOf("$") != -1) {
        ExpressionEvaluator eval = new ExpressionEvaluator(this, pageContext);
        listeSelectionnee = (HashSet) eval.eval("nomListeSelectionnee",
            nomListeSelectionnee, HashSet.class);
      }
    }

    Integer compteurColonne = (Integer) pageContext.getAttribute(
        "compteurColonneListeNavigation");

    if (compteurColonne == null) {
      compteurColonne = new Integer(0);
    } else {
      compteurColonne = new Integer(compteurColonne.intValue() + 1);
    }

    pageContext.setAttribute("compteurColonneListeNavigation", compteurColonne);

    try {
      pageContext.setAttribute(ColonneMultiboxTag.COLONNE_MULTIBOX + compteurColonne.toString(), this.clone());
    } catch (CloneNotSupportedException e) {
      if (log.isErrorEnabled()) {
        log.error(e);
      }
    }

    return super.doStartTag();
  }
}
