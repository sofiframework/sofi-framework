/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.displaytag.tags.el.ColumnTag;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Balise permettant de traiter une colonne d'une liste de navigation page par page
 * avec tri automatisé.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class ColonneTag extends ColumnTag implements Cloneable {
  /**
   * 
   */
  private static final long serialVersionUID = 8068896396091185777L;
  public static String COLONNE = "colonneTag";
  private Log log = LogFactory.getLog(ColonneTag.class);

  /**
   * Fixer si un lien hypertexte automatique est généré.
   * @param s true si un lien hypertexte automatique est généré.
   */
  public void setLienHypertexteAutomatique(String s) {
    super.setAutolink(s);
  }

  /**
   * Fixer le decorateur
   * @param s true le decorateur
   */
  public void setDecorateur(String s) {
    super.setDecorator(s);
  }

  /**
   * Fixer si la colonne permet un tri
   * @param s
   */
  public void setTrier(String s) {
    if ((s != null) && s.equals("true")) {
      super.setHeaderClass("colonnetri");
    }

    super.setSortable(s);
  }

  /**
   * Fixer la fonction javascript qui permet de spécifier un ou des champs
   * dans le formulaire parent.
   * @param nomFonction le nom de la fonction javascript.
   */
  @Override
  public void setFonction(String nomFonction) {
    super.setFonction(nomFonction);
  }

  /**
   * Fixer les paramètres de la fonction javascript spécifier dans
   * la méthode <code>setFonction</code>.
   * @param parametresFonction les paramètres de la fonction javascript.
   */
  @Override
  public void setParametresFonction(String parametresFonction) {
    super.setParametresFonction(parametresFonction);
  }

  /**
   * Retourne le libelle de la colonne.
   */
  public String getLibelle() {
    return super.getTitle();
  }

  /**
   * Fixer le libellé de la colonne de la liste.
   * @param libelle le libellé de la colonne de la liste
   */
  public void setLibelle(String libelle) {
    super.setTitle(libelle);
  }

  /**
   * Retourne l'aide contextuell de la colonne.
   */
  @Override
  public String getAideContextuelle() {
    return super.getAideContextuelle();
  }

  /**
   * Fixer l'aide contextuelle de la colonne de la liste.
   * @param l'aide contextuell le libellé de la colonne de la liste
   */
  @Override
  public void setAideContextuelle(String aideContextuelle) {
    super.setAideContextuelle(aideContextuelle);
  }

  @Override
  public int doStartTag() throws JspException {

    try {
      if (EvaluateurExpression.isEvaluerEL(pageContext)) {
        evaluerEL();
      }

      Utilisateur utilisateur =
          UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // L'utilisateur à droit de voir le composant
        if (!GestionSecurite.getInstance().isUtilisateurAccesObjetSecurisable(utilisateur,
            getLibelle())) {
          return SKIP_BODY;
        }
      }

      /**
       * Appliquer le gestionnaire des libellés
       */
      Libelle libelle =
          UtilitaireLibelle.getLibelle(getLibelle(), pageContext);

      if (libelle != null) {
        setLibelle(libelle.getMessage());
      }

      if (getAideContextuelle() != null) {
        Libelle aideContextuelle =
            UtilitaireLibelle.getLibelle(getAideContextuelle(), pageContext);
        super.setAideContextuelle(aideContextuelle.getMessage());
      } else {
        if (!UtilitaireString.isVide(libelle.getAideContextuelle())) {
          super.setAideContextuelle(libelle.getAideContextuelle());
        } else {
          super.setAideContextuelle("");
        }
      }

      pageContext.setAttribute(ColonneTag.COLONNE + getProperty(), this);

      return super.doStartTag();
    } catch (Exception e) {
      String message = "Erreur lors du traitement doStartTag d'une colonne.";

      if (log.isErrorEnabled()) {
        log.error(message, e);
      }

      throw new SOFIException(message, e);
    }
  }

  @Override
  public int doEndTag() throws JspException {
    try {

      Utilisateur utilisateur =
          UtilitaireControleur.getUtilisateur(this.pageContext.getSession());
      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // L'utilisateur à droit de voir le composant
        if (!GestionSecurite.getInstance().isUtilisateurAccesObjetSecurisable(utilisateur,
            getLibelle())) {
          return SKIP_BODY;
        }
      }

      StringBuffer nomListeColonne = new StringBuffer("SOFI_LISTE_COLONNE");
      nomListeColonne.append(getProperty());

      List listeColonne =
          (List)pageContext.getAttribute(nomListeColonne.toString());

      if (getHref() != null) {
        if (listeColonne == null) {
          listeColonne = new ArrayList();
          pageContext.setAttribute(nomListeColonne.toString(), listeColonne);
        }

        try {
          listeColonne.add(this.clone());
        } catch (Exception e) {
          if (log.isErrorEnabled()) {
            log.error("Erreur lors du clonage du tag de colonne courant dans le doEndTag.",
                e);
          }
        }
      }

      return super.doEndTag();
    } catch (Exception e) {
      String message = "Erreur lors du traitement doEndTag d'une colonne.";

      if (log.isErrorEnabled()) {
        log.error(message, e);
      }

      throw new SOFIException(message, e);
    } finally {
      cleanUp();
    }
  }

  /**
   * Retourne l'adresse spécifier comme href dans la balise.
   * @return l'adresse spécifier comme href dans la balise.
   */
  public Href getAdresseBalise() {
    if (getHref() == null) {
      return null;
    } else {
      return new Href(getHrefString());
    }
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) de la colonne.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    if (getFormat() != null) {
      String format =
          EvaluateurExpression.evaluerString("format", getFormat(), this,
              pageContext);
      setFormat(format);
    }

    if (getLibelle() != null) {
      String libelle =
          EvaluateurExpression.evaluerString("libelle", getLibelle(), this,
              pageContext);
      setLibelle(libelle);
    }

    if (getHrefString() != null) {
      String href =
          EvaluateurExpression.evaluerString("hrefString", getHrefString(), this,
              pageContext);
      setHref(href);
    }

    if (getAideContextuelle() != null) {
      String aideContextuelle =
          EvaluateurExpression.evaluerString("aideContextuelle",
              getAideContextuelle(), this, pageContext);
      setAideContextuelle(aideContextuelle);
    }

    if (getAideContextuelleCellule() != null) {
      String aideContextuelleCellulue =
          EvaluateurExpression.evaluerString("aideContextuelleCellule",
              getAideContextuelleCellule(), this, pageContext);
      setAideContextuelleCellule(aideContextuelleCellulue);
    }

    if (getClassCellule() != null) {
      String classCellule =
          EvaluateurExpression.evaluerString("classCellule", getClassCellule(),
              this, pageContext);
      setClassCellule(classCellule);
    }

    if (getCache() != null) {
      String cache =
          EvaluateurExpression.evaluerString("cache", getCache(), this,
              pageContext);
      setCache(cache);
    }

    if (getSousCache() != null) {
      Object sousCache =
          EvaluateurExpression.evaluer("sousCache", (String)getSousCache(),
              Object.class, this, pageContext);
      setSousCache(sousCache);
    }
  }
}
