/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * Balise Jsp qui permet d'offrir des méthodes utilitaire sur des listes.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 2.0.1
 * @since SOFI 2.0.2 Ajout d'une fonctionnalité qui permet de valider
 * si une certaine valeur dans la liste ou une valeur pour une propriété d'un objet de la liste.
 */
public class UtilitaireListeTag extends TagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 2013878012533488878L;

  /**
   * La liste a traiter.
   */
  private Object liste;

  /**
   * Le nom de variable temporaire qui loge le résultat de la balise.
   */
  private String var;

  /**
   * La position utilisé afin d'extraire une instance de la liste.
   */
  private String position;

  /**
   * La valeur servant de comparateur à utiliser pour valider si existe dans la liste.
   */
  private String valeurComparateur;

  /**
   * La propriété, optionnel, d'un objet de la liste servant de comparateur à utiliser pour valider si valeur existe dans la liste.
   */
  private String proprieteComparateur;

  /**
   * Constructeur par défault.
   */
  public UtilitaireListeTag() {
  }

  /**

  /**
   * Exécution de l'ouverture de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {

    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    List listeATraiter = null;
    Object valeurRetour = null;

    if (ListeNavigation.class.isInstance(getListe())) {
      ListeNavigation listeNavigation = (ListeNavigation)getListe();
      listeATraiter = listeNavigation.getListe();
    } else {
      listeATraiter = (List)getListe();
    }

    if ((getPosition() != null) && (listeATraiter != null)) {
      int position = new Integer(getPosition()).intValue();

      if (position <= (listeATraiter.size() - 1)) {
        valeurRetour = listeATraiter.get(position);
      }

    }

    if ((getValeurComparateur() != null) && (listeATraiter != null)) {
      Iterator iterateur = listeATraiter.iterator();

      String valeurATester = "";

      boolean existe = false;
      while (iterateur.hasNext() && !existe) {

        Object valeur = iterateur.next();

        if (getProprieteComparateur() != null) {
          try {
            valeurATester =
                UtilitaireObjet.getValeurAttribut(valeur, getProprieteComparateur()).toString();
          } catch (Exception e) {
          }
        } else {
          valeurATester = valeur.toString();
        }

        if (valeurATester.equals(getValeurComparateur())) {
          existe = true;
          valeurRetour = valeur;
        }
      }
    }

    this.pageContext.getRequest().setAttribute(getVar(), valeurRetour);
    return (SKIP_BODY);
  }

  /**
   * Exécution de la fin de la balise.
   * @return Décision à prendre pour le reste de la page JSP
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doEndTag() throws JspException {
    release();

    return (EVAL_PAGE);
  }

  /**
   * Évaluer les expressionn régulières
   */
  public void evaluerEL() throws JspException {
    Object liste =
        EvaluateurExpression.evaluer("liste", (String)getListe(), Object.class,
            this, pageContext);
    setListe(liste);

    String position =
        EvaluateurExpression.evaluerString("position", getPosition(), this,
            pageContext);
    setPosition(position);

    String valeurComparateur =
        EvaluateurExpression.evaluerString("valeurComparateur",
            getValeurComparateur(), this, pageContext);
    setValeurComparateur(valeurComparateur);

    String proprieteComparateur =
        EvaluateurExpression.evaluerString("proprieteComparateur",
            getProprieteComparateur(), this, pageContext);
    setProprieteComparateur(proprieteComparateur);
  }

  /**
   * Libération des resources acquises.
   */
  @Override
  public void release() {
    setVar(null);
    setPosition(null);
    setListe(null);
    super.release();
  }

  /**
   * Fixer le nom de la variable temporaire pour loger le résultat de la balise.
   * @param var le nom de la variable temporaire pour loger le résultat de la balise.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne le nom de la variable temporaire pour loger le résultat de la balise.
   * @return le nom de la variable temporaire pour loger le résultat de la balise.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer la position de l'instance a extraire dans la liste.
   * @param position la position de l'instance a extraire dans la liste.
   */
  public void setPosition(String position) {
    this.position = position;
  }

  /**
   * Retourne la postion de l'instance a extraire dans la liste.
   * @return la postion de l'instance a extraire dans la liste.
   */
  public String getPosition() {
    return position;
  }

  /**
   * Fixer le nom de la liste a traiter.
   * @param liste le nom de la liste a traiter.
   */
  public void setListe(Object liste) {
    this.liste = liste;
  }

  /**
   * Retourne le nom de la liste a traiter.
   * @return le nom de la liste a traiter.
   */
  public Object getListe() {
    return liste;
  }

  /**
   * Fixer la propriété, optionnel, servant de comparateur à utiliser pour valider si valeur existe dans la liste.
   * @param proprieteComparateur la propriété, optionnel, servant de comparateur à utiliser pour valider si valeur existe dans la liste.
   */
  public void setProprieteComparateur(String proprieteComparateur) {
    this.proprieteComparateur = proprieteComparateur;
  }

  /**
   * Retourne la propriété, optionnel, servant de comparateur à utiliser pour valider si valeur existe dans la liste.
   * @return la propriété, optionnel, servant de comparateur à utiliser pour valider si valeur existe dans la liste.
   */
  public String getProprieteComparateur() {
    return proprieteComparateur;
  }

  /**
   * Fixer la valeur servant de comparateur à utiliser pour valider si existe dans la liste.
   * @param valeurComparateur la valeur servant de comparateur à utiliser pour valider si existe dans la liste.
   */
  public void setValeurComparateur(String valeurComparateur) {
    this.valeurComparateur = valeurComparateur;
  }

  /**
   * Retourne la valeur servant de comparateur à utiliser pour valider si existe dans la liste.
   * @return la valeur servant de comparateur à utiliser pour valider si existe dans la liste.
   */
  public String getValeurComparateur() {
    return valeurComparateur;
  }
}
