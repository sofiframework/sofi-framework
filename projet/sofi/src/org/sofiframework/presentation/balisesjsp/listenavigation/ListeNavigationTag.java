/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.displaytag.exception.InvalidTagAttributeValueException;
import org.sofiframework.displaytag.tags.el.TableTag;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise permettant de traiter un liste de navigation page par page
 * avec tri automatisé.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class ListeNavigationTag extends TableTag {
  /**
   * 
   */
  private static final long serialVersionUID = 7899942301390057060L;
  public static String LISTE_NAVIGATION_TAG = "listeNavigationTag";
  private Log log = LogFactory.getLog(ListeNavigationTag.class);

  /**
   * Returns true if this tag should render as xhtml.
   */
  protected boolean isXhtml() {
    return TagUtils.getInstance().isXhtml(this.pageContext);
  }

  /**
   * Retourne la balise de fermeture correspondant au type de document
   * @return la balise de fermeture
   */
  protected String getElementClose(){
    return this.isXhtml() ? "/>" : ">";
  }

  /**
   * Retourne le nom de la liste.
   * @return le nom de la liste.
   */
  public Object getNomListe() {
    return getName();
  }

  /**
   * Fixer le nom de la liste.
   * @param nom le nom de la liste.
   */
  public void setNomListe(Object nomListe) {
    super.setSort("list");
    super.setDecorator(
        "org.sofiframework.presentation.balisesjsp.listenavigation.ListeNavigationDecorateur");
    setName(nomListe);
  }

  /**
   * Fixer le numéro de colonne du tri par défaut.
   * @param triDefaut le numéro de colonne du tri par défaut.
   */
  public void setTriDefaut(String triDefaut) {
    super.setDefaultsort(triDefaut);
  }

  /**
   * Retourne l'action "xxx.do" à appeler
   * @return l'action "xxx.do" à appeler
   */
  public String geAction() {
    return getRequestURI();
  }

  /**
   * Fixer l'action "xxx.do" à appeler
   * @param s
   */
  public void setAction(String s) {
    setRequestURI(s);
  }

  /**
   * Retourne le décorateur à utiliser pour la liste de navigation.
   * @return  le décorateur à utiliser pour la liste de navigation.
   */
  public String getDecorateur() {
    return getDecorator();
  }

  /**
   * Fixer le décorateur à utiliser pour la liste de navigation.
   * @param le décorateur à utiliser pour la liste de navigation.
   */
  public void setDecorateur(String s) {
    super.setDecorator(s);
  }

  /**
   * Retourne si on désire trier seulement les éléments de la page courante.
   * @return true si on désire trier seulement les élements de la page courante.
   */
  public boolean isTrierPageSeulement() {
    if ((getSort() != null) && getSort().equals("page")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer si on désire trier seulement les éléments de la page courante.
   * @param trierPageSeulement true si on désire trier seulement les élements de la page courante.
   */
  public void setTrierPageSeulement(boolean trierPageSeulement) {
    if (trierPageSeulement) {
      super.setSort("page");
    } else {
      super.setSort("list");
    }
  }

  /**
   * Fixer si on afficher l'entête de la liste même si la liste est vide.
   * @param afficheEnteteSiListeVide true si on désire afficher l'entête même si la liste est vide.
   */
  @Override
  public void setAfficheEnteteSiListeVide(String afficheEnteteSiListeVide) {
    if (afficheEnteteSiListeVide == null) {
      super.setAfficheEnteteSiListeVide("true");
    } else {
      super.setAfficheEnteteSiListeVide(afficheEnteteSiListeVide);
    }
  }

  /**
   * Fixer le tri par défaut en descendant.
   * @param triDescendantDefaut false si le tri par défaut n'est pas descendant
   */
  public void setTriDescendantDefaut(String triDescendantDefaut) {
    try {
      if (triDescendantDefaut == null) {
        setDefaultorder("ascending");
      } else {
        Boolean bTriDescendantDefaut = new Boolean(triDescendantDefaut);

        if (bTriDescendantDefaut.booleanValue()) {
          setDefaultorder("descending");
        } else {
          setDefaultorder("ascending");
        }
      }
    } catch (InvalidTagAttributeValueException e) {
    }
  }

  @Override
  public int doStartTag() throws JspException {
    try {
      if (EvaluateurExpression.isEvaluerEL(pageContext)) {
        evaluerEL();
      } else {
        if (this.getDivId() == null) {
          if (this.getRequestURI() != null) {
            /*IdentifiantUnique id = new IdentifiantUnique(
                                super.getPageContext().getRequest().getRemoteAddr());
                        this.setDivId(id.toString());*/
            this.setDivId(String.valueOf(this.getRequestURI().hashCode()));
          }else {
            this.setDivId("listeNavigation");
          }
        }
      }

      StringBuffer resultat = new StringBuffer("");

      if (isAjax() &&
          !UtilitaireAjax.isAffichageAjax(pageContext.getRequest())) {
        if (!UtilitaireString.isVide(getColonneLienDefaut())) {
          StringBuffer resultatFonctionJSApresChargement = new StringBuffer("");

          resultatFonctionJSApresChargement.append("miseEnValeurListe('");
          resultatFonctionJSApresChargement.append("table_");
          resultatFonctionJSApresChargement.append(getDivId());
          resultatFonctionJSApresChargement.append("','");
          resultatFonctionJSApresChargement.append(getColonneLienDefaut());
          resultatFonctionJSApresChargement.append("');");
          pageContext.setAttribute("fonctionJSApresChargement",
              resultatFonctionJSApresChargement.toString());

          if (pageContext.getRequest().getParameter("formulaireValeurRetour") != null) {
            // Générer champ input
            resultat.append("<input type=\"hidden\" id=\"jsListeNavigation\"");
            resultat.append(" onclick=\"");
            resultat.append(resultatFonctionJSApresChargement.toString());
            // Fermeture de la balise selon la valeur de l'attribut xhtml de la balise
            // <html:html> de struts-html
            //            resultat.append("\"/>");
            resultat.append("\"");
            resultat.append(this.getElementClose());
          }
        } else {
          if (pageContext.getRequest().getParameter("formulaireValeurRetour") != null) {
            // @todo offrir la possiblité de mettre d'autres évènement de chargement.
            resultat.append("<input type=\"hidden\" id=\"jsListeNavigation\"");
            resultat.append("onclick=\"");
            resultat.append("\"");
            // Fermeture de la balise selon la valeur de l'attribut xhtml de la balise
            // <html:html> de struts-html
            //            resultat.append("/>");
            resultat.append(this.getElementClose());
          }
        }

        boolean valeurRetour = (pageContext.getRequest().getParameter("formulaireValeurRetour") != null);
        boolean pasAjax = (pageContext.getRequest().getParameter("ajax") == null);
        boolean ajaxRafraichir = (pageContext.getRequest().getParameter("ajax_rafraichir") != null);
        boolean divPersistant = (pageContext.getRequest().getParameter("divPersistant") != null);
        boolean divOuvert = (pageContext.getRequest().getParameter("divOuvert") != null);

        if (valeurRetour || pasAjax || ajaxRafraichir || divPersistant || divOuvert) {
          resultat.append("<div id=\"");
          resultat.append(getDivId());
          resultat.append("\">");
        }
      } else {
        resultat.append("<div id=\"");
        resultat.append(getDivId());
        // Non conforme au W3C.
        //        resultat.append("\"/>");
        resultat.append("\"></div>");
      }

      TagUtils.getInstance().write(pageContext, resultat.toString());

      return super.doStartTag();
    } catch (Exception e) {
      String message = "Erreur lors du traitement doStartTag de la balise ListeNavigation.";

      if (log.isErrorEnabled()) {
        log.error(message, e);
      }

      throw new SOFIException(message, e.getCause());
    }
  }

  @Override
  public int doEndTag() throws JspException {
    try {
      int retour = super.doEndTag();

      StringBuffer resultat = new StringBuffer("");

      if (!UtilitaireString.isVide(getColonneLienDefaut()) &&
          (!isAjax() ||
              (pageContext.getRequest().getParameter("ajax") == null))) {
        resultat.append("<script type=\"text/javascript\">miseEnValeurListe(\"");
        resultat.append("table_");
        resultat.append(getDivId());
        resultat.append("\",\"");
        resultat.append(getColonneLienDefaut());
        resultat.append("\");</script>");
      }

      if (isAjax() &&
          !UtilitaireAjax.isAffichageAjax(pageContext.getRequest()) &&
          ((pageContext.getRequest().getParameter("formulaireValeurRetour") != null) ||
              ((pageContext.getRequest().getParameter("ajax") == null) ||
                  (pageContext.getRequest().getParameter("ajax_rafraichir") != null)))) {
        resultat.append("</div>");
      }

      TagUtils.getInstance().write(pageContext, resultat.toString());

      pageContext.removeAttribute("compteurColonneListeNavigation");
      pageContext.removeAttribute("compteurColonneMultibox");
      pageContext.removeAttribute("compteurLigneListeNavigation");

      return retour;
    } catch (Exception e) {
      String message = "Erreur lors du traitement doEndTag de la balise ListeNavigation.";

      if (log.isErrorEnabled()) {
        log.error(message, e);
      }

      throw new SOFIException(e);
    } finally {
      colonneLienDefaut = null;
      cleanUp();
    }
  }

  @Override
  public ArrayList getListeParametresSelection() {
    return super.getListeParametresSelection();
  }

  @Override
  public void setListeParametresSelection(ArrayList listeParametresSelection) {
    super.setListeParametresSelection(listeParametresSelection);
    ;
  }

  @Override
  public HashMap getListeParametresCleSelection() {
    return super.getListeParametresCleSelection();
  }

  @Override
  public void setListeParametresCleSelection(
      HashMap listeParametresCleSelection) {
    super.setListeParametresCleSelection(listeParametresCleSelection);
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) de la colonne.
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  protected void evaluerEL() throws JspException {
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    String nomListeId = (String) getNomListe();

    // On vérifier s'il y a présence d'un divId si oui, on évalue le el
    if (!UtilitaireString.isVide(getDivId())) {
      String divId = EvaluateurExpression.evaluerString("divId",
          getDivId(), this, pageContext);
      setDivId(divId);
    }

    if ((formulaire == null) ||
        ((formulaire != null) && !formulaire.isFormulaireContenaitDesErreurs())) {
      // Si aucun divId n'est affecté, on génère le divId avec le nom de la liste
      if (UtilitaireString.isVide(getDivId())) {
        if (nomListeId.substring(0, 1).equals("$")) {
          String id = nomListeId.substring(2, nomListeId.length() - 1);
          setDivId(id);

        }
      }
    }

    if (getColgroup() != null) {
      String colgroup = EvaluateurExpression.evaluerString("colgroup",
          getColgroup(), this, pageContext);
      setColgroup(colgroup);
    }

    super.evaluerEL();
  }
}
