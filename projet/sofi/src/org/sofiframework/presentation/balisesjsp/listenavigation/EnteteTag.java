/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import javax.servlet.jsp.JspException;

import org.sofiframework.constante.ConstantesBaliseJSP;


/**
 * Balise permettant de créer une entête de liste de navigation personnalisé.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.5
 */
public class EnteteTag extends javax.servlet.jsp.tagext.BodyTagSupport {

  /**
   * 
   */
  private static final long serialVersionUID = -1721463408608789662L;
  /**
   * Le nom de variable temporaire qui loge l'entete.
   */
  private String var;
  private boolean gabaritBasListe = false;

  @Override
  public int doStartTag() throws JspException {
    if (getVar() == null) {
      if (isGabaritBasListe()) {
        setVar(ConstantesBaliseJSP.ENTETE_BAS_LISTE_NAVIGATION_DEFAUT);
      } else {
        setVar(ConstantesBaliseJSP.ENTETE_LISTE_NAVIGATION_DEFAUT);
      }
    }

    return super.doStartTag();
  }

  @Override
  public int doEndTag() throws JspException {
    // Traiter le body de la balise.
    if (bodyContent != null) {
      String valeur = bodyContent.getString();

      if (valeur == null) {
        valeur = "";
      }

      this.pageContext.getServletContext().setAttribute(getVar(), valeur);
    }

    gabaritBasListe = false;
    var = null;

    return super.doEndTag();
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getVar() {
    return var;
  }

  public void setGabaritBasListe(boolean gabaritBasListe) {
    this.gabaritBasListe = gabaritBasListe;
  }

  public boolean isGabaritBasListe() {
    return gabaritBasListe;
  }
}
