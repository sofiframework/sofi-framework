/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.listenavigation;

import java.util.HashMap;
import java.util.HashSet;


public class InfoListeCouranteMultibox {
  private HashMap listeSelectionnee;
  private String nomAttribut;
  private HashSet listeIdentifiantListeCourante;

  public InfoListeCouranteMultibox() {
    listeIdentifiantListeCourante = new HashSet();
  }

  public HashMap getListeSelectionnee() {
    return listeSelectionnee;
  }

  public void setListeSelectionnee(HashMap listeSelectionnee) {
    this.listeSelectionnee = listeSelectionnee;
  }

  public String getNomAttribut() {
    return nomAttribut;
  }

  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  public HashSet getListeIdentifiantListeCourante() {
    return listeIdentifiantListeCourante;
  }

  public void setListeIdentifiantListeCourante(
      HashSet listeIdentifiantListeCourante) {
    this.listeIdentifiantListeCourante = listeIdentifiantListeCourante;
  }

  public void ajouterIdentifiantListeCourante(Object identifiant) {
    this.listeIdentifiantListeCourante.add(identifiant);
  }

  public String getNomParametreRequete() {
    return getNomAttribut() + "Param";
  }
}
