/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.menu;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Balise permettant de spécifier un menu basé sur un gabarit Velocity.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class MenuTag extends TagSupport {

  private static final long serialVersionUID = -2909228286387766893L;

  public static final String CLE_AFFICHEUR = "org.sofiframework.presentation.balisesjsp.menu.afficheur";
  public static final String GABARIT = "sofiGabarit";
  protected AfficheurMenuVelocity afficheur = null;

  /**
   * Le nom du menu
   */
  private String nom;

  /**
   * Le nom du gabarit
   */
  private String gabaritVelocity;

  /**
   * Permet d'inactiver la sélection des menus, par conséquent,
   * les menus représentent seulement la navigation de l'utilisateur.
   */
  private String inactiverSelection;

  /**
   * Spécifier une clé de message lorsque la sélection du menu n'est pas
   * possible.
   */
  private String cleMessageSelectionInactif;

  /**
   * Retourne le nom du menu.
   * @return le nom du menu.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du menu.
   * @param nom le nom du menu.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le fichier gabarit de type Velocity.
   * @return le fichier gabarit de type Velocity.
   */
  public String getGabaritVelocity() {
    return gabaritVelocity;
  }

  /**
   * Fixer le fichier gabarit de type Velocity.
   * @param gabaritVelocity le fichier gabarit de type Velocity.
   */
  public void setGabaritVelocity(String gabaritVelocity) {
    this.gabaritVelocity = gabaritVelocity;
  }

  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    //Extraire le répertoire de menu
    RepertoireMenu menuRep = (RepertoireMenu) pageContext.findAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);

    if (menuRep == null) {
      throw new JspException("Menu non trouvé");
    }

    //extraire l'afficheur velocity.
    afficheur = menuRep.getAfficheurMenu();

    afficheur.setGabaritVelocity(gabaritVelocity);

    afficheur.init(pageContext);

    pageContext.setAttribute(CLE_AFFICHEUR, afficheur);

    pageContext.setAttribute(GABARIT, gabaritVelocity);

    if (isInactivationOnglet()) {
      pageContext.setAttribute(RepertoireMenu.INACTIVER_SELECTION, Boolean.TRUE);

      if (getCleMessageSelectionInactif() != null) {
        pageContext.setAttribute(RepertoireMenu.CLE_MESSAGE_SELECTION_INACTIF,
            getCleMessageSelectionInactif());
      }
    }

    return (EVAL_BODY_INCLUDE);
  }

  @Override
  public int doEndTag() throws JspException {
    afficheur.fin(pageContext);
    pageContext.removeAttribute(CLE_AFFICHEUR);
    pageContext.removeAttribute(RepertoireMenu.INACTIVER_SELECTION);
    pageContext.removeAttribute(RepertoireMenu.CLE_MESSAGE_SELECTION_INACTIF);
    setInactiverSelection("false");
    setCleMessageSelectionInactif(null);
    pageContext.setAttribute(GABARIT, null);

    return (EVAL_PAGE);
  }

  /**
   * Fixer true si vous désirez désactivé la sélection des menus.
   * @param inactiverSelection true si vous désirez désactivé la sélection des menus.
   */
  public void setInactiverSelection(String inactiverSelection) {
    this.inactiverSelection = inactiverSelection;
  }

  /**
   * Est-ce que le composants de menu doit désactivé la sélection de ces menus
   * @return true si vous désirez désactivé la sélection des menus.
   */
  public String getInactiverSelection() {
    return inactiverSelection;
  }

  /**
   * Traiter l'expression régulière (EL) pour l'adresse web (href) du bouton.
   * @throws javax.servlet.jsp.JspException
   */
  protected void evaluerEL() throws JspException {
    if ((getInactiverSelection() != null) &&
        (pageContext.getSession().getAttribute(Constantes.FORMULAIRE_EN_COURS) != null)) {
      String inactiverSelection = EvaluateurExpression.evaluerString("inactiverSelection",
          getInactiverSelection(), this, pageContext);
      setInactiverSelection(inactiverSelection);
    } else {
      setInactiverSelection("false");
    }

    String cleMessageSelectionInactif = EvaluateurExpression.evaluerString("cleMessageSelectionInactif",
        getCleMessageSelectionInactif(), this, pageContext);
    setCleMessageSelectionInactif(cleMessageSelectionInactif);

    String gabaritVelocity = EvaluateurExpression.evaluerString("gabaritVelocity",
        getGabaritVelocity(), this, pageContext);
    setGabaritVelocity(gabaritVelocity);
  }

  private boolean isInactivationOnglet() {
    if ((inactiverSelection == null) || inactiverSelection.equals("false")) {
      return false;
    } else {
      return true;
    }
  }

  public void setCleMessageSelectionInactif(String cleMessageSelectionInactif) {
    this.cleMessageSelectionInactif = cleMessageSelectionInactif;
  }

  public String getCleMessageSelectionInactif() {
    return cleMessageSelectionInactif;
  }
}
