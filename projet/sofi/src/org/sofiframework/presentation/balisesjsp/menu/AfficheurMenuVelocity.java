/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.menu;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.tools.VelocityFormatter;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.composantweb.menu.ComposantMenu;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Afficheur de menu avec l'aide du framework Velocity.
 * @author Jean-François Brassard (Nurun inc)
 * @version SOFI 1.0
 */
public class AfficheurMenuVelocity {
  /**
   * Clé utilisé pour acceder au servlet context dans
   * les attributs applicate de velocity.
   */
  public static final String SERVLET_CONTEXT_KEY =
      ServletContext.class.getName();

  /**
   * Instance commune de journalisation.
   */
  private Log log =
      LogFactory.getLog(org.sofiframework.presentation.balisesjsp.menu.AfficheurMenuVelocity.class);

  /**
   * Le contexte de la page en traitement.
   */
  private PageContext pageContext = null;

  /**
   * Le nom du menu
   */
  private String nom;

  /**
   * Le nom du gabarit
   */
  private String gabaritVelocity;

  /**
   * Le libellé du menu
   */
  private Libelle libelle;

  /**
   * Initialisation.
   * @param pageContext le contexte de la page
   */
  public void init(PageContext pageContext) {
    this.pageContext = pageContext;
  }

  /**
   * Affichage du menu.
   * @param menu le menu en traitement.
   * @param pageContext le contexte de la page.
   * @throws javax.servlet.jsp.JspException Exception JSP
   * @throws java.io.IOException erreur en entré-sortie
   */
  public void affichage(ComposantMenu menu) throws JspException, IOException {
    affichageComposantsMenu(menu);
  }

  /**
   *
   * @param menu
   * @throws javax.servlet.jsp.JspException
   * @throws java.io.IOException
   */
  protected void affichageComposantsMenu(ComposantMenu menu) throws JspException,
  IOException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    Template t = null;

    try {
      String template = getGabaritVelocity();

      if (template == null) {
        throw new JspException("Vous devez spécifier un tempate utilisant l'attribut gabaritVelocity.");
      }

      t = Velocity.getTemplate(template);
    } catch (Exception e) {
      String msg = "Erreur d'initialisation de Velocity: " + e.toString();
      log.error(msg, e);
      throw new JspException(msg, e);
    }

    StringWriter sw = new StringWriter();
    VelocityContext context = new VelocityContext();

    context.put("formatter", new VelocityFormatter(context));
    context.put("now", Calendar.getInstance().getTime());
    context.put("ctxPath", request.getContextPath());

    // add a Map for use by the Explandable List Menu
    context.put("map", new HashMap());

    // vérifier si le nom est présent
    if (!UtilitaireString.isVide(nom)) {
      Object val1 =
          TagUtils.getInstance().lookup(pageContext, getNom(), null, null);

      if (val1 != null) {
        context.put(getNom(), val1);
      }
    }

    // Attribut dans la porté request
    Enumeration enumeration = request.getAttributeNames();

    while (enumeration.hasMoreElements()) {
      String name = (String)enumeration.nextElement();
      Object value = request.getAttribute(name);
      context.put(name, value);
    }

    context.put("request", request);
    context.put("session", request.getSession());

    // Traiter les url externe
    if (menu.getAdresseWeb() == null) {
      menu.setUrlExiste(false);
    } else {
      if (menu.getAdresseWeb().indexOf("$") != -1) {
        menu.setUrlExiste(false);
      } else {
        menu.setUrlExiste(true);
      }
    }

    context.put("menu", menu);
    context.put("displayer", this);

    try {
      t.merge(context, sw);
    } catch (Exception e) {
      e.printStackTrace();
      throw new JspException(e);
    }

    String result = sw.getBuffer().toString();

    // Écrire l'élément dans la sortie
    TagUtils.getInstance().write(pageContext, result);
  }

  public void fin(PageContext context) {
    this.pageContext = null;
  }

  /**
   * Retourne à velocity le libellé du composant de menu
   * @param menu le composant de menu
   * @return le libellé du composant de menu
   */
  public String getMessage(ComposantMenu menu) {
    Libelle libelle = null;

    if (!UtilitaireString.isVide(menu.getLibelle())) {
      libelle = UtilitaireLibelle.getLibelle(menu.getLibelle(), pageContext);
      setLibelle(libelle);
    }

    // Retirer tous les retours de chariots incluts dans le libelle de composant de menu pour le fil d'ariane.
    String message = libelle.getMessage();
    if (isGabaritFilNavigation()) {
      message = UtilitaireString.remplacerTous(message, "<br/>", " ");
      message = UtilitaireString.remplacerTous(message, "<br />", " ");
    }

    if (menu.isLibellePremiereLettreEnMajuscule()) {
      return UtilitaireString.convertirPremiereLettreSeulementEnMajuscule(message);
    } else {
      return message;
    }
  }

  /**
   * Retourne à velocity l'aide contextuelle du composant de menu
   * @param menu le composant de menu
   * @return l'aide contextuelle du composant de menu
   */
  public String getAideContextuelle(ComposantMenu menu) {
    Libelle libelle = null;

    if (getLibelle() == null) {
      getMessage(menu);
    }

    if (!UtilitaireString.isVide(getLibelle().getAideContextuelle())) {
      return getLibelle().getAideContextuelle();
    }

    if (!UtilitaireString.isVide(menu.getAideContextuelle())) {
      libelle =
          UtilitaireLibelle.getLibelle(menu.getAideContextuelle(), pageContext);

      return libelle.getMessage();
    } else {
      return "";
    }
  }

  /**
   * Fixer le nom du menu
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom du menu
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le gabarit velocity
   */
  public void setGabaritVelocity(String gabaritVelocity) {
    this.gabaritVelocity = gabaritVelocity;
  }

  /**
   * Retourne le gabarit velocity
   */
  public String getGabaritVelocity() {
    return gabaritVelocity;
  }

  /**
   * Fixer le libellé du menu
   * @param libelle le libellé du menu
   */
  public void setLibelle(Libelle libelle) {
    this.libelle = libelle;
  }

  /**
   * Retourne le libellé du menu
   * @return le libellé du menu
   */
  public Libelle getLibelle() {
    return libelle;
  }

  /**
   * Est-ce que le gabarit en traitement traite un fil de navigation?
   * @return true si le gabarit en traitement traite un fil de navigation
   */
  public boolean isGabaritFilNavigation() {
    if ((getGabaritVelocity() != null) &&
        (getGabaritVelocity().toString().indexOf("fil_navigation") != -1)) {
      return true;
    } else {
      return false;
    }
  }
}
