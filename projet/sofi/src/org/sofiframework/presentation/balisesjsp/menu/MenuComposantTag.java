/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.menu;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.composantweb.menu.ComposantMenu;
import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;


/**
 * Balise permettant d'extraire un composant du menu.
 * <p>
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 2.0.6
 */
public class MenuComposantTag extends TagSupport {

  /**
   * 
   */
  private static final long serialVersionUID = -3013751895217852790L;

  /**
   * Instance de journalisation
   */
  private Log log = LogFactory.getLog(MenuComposantTag.class);

  /**
   * Identifiant du menu
   */
  private String identifiant = null;

  /**
   * Le nom de variable temporaire qui loge l'instance du composant.
   */
  private String var = null;


  public MenuComposantTag() {
  }

  /**
   * Retourne l'identifiant de menu que l'on désire accéder
   * @return l'identifiant de menu
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant de menu
   * @param identifiant l'identifiant de menu
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Fixer la variable temporaire qui loge l'instance du composant.
   * @param var la variable temporaire qui loge l'instance du composant.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Retourne la variable temporaire qui loge l'instance du composant.
   * @return la variable temporaire qui loge l'instance du composant.
   */
  public String getVar() {
    return var;
  }

  /**
   * Initialisation de la balise de Menu qui gère les sections parent du menu
   * @return
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    // Évaluer les expression régulière.
    evaluerEL();

    RepertoireMenu repertoireMenu =
        (RepertoireMenu)pageContext.getSession().getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);

    if (repertoireMenu == null || repertoireMenu.getTousLesMenus() == null) {
      throw new JspException("Le répertoire de menu est inexistant");
    }

    ComposantMenu menu =
        repertoireMenu.getComposantMenuDeTouslesMenus("menu" + getIdentifiant());

    if (menu != null) {
      menu = (ComposantMenu)menu.clone();
    } else {
      log.error("Menu non trouvé");
    }

    //Conserver le menu dans le contexte pour utilisation dans le doEndTag.
    pageContext.getRequest().setAttribute(getVar(), menu);


    return (SKIP_BODY);
  }

  @Override
  public int doEndTag() throws JspException {

    return (EVAL_PAGE);
  }


  /**
   * Libérer les ressources
   */
  @Override
  public void release() {
    super.release();
    setIdentifiant(null);
    setVar(null);
  }

  /**
   * Permet de traiter les expressions JSTL.
   * @exception JspException si une exception JSP est traité
   */
  private void evaluerEL() throws JspException {
    String resultat =
        EvaluateurExpression.evaluerString("identifiant", getIdentifiant(), this,
            pageContext);
    setIdentifiant(resultat);
  }
}
