/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.menu;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.directive.Directive;
import org.apache.velocity.runtime.parser.ParserTreeConstants;
import org.apache.velocity.runtime.parser.node.ASTReference;
import org.apache.velocity.runtime.parser.node.Node;
import org.apache.velocity.runtime.parser.node.SimpleNode;


/**
 *   Classe technique utilisé pour la génération de menu, afin d'offrir
 *   la possibilité d'utiliser une valeur local pour un itération.
 *
 *   Ex :
 *
 *   #set($foo = 1)
 *   $foo
 *   #local($foo)
 *      #set($foo = 2)
 *      $foo
 *   #end
 *    $foo
 *
 *    devrait afficher
 *
 *    1
 *       2
 *    1
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class LocalDirective extends Directive {
  @Override
  public String getName() {
    return "local";
  }

  @Override
  public int getType() {
    return BLOCK;
  }

  @Override
  public boolean render(InternalContextAdapter context, Writer writer, Node node)
      throws IOException, MethodInvocationException, ResourceNotFoundException,
      ParseErrorException {
    Map data = new HashMap();

    int num = node.jjtGetNumChildren();

    /*
     * extraires les références
     */
    for (int i = 0; i < num; i++) {
      SimpleNode child = (SimpleNode) node.jjtGetChild(i);

      /*
       * si un block, juste exécutement
       */
      if (child.getType() == ParserTreeConstants.JJTBLOCK) {
        child.render(context, writer);

        break;
      } else {
        /* sauvegarder les valeurs */
        if (child.getType() == ParserTreeConstants.JJTREFERENCE) {
          data.put(child, child.execute(null, context));
        }
      }
    }

    Iterator it = data.keySet().iterator();

    while (it.hasNext()) {
      ASTReference ref = (ASTReference) it.next();

      ref.setValue(context, data.get(ref));
    }

    return true;
  }
}
