/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.menu;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Onglet;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.composantweb.menu.ComposantMenu;
import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise permettant de spécifier les sections parent (ceux de plus haut niveau) d'un menu.
 * <p>
 * 
 * @author : Jean-François Brassard (Nurun inc.)
 * @version : SOFI 1.0
 */
public class MenuSectionTag extends TagSupport {
  private static final long serialVersionUID = 1851240542801061647L;

  /** Variable qui identifie si le menu enfant de la section est déjà en mode consultation */
  private static final String INDICATEUR_MENU_TROUVE = "indicateurMenuTrouve";

  /**
   * Instance de journalisation
   */
  private Log log = LogFactory.getLog(MenuSectionTag.class);

  /**
   * Identifiant du menu
   */
  private String identifiant = null;

  /**
   * La cible du menu
   */
  private String cible = null;

  /**
   * Afficher le libelle avec seulement la première lettre en majuscule.
   */
  private String libellePremiereLettreEnMajuscule;

  /**
   * Spécifie le niveau d'onglet à traiter
   */
  private String niveauOnglet;

  /**
   * Spécifie le numéro d'itérateur du menu. La première occurence est spécifié par 1.
   */
  private String noMenu;

  /**
   * Désactiver le service sélectionné.
   */
  private boolean desactiverServiceSelectionne;

  /**
   * Exclure la sélection automatique de l'onglet enfant lorsque service sélectionné.
   */
  private boolean exclureSelectionOngletEnfant;

  public MenuSectionTag() {
  }

  /**
   * Retourne l'identifiant de menu que l'on désire accéder
   * 
   * @return l'identifiant de menu
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant de menu
   * 
   * @param identifiant
   *          l'identifiant de menu
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Fixer la cible du menu
   * 
   * @param cible
   *          cible du menu
   */
  public void setCible(String cible) {
    this.cible = cible;
  }

  /**
   * Retourne la cible du menu
   * 
   * @return cible du menu
   */
  public String getCible() {
    return cible;
  }

  /**
   * Initialisation de la balise de Menu qui gère les sections parent du menu
   * 
   * @return
   * @throws javax.servlet.jsp.JspException
   */
  @Override
  public int doStartTag() throws JspException {
    try {
      // Évaluer les expression régulière.
      evaluerEL();

      RepertoireMenu repertoireMenu = (RepertoireMenu) pageContext.getSession().getAttribute(
          RepertoireMenu.CLE_REPERTOIRE_MENU);

      if (repertoireMenu == null || repertoireMenu.getTousLesMenus() == null) {
        throw new JspException("Le répertoire de menu est inexistant");
      }

      ComposantMenu menu = repertoireMenu.getComposantMenuDeTouslesMenus(getIdentifiant());

      if (menu != null) {
        menu = (ComposantMenu) menu.clone();
      } else {
        log.error("Menu non trouvé");
      }

      if (getAfficheurMenuVelocity() != null && getAfficheurMenuVelocity().isGabaritFilNavigation()) {
        if (menu.getOrdreAffichage().intValue() == -1000) {
          menu.setOrdreAffichage(new Integer(1000));
        }
      }

      // Conserver le menu dans le contexte pour utilisation dans le doEndTag.
      pageContext.setAttribute("composantMenuTraitement", menu);

      modificationFilNavigation(menu, pageContext);

      // Si disponible spécifier le numéro de menu
      if (getNoMenu() != null) {
        int compteur = Integer.parseInt(getNoMenu()) + 1;
        menu.setNoMenu((new Integer(compteur)));
      }

      // String adressseActionCourante = Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL;
      ObjetSecurisable objetSecurisable = UtilitaireControleur.getServiceCourantParent(pageContext.getSession());

      StringBuffer nomOngletATraiter = new StringBuffer(Constantes.ONGLET_SELECTIONNEE);
      boolean niveauOnglet1 = true;

      if ((getNiveauOnglet() != null) && !getNiveauOnglet().equals("1")) {
        nomOngletATraiter.append(getNiveauOnglet());
        niveauOnglet1 = false;
      }

      ObjetSecurisable ongletSelectionne = (ObjetSecurisable) pageContext.getSession().getAttribute(
          nomOngletATraiter.toString());
      Onglet ongletEnfantASelectionne = null;

      Integer selectionOngletNiveau = (Integer) pageContext.getRequest().getAttribute("selectionOngletNiveau");

      if ((selectionOngletNiveau == null)
          || ((getNiveauOnglet() != null) && (selectionOngletNiveau.intValue() != Integer.parseInt(getNiveauOnglet())))) {
        // Valider si pour un service sélectionné il existe un onglet, si oui
        // on doit le sélectionner.
        if (objetSecurisable != null) {
          if ((getNiveauOnglet() == null) || getNiveauOnglet().equals("1")) {
            // adressseActionCourante = objetSecurisable.getAdresseWeb();
            if ((ongletSelectionne == null) && (objetSecurisable instanceof Service)) {
              if ((objetSecurisable.getListeObjetSecurisableEnfants() != null)
                  && (objetSecurisable.getListeObjetSecurisableEnfants().size() > 0)
                  && (objetSecurisable.getListeObjetSecurisableEnfants().get(0) != null)) {
                ObjetSecurisable enfant = (ObjetSecurisable) objetSecurisable.getListeObjetSecurisableEnfants().get(0);

                if (enfant instanceof Onglet) {
                  ongletEnfantASelectionne = (Onglet) enfant;
                }
              }
            }
          }
        }

        // Valider si l'onglet sélecionné a un onglet enfant d'un niveau
        // supérieur si oui son enfant doit etre sélectionné
        if ((pageContext.getAttribute("ongletASelectionne") == null) && ongletSelectionne instanceof Onglet
            && (ongletSelectionne.getListeObjetSecurisableEnfants().size() > 1)
            && ongletSelectionne.getListeObjetSecurisableEnfants().get(0) instanceof Onglet) {
          Onglet ongletEnfant = (Onglet) ongletSelectionne.getListeObjetSecurisableEnfants().get(0);
          Onglet ongletSelectionnne = (Onglet) ongletSelectionne;
          Integer niveauOngletSelectionne = new Integer(1);

          if (ongletSelectionnne.getNiveauOnglet() != null) {
            niveauOngletSelectionne = ongletSelectionnne.getNiveauOnglet();
          }

          if ((ongletEnfant.getNiveauOnglet() != null)
              && (ongletEnfant.getNiveauOnglet().intValue() > niveauOngletSelectionne.intValue())) {
            ongletEnfantASelectionne = ongletEnfant;
          }
        }

        if (ongletEnfantASelectionne != null) {
          if (pageContext.getAttribute("ongletASelectionne") == null) {
            pageContext.getRequest().setAttribute("ongletASelectionne", ongletEnfantASelectionne.getAdresseWeb());
          }
        }
      }

      HashSet repertoireComposantInterfaceExclus = UtilitaireControleur
          .getRepertoireComposantInterfaceExclus(pageContext.getSession());

      String adresseOngletASelectionner = (String) pageContext.getRequest().getAttribute("ongletASelectionne");

      if (!niveauOnglet1 && (ongletSelectionne != null)) {
        pageContext.getRequest().setAttribute("ongletASelectionne", "");
      }

      if ((menu != null)
          && ((repertoireComposantInterfaceExclus == null) || ((repertoireComposantInterfaceExclus != null) && !repertoireComposantInterfaceExclus
              .contains(menu.getLibelle())))) {

        try {
          try {
            // Permet de traiter les paramètres à un onglet.
            HashMap parametresOnglet = (HashMap) pageContext.getSession().getAttribute("parametresUrlOnglet");

            String adresseOngletSelectionne = "";

            if (ongletSelectionne != null) {
              adresseOngletSelectionne = ongletSelectionne.getAdresseWeb();
            }

            if ((menu.getAction() != null) && !menu.getAction().equals("")) {
              menu.setLocationDeBase(menu.getAction());
            }

            if (((menu.getAdresseWeb() != null) || (menu.getLocationDeBase() != null))) {
              String location = null;

              if (menu.getLocationDeBase() != null) {
                location = menu.getLocationDeBase();
              } else {
                location = menu.getAdresseWeb().substring(1);

                int separateur = location.indexOf("/") + 1;
                location = location.substring(separateur);
                menu.setLocationDeBase(location);
              }

              // Spécifier si le menu est sélectionné
              if (menu.isOnglet()) {
                if (adresseOngletSelectionne.equals(menu.getLocationDeBase())
                    || (menu.getAction().equals(adresseOngletASelectionner) && !isExclureSelectionOngletEnfant())) {
                  menu.setSelectionne(true);
                } else {
                  menu.setSelectionne(false);
                }
              }

              // Spécifier les parents du menu comme sélectionné.
              // Si le parent de l'onglet est sélectionné et que l'objet dans la request n'est pas
              // présent alors on sélectionne seulement le premier onglet
              if ((this.pageContext.getSession().getAttribute(Constantes.TYPE_SERVICE_SELECTIONNE) != null)
                  && menu.isOnglet() && (this.pageContext.getRequest().getAttribute(INDICATEUR_MENU_TROUVE) == null)) {
                menu.setSelectionne(true);
                this.pageContext.getRequest().setAttribute(INDICATEUR_MENU_TROUVE, Boolean.TRUE);
                this.pageContext.getSession().removeAttribute(Constantes.TYPE_SERVICE_SELECTIONNE);
              }

              if (parametresOnglet != null) {
                HashMap parametres = (HashMap) parametresOnglet.get(location);

                if ((parametres != null) && isInjectionParametreOnglet()) {
                  Href url = new Href(menu.getLocationDeBase());
                  url.setListeParametres(parametres);

                  String nouveauUrl = url.toString();
                  menu.setAdresseWeb(nouveauUrl);
                }
              }
            }

            if (pageContext.getAttribute(RepertoireMenu.INACTIVER_SELECTION) == null) {
              menu.setSelectionDesative(false);

              if ((menu.getAction() != null) && (menu.getAdresseWeb() == null)) {
                menu.setAdresseWeb(menu.getAction());
              }

              setAdresseWeb(menu);
            } else {
              if (pageContext.getAttribute(RepertoireMenu.CLE_MESSAGE_SELECTION_INACTIF) != null) {
                String cleMessage = (String) pageContext.getAttribute(RepertoireMenu.CLE_MESSAGE_SELECTION_INACTIF);
                Message message = UtilitaireMessage.getMessage(cleMessage, UtilitaireBaliseJSP.getLocale(pageContext)
                    .getLanguage(), null);
                StringBuffer messageAvertissement = new StringBuffer("javascript:alert('");
                messageAvertissement.append(message.getMessage());
                messageAvertissement.append("');");

                if (menu.getAction() == null) {
                  menu.setAction(menu.getAdresseWeb());
                }

                menu.setAdresseWeb(messageAvertissement.toString());
                setAdresseWeb(menu);
              }

              menu.setSelectionDesative(true);
            }
          } catch (MalformedURLException m) {
            log.error("Action ou forward inconnu: " + m.getMessage());
            log.info("l'adresse Web sera configuré a #");
            menu.setAdresseWeb("#");
          }

          menu.setLibellePremiereLettreEnMajuscule(isTraitementLibellePremiereLettreEnMajuscule());

          if (isDesactiverServiceSelectionne()) {
            if ((GestionSecurite.getInstance().getPageAccueilDefaut().intValue() != menu.getObjetSecurisable()
                .getSeqObjetSecurisable().intValue())
                && (menu.getNbMenuEnfants().intValue() == 0)) {
              menu.setAdresseWeb(null);
              menu.setAction(null);
              menu.setUrl(null);
            }
          }

          getAfficheurMenuVelocity().affichage(menu);
        } catch (Exception e) {
          log.error(e);
        }
      }
    } catch (Exception e) {
      log.error(e);
    }

    return (SKIP_BODY);
  }

  @Override
  public int doEndTag() throws JspException {
    try {
      if (getAfficheurMenuVelocity().isGabaritFilNavigation()) {
        ComposantMenu menu = (ComposantMenu) pageContext.getAttribute("composantMenuTraitement");

        if (menu.getOrdreAffichage().intValue() == 1000) {
          menu.setOrdreAffichage(new Integer(-1000));
        }
      }
    } catch (Exception e) {
      log.error(e);
    }

    return (EVAL_PAGE);
  }

  /**
   * Permet de désactiver l'ajout des parametres de requete dans les onglets.
   * 
   * @return true on doit les ajouter false le les ajout plus
   */
  private boolean isInjectionParametreOnglet() {
    String valeur = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.INJECTION_PARAMETRE_ONGLET);

    return (valeur != null) && valeur.trim().toLowerCase().equals("true");
  }

  /**
   * Fixer la valeur pour l'adresse web du menu.
   * <p>
   * Si un forward existe, la valeur sera extraite du fichier struts-config.xml
   * 
   * @param menu
   *          la composante de menu pour générer l'adresse web.
   */
  protected void setAdresseWeb(ComposantMenu menu) throws MalformedURLException, JspException {
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // si l'adresse web est null, fixer un contexte relatif à la page,
    if (menu.getAdresseWeb() == null) {
      if (menu.getPage() != null) {
        menu.setAdresseWeb(request.getContextPath() + getPage(menu.getPage()));
      } else if (menu.getForward() != null) {
        String fwd = TagUtils.getInstance().computeURL(pageContext, menu.getForward(), null, null, null, null, null,
            null, false);
        menu.setAdresseWeb(fwd);
      } else if (menu.getAction() != null) {
        // générer un url avec une action Struts,
        // concaténer le path,
        // Servlet Mapping (path mapping or extension mapping)
        // module prefix (si existe & et le Session ID
        String action = TagUtils.getInstance().computeURL(pageContext, null, null, null, menu.getAction(), null, null,
            null, false);
        menu.setAdresseWeb(action);
      }
    }

    String url = menu.getAdresseWeb();

    String resultat = EvaluateurExpression.evaluerString("url", url, this, pageContext);

    menu.setUrl(resultat);

    // Répétez pour tous les menus
    ComposantMenu[] sousMenu = menu.getComposantMenus();

    if (sousMenu.length > 0) {
      for (int i = 0; i < sousMenu.length; i++) {
        setAdresseWeb(sousMenu[i]);
      }
    }
  }

  /**
   * Retourne la valeur dans la page avec un "/" si pas déjà là.
   * 
   * @param page
   *          La valeur pour cette page.
   */
  private String getPage(String page) {
    if (page.startsWith("/")) {
      return page;
    } else {
      page = "/" + page;
    }

    return page;
  }

  /**
   * Libérer les ressources
   */
  @Override
  public void release() {
    super.release();
    setIdentifiant(null);
    setCible(null);
  }

  /**
   * Permet de traiter les expressions JSTL.
   * 
   * @exception JspException
   *              si une exception JSP est traité
   */
  private void evaluerEL() throws JspException {
    String resultat = EvaluateurExpression.evaluerString("identifiant", getIdentifiant(), this, pageContext);
    setIdentifiant(resultat);

    String niveauOnglet = EvaluateurExpression.evaluerString("niveauOnglet", getNiveauOnglet(), this, pageContext);
    setNiveauOnglet(niveauOnglet);

    String noMenu = EvaluateurExpression.evaluerString("noMenu", getNoMenu(), this, pageContext);
    setNoMenu(noMenu);
  }

  /**
   * Modification dynamique d'un élément du fil de navigation.
   * 
   * @param contexte
   *          le contexte de la page.
   * @param menu
   *          le menu a traiter.
   */
  private void modificationFilNavigation(ComposantMenu menu, PageContext contexte) {
    // Modification dynamique du service sélectionné dans le fil de navigation.
    if (getAfficheurMenuVelocity().isGabaritFilNavigation()) {
      String urlAffiche = (String) contexte.getSession().getAttribute(Constantes.ADRESSE_ACTION_AFFICHE);

      ObjetSecurisable objetCourant = GestionSecurite.getInstance().getObjetSecurisableParAdresse(urlAffiche);

      ObjetSecurisable objetParent = null;

      if (objetCourant != null) {
        objetParent = objetCourant.getObjetSecurisableParent();
      }

      boolean libelleAModifier = contexte.getSession().getAttribute("LIBELLE_A_MODIFIER_" + menu.getLibelle()) != null;

      boolean enfantDifferentParent = (menu.getAdresseWeb() == null) || (objetParent == null)
          || (objetParent.getAdresseWeb() == null) || (menu.getAdresseWeb().indexOf(objetParent.getAdresseWeb()) == -1);

      if (libelleAModifier && (objetParent != null) && enfantDifferentParent) {
        HashMap libelleTmp = (HashMap) pageContext.getSession().getAttribute("LIBELLE_A_MODIFIER_" + menu.getLibelle());
        String libelleRemplacement = (String) libelleTmp.get("libelle");

        String urlRemplacement = (String) libelleTmp.get("url");

        if (urlRemplacement != null) {
          Href url = new Href(urlRemplacement);
          menu.setAdresseWeb(url.toString());
        } else {
          // Valider si le libelle de remplacement est une composant du référentiel
          // si oui, il faut prendre son adresse web associé.
          ObjetSecurisable objet = GestionSecurite.getInstance().getObjetSecurisable(libelleRemplacement);

          if (objet != null) {
            urlRemplacement = objet.getAdresseWeb();
          }
        }

        if (urlRemplacement != null) {
          Href url = new Href(urlRemplacement);
          menu.setAdresseWeb(url.toString());
          menu.setAction(null);
        }

        String[] parametresLibelle = (String[]) libelleTmp.get("parametresLibelle");

        Libelle libelle = UtilitaireLibelle.getLibelle(libelleRemplacement, pageContext, parametresLibelle);

        menu.setLibelle(libelle.getMessage());
      }
    }
  }

  public void setLibellePremiereLettreEnMajuscule(String libellePremiereLettreEnMajuscule) {
    this.libellePremiereLettreEnMajuscule = libellePremiereLettreEnMajuscule;
  }

  public String getLibellePremiereLettreEnMajuscule() {
    return libellePremiereLettreEnMajuscule;
  }

  public boolean isTraitementLibellePremiereLettreEnMajuscule() {
    if ((getLibellePremiereLettreEnMajuscule() != null)
        && getLibellePremiereLettreEnMajuscule().toString().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public AfficheurMenuVelocity getAfficheurMenuVelocity() {
    AfficheurMenuVelocity afficheur = (AfficheurMenuVelocity) pageContext.getAttribute(MenuTag.CLE_AFFICHEUR);
    return afficheur;
  }

  public void setNiveauOnglet(String niveauOnglet) {
    this.niveauOnglet = niveauOnglet;
  }

  public String getNiveauOnglet() {
    return niveauOnglet;
  }

  public void setNoMenu(String noMenu) {
    this.noMenu = noMenu;
  }

  public String getNoMenu() {
    return noMenu;
  }

  public void setDesactiverServiceSelectionne(boolean desactiverServiceSelectionne) {
    this.desactiverServiceSelectionne = desactiverServiceSelectionne;
  }

  public boolean isDesactiverServiceSelectionne() {
    return desactiverServiceSelectionne;
  }

  public boolean isExclureSelectionOngletEnfant() {
    return exclureSelectionOngletEnfant;
  }

  public void setExclureSelectionOngletEnfant(boolean exclureSelectionOngletEnfant) {
    this.exclureSelectionOngletEnfant = exclureSelectionOngletEnfant;
  }
}
