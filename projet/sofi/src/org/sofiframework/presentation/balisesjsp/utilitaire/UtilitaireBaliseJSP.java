/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.utilitaire;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;
import org.apache.struts.taglib.html.FormTag;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.base.BaliseValeurCache;
import org.sofiframework.presentation.balisesjsp.base.BlocSecuriteTag;
import org.sofiframework.presentation.balisesjsp.base.ContenuDynamiqueTag;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.balisesjsp.securite.SecuriteApplicative;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.struts.form.SommaireValidationForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe utilitaire qui offre divers fonctionnalités nécessaires pour la génération dans les balises JSP.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.2
 */
public class UtilitaireBaliseJSP {

  public static final String NOM_FONCTION_RETOUR_VALEUR = "retournerValeurs";
  public static final String NOM_FONCTION_RETOUR_VALEUR_AJAX = "retournerValeursAjax";
  public static final String HTML_CHAMP_OBLIGATOIRE_PAR_DEFAUT = "<span class=\"ind_obligatoire\">*</span>";

  /** L'objet de log pour cette classe */
  private static Log log = LogFactory.getLog(UtilitaireBaliseJSP.class);

  /**
   * Retourne le code de fermeture de balise correspondant au DOCTYPE de la page qui exécute le tag.
   * <p>
   * 
   * @param contexte
   *          le contexte de la page qui exécute le tag appelant
   * @return "/>" si la page est en XHTML sinon retourne ">"
   */
  public static String getElementClose(PageContext contexte) {
    return isXhtml(contexte) ? "/>" : ">";
  }

  /**
   * Permet de savoir si la page qui exécute le tag appelant s'exécute en XHTML
   * <p>
   * 
   * @param contexte
   *          le contexte de la page qui exécute le tag appelant
   * @return true si la page est en XHTML
   */
  public static boolean isXhtml(PageContext contexte) {
    return TagUtils.getInstance().isXhtml(contexte);
  }

  /**
   * Indique si le bloc d'affichage en cours est sécurisé en lecture seulement.
   * 
   * @return true si le bloc d'affichage en cours est sécurisé en lecture seulement.
   * @param pageContext
   *          le contexte de la page
   * @since SOFI 2.0.1
   */
  public static boolean isBlocSecuriseEnLectureSeulement(PageContext pageContext) {
    if (pageContext.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Déterminer si le formulaire courant est en affichage seulement. On doit vérifier si le formulaire parent
   * 
   * @returntrue, le formulaire doit etre affiché en affichage seulement. False, le formulaire sera éditable.
   */
  public static boolean isFormulaireCourantEnAffichageSeulement(PageContext pageContext, String nomPropriete) {
    BaseForm formulaire = UtilitaireBaliseNested.getFormulaireCourant(pageContext, nomPropriete);
    boolean affichageSeulement = false;

    if (formulaire != null) {
      affichageSeulement = formulaire.isModeAffichageSeulement();

      /**
       * Si le formulaire courant n'est pas en mode affichage seulement, on va voir le formulaire parent.
       */
      if (formulaire.getFormulaireParent() != null && !affichageSeulement) {
        affichageSeulement = formulaire.getFormulaireParent().isModeAffichageSeulement();
      }
    }

    return affichageSeulement;
  }

  /**
   * Retourne le nom du formulaire correspondant à un formulaire dans une page JSP.
   * <p>
   * 
   * @param pageContext
   *          le contexte de la page en cours
   * @return le nom du formulaire
   */
  public static String getNomFormulaire(PageContext pageContext) {
    FormTag formTag = (FormTag) pageContext.findAttribute(Constants.FORM_KEY);
    String nomFormulaire = null;

    if (formTag != null) {
      nomFormulaire = formTag.getBeanName();
    } else {
      nomFormulaire = (String) pageContext.getSession().getAttribute(Constantes.FORMULAIRE);
    }

    return nomFormulaire;
  }

  /**
   * Génère une fonction JavaScript qui permet de spécifier si le formulaire a été modifié.
   * <p>
   * 
   * @param pageContext
   *          le contexte de la page en cours
   * @param nomAttribut
   *          le nom de l'attribut nested s'il y en a un
   * @return le bout de code HTML représentant la fonction Javascript
   */
  public static String genererFonctionIndModificationFormulaire(PageContext pageContext, String nomAttribut) {
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(pageContext);

    if ((formulaire != null) && formulaire.isTransactionnel() && !formulaire.isModeLectureSeulement()
        && (pageContext.getRequest().getAttribute(Constantes.IND_MODIFICATION_FORMULAIRE) != null)) {
      // Préparation de la méthode javascript qui indique si le formulaire
      // a été modifié
      String nomFormulaire = getNomFormulaire(pageContext);
      StringBuffer nomFormulaireFonctionJS = new StringBuffer();
      nomFormulaireFonctionJS.append(nomFormulaire);

      StringBuffer paramScriptModif = new StringBuffer();
      String nomFormulaireImbrique = UtilitaireBaliseNested.getReferenceFormulaireImbrique(pageContext, null);

      if (nomFormulaireImbrique != null) {
        String nomFormulaireImbriquePourFonction = genererNomFonctionJSPourModificationFormulaire(nomFormulaireImbrique);
        paramScriptModif.append(nomFormulaire);
        paramScriptModif.append(nomFormulaireImbrique);
        paramScriptModif.append("scriptModif");
        nomFormulaireFonctionJS.append("_");
        nomFormulaireFonctionJS.append(nomFormulaireImbriquePourFonction);
      } else {
        paramScriptModif.append(nomFormulaire);
        paramScriptModif.append("scriptModif");
      }

      if (pageContext.getRequest().getAttribute(paramScriptModif.toString()) == null) {
        pageContext.getRequest().setAttribute(paramScriptModif.toString(), "existe");

        StringBuffer nomChamp = new StringBuffer();
        if (nomFormulaireImbrique != null) {
          nomChamp.append(nomFormulaireImbrique).append(".");
        }

        Object valeurModifie = UtilitaireObjet.getValeurAttribut(formulaire, nomChamp + "modifie");
        String prototypeHidden = "<input type=\"hidden\" name=\"{0}\" value=\"{1}\" />\n";
        StringBuffer hiddens = new StringBuffer();

        // Faire des champs hidden pour les indicateurs nouveau en erreur et
        // modification
        hiddens
        .append(
            MessageFormat.format(prototypeHidden,
                new Object[] { nomChamp + "indNouveau", formulaire.isNouveauFormulaire() ? "1" : "0" }))
                .append(
                    MessageFormat.format(prototypeHidden,
                        new Object[] { nomChamp + "indModification", Boolean.TRUE.equals(valeurModifie) ? "1" : "0" }))
                        .append(
                            MessageFormat.format(prototypeHidden,
                                new Object[] { nomChamp + "indErreur", formulaire.isFormulaireEnErreur() ? "1" : "0" }));

        try {
          // Ecrire la fonction javascript dans la sortie en écriture
          TagUtils.getInstance().write(pageContext, hiddens.toString());
        } catch (JspException e) {
          if (log.isErrorEnabled()) {
            log.error(e.getMessage(), e);
          }
        }
      }

      return nomFormulaireFonctionJS.toString();
    }

    return "";
  }

  /**
   * Retirer les caractères illégaux pour nommer la fonction JS, soit les "[ ] ."
   * 
   * @return Un nom de fonction JS valide.
   * @param nomFormulaire
   *          le nom de formulaire.
   */
  public static String genererNomFonctionJSPourModificationFormulaire(String nomFormulaire) {
    // Retirer les caractères illégaux pour nommer la fonction JS, soit les
    // "[ ] ."
    // nomFormulaire = UtilitaireString.remplacerTous(nomFormulaire, "[", "");
    // nomFormulaire = UtilitaireString.remplacerTous(nomFormulaire, "]", "");
    // nomFormulaire = UtilitaireString.remplacerTous(nomFormulaire, ".", "");

    return nomFormulaire;
  }

  /**
   * Permet de générer la méthode JavaScript qui permet de notifier que le formulaire a été modifié.
   * 
   * @return la méthode JavaScript qui permet de notifier que le formulaire a été modifié.
   * @param nomFormulaire
   *          le formulaire en traitement.
   */
  public static String genererNotifierFormulaire(String nomFormulaire) {
    StringBuffer resultat = new StringBuffer();
    resultat.append("notifierModificationFormulaire('");
    // resultat.append("notifier");
    resultat.append(nomFormulaire);
    // resultat.append("();");
    resultat.append("');");
    return resultat.toString();
  }

  public static String genererFonctionRetourValeursAjax(PageContext pageContext) {
    // L'index du div popup est générer par la liste de valeurs ajax.
    String indexDivAjax = pageContext.getRequest().getParameter("indexDivAJAX");
    if (indexDivAjax != null) {
      pageContext.getSession().setAttribute("indexDivAJAX", indexDivAjax);
    } else {
      indexDivAjax = (String) pageContext.getSession().getAttribute("indexDivAJAX");
    }
    String attributRetour = pageContext.getRequest().getParameter("attributRetour");
    String formulaireValeurRetour = pageContext.getRequest().getParameter("formulaireValeurRetour");
    String transactionnel = pageContext.getRequest().getParameter("transactionnel");
    if (attributRetour != null) {
      UtilitaireControleur.setAttributTemporairePourAction("attributRetour", attributRetour,
          (HttpServletRequest) pageContext.getRequest());
      UtilitaireControleur.setAttributTemporairePourAction("formulaireValeurRetour", formulaireValeurRetour,
          (HttpServletRequest) pageContext.getRequest());
      UtilitaireControleur.setAttributTemporairePourAction("listeValeur_transactionnel_" + indexDivAjax,
          transactionnel, (HttpServletRequest) pageContext.getRequest());
    } else {
      attributRetour = (String) pageContext.getSession().getAttribute("attributRetour");
      formulaireValeurRetour = (String) pageContext.getSession().getAttribute("formulaireValeurRetour");
      transactionnel = (String) pageContext.getSession().getAttribute("listeValeur_transactionnel_" + indexDivAjax);
    }

    String nomFormulaireCourant = null;

    if (formulaireValeurRetour != null) {
      nomFormulaireCourant = formulaireValeurRetour;
    } else {
      nomFormulaireCourant = (String) pageContext.getSession().getAttribute(Constantes.FORMULAIRE);
    }

    BaseForm formulaireContenantListeValeur = (BaseForm) pageContext.getSession().getAttribute(nomFormulaireCourant);

    String proprietes = UtilitaireString.decoderUrl(attributRetour);
    proprietes = UtilitaireString.enleveTousLesEspaces(proprietes);

    boolean formulaireImbrique = proprietes.indexOf(".") != -1;
    String nomFormulaireImbrique = null;
    if (formulaireImbrique) {
      nomFormulaireImbrique = proprietes.substring(0, proprietes.indexOf("."));
    }

    String templateFonction = "var modifier = setValeursLovAjax(''{0}'', new Array({1}), new Array({2}),''{3}''); if (modifier) '{'{4}'}' fermerFenetreAJAX(''closeImage{5}'');";

    String listeNomChamp = "'" + UtilitaireString.remplacerTous(proprietes, ",", "','") + "'";
    StringBuffer listeValeur = new StringBuffer();
    int nombreValeur = (new StringTokenizer(listeNomChamp, ",").countTokens());
    for (int i = 0; i < nombreValeur;) {
      listeValeur.append("{").append(i++).append("}");
      if (i < nombreValeur) {
        listeValeur.append(",");
      }
    }

    StringBuffer notifier = new StringBuffer();

    if (formulaireContenantListeValeur.isTransactionnel() && (transactionnel == null)) {
      notifier.append("window.notifierModificationFormulaire(\\'").append(nomFormulaireCourant).append("\\');");
      if (formulaireImbrique) {
        notifier.append(getNomFonctionModificationFormulaire(nomFormulaireCourant, nomFormulaireImbrique));
        notifier.append("();");
      }
    }

    String idAjaxJSModificationFormulaire = pageContext.getRequest().getParameter("idAjaxJSModificationFormulaire");

    if (pageContext.getRequest().getParameter("sofi_initialiser_liste") != null) {
      pageContext.getSession().removeAttribute("idAjaxJSModificationFormulaire_" + indexDivAjax);
    }

    if (idAjaxJSModificationFormulaire != null) {
      UtilitaireControleur.setAttributTemporairePourAction("idAjaxJSModificationFormulaire_" + indexDivAjax,
          idAjaxJSModificationFormulaire, (HttpServletRequest) pageContext.getRequest());
    } else {
      idAjaxJSModificationFormulaire = (String) pageContext.getSession().getAttribute(
          "idAjaxJSModificationFormulaire_" + indexDivAjax);
    }

    String fonctionJSApresModification = "";
    // Ajouter la fonction personnalisé si exitante après la notification de
    // modification du formulaire.
    if (idAjaxJSModificationFormulaire != null) {
      fonctionJSApresModification = (String) pageContext.getSession().getAttribute(idAjaxJSModificationFormulaire);
    }

    String fonction = MessageFormat.format(templateFonction, new Object[] { nomFormulaireCourant, listeNomChamp,
        listeValeur, notifier.toString(), fonctionJSApresModification, indexDivAjax });
    return fonction;
  }

  /**
   * Génère une fonction javascript utilisée pour reporter les valeurs d'une liste de navigation vers le formulaire
   * <p>
   * 
   * @param pageContext
   *          Contexte de la page JSP
   * @param ajax
   *          true si le retour est d'un div ajax, false si il s'agit d'une nouvelle fenêtre.
   * @return Texte de la fonction générée
   */
  public static StringBuffer genererFonctionRetourValeurs(PageContext pageContext, boolean ajax) {
    /*
     * Les noms de fonction sont différents en ajax et non-ajax En ajax on demeure dans la même fenêtre.
     */
    String nomFonction = ajax ? NOM_FONCTION_RETOUR_VALEUR_AJAX : NOM_FONCTION_RETOUR_VALEUR;
    String fenetre = ajax ? "window" : "opener";

    // L'index du div popup est générer par la liste de valeurs ajax.
    String indexDivAjax = null;

    if (ajax) {
      indexDivAjax = pageContext.getRequest().getParameter("indexDivAJAX");

      if (indexDivAjax != null) {
        pageContext.getSession().setAttribute("indexDivAJAX", indexDivAjax);
      } else {
        indexDivAjax = (String) pageContext.getSession().getAttribute("indexDivAJAX");
      }
    }

    String jsFermerFenetre = ajax ? ("\n  fermerFenetreAJAX('closeImage" + indexDivAjax + "');")
        : "\n  window.close();";
    String suffix = ajax ? "AJAXIn" : "In";

    StringBuffer fonction = new StringBuffer();

    String attributRetour = pageContext.getRequest().getParameter("attributRetour");
    String formulaireValeurRetour = pageContext.getRequest().getParameter("formulaireValeurRetour");
    String transactionnel = pageContext.getRequest().getParameter("transactionnel");

    if (attributRetour != null) {
      UtilitaireControleur.setAttributTemporairePourAction("attributRetour", attributRetour,
          (HttpServletRequest) pageContext.getRequest());
      UtilitaireControleur.setAttributTemporairePourAction("formulaireValeurRetour", formulaireValeurRetour,
          (HttpServletRequest) pageContext.getRequest());
      UtilitaireControleur.setAttributTemporairePourAction("listeValeur_transactionnel_" + indexDivAjax,
          transactionnel, (HttpServletRequest) pageContext.getRequest());
    } else {
      attributRetour = (String) pageContext.getSession().getAttribute("attributRetour");
      formulaireValeurRetour = (String) pageContext.getSession().getAttribute("formulaireValeurRetour");
      transactionnel = (String) pageContext.getSession().getAttribute("listeValeur_transactionnel_" + indexDivAjax);
    }

    if (attributRetour != null) {
      String proprietes = UtilitaireString.decoderUrl(attributRetour);

      String nomFormulaireImbrique = null;

      boolean formulaireImbrique = false;

      if (proprietes != null) {
        if (!ajax) {
          // L'attribut LANGUAGE de la balise SCRIPT est DEPRECATED.
          // REF: http://www.w3.org/TR/REC-html40/interact/scripts.html#h-18.2.1
          // fonction.append("<script type=\"text/javascript\" language=\"JavaScript\">");
          fonction.append("<script type=\"text/javascript\">");
          fonction.append("\nfunction ").append(nomFonction).append("(");
        }

        // Traiter les formulaires imbriqués
        if (proprietes.indexOf(".") != -1) {
          formulaireImbrique = true;
        }

        // Parcourir les propriétés à populer
        int index = 0;

        for (StringTokenizer tk = new StringTokenizer(proprietes, ","); tk.hasMoreTokens();) {
          String propriete = tk.nextToken();

          if (formulaireImbrique) {
            nomFormulaireImbrique = propriete.substring(0, propriete.lastIndexOf("."));
            propriete = propriete.substring(propriete.lastIndexOf(".") + 1);
          }

          if (ajax) {
            fonction.append("var ");
            fonction.append(propriete).append(suffix).append(" = ");
            fonction.append("{").append(index++).append("}");
          } else {
            fonction.append(propriete).append(suffix);
          }

          if (tk.hasMoreTokens()) {
            if (!ajax) {
              fonction.append(", ");
            }
          }

          if (ajax) {
            fonction.append("; ");
          }
        }

        if (!ajax) {
          fonction.append(") {");
        }

        String premierChampFormulaire = null;
        String premierParametre = null;

        String nomFormulaireCourant = null;

        if (formulaireValeurRetour != null) {
          nomFormulaireCourant = formulaireValeurRetour;
        } else {
          nomFormulaireCourant = (String) pageContext.getSession().getAttribute(Constantes.FORMULAIRE);
        }

        BaseForm formulaireContenantListeValeur = (BaseForm) pageContext.getSession()
            .getAttribute(nomFormulaireCourant);

        // Affecter les valeurs
        for (StringTokenizer tk = new StringTokenizer(proprietes, ","); tk.hasMoreTokens();) {
          String propriete = tk.nextToken().trim();
          String proprieteFormulaire = propriete;

          if (formulaireImbrique) {
            propriete = propriete.substring(propriete.lastIndexOf(".") + 1);
          }

          fonction.append("\n");
          fonction.append("if (");
          fonction.append(propriete);
          fonction.append(suffix).append(" && ");
          fonction.append(propriete);
          fonction.append(suffix).append(" != 'null'){");

          StringBuffer champFormulaire = new StringBuffer();
          StringBuffer refChampFormulaire = new StringBuffer(fenetre);

          if (!UtilitaireString.isVide(nomFormulaireCourant)) {
            refChampFormulaire.append(".document.forms['" + nomFormulaireCourant + "'].elements['");
          } else {
            refChampFormulaire.append(".document.forms[0].elements['");
          }

          refChampFormulaire.append(proprieteFormulaire);
          refChampFormulaire.append("']");
          champFormulaire.append(refChampFormulaire);
          champFormulaire.append(".value");

          StringBuffer parametre = new StringBuffer();
          parametre.append(propriete);
          parametre.append(suffix);

          if ((premierChampFormulaire == null) && (nomFormulaireCourant != null)
              && (formulaireContenantListeValeur != null)) {
            premierChampFormulaire = champFormulaire.toString();
            premierParametre = parametre.toString();
            fonction.append("\n");
            fonction.append("var champFormulaire =");
            fonction.append(premierChampFormulaire);
            fonction.append(";");
            fonction.append("\n");
          }

          fonction.append("\n");
          fonction.append(champFormulaire);
          fonction.append(" = ");

          fonction.append(parametre);
          fonction.append(";");

          fonction.append("\n");
          fonction.append("} else {");

          fonction.append("\n");
          fonction.append(champFormulaire);
          fonction.append(" = '';");

          fonction.append("\n");
          fonction.append("}");

          /*
           * On doit réajuster les styles des champs lors de la sélection. Pour enlever les style erreur
           */
          if (ajax) {
            fonction.append("appliquerStyleChampSaisie(").append(refChampFormulaire).append(");");
          }
        }

        if ((nomFormulaireCourant != null) && (formulaireContenantListeValeur != null)) {
          fonction.append("\n");
          fonction.append("if (champFormulaire != ");
          fonction.append(premierParametre);
          fonction.append("){");
          fonction.append("\n");
          fonction.append("try {");

          if (formulaireContenantListeValeur.isTransactionnel() && (transactionnel == null)) {
            fonction.append("try {");
            fonction.append(fenetre);
            fonction.append(".notifier");
            fonction.append(nomFormulaireCourant);
            fonction.append("();");

            if (formulaireImbrique) {
              fonction.append(getNomFonctionModificationFormulaire(nomFormulaireCourant, nomFormulaireImbrique));
              fonction.append("();");
            }

            fonction.append("}catch(e){}\n");
          }

          String idAjaxJSModificationFormulaire = pageContext.getRequest().getParameter(
              "idAjaxJSModificationFormulaire");

          if (pageContext.getRequest().getParameter("sofi_initialiser_liste") != null) {
            pageContext.getSession().removeAttribute("idAjaxJSModificationFormulaire_" + indexDivAjax);
          }

          if (idAjaxJSModificationFormulaire != null) {
            UtilitaireControleur.setAttributTemporairePourAction("idAjaxJSModificationFormulaire_" + indexDivAjax,
                idAjaxJSModificationFormulaire, (HttpServletRequest) pageContext.getRequest());
          } else {
            idAjaxJSModificationFormulaire = (String) pageContext.getSession().getAttribute(
                "idAjaxJSModificationFormulaire_" + indexDivAjax);
          }

          // Ajouter la fonction personnalisé si exitante après la notification
          // de modification du formulaire.
          if (idAjaxJSModificationFormulaire != null) {
            String fonctionJSApresModification = (String) pageContext.getSession().getAttribute(
                idAjaxJSModificationFormulaire);
            fonction.append(fonctionJSApresModification);
          }

          fonction.append("focusPremierChampListeValeur('").append(proprietes).append("');");
          fonction.append("}catch(e){}\n");
          fonction.append("}");
        }

        fonction.append(jsFermerFenetre);

        if (!ajax) {
          fonction.append("\n}");
          fonction.append("\n</script>");
        }
      }
    }

    return fonction;
  }

  /**
   * Retourne le formulaire de type BaseForm qui est dans le request ou la session
   * <p>
   * 
   * @param pageContext
   *          le contexte de la page HTML en cours
   * @return BaseForm le formulaire en traitement
   */
  public static BaseForm getBaseForm(PageContext pageContext) {
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    BaseForm baseForm = null;

    if (nomFormulaire != null) {
      baseForm = (BaseForm) pageContext.getRequest().getAttribute(nomFormulaire);

      if (baseForm == null) {
        baseForm = (BaseForm) pageContext.getSession().getAttribute(nomFormulaire);
      }
    }

    return baseForm;
  }

  /**
   * Permet de modifier l'attribut onchange de la balise JSP de saisie afin de pouvoir spécifier si le formulaire a été
   * modifié.
   * <p>
   * 
   * @param pageContext
   *          le contexte de la page HTML en cours
   * @param nomFormulaire
   *          le nom du formulaire dans le request ou la session
   * @param nomFormulaireJS
   *          le nom du formulaire à l'intérieur du formulaire dans le cas où on utilise un nested
   * @param onChange
   *          chose à exécuter sur le onChange de la méthode Javascript
   * @param resultat
   *          le bout de code à laquel il faut ajouter du HTML
   * @return javascript permettant la gestion de modifification d'un formulaire
   */
  public static StringBuffer specifierGestionModification(PageContext pageContext, String nomFormulaire,
      String nomFormulaireJS, String onChange, StringBuffer resultat) {

    resultat.append(" onchange=\"");

    resultat.append(genererJSFonctionModification(pageContext, nomFormulaire, nomFormulaireJS, onChange));

    resultat.append("\"");

    return resultat;
  }

  /**
   * Permet de generer les traitement JS lors d'une modification dans un formulaire transactionnel.
   * <p>
   * 
   * @param pageContext
   *          le contexte de la page HTML en cours
   * @param nomFormulaire
   *          le nom du formulaire dans le request ou la session
   * @param nomFormulaireJS
   *          le nom du formulaire à l'intérieur du formulaire dans le cas où on utilise un nested
   * @param onChange
   *          chose à exécuter sur le onChange de la méthode Javascript
   * @return javascript permettant la gestion de modifification d'un formulaire
   * @since SOFI 2.1
   */
  public static StringBuffer genererJSFonctionModification(PageContext pageContext, String nomFormulaire,
      String nomFormulaireJS, String onChange) {

    StringBuffer resultat = new StringBuffer();

    boolean isNested = false;

    if (nomFormulaireJS.indexOf("_") != -1) {
      isNested = true;
    }

    BaseForm formulaire = getBaseForm(pageContext);

    if (formulaire != null && formulaire.isTransactionnel()) {
      resultat.append(genererNotifierFormulaire(nomFormulaire));

      if (isNested) {
        resultat.append(genererNotifierFormulaire(nomFormulaireJS));
      }
    }

    if (onChange != null) {
      resultat.append(onChange);

      if ((onChange.lastIndexOf(";") + 1) != onChange.length()) {
        resultat.append(";");
      }
    }

    return resultat;
  }

  /**
   * Permet d'ajouter un style lorsque qu'un champ de saisie est sélectionné.
   * <p>
   * 
   * @param styleClass
   *          le style à donner au champ de saisie à l'état normal
   * @param styleClassFocus
   *          le style à donner au champ de saisie lors qu'il possède le focus
   * @param resultat
   *          le code HTML à laquelle il faut ajouter du code
   * @return javascript permettant de spécifiant un style de focus des champs de saisie
   */
  public static StringBuffer specifierStyleFocus(String styleClass, String styleClassFocus, String fonctionOnFocus,
      String fonctionOnBlur, StringBuffer resultat) {

    Boolean activerStyleFocusChampSaisie = GestionParametreSysteme.getInstance().getBoolean(
        "activerStyleFocusChampSaisie");

    if (activerStyleFocusChampSaisie.booleanValue()) {
      // Ajouter la notion de focus sur un champ de saisie;
      resultat.append(" onfocus=\"this.className='");
      if (styleClass != null) {
        resultat.append(styleClass);
        resultat.append(" ");
      }
      if (styleClassFocus != null) {
        resultat.append(styleClassFocus);
      } else {
        resultat.append("saisieFocus");
      }
      resultat.append("';");
      if (!UtilitaireString.isVide(fonctionOnFocus)) {
        resultat.append(fonctionOnFocus);
      }
      resultat.append("\"");
      resultat.append(" onblur=\"this.className='");
      if (styleClass != null) {
        resultat.append(styleClass);
        resultat.append("';");
      } else {
        resultat.append("saisieNormal';");
      }
      if (!UtilitaireString.isVide(fonctionOnBlur)) {
        resultat.append(fonctionOnBlur);
      }
      resultat.append("\"");
    }
    return resultat;
  }

  /**
   * Permet de faire la gestion des styles et de l'affichage d'un composant qui est en lecture seulement.
   * <p>
   * Cette méthode génère un bout de code pour mettre un attribut en mode "readOnly". Ce mode n'est disponible que pour
   * les champs des balises : <br>
   * <li>DateTag
   * <li>ListeValeurTag
   * <li>PasswordTag
   * <li>TextAreaTag
   * <li>TextTag Pour les autres types de balises, il faut absolument utiliser la méthode
   * <code>specifierStyleInactif(String, StringBuffer)</code>.
   * <p>
   * Le style par défaut à utiliser pour les éléments en lecture seulement peut être défini dans un paramètre système de
   * l'application sous le nom de <code>
   * styleLectureSeulementParDefaut</code>. Dans le cas ou l'application ne se sert pas du plug-in de paramètres système
   * ou que le paramètre n'est pas trouvé, le style par défaut utilisé est <code>lectureSeulement</code>.
   * <p>
   * 
   * @param styleClassLectureSeulement
   *          le style à imposer advenant le cas où on ne désire pas utiliser le style par défaut.
   * @param resultat
   *          la chaine de caractères représentant le code HTML à afficher dans la page.
   */
  public static void specifierLectureSeulement(String styleLectureSeulement, String style, StringBuffer resultat) {
    // Vérifier si le paramètre système existe et le prendre au besoin
    String styleAUtiliser = "lectureSeulement";
    GestionParametreSysteme gestionParametre = GestionParametreSysteme.getInstance();

    if (gestionParametre != null) {
      String parametre = gestionParametre.getString("styleLectureSeulementParDefaut");

      if ((parametre != null) && (parametre.trim().length() > 0)) {
        styleAUtiliser = parametre;
      }
    }

    if ((styleLectureSeulement == null) && (style != null)) {
      StringBuffer styleLecture = new StringBuffer(style);
      styleLecture.append("_");
      styleLecture.append(styleAUtiliser);
      styleAUtiliser = styleLecture.toString();
    }

    // Appliquer le style
    resultat.append(" class=\"");

    if ((styleLectureSeulement != null) && (styleLectureSeulement.trim().length() > 0)) {
      resultat.append(styleLectureSeulement);
    } else {
      resultat.append(styleAUtiliser);
    }

    resultat.append("\"");
  }

  /**
   * Permet de faire la gestion des styles et de l'affichage d'un composant qui est en lecture seulement, mais en mode
   * disabled.
   * <p>
   * Cette méthode génère un bout de code pour mettre un attribut en mode "disabled". Ce mode est à utiliser pour les
   * champs des balises : <br>
   * <li>CheckBoxTag
   * <li>MultiBoxTag
   * <li>RadioTag
   * <li>SelectTag Pour les autres types de balises, il est mieux d'utiliser la méthode
   * <code>specifierStyleLectureSeulement(String, StringBuffer)</code>.
   * <p>
   * Le style par défaut à utiliser pour les éléments en lecture seulement (de mode inactif) peut être défini dans un
   * paramètre système de l'application sous le nom de <code>styleInactifParDefaut</code>. Dans le cas ou l'application
   * ne se sert pas du plug-in de paramètres système ou que le paramètre n'est pas trouvé, le style par défaut utilisé
   * est <code>composantInactif</code>.
   * <p>
   * 
   * @param styleIncatif
   *          le style à imposer advenant le cas où on ne désire pas utiliser le style par défaut.
   * @param resultat
   *          la chaine de caractères représentant le code HTML à afficher dans la page.
   */

  // public static void specifierComposantInactif(String styleComposantIncatif,
  // String styleComposant, StringBuffer resultat) {
  // // Vérifier si le paramètre système existe et le prendre au besoin
  // String styleAUtiliser = "lectureSeulement";
  // GestionParametreSysteme gestionParametre =
  // GestionParametreSysteme.getInstance();
  //
  // if (gestionParametre != null) {
  // String parametre = gestionParametre.getString("styleInactifParDefaut");
  //
  // if ((parametre != null) && (parametre.trim().length() > 0)) {
  // styleAUtiliser = parametre;
  // }
  // }
  //
  // // Appliquer le style
  // resultat.append(" class=\"");
  //
  // if ((styleComposantIncatif != null) &&
  // (styleComposantIncatif.trim().length() > 0)) {
  // resultat.append(styleComposantIncatif);
  // } else {
  // resultat.append(styleAUtiliser);
  // }
  //
  // resultat.append("\"");
  // }

  /**
   * Permet de générer un libellé pour des boites à coché multiple ou des boite radio.
   * <p>
   * Cette méthode fait la gestion des paramètres nulls.
   * <p>
   * 
   * @param nomAttribut
   *          le nom de l'attribut du formulaire sur lequel faire le focus lors d'un click sur le libellé
   * @param no
   *          le numéro de du champ pour lequel on ajoute un libellé
   * @param aideContextuelle
   *          le message d'aide contextuel (peut contenir un code de libellé)
   * @param cleLibelle
   *          le libellé a afficher (peut contenir un code de libellé)
   * @param contexte
   *          le contexte de la page JSP qui utilise la balise
   * @param resultat
   *          le bout de string représentant le libellé
   */
  public static void genererLibelle(String nomAttribut, Integer no, String aideContextuelle, String cleLibelle,
      PageContext contexte, StringBuffer resultat) {
    Libelle libelle = null;

    // Si libellé existe, on l'ajoute
    if (cleLibelle != null) {
      resultat.append("<label for=\"");
      resultat.append(nomAttribut);
      resultat.append(no.toString());
      resultat.append("\"");

      libelle = UtilitaireLibelle.getLibelle(cleLibelle, contexte, null);

      genererAideContextuelle(aideContextuelle, libelle, contexte, resultat);

      resultat.append(">");

      resultat.append(libelle.getMessage());
      resultat.append("</label>");
    }
  }

  public static void genererLibelle(String nomAttribut, String aideContextuelle, String cleLibelle, String styleClass,
      boolean obligatoire, boolean affichageIndObligatoire, PageContext contexte, StringBuffer resultat) {
    genererLibelle(nomAttribut, aideContextuelle, cleLibelle, styleClass, obligatoire, affichageIndObligatoire, null,
        contexte, resultat);
  }

  /**
   * Permet de générer un libellé avec de l'aide contextuelle.
   * <p>
   * <p>
   * 
   * @param nomAttribut
   *          le nom de l'attribut du formulaire sur lequel faire le focus lors d'un click sur le libellé
   * @param aideContextuelle
   *          le message d'aide contextuel (peut contenir un code de libellé)
   * @param cleLibelle
   *          le libellé a afficher (peut contenir un code de libellé)
   * @param styleClass
   *          classe de la feuille de style à utiliser.
   * @param obligatoire
   *          valeur indiquand si le champ est obligatoire
   * @param contexte
   *          le contexte de la page JSP qui utilise la balise
   * @param resultat
   *          le bout de string représentant le libellé
   */
  public static void genererLibelle(String nomAttribut, String aideContextuelle, String cleLibelle, String styleClass,
      boolean obligatoire, boolean affichageIndObligatoire, Object[] parametres, String codeClient,
      PageContext contexte, StringBuffer resultat) {
    // Permet d'aller rechercher si l'indicateur doit être à gauche ou
    // à droite du libellé. False est la valeur par défaut et signifie à
    // gauche du libellé.
    Boolean estPositioneeDroite = GestionParametreSysteme.getInstance().getBoolean(
        Constantes.POSITION_INDICATEUR_OBLIGATOIRE_DROITE);

    // Si libellé existe, on l'ajoute
    if (cleLibelle != null) {
      Libelle libelle = UtilitaireLibelle.getLibelle(cleLibelle, contexte, parametres, codeClient);

      if (!UtilitaireString.isVide(nomAttribut) && (nomAttribut.indexOf("null") == -1)) {
        resultat.append("<label for=\"");
        resultat.append(nomAttribut);
        resultat.append("\"");

        if (!UtilitaireString.isVide(styleClass)) {
          resultat.append(" class=\"");
          resultat.append(styleClass);
          resultat.append("\"");
        }

        genererAideContextuelle(aideContextuelle, libelle, contexte, resultat);

        resultat.append(">");

        // pour générer l'indicateur d'attribut obligatoire à gauche du champ
        boolean isGenererObligatoire = ((obligatoire == true) || affichageIndObligatoire)
            && !estPositioneeDroite.booleanValue();
        if (isGenererObligatoire) {
          genererObligatoire(obligatoire, affichageIndObligatoire, null, nomAttribut, resultat, contexte);
        }

        resultat.append(libelle.getMessage());

        // pour générer l'indicateur d'attribut obligatoire à droite du champ
        if (((obligatoire == true) || affichageIndObligatoire) && estPositioneeDroite.booleanValue()) {
          genererObligatoire(obligatoire, affichageIndObligatoire, null, nomAttribut, resultat, contexte);

          // pour générer la fermeture de l'indicateur d'attribut obligatoire à
          // gauche du champ
        } else if (isGenererObligatoire) {
          genererFermetureObligatoire(obligatoire, affichageIndObligatoire, null, nomAttribut, resultat, contexte);
        }

        resultat.append("</label>");
      } else {
        genererLibelleEtAideContextuelle(aideContextuelle, libelle, styleClass, contexte, resultat);
      }
    }
  }

  public static void genererLibelle(String nomAttribut, String aideContextuelle, String cleLibelle, String styleClass,
      boolean obligatoire, boolean affichageIndObligatoire, Object[] parametres, PageContext contexte,
      StringBuffer resultat) {
    genererLibelle(nomAttribut, aideContextuelle, cleLibelle, styleClass, obligatoire, affichageIndObligatoire,
        parametres, null, contexte, resultat);
  }

  /**
   * Générer l'aide contextuelle.
   * 
   * @param aideContextuelle
   *          l'aide contextuelle
   * @param libelle
   *          le libellé pouvant contenir de l'aide contextuelle
   * @param contexte
   *          le contexte de la page
   * @param resultat
   *          le resultat de la balise à afficher
   */
  private static void genererAideContextuelle(String aideContextuelle, Libelle libelle, PageContext contexte,
      StringBuffer resultat) {
    if (aideContextuelle != null) {
      aideContextuelle = UtilitaireLibelle.getLibelle(aideContextuelle, contexte, null).getMessage();
    } else {
      aideContextuelle = libelle.getAideContextuelle();
    }

    if (!UtilitaireString.isVide(aideContextuelle) && !aideContextuelle.equals("&nbsp;")) {
      resultat.append(" title = \"");
      resultat.append(UtilitaireString.convertirEnHtml(aideContextuelle));
      resultat.append("\"");
    }
  }

  /**
   * Générer l'aide contextuelle.
   * 
   * @param aideContextuelle
   *          l'aide contextuelle
   * @param libelle
   *          le libellé pouvant contenir de l'aide contextuelle
   * @param styleClass
   *          le style CSS
   * @param contexte
   *          le contexte de la page
   * @param resultat
   *          le resultat de la balise à afficher
   */
  private static void genererLibelleEtAideContextuelle(String aideContextuelle, Libelle libelle, String styleClass,
      PageContext contexte, StringBuffer resultat) {
    // Extraire l'aide contextuelle.
    if (!UtilitaireString.isVide(aideContextuelle)) {
      aideContextuelle = UtilitaireLibelle.getLibelle(aideContextuelle, contexte, null).getMessage();
    } else {
      aideContextuelle = libelle.getAideContextuelle();
    }

    if (!UtilitaireString.isVide(aideContextuelle) || !UtilitaireString.isVide(styleClass)) {
      resultat.append("<span");
    }

    if (!UtilitaireString.isVide(styleClass)) {
      resultat.append(" class=\"");
      resultat.append(styleClass);
    }

    if (!UtilitaireString.isVide(aideContextuelle)) {
      resultat.append(" title = \"");
      resultat.append(UtilitaireString.convertirEnHtml(aideContextuelle));
      resultat.append("\"");
    }

    if (!UtilitaireString.isVide(aideContextuelle) || !UtilitaireString.isVide(styleClass)) {
      resultat.append("\">");
    }

    resultat.append(libelle.getMessage());

    if (!UtilitaireString.isVide(aideContextuelle) || !UtilitaireString.isVide(styleClass)) {
      resultat.append("</span>");
    }
  }

  /**
   * Permet de générer un libellé avec de l'aide contextuelle pour un liste d'objets(formulaire) imbriqués(nested).
   * <p>
   * Cette méthode fait la gestion des paramètres null.
   * <p>
   * 
   * @param nomListe
   *          le nom de la liste qui contient l'attribut pour lequel on ajoute un libellé
   * @param nomAttribut
   *          le nom de l'attribut du formulaire sur lequel faire le focus lors d'un click sur le libellé
   * @param aideContextuelle
   *          le message d'aide contextuel (peut contenir un code de libellé)
   * @param cleLibelle
   *          le libellé a afficher (peut contenir un code de libellé)
   * @param styleClass
   *          le style du libellé (feuille de style)
   * @param contexte
   *          le contexte de la page JSP qui utilise la balise
   * @param resultat
   *          le bout de string représentant le libellé
   */
  public static void genererLibelle(String nomListe, String nomAttribut, int indice, String aideContextuelle,
      String cleLibelle, String styleClass, boolean obligatoire, boolean affichageIndObligatoire, Object[] parametres,
      String codeClient, PageContext contexte, StringBuffer resultat) {
    // Permet d'aller rechercher si l'indicateur doit être à gauche ou
    // à droite du libellé. False est la valeur par défaut et signifie à
    // gauche du libellé.
    Boolean estPositioneeDroite = GestionParametreSysteme.getInstance().getBoolean(
        Constantes.POSITION_INDICATEUR_OBLIGATOIRE_DROITE);

    Libelle libelle = null;

    // Si libellé existe, on l'ajoute, toujours valider isVide au lieu de !=
    // null.
    if (!UtilitaireString.isVide(cleLibelle) && !UtilitaireString.isVide(nomListe)
        && !UtilitaireString.isVide(nomAttribut)) {
      StringBuffer nomListeAttribut = new StringBuffer();

      if (indice != -1) {
        nomListeAttribut.append(nomListe);
        nomListeAttribut.append("[");
        nomListeAttribut.append(indice);
        nomListeAttribut.append("].");
        nomListeAttribut.append(nomAttribut);
      } else {
        nomListeAttribut.append(nomAttribut);
      }

      if (!UtilitaireString.isVide(nomAttribut) && (nomAttribut.indexOf("null") == -1)) {
        resultat.append("<label for=\"");

        if (!UtilitaireString.isVide(nomListe)) {
          resultat.append(nomListeAttribut);
        } else {
          resultat.append(nomAttribut);
        }

        resultat.append("\"");
      } else {
        resultat.append("<span ");
      }

      if (!UtilitaireString.isVide(styleClass)) {
        resultat.append(" class=\"");
        resultat.append(styleClass);
        resultat.append("\"");
      }

      libelle = UtilitaireLibelle.getLibelle(cleLibelle, contexte, parametres, codeClient);

      genererAideContextuelle(aideContextuelle, libelle, contexte, resultat);
      resultat.append(">");

      // pour générer l'indicateur d'attribut obligatoire à gauche du champ
      boolean isGenererObligatoire = (obligatoire || affichageIndObligatoire) && !estPositioneeDroite.booleanValue();
      if (isGenererObligatoire) {
        try {
          genererObligatoire(obligatoire, affichageIndObligatoire, nomListe, nomAttribut, resultat, contexte);
        } catch (Exception e) {
        }
      }

      resultat.append(libelle.getMessage());

      // pour générer l'indicateur d'attribut obligatoire à droite du champ
      if (((obligatoire == true) || affichageIndObligatoire) && estPositioneeDroite.booleanValue()) {
        genererObligatoire(obligatoire, affichageIndObligatoire, nomListe, nomAttribut, resultat, contexte);
      } else if (isGenererObligatoire) {
        try {
          genererFermetureObligatoire(obligatoire, affichageIndObligatoire, nomListe, nomAttribut, resultat, contexte);
        } catch (Exception e) {
        }
      }

      if (!UtilitaireString.isVide(nomAttribut) && (nomAttribut.indexOf("null") == -1)) {
        resultat.append("</label>");
      } else {
        resultat.append("</span>");
      }
    }
  }

  public static void genererLibelle(String nomListe, String nomAttribut, int indice, String aideContextuelle,
      String cleLibelle, String styleClass, boolean obligatoire, boolean affichageIndObligatoire, Object[] parametres,
      PageContext contexte, StringBuffer resultat) {
    genererLibelle(nomListe, nomAttribut, indice, aideContextuelle, cleLibelle, styleClass, obligatoire,
        affichageIndObligatoire, parametres, null, contexte, resultat);
  }

  public static void genererLibelle(String nomListe, String nomAttribut, int indice, String aideContextuelle,
      String cleLibelle, String styleClass, boolean obligatoire, boolean affichageIndObligatoire, PageContext contexte,
      StringBuffer resultat) {
    genererLibelle(nomListe, nomAttribut, indice, aideContextuelle, cleLibelle, styleClass, obligatoire,
        affichageIndObligatoire, null, null, contexte, resultat);
  }

  /**
   * Spécifier qu'un attribut est obligatoire
   * <p>
   * 
   * @param obligatoire
   *          true si le champ de saisie est obligatoire.
   * @param affichageIndObligatoire
   *          true si on désire seulement l'affichage de l'indicateur.
   * @param nomFormulaireImbrique
   *          le nom du formulaire imbrique si nested
   * @param nomAttribut
   *          le nom de l'attribut a traiter
   * @param resultat
   *          le bout de code HTML à laquelle on ajoute l'attribut obligatoire
   * @param contexte
   *          le contexte de la page
   */
  public static void genererObligatoire(boolean obligatoire, boolean affichageIndObligatoire,
      String nomFormulaireImbrique, String nomAttribut, StringBuffer resultat, PageContext contexte) {

    // Ajouter indicateur obligatoire.
    if (obligatoire || affichageIndObligatoire) {

      // code html par défaut
      String htmlAUtiliser = HTML_CHAMP_OBLIGATOIRE_PAR_DEFAUT;

      // On récupère le code html redéfini par l'application
      String htmlRedefini = GestionParametreSysteme.getInstance().getString(
          ConstantesParametreSysteme.HTML_CHAMP_OBLIGATOIRE);

      // Si on a redéfini le style du champ obligatoire
      if (!UtilitaireString.isVide(htmlRedefini)) {
        String[] st = htmlRedefini.split("libelle");

        // On vérifie que le code html est valide :
        // PremiereParte{libelle}DeuxiemePartie
        if (isHtmlChampObligtoireValide(st)) {

          // Utiliser la première partie du code html
          htmlAUtiliser = st[0];
        }
      }
      resultat.append(htmlAUtiliser);

      if (obligatoire) {
        ajouterAttributObligatoire(nomFormulaireImbrique, nomAttribut, contexte);
      }
    }
  }

  /**
   * Valide si le code Html pour les champs obligatoires
   * 
   * @param html
   * @return
   */
  private static boolean isHtmlChampObligtoireValide(String[] html) {

    return (html.length > 1 && !UtilitaireString.isVide(html[0]) && !UtilitaireString.isVide(html[1]));
  }

  /**
   * Génère la fermeture du tag obligatoire pour un champ. N'est utile que si un style d'affichage obligatoire a été
   * redéfini par un paramètre système.
   * <p>
   * 
   * @param obligatoire
   *          true si le champ de saisie est obligatoire.
   * @param affichageIndObligatoire
   *          true si on désire seulement l'affichage de l'indicateur.
   * @param nomFormulaireImbrique
   *          le nom du formulaire imbrique si nested
   * @param nomAttribut
   *          le nom de l'attribut a traiter
   * @param resultat
   *          le bout de code HTML à laquelle on ajoute l'attribut obligatoire
   * @param contexte
   *          le contexte de la page
   */
  public static void genererFermetureObligatoire(boolean obligatoire, boolean affichageIndObligatoire,
      String nomFormulaireImbrique, String nomAttribut, StringBuffer resultat, PageContext contexte) {

    // On vérifier si on a ajouté indicateur obligatoire.
    if (obligatoire || affichageIndObligatoire) {

      // On récupère le code html redéfini par l'application
      String htmlChampObligatoire = GestionParametreSysteme.getInstance().getString(
          ConstantesParametreSysteme.HTML_CHAMP_OBLIGATOIRE);

      // Si on a redéfini le style du champ obligatoire
      if (!UtilitaireString.isVide(htmlChampObligatoire)) {

        String[] st = htmlChampObligatoire.split("libelle");

        // On vérifie que le code html est valide :
        // PremierPartie{libelle}DeuxiemePartie
        if (isHtmlChampObligtoireValide(st)) {
          // On concaténer la deuxième partie du code html
          resultat.append(st[1]);
        }
      }

    }
  }

  /**
   * Ajouter un attribut obligatoire au référentiel des validations du formulaire.
   * 
   * @param contexte
   *          le contexte de la page.
   * @param nomAttribut
   *          le nom d'attribut obligatoire.
   * @param nomFormulaireImbrique
   *          le nom du formulaire imbrique.
   * @see org.sofiframework.presentation.struts.form.SommaireValidationForm
   */
  public static void ajouterAttributObligatoire(String nomFormulaireImbrique, String nomAttribut, PageContext contexte) {
    try {
      BaseForm baseForm = getBaseForm(contexte);
      String cle = getCleParametreSystemeAdresseAttributValidation(contexte, nomFormulaireImbrique);

      if ((GestionParametreSysteme.getInstance().getParametreSysteme(cle.toString()) == null)
          || !baseForm.isAttributsObligatoireEnCache()) {
        SommaireValidationForm sommaireValidationForm = null;

        sommaireValidationForm = (SommaireValidationForm) contexte.getRequest().getAttribute(cle);

        if (sommaireValidationForm == null) {
          sommaireValidationForm = new SommaireValidationForm();
          contexte.getRequest().setAttribute(cle.toString(), sommaireValidationForm);
        }

        // Ajouter l'attribut obligatoire.
        sommaireValidationForm.ajouterObligatoire(nomAttribut);
      }
    } catch (Exception e) {
    }
  }

  public static String getCleParametreSystemeAdresseAttributValidation(PageContext contexte, String nomListe) {
    String adresseActionValidation = getAdresseActionPourFormulaireCourant(contexte);

    if (adresseActionValidation != null) {
      StringBuffer cle = new StringBuffer();

      if (adresseActionValidation.indexOf("?") != -1) {
        cle.append(adresseActionValidation.substring(0, adresseActionValidation.indexOf("?")));
      } else {
        cle.append(adresseActionValidation);
      }

      cle.append(Constantes.LISTE_ATTRIBUT_VALIDATION_FORMULAIRE);

      if (nomListe != null) {
        cle.append(nomListe);
      }

      return cle.toString();
    } else {
      return null;
    }
  }

  /**
   * Permet d'obtenir la valeur EL (Ex: ${objet.propriete}) d'un parameter du tag.
   * <p>
   * 
   * @param nomParametre
   *          le nom du paramètre qui contient la valeur "valeurParametre"
   * @param valeurParametre
   *          la string représentant l'expression EL
   * @param classeParametre
   *          le type de retour que la valeur EL doit prendre
   * @param balise
   *          l'objet Tag qui appelle cette méthode
   * @param context
   *          le context de la page qui contient la valeur de l'expression JSTL
   * @return la valeur de l'objet traité par les expressions JSTL.
   */
  public static Object getValeurEL(String nomParametre, String valeurParametre, Class classeParametre,
      javax.servlet.jsp.tagext.Tag balise, javax.servlet.jsp.PageContext context) {
    try {
      return ExpressionEvaluatorManager.evaluate(nomParametre, valeurParametre, classeParametre, balise, context);
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return valeurParametre;
  }

  /**
   * Retourne un message d'erreur si l'attribut du formulaire traité est en erreur.
   * <p>
   * 
   * @param formulaire
   *          le formulaire en traitement
   * @param attribut
   *          le nom d'attribut a traité
   * @param contexte
   *          le contexte de la page
   * @return un message d'erreur s'il y a lieu
   * @see org.sofiframework.application.message.objetstransfert.MessageErreur
   */
  public static MessageErreur getMessageErreur(BaseForm formulaire, String attribut, PageContext contexte) {
    MessageErreur erreur = null;

    if (formulaire != null) {
      erreur = formulaire.getMessageErreur(attribut);

      // Traiter le message.
      if ((erreur != null) && UtilitaireString.isVide(erreur.getMessage())) {
        Message message = UtilitaireMessage.get(erreur.getCleMessage(), null, contexte);
        erreur.setMessage(message.getMessage());
      }
    }

    ajouterPremierMessageErreur(erreur, contexte);

    return erreur;
  }

  /**
   * Permet d'inactiver un bouton style image lorsque le client a appuyé une fois sur une image.
   * <p>
   * Évite la double-soumission. Il est important que le bout de code à insérer ne soit pas inclus à l'intérieur d'un
   * formulaire sinon la méthode javascript ne fonctionnera pas.
   * <p>
   * 
   * @param contexte
   *          le contexte de la page qui exécute le tag appelant
   * @return la fonction Javascript qui inactive les images
   */
  public static String getFonctionInactiverImage(PageContext contexte) {
    StringBuffer fonction = new StringBuffer();
    boolean jsEcrit = contexte.getAttribute("jsEcritListNavigation") != null;

    if (!jsEcrit) {
      // Fermeture de la balide non conforme au W3C lorsque la page est en
      // XHTML.
      // fonction.append("\n<input type=\"hidden\" name=\"navigue\" id=\"navi\" value=\"0\">");
      fonction.append("\n<input type=\"hidden\" name=\"navigue\" id=\"navi\" value=\"0\"");
      fonction.append(getElementClose(contexte));
      fonction.append("\n<script type=\"text/javascript\">");
      // Ajout d'un commentaire pour que les caractères spéciaux (<, >, ...) du
      // JS soient correctement interprétés en XHTML.
      if (isXhtml(contexte)) {
        fonction.append("\n//<!--");
      }
      fonction.append("\nfunction clickImage(url) {");
      fonction.append("\n  if (document.getElementById('navi').value == 0) {");
      fonction.append("\n    document.getElementById('navi').value = 1;");
      fonction.append("\n    disableImagesNavigation();");
      fonction.append("\n    setTimeout(\"window.location.href = '\" + url + \"';\", 50);");
      fonction.append("\n    ");
      fonction.append("\n  }");
      fonction.append("\n}");
      fonction.append("\nfunction disableImagesNavigation() {");
      fonction.append("\n  disableImage('bt_premier');");
      fonction.append("\n  disableImage('bt_precedent');");
      fonction.append("\n  disableImage('bt_suivant');");
      fonction.append("\n  disableImage('bt_dernier');");
      fonction.append("\n}");
      fonction.append("\nfunction disableImage(nom_image) {");
      fonction.append("\n  image = document.images[nom_image];");
      fonction.append("\n  if (image.src.indexOf('_off.gif') < 0) {");
      fonction.append("\n    image.src = image.src.substring(0, image.src.indexOf('.gif')) + '_off.gif';");
      fonction.append("\n  }");
      fonction.append("\n}");
      fonction.append("\n</script>\n");

      contexte.setAttribute("jsEcritListNavigation", Boolean.TRUE);
    }

    return fonction.toString();
  }

  /**
   * Générer un url avec suivi de session.
   * 
   * @param contexte
   *          le pageContext
   * @param lienComplet
   *          le lien hypertexte au complet.
   * @return le lien hypertexte avec suivi de session.
   * @throws javax.servlet.jsp.JspException
   *           l'exception JSP
   */
  public static String genererUrl(PageContext contexte, String lienComplet) throws JspException {
    if (!UtilitaireString.isVide(lienComplet)) {
      try {
        String url = TagUtils.getInstance().computeURL(contexte, null, lienComplet, null, null, null, new HashMap(),
            null, false);

        return url;
      } catch (MalformedURLException e) {
        TagUtils.getInstance().saveException(contexte, e);
        throw new JspException(e);
      }
    } else {
      return null;
    }
  }

  /**
   * Retourne l'adresse de l'action courante.
   * 
   * @param request
   *          la requête Http en traitement.
   * @return l'adresse de l'action courante.
   */
  public static String getAdresseActionCourante(ServletRequest request) {
    String adresse = (String) request.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE);

    if (adresse != null && adresse.indexOf("/") == 0) {
      adresse = adresse.substring(1);
    }

    return adresse;
  }

  public static boolean isActionSecurise(HttpServletRequest request) {

    Object actions = GestionParametreSysteme.getInstance().getListeValeursAvecSystemeCommun(
        ConstantesParametreSysteme.ACTION_NON_SECURISE);

    if (actions != null && actions instanceof String && actions.toString().indexOf(",") != -1) {
      actions = GestionParametreSysteme.getInstance().getListeValeurAvecChaineMultiple(actions.toString());
    }

    String action = getAdresseActionCourante(request);

    if (!String.class.isInstance(actions)) {
      HashSet listeActionNonSecurise = (HashSet) actions;

      if (listeActionNonSecurise != null) {
        if (listeActionNonSecurise.contains(action)) {
          return false;
        } else {
          return true;
        }
      }

      return true;
    } else {
      if (actions.equals(action)) {
        return false;
      } else {
        return true;
      }
    }
  }

  /**
   * Retourne l'adresse de l'action pour le formulaire en traitement.
   * 
   * @param request
   *          la requête Http en traitement.
   * @return l'adresse de l'action courante.
   */
  public static String getAdresseActionPourFormulaireCourant(PageContext contexte) {
    String adresse = (String) contexte.getRequest().getAttribute(Constantes.ADRESSE_ACTION_FORMULAIRE);

    if ((adresse != null) && (adresse.indexOf("/") == 0)) {
      adresse = adresse.substring(1);
    }

    return adresse;
  }

  public static String getNomSoumissionFormulaireCourant(PageContext contexte, String nomFormulaire) {
    String noSoumissionsFormulaire = (String) contexte.getRequest().getAttribute("noSoumissionFormulaire");

    if (noSoumissionsFormulaire == null) {
      noSoumissionsFormulaire = "1";

      contexte.getRequest().setAttribute("noSoumissionFormulaire", noSoumissionsFormulaire);
    } else {
      int valeurNoSoumissionFormulaire = Integer.valueOf(noSoumissionsFormulaire).intValue() + 1;
      noSoumissionsFormulaire = String.valueOf(valeurNoSoumissionFormulaire);
    }

    if (nomFormulaire == null) {
      nomFormulaire = "forms[0]";
    }

    StringBuffer nomFonctionJS = new StringBuffer();
    nomFonctionJS.append("fonctionSubmit");
    nomFonctionJS.append(noSoumissionsFormulaire);

    return nomFonctionJS.toString();
  }

  public static String genererSoumissionFormulaire(PageContext contexte, String lien, String nomFormulaire,
      boolean modificationFormulaire, boolean image, String imageInactive, String messageConfirmation)
          throws JspException {
    HttpServletRequest request = (HttpServletRequest) contexte.getRequest();
    StringBuffer resultat = new StringBuffer();

    String noSoumissionsFormulaire = (String) contexte.getSession().getAttribute("noSoumissionFormulaire");

    if (noSoumissionsFormulaire == null) {
      noSoumissionsFormulaire = "1";
      UtilitaireControleur.setAttributTemporairePourAction("noSoumissionFormulaire", noSoumissionsFormulaire, request);
    } else {
      int valeurNoSoumissionFormulaire = Integer.valueOf(noSoumissionsFormulaire).intValue() + 1;
      UtilitaireControleur.setAttributTemporairePourAction("noSoumissionFormulaire",
          String.valueOf(valeurNoSoumissionFormulaire), request);
      noSoumissionsFormulaire = String.valueOf(valeurNoSoumissionFormulaire);
    }

    if (nomFormulaire == null) {
      nomFormulaire = "forms[0]";
    }

    StringBuffer nomFonctionJS = new StringBuffer();
    nomFonctionJS.append("fonctionSubmit");
    nomFonctionJS.append(noSoumissionsFormulaire);

    contexte.setAttribute("nomFonctionSoumissionJS", nomFonctionJS);

    StringBuffer soumissionFormJS = new StringBuffer();
    soumissionFormJS.append("document.");
    soumissionFormJS.append(nomFormulaire);
    soumissionFormJS.append(".action ='");
    soumissionFormJS.append(lien);
    soumissionFormJS.append("';");

    if (modificationFormulaire) {
      soumissionFormJS.append("document.");
      soumissionFormJS.append(nomFormulaire);
      soumissionFormJS.append(".indModification.value = 1;");
    }

    // resultat.append("\n<script type=\"text/javascript\">");
    if (messageConfirmation != null) {
      // Si message de confirmation, ouvrir la condition.
      resultat.append("if (confirm('");
      resultat.append(UtilitaireString.convertirEnJavaScript(messageConfirmation));
      resultat.append("')) {");
    } else {
      resultat.append("function ");
      resultat.append(nomFonctionJS);
      resultat.append("() {");
    }

    if (image) {
      resultat.append(soumissionFormJS);
      resultat.append("    setTimeout('");
      resultat.append("document.").append(nomFormulaire).append(".submit();', 10);");
    } else {
      resultat.append(soumissionFormJS);
      resultat.append("document.");
      resultat.append(nomFormulaire);
      resultat.append(".submit();");
    }

    resultat.append("}");

    return resultat.toString();
  }

  /**
   * Est-ce que la valeur d'attribut doit être traité en EL.
   * 
   * @return true si la valeur d'attribut doit être traité en EL.
   * @param valeurAttribut
   *          la valeur d'attribut a traiter.
   */
  public static boolean isValeurEL(String valeurAttribut) {
    if ((valeurAttribut != null) && (valeurAttribut.length() > 1) && valeurAttribut.substring(0, 1).equals("$")) {
      return true;
    } else {
      return false;
    }
  }

  public static LinkedHashMap getListeMessageAvecHrefConfirmation(PageContext contexte) {
    LinkedHashMap listeMessageAvecHrefConfirmation = (LinkedHashMap) contexte.getRequest().getAttribute(
        "listeMessageAvecHrefConfirmation");

    if (listeMessageAvecHrefConfirmation == null) {
      listeMessageAvecHrefConfirmation = new LinkedHashMap();
      contexte.getRequest().setAttribute("listeMessageAvecHrefConfirmation", listeMessageAvecHrefConfirmation);
    }

    return listeMessageAvecHrefConfirmation;
  }

  /**
   * 
   * @param contexte
   * @param formatMessage
   * @param formatSortie
   * @param format
   * @param nomListe
   * @param nomAttribut
   */
  public static void traiterAttributFormat(String nomAttribut, String nomListe, String format, String formatSaisie,
      String formatSortie, String formatMessage, PageContext contexte) {
    BaseForm baseForm = getBaseForm(contexte);

    if (format != null) {
      String cle = getCleParametreSystemeAdresseAttributValidation(contexte, nomListe);

      if ((cle != null)
          && ((GestionParametreSysteme.getInstance().getParametreSysteme(cle.toString()) == null) || !baseForm
              .isAttributsValidationEnCache())) {
        SommaireValidationForm sommaireValidationForm = null;

        sommaireValidationForm = (SommaireValidationForm) contexte.getRequest().getAttribute(cle);

        if (sommaireValidationForm == null) {
          sommaireValidationForm = new SommaireValidationForm();
          contexte.getRequest().setAttribute(cle.toString(), sommaireValidationForm);
        }

        // Ajouter l'attribut obligatoire.
        sommaireValidationForm.ajouterFormat(nomAttribut, format, formatSaisie, formatSortie, formatMessage);
      }
    }
  }

  public static void traiterAttributFormat(String nomAttribut, String nomFormulaireImbrique, String type,
      String tailleMaximale, String messageErreur, PageContext contexte) {
    // Accéder au formulaire.
    BaseForm formulaire = UtilitaireBaliseJSP.getBaseForm(contexte);

    Integer taille = null;

    if (tailleMaximale != null) {
      taille = new Integer(tailleMaximale);
    }

    formulaire.ajouterFormat(nomFormulaireImbrique, nomAttribut, type, taille, messageErreur);
  }

  public static Locale getLocale(PageContext contexte) {
    return UtilitaireControleur.getLocale(contexte.getSession());
  }

  public static void ajouterPremierMessageErreur(MessageErreur erreur, PageContext contexte) {
    MessageErreur premierMessageErreur = (MessageErreur) contexte.getRequest().getAttribute(
        Constantes.PREMIER_MESSAGE_ERREUR);

    if ((premierMessageErreur == null) && (erreur != null)) {
      contexte.getRequest().setAttribute(Constantes.PREMIER_MESSAGE_ERREUR, erreur);
    }
  }

  public static String getNomFonctionModificationFormulaire(String nomFormulaireJS, String propriete) {
    StringBuffer nomFonction = new StringBuffer();
    nomFonction.append("notifier");
    nomFonction.append(nomFormulaireJS);
    nomFonction.append("_");

    propriete = UtilitaireString.remplacerTous(propriete, "[", "");
    propriete = UtilitaireString.remplacerTous(propriete, "]", "");
    propriete = UtilitaireString.remplacerTous(propriete, ".", "");
    nomFonction.append(propriete);

    return nomFonction.toString();
  }

  /**
   * Retourne le compteur présent qui identifie un élément unique pour une boite à cocher multiple.
   * 
   * @param contexte
   *          le contexte de la page.
   * @param nomAttribut
   *          le nom d'attribut multibox a traiter.
   */
  public static Integer getCompteurBoiteACocherMultibox(String nomAttribut, boolean incrementer, PageContext contexte) {
    // Partir un compteur pour le nombre de boite à cocher du même nom
    String attributCompteurBoite = "ind" + nomAttribut;
    Integer no = (Integer) contexte.getAttribute(attributCompteurBoite);

    if (no == null) {
      no = new Integer(1);
    } else {
      if (incrementer) {
        no = new Integer(no.intValue() + 1);
      }
    }

    if (incrementer) {
      contexte.setAttribute(attributCompteurBoite, no);
    }

    return no;
  }

  /**
   * Permet de générer une composante invisible pour être focusé. Utiliser surtout pour spécifier un message tout en
   * appliquant un focus ou le message est affiché.
   * 
   * @return la composante invisible
   * @param aideContextuelle
   *          l'aide contextuelle a afficher.
   * @param nomAttribut
   *          le nom d'attribut a focuser.
   */
  public static String genererChampInvisiblePourFocus(String nomAttribut, String aideContextuelle) {
    StringBuffer resultat = new StringBuffer();
    resultat.append("<input ");
    resultat.append(" type=image onclick=\"javascript:void(0);return false;\" id=\"");
    resultat.append(nomAttribut);
    resultat.append("\" class = \"invisibleErreur\" src = \"images/sofi/spacer.gif\"");

    if (aideContextuelle != null) {
      resultat.append(" title=\"");
      resultat.append(aideContextuelle);

      resultat.append("\"");
    }

    resultat.append("/>");

    return resultat.toString();
  }

  /**
   * Retourne le prochain nom de propriete vide (qui n'a pas été spécifié dans la balise).
   * 
   * @return le prochain nom de propriete vide
   * @param contexte
   *          le contexte de la page.
   */
  public static String getNomProprieteVide(PageContext contexte) {
    if (contexte.getSession().getAttribute(ConstantesBaliseJSP.PROPRIETE_VIDE) != null) {
      Integer compteurProprieteVide = (Integer) contexte.getSession().getAttribute(ConstantesBaliseJSP.PROPRIETE_VIDE);
      Integer prochaineValeur = new Integer(compteurProprieteVide.intValue() + 1);
      contexte.getRequest().setAttribute(ConstantesBaliseJSP.PROPRIETE_VIDE, prochaineValeur);

      UtilitaireControleur.setAttributTemporairePourAction(ConstantesBaliseJSP.PROPRIETE_VIDE, prochaineValeur,
          (HttpServletRequest) contexte.getRequest());

      return ConstantesBaliseJSP.PROPRIETE_VIDE + prochaineValeur.toString();
    } else {
      UtilitaireControleur.setAttributTemporairePourAction(ConstantesBaliseJSP.PROPRIETE_VIDE, new Integer("1"),
          (HttpServletRequest) contexte.getRequest());

      return ConstantesBaliseJSP.PROPRIETE_VIDE + "1";
    }
  }

  /**
   * Fixer le prochain nom de message d'attente unique.
   * 
   * @param request
   *          la requête présentement en traitement.
   */
  public static void setProchainNomMessageAttente(ServletRequest request) {
    Integer compteurNbMessageAttente = (Integer) request.getAttribute("sofi_nb_message_attente");

    if (compteurNbMessageAttente == null) {
      compteurNbMessageAttente = new Integer(0);
    }

    compteurNbMessageAttente = new Integer(compteurNbMessageAttente.intValue() + 1);
    request.setAttribute("sofi_nb_message_attente", compteurNbMessageAttente);
  }

  /**
   * Retourne le nom unique de message attente.
   * 
   * @return le nom unique de message attente.
   * @param request
   *          la requête présentement en traitement.
   */
  public static String getNomMessageAttente(ServletRequest request) {
    Integer compteurNbMessageAttente = (Integer) request.getAttribute("sofi_nb_message_attente");

    return "message_attente_" + compteurNbMessageAttente;
  }

  /**
   * Génère un message d'attente.
   * 
   * @return un message d'attente.
   * @param contexte
   *          le contexte de la page.
   * @param libelleMessageAttente
   *          le libellé de message d'attente.
   */
  public static String genererMessageAttente(String libelleMessageAttente, PageContext contexte) {
    StringBuffer codeHtml = new StringBuffer();
    UtilitaireVelocity utilitaireVelocity = new UtilitaireVelocity();
    Message message = UtilitaireMessage.get(libelleMessageAttente, null, contexte);
    utilitaireVelocity.ajouterAuContexte("libelleMessageAttente", message.getMessage());
    utilitaireVelocity.ajouterAuContexte("nomMessageAttente", getNomMessageAttente(contexte.getRequest()));

    codeHtml.append(utilitaireVelocity
        .genererHTML("org/sofiframework/presentation/velocity/gabarit/message_attente.vm"));

    return codeHtml.toString();
  }

  /**
   * Permet de générer les évènements JavaScript qui permet d'afficher un message d'atttente.
   * 
   * @return les évènements JavaScript qui permet d'afficher un message d'atttente.
   * @param positionY
   *          la position Y relative du message d'attente.
   * @param positionX
   *          la position X relative du message d'attente.
   * @param contexte
   *          le contexte de la page.
   */
  public static String genererEvenementMessageAttente(PageContext contexte, String libelle, String idBouton,
      String positionX, String positionY) {
    // StringBuffer onClickBuffer = new StringBuffer();
    // onClickBuffer.append("move('");
    // onClickBuffer.append(UtilitaireBaliseJSP.getNomMessageAttente(
    // contexte.getRequest()));
    // onClickBuffer.append("',");
    //
    // if (!UtilitaireString.isVide(positionY)) {
    // onClickBuffer.append(positionY);
    // } else {
    // onClickBuffer.append(-150);
    // }
    //
    // onClickBuffer.append(", ");
    //
    // if (!UtilitaireString.isVide(positionX)) {
    // onClickBuffer.append(positionX);
    // } else {
    // onClickBuffer.append(0);
    // }
    //
    // onClickBuffer.append(");show('");
    // onClickBuffer.append(UtilitaireBaliseJSP.getNomMessageAttente(
    // contexte.getRequest()));
    // onClickBuffer.append("');");
    //
    // return onClickBuffer.toString();
    StringBuffer onClickBuffer = new StringBuffer();
    onClickBuffer.append("afficherMessageAttente('");
    onClickBuffer.append(libelle);
    onClickBuffer.append("',");
    onClickBuffer.append("'");
    onClickBuffer.append(idBouton);
    onClickBuffer.append("',");

    if (!UtilitaireString.isVide(positionY)) {
      onClickBuffer.append(positionY);
    } else {
      onClickBuffer.append(0);
    }

    onClickBuffer.append(", ");

    if (!UtilitaireString.isVide(positionX)) {
      onClickBuffer.append(positionX);
    } else {
      onClickBuffer.append(0);
    }

    onClickBuffer.append(");");

    return onClickBuffer.toString();
  }

  /**
   * Retourne le ou les fonctions JavaScript qui doivent être appliqué sur le onload.
   * 
   * @return le ou les fonctions JavaScript séparé par une virgule.
   */
  public static String getFonctionJSOnLoad() {
    StringBuffer resultat = new StringBuffer();
    String onLoadMenuMultiMenu = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.FONCTION_AFFICHAGE_MULTIMENU_HORIZONTAL);

    if (!onLoadMenuMultiMenu.equals("")) {
      resultat.append(onLoadMenuMultiMenu);
    }

    String onLoadJavaScript = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.FONCTION_JAVASCRIPT_ONLOAD);

    if (!onLoadJavaScript.equals("")) {
      resultat.append(onLoadJavaScript);
    }

    return resultat.toString();
  }

  /**
   * Retourne vrai si il y a de l'aide en ligne sur le composant passé en paramètre
   * 
   * @return boolean
   * @param contexte
   * @param identifiant
   */
  public static boolean isAideEnligne(String identifiant, PageContext contexte) {
    Aide aideEnLigne = null;

    try {
      aideEnLigne = GestionAide.getInstance().getAide(GestionSecurite.getInstance().getObjetSecurisable(identifiant),
          UtilitaireBaliseJSP.getLocale(contexte));
    } catch (Exception e) {
    }

    return (aideEnLigne != null);
  }

  /**
   * Permet de générer un icone d'aide avec bulle d'aide contextuelle.
   * 
   * @param contexte
   *          le contexte de la page.
   * @param attribut
   *          l'attribut dont l'icone d'aide est associé.
   * @param resultat
   *          le resultat Html
   * @param identifiant
   *          l'identifiant du libellé a traiter.
   */
  public static void genererIcodeAide(String identifiant, String attribut, StringBuffer resultat, PageContext contexte)
      throws JspException {
    String contenuAide = null;

    boolean aideEnLigne = false;

    if (UtilitaireBaliseJSP.isAideEnligne(identifiant, contexte)) {
      Href lienAide = new Href(GestionAide.getInstance().getUrlAide());
      lienAide.ajouterParametre("sofi_seq_objet_java_aide",
          GestionSecurite.getInstance().getObjetSecurisable(identifiant).getSeqObjetSecurisable());

      resultat.append("&nbsp;<a class=\"aide\"");
      resultat.append(" id = \"aide_");
      resultat.append(attribut);
      resultat.append("\" href=\"javascript:void(0)\" onClick=\"javascript:");

      if (!GestionAide.getInstance().isAjax()) {
        resultat.append(genererFonctionPopupAide(lienAide.toString(), contexte));
      } else {
        resultat.append(genererFonctionPopupAjax(lienAide.toString(), "aide_" + attribut, contexte));
      }

      resultat.append("\">");

      if (!UtilitaireString.isVide(GestionParametreSysteme.getInstance().getString("iconeAide"))) {
        resultat.append(GestionParametreSysteme.getInstance().getParametreSysteme("iconeAide"));
      } else {
        resultat.append("&nbsp;&nbsp;");
      }

      resultat.append("</a>");
      aideEnLigne = true;
    }

    if ((contenuAide == null) && !aideEnLigne) {
      Libelle libelle = UtilitaireLibelle.getLibelle(identifiant, contexte);

      if (!UtilitaireString.isVide(libelle.getAideContextuelleExterne())) {
        Boolean multilingue = GestionParametreSysteme.getInstance().getBoolean(
            ConstantesParametreSysteme.AIDE_CONTEXTUELLE_MULTILINGUE);
        String extension = GestionParametreSysteme.getInstance().getString(
            ConstantesParametreSysteme.AIDE_CONTEXTUELLE_PAGE_EXTENSION);

        String structureUrlExterneAide = GestionParametreSysteme.getInstance().getString(
            ConstantesParametreSysteme.AIDE_STRUCTURE__EXTERNE);

        if (UtilitaireString.isVide(structureUrlExterneAide)) {
          throw new SOFIException(
              "Vous devez spécifier une valeur pour le paramètre système 'aideStructureUrlExterne'.");
        }

        String contenu = ContenuDynamiqueTag.getContenuDynamique(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_BASE_URL,
            null, libelle.getAideContextuelleExterne(), extension, multilingue.booleanValue(), structureUrlExterneAide,
            false, null, null, null, null, false,  contexte);

        contenuAide = contenu;
      } else {
        if (!UtilitaireString.isVide(libelle.getAideContextuelleHtml())) {
          contenuAide = libelle.getAideContextuelleHtml();
        } else {
          contenuAide = libelle.getAideContextuelle();
        }
      }

      if (StringUtils.isNotEmpty(contenuAide)) {
        resultat.append("&nbsp;<a class=\"aide\" title = \"");
        resultat.append(contenuAide);
        resultat.append("\">");

        if (!UtilitaireString.isVide(GestionParametreSysteme.getInstance().getString("iconeAide"))) {
          resultat.append(GestionParametreSysteme.getInstance().getParametreSysteme("iconeAide"));
        } else {
          resultat.append("&nbsp;&nbsp;");
        }

        resultat.append("</a>");
      }
    }
  }

  /**
   * Permet de générer la fonction du popup d'aide générique à partir des informations fournient par la balise AideTag
   * 
   * @throws javax.servlet.jsp.JspException
   * @return le code html de la fonction du popup d'Aide
   * @param pageContext
   * @param href
   */
  public static String genererFonctionPopupAide(String href, PageContext pageContext) throws JspException {
    return genererFonctionPopup(href,
        ((Boolean) GestionAide.getInstance().getInfoPopupAide().get("maximiser")).booleanValue(), pageContext,
        (String) GestionAide.getInstance().getInfoPopupAide().get("paramId"), (String) GestionAide.getInstance()
        .getInfoPopupAide().get("paramName"),
        (String) GestionAide.getInstance().getInfoPopupAide().get("paramProperty"), (String) GestionAide.getInstance()
        .getInfoPopupAide().get("paramScope"), (String) GestionAide.getInstance().getInfoPopupAide().get("name"),
        (String) GestionAide.getInstance().getInfoPopupAide().get("property"), (String) GestionAide.getInstance()
        .getInfoPopupAide().get("scope"),
        ((Boolean) GestionAide.getInstance().getInfoPopupAide().get("adresseSurMemeDomaine")).booleanValue(),
        (String) GestionAide.getInstance().getInfoPopupAide().get("nomFenetre"), (String) GestionAide.getInstance()
        .getInfoPopupAide().get("largeurFenetre"),
        (String) GestionAide.getInstance().getInfoPopupAide().get("hauteurFenetre"), (String) GestionAide.getInstance()
        .getInfoPopupAide().get("scrollbars"),
        ((Boolean) GestionAide.getInstance().getInfoPopupAide().get("barreOutil")).booleanValue(),
        ((Boolean) GestionAide.getInstance().getInfoPopupAide().get("barreMenu")).booleanValue(),
        ((Boolean) GestionAide.getInstance().getInfoPopupAide().get("barreAdresse")).booleanValue());
  }

  /**
   * Permet de générer une fonction de popup
   * 
   * @throws javax.servlet.jsp.JspException
   * @return le code html de la fonction de popup
   * @param barreAdresse
   * @param barreMenu
   * @param barreOutil
   * @param scrollbars
   * @param hauteurFenetre
   * @param largeurFenetre
   * @param nomFenetre
   * @param adresseSurMemeDomaine
   * @param scope
   * @param property
   * @param name
   * @param paramScope
   * @param paramProperty
   * @param paramName
   * @param paramId
   * @param pageContext
   * @param maximiser
   * @param href
   */
  public static String genererFonctionPopup(String href, boolean maximiser, PageContext pageContext, String paramId,
      String paramName, String paramProperty, String paramScope, String name, String property, String scope,
      boolean adresseSurMemeDomaine, String nomFenetre, String largeurFenetre, String hauteurFenetre,
      String scrollbars, boolean barreOutil, boolean barreMenu, boolean barreAdresse) throws JspException {
    StringBuffer popupBuffer = new StringBuffer();

    if (!maximiser) {
      popupBuffer.append("popup('");
    } else {
      popupBuffer.append("popupMaximise('");
    }

    String url = null;
    String forward = null;
    String page = null;
    String action = null;
    String anchor = null;

    // Identify the parameters we will add to the completed URL
    Map params = TagUtils.getInstance().computeParameters(pageContext, paramId, paramName, paramProperty, paramScope,
        name, property, scope, false);

    try {
      url = TagUtils.getInstance().computeURL(pageContext, forward, href.trim(), page, action, null, params, anchor,
          false);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(pageContext, e);
      throw new JspException(e.toString());
    }

    if (adresseSurMemeDomaine) {
      popupBuffer.append("../");
    }

    popupBuffer.append(url);

    popupBuffer.append("','");

    if (nomFenetre == null) {
      nomFenetre = "popup";
    }

    popupBuffer.append(nomFenetre);

    if (!maximiser) {
      popupBuffer.append("','");
      popupBuffer.append(largeurFenetre);
      popupBuffer.append("','");
      popupBuffer.append(hauteurFenetre);
    }

    popupBuffer.append("','");

    popupBuffer.append("status=1,resizable=1");

    if (barreOutil) {
      popupBuffer.append(",toolbar=1");
    }

    if (barreMenu) {
      popupBuffer.append(",menubar=1");
    }

    if (barreAdresse) {
      popupBuffer.append(",location=1");
    } else {
      popupBuffer.append(",location=0");
    }

    popupBuffer.append(",scrollbars=");

    if ((scrollbars == null) || scrollbars.trim().toLowerCase().equals("true")) {
      popupBuffer.append("1");
    } else {
      popupBuffer.append("0");
    }

    popupBuffer.append("')");
    popupBuffer.append(";");

    return popupBuffer.toString();
  }

  public static String genererFonctionPopupAjax(String href, String property, PageContext contexte) throws JspException {
    String nomFenetre = (String) GestionAide.getInstance().getInfoPopupAide().get("nomFenetre");
    String largeurFenetre = (String) GestionAide.getInstance().getInfoPopupAide().get("largeurFenetre");
    String hauteurFenetre = (String) GestionAide.getInstance().getInfoPopupAide().get("hauteurFenetre");

    StringBuffer popupBuffer = new StringBuffer();

    popupBuffer.append("genererFenetreFlottanteAJAX('");

    if (nomFenetre == null) {
      nomFenetre = "popup";
    }

    popupBuffer.append(nomFenetre);
    popupBuffer.append("','");

    popupBuffer.append(property);
    popupBuffer.append("','");

    String url = null;
    String forward = null;
    String page = null;
    String action = null;
    String anchor = null;

    // Identify the parameters we will add to the completed URL
    Map params = TagUtils.getInstance()
        .computeParameters(contexte, null, null, null, null, null, property, null, false);

    try {
      url = TagUtils.getInstance().computeURL(contexte, forward, href, page, action, null, params, anchor, false);
    } catch (MalformedURLException e) {
      TagUtils.getInstance().saveException(contexte, e);
      throw new JspException(e.toString());
    }

    popupBuffer.append(url);

    popupBuffer.append("','");
    popupBuffer.append(largeurFenetre);
    popupBuffer.append("','");
    popupBuffer.append(hauteurFenetre);
    popupBuffer.append("','");
    popupBuffer.append("null");
    popupBuffer.append("','");
    popupBuffer.append("null");

    popupBuffer.append("',event)");
    popupBuffer.append(";");

    return popupBuffer.toString();
  }

  public static String getNomFonctionJSEnCours(PageContext contexte) {
    try {
      return ((StringBuffer) contexte.getAttribute("nomFonctionSoumissionJS")).toString();
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * Appliquer un filtre sur les valeurs d'un cache.
   */
  public static Map appliquerFiltreCache(Map collectionCache, String nomClasseFiltre, BaliseValeurCache balise,
      PageContext pageContext) throws JspException {
    Map collection = null;
    if (nomClasseFiltre != null) {
      try {
        FiltreCollection filtreCollection = (FiltreCollection) Thread.currentThread().getContextClassLoader()
            .loadClass(nomClasseFiltre).newInstance();
        collection = (Map) filtreCollection.filtrer(collectionCache, balise, pageContext);
      } catch (Exception ex) {
        if (log.isErrorEnabled()) {
          log.error("Erreur pour filtrer la collection.", ex);
        }
        throw new JspException("Erreur pour filtrer la collection.", ex);
      }
    } else {
      collection = collectionCache;
    }
    return collection;
  }

  /**
   * Permet de spécifier si le composant en traitement est accessible selon une sécurité applicative spécifié par une
   * classe spécifique.
   * 
   * @param classeSecuriteApplicative
   *          la classe de sécurité applicative a appliquer.
   * @param contexte
   *          le contexte de la page.
   * @return true si le composant est accesssible.
   * @since SOFI 2.0.4
   */
  public static boolean isAccesComposant(String classeSecuriteApplicative, PageContext contexte) {
    SecuriteApplicative securiteApplicative = null;

    if (classeSecuriteApplicative != null) {
      try {
        securiteApplicative = (SecuriteApplicative) Thread.currentThread().getContextClassLoader()
            .loadClass(classeSecuriteApplicative).newInstance();
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur lors de l'instanciation de la classe de la securite applicative.", e);
        }
      }
    }

    if (securiteApplicative == null || securiteApplicative.isComposantDisponible(contexte)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Permet de spécifier si le composant en traitement doit être en lecture seulement selon une sécurité applicative
   * spécifié par une classe spécifique.
   * 
   * @param classeSecuriteApplicative
   *          la classe de sécurité applicative a appliquer.
   * @param contexte
   *          le contexte de la page.
   * @return true si le composant est accesssible en lecture seulement.
   * @since SOFI 2.0.4
   */
  public static boolean isAccesComposantLectureSeulement(String classeSecuriteApplicative, PageContext contexte) {
    SecuriteApplicative securiteApplicative = null;

    if (classeSecuriteApplicative != null) {
      try {
        securiteApplicative = (SecuriteApplicative) Thread.currentThread().getContextClassLoader()
            .loadClass(classeSecuriteApplicative).newInstance();
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur lors de l'instanciation de la classe de la securite applicative.", e);
        }
      }
    }

    if (securiteApplicative != null && securiteApplicative.isComposantLectureSeulement(contexte)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Indique que la sécurité doit être activé pour le composant en traitement.
   * 
   * @param contexte
   *          le contexte de la page.
   * @return true si la sécurité doit être activé pour le composant en traitement.
   */
  public static boolean isSecuriteActif(PageContext contexte) {
    HttpServletRequest request = (HttpServletRequest) contexte.getRequest();
    boolean actionSecurise = UtilitaireControleur.isActionSecurise(request, getActionCourant(request));
    if (GestionSecurite.getInstance().isSecuriteActif() && actionSecurise) {
      return true;
    } else {
      return false;
    }
  }

  public static String getActionCourant(HttpServletRequest request) {
    String url = getAdresseActionCourante(request);
    if (url != null) {
      int debut = url.lastIndexOf("/") + 1;
      int fin = url.lastIndexOf(".");
      return url.substring(debut, fin);
    } else {
      return "";
    }

  }

  public static boolean isCacherLienInterdit(String href, HttpServletRequest request) {
    boolean cacherLien = false;
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
    Boolean validerHrefLien = GestionParametreSysteme.getInstance().getBoolean(
        ConstantesParametreSysteme.CACHER_HREF_SECURISE);

    if (validerHrefLien.booleanValue()) {
      boolean hrefInterdit = UtilitaireControleur.isActionSecurise(request)
          && (GestionSecurite.getInstance().getListeObjetSecurisablesParAdresse().get(href) != null)
          && (utilisateur.getListeObjetSecurisablesParAdresse().get(href) == null);
      cacherLien = hrefInterdit;
    }

    return cacherLien;
  }

  /**
   * Permet de spécifier si le formulaire est en mode lecture seulement.
   * 
   * @param contexte
   *          le contexte de la page
   * @param propriete
   *          la propriété en traitement s'il y a lieu.
   * @return true si le formulaire correspondant à la propriété en cours est en lecture seulement.
   */
  public static boolean isFormulaireEnLectureSeulement(PageContext contexte, String propriete) {
    BaseForm formulaire = null;

    if (propriete == null) {
      propriete = NestedPropertyHelper.getCurrentProperty((HttpServletRequest) contexte.getRequest());

    }

    // Extraire le formulaire en traitement.
    formulaire = UtilitaireBaliseNested.getFormulaireCourant(contexte, propriete);

    // Valider tout d'abord si dans un bloc en lecture seulement.
    boolean lectureSeule = contexte.getRequest().getAttribute(BlocSecuriteTag.MODE_LECTURE_SEULEMENT) != null;

    if (formulaire != null && !lectureSeule) {
      lectureSeule = formulaire.isModeLectureSeulement();

      if ((formulaire.getFormulaireParent() != null) && !lectureSeule) {
        lectureSeule = formulaire.getFormulaireParent().isModeLectureSeulement();
      }
    }

    return lectureSeule;
  }

  /**
   * Retourne le parametre a ajouter dans un url pour meilleur de la cache des navigateurs Web. La valeur du parametre
   * sera la date et l'heure du derniere deploiement.
   * 
   * @param premierParametre
   *          est-ce le premier parametre de l'url
   * @return le parametre a ajouter dans un url pour meilleur de la cache des navigateurs Web.
   */
  public static String getParametreUrlDateDeploiement(boolean premierParametre) {
    StringBuffer resultat = new StringBuffer();
    String dateDeploiement = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.DATE_DEPLOIEMENT);

    if (!UtilitaireString.isVide(dateDeploiement)) {
      if (premierParametre) {
        resultat.append("?");
      } else {
        resultat.append("&");
      }
      resultat.append("dateDeploiement=");
      resultat.append(dateDeploiement.replaceAll(" ", "%20"));
    }

    return resultat.toString();
  }
}
