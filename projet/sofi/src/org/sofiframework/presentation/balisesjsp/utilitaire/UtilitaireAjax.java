/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.utilitaire;

import java.util.StringTokenizer;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.nested.UtilitaireBaliseNested;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe utilitaire qui offre divers fonctionnalités nécessaires pour la génération
 * de fonctionnalité Ajax dans les balises JSP.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.5
 */
public class UtilitaireAjax {
  /** L'objet de log pour cette classe */
  private static Log log = LogFactory.getLog(UtilitaireAjax.class);

  /**
   * Générer un traitement JavaScript qui permet de faire une requête Http en GET.
   * @param href le ligne hypertexte à appeller.
   * @return le traitement JavaScript qui permet de faire une requête Http en GET.
   */
  static public String traiterRequeteHttpGET(Href href) {
    StringBuffer resultat = new StringBuffer();
    resultat.append(getLienAjax(href.getUrlPourRedirection(), null, null));
    resultat.append("traiterRequeteGET(lienAjax);");

    return resultat.toString();
  }

  /**
   * Générer un traitement JavaScript qui permet de faire une requête Http en GET avec réponse du serveur.
   * @param href le ligne hypertexte à appeller.
   * @return le traitement JavaScript qui permet de faire une requête Http en GET.
   */
  static public String traiterRequeteHttpGETAvecReponse(String href,
      String nomDiv, String nomElement) {
    StringBuffer resultat = new StringBuffer();

    if (href != null) {
      resultat.append(getLienAjax(href, null, null));
    }

    resultat.append("var reponse = traiterRequeteGETAvecReponseHTML(lienAjax);");

    String nomId = null;

    if (nomDiv != null) {
      nomId = nomDiv;
    } else {
      nomId = nomElement;
    }

    resultat.append("if (reponse != 'erreur500') { document.getElementById('");
    resultat.append(nomId);
    resultat.append("').");

    if (nomDiv != null) {
      resultat.append("innerHTML");
    } else {
      resultat.append("value");
    }

    resultat.append("= reponse;}void(0);");

    return resultat.toString();
  }

  /**
   * Retourne le lien standard Ajax avec SOFI.
   * @return le lien standard Ajax
   * @param href l'adresse a appeler en Ajax.
   */
  static public String getLienAjax(String href, String nomParametre, String id) {
    StringBuffer resultat = new StringBuffer();
    resultat.append("lienAjax = '");
    resultat.append(href);
    resultat.append("'+ '&ajax=true&ms=' + heureCourante()");

    if (nomParametre != null) {
      resultat.append("+ '&");
      resultat.append(nomParametre);
      resultat.append("=' + ");
      resultat.append("escape(document.getElementById('");
      resultat.append(id);
      resultat.append("').value)");
    }

    resultat.append(";");

    return resultat.toString();
  }

  /**
   * Est-ce qu'il y a un traitement d'affichage Ajax en cours.
   * @return true s'il y a un traitement d'affichage Ajax en cours.
   * @param request la requête en cours.
   */
  public static boolean isAffichageAjax(ServletRequest request) {
    return request.getAttribute(Constantes.AFFICHAGE_AJAX) != null;
  }

  static public String traiterAffichageDivAjaxAsynchrone(String href,
      String nomDiv) {
    StringBuffer resultat = new StringBuffer();

    if (href != null) {
      resultat.append(getLienAjax(href, null, null));
    }

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterAffichageDivAjaxAsynchrone(lienAjax,'");
    resultat.append(nomDiv);
    resultat.append("');");

    return resultat.toString();
  }

  static public String traiterAffichageDivAjaxAsynchrone(String href,
      String nomDiv, String nomParametre, String idValeur,
      String fonctionJSPendantChargement, String fonctionJSApresChargement,
      PageContext contexte) {
    StringBuffer resultat = new StringBuffer();

    if (href != null) {
      if ((nomDiv != null) && (nomDiv.indexOf("combo.") == -1)) {
        href += "&ajaxDiv=true";
      } else {
        if ((nomDiv != null) && (nomDiv.indexOf("combo.") != -1)) {
          nomDiv = nomDiv.substring(nomDiv.indexOf("combo.") + 6);
        }
      }

      resultat.append(getLienAjax("lienAjax", href, nomParametre, idValeur,
          contexte));
    }

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterAffichageDivAjaxAsynchrone(lienAjax,'");
    resultat.append(nomDiv);
    resultat.append("','");
    resultat.append(UtilitaireString.convertirEnJavaScript(
        fonctionJSPendantChargement));
    resultat.append("','");
    resultat.append(UtilitaireString.convertirEnJavaScript(
        fonctionJSApresChargement));
    resultat.append("');");

    return resultat.toString();
  }

  /**
   * Générer un traitement JavaScript qui permet de faire une requête Http en GET asynchrone.
   * @param href le ligne hypertexte à appeller.
   * @return le traitement JavaScript qui permet de faire une requête Http en GET asynchrone.
   */
  static public String traiterRequeteHttpGETAsynchrone(Href href) {
    StringBuffer resultat = new StringBuffer();
    resultat.append(getLienAjax(href.toString(), null, null));

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterRequeteGETASynchrone(lienAjax);");

    return resultat.toString();
  }
  
  /**
   * Générer un traitement JavaScript qui permet de faire une requête Http en GET asynchrone.
   * @param href le ligne hypertexte à appeller.
   * @param idAttributRetourValeur l'id de l'attribut retour de la valeur
   * @return le traitement JavaScript qui permet de faire une requête Http en GET asynchrone.
   */
  static public String traiterRequeteHttpGETAvecReponse(Href href, String idAttributRetourValeur) {
    StringBuffer resultat = new StringBuffer();
    resultat.append(getLienAjax(href.toString(), null, null));

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterRequeteGETAvecReponse(lienAjax,");
    resultat.append(idAttributRetourValeur);
    resultat.append(");");
    return resultat.toString();
  }

  /**
   * Générer un traitement Javascript Ajax qui permet de remplir une liste déroulante dynamiquement.
   * @return le traitement Javascript qui permet la population d'une liste déroulante dynamiquement.
   * @param nomParametre le nom de paramètre qui contient la valeur sélectionné.
   * @param idProprieteSource l'identifiant unique de propriété à remplir.
   * @param idProprieteDestination l'identifiant unique de propriété à remplir.
   * @param href le lien hypertexte à appeller.
   */
  static public String traiterListeDeroulanteAjaxAsynchrone(String href,
      String idProprieteSource, String idProprieteDestination, String nomParametre) {
    StringBuffer resultat = new StringBuffer();

    if (href != null) {
      resultat.append(getLienAjax(href, nomParametre, idProprieteSource));
    }

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterListeDeroulantAjaxAsynchrone(lienAjax,'");
    resultat.append(idProprieteDestination);
    resultat.append("');");

    return resultat.toString();
  }

  static public String traiterAffichageDivAjaxAsynchroneAvecMessage(
      String href, String nomDiv, String nomParametre, String id,
      String hrefMessage, String nomDivMessage, String fonctionJSAvantChargement,
      String fonctionJSApresChargement, PageContext contexte) {
    StringBuffer resultat = new StringBuffer();

    if (href != null) {
      resultat.append(getLienAjax("lienAjax", href, nomParametre, id, contexte));
    }

    if (hrefMessage != null) {
      resultat.append(getLienAjax("lienAjax2", hrefMessage, null, null, contexte));
    }

    Boolean debugAjax = GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.DEBUG_AJAX);

    if (debugAjax.booleanValue()) {
      resultat.append("setDebugAjax(true);");
    }

    resultat.append("traiterAffichageDivAjaxAsynchroneAvecMessage(lienAjax,'");
    resultat.append(nomDiv);
    resultat.append("', lienAjax2,'");
    resultat.append(nomDivMessage);
    resultat.append("','");
    resultat.append(UtilitaireString.convertirEnJavaScript(
        fonctionJSAvantChargement));
    resultat.append("','");
    resultat.append(UtilitaireString.convertirEnJavaScript(
        fonctionJSApresChargement));
    resultat.append("');");

    return resultat.toString();
  }

  /**
   * Retourne le lien standard Ajax avec SOFI.
   * @return le lien standard Ajax
   * @param href l'adresse a appeler en Ajax.
   * @since SOFI 2.0.1 Support de plusieurs paramètres associé a leur valeur.
   */
  static public String getLienAjax(String nomParametreLienJS, String href,
      String nomParametre, String id, PageContext contexte) {
    StringBuffer resultat = new StringBuffer();

    resultat.append(nomParametreLienJS);
    resultat.append(" = '");

    resultat.append(href);
    resultat.append("'+ '&ajax=true&ms=' + heureCourante()");

    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(contexte,
        null);

    if (nomParametre != null) {
      if (id == null) {
        throw new SOFIException(
            "ERREUR : Vous devez spécifier une propriété 'nomParametreValeur' lorsque vous spécifiez une propriété 'idProprieteValeur'");
      }

      if (nomParametre.indexOf(",") != -1) {
        StringTokenizer listeNomParametre = new StringTokenizer(nomParametre,
            ",");
        StringTokenizer listeId = new StringTokenizer(id, ",");

        if (listeNomParametre.countTokens() != listeId.countTokens()) {
          throw new SOFIException(
              "ERREUR : Vous devez spécifier un nombre égal de propriété 'nomParametreValeur' à la une propriété 'idProprieteValeur'");
        }

        while (listeNomParametre.hasMoreTokens()) {
          String tNomParametre = listeNomParametre.nextToken().trim();
          String tId = listeId.nextToken().trim();
          StringBuffer idComplet = new StringBuffer();

          if (nomFormulaireImbrique != null) {
            if ((tId.indexOf(".") == -1) ||
                (tId.indexOf(nomFormulaireImbrique + ".") == -1)) {
              idComplet.append(nomFormulaireImbrique);
              idComplet.append(".");
            }

            idComplet.append(tId.trim());
          } else {
            idComplet.append(tId.trim());
          }

          resultat.append(genererProprieteAvecValeurARetourner(tNomParametre,
              idComplet.toString()));
        }
      } else {
        StringBuffer idComplet = new StringBuffer();

        if (nomFormulaireImbrique != null) {
          if ((id.indexOf(".") == -1) ||
              (id.indexOf(nomFormulaireImbrique + ".") == -1)) {
            idComplet.append(nomFormulaireImbrique);
            idComplet.append(".");
          }

          idComplet.append(id.trim());
        } else {
          idComplet.append(id.trim());
        }

        resultat.append(genererProprieteAvecValeurARetourner(
            nomParametre.trim(), idComplet.toString()));
      }
    }

    resultat.append(";");

    return resultat.toString();
  }

  static private String genererProprieteAvecValeurARetourner(
      String nomParametre, String id) {
    StringBuffer resultat = new StringBuffer();
    resultat.append("+ '&");
    resultat.append(nomParametre);
    resultat.append("=' + ");
    resultat.append("escape(document.getElementById('");
    resultat.append(id);
    resultat.append("').value)");

    return resultat.toString();
  }
}
