/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.utilitaire;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;


/**
 * Classe utilitaire afin d'aider à évaluer les expressions régulière JSTL.
 * Principalement, il encapsule les appels à la classe ExpressionEvaluationManager
 * pour faciliter l'utilisation de cette classe.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class EvaluateurExpression {
  /**
   * Évaluer une expression régulière qui retourne un objet du type spécifier
   * @param nomAttribut le nom de l'attribut
   * @param valeurAttribut la valeur de l'attribut
   * @param classeRetour la classe de l'instance de retour
   * @param balise la balise à traiter
   * @param contexte le contexte de la balise
   * @return le résultat de l'évaluation dans l'objet spécifier
   * @throws javax.servlet.jsp.JspException
   */
  public static Object evaluer(String nomAttribut, String valeurAttribut,
      Class classeRetour, Tag balise, PageContext contexte)
          throws JspException {
    Object resultat = null;

    if (valeurAttribut != null) {
      resultat = ExpressionEvaluatorManager.evaluate(nomAttribut,
          valeurAttribut, classeRetour, balise, contexte);
    }

    return resultat;
  }

  /**
   * Évaluer une expression régulière qui retourne un String
   * @param nomAttribut le nom de l'attribut
   * @param valeurAttribut la valeur de l'attribut
   * @param classeRetour la classe de l'instance de retour
   * @param balise la balise à traiter
   * @param contexte le contexte de la balise
   * @return le résultat de l'évaluation en String
   * @throws javax.servlet.jsp.JspException
   */
  public static String evaluerString(String nomAttribut, String valeurAttribut,
      Tag balise, PageContext contexte) throws JspException {
    return (String) evaluer(nomAttribut, valeurAttribut, String.class, balise,
        contexte);
  }

  /**
   * Évaluer une expression régulière qui retourne un boolean
   * @param nomAttribut le nom de l'attribut
   * @param valeurAttribut la valeur de l'attribut
   * @param classeRetour la classe de l'instance de retour
   * @param balise la balise à traiter
   * @param contexte le contexte de la balise
   * @return le résultat de l'évaluation en boolean
   * @throws javax.servlet.jsp.JspException
   */
  public static boolean evaluerBoolean(String nomAttribut,
      String valeurAttribut, Tag balise, PageContext contexte)
          throws JspException {
    Boolean resultat = (Boolean) evaluer(nomAttribut, valeurAttribut,
        Boolean.class, balise, contexte);

    if (resultat != null) {
      return resultat.booleanValue();
    } else {
      return false;
    }
  }

  /**
   * Évaluer une expression régulière qui retourne un integer (int)
   * @param nomAttribut le nom de l'attribut
   * @param valeurAttribut la valeur de l'attribut
   * @param classeRetour la classe de l'instance de retour
   * @param balise la balise à traiter
   * @param contexte le contexte de la balise
   * @return le résultat de l'évaluation en integer (int)
   * @throws javax.servlet.jsp.JspException
   */
  public static int evaluerInt(String nomAttribut, String valeurAttribut,
      Tag balise, PageContext contexte) throws JspException {
    Integer resultat = (Integer) evaluer(nomAttribut, valeurAttribut,
        Integer.class, balise, contexte);

    if (resultat != null) {
      return resultat.intValue();
    } else {
      return -1;
    }
  }

  /**
   * Est-ce qu'il est nécessaire d'évaleur les EL?
   * On valide avec la version du servlet utilisé si supérieure à 2.3, alors JSP 2
   * donc les EL sont évalué automatiquement par le conteneur.
   * @return true s'il est nécessaire d'évaluer les EL.
   * @param contexte le contexte de la page.
   */
  public static boolean isEvaluerEL(PageContext contexte) {
    int servletVersionMajeur = (new Integer(contexte.getServletConfig()
        .getServletContext()
        .getMajorVersion())).intValue();
    int servletVersionMineur = (new Integer(contexte.getServletConfig()
        .getServletContext()
        .getMinorVersion())).intValue();

    // Un paramètre indique que JSP 1.x  doit être utilisé.
    boolean traiterJspVersion1x = GestionParametreSysteme.getInstance()
        .getBoolean(ConstantesParametreSysteme.TRAITER_JSP_VERSION_1X)
        .booleanValue();

    if (!traiterJspVersion1x &&
        ((servletVersionMajeur > 2) ||
            ((servletVersionMajeur == 2) && (servletVersionMineur > 3)))) {
      return false;
    } else {
      return true;
    }
  }
}
