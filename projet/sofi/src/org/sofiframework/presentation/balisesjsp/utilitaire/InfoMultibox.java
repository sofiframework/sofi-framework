/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.utilitaire;

import java.io.Serializable;
import java.util.HashSet;


/**
 * Conteneur pour les informations d'une liste de boîte à cocher.
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.1
 */
public class InfoMultibox implements Serializable {

  private static final long serialVersionUID = 1285003452100129314L;

  // La liste sélectionnée.
  private HashSet listeSelectionnee;

  // La liste sélectionné originalement.
  private HashSet listeSelectionneeOriginal;

  // Le nom d'attribut identifiant le composant.
  private String nomAttribut;

  // La liste des éléments de la liste courante.
  private HashSet listeIdentifiantListeCourante;

  // La liste des éléments nouvellement sélectionné.
  private HashSet listeNouvelleSelection;

  // La liste des éléments déselectionnées.
  private HashSet listeDeselection;

  // Indicateur si le dernier traitement est une nouvelle sélection.
  private boolean dernierTraitementNouvelleSelection;

  // Le dernier identifiant nouvellement sélectionné.
  private String dernierIdentifiantNouvelleSelection;

  // Le dernier identifiant qui a été déselectionné.
  private String dernierIdentifiantDeselection;

  public InfoMultibox() {
    listeIdentifiantListeCourante = new HashSet();
    listeSelectionnee = new HashSet();
    listeNouvelleSelection = new HashSet();
    listeDeselection = new HashSet();
  }

  /**
   *
   * @return
   */
  public HashSet getListeSelectionnee() {
    return listeSelectionnee;
  }

  /**
   *
   * @param listeSelectionnee
   */
  public void setListeSelectionnee(HashSet listeSelectionnee) {
    this.listeSelectionnee = listeSelectionnee;
  }

  /**
   *
   * @return
   */
  public String getNomAttribut() {
    return nomAttribut;
  }

  /**
   *
   * @param nomAttribut
   */
  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  /**
   *
   * @param identifiant
   */
  public void ajouterIdentifiantListeCourante(Object identifiant) {
    this.listeIdentifiantListeCourante.add(identifiant);
  }

  public HashSet getListeIdentifiantListeCourante() {
    return listeIdentifiantListeCourante;
  }

  /**
   *
   * @return
   */
  public String getNomParametreRequete() {
    return this.nomAttribut + "Param";
  }

  /**
   *
   * @return
   */
  public HashSet getListeNouvelleSelection() {
    return listeNouvelleSelection;
  }

  /**
   *
   * @param listeNouvelleSelection
   */
  public void setListeNouvelleSelection(HashSet listeNouvelleSelection) {
    this.listeNouvelleSelection = listeNouvelleSelection;
  }

  /**
   *
   * @param identifiant
   */
  public void ajouterListeNouvelleSelection(Object identifiant) {
    this.listeNouvelleSelection.add(identifiant);
  }

  /**
   *
   * @param identifiant
   */
  public void supprimerListeNouvelleSelection(Object identifiant) {
    this.listeNouvelleSelection.remove(identifiant);
  }

  /**
   * Retourne la liste de déselection.
   * @return la liste de déselection.
   */
  public HashSet getListeDeselection() {
    return listeDeselection;
  }

  /**
   * Fixer la liste de déselection.
   * @param listeDeselection la liste de déselection.
   */
  public void setListeDeselection(HashSet listeDeselection) {
    this.listeDeselection = listeDeselection;
  }

  /**
   * Ajouter un identifiant de la liste de déselection.
   * @param identifiant un identifiant de la liste de déselection.
   */
  public void ajouterListeDeselection(Object identifiant) {
    this.listeDeselection.add(identifiant);
  }

  /**
   * Supprimer un identifiant de la liste de déselection.
   * @param identifiant l'identifiant à supprimer de la liste de déselection.
   */
  public void supprimerListeDeselection(Object identifiant) {
    this.listeDeselection.remove(identifiant);
  }

  /**
   * Retourne true si le dernier traitement est une de
   * nouvelle sélection
   * @return true si le dernier traitement est une de
   * nouvelle sélection
   */
  public boolean isDernierTraitementNouvelleSelection() {
    return dernierTraitementNouvelleSelection;
  }

  /**
   * Fixer true si le dernier traitement est une de
   * nouvelle sélection
   * @param dernierTraitementNouvelleSelection true si le dernier traitement est une de
   * nouvelle sélection
   */
  public void setDernierTraitementNouvelleSelection(
      boolean dernierTraitementNouvelleSelection) {
    this.dernierTraitementNouvelleSelection = dernierTraitementNouvelleSelection;
  }

  /**
   * Retourne le dernier identifiant du goupe de boite à cocher
   * qui a été nouvellement sélectionné.
   * @return le dernier identifiant du goupe de boite à cocher
   * qui a été nouvellement sélectionné.
   */
  public String getDernierIdentifiantNouvelleSelection() {
    return dernierIdentifiantNouvelleSelection;
  }

  /**
   * Fixer le dernier identifiant du goupe de boite à cocher
   * qui a été nouvellement sélectionné.
   * @param dernierIdentifiantNouvelleSelection le dernier identifiant du goupe de boite à cocher
   * qui a été nouvellement sélectionné.
   */
  public void setDernierIdentifiantNouvelleSelection(
      String dernierIdentifiantNouvelleSelection) {
    this.dernierIdentifiantNouvelleSelection = dernierIdentifiantNouvelleSelection;
  }

  /**
   * Retourne le dernier identifiant du goupe de boite à cocher
   * qui a été sélectionné.
   * @return le dernier identifiant du goupe de boite à cocher
   * qui a été sélectionné.
   */
  public String getDernierIdentifiantDeselection() {
    return dernierIdentifiantDeselection;
  }

  /**
   * Fixer le dernier identifiant du goupe de boite à cocher
   * qui a été sélectionné.
   * @param dernierIdentifiantDeselection le dernier identifiant du goupe de boite à cocher
   * qui a été sélectionné.
   */
  public void setDernierIdentifiantDeselection(
      String dernierIdentifiantDeselection) {
    this.dernierIdentifiantDeselection = dernierIdentifiantDeselection;
  }

  /**
   * Retourne la liste sélectionné original.
   * @return la liste sélectionné original.
   */
  public HashSet getListeSelectionneeOriginal() {
    return listeSelectionneeOriginal;
  }

  /**
   * Fixer la liste sélectionné original.
   * @param listeSelectionneeOriginal la liste sélectionné original.
   */
  public void setListeSelectionneeOriginal(HashSet listeSelectionneeOriginal) {
    this.listeSelectionneeOriginal = listeSelectionneeOriginal;
  }

  /**
   * Retourne le nom d'attribut sans son association avec les formulaires
   * imbriqués.
   * @return le nom d'attribut sans son association avec les formulaires
   * imbriqués.
   */
  public String getNomAttributSansFormulaireImbrique() {
    if (getNomAttribut().indexOf(".") != -1) {
      return getNomAttribut().substring(getNomAttribut().lastIndexOf("."));
    } else {
      return null;
    }
  }
}
