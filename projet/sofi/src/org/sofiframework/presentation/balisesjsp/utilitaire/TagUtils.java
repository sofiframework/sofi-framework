package org.sofiframework.presentation.balisesjsp.utilitaire;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Utilitaire sur l'utilisation générique des balises JSP.
 * @author jfbrassard
 * @version 3.1
 */

public class TagUtils {

  /**
   * The Singleton instance.
   */
  private static final TagUtils instance = new TagUtils();

  /**
   * Commons logging instance.
   */
  private static final Log log = LogFactory.getLog(TagUtils.class);

  /**
   * Constructor for TagUtils.
   */
  protected TagUtils() {
    super();
  }

  /**
   * Returns the Singleton instance of TagUtils.
   */
  public static TagUtils getInstance() {
    return instance;
  }

  /**
   * 
   * @param pageContext
   * @param text
   * @throws JspException
   */
  public void write(PageContext pageContext, String text) throws JspException {

    JspWriter writer = pageContext.getOut();

    try {
      writer.print(text);

    } catch (Exception e) {
      log.error(e);
      throw new JspException(e);
    }

  }

  public Locale getLocale(PageContext pageContext) {
    return UtilitaireControleur.getLocale(pageContext.getSession());
  }


  /**
   * 
   * @param request
   * @return
   */
  public String getCertificat(HttpServletRequest request) {
    return UtilitaireControleur.getCertificat(request);
  }

}
