/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.logic.IterateTag;
import org.apache.struts.taglib.nested.NestedNameSupport;
import org.apache.struts.taglib.nested.NestedPropertyHelper;


/**
 * Balise Jsp permettant d'itérer sur des formulaires imbriqués.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.2
 * @see org.sofiframework.presentation.balisesjsp.html.CheckboxTag
 */
public class NestedIterateTag extends IterateTag implements NestedNameSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 8544999553168251735L;

  // le formulaire imbriqué courant.
  private String nesting = null;

  // les propriétés original.
  private String originalName = null;
  private String originalProperty = null;

  // l'environnement original des formulaires imbriqués.
  private String originalNesting = null;
  private String originalNestingName = null;

  /**
   * Ré-écriture du coeur de la méthode IterateTag. Extrait les propriétés relative et laisse
   * le reste de l'implémentation originale.
   * @return int continuation de la directive JSP.
   */
  @Override
  public int doStartTag() throws JspException {
    // les valeurs originals.
    originalName = getName();
    originalProperty = getProperty();

    // fixer l'ID
    if ((id == null) || (id.trim().length() == 0)) {
      id = property;
    }

    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // le formulaire imbriqué original
    originalNesting = NestedPropertyHelper.getCurrentProperty(request);
    originalNestingName = NestedPropertyHelper.getCurrentName(request, this);

    // Fixer le formulaire parent si spécifier.
    if (getName() == null) {
      // vérifier le formulaire imbriqué demandé.
      nesting = NestedPropertyHelper.getAdjustedProperty(request, getProperty());
    } else {
      // traiter seulement la propriété.
      nesting = getProperty();
    }

    // fixer les propriétés.
    NestedPropertyHelper.setNestedProperties(request, this);

    // continuer le traitement original.
    int temp = super.doStartTag();

    // fixer les nouvelles référence.
    NestedPropertyHelper.setName(request, getName());
    NestedPropertyHelper.setProperty(request, deriveNestedProperty());

    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, null);
    pageContext.getRequest().setAttribute(UtilitaireBaliseNested.NOM_FORMULAIRE_IMBRIQUE, nomFormulaireImbrique);

    return temp;
  }

  /**
   * Génération des index des formulaires imbriqués.
   * @return String les index des formulaires imbriqués.
   */
  private String deriveNestedProperty() {
    Object idObj = pageContext.getAttribute(id);

    if (idObj instanceof Map.Entry) {
      return nesting + "(" + ((Map.Entry) idObj).getKey() + ")";
    } else {
      return nesting + "[" + this.getIndex() + "]";
    }
  }

  /**
   * Ré-écriture des références inclus avec les index des formulaires imbriqués.
   * @return int continuation de la directive JSP.
   */
  @Override
  public int doAfterBody() throws JspException {
    // emmagasiner le résultat original.
    int temp = super.doAfterBody();
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    if (temp != SKIP_BODY) {
      // Fixer les nouvelles références.
      NestedPropertyHelper.setProperty(request, deriveNestedProperty());
    }

    //    // Fixer le compteur de ligne pour la liste
    //    int compteur = getIndex();
    //    String nomFormulaireImbrique = getProperty();
    //    pageContext.setAttribute(nomFormulaireImbrique + "noLigne",
    //      new Integer(compteur));
    // retourne le résultat.

    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, null);
    pageContext.getRequest().setAttribute(UtilitaireBaliseNested.NOM_FORMULAIRE_IMBRIQUE, nomFormulaireImbrique);

    return temp;
  }

  /**
   * Compléter la génération de la balise. Toutes les balises de formulaire imbriqués sont rétabli.
   * @return int continuation de la directive JSP.
   * @throws JspException Exception JSP lancé.
   */
  @Override
  public int doEndTag() throws JspException {
    //    UtilitaireBaliseNested.supprimerReferenceFormulaireImbrique(pageContext,
    //      getProperty());
    // Appeler la fin de la balise du parent.
    int i = super.doEndTag();

    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // initialise les valeurs originals.
    super.setName(originalName);
    super.setProperty(originalProperty);

    // initialise les valeurs originals du formulaire imbriqué.
    if (originalNesting == null) {
      NestedPropertyHelper.deleteReference(request);
    } else {
      NestedPropertyHelper.setProperty(request, originalNesting);
      NestedPropertyHelper.setName(request, originalNestingName);
    }

    pageContext.getRequest().removeAttribute(UtilitaireBaliseNested.NOM_FORMULAIRE_IMBRIQUE);

    return i;
  }

  /**
   * Libérer les ressources de la balise et initialiser les valeurs.
   */
  @Override
  public void release() {
    // libere les variables de la classe parent.
    super.release();

    // initialise les valeurs original.
    originalName = null;
    originalProperty = null;
    originalNesting = null;
    originalNestingName = null;
  }
}
