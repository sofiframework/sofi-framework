/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireObjet;


/**
 * Classe utilitaire pour le traitement des formulaires imbriqués (Nested).
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.4
 */
public class UtilitaireBaliseNested {

  public static final String NOM_FORMULAIRE_IMBRIQUE = "nom_formulaire_imbrique";

  /**
   * Retourne le nom du formulaire imbriqué avec l'aide du nom attribut complet.
   * @return le nom du formulaire imbriqué
   * @param nomAttribut le nom attribut complet.
   */
  public static String getNomFormulaireImbrique(String nomAttribut) {
    // Extraire le nom de formulaire imbriqué
    int indiceAttribut = nomAttribut.lastIndexOf(".");

    String nomFormulaireImbrique = null;

    if (indiceAttribut != -1) {
      nomFormulaireImbrique = nomAttribut.substring(0, indiceAttribut);
    }

    return nomFormulaireImbrique;
  }

  /**
   * Obtenir le formulaire en cours (retourne le formulaire imbriqué si il s'agit d'un nested.)
   * @return
   */
  public static BaseForm getFormulaireCourant(PageContext pageContext, String nomPropriete) {
    BaseForm baseForm = null;
    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(pageContext, nomPropriete);
    if (nomFormulaireImbrique == null) {
      baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);
    } else {
      baseForm = (BaseForm) UtilitaireObjet.getValeurAttribut(
          UtilitaireBaliseJSP.getBaseForm(pageContext), nomFormulaireImbrique);
    }
    return baseForm;
  }

  /**
   * Retourne les noms de formulaire imbriqué en traitement.
   * <p>
   * Les noms des formulaires imbriqué sont séparé par une virgule.
   * @return  le nom de la liste en traitement.
   * @param contexte le contexte de la page.
   */
  public static String getReferenceFormulaireImbrique(PageContext contexte,
      String nomAttribut) {
    String nomListe = null;

    if ((nomAttribut == null) || (nomAttribut.indexOf(".") != -1)) {
      nomListe = NestedPropertyHelper.getCurrentProperty((HttpServletRequest) contexte.getRequest());
    }

    return nomListe;
  }

  /**
   * Retourne les noms de formulaire imbriqué en traitement.
   * <p>
   * Les noms des formulaires imbriqué sont séparé par une virgule.
   * @return  le nom de la liste en traitement.
   * @param contexte le contexte de la page.
   */
  public static String getNomAttributSeul(String nomAttributImbrique) {
    String nomAttribut = nomAttributImbrique;

    if (nomAttributImbrique.indexOf(".") != -1) {
      nomAttribut = nomAttributImbrique.substring(nomAttributImbrique.indexOf(
          "."));
    }

    return nomAttribut;
  }

  /**
   * Retourne le nom de formulaire imbriqué en traitement.
   * @return le nom de formulaire imbriqué en traitement.
   * @param contexte le contexte de la page.
   */
  public static String getNomFormulaireImbrique(PageContext contexte,
      String nomAttribut) {
    // Traiter le format du champ de saisie.
    String nomListe = getReferenceFormulaireImbrique(contexte, nomAttribut);

    if ((nomListe != null) && (nomListe.indexOf(",") != -1)) {
      nomListe = nomListe.substring(nomListe.lastIndexOf(",") + 1);
    }

    return nomListe;
  }

  public static void ajouterReferenceFormulaireImbrique(PageContext contexte,
      String nomListe) {
    String nomsListeTraitement = (String) contexte.getAttribute(
        "nomsListeTraitement");

    if (nomsListeTraitement != null) {
      StringBuffer resultat = new StringBuffer();
      resultat.append(nomsListeTraitement);
      resultat.append(",");
      resultat.append(nomListe);
      contexte.setAttribute("nomsListeTraitement", resultat.toString());
    } else {
      contexte.setAttribute("nomsListeTraitement", nomListe);
    }
  }

  public static void supprimerReferenceFormulaireImbrique(
      PageContext contexte, String nomListe) {
    String nomsListeTraitement = (String) contexte.getAttribute(
        "nomsListeTraitement");

    int indiceNomListe = nomsListeTraitement.indexOf(nomListe);

    if (indiceNomListe != 0) {
      nomsListeTraitement = nomsListeTraitement.substring(0, indiceNomListe -
          1);
    } else {
      nomsListeTraitement = null;
    }

    contexte.setAttribute("nomsListeTraitement", nomsListeTraitement);
  }

  /**
   * Retourne l'index, s'il y a lieu, en cours pour l'attribut spécifié.
   * @return l'index, s'il y a lieu, en cours pour l'attribut spécifié.
   * @param contexte le contexte de la page.
   */
  public static String getIndexListeNested(PageContext contexte) {
    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(contexte,
        null);
    String index = null;

    if ((nomFormulaireImbrique != null) &&
        (nomFormulaireImbrique.indexOf("[") != -1)) {
      // Indice du début de l'index.
      int debutIndice = nomFormulaireImbrique.lastIndexOf("[") + 1;

      // Indice de fin de l'index.
      int finIndice = nomFormulaireImbrique.lastIndexOf("]");

      index = nomFormulaireImbrique.substring(debutIndice, finIndice);
    }

    return index;
  }

  /**
   * Retourne l'identifiant unique pour un composant interface.
   * @param nomAttribut l'attribut à fixer qui spécifie l'index en cours.
   * @param contexte le contexte de la page.
   * @return l'identifiant unique d'un élément de formulaire imbriqué.
   */
  public static String getIdentifiantUnique(String nomAttribut,
      PageContext contexte) {
    // Extraire le nom du formulaire imbrique a traiter s'il y a lieu.
    String nomFormulaireImbrique = UtilitaireBaliseNested.getNomFormulaireImbrique(contexte,
        null);

    if (nomAttribut.indexOf(".") != -1) {
      nomFormulaireImbrique = nomAttribut;
    } else {
      if (nomFormulaireImbrique == null) {
        return nomAttribut;
      }

      nomFormulaireImbrique += ".";
      nomFormulaireImbrique += nomAttribut;
    }

    //String identifiantUniqueImbrique = UtilitaireString.remplacerTous(nomFormulaireImbrique,
    //    "[", "");
    //identifiantUniqueImbrique = UtilitaireString.remplacerTous(identifiantUniqueImbrique,
    //    "]", "");

    //    identifiantUniqueImbrique = UtilitaireString.remplacerTous(identifiantUniqueImbrique,
    //        ".", "");
    return nomFormulaireImbrique;
  }
}
