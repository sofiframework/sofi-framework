/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.nested.NestedNameSupport;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.presentation.balisesjsp.html.HiddenTag;


public class NestedHiddenTag extends HiddenTag implements NestedNameSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 5437672572552290543L;
  /* the usual private member variables */
  private String originalName = null;
  private String originalProperty = null;

  public NestedHiddenTag() {
  }

  @Override
  public int doStartTag() throws JspException {
    // get the original properties
    originalName = getName();
    originalProperty = getProperty();

    // request
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // set the properties
    NestedPropertyHelper.setNestedProperties(request, this);

    // let the super do it's thing
    return super.doStartTag();
  }

  @Override
  public int doEndTag() throws JspException {
    // do the super's ending part
    int i = super.doEndTag();

    // reset the properties
    setName(originalName);
    setProperty(originalProperty);

    // continue
    return i;
  }

  /**
   * Release the tag's resources and reset the values.
   */
  @Override
  public void release() {
    super.release();

    // reset the originals
    originalName = null;
    originalProperty = null;
  }
}
