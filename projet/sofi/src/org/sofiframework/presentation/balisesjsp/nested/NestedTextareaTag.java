/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.nested.NestedNameSupport;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.presentation.balisesjsp.html.TextareaTag;


/**
 * Balise Jsp affichant une zone de texte multilignes.
 * <p>
 * Cette balise sert pour les éléments imbriqués à l'intérieur d'un formulaire (nested).
 * Elle hérite de la classe org.sofiframework.presentation.balisesjsp.html.TextareaTag donc
 * elle se comporte sensiblement de la même manière
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.2
 * @see org.sofiframework.presentation.balisesjsp.html.TextareaTag
 */
public class NestedTextareaTag extends TextareaTag implements NestedNameSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -3318142358310334797L;

  /** le nom original de la zone de texte multilignes */
  private String originalName = null;

  /** la propriété du formulaire représentant la zone de texte multilignes */
  private String originalProperty = null;

  /**
   * Début du traitement de la balise Jsp
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    // Obtenir la propriété
    originalName = getName();
    originalProperty = getProperty();

    // Obtenir le request
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // Fixer la propriété
    NestedPropertyHelper.setNestedProperties(request, this);

    return super.doStartTag();
  }

  /**
   * Termine le traitement de la balise Jsp
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    int i = super.doEndTag();

    // Faire un reset des propriétés
    setName(originalName);
    setProperty(originalProperty);

    return i;
  }

  /**
   * Méthode qui sert à libérer les ressources utilisées
   */
  @Override
  public void release() {
    super.release();
    originalName = null;
    originalProperty = null;
  }
}
