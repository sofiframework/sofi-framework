/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.nested.NestedNameSupport;
import org.apache.struts.taglib.nested.NestedPropertyHelper;


/**
 * Balise générant un champ de saisie de texte suivi d'une loupe.
 * <p>
 * Le champ de saisie se comporte comme n'importe quel champ text normal. Cependant
 * lors que l'on click sur l'icône de la loupe qui se situe à droit, cela lance une
 * recherche en passant comme valeur l'attribut inscrit dans le champ texte. Bien
 * sur, la recherche ne se lance pas automatiquement, il faut programmer le controleur
 * qui se chargera de faire la gestion de la recherche.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.2
 */
public class NestedListeValeursTag extends org.sofiframework.presentation.balisesjsp.html.ListeValeursTag
implements NestedNameSupport {

  /**
   * 
   */
  private static final long serialVersionUID = -107212629680736532L;

  /** le nom original du champ de saisie de texte */
  private String originalName = null;

  /** la propriété du formulaire représentant le champ de saisie de texte */
  private String originalProperty = null;

  /** Constructeur par défaut */
  public NestedListeValeursTag() {
  }

  /**
   * Débute le traitement de la balise Jsp.
   * <p>
   * @exception JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    // Obtenir la propriété
    originalName = getName();
    originalProperty = getProperty();

    // Obtenir le request
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    // Fixer la propriété
    NestedPropertyHelper.setNestedProperties(request, this);

    return super.doStartTag();
  }

  /**
   * Termine le traitement de la balise Jsp
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    int i = super.doEndTag();

    // Faire un reset des propriétés
    setName(originalName);
    setProperty(originalProperty);

    return i;
  }

  /**
   * Méthode qui sert à libérer les ressources utilisées
   */
  @Override
  public void release() {
    super.release();
    originalName = null;
    originalProperty = null;
  }
}
