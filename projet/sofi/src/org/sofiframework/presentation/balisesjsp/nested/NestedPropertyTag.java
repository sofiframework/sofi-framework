/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.sofiframework.presentation.struts.form.BaseForm;


/**
 * NestedPropertyTag.
 *
 * The one of only two additions in this nested suite of tags. This is so that
 * you can specify extra levels of nesting in one elegant tag rather than having
 * to propagate and manage an extra dot notated property in nested child tags.
 *
 * It's simply recognised by the helper class and it's property is added to the
 * nesting list.
 *
 * @since Struts 1.1
 * @version $Rev: 54929 $ $Date: 2004-10-16 09:38:42 -0700 (Sat, 16 Oct 2004) $
 */
public class NestedPropertyTag
extends org.apache.struts.taglib.nested.NestedPropertyTag {
  /**
   * 
   */
  private static final long serialVersionUID = -5005064141353986767L;

  /**
   * Overriding method of the heart of the tag. Gets the relative property
   * and tells the JSP engine to evaluate its body content.
   *
   * @return int JSP continuation directive.
   */
  @Override
  public int doStartTag() throws JspException {
    // Extraire le formulaire en traitement.
    BaseForm formulaire = BaseForm.getFormulaire(pageContext.getSession());

    // store original result
    int temp = super.doStartTag();

    String nomFormulaireImbrique = NestedPropertyHelper.getCurrentProperty((HttpServletRequest) pageContext.getRequest());

    pageContext.getRequest().setAttribute(UtilitaireBaliseNested.NOM_FORMULAIRE_IMBRIQUE, nomFormulaireImbrique);

    if (formulaire != null) {
      // Ajouter le nom de la liste de formulaire imbriqué à traiter.
      formulaire.ajouterListeFormulaireImbriqueATraiter(nomFormulaireImbrique);
    }

    return temp;
  }

  /**
   * Evaluate the rest of the page
   *
   * @return int JSP continuation directive.
   */
  @Override
  public int doEndTag() throws JspException {
    // super result
    int temp = super.doEndTag();

    pageContext.getRequest().removeAttribute(UtilitaireBaliseNested.NOM_FORMULAIRE_IMBRIQUE);

    return temp;
  }
}
