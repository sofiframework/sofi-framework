/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.util.ResponseUtils;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Balise JSP générant un champ texte suivi d'un icone calendrier qui appel
 * un calendrier javascript lorsqu'on clic dessus.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.2
 */
public class DateTag extends NestedTextTag {

  private static final long serialVersionUID = -271152250047424430L;

  /** Spécifie si la balise date inclus les heures et minutes */
  protected boolean heure = false;

  /** Spécifie si l'icone calendrier inclus */
  protected String iconeCalendrier = null;

  /** Spécifie si la sélection d'une date se fait en 1 seul clic */
  protected boolean unClic = true;

  /** Constructeur par défaut */
  public DateTag() {
  }

  /**
   * Obtenir la valeur qui spécifie si l'heure doit apparaître dans la balise.
   * <p>
   * @return la valeur qui spécifie si l'heure doit apparaître dans la balise
   */
  public boolean getHeure() {
    return (this.heure);
  }

  /**
   * Fixer la valeur qui spécifie si l'heure doit apparaître dans la balise.
   * <p>
   * @param heure la valeur qui spécifie si l'heure doit apparaître dans la balise
   */
  public void setHeure(boolean heure) {
    this.heure = heure;
  }

  /**
   * Obtenir la valeur qui spécifie si l'icône du calendrier doit être affiché.
   * <p>
   * @return la valeur qui spécifie si l'icône du calendrier doit être affiché
   */
  public String getIconeCalendrier() {
    return (this.iconeCalendrier);
  }

  /**
   * Fixer la valeur qui spécifie si l'icône du calendrier doit être affiché.
   * <p>
   * @param iconeCalendrier la valeur qui spécifie si l'icône du calendrier doit être affiché
   */
  public void setIconeCalendrier(String iconeCalendrier) {
    this.iconeCalendrier = iconeCalendrier;
  }

  /**
   * Est-ce que l'on désire afficher l'icone de calendrier.
   * @return true si on désire afficher l'icone de calendrier.
   */
  public boolean isAfficherIconeCalendrier() {
    return (getIconeCalendrier() != null) &&
        getIconeCalendrier().toString().toLowerCase().equals("true");
  }

  /**
   * Obtenir la valeur qui indique si l'ouverture du calendrier doit se faire suite
   * à un clic de souris ou deux.
   * <p>
   * @return la valeur qui indique si l'ouverture du calendrier doit se faire suite
   * à un clic de souris ou deux
   */
  public boolean getUnClic() {
    return (this.unClic);
  }

  /**
   * Fixer la valeur qui indique si l'ouverture du calendrier doit se faire suite
   * à un clic de souris ou deux.
   * <p>
   * @param unClic la valeur qui indique si l'ouverture du calendrier doit se faire suite
   * à un clic de souris ou deux
   */
  public void setUnClic(boolean unClic) {
    this.unClic = unClic;
  }

  /**
   * Fixer la propriété représentant la date.
   * <p>
   * @param newProperty la propriété représentant la date
   */
  @Override
  public void setProperty(String newProperty) {
    super.setProperty(newProperty);
  }

  /**
   * Execute le début de la balise.
   * <p>
   * @throws JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doStartTag() throws JspException {
    super.doStartTag();

    //doAfterValue(pageContext, getDisabled(), getReadonly());
    return (EVAL_BODY_BUFFERED);
  }

  /**
   * Méthode qui sert à générer le contenu de la balise à afficher.
   * <p>
   * @param resultat le code à inscrire à l'intérieur de la balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  protected void ecrireBalise(StringBuffer resultat) throws JspException {
    // Le nom du formulaire en traitement.
    String nomFormulaire = UtilitaireBaliseJSP.getNomFormulaire(pageContext);
    BaseForm baseForm = UtilitaireBaliseJSP.getBaseForm(pageContext);

    // Préparation de la méthode javascript qui indique si le formulaire
    // a été modifié
    String nomFormulaireJS = UtilitaireBaliseJSP.genererFonctionIndModificationFormulaire(pageContext,
        property);

    String valeurDuChampATraiter = null;
    String valeurDate = "";

    if (value != null) {
      resultat.append(ResponseUtils.filter(value));
    } else {
      try {
        valeurDuChampATraiter = (String) TagUtils.getInstance().lookup(this.pageContext,
            this.name, this.property, null);
      } catch (Exception e) {
        valeurDuChampATraiter = (String) UtilitaireObjet.getValeurAttribut(baseForm,
            getProperty());
      }

      if (!getHeure() && (valeurDuChampATraiter != null) &&
          (valeurDuChampATraiter.length() >= 10)) {
        valeurDate = valeurDuChampATraiter.substring(0, 10);
      } else {
        valeurDate = valeurDuChampATraiter;
      }

      if (valeurDate == null) {
        valeurDate = "";
      }
    }

    if (!UtilitaireString.isVide(nomFormulaireJS) &&
        ((!getDisabled() && !getReadonly() && (iconeCalendrier == null)) ||
            ((iconeCalendrier != null) && iconeCalendrier.equals("true")))) {
      resultat.append("<script type=\"text/javascript\">");
      resultat.append("function ");
      resultat.append(UtilitaireBaliseJSP.getNomFonctionModificationFormulaire(
          nomFormulaireJS, getProperty()));
      resultat.append("(valeur) {");
      resultat.append("if (valeur != '");
      resultat.append(valeurDate);
      resultat.append("') {");
      resultat.append("notifier");
      resultat.append(nomFormulaireJS);
      resultat.append("();}}</script>");
    }

    resultat.append("<input type=\"text");
    resultat.append("\" name=\"");
    resultat.append(property);
    resultat.append("\"");

    if (getAccesskey() != null) {
      resultat.append(" accesskey=\"");
      resultat.append(getAccesskey());
      resultat.append("\"");
    }

    if (accept != null) {
      resultat.append(" accept=\"");
      resultat.append(accept);
      resultat.append("\"");
    }

    if (maxlength != null) {
      resultat.append(" maxlength=\"");
      resultat.append(maxlength);
      resultat.append("\"");
      resultat.append(" size=\"");
      resultat.append(maxlength);
      resultat.append("\"");
    } else { //Spécifier 10 comme défaut
      resultat.append(" maxlength=\"");

      if (getHeure()) {
        resultat.append(16);
      } else {
        resultat.append(10);
      }

      resultat.append("\"");

      resultat.append(" size=\"");

      if (getHeure()) {
        resultat.append(16);
      } else {
        resultat.append(10);
      }

      resultat.append("\"");
    }

    if (this.getTabindex() != null) {
      resultat.append(" tabindex=\"");
      resultat.append(this.getTabindex());
      resultat.append("\"");
    }

    resultat.append(" id=\"");
    resultat.append(getProperty());
    resultat.append("\"");

    if ((!getReadonly() && !getDisabled()) || !isAfficherIconeCalendrier()) {
      UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
          getStyleClassFocus(), getOnfocus(), getOnblur(), resultat);
      resultat = UtilitaireBaliseJSP.specifierGestionModification(pageContext,
          nomFormulaire, nomFormulaireJS, getOnchange(), resultat);
    }

    // Spécifier le mode lectureSeulement
    if (baseForm.isModeLectureSeulement() || this.getDisabled() ||
        this.getReadonly()) {
      UtilitaireBaliseJSP.specifierLectureSeulement(this.getStyleClassLectureSeulement(),
          getStyleClass(), resultat);
    }

    if (!UtilitaireString.isVide(nomFormulaireJS) && !getReadonly() &&
        !getDisabled()) {
      StringBuffer onBlur = new StringBuffer();
      onBlur.append(UtilitaireBaliseJSP.getNomFonctionModificationFormulaire(
          nomFormulaireJS, getProperty()));
      onBlur.append("(this.value)");
      UtilitaireBaliseJSP.specifierStyleFocus(getStyleClass(),
          getStyleClassFocus(), null, onBlur.toString(), resultat);
    }

    resultat.append(" value=\"");

    resultat.append(ResponseUtils.filter(valeurDate));

    resultat.append("\"");
    resultat.append(prepareEventHandlers());
    resultat.append(prepareStyles());
    resultat.append(">");

    org.sofiframework.presentation.balisesjsp.html.DateTag.afficherCalendrier(resultat,
        nomFormulaire, getDisabled(), getReadonly(), this.getAlt(), getProperty(),
        getIconeCalendrier(), getHeure(), pageContext);
  }

  /**
   * Traiter la fin de la balise.
   * <p>
   * L'implémentation par défaut ne fait rien.
   * <p>
   * @throws JspException si une exception JSP est lancé
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   */
  @Override
  public int doEndTag() throws JspException {
    return super.doEndTag();
  }

  /**
   * Méthode qui sert à libérer les ressources utilisées
   */
  @Override
  public void release() {
    super.release();
  }
}
