/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.nested;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;

/**
 * Balise Jsp utilitaire pour les traitements de balises Nested.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.1
 */
public class NestedUtilitaireTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -1626775614992807185L;

  /** Le rôle à valider **/
  private String nomAttribut;

  /**
   * Le nom de variable temporaire qui loge la référence nested unique pour le
   * nom d'attribut spécifié.
   */
  private String var;

  /**
   * Début du traitement de la balise Jsp
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      this.evaluerEL();
    }

    if (nomAttribut != null && getVar() != null) {
      String nomUnique =
          UtilitaireBaliseNested.getIdentifiantUnique(getNomAttribut(),
              pageContext);
      this.pageContext.getRequest().setAttribute(getVar(), nomUnique);
    }
    return super.doStartTag();
  }

  /**
   * Termine le traitement de la balise Jsp
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws JspException si une exception JSP est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    setNomAttribut(null);
    setVar(null);
    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    String nomAttribut =
        EvaluateurExpression.evaluerString("nomAttribut", this.nomAttribut, this,
            pageContext);
    setNomAttribut(nomAttribut);
  }

  public void setNomAttribut(String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  public String getNomAttribut() {
    return nomAttribut;
  }

  public void setVar(String var) {
    this.var = var;
  }

  public String getVar() {
    return var;
  }
}
