/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.securite;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise servant à valider si un utilisateur possède bien
 * le droit d'accès à un composant du référentiel. Il est
 * possible d'y associer une classe qui va permettre d'appliquer de la sécurité
 * applicative personnalisé supplémentaire.
 * <p>
 * Voici un exemple rapide d'utilisation :<br>
 * <code>
 * &nbsp;&nbsp;&lt;sofi-securite:composant identifiant="infra_sofi.libelle.gestion_role.bloc.liste_role"
 *                  var="accessible"
 *                  classeSecuriteApplication="org.sofiframework.infrastructure.presentation.balisejsp.securite.SecuriteRole"/&gt;<br>
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.4
 */
public class ComposantTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -7935250654465172050L;

  protected static Log log = LogFactory.getLog(ComposantTag.class);

  /**
   * Nom de la variable placée dans le PageContext qui détermine si les composant
   * doivent être en mode lecture seulement
   */
  public static final String MODE_LECTURE_SEULEMENT = "modeLectureSeulement";

  /** l'identifiant du bloc de sécurité */
  private String identifiant;

  /**
   * Le nom de variable temporaire qui loge si l'utilisateur a accès ou non au bloc de sécurité.
   * <p>
   * La valeur est un Boolean.
   */
  private String var;

  /**
   * Variable qui loge si l'utilisateur a accès au bloc en lecture seulement ou pas.
   **/
  private String varLectureSeulement;

  /**
   * La classe qui permet de faire appliquer de la sécurité applicative
   * personnalisé sur le composant.
   */
  private String classeSecuriteApplicative;

  /**
   * Le nom de variable temporaire qui loge l'instance du composant.
   */
  private String varComposant;

  /** Constructeur par défaut */
  public ComposantTag() {
  }

  /**
   * Obtenir l'identifiant du bloc de sécurité.
   * <p>
   * @return l'identifiant du bloc de sécurité
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant du bloc de sécurité.
   * <p>
   * @param identifiant l'identifiant du bloc de sécurité
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = identifiant;
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * <p>
   * Vérifie si l'utilisateur à droit au bloc de sécurité et agit en conséquence.
   * <p>
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException Erreur lors du traitement de la balise JSP
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    // Récupération de l'utilisateur présent dans la session
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (utilisateur != null) {
      GestionSecurite gestionSecurite = GestionSecurite.getInstance();

      boolean accesComposant = false;

      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // Vérification si l'utilisateur à droit au composant
        boolean possedeDroitAcces =
            gestionSecurite.isUtilisateurAccesObjetSecurisable(utilisateur,
                identifiant);

        // L'utilisateur possède les droit d'accès
        if (possedeDroitAcces) {

          boolean acces = false;
          boolean lectureSeulement;
          if (UtilitaireBaliseJSP.isAccesComposant(getClasseSecuriteApplicative(),
              pageContext)) {
            accesComposant = true;
            lectureSeulement =
                gestionSecurite.isUtilisateurAccesObjetSecurisableLectureSeulement(utilisateur,
                    this.getIdentifiant());

            if (!lectureSeulement) {
              // Valider la sécurité applicative personnalisé.
              lectureSeulement =
                  UtilitaireBaliseJSP.isAccesComposantLectureSeulement(getClasseSecuriteApplicative(),
                      pageContext);
            }

            if (getVar() != null) {
              this.pageContext.getRequest().setAttribute(getVar(),
                  Boolean.TRUE);
              accesComposant = true;
            }

            // L'utilisateur peut le consulter seulement en lecture seule
            if (lectureSeulement) {
              this.pageContext.getRequest().setAttribute(MODE_LECTURE_SEULEMENT,
                  new Boolean(true));
              accesComposant = true;
            }

            if (getVarLectureSeulement() != null) {
              this.pageContext.getRequest().setAttribute(getVarLectureSeulement(),
                  new Boolean(lectureSeulement));
              accesComposant = lectureSeulement;
            }

            if (accesComposant) {
              ObjetSecurisable composant = (ObjetSecurisable)GestionSecurite.getInstance().getListeObjetSecurisablesParNom().get(getIdentifiant());
              if (getVarComposant() != null) {
                this.pageContext.getRequest().setAttribute(getVarComposant(), composant);
              }
            }

            return EVAL_BODY_INCLUDE;
          } else {
            if (getVar() != null) {
              this.pageContext.getRequest().setAttribute(getVar(),
                  Boolean.FALSE);
            }
          }
        }
      }

      if (getVar() != null) {
        this.pageContext.getRequest().setAttribute(getVar(), Boolean.FALSE);
      }

    }

    return SKIP_BODY;
  }

  /**
   * Complété la fin de la balise.
   * <p>
   * Enleve la variable <code>modeLectureSeulement</code> du contexte de page
   * afin de terminer le traitement comme il faut.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException Exception générique
   */
  @Override
  public int doEndTag() throws JspException {
    this.pageContext.getRequest().removeAttribute(MODE_LECTURE_SEULEMENT);

    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    String identifiant =
        EvaluateurExpression.evaluerString("identifiant", this.identifiant, this,
            pageContext);
    setIdentifiant(identifiant);
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc ou non.
   * @return la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer la variable qui loge  (boolean) si l'utilisateur a accès au bloc ou non.
   * @param var la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Fixer la variable qui loge (boolean) si l'utilisateur a accès au composant en lecture seulement ou pas
   * @param varLectureSeulement la variable qui loge si l'utilisateur a accès au composant en lecture seulement ou pas.
   */
  public void setVarLectureSeulement(String varLectureSeulement) {
    this.varLectureSeulement = varLectureSeulement;
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au composant en lecture seulement ou pas.
   * @return la variable qui loge si l'utilisateur a accès au composant en lecture seulement ou pas.
   */
  public String getVarLectureSeulement() {
    return varLectureSeulement;
  }

  /**
   * Permet de fixer la classe qui permet d'appliquer de la sécurité
   * applicative sur le composant en traitement.
   * @param classeSecuriteApplicative  la classe qui permet d'appliquer de la sécurité
   * applicative sur le composant en traitement.
   */
  public void setClasseSecuriteApplicative(String classeSecuriteApplicative) {
    this.classeSecuriteApplicative = classeSecuriteApplicative;
  }

  /**
   * Retourne la classe qui permet d'appliquer de la sécurité
   * applicative sur le composant en traitement.
   * @return la classe qui permet d'appliquer de la sécurité
   * applicative sur le composant en traitement.
   */
  public String getClasseSecuriteApplicative() {
    return classeSecuriteApplicative;
  }

  public void setVarComposant(String varComposant) {
    this.varComposant = varComposant;
  }

  public String getVarComposant() {
    return varComposant;
  }
}
