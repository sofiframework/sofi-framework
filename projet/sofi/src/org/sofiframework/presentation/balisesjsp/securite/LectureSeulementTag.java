/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.securite;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;

/**
 * Balise servant a traiter du traitement spécifique
 * lorsque la zone en traitment est en lecture seulement.
 * <p>
 * Voici un exemple rapide d'utilisation :<br>
 * <code>
 * &nbsp;&nbsp;&lt;sofi-securite:lectureSeulement
 *                  var="lectureSeulement"
 *                  /&gt;
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.1
 */
public class LectureSeulementTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = -901660320392403935L;

  protected static Log log = LogFactory.getLog(LectureSeulementTag.class);

  /**
   * Le nom de variable temporaire qui loge si l'utilisateur a accès ou non
   * a la zone en cours d'affichage en lecture seulement ou pas.
   * <p>
   * La valeur est un Boolean.
   */
  private String var;

  /** Constructeur par défaut */
  public LectureSeulementTag() {
  }

  /**
   * Traitement effectué à l'ouverture de la balise.
   * <p>
   * Vérifie si l'utilisateur à droit au bloc de sécurité et agit en conséquence.
   * <p>
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException Erreur lors du traitement de la balise JSP
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    if (getVar() != null) {
      this.pageContext.getRequest().setAttribute(getVar(), Boolean.TRUE);
    }

    boolean lectureSeulement =
        UtilitaireBaliseJSP.isFormulaireEnLectureSeulement(pageContext, null);

    // L'utilisateur peut le consulter seulement en lecture seule
    if (lectureSeulement) {
      this.pageContext.getRequest().setAttribute(getVar(), Boolean.TRUE);

      return EVAL_BODY_INCLUDE;
    } else {
      if (getVar() != null) {
        this.pageContext.getRequest().setAttribute(getVar(), Boolean.FALSE);
      }

    }
    return SKIP_BODY;
  }

  /**
   * Complété la fin de la balise.
   * <p>
   * Enleve la variable <code>modeLectureSeulement</code> du contexte de page
   * afin de terminer le traitement comme il faut.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException Exception générique
   */
  @Override
  public int doEndTag() throws JspException {

    return (EVAL_PAGE);
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    String var =
        EvaluateurExpression.evaluerString("var", this.var, this, pageContext);
    setVar(var);
  }

  /**
   * Retourne la variable qui loge (boolean) si l'utilisateur a accès au bloc ou non.
   * @return la variable qui loge si l'utilisateur a accès au bloc ou non.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer la variable qui loge  (boolean) si l'utilisateur a accès en lecture
   * seulement.
   * @param var la variable qui loge si l'utilisateur  a accès en lecture seulement.
   */
  public void setVar(String var) {
    this.var = var;
  }

}
