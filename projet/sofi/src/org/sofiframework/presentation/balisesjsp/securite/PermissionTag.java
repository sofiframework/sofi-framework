/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.securite;

import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Balise servant à valider si un utilisateur possède une permission X.
 * On doit spécifier le nom de la variable qui va loger si l'utilisateur
 * possède la permission à valider. La valeur du var est un Boolean (true/false).
 * <p>
 * Voici un exemple rapide d'utilisation :<br>
 * Disons que la section de page JSP ressemble à ceci<br>
 * <code>
 * &nbsp;&nbsp;&lt;securite permission="DOSSIER_MODIFICATION" var="dossierModifiable" /&gt;<br>
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 3.0
 */
public class PermissionTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 4817621277452448886L;

  /** Le permission a valider **/
  private String permission;

  /**
   * Le nom de variable temporaire qui loge si l'utilisateur possede la permission spécifiee.
   * <p>
   * La valeur est un Boolean.
   */
  private String var;

  /** Constructeur par défaut */
  public PermissionTag() {
  }

  /**
   * Traitement effectue à l'ouverture de la balise.
   * <p>
   * @return Action a poser suivant l'exécution du traitement d'ouverture de la balise.
   * @throws javax.servlet.jsp.JspException Erreur lors du traitement de la balise JSP
   */
  @Override
  public int doStartTag() throws JspException {
    evaluerEL();

    // Recuperation de l'utilisateur present dans la session
    Utilisateur utilisateur =
        UtilitaireControleur.getUtilisateur(this.pageContext.getSession());

    if (utilisateur != null) {

      if (UtilitaireBaliseJSP.isSecuriteActif(pageContext)) {
        // Verification si l'utilisateur possede la permission.
        boolean possedePermission = false;

        if ((getPermission() != null) && (getPermission().indexOf(",") != -1)) {
          StringTokenizer liste = new StringTokenizer(getPermission(), ",");

          while (liste.hasMoreTokens() && !possedePermission) {
            String permission = liste.nextToken();
            possedePermission =
                utilisateur.isPossedePermission(
                    permission);
          }
        } else {
          possedePermission =
              utilisateur.isPossedePermission(
                  getPermission());
        }

        // L'utilisateur possède les droit d'accès à la permission
        if (possedePermission) {
          if (getVar() != null) {
            this.pageContext.getRequest().setAttribute(getVar(), Boolean.TRUE);
          }

          return EVAL_BODY_INCLUDE;
        }
      }
    }

    if (getVar() != null) {
      this.pageContext.getRequest().setAttribute(getVar(), Boolean.FALSE);
    }

    return SKIP_BODY;
  }

  /**
   * Complété la fin de la balise.
   * <p>
   * @return valeur indiquant à l'interpréteur de servlet quoi faire après avoir traité la balise en cours
   * @throws JspException Exception générique
   */
  @Override
  public int doEndTag() throws JspException {
    return (EVAL_PAGE);
  }

  /**
   * Obtenir le rôle à valider.
   * <p>
   * @return le rôle à valider.
   */
  public String getPermission() {
    return permission;
  }

  /**
   * Fixer la permission à valider.
   * <p>
   * @param permission la permission à valider.
   */
  public void setPermission(String permission) {
    this.permission = permission;
  }

  /**
   * Retourne le nom du var qui va connaitre si l'utilisateur
   * possède le role spécifié.
   */
  public String getVar() {
    return var;
  }

  /**
   * Fixer le nom du var qui va connaitre si l'utilisateur
   * possède le role spécifié.
   */
  public void setVar(String var) {
    this.var = var;
  }

  /**
   * Traiter l'expression régulière (EL).
   * @throws javax.servlet.jsp.JspException
   */
  public void evaluerEL() throws JspException {
    String role =
        EvaluateurExpression.evaluerString("permission", this.permission, this, pageContext);

    setPermission(permission);
  }
}
