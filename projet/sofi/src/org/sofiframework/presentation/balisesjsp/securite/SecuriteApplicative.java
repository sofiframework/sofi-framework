/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.securite;

import javax.servlet.jsp.PageContext;

/**
 * Interface qui doit être implémenté afin de spécifier de la sécurité
 * applicative spécifique pour spécifier si l'utilisateur a accès ou encore
 * à un accès réservé à la consultation.
 * @author Jean-Francois Brassard
 * @version SOFI 2.0.4
 */

public interface SecuriteApplicative {

  /**
   * Méthode a implémenter qui permet de spécifier si le composant est disponible
   * selon une sécurité applicative spécifique.
   * @param contexte le contexte de la page.
   * @return true
   */
  public boolean isComposantDisponible(PageContext contexte);

  /**
   * Méthode a implémenter qui permet de spécifier si le composant est réservé
   * à la consultation seulement selon une sécurité applicative spécifique.
   * @param contexte le contexte de la page.
   * @return true
   */
  public boolean isComposantLectureSeulement(PageContext contexte);
}
