/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.tiles;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.taglib.ComponentConstants;
import org.apache.struts.tiles.taglib.GetAttributeTag;


/**
 * Classe de base qui hérite du tag apache pour obtenir une valeur d'attribut
 * de tuile.
 * <p>
 * Cette classe est utilisée comme assise pour définir des classes
 * de paramètres avec gestion de libellés et de messages.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BaseGetAttributeTag extends GetAttributeTag {
  /**
   * 
   */
  private static final long serialVersionUID = -3309995880491232812L;

  /** Constructeur par défaut */
  public BaseGetAttributeTag() {
  }

  /**
   * Traiter la fin de la balise.
   * <p>
   * @return la valeur qui indique à la page quoi faire après avoir traité cette balise
   * @throws javax.servlet.jsp.JspException si une exception JSP est lancé
   */
  @Override
  public int doEndTag() throws JspException {
    // Vérifier le role
    if ((this.getRole() != null) &&
        !((HttpServletRequest) pageContext.getRequest()).isUserInRole(
            this.getRole())) {
      return EVAL_PAGE;
    }

    // Obtenir le contexte
    ComponentContext compContext = (ComponentContext) pageContext.getAttribute(ComponentConstants.COMPONENT_CONTEXT,
        PageContext.REQUEST_SCOPE);

    if (compContext == null) {
      throw new JspException(
          "Error - tag.getAsString : component context is not defined. Check tag syntax");
    }

    Object value = compContext.getAttribute(this.getAttribute());

    if (value == null) {
      if (this.getIgnore() == false) {
        throw new JspException("Error - tag.getAsString : attribute '" +
            this.getAttribute() + "' not found in context. Check tag syntax");
      } else {
        return EVAL_PAGE;
      }
    } else {
      // Permettre de modifier la valeur avant de l'écrire dans la page
      value = this.getAffichageValeur(value);
    }

    try {
      pageContext.getOut().print(value);
    } catch (IOException ex) {
      ex.printStackTrace();
      throw new JspException("Error - tag.getProperty : IOException ");
    }

    return EVAL_PAGE;
  }

  /**
   * Permet de modifier la valeur de l'attribut avant de l'écrire
   * <p>
   * @param valeur la clé de la valeur à aller chercher
   * @return la nouvelle valeur qui correspond à la description de la clé
   */
  protected abstract Object getAffichageValeur(Object valeur);
}
