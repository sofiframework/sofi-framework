/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.balisesjsp.tiles;

import org.sofiframework.application.message.UtilitaireLibelle;


/**
 * Balise d présentation d'un libellé.
 * <p>
 * la propriété name représente un vaiable configurable d'un gabarit tile.
 * La valeur de la variable est l'identifiant de la classe de messagerie
 * libellé configurée dans l'application.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class GetLibelleTag extends BaseGetAttributeTag {
  /**
   * 
   */
  private static final long serialVersionUID = -5579736104273003871L;

  /** Constructeur par défaut */
  public GetLibelleTag() {
  }

  /**
   * Obtenir la description du libellé à parir de sa clé.
   * <p>
   * @param valeur la clé de la valeur à aller chercher
   * @return la nouvelle valeur qui correspond à la description de la clé
   */
  @Override
  protected Object getAffichageValeur(Object valeur) {
    String cle = (valeur instanceof String) ? (String) valeur : valeur.toString();

    return UtilitaireLibelle.getLibelle(cle, pageContext).getMessage();
  }
}
