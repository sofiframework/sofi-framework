/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet qui permet de faire proxy d'un domaine avec un URL local. Un appel a
 * un répertoire sera intercepté par le servlet. La requête sera recomposé avec
 * le nom de domaine et effectuée par http client. La réponse est réécrite dans
 * la réponse du servlet. Le type de contenu sera aussi reconduit. Si le
 * paramètre forcer type contenu est rensigné alors le type de contenu sera
 * toujours renseigné à la valeur spécifiée.
 * 
 * Ce servelt permet de facilement cacher un serveur de contenu statique ou
 * dynamique. Le serveur lui même peut être caché et seulement répondre aux
 * requêtes venant du serveur applicatif. Il est possible de définir plusieurs
 * fois le servlet pour faire un proxy de plusieurs domaines.
 * 
 * Exemple :
 * 
 * <servlet> <servlet-name>javadocProxyServlet</servlet-name>
 * <servlet-class>org.
 * sofiframework.presentation.DomainProxyServlet</servlet-class> <init-param>
 * <param-name>domain</param-name>
 * <param-value>https://sofiframework.jira.com/svn
 * /FRAMEWORK/trunk/doc/api/</param-value> </init-param> <init-param>
 * <param-name>forceContentType</param-name>
 * <param-value>text/html;charset=UTF-8</param-value> </init-param> </servlet>
 * <servlet-mapping> <servlet-name>javadocProxyServlet</servlet-name>
 * <url-pattern>/javadoc/*</url-pattern> </servlet-mapping>
 * 
 * URL original : https://sofiframework.jira.com/svn/FRAMEWORK/trunk/doc/api/
 * 
 * Sous cet url se trouve la documentation de SOFI. L'on désire pouvoir accéder
 * aux page de la documentation en tant que document HTML non pas text (SVN
 * envoi le type de contenu text).
 * 
 * domain : sera donc
 * https://sofiframework.jira.com/svn/FRAMEWORK/trunk/doc/api/
 * 
 * forcer type contenu : text/html;charset=UTF-8
 * 
 * Permet de changer le type de contenu des documents récupérés.
 * 
 * Le mapping du servlet serait /javadoc/*
 * 
 * Par exemple lors de l'appel de l'url javadoc/index.html, c'est le servlet qui
 * est appelé. Un appel HTTP client est ensuite fait à l'adresse
 * https://sofiframework.jira.com/svn/FRAMEWORK/trunk/doc/api/index.html Comme
 * les URLs sont relatifs dans tous les documents de Javadoc alors les appels
 * sont toujours fait à javadoc/
 * 
 * Cette stratégie peut seulement fonctionner si les urls sont relatifs. Les
 * fonctionnalités du servlet pourraient être étandu pour que les URLs absolu
 * qui référence au même domaine soient remplacés par les URLs relatifs.
 * 
 * @author Jean-Maxime Pelletier
 */
public class DomainProxyServlet extends BaseProxyServlet {

  private static final long serialVersionUID = -2538961030545072766L;

  /**
   * Domaine qui doit être en proxy par le servlet.
   */
  private String domain = null;

  /**
   * Est-ce que l'on désire forcer un type de contenu. Par exemple si l'on
   * obtient une ressource html de SVN, elle sera avec le type de contenu text.
   * Si l'on veut présenter le contenu en html on doit pouvoir forcer le type de
   * contenu à html.
   */
  private String forceContentType = null;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    this.domain = config.getInitParameter("domain");
    this.forceContentType = config.getInitParameter("forceContentType");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    // Path exemple /index.html
    String path = req.getPathInfo();

    /* Enlever le premier slash. Le domaine doit être saisi avec un slash à la
     * fin.
     */
    String url = this.domain + path.substring(1);

    //Ecrire la reponse de l'appel de l'URL dans la réponse du servlet.
    this.ecrireContenuUrlDansReponse(url, this.forceContentType, resp);
  }
}