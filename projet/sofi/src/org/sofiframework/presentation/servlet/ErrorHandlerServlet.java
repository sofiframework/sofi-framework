package org.sofiframework.presentation.servlet;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireException;

/**
 * Servlet de base qui permet de faire la gestion des erreurs interne du serveur
 * d'un site.
 * 
 * @author jf.brassard
 * @since 3.2
 *
 */

public abstract class ErrorHandlerServlet extends HttpServlet {

  private static final long serialVersionUID = -1031072798939903860L;

  private static Log log = LogFactory.getLog(ErrorHandlerServlet.class);

  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    Exception exception = (Exception) request
        .getAttribute("javax.servlet.error.exception");

    Integer statusCode = getStatusCode(request);

    if (statusCode != null && statusCode != 200) {

      try {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        UtilitaireVelocity utilitaireVelocity = new UtilitaireVelocity();
        UtilitaireVelocity.initialiser();

        String locale = UtilitaireControleur.getLocale(request.getSession())
            .toString();

        Message message = GestionLibelle.getInstance().get(
            getErrorPageLabelKey(), locale, null, getCodeClient(request));

        Message title = GestionLibelle.getInstance().get(
            getErrorTitleLabelKey(), locale, new String[]{statusCode.toString()}, getCodeClient(request));

        utilitaireVelocity.ajouterAuContexte("title", title.getMessage());
        utilitaireVelocity.ajouterAuContexte("body", message.getMessage());

        String exceptionStr = null;

        if (exception != null) {
          exceptionStr = UtilitaireException
              .getMessageExceptionAuComplet(exception);

          utilitaireVelocity.ajouterAuContexte("exception", exceptionStr);

        }

        utilitaireVelocity.ajouterAuContexte("errorCode", statusCode);

        String content = utilitaireVelocity.genererHTML(getLocationTemplate());

        writeEvent(exceptionStr, request, response);

        out.println(content);

      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error(e);
        }
      }
    }

  }

  public String getRequestUri(HttpServletRequest request) {
    String requestUri = (String) request
        .getAttribute("javax.servlet.error.request_uri");

    if (requestUri == null) {
      requestUri = "N/A";
    }

    return requestUri;
  }

  public Integer getStatusCode(HttpServletRequest request) {
    Integer statusCode = (Integer) request
        .getAttribute("javax.servlet.error.status_code");
    return statusCode;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }

  abstract public String getLocationTemplate();

  abstract public void writeEvent(String exceptionDesc,
      HttpServletRequest request, HttpServletResponse response);

  abstract public String getErrorPageLabelKey();

  abstract public String getErrorTitleLabelKey();
  
  abstract public String getCodeClient(HttpServletRequest request);
}
