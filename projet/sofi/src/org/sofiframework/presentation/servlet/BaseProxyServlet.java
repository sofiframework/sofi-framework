/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 * Permet d'écrire le contenu de la réponse d'un URL dans la réponse du servlet.
 * Sert essentiellement de proxy pour une autre ressource web.
 * 
 * @author Jean-Maxime Pelletier
 */
public class BaseProxyServlet extends HttpServlet {

  private static final long serialVersionUID = -5166504011378226767L;

  /**
   * Ecrire la réponse du get de l'URL dans la réponse du servlet.
   * @param url URL qui doit être appelé par HTTP Client
   * @param forceContentType Force le type contenu
   * @param resp Réponse du servlet
   * @throws IOException Exception d'entrée sortie
   * @throws HttpException Exception HTTP
   */
  protected void ecrireContenuUrlDansReponse(String url,
      String forceContentType, HttpServletResponse resp) throws IOException,
      HttpException {
    /*
     * Obtenir la réponse avec un get http client.
     */
    HttpClient client = new HttpClient();
    GetMethod get = new GetMethod(url);
    client.executeMethod(get);

    /*
     * Reconduire le content type dans la réponse.
     */
    Header[] contentType = get.getResponseHeaders("Content-Type");

    /*
     * Ecrire la reponse de l'url dans la reponse actuelle, comme ca le call
     * ajax obtien la réponse de l'autre domaine.
     */
    String reponse = get.getResponseBodyAsString();
    if (contentType != null && contentType.length > 0) {
      resp.setContentType(contentType[0].getValue());
    }

    /*
     * Changer le content type si c'est nécessaire.
     */
    if (forceContentType != null) {
      resp.setContentType(forceContentType);
    }

    resp.getWriter().write(reponse);
    resp.flushBuffer();
  }
}
