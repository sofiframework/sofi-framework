/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Proxy Servlet qui permet de faire des requêtes ajax sur un autre domaine. Est
 * utile pour effectuer des requêtes ajax sur un autre domaine.
 * 
 * Par exemple :
 * 
 * Le mapping du servlet pourrait être ajax.get
 * 
 * Ainsi avec le javascript suivant :
 * 
 * var url = 'http://sofiframework.jira.com/plugins/servlet/streams';
 * 
 * $.ajax({
 *  type: "GET",
 *  url: "ajax.get?url=" + url,
 *  dataType: "xml",
 *  success: function(xml) {}
 *  });
 * 
 * Donc avec l'appel suivant on peut obtenir le contenu
 * d'une page d'un autre domaine. Les appels ajax qui sont
 * directement vers un autre domaine sont bloqué pour des
 * fins de sécurité.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UrlProxyServlet extends BaseProxyServlet {

  private static final long serialVersionUID = -2538961030545072766L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String url = req.getParameter("url");
    String forceContentType = req.getParameter("forceContentType");

    this.ecrireContenuUrlDansReponse(url, forceContentType, resp);
  }


}