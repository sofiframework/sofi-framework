/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ecouteur;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;


/**
 * Classe permettant d'écouter les évènement de création et de suppresion de
 * session utilisateur.
 * <p>
 * Pour le configurer ajouter ce bout de code dans le fichier web.xml:
 * &nbsp;&lt;listener&gt;
 * &nbsp;&nbsp;&nbsp;&lt;listener-class&gt;
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;org.sofiframework.struts.controleur.SessionEcouteur
 * &nbsp;&nbsp;&nbsp;&lt;/listener-class&gt;
 * &nbsp;&lt;/listener&gt;
 * @author Jean-François Brassard  (Nurun inc.)
 * @version SOFI 1.5
 */
public class SessionEcouteur extends CompteurSessionListener {

  /**
   * Évènement lors de la suppression de la session de l'utilisateur.
   * @param hse
   */
  @Override
  public void sessionDestroyed(HttpSessionEvent hse) {
    HttpSession session = hse.getSession();

    if (UtilitaireControleur.isJournalisationActive()) {
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(session);

      ObjetSecurisable dernierService = UtilitaireSession.getServiceAJournaliser(session);

      // Appel de la journaliser de la fin d'utilisateur du service.
      journaliserFinService(dernierService, utilisateur, session);
    }

    super.sessionDestroyed(hse);
  }

  /**
   * Appel de la journalisation afin de signifier la fin d'utilisation d'un service.
   * <p>
   * Nécessite une ré-écriture.
   * @param service le service en cours.
   * @param utilisateur l'utilisateur courant.
   * @param session la session en traitement.
   */
  protected void journaliserFinService(ObjetSecurisable service,
      Utilisateur utilisateur, HttpSession session) {
  }
}
