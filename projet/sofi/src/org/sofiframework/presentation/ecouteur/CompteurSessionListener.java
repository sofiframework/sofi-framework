/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ecouteur;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;


/**
 * Classe permettant d'écouter lorsqu'il y a une session créer et de detruite.
 * Permet de compter le nombre d'usager présentement en ligne ainsi que le nombre
 * total depuis de lancement de l'application.
 * <p>
 * Pour le configurer ajouter ce bout de code dans le fichier web.xml:
 * &nbsp;&lt;listener&gt;
 * &nbsp;&nbsp;&nbsp;&lt;listener-class&gt;
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;org.sofiframework.struts.controleur.CompteurSessionListener
 * &nbsp;&nbsp;&nbsp;&lt;/listener-class&gt;
 * &nbsp;&lt;/listener&gt;
 * @author Jean-François Brassard  (Nurun inc.)
 * @version SOFI 1.0
 */
public class CompteurSessionListener implements ServletContextListener,
HttpSessionListener {
  /** Variable désignant le log à utiliser pour cette classe */
  private Log log = LogFactory.getLog(org.sofiframework.presentation.ecouteur.CompteurSessionListener.class.getName());

  /** le contexte du servlet */
  ServletContext servletContext;

  /**
   * Méthode qui sert à comptabiliser le nombre de sessions utilisateur.
   * <p>
   * Cette méthode est executée lors qu'un contexte est initialisé, donc à chaque page consultée
   * <p>
   * @param sce l'évènement exécuté sur un ServletContect
   */
  @Override
  public void contextInitialized(ServletContextEvent sce) {
    if (GestionSurveillance.getInstance().isActif()) {
      org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
      .setDateDepartApplication();
    }
  }

  /**
   * Méthode qui ne sert à rien.
   */
  @Override
  public void contextDestroyed(ServletContextEvent sce) {
  }

  /**
   * Méthode qui sert à comptabiliser le nombre de sessions utilisateur.
   * <p>
   * Cette méthode est executée lors qu'une session utilisateur est initialisée
   * <p>
   * @param hse l'évènement exécuté sur un la session utilisateur
   */
  @Override
  public void sessionCreated(HttpSessionEvent hse) {
    //    if (GestionSurveillance.getInstance().isActif()) {
    //      HttpSession session = hse.getSession();
    //      session.setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION, Boolean.TRUE);
    //      org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
    //                                                                 .incrementerCompteurUsager();
    //
    //      String nbUtilisateur = "Nombre d'utilisateur en ligne:" +
    //        GestionSurveillance.getInstance().getNbUtilisateurEnLigne();
    //      log.info(nbUtilisateur);
    //    }
  }

  /**
   * Méthode qui sert à comptabiliser le nombre de sessions utilisateur.
   * <p>
   * Cette méthode est executée lors qu'une session utilisateur est détruite.
   * <p>
   * @param hse l'évènement exécuté sur un la session utilisateur
   */
  @Override
  public void sessionDestroyed(HttpSessionEvent hse) {
    if (GestionSurveillance.getInstance().isActif()) {
      try {
        Object utilisateur = UtilitaireControleur.getUtilisateur(hse.getSession());

        if (utilisateur != null) {
          GestionSurveillance.getInstance()
          .decrementerCompteurUsagerAuthentifier();

          String attributCle = GestionSurveillance.getInstance()
              .getParamCleUtilisateur();

          Object valeurCle = org.sofiframework.utilitaire.UtilitaireObjet.getValeurAttribut(utilisateur,
              attributCle);

          if (log.isInfoEnabled()) {
            log.info("FIN de la session de l'utilisateur: " + valeurCle);
          }

          org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
          .supprimerUtilisateurDeLaListe(valeurCle);
          org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
          .decrementerCompteurUsager();
        }
      } catch (Exception e) {
        // Rien faire.
      }
    }
  }
}
