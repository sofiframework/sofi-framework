/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.upload.MultipartRequestWrapper;
import org.apache.struts.util.RequestUtils;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.parametresysteme.UtilitaireParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Action;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Onglet;
import org.sofiframework.application.securite.objetstransfert.Section;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.AttributTri;
import org.sofiframework.composantweb.menu.ComposantMenu;
import org.sofiframework.composantweb.menu.ComposantMenuModifiable;
import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireNavigateur;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.fichier.FichierTexte;
import org.sofiframework.utilitaire.fichier.UtilitaireFichier;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Classe offrant plusieurs méthodes qui sont utilisé par BaseRequestProcessor et/ou BaseTilesRequestProcessor.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.controleur.BaseRequestProcessor
 * @see org.sofiframework.presentation.struts.controleur.BaseTilesRequestProcessor
 */
public class RequestProcessorHelper {
  /** Variable désignant l'instance commune de journalisation */
  private static final Log log = LogFactory.getLog(RequestProcessorHelper.class);

  /**
   * Permet de spécifier dans le request si le navigateur est de type Mozilla (Moteur Gecko) ou encore Microsoft
   * Internet Explorer.
   * <p>
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  public static void specfifierTypeNavigateur(HttpServletRequest request) {
    if (request.getSession().getAttribute(Constantes.TYPE_NAVIGATEUR) == null) {
      int typeNavigateurUtilise = 0;

      if (UtilitaireNavigateur.isInternetExplorer_55EtPlus(request)) {
        typeNavigateurUtilise = Constantes.IE_55;
      } else {
        if (UtilitaireNavigateur.isMozilla(request)) {
          typeNavigateurUtilise = Constantes.MOZILLA;
        }
      }

      request.getSession().setAttribute(Constantes.TYPE_NAVIGATEUR, new Integer(typeNavigateurUtilise));
      request.getSession().setAttribute(Constantes.DESC_TYPE_NAVIGATEUR,
          UtilitaireNavigateur.getDescriptionTypeNavigateur(request));
    }
  }

  /**
   * Méthode permettant d'appliquer une nouvelle authentification en générant le menu personnalisé de l'utilisateur.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param mapping
   *          ActionMapping utitlisé pour sélectionner cette instance.
   * @param servletContext
   *          le servlet de contecte à utiliser
   */
  public static void appliquerNouvelleAuthentification(HttpServletRequest request, ActionMapping mapping,
      ServletContext servletContext) {
    // Extraire l'utilisateur en session.
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    if (UtilitaireControleur.isNouvelleAuthentification(request)) {
      RepertoireMenu.creerRepertoireMenu(utilisateur, request);
    }
  }

  /**
   * Méthode permettant de spécifier lequel des onglets du service sélectionné peut être offert à l'utilisateur.
   * <p>
   * 
   * @param conserverServiceInitial
   *          indique si le traitement qui spécifie le détail pour un service doit conserver les paramètre du service
   *          initialement appelé.
   * @param request
   *          la requête HTTP en traitement.
   * @param mapping
   *          ActionMapping utitlisé pour sélectionner cette instance.
   */
  public static void specifierDetailPourService(boolean conserverServiceInitial, HttpServletRequest request,
      ActionMapping mapping) {
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    if ((utilisateur != null) && (utilisateur.getListeObjetSecurisablesParAdresse() != null)) {
      RepertoireMenu repertoireMenu = (RepertoireMenu) request.getSession().getAttribute(
          RepertoireMenu.CLE_REPERTOIRE_MENU);

      if (repertoireMenu != null) {
        if (!conserverServiceInitial) {
          initialiserSelectionMenu(repertoireMenu, false, request);
        }

        request.getSession().setAttribute(Constantes.ONGLET_SELECTIONNEE + "2", null);
      }

      // Appliquer seulement si un repertoire de menu est spécifié
      if (repertoireMenu != null) {
        HashMap parametres = new HashMap();

        BaseForm formulaire = null;

        if (mapping.getName() != null) {
          formulaire = (BaseForm) request.getSession().getAttribute(mapping.getName());
        }

        // Retenir les paramètres de l'adresse sélectionné.
        try {
          Map hmParametres = request.getParameterMap();

          if (hmParametres != null) {
            Iterator listeParametres = hmParametres.keySet().iterator();
            Object valeur = null;

            while (listeParametres.hasNext()) {
              String param = (String) listeParametres.next();
              valeur = hmParametres.get(param);

              if ((formulaire != null) && !param.equals("methode") && !param.equals("ajax")
                  && !param.equals("impression")) {
                Object valeurFormulaire = UtilitaireObjet.getValeurAttribut(formulaire, param);

                if (valeurFormulaire == null) {
                  parametres.put(param, valeur);
                }
              } else {
                if ((valeur != null) && (valeur.toString().indexOf("'") == -1)) {
                  String nomCookieAuthentification = GestionSecurite.getInstance().getNomCookieCertificat();

                  if (!valeur.equals(Constantes.NOUVELLE_AUTHENTIFICATION)) {
                    if ((nomCookieAuthentification == null) && !param.equals("ajax") && !param.equals("impression")) {
                      parametres.put(param, valeur);
                    } else {
                      if ((nomCookieAuthentification != null) && !nomCookieAuthentification.equals(valeur)
                          && !param.equals("ajax") && !param.equals("impression")) {
                        parametres.put(param, valeur);
                      }
                    }
                  }
                }
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        String urlAvecMethode = getUrlCourant(request, mapping);

        ObjetSecurisable serviceSelectionne = null;

        ObjetSecurisable serviceSelectionneOriginal = GestionSecurite.getInstance().getObjetSecurisableParAdresse(
            urlAvecMethode);

        if (serviceSelectionneOriginal == null && request.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE) != null) {
          String actionCourante = request.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE).toString();
          serviceSelectionne = GestionSecurite.getInstance().getObjetSecurisableParAdresse(actionCourante);
        } else {
          serviceSelectionne = serviceSelectionneOriginal;
        }

        if (serviceSelectionne != null) {
          if ((GestionSecurite.getInstance().getPageAccueilDefaut() != null)
              && (serviceSelectionne.getSeqObjetSecurisable().intValue() == GestionSecurite.getInstance()
              .getPageAccueilDefaut().intValue())) {
            initialiserSelectionMenu(repertoireMenu, false, request);
          }

          request.getSession().setAttribute(Constantes.SERVICE_SELECTIONNEE, serviceSelectionne);

          // Initialiser l'onglet sélectionné car c'est un service de
          // sélectionné
          if (serviceSelectionne instanceof Service) {
            request.getSession().setAttribute(Constantes.ONGLET_SELECTIONNEE, null);
          }

          // Si le service sélectionner est un onglet alors le mémoriser.
          if (serviceSelectionne instanceof Onglet) {
            Onglet onglet = (Onglet) serviceSelectionne;

            if ((onglet.getNiveauOnglet() != null) && (onglet.getNiveauOnglet().intValue() > 1)) {
              // Spécifier le niveau d'onglet selectionné.
              request.setAttribute("selectionOngletNiveau", onglet.getNiveauOnglet());

              while ((onglet.getNiveauOnglet() != null) && (onglet.getNiveauOnglet().intValue() > 1)) {
                request.getSession().setAttribute(Constantes.ONGLET_SELECTIONNEE + onglet.getNiveauOnglet().toString(),
                    serviceSelectionne);

                onglet = (Onglet) onglet.getObjetSecurisableParent();
              }

              serviceSelectionne = onglet;
            }

            request.getSession().setAttribute(Constantes.ONGLET_SELECTIONNEE, serviceSelectionne);
          }
        } else {
          serviceSelectionne = (ObjetSecurisable) request.getSession().getAttribute(Constantes.SERVICE_SELECTIONNEE);
        }

        boolean traiterNavigationCompleteDansFilAriane = GestionParametreSysteme.getInstance()
            .getBoolean("traiterNavigationCompleteDansFilAriane").booleanValue();

        ObjetSecurisable objetSecurisableSelectionne = null;

        if (traiterNavigationCompleteDansFilAriane) {
          objetSecurisableSelectionne = serviceSelectionneOriginal;
        } else {
          objetSecurisableSelectionne = serviceSelectionne;
        }

        if (!urlAvecMethode.equals("")) {
          // Si l'url contient des paramètres, conserver dans la session
          if (parametres.size() > 0) {
            HashMap parametresUrlOnglet = (HashMap) request.getSession().getAttribute("parametresUrlOnglet");

            if (parametresUrlOnglet == null) {
              parametresUrlOnglet = new HashMap();
            }

            parametresUrlOnglet.remove(urlAvecMethode);
            parametresUrlOnglet.put(urlAvecMethode, parametres);
            request.getSession().setAttribute("parametresUrlOnglet", parametresUrlOnglet);
          }
        }

        // Initialiser l'indicateur du type d'objet sélectionné est un service.
        request.getSession().removeAttribute(Constantes.TYPE_SERVICE_SELECTIONNE);

        if ((objetSecurisableSelectionne == null)
            && (UtilitaireSession.getServiceCourant(request.getSession()) != null)) {
          objetSecurisableSelectionne = UtilitaireSession.getServiceCourant(request.getSession());
        } else {
          // Spécifier si l'objet du référentiel sélectionné est de type
          // service.
          if ((objetSecurisableSelectionne != null)
              && (objetSecurisableSelectionne.getObjetSecurisableParent() != null)
              && Service.class.isInstance(objetSecurisableSelectionne.getObjetSecurisableParent())) {
            // Extraire le service parent.
            ObjetSecurisable serviceParent = objetSecurisableSelectionne.getObjetSecurisableParent();
            String adresseWebParent = serviceParent.getAdresseWeb();

            if (adresseWebParent != null && adresseWebParent.indexOf(urlAvecMethode) != -1) {
              request.getSession().setAttribute(Constantes.TYPE_SERVICE_SELECTIONNE, Boolean.TRUE);
            }
          }
        }

        // Valider si l'onglet sélecionné a un onglet enfant d'un niveau
        // supérieur
        if (objetSecurisableSelectionne instanceof Onglet
            && (objetSecurisableSelectionne.getListeObjetSecurisableEnfants().size() > 1)
            && objetSecurisableSelectionne.getListeObjetSecurisableEnfants().get(0) instanceof Onglet) {
          Onglet ongletEnfant = (Onglet) objetSecurisableSelectionne.getListeObjetSecurisableEnfants().get(0);
          Onglet ongletSelectionnne = (Onglet) objetSecurisableSelectionne;
          Integer niveauOngletSelectionne = Integer.valueOf(1);

          if (ongletSelectionnne.getNiveauOnglet() != null) {
            niveauOngletSelectionne = ongletSelectionnne.getNiveauOnglet();
          }

          if ((ongletEnfant.getNiveauOnglet() != null)
              && (ongletEnfant.getNiveauOnglet().intValue() > niveauOngletSelectionne.intValue())) {
            urlAvecMethode = ongletEnfant.getAdresseWeb();
          }
        }

        // Construire la liste d'onglets s'il y a lieu
        construireListeOnglet(objetSecurisableSelectionne, urlAvecMethode, 1, request);

        if (objetSecurisableSelectionne != null) {
          request.getSession().setAttribute(Constantes.SERVICE_SELECTIONNEE, objetSecurisableSelectionne);

          boolean parentNull = false;

          while (!parentNull && !objetSecurisableSelectionne.getClass().isAssignableFrom(Service.class)
              && !objetSecurisableSelectionne.getClass().isAssignableFrom(Onglet.class)) {
            parentNull = objetSecurisableSelectionne.getObjetSecurisableParent() == null;

            if (!parentNull) {
              objetSecurisableSelectionne = objetSecurisableSelectionne.getObjetSecurisableParent();
            }
          }

          ComposantMenu composantMenuSelectionne = repertoireMenu.getComposantMenuDeTouslesMenus("menu"
              + objetSecurisableSelectionne.getSeqObjetSecurisable().toString());

          if (!conserverServiceInitial) {
            ArrayList filNavigation = new ArrayList();

            if ((composantMenuSelectionne != null) && (repertoireMenu.getComposantMenuAccueil() != null)) {
              repertoireMenu.getComposantMenuAccueil().setEnfantsPourFilNavigation(true);

              // Ajouter le composant de menu de la page d'acceuil.
              if (!GestionSecurite.getInstance().isPageAccueilInclusDansMenu()) {
                filNavigation.add(repertoireMenu.getComposantMenuAccueil());
              }

              boolean exclureFacetteDansFilAriane = GestionParametreSysteme.getInstance()
                  .getBoolean("exclureFacetteDansFilAriane").booleanValue();

              if (GestionSecurite.getInstance().getCodeFacette() != null && !exclureFacetteDansFilAriane) {

                String nomFacette = (String) request
                    .getSession()
                    .getServletContext()
                    .getAttribute(
                        "facetteApplication_" + UtilitaireControleur.getLocale(request.getSession()).toString());
                if (nomFacette == null) {
                  DomaineValeur domaineValeur = GestionDomaineValeur.getInstance().getDomaineValeur(
                      "LISTE_FACETTE_APPLICATION", GestionSecurite.getInstance().getCodeFacette(),
                      UtilitaireControleur.getLocale(request.getSession()).toString());

                  if (domaineValeur != null) {
                    nomFacette = domaineValeur.getDescription(UtilitaireControleur.getLocale(request.getSession())
                        .toString());
                    request
                    .getSession()
                    .getServletContext()
                    .setAttribute(
                        "facetteApplication_" + UtilitaireControleur.getLocale(request.getSession()).toString(),
                        nomFacette);
                  }
                }

                request.getSession().setAttribute("facetteApplication", nomFacette);

                filNavigation.add(repertoireMenu.getComposantMenuFacette(nomFacette));
              }

              composantMenuSelectionne.setEnfantsPourFilNavigation(false);

              if (composantMenuSelectionne != null) {
                genererFilNavigation(request, composantMenuSelectionne, filNavigation, false);
              }

              try {
                ComposantMenu dernierComposantDansFilNavigation = (ComposantMenu) filNavigation.get(filNavigation
                    .size() - 1);

                if ((composantMenuSelectionne != null)
                    && composantMenuSelectionne.isService()
                    && (!composantMenuSelectionne.getIdentifiant().equals(
                        dernierComposantDansFilNavigation.getIdentifiant()))) {
                  filNavigation.add(composantMenuSelectionne);
                }
              } catch (Exception e) {
                filNavigation.add(composantMenuSelectionne.getParent());
              }

              ComposantMenu dernierComposantMenu = (ComposantMenu) filNavigation.get(filNavigation.size() - 1);

              if (dernierComposantMenu != null) {
                dernierComposantMenu.setDernierMenuEnfant(true);
              }

              // Ajouter le fil de navigation à l'utilisateur
              UtilitaireControleur.getUtilisateur(request.getSession()).setFilNavigation(filNavigation);
            } else {
              if (composantMenuSelectionne != null) {
                genererFilNavigation(request, composantMenuSelectionne, filNavigation, false);
              }

              // Ajouter le fil de navigation à l'utilisateur
              UtilitaireControleur.getUtilisateur(request.getSession()).setFilNavigation(filNavigation);
            }

            ajusterFilAriane(filNavigation);

            request.setAttribute("nbListeFilNavigation", new Integer(filNavigation.size()));
          }
        }
      }

      if (repertoireMenu != null) {
        // Exclure les composantes d'interface demandé.
        exclureComposantInterface(request);
        initialiserSelectionMenu(repertoireMenu, true, request);
      }
    }
  }

  private static void construireListeOnglet(ObjetSecurisable objetSecurisableSelectionne, String urlAvecMethode,
      int niveauATraiter, HttpServletRequest request) {
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    RepertoireMenu repertoireMenu = (RepertoireMenu) request.getSession().getAttribute(
        RepertoireMenu.CLE_REPERTOIRE_MENU);

    List listeObjetsSecurisable = (List) utilisateur.getListeObjetSecurisablesParAdresse().get(urlAvecMethode);

    if ((listeObjetsSecurisable != null) && (listeObjetsSecurisable.size() > 0)) {
      Iterator iterateurObjetsSecurisable = listeObjetsSecurisable.iterator();
      boolean isTrouve = false;
      ArrayList listeOnglets = new ArrayList();

      while (iterateurObjetsSecurisable.hasNext()) {
        ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateurObjetsSecurisable.next();

        if (objetSecurisableSelectionne == null) {
          // Sélectionner le premier onglet par défaut, si aucun autre de
          // spécifier.
          objetSecurisableSelectionne = objetSecurisable;
        }

        if (objetSecurisable instanceof Onglet
            || ((GestionSecurite.getInstance().getPageAccueilDefaut() == null) || ((objetSecurisable
                .getSeqObjetSecurisable().intValue() != GestionSecurite.getInstance().getPageAccueilDefaut().intValue()) && !GestionSecurite
                .getInstance().isPageAccueilInclusDansMenu()))
                || ((objetSecurisable.getSeqObjetSecurisable().intValue() == GestionSecurite.getInstance()
                .getPageAccueilDefaut().intValue()) && GestionSecurite.getInstance().isPageAccueilInclusDansMenu())) {
          if (utilisateur.getListeObjetSecurisablesParAdresse().containsKey(objetSecurisable.getAdresseWeb())) {
            ComposantMenu composantMenu = null;

            // DEBUT MODIF
            if (objetSecurisable instanceof Action
                && ((objetSecurisable.getListeObjetSecurisableEnfants() == null) || (objetSecurisable
                    .getListeObjetSecurisableEnfants().size() == 0)) && (listeObjetsSecurisable.size() == 1)) {
              ObjetSecurisable objetSecurisable2 = (ObjetSecurisable) objetSecurisable.clone();
              ArrayList listeOngletsTemp = new ArrayList();

              while (!(objetSecurisable2 instanceof Service)) {
                objetSecurisable2 = objetSecurisable2.getObjetSecurisableParent();

                if (objetSecurisable2 instanceof Onglet) {
                  listeOngletsTemp.add(objetSecurisable2);
                }
              }

              if (listeOngletsTemp.size() != 0) {
                // On inverse les onglets contenu dans le tableau étant
                // donnée que nous avons parcouru le référentiel en partant
                // des feuilles
                listeOngletsTemp = inverserListeOnglets(listeOngletsTemp);

                Iterator iteratorOngletsInverses = listeOngletsTemp.iterator();

                while (iteratorOngletsInverses.hasNext()) {
                  ObjetSecurisable unOnglet = (ObjetSecurisable) iteratorOngletsInverses.next();
                  preparerOnglets(repertoireMenu, unOnglet, composantMenu, urlAvecMethode, isTrouve,
                      objetSecurisableSelectionne, listeOnglets, niveauATraiter, request);
                }
              }
            }

            // FIN MODIF JF
            preparerOnglets(repertoireMenu, objetSecurisable, composantMenu, urlAvecMethode, isTrouve,
                objetSecurisableSelectionne, listeOnglets, niveauATraiter, request);
          } else {
            iterateurObjetsSecurisable.remove();
          }
        }
      }

      boolean ordreModifie = false;

      if (GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AJUSTER_ONGLET_AVEC_PARENT)
          .booleanValue()) {
        ordreModifie = ajusterOngletAvecParent(listeOnglets);
      }

      if (!ordreModifie) {
        // Trier la liste selon l'ordre d'affichage spécifié.
        listeOnglets = (ArrayList) UtilitaireListe.trierListe(listeOnglets, new AttributTri[] { new AttributTri(
            "ordreAffichage", true) });
      }

      // Modifier la liste des onglets s'il y a lieu
      modifierListeOnglets(objetSecurisableSelectionne, listeOnglets, urlAvecMethode, niveauATraiter, request);

      int compteurOngletAffichage = 1;
      for (int i = 0; i < listeOnglets.size(); i++) {
        ComposantMenu composant = (ComposantMenu) listeOnglets.get(i);

        if (composant.getOrdreAffichage() != null && composant.getOrdreAffichage().intValue() > 0) {
          composant.setNoMenu(new Integer(compteurOngletAffichage));
          compteurOngletAffichage = compteurOngletAffichage + 1;
        }
      }

      if (niveauATraiter > 1) {
        request.setAttribute(getNomListeOnglet(niveauATraiter), listeOnglets);
      } else {
        request.getSession().setAttribute(getNomListeOnglet(niveauATraiter), listeOnglets);
      }
    }
  }

  private static boolean ajusterOngletAvecParent(ArrayList listeOnglets) {
    int indice = listeOnglets.size() - 1;

    boolean modifie = false;

    while (indice > 1) {
      int depart = indice;
      ComposantMenu composantATester = (ComposantMenu) listeOnglets.get(indice);

      if ((composantATester.getParent() != null) && composantATester.getParent().isOnglet()) {
        boolean trouve = false;

        for (int i = depart; ((i >= 0) && !trouve); i--) {
          ComposantMenu composant = (ComposantMenu) listeOnglets.get(i);

          if (composant.getOrdreAffichage().intValue() > 0
              && composant.getIdentifiant().equals(composantATester.getIdentifiantParent())
              && i + composantATester.getOrdreAffichage().intValue() < listeOnglets.size()) {
            listeOnglets.add(i + composantATester.getOrdreAffichage().intValue(), composantATester);
            listeOnglets.remove(indice + 1);
            trouve = true;
            modifie = true;
          } else {
            if (composant.getIdentifiantParent().equals(composantATester.getIdentifiantParent()) && (indice != depart)) {
              trouve = true;
              modifie = true;
            }
          }
        }
      }

      indice--;
    }

    return modifie;
  }

  /**
   * Génération de la liste d'onglets.
   * 
   * @param niveauATraiter
   * @param listeOnglets
   * @param request
   * @param objetSecurisableSelectionne
   * @param isTrouve
   * @param urlAvecMethode
   * @param composantMenu
   * @param objetSecurisable
   * @param repertoireMenu
   */
  private static void preparerOnglets(RepertoireMenu repertoireMenu, ObjetSecurisable objetSecurisable,
      ComposantMenu composantMenu, String urlAvecMethode, boolean isTrouve,
      ObjetSecurisable objetSecurisableSelectionne, ArrayList listeOnglets, int niveauATraiter,
      HttpServletRequest request) {
    Onglet onglet = null;

    if (objetSecurisable instanceof Onglet) {
      onglet = (Onglet) objetSecurisable;
    } else {
      // Quitter immédiatement le traitement car n'est pas un onglet.
      return;
    }

    if ((repertoireMenu.getMenu(objetSecurisable.getSeqObjetSecurisable().toString()) == null)
        && ((onglet.getNiveauOnglet() == null) || (onglet.getNiveauOnglet().intValue() == niveauATraiter))) {
      composantMenu = new ComposantMenu();
      composantMenu.setIdentifiant(objetSecurisable.getSeqObjetSecurisable().toString());
      composantMenu.setLibelle(objetSecurisable.getNom());
      composantMenu.setAction(objetSecurisable.getAdresseWeb());
      composantMenu.setAideContextuelle(objetSecurisable.getAideContextuelle());
      composantMenu.setOrdreAffichage(objetSecurisable.getOrdrePresentation());
      composantMenu.setStyleCSS(objetSecurisable.getStyleCss());

      if (objetSecurisable.getObjetSecurisableParent() != null) {
        composantMenu.setIdentifiantParent(objetSecurisable.getObjetSecurisableParent().getSeqObjetSecurisable()
            .toString());
      }

      // Spécifier l'onglet sélectionnée.
      if (urlAvecMethode.equals(objetSecurisable.getAdresseWeb())
          || (urlAvecMethode.equals(objetSecurisable.getObjetSecurisableParent().getAdresseWeb()) && !isTrouve)) {
        isTrouve = true;

        composantMenu.setSelectionne(true);
      } else {
        composantMenu.setSelectionne(false);
      }

      composantMenu = repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);

      // Valider si le composant d'interface est actif, sinon le desactiver.
      if (UtilitaireControleur.isComposantInterfaceExclus(request, composantMenu.getLibelle())) {
        composantMenu.setActif(false);
      } else {
        composantMenu.setActif(true);
        listeOnglets.add(composantMenu);
      }
    } else {
      if ((onglet.getNiveauOnglet().intValue() > niveauATraiter)
          && (request.getAttribute(getNomListeOnglet(onglet.getNiveauOnglet().intValue())) == null)) {
        construireListeOnglet(objetSecurisableSelectionne, urlAvecMethode, onglet.getNiveauOnglet().intValue(), request);
      }
    }
  }

  /**
   * Retourne le nom dont porte la liste d'onglet dans la session.
   * 
   * @return le nom dont porte la liste d'onglet dans la session.
   * @param niveauOnglet
   *          le niveau d'onglet dont on désire le nom.
   */
  public static String getNomListeOnglet(int niveauOnglet) {
    StringBuffer nomAttributListeOnglet = new StringBuffer();

    nomAttributListeOnglet.append("listeOnglets");

    if (niveauOnglet > 1) {
      nomAttributListeOnglet.append(niveauOnglet);
    }

    return nomAttributListeOnglet.toString();
  }

  /**
   * Initialiser le menu afin qu'aucun composant de menu n'est sélectionné
   */
  private static void initialiserSelectionMenu(RepertoireMenu menu, boolean traiterComposantAExclure,
      HttpServletRequest request) {
    Iterator iterateurMenu = menu.getMenuPremierNiveau().iterator();

    while (iterateurMenu.hasNext()) {
      ComposantMenu composantMenu = (ComposantMenu) iterateurMenu.next();

      if (!traiterComposantAExclure) {
        composantMenu.setSelectionne(false);
      }

      initialiserComposantMenu(composantMenu, traiterComposantAExclure, request);
    }
  }

  /**
   * Initialiser les composants de menu afin qu'il ne soit pas sélectionné
   */
  private static void initialiserComposantMenu(ComposantMenu menu, boolean traiterComposantAExclure,
      HttpServletRequest request) {
    if ((menu.getComposantsMenu() != null) && (menu.getComposantsMenu().size() > 0)) {
      Iterator iterateurMenu = menu.getComposantsMenu().iterator();

      while (iterateurMenu.hasNext()) {
        ComposantMenu composantMenu = (ComposantMenu) iterateurMenu.next();

        if (traiterComposantAExclure) {
          // Valider si le composant d'interface est actif, sinon le desactiver.
          if (UtilitaireControleur.isComposantInterfaceExclus(request, composantMenu.getLibelle())) {
            composantMenu.setActif(false);
          } else {
            composantMenu.setActif(true);
          }
        }

        initialiserComposantMenu(composantMenu, traiterComposantAExclure, request);

        if (!traiterComposantAExclure) {
          composantMenu.setSelectionne(false);
        }
      }
    } else {
      if (!traiterComposantAExclure) {
        menu.setSelectionne(false);
      }
    }
  }

  /**
   * Génération du fil de navigation (fil d'ariane) avec l'aide du composant de menu actuellement sélectionné
   * 
   * @param request
   *          la requête en traitement
   * @param composantMenu
   *          le composant de menu à traiter
   * @param filNavigation
   *          le fil de navigation
   * @since 2.1 Ajout du support de la navigation complete dans le fil d'ariance, les onglets peuvent ainsi s'ajouter
   *        dans le fil d'ariane.
   */
  private static void genererFilNavigation(HttpServletRequest request, ComposantMenu composantMenu,
      ArrayList filNavigation, boolean isOngletSelectionne) {

    boolean traiterNavigationCompleteDansFilAriane = GestionParametreSysteme.getInstance()
        .getBoolean("traiterNavigationCompleteDansFilAriane").booleanValue();

    if (composantMenu.isOnglet() && !isOngletSelectionne) {
      composantMenu.setSelectionne(true);
      isOngletSelectionne = true;
    } else {
      if ((composantMenu.isService() && !GestionParametreSysteme.getInstance()
          .getBoolean(ConstantesParametreSysteme.MULTIMENU_DESACTIVE_SELECTION_SERVICE).booleanValue())
          || composantMenu.isSection()) {
        composantMenu.setSelectionne(true);

        if (composantMenu.getOrdreAffichage().intValue() == 0) {
          // Nous devons l'afficher dans le fil de navigation s'il y a lieu,
          // donc
          // nous modifions l'indicateur d'ordre d'affichage temporairement à
          // -1000.
          composantMenu.setOrdreAffichage(new Integer(-1000));
        }
      }
    }

    ComposantMenu composantMenuParent = composantMenu.getParent();

    if (composantMenuParent != null) {
      genererFilNavigation(request, composantMenuParent, filNavigation, isOngletSelectionne);
    }

    boolean conditionParentOngletPermis = true;
    if (!traiterNavigationCompleteDansFilAriane) {
      conditionParentOngletPermis = composantMenuParent != null && !composantMenuParent.isOnglet();
    }

    // Générer le fil de navigation de l'utilisateur si le parent n'est pas un
    // onglet
    if ((composantMenuParent != null) && conditionParentOngletPermis) {
      List listeEnfants = composantMenuParent.getObjetSecurisable().getListeObjetSecurisableEnfants();
      boolean isEnfantsFilNavigation = false;

      if ((listeEnfants != null) && (listeEnfants.size() > 0)) {

        boolean conditionEnfantOngletPermis = Service.class.isInstance(listeEnfants.get(0));
        if (traiterNavigationCompleteDansFilAriane) {
          conditionEnfantOngletPermis = Onglet.class.isInstance(listeEnfants.get(0));
        }

        if (Service.class.isInstance(listeEnfants.get(0)) || conditionEnfantOngletPermis) {
          ObjetSecurisable objet = (ObjetSecurisable) listeEnfants.get(0);

          if ((objet.getOrdrePresentation() != null) && (objet.getOrdrePresentation().intValue() > 0)) {
            isEnfantsFilNavigation = true;
          }
        }
      }

      composantMenuParent.setEnfantsPourFilNavigation(isEnfantsFilNavigation);

      ComposantMenu dernierComposant = null;

      if (filNavigation.size() > 0) {
        dernierComposant = (ComposantMenu) filNavigation.get(filNavigation.size() - 1);
      } else {
        dernierComposant = composantMenuParent;
      }

      if (!(dernierComposant.getIdentifiant().equals(composantMenuParent.getIdentifiant()))) {
        ajouterFilAriane(filNavigation, composantMenuParent);
      }
    }

    if ((composantMenu.getObjetSecurisable().getListeObjetSecurisableEnfants().size() == 0)
        && (composantMenuParent != null)) {
      if (!composantMenu.isOnglet() || traiterNavigationCompleteDansFilAriane) {
        composantMenuParent.setEnfantsPourFilNavigation(true);
        ajouterFilAriane(filNavigation, composantMenu);
      }
    } else {
      if (isOngletSelectionne && traiterNavigationCompleteDansFilAriane) {

        boolean composantCache = composantMenu.getOrdreAffichage().intValue() == 0;

        if (composantMenuParent != null && !composantCache) {
          composantMenuParent.setEnfantsPourFilNavigation(true);
        }

        composantMenu.setEnfantsPourFilNavigation(false);
        if (composantMenu.getOrdreAffichage().intValue() != 0) {
          ajouterFilAriane(filNavigation, composantMenu);
        }
      }
    }

    // Traiter si c'est un service de sélectionnée et le service enfant est
    // aussi un service.
    if ((composantMenu.getObjetSecurisable().getListeObjetSecurisableEnfants().size() >= 1)
        && composantMenu.isService()
        && Service.class.isInstance(composantMenu.getObjetSecurisable().getListeObjetSecurisableEnfants().get(0))) {
      request.getSession().setAttribute(Constantes.SERVICE_SELECTIONNEE_PARENT, composantMenu.getObjetSecurisable());

      filNavigation.add(composantMenu);
    }

    if ((composantMenu.getObjetSecurisable().getListeObjetSecurisableEnfants().size() >= 1)
        && (!(composantMenu.getObjetSecurisable().getListeObjetSecurisableEnfants().get(0) instanceof Onglet))) {
      if (composantMenuParent != null) {
        composantMenuParent.setEnfantsPourFilNavigation(true);
      }
    }

    ObjetSecurisable service = composantMenu.getObjetSecurisable();

    // Spécifier le service parent.
    if (Service.class.isInstance(service) && Section.class.isInstance(service.getObjetSecurisableParent())) {
      request.getSession().setAttribute(Constantes.SERVICE_SELECTIONNEE_PARENT, composantMenu.getObjetSecurisable());
    }
  }

  private static void ajouterFilAriane(ArrayList filAriane, ComposantMenu composantAAjouter) {

    ObjetSecurisable accueil = GestionSecurite.getInstance().getComposantMenuAccueil();

    if (accueil != null && !GestionSecurite.getInstance().isPageAccueilInclusDansMenu()) {
      if (composantAAjouter.getObjetSecurisable() != null
          && accueil.getSeqObjetSecurisable().longValue() != composantAAjouter.getObjetSecurisable()
          .getSeqObjetSecurisable().longValue()) {
        filAriane.add(composantAAjouter);
      }

    } else {
      filAriane.add(composantAAjouter);
    }
  }

  private static void ajusterFilAriane(ArrayList filAriane) {
    boolean composantAExclure = false;
    if (filAriane != null && filAriane.size() > 0) {
      for (int i = 0; i < filAriane.size(); i++) {
        ComposantMenu composant = (ComposantMenu) filAriane.get(i);
        if (i != filAriane.size() - 1) {
          composant.setEnfantsPourFilNavigation(true);
        } else {
          composant.setEnfantsPourFilNavigation(false);
        }
        if (composant.getOrdreAffichage() <= 0) {
          filAriane.remove(i);
          i--;
          composantAExclure = true;
        }
      }
    }

    if (composantAExclure) {
      // Si des composant d'excluent il faut ajuster à nouveau.
      ajusterFilAriane(filAriane);
    }
  }

  /**
   * Applique le traitement nécessaire si la requête du client provient de la persistance d'un DIV pour générer son
   * contenu.
   * 
   * @param request
   *          la requête en traitement.
   */
  public static boolean appliquerPersistanceDiv(HttpServletRequest request) {
    // Si on détecte le paramètre divPersistant dans l'url appellé pas le
    // client,
    // il faut alors exécuter le code nécessaire pour traiter les div
    // persistant.
    // De plus, il fait s'assurer que ce n'est pas un rafraichissement
    // automatique
    // qui a appellé l'action.
    boolean fermerSection = false;

    if (!UtilitaireControleur.isRafraichissementAjax(request) && (request.getParameter("divPersistant") != null)) {
      String divPersistant = request.getParameter("divPersistant");

      if (!UtilitaireControleur.isDivOuvert(divPersistant, request).booleanValue()) {
        setEtatPersistanceDiv(divPersistant, Boolean.TRUE, request);
      } else {
        setEtatPersistanceDiv(divPersistant, Boolean.FALSE, request);

        fermerSection = true;
      }
    }

    return fermerSection;
  }

  /**
   * Permet de fixer l'état de persistance d'un div spécifique afin de spécifier s'il est ouvert ou fermée.
   * 
   * @param request
   *          la requête en traitement.
   * @param ouvert
   *          true si le div est ouvert.
   * @param idDiv
   *          l'identifiant du div.
   */
  public static void setEtatPersistanceDiv(String idDiv, Boolean ouvert, HttpServletRequest request) {
    HashMap ensembleDivOuvert = (HashMap) request.getSession().getAttribute("tableau_div_persistant");

    if (ensembleDivOuvert == null) {
      ensembleDivOuvert = new HashMap();
    }

    ensembleDivOuvert.put(idDiv, ouvert);

    request.getSession().setAttribute("tableau_div_persistant", ensembleDivOuvert);
  }

  /**
   * Appliquer la sécurité de navigation pour l'url demandé par l'utilisateur. Si l'utilisateur ne possède pas ce droit,
   * lancer une exception de sécurité.
   * 
   * @param utilisateur
   *          l'utilisateur en cours.
   * @param request
   *          la requête en traitement.
   * @param baseForm
   *          le formulaire à afficher s'il y a lieu
   * @param mapping
   *          l'instance de mapping en cours
   */
  public static void appliquerSecuritePourNavigation(Utilisateur utilisateur, HttpServletRequest request,
      BaseForm baseForm, ActionMapping mapping) {
    String urlCourant = getUrlCourant(request, mapping);

    if ((utilisateur != null) && GestionSecurite.getInstance().isSecuriteActif()
        && (utilisateur.getListeObjetSecurisablesParAdresse() != null)) {

      boolean urlAutorisePourUtilisateur = utilisateur.getListeObjetSecurisablesParAdresse().get(urlCourant) != null;

      boolean urlSecuriseDansReferentiel = GestionSecurite.getInstance().getListeObjetSecurisablesParAdresse()
          .get(urlCourant) != null;

      if (urlSecuriseDansReferentiel && !urlAutorisePourUtilisateur && UtilitaireControleur.isActionSecurise(request)) {
        // Si l'utilisateur ne possède pas le droit d'accéder à l'url demandé et
        // le référence
        // des objets sécurisable possède ce url, lancé une exception de refus
        // d'accès.
        String cleMessage = "infra_sofi.erreur.commun.securite_exception";
        Message message = UtilitaireMessage.getMessage(cleMessage, UtilitaireControleur.getLocale(request.getSession())
            .toString(), null);
        String messageException = message.getMessage() + urlCourant;
        throw new SecuriteException(messageException);
      } else {
        List listeObjetSecurisables = (List) utilisateur.getListeObjetSecurisablesParAdresse().get(urlCourant);
        ObjetSecurisable objetSecurisableSecure = GestionSecurite.getInstance().getObjetSecurisableParAdresse(
            urlCourant, listeObjetSecurisables);

        if (baseForm != null) {
          if ((objetSecurisableSecure != null) && objetSecurisableSecure.isLectureSeulement()) {
            // Si l'objet sécurisable est en lecture seulement.
            baseForm.setModeLectureSeulement(true);
          } else {
            baseForm.setModeLectureSeulement(false);
          }
        }
      }
    }
  }

  /**
   * Retourne l'url courant demandé. Seulement l'action et la demande démandé est retourné dans l'url.
   * 
   * @param request
   *          la requête en traitement
   * @param mapping
   *          l'instance de mapping en traitement
   * @return l'url courant
   */
  public static String getUrlCourant(HttpServletRequest request, ActionMapping mapping) {
    String url = "";
    String methode = request.getParameter(UtilitaireParametreSysteme.getMethodeParam());

    if ((methode != null) && !methode.equals("")) {
      String path = mapping.getPath();

      StringBuffer req = new StringBuffer();
      req.append(path.substring(1));
      req.append(".do");
      req.append("?");
      req.append(UtilitaireParametreSysteme.getMethodeParam());
      req.append("=");
      req.append(methode);

      Map listeParametres = GestionSecurite.getInstance().getListeParametresPourAdresse(req.toString());

      if (listeParametres != null) {
        Iterator iterateurListe = listeParametres.keySet().iterator();

        while (iterateurListe.hasNext()) {
          req.append("&");

          String cle = (String) iterateurListe.next();
          req.append(cle);
          req.append("=");

          String valeur = request.getParameter(cle);
          req.append(valeur);
        }
      }

      url = req.toString();
    }

    return url;
  }

  /**
   * Permet a un formulaire d'avoir des accents lorsque le formulaire contient des attribut pour upload.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   */
  protected static HttpServletRequest processMultipart(HttpServletRequest request) {
    if (!"POST".equalsIgnoreCase(request.getMethod())) {
      return (request);
    }

    String contentType = request.getContentType();

    if ((contentType != null) && contentType.startsWith("multipart/form-data")) {
      MultipartRequestWrapper multipart = new MultipartRequestWrapper(request);
      return multipart;
    }

    return request;
  }

  /**
   * Populer les propriétés de l'instance spécifique ActionForm depuis les paramètres de la requête. En plus, l'attribut
   * <code>Globals.CANCEL_KEY</code> est fixé si la requête qui est soumitté avec le bouton créé avec
   * <code>CancelTag</code>.
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param form
   *          l'instance du formulaire ActionForm qui est a populer
   * @param mapping
   *          Le ActionMapping qui est utilisé
   * 
   * @exception ServletException
   *              si une exception par RequestUtils.populate()
   */
  protected static void processPopulate(ActionServlet servlet, HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping) throws ServletException {
    BaseForm formulaire = (BaseForm) form;

    if (formulaire == null) {
      return;
    }

    // Populer les propriétés de l'instance ActionForm
    if (log.isDebugEnabled()) {
      log.debug(" Population de l'objet Formulaire depuis la requête");
    }

    form.setServlet(servlet);

    boolean isPOST = "POST".equalsIgnoreCase(request.getMethod());

    if (!UtilitaireControleur.isAppelAjax(request) || isPOST) {
      if (mapping.getScope().toLowerCase().equals("session") && isPOST) {
        if (!formulaire.isFormulaireSurPlusieursPages() && !formulaire.isTraiterAttributsAfficheSeulement()) {
          formulaire.initialiserFormulaire(request, mapping, true);
        } else {
          formulaire.initialiserFormulaire(request, mapping);
          formulaire.initialiserAttributBooleanEtTableau();
        }
      } else {
        formulaire.initialiserFormulaire(request, mapping);
      }
    }

    if (mapping.getMultipartClass() != null) {
      request.setAttribute(Globals.MULTIPART_KEY, mapping.getMultipartClass());
    }

    formulaire.preparerPopulation(mapping, request);

    RequestUtils.populate(form, mapping.getPrefix(), mapping.getSuffix(), request);

    // Fixer l'annuler de la requête si approprie
    if ((request.getParameter(org.apache.struts.taglib.html.Constants.CANCEL_PROPERTY) != null)
        || (request.getParameter(org.apache.struts.taglib.html.Constants.CANCEL_PROPERTY_X) != null)) {
      request.setAttribute(Globals.CANCEL_KEY, Boolean.TRUE);
    }
  }

  /**
   * Suppression de tous les attributs qui ont été temporairement fixer pour une action précise.
   * 
   * @param ancienActionSelectionne
   *          l'ancienne action sélectionnée.
   * @param actionCourante
   *          l'action courante.
   * @param request
   *          la requête présentement traité.
   * @param mapping
   *          l'instance de mapping courant.
   */
  protected static void suppressionAttributsTemporairePourUneAction(String ancienActionSelectionne,
      String actionCourante, HttpServletRequest request, ActionMapping mapping) {

    boolean navigationInterService = request.getSession().getAttribute("sofi_inter_service") != null;

    if (!navigationInterService) {
      // Supprimer tous les attributs qui ont logé temporairement pour une
      // action.
      if ((ancienActionSelectionne != null) && !ancienActionSelectionne.equals(actionCourante)) {
        HashSet listeAttributs = (HashSet) request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_ACTION);

        if (listeAttributs != null) {
          Iterator iterateur = listeAttributs.iterator();

          while (iterateur.hasNext()) {
            String identifiant = (String) iterateur.next();

            HashSet listeAttributTemporaire = (HashSet) request.getSession().getAttribute(
                Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT);

            if (!listeAttributTemporaire.contains(identifiant)) {
              request.getSession().setAttribute(identifiant, null);
              iterateur.remove();
            }
          }
        }
      }
      // Supprimer tous les attributs qui ont logé temporairement pour un
      // service.
      RequestProcessorHelper.suppressionAttributsTemporairePourUnService(ancienActionSelectionne, actionCourante,
          request, mapping);
    }
  }

  /**
   * Suppression de tous les attributs qui ont été temporairement fixer pour une action précise.
   * 
   * @param ancienActionSelectionne
   *          l'ancienne action sélectionnée.
   * @param actionCourante
   *          l'action courante.
   * @param request
   *          la requête présentement traité.
   * @param mapping
   *          l'instance de mapping courant.
   */
  protected static void suppressionAttributsTemporairePourUnService(String ancienActionSelectionne,
      String actionCourante, HttpServletRequest request, ActionMapping mapping) {

    boolean navigationInterService = request.getSession().getAttribute("sofi_inter_service") != null;

    if (!navigationInterService) {
      HashSet listeAttributs = (HashSet) request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_SERVICE);
      if (listeAttributs != null) {
        ObjetSecurisable objetSecurisableAncien = GestionSecurite.getInstance().getObjetSecurisableParAdresse(
            ancienActionSelectionne);
        ObjetSecurisable objetSecurisableCourant = GestionSecurite.getInstance().getObjetSecurisableParAdresse(
            actionCourante);

        boolean supprimerAttribut = false;

        if ((objetSecurisableAncien == null) || (objetSecurisableCourant == null)) {
          supprimerAttribut = false;
        }

        if ((objetSecurisableAncien != null) && (objetSecurisableCourant != null)) {
          ObjetSecurisable serviceAncien = getService(objetSecurisableAncien);
          ObjetSecurisable serviceCourant = getService(objetSecurisableCourant);

          if (!serviceAncien.getAdresseWeb().equals(serviceCourant.getAdresseWeb())) {
            supprimerAttribut = true;
          }
        }

        // Supprimer tous les attributs qui ont logé temporairement pour une
        // action.
        if (supprimerAttribut) {
          if (listeAttributs != null) {
            Iterator iterateur = listeAttributs.iterator();

            while (iterateur.hasNext()) {
              String identifiant = (String) iterateur.next();
              HashSet listeAttributTemporaire = (HashSet) request.getSession().getAttribute(
                  Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT);

              if (!listeAttributTemporaire.contains(identifiant)) {
                request.getSession().setAttribute(identifiant, null);
                iterateur.remove();
              }
            }
          }
        }
      }
    }
  }

  private static ObjetSecurisable getService(ObjetSecurisable objetSecurisable) {
    if (objetSecurisable != null) {
      while (!(Service.class.isInstance(objetSecurisable) || (Section.class.isInstance(objetSecurisable) && (objetSecurisable
          .getAdresseWeb() != null)))) {
        objetSecurisable = objetSecurisable.getObjetSecurisableParent();
      }

      return objetSecurisable;
    } else {
      return null;
    }
  }

  /**
   * Modifier le référentiel pour un utilisateur.
   * 
   * @param request
   *          la requête HTTP en traitement.
   * @param utilisateur
   *          l'utilisateur en traitement.
   */
  protected static void modificationReferentielUtiliateur(Utilisateur utilisateur, HttpServletRequest request) {
    HashMap repertoireComposantInterfaceAAjouter = (HashMap) request.getSession().getAttribute(
        Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_AJOUTER);

    if ((repertoireComposantInterfaceAAjouter != null) && (repertoireComposantInterfaceAAjouter.size() > 0)) {
      Iterator listeReferentielUtilisateur = utilisateur.getListeObjetSecurisables().iterator();

      while (listeReferentielUtilisateur.hasNext()) {
        modificationReferentielUtiliateurEnfant(listeReferentielUtilisateur, repertoireComposantInterfaceAAjouter);
      }

      RepertoireMenu.creerRepertoireMenu(utilisateur, request);
      GestionSecurite.getInstance().genererReferentielObjetSecurisables(utilisateur);
    }

    request.getSession().setAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_AJOUTER, null);
  }

  private static void modificationReferentielUtiliateurEnfant(Iterator iterateurEnfant,
      HashMap repertoireComposantInterfaceAAjouter) {
    while (iterateurEnfant.hasNext()) {
      ObjetSecurisable objetSecurisableParent = (ObjetSecurisable) iterateurEnfant.next();
      String identifiantParent = objetSecurisableParent.getNom();

      if (repertoireComposantInterfaceAAjouter.get(identifiantParent) != null) {
        List listeAjoutReferentiel = (List) repertoireComposantInterfaceAAjouter.get(identifiantParent);

        // Spécifier le service parent.
        Iterator iterateur = listeAjoutReferentiel.iterator();

        while (iterateur.hasNext()) {
          ObjetSecurisable service = (ObjetSecurisable) iterateur.next();
          service.setObjetSecurisableParent(objetSecurisableParent);
        }

        objetSecurisableParent.getListeObjetSecurisableEnfants().addAll(listeAjoutReferentiel);
      }

      // Populer les objets sécurisables enfants.
      if ((objetSecurisableParent.getListeObjetSecurisableEnfants() != null)
          && (objetSecurisableParent.getListeObjetSecurisableEnfants().size() > 0)) {
        modificationReferentielUtiliateurEnfant(objetSecurisableParent.getListeObjetSecurisableEnfants().iterator(),
            repertoireComposantInterfaceAAjouter);
      }
    }
  }

  /**
   * Permet d'exclure un composant d'interface tel qu'un onglet correspondant à son identifiant pour l'action courante.
   * 
   * @param request
   *          la requête en traitement.
   */
  public static void exclureComposantInterface(HttpServletRequest request) {
    HashSet liste = (HashSet) request.getSession().getAttribute("listeIdentifiantAExlure");

    if (liste != null) {
      Iterator iterateur = liste.iterator();

      while (iterateur.hasNext()) {
        String identifiant = (String) iterateur.next();

        try {
          HashSet repertoireOngletExlure = (HashSet) request.getSession().getAttribute(
              Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL);

          if (repertoireOngletExlure == null) {
            repertoireOngletExlure = new HashSet();
            request.getSession().setAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL,
                repertoireOngletExlure);
          }

          if (!repertoireOngletExlure.contains(identifiant)) {
            repertoireOngletExlure.add(identifiant);
          }
        } catch (Exception e) {
        }
      }
    }
  }

  /**
   * Méthode permettant d'inverser les onglets contenus dans une liste
   * 
   * @param listeOngletsAInverser
   */
  private static ArrayList inverserListeOnglets(ArrayList listeOngletsAInverser) {
    Object[] listeOngletsObjet = listeOngletsAInverser.toArray();
    CollectionUtils.reverseArray(listeOngletsObjet);

    List list = Arrays.asList(listeOngletsObjet);
    listeOngletsAInverser = new ArrayList(list);

    return listeOngletsAInverser;
  }

  /**
   * Fixer le composant d'interface du référentiel en traitement.
   * 
   * @param mapping
   *          l'instance du mapping courant.
   * @param request
   *          la requête en traitemetn.
   */
  public static void fixerComposantReferentielEnTraitement(HttpServletRequest request, ActionMapping mapping) {
    String urlAvecMethode = RequestProcessorHelper.getUrlCourant(request, mapping);

    ObjetSecurisable serviceSelectionne = GestionSecurite.getInstance().getObjetSecurisableParAdresse(urlAvecMethode);

    if (serviceSelectionne != null) {
      request.getSession().setAttribute(Constantes.SERVICE_SELECTIONNEE, serviceSelectionne);
    }
  }

  public static void traiterFichierInfoVersion(ServletContext servletContext) {
    if (servletContext.getAttribute("sofiFichierInfoVersionTraite") == null) {
      String cheminReelApp = UtilitaireRequest.getEmplacementWebInfReel(servletContext);

      try {
        String nomFichierComplet = cheminReelApp + "version.html";
        File fichierVersionHtml = new File(nomFichierComplet);
        String contenuFichier = UtilitaireFichier.lire(fichierVersionHtml);

        // Modifier le contenu du fichier courant.

        java.util.Date dateHeure = new java.util.Date();

        contenuFichier = UtilitaireString.remplacerTous(contenuFichier, "@dateDemarrage@",
            UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(dateHeure));

        // Supprimer le fichier présent.
        UtilitaireFichier.supprimerFichier(nomFichierComplet);

        // Générer un nouveau fichier avec la date du dernier démarrage.
        FichierTexte fichier = new FichierTexte("version.html", cheminReelApp);
        fichier.setContenu(new StringBuffer(contenuFichier));
        fichier.genererFichierTexte();

        servletContext.setAttribute("sofiFichierInfoVersionTraite", Boolean.TRUE);

      } catch (Exception e) {
        if (log.isInfoEnabled()) {
          log.info("Le fichier version.html est introuvable. Il doit être produit lors de la création de l'archive.", e);
        }
      }
    }
  }

  /**
   * Cette methode permet de modifier le ou les objet securisables de la liste d'onglets selon les informations
   * contenues dans l'objet serviceParentAvecComposantsMenusAModifier.
   * 
   * L'objet serviceParentAvecComposantsMenusAModifier peut etre dans la session ou dans le contexte de l'application
   * via "Spring".
   * 
   * @return
   * @param request
   * @param niveauATraiter
   * @param urlAvecMethode
   * @param objetSecurisableSelectionne
   */
  public static void modifierListeOnglets(ObjetSecurisable objetSecurisableSelectionne, List listeOnglets,
      String urlAvecMethode, int niveauATraiter, HttpServletRequest request) {
    if ((listeOnglets != null) && !listeOnglets.isEmpty() && objetSecurisableSelectionne instanceof Onglet) {
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

      // On obtient le context pour retrouve le bean
      // serviceParentAvecComposantsMenusAModifier
      WebApplicationContext context = null;

      try {
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
      } catch (IllegalStateException ise) {
        // Ne rien faire, car on n'est pas oublige d'utiliser spring dans ce
        // cas...
      }

      Map serviceParentAvecComposantsMenusAModifier = null;

      if (context != null && context.containsBean("serviceParentAvecComposantsMenusAModifier")) {
        serviceParentAvecComposantsMenusAModifier = (Map) context.getBean("serviceParentAvecComposantsMenusAModifier");
      } else {
        // Si on utilise pas Spring, on va voir dans la session si le bean
        // serviceParentAvecComposantsMenusAModifier est présent
        request.getSession().getAttribute("serviceParentAvecComposantsMenusAModifier");
      }

      if (serviceParentAvecComposantsMenusAModifier != null) {
        for (Iterator iterIdentificantObjetSecurisableParent = serviceParentAvecComposantsMenusAModifier.keySet()
            .iterator(); iterIdentificantObjetSecurisableParent.hasNext();) {
          String identifiantObjetSecurisableParent = (String) iterIdentificantObjetSecurisableParent.next();

          // On obtient le service parent de l'objet sécurisable sélectionner
          ObjetSecurisable serviceParent = getServiceObjetSecurisableSelectionne(objetSecurisableSelectionne
              .getObjetSecurisableParent());

          boolean isBonParentSelonNiveauATraiter = serviceParent.getNom().equals(identifiantObjetSecurisableParent);

          if (isBonParentSelonNiveauATraiter) {
            // Obtenir la liste de composant de menu � modifier.
            List listeComposantMenuAModifier = (List) serviceParentAvecComposantsMenusAModifier
                .get(identifiantObjetSecurisableParent);

            if (listeComposantMenuAModifier != null) {
              for (Iterator iterComposantMenuAModifier = listeComposantMenuAModifier.iterator(); iterComposantMenuAModifier
                  .hasNext();) {
                ComposantMenuModifiable composant = (ComposantMenuModifiable) iterComposantMenuAModifier.next();

                // Si le composant de menu qui doit �tre modifier est dans la
                // liste d'onglet, on applique la modification.
                if (getComposantMenuSelonNom(listeOnglets, composant.getLibelle()) != null) {
                  composant.appliquerModification(utilisateur, identifiantObjetSecurisableParent, listeOnglets,
                      niveauATraiter, request);
                }
              }
            }
          }
        }
      }
    }
  }

  private static ObjetSecurisable getServiceObjetSecurisableSelectionne(ObjetSecurisable objetSecurisableSelectionner) {
    ObjetSecurisable objetSecurisable = null;

    if (objetSecurisableSelectionner instanceof Service) {
      objetSecurisable = objetSecurisableSelectionner;
    } else {
      objetSecurisable = getServiceObjetSecurisableSelectionne(objetSecurisableSelectionner.getObjetSecurisableParent());
    }

    return objetSecurisable;
  }

  private static ComposantMenu getComposantMenuSelonNom(List listeOnglets, String nom) {
    ComposantMenu ongletRecherche = null;

    for (Iterator iterOnglet = listeOnglets.iterator(); iterOnglet.hasNext() && (ongletRecherche == null);) {
      ComposantMenu onglet = (ComposantMenu) iterOnglet.next();

      if (onglet.getLibelle().equals(nom)) {
        ongletRecherche = onglet;
      }
    }

    return ongletRecherche;
  }

  /**
   * Indique si le formulaire a déjà été initialisé.
   * 
   * @param request
   *          la requête en traitement.
   * @param mapping
   *          l'instance de mapping courant.
   * @return true si le formulaire a déjà été initialisé.
   */
  public static boolean isFormulaireDejaInitialise(HttpServletRequest request, ActionMapping mapping) {
    String ancienFormulaire = (String) request.getSession().getAttribute(Constantes.NOM_DERNIER_FORMULAIRE);

    if ((mapping.getName() == null) || (ancienFormulaire == null)) {
      return false;
    }

    if (ancienFormulaire.equals(mapping.getName())) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne l'adresse de l'action sélectionné
   * 
   * @param request
   *          la requête en traitement.
   * @param mapping
   *          l'instance de mapping.
   * @return l'adresse de l'action sélectionné
   */
  public static String getAdresseActionCourante(HttpServletRequest request, ActionMapping mapping) {
    StringBuffer adresseCourante = new StringBuffer();
    adresseCourante.append(mapping.getPath());
    adresseCourante.append(".do?");

    String methodeParam = UtilitaireParametreSysteme.getMethodeParam();

    adresseCourante.append(methodeParam);
    adresseCourante.append("=");

    String methode = request.getParameter(methodeParam);
    adresseCourante.append(methode);

    return adresseCourante.toString();
  }

  /**
   * Retourne le formulaire courant
   * 
   * @return le formulaire courant.
   * @param request
   */
  public static BaseForm getFormulaireCourant(HttpServletRequest request) {
    String nomFormulaire = (String) request.getSession().getAttribute(Constantes.FORMULAIRE);

    if (nomFormulaire != null) {
      return (BaseForm) request.getSession().getAttribute(nomFormulaire);
    } else {
      return null;
    }
  }
}
