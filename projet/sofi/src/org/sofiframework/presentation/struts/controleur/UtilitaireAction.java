/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Utilitaire pour la gestion des cache et autres fonctionnalités SOFI.
 *
 * @author Guillaume Poirier, Steve Tremblay
 * @since SOFI 2.0
 */
public class UtilitaireAction extends BaseDispatchAction {
  /**
   * Indicateur d'initialisation de tous les libellés.
   */
  public final static String CACHE_LIBELLE = "LIBELLE";

  /**
   * Indicateur d'initialisation de tous les messages.
   */
  public final static String CACHE_MESSAGE = "MESSAGE";

  /**
   * Indicateur d'initialisation de tous les aides en ligne.
   */
  public final static String CACHE_AIDE = "AIDE";

  /**
   * Indicateur d'initialisation d'un domaine de valeur.
   */
  public final static String CACHE_DOMAINE_VALEUR = "DOMAINEVALEUR";
  //
  //  /**
  //   * URL permetteant de mettre à jour un libellé
  //   */
  //  public final static String URL_MAJ_LIBELLE = "utilitaireSOFI.do?methode=mettreAJourLibelle&cleLibelle=";
  //
  //  /**
  //   * URL permettant de mettre à jour un message.
  //   */
  //  public final static String URL_MAJ_MESSAGE = "utilitaireSOFI.do?methode=mettreAJourMessage&cleMessage=";
  //
  //  /**
  //   * URL permettant de mettre à jour un paramètre système.
  //   */
  //  public final static String URL_MAJ_PARAMETRE_SYSTEME = "utilitaireSOFI.do?methode=mettreAJourParametreSysteme&codeParametre=";
  //
  //  /**
  //   * URL permettant de mettre à jour tous les libellés.
  //   */
  //  public final static String URL_MAJ_TOUS_LIBELLES =
  //    "utilitaireSOFI.do?methode=viderCache&nomCache=" + CACHE_LIBELLE;
  //
  //  /**
  //   * URL permettant de mettre à jour tous les messages.
  //   */
  //  public final static String URL_MAJ_TOUS_MESSAGES =
  //    "utilitaireSOFI.do?methode=viderCache&nomCache=" + CACHE_MESSAGE;
  //
  //  /**
  //   * URL permettant de mettre à jour tous les messages.
  //   */
  //  public final static String URL_MAJ_TOUS_AIDES =
  //    "utilitaireSOFI.do?methode=viderCache&nomCache=" + CACHE_AIDE;
  //
  //  public final static String URL_LISTER_OBJET_CACHE = "utilitaireSOFI.do?methode=obtenirListeCaches";
  //
  //  public final static String URL_RECHARGER_OBJET_CACHE = "utilitaireSOFI.do?methode=rechargerObjetCache&nomObjetCache=";

  /**
   * Vider une cache. On doit passer le nom de la cache, l'utilisateur gestionnaire de l'application
   * et le mot de passe de l'utilisateur.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward viderCache(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    //Obtention de la cache à vider
    String nomCacheAVider = request.getParameter("nomCache");

    if (UtilitaireString.isVide(nomCacheAVider)) {
      throw new SOFIException("Vous devez spécifier le nom de la cache à vider");
    }

    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application ");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application ");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    // Vider la cache demandée
    if (nomCacheAVider.equals(CACHE_AIDE)) {
      GestionAide.getInstance().setIsViderCache(true);

      Iterator it = GestionCache.getInstance().getCache("aideEnLigne")
          .iterateurCles();

      while (it.hasNext()) {
        it.next();
        it.remove();
      }
    } else if (nomCacheAVider.equals(CACHE_LIBELLE)) {
      GestionLibelle.getInstance().initialiserCache();
    } else if (nomCacheAVider.equals(CACHE_MESSAGE)) {
      GestionMessage.getInstance().initialiserCache();
    } else if (nomCacheAVider.equals(CACHE_DOMAINE_VALEUR)) {
      GestionDomaineValeur.getInstance().setIsViderCache(true);
    } else {
      throw new SOFIException(
          "La cache demandée n'est pas un nom valide de cache de SOFI.");
    }

    response.setContentType("text/plain");
    response.getWriter().println("OK");

    return null;
  }

  /**
   * Recharger un objet cache. On doit passer le nom de l'objet cache, l'utilisateur gestionnaire de l'application
   * et le mot de passe de l'utilisateur.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward rechargerObjetCache(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    //Obtention de la cache à recharger
    String nomObjetCache = request.getParameter("nomObjetCache");

    if (UtilitaireString.isVide(nomObjetCache)) {
      throw new SOFIException(
          "Vous devez spécifier le nom de l'objet cache à recharger");
    }

    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    GestionCache gestionCache = GestionCache.getInstance();

    if (gestionCache.isObjetCacheExiste(nomObjetCache)) {
      gestionCache.rechargerCache(nomObjetCache);

      //ObjetCache objetCache = gestionCache.getCache(nomObjetCache);
      //objetCache.chargerDonnees();
    } else {
      throw new SOFIException(
          "Cette cache n'existe pas et ne peut donc pas être rechargée.");
    }

    response.setContentType("text/plain");
    response.getWriter().println("OK");

    return null;
  }

  /**
   * Mettre à jour un paramètre système. On doit passer le code du paramètre, l'utilisateur gestionnaire de l'application
   * et le mot de passe de l'utilisateur.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward mettreAJourParametreSysteme(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    //Obtention du paramètres à mettre à jour
    String codeParametre = request.getParameter("codeParametre");

    if (UtilitaireString.isVide(codeParametre)) {
      throw new SOFIException(
          "Vous devez spécifier le code du paramètre système a mettre à jour");
    }

    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    GestionParametreSysteme gestionParametre = GestionParametreSysteme.getInstance();

    if (gestionParametre.getParametreSystemeEJB(codeParametre) != null) {
      gestionParametre.supprimerParametreSysteme(codeParametre);
    } else {
      throw new SOFIException(
          "Ce paramètre système n'existe pas et ne peut donc pas être mis à jour.");
    }

    response.setContentType("text/plain");
    response.getWriter().println("OK");

    return null;
  }

  /**
   * Mettre à jour un libellé. On doit passer le code du libellé, l'utilisateur gestionnaire de l'application
   * et le mot de passe de l'utilisateur.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward mettreAJourLibelle(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    // Obtention de la clé du libellé à mettre à jour
    String cleLibelle = request.getParameter("cleLibelle");

    if (UtilitaireString.isVide(cleLibelle)) {
      throw new SOFIException(
          "Vous devez spécifier la clé du libellé a mettre à jour");
    }

    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    GestionLibelle.getInstance().supprimer(cleLibelle);

    response.setContentType("text/plain");
    response.getWriter().println("OK");

    return null;
  }

  /**
   * Mettre à jour un message. On doit passer la clé du message, l'utilisateur gestionnaire de l'application
   * et le mot de passe de l'utilisateur.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward mettreAJourMessage(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    // Obtention de la clé du message à mettre à jour
    String cleMessage = request.getParameter("cleMessage");

    if (UtilitaireString.isVide(cleMessage)) {
      throw new SOFIException(
          "Vous devez spécifier la clé du message a mettre à jour");
    }

    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    GestionMessage.getInstance().supprimer(cleMessage);

    response.setContentType("text/plain");
    response.getWriter().println("OK");

    return null;
  }

  /**
   * Obtenir la liste des objets caches configurés dans l'application. On doit passer le code d'usager de gestion
   * et le mot de passe de l'usager.
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte contenant la liste des caches séparés par des "|" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward obtenirListeCaches(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    //Obtention du nom de l'utilisateur de gestion de l'application
    String usagerGestion = request.getParameter("usagerGestion");

    if (UtilitaireString.isVide(usagerGestion)) {
      throw new SOFIException(
          "Vous devez fournir le nom de l'utilisateur de gestion de l'application");
    }

    //Obtention du mot de passe de l'utilisateur de gestion de l'application
    String motPasse = request.getParameter("motPasse");

    if (UtilitaireString.isVide(motPasse)) {
      throw new SOFIException(
          "Vous devez fournir le mot de passe de l'utilisateur de gestion de l'application");
    }

    //Valider le mot de passe et le user de gestion
    this.validerCodeUtilisateurMotPasseAdmin(usagerGestion, motPasse);

    GestionCache gestionCache = GestionCache.getInstance();

    if (gestionCache.isObjetCacheActif()) {
      response.setContentType("text/plain");

      Iterator iterateurCaches = gestionCache.getListeCachesConfigurees()
          .iterator();
      int i = 0;
      String reponse = "";

      while (iterateurCaches.hasNext()) {
        String nomCache = (String) iterateurCaches.next();

        if (i != 0) {
          reponse = reponse + "|";
        }

        reponse = reponse + nomCache;
        i++;
      }

      response.getWriter().println(reponse);

      return null;
    } else {
      throw new SOFIException(
          "Le gestionnaire de la cache n'est pas activé dans l'application.");
    }
  }

  /**
   * Chacher le code d'utilisateur de test sur le poste du développeur. Disponible seulement
   * si le paramètre systèmes codeUtilisateurTest est défini dans le fichier de paramètres.
   * On doit fournir le nouveau codeUtilisateurTest. Retourne à un mapping "accueil" qui doit être
   * défini dans le struts-config.xml.
   *
   * @param response l'objet réponse http
   * @param request l'objet requête http
   * @param form le formulaire (vide dans notre cas)
   * @param mapping le mapping struts
   * @return une réponse texte, "OK" en cas de réussite, sinon lance une exception
   * @throws java.lang.Exception En cas de problème
   */
  public ActionForward changerUtilisateur(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    GestionParametreSysteme parametres = GestionParametreSysteme.getInstance();

    if (!UtilitaireString.isVide(parametres.getString("codeUtilisateurTest"))) {
      String code = request.getParameter("codeUtilisateur");

      if (code != null) {
        parametres.ajouterParametreSysteme("codeUtilisateurTest", code);
      }

      request.getSession().invalidate();

      return mapping.findForward("accueil");
    } else {
      throw new SOFIException(
          "L'utilisateur peut seulement être changé en mode test.");
    }
  }

  /**
   * Valider le code d'utilisateur et le mot de passe de gestion avec le service application
   * @param codeUtilisateurAdmin le code de l'utilisateur de gestion
   * @param motPasse le mot de passe de l'utilisateur de gestion
   * @return vrai si la validation a réussie, sinon lance une SOFIException
   * @throws java.lang.Exception En cas de problème de validation
   */
  private boolean validerCodeUtilisateurMotPasseAdmin(
      String codeUtilisateurAdmin, String motPasse) throws Exception {
    //Obtenir l'application via le service application
    ServiceApplication serviceApplication = GestionSecurite.getInstance().getServiceApplicationLocal();

    if (serviceApplication == null) {
      throw new SOFIException("Le service application n'est pas disponible.");
    }

    Application application = serviceApplication.getApplication(GestionSecurite.getInstance()
        .getCodeApplication());

    if (application == null) {
      throw new SOFIException(
          "Le système n'est pas défini dans la console de gestion SOFI.");
    } else {
      if (codeUtilisateurAdmin.equals(application.getCodeUtilisateurAdmin()) &&
          motPasse.equals(application.getMotPasseAdmin())) {
        return true;
      } else {
        throw new SOFIException(
            "Le code d'utilisateur et le mot de passe administrateur de l'application sont invalides.");
      }
    }
  }
}
