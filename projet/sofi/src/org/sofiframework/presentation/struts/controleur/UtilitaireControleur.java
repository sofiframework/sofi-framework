package org.sofiframework.presentation.struts.controleur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.util.LabelValueBean;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Utilitaire sur l'accès au contrôleur.
 * <p>
 * Basé sur
 * <code>org.sofiframework.presentation.utilitaire.UtilitaireControleur</code>.
 * 
 * @author jfbrassard
 *
 */

public class UtilitaireControleur extends org.sofiframework.presentation.utilitaire.UtilitaireControleur {

	/**
	 * Méthode servant à transformer une liste d'objets en liste de
	 * LabelValueBean.
	 * <p>
	 * Cette méthode utilise l'introspection pour retourner une liste d'objets
	 * de type LabelValueBean. Ainsi le type d'objets contenu dans la liste
	 * passé en paramètres est sans importance.<BR>
	 * <B>Note :</B> Il est important pour que la méthode fonctionne
	 * correctement que les "getters" pour les attributs passé en paramètres ne
	 * nécessitent aucun paramètre. De plus il faut que le nom des attributs
	 * passés en paramètres soit identique au nom de l'attribut dans l'objet.
	 * <p>
	 * 
	 * @param listeItems
	 *            La liste contenant les objets pour lequel on veut faire une
	 *            liste de LabelValueBean
	 * @param attributLabel
	 *            Le nom de l'attribut dans l'objet qui sera transféré en Label
	 * @param attributValue
	 *            Le nom de l'attribut dans l'objet qui sera transféré en Value
	 * @param ajouterValeurVide
	 *            true si on veut tenir compte des valeurs vide du style
	 *            LabelValueBean("", ""), false sinon
	 * @return ArrayList La liste qui contient tous les objets de type
	 *         LabelValueBean
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList convertirEnListeDeroulante(ArrayList listeItems, String attributLabel, String attributValue,
			boolean ajouterValeurVide) {
		ArrayList listeLabelValueBeans = new ArrayList();

		try {
			if (ajouterValeurVide) {
				listeLabelValueBeans.add(new LabelValueBean("", ""));
			}

			Iterator iterateur = listeItems.iterator();

			while (iterateur.hasNext()) {
				// Prendre l'objet
				Object item = iterateur.next();

				// Trouver l'attribut Label dans l'objet
				Object valeurLabel = PropertyUtils.getProperty(item, attributLabel);
				Object valeurValue = PropertyUtils.getProperty(item, attributValue);

				listeLabelValueBeans.add(new LabelValueBean(valeurLabel.toString(), valeurValue.toString()));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return listeLabelValueBeans;
	}

	/**
	 * Le nom de l'adresse de la l'action courante (action + methode).
	 * 
	 * @param request
	 *            la requête en traitement.
	 * @return l'adresse de la l'action courante (action + methode).
	 */
	public static String getAdresseActionCourante(HttpServletRequest request) {
		StringBuffer actionComplete = new StringBuffer();
		String action = getNomActionCourant(request);
		actionComplete.append(action);
		actionComplete.append(".do");
		actionComplete.append("?");

		String methodeParam = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.METHODE_PARAM);

		if (StringUtils.isEmpty(methodeParam)) {
			methodeParam = "methode";
		}
		actionComplete.append(methodeParam);

		String methode = request.getParameter(methodeParam);

		if (methode != null) {
			actionComplete.append("=");

			actionComplete.append(methode);
		}

		return actionComplete.toString();
	}

	/**
	 * Retourne les paramètres onglets pour l'action courante.
	 * 
	 * @return les paramètres onglets pour l'action courante.
	 * @param request
	 *            la requête présentement en cours.
	 */
	public static HashMap getParametreOngletPourAction(HttpServletRequest request) {
		// Permet de traiter les paramètres à un onglet.
		HashMap repertoireParametreOnglet = (HashMap) request.getSession().getAttribute("parametresUrlOnglet");

		// Action courante;
		String actionCourante = UtilitaireControleur.getAdresseActionCourante(request);

		HashMap parametreOnglet = null;

		if (repertoireParametreOnglet != null) {
			parametreOnglet = (HashMap) repertoireParametreOnglet.get(actionCourante);
		}

		return parametreOnglet;
	}

	/**
	 * Retourne le formulaire courant
	 * 
	 * @return le formulaire courant.
	 * @param request
	 */
	public static BaseForm getFormulaireCourant(HttpServletRequest request) {
		String nomFormulaire = (String) request.getSession().getAttribute(Constantes.FORMULAIRE);

		if (nomFormulaire != null) {
			return (BaseForm) request.getSession().getAttribute(nomFormulaire);
		} else {
			return null;
		}
	}

}
