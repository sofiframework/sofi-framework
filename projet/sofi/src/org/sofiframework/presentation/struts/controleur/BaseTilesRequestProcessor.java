/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.InvalidCancelException;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.tiles.TilesRequestProcessor;
import org.sofiframework.application.aide.GestionAide;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.presentation.filtre.FiltreDiagnostic;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe d'un RequestProcessor de base qui permet le traitement des Tiles.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.apache.struts.tiles.TilesRequestProcessor
 */
public class BaseTilesRequestProcessor extends TilesRequestProcessor {
  /** Variable désignant l'instance commune de journalisation */
  private static final Log log = LogFactory
      .getLog(BaseTilesRequestProcessor.class);

  /**
   * Variable désignant le nom du chemin par défaut pour accéder au site de
   * surveillance du site actuel
   */
  public static final String INFO_ACTIVITE_SITE_URL = "/infoSite";

  /**
   * Méthode appelée avant chaque exécution de requête.
   * <p>
   * Cette méthode sert à prendre le module d'application (en appelant la
   * méthode getModele) et à le mettre dans la request de la requête. Ainsi on
   * s'assure que le module d'application est toujours disponible tout au long
   * du traitement de la requête.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @return cette méthode retourne toujours la valeur "true" ce qui veut dire
   *         que l'exécution de la requête doit continuer
   */
  @Override
  protected boolean processPreprocess(HttpServletRequest request,
      HttpServletResponse response) {
    // Spécifier le type de navigateur
    RequestProcessorHelper.specfifierTypeNavigateur(request);

    // Traiter le fichier d'information de la version du déploiement.
    RequestProcessorHelper.traiterFichierInfoVersion(getServletContext());

    // Accéder au modèle de l'application.
    Object modele = this.getModele(request, response);

    // Construire seulement si plug-in Struts de configuré et premier accès
    if ((GestionCache.getInstance().getDateDerniereMaj() != null)
        && !GestionCache.getInstance().isObjetCacheActif()) {
      genererObjetsCache(modele);
    }

    // Générer le référentiel des objets Java
    GestionSecurite.genererReferentiel();

    // Placer l'Application Module dans le request.
    request.setAttribute(Constantes.MODELE, modele);

    boolean continuerProcessus = true;

    try {
      // Identification de l'url demandé.
      String url = processPath(request, response).substring(1);

      if ((GestionAide.getInstance().getUrlAide() != null)
          && (GestionAide.getInstance().getUrlAide().indexOf(url) != -1)) {
        // Si l'appel demandé est de l'aide en ligne.
        continuerProcessus = false;

        // Générer l'aide en ligne demandé par l'utilisateur.
        UtilitaireVelocity utilitaireVelocity = GestionAide.getInstance()
            .preparerAide(request);
        traitementSpecifiqueAide(utilitaireVelocity, request);
        GestionAide.getInstance().ecrireReponseAide(utilitaireVelocity,
            response);
      }
    } catch (IOException e) {
      log.error("Erreur pour ecrire l'aide en ligne.", e);
    }

    return continuerProcessus;
  }

  /**
   * Méthode à surcharger pour spécifier des attributs spécifiques à l'aide.
   * 
   * @param utilitaireVelocity
   *          l'utilitaire Velocity
   * @param request
   *          la requête en traitement.
   */
  protected void traitementSpecifiqueAide(
      UtilitaireVelocity utilitaireVelocity, HttpServletRequest request) {

  }

  protected void traitementAvantPreparationOnglets(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
  }

  protected void traitementApresPreparationOnglets(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
  }

  @Override
  public void process(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    // Wrap multipart requests with a special wrapper
    request = processMultipart(request);

    // Identify the path component we will use to select a mapping
    String path = processPath(request, response);

    if (path == null) {
      return;
    }

    if (log.isDebugEnabled()) {
      log.debug("Processing a '" + request.getMethod() + "' for path '" + path
          + "'");
    }

    if ("POST".equals(request.getMethod())
        && (request.getSession().getAttribute("SOFI_ACTION_POST") == null)) {
      request.getSession().setAttribute("SOFI_ACTION_POST", Boolean.TRUE);
    }

    // Select a Locale for the current user if requested
    processLocale(request, response);

    // Set the content type and no-caching headers if requested
    processContent(request, response);
    processNoCache(request, response);

    // General purpose preprocessing hook
    if (!processPreprocess(request, response)) {
      return;
    }

    this.processCachedMessages(request, response);

    // Identify the mapping for this request
    ActionMapping mapping = processMapping(request, response, path);

    if (mapping == null) {
      return;
    }

    // Check for any role required to perform this action
    if (!processRoles(request, response, mapping)) {
      return;
    }

    // Process any ActionForm bean related to this request
    ActionForm form = processActionForm(request, response, mapping);
    processPopulate(request, response, form, mapping);

    // Validate any fields of the ActionForm bean, if applicable
    try {
      if (!processValidate(request, response, form, mapping)) {
        return;
      }
    } catch (IOException e) {
      throw e;
    } catch (ServletException e) {
      throw e;
    }

    // Process a forward or include specified by this mapping
    if (!processForward(request, response, mapping)) {
      return;
    }

    if (!processInclude(request, response, mapping)) {
      return;
    }

    // Create or acquire the Action instance to process this request
    Action action = processActionCreate(request, response, mapping);

    if (action == null) {
      return;
    }

    // Call the Action instance itself
    ActionForward forward = processActionPerform(request, response, action,
        form, mapping);

    // Process the returned ActionForward instance
    processForwardConfig(request, response, forward);
  }

  /**
   * Cette méthode s'exécute avant tout appel de controleur (Action.class).
   * <p>
   * Cette méthode se charge aussi de capter les erreurs java dans un controleur
   * et de l'inscrire dans un journal. Ainsi si une erreur survient, la page
   * d'erreur correspondant à l'exception attrapée est lancée.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param action
   *          L'action à lancer pour exécuter le controleur
   * @param form
   *          Le formulaire associé à la requête
   * @param mapping
   *          L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return la redirection vers la page à visiter une fois le traitement
   *         terminé
   * @throws ServletException
   *           Exception générique
   * @throws IOException
   *           Exception génériquere
   */
  @Override
  protected ActionForward processActionPerform(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException {
    // Permet de traiter une requête provenant de la persistance d'un DIV
    // afin de générer son propre contenu.
    boolean fermerSection = RequestProcessorHelper
        .appliquerPersistanceDiv(request);

    // Accéder au paramètre système.
    boolean traiterFermetureNonAutomatique = request
        .getParameter(ConstantesBaliseJSP.AJAX_ECLATABLE_FERMETURE_NON_AUTOMATIQUE) != null;

    if (fermerSection && !traiterFermetureNonAutomatique) {
      String reponseVide = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody></tbody></table>";
      response.getWriter().write(reponseVide);

      return null;
    }

    // Accéder à la surveillance.
    GestionSurveillance surveillance = GestionSurveillance.getInstance();

    // Spécifier l'ancienne action.
    String ancienneAction = (String) request.getSession().getAttribute(
        Constantes.ADRESSE_ACTION_AFFICHE);

    // Spcéfier l'action courante.
    String actionCourante = null;

    if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .isAppelAjax(request)) {
      // Spécifier l'action courante seulement si le traitement n'est pas ajax,
      // sinon
      // conserver l'ancienne action.
      actionCourante = RequestProcessorHelper.getAdresseActionCourante(request,
          mapping);
    } else {
      actionCourante = ancienneAction;
    }

    // Le service avant traitement.
    ObjetSecurisable serviceDebut = UtilitaireSession
        .getServiceAJournaliser(request.getSession());

    // Fixer le composant d'interface en traitement
    RequestProcessorHelper.fixerComposantReferentielEnTraitement(request,
        mapping);

    // Conserver les paramètres de la liste de valeur, s'il y a lieu.
    conserverParametresListeValeur(request);

		String actionAuthentification = getUrlAuthentification(request);

    if (GestionSecurite.isApplicationGrandPublic()) {
      org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .getUtilisateur(request.getSession());
    }

    // Initialisation des attributs temporaires en traitement durant la requête.
    HashSet listeAttributTemporaire = (HashSet) request.getSession()
        .getAttribute(Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT);

    if (listeAttributTemporaire == null) {
      request.getSession().setAttribute(
          Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT, new HashSet());
    }

    // Traitement lorsque la session est terminé.
    // ----------------------------------------------------------------------------------------------------------------
    if ((request.getSession().getAttribute(Constantes.UTILISATEUR) == null)
        && (actionAuthentification != null)) {
      if (!actionAuthentification.startsWith("/")) {
        actionAuthentification = "/" + actionAuthentification;
        GestionParametreSysteme.getInstance().supprimerParametreSysteme(
            ConstantesParametreSysteme.URL_AUTHENTIFICATION);
        GestionParametreSysteme.getInstance().ajouterParametreSysteme(
            ConstantesParametreSysteme.URL_AUTHENTIFICATION,
            actionAuthentification);
      }

      if (!UtilitaireString.isVide(actionAuthentification)) {
        if (!actionCourante.equals(actionAuthentification)
            && org.sofiframework.presentation.utilitaire.UtilitaireControleur
                .isActionSecurise(request)) {
          ActionForward authentification = new ActionForward(
              actionAuthentification);

          if (log.isInfoEnabled()) {
            log.info("Session terminé. Retour a la page d'authentification");
          }

          // Libérer le modèle.
          libererModele(request, response);

          return authentification;
        }
      }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Accéder à l'utilisateur en cours.
    Utilisateur utilisateur = org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .getUtilisateur(request.getSession());

    if (utilisateur != null) {
      if (log.isDebugEnabled()) {
        log.debug("Code utilisateur en traitement : "
            + utilisateur.getCodeUtilisateur());
      }
    }

    try {
      // Appel du traitement spécifique du contrôleur avant chacun des actions.
      ActionForward forward = traitementAvantAction(request, response, action,
          form, mapping);

      // La dernière vue sélectionné par l'utilisateur.
      ActionForward derniereVue = (ActionForward) request.getSession()
          .getAttribute("derniereVue");

      boolean ignorerRetour = false;

      if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .isAppelAjax(request)
          && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
              .isRafraichissementAjax(request)) {
        if (request.getParameter("SOFIignorerRetour") != null) {
          ignorerRetour = true;
          request.getSession().setAttribute("SOFIignorerRetour", Boolean.TRUE);
        } else {
          Href avantDernierHrefAvecParametres = (Href) request
              .getSession()
              .getAttribute(Constantes.DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE);

          // Loger l'avant dernier href avec paramètres sélectionné.
          request.getSession().setAttribute(
              Constantes.AVANT_DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE,
              avantDernierHrefAvecParametres);

          // Spécifier la dernière adresse avec paramètres sélectionné.
          Href href = new Href(request.getRequestURL().toString(), response);
          href.ajouterListeParametres(request.getParameterMap());

          request.getSession().setAttribute(
              Constantes.DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE, href);
        }
      }

      // Traitement seulement si le forward existe et qu'il n'est pas un
      // redirect.
      if ((derniereVue == null)
          || (!derniereVue.getRedirect()
              && !request.getMethod().equals("POST")
              && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
                  .isAppelAjax(request) && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
                .isRafraichissementAjax(request))) {
        // Spécifier l'action courante, si la dernière vue afficher à
        // l'utilisateur n'est pas un redirect et action existe dans referentiel
        // SOFI.
        ObjetSecurisable actionSelectionne = GestionSecurite.getInstance()
            .getObjetSecurisableParAdresse(actionCourante);

        if (actionSelectionne != null) {
          request.setAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE,
              actionCourante);
        } else {
          if (request.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE) != null) {
            actionCourante = request.getAttribute(
                Constantes.ADRESSE_ACTION_SELECTIONNE).toString();
          }
        }
      }

      if (utilisateur != null) {

        // Permet d'effectuer du traitement personnalisé avant le traitement de
        // la sécurité.
        traitementAvantSecurite(serviceDebut, request, response, form, mapping);

        // Traiter sécurité sur adresse sélectionné
        RequestProcessorHelper.appliquerSecuritePourNavigation(utilisateur,
            request, (BaseForm) form, mapping);

        // Traitement si nouvelle authentification
        RequestProcessorHelper.appliquerNouvelleAuthentification(request,
            mapping, getServletContext());
      }

      // L'action est traité seulement si le forward est égale a null
      if (forward == null) {
        forward = action.execute(mapping, form, request, response);
      }

      ActionForward forwardApresAction = traitementApresAction(request,
          response, action, form, mapping);

      // Traitement si nouvelle authentification
      if (org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .isNouvelleAuthentification(request)
          && GestionParametreSysteme.getInstance()
              .getBoolean(ConstantesParametreSysteme.APPLICATION_GRAND_PUBLIC)
              .booleanValue()) {
        // Dans le cas que l'authentification ne se fait pas via le
        // FiltreAuthentification.
        RequestProcessorHelper.appliquerNouvelleAuthentification(request,
            mapping, getServletContext());
      }

      // L'action est traité seulement si le forward est égale a null
      if (forwardApresAction != null) {
        forward = forwardApresAction;
      }

      if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .isNouvelleAuthentification(request) && (utilisateur == null)) {
        // Indique que la session vient d'être fermé
        GestionSurveillance.fermerSession(request);
      }

      // Spécifier que la nouvelle authentification a été complété.
      if (org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .isNouvelleAuthentification(request)) {
        try {
          GestionSurveillance.ajouterUtilisateurDansSurveillance(utilisateur,
              request);
        } catch (Exception e) {
        }

        org.sofiframework.presentation.utilitaire.UtilitaireControleur
            .terminerNouvelleAuthentification(request);
      }

      // Indique si la navigation est présentement dans un service appelé par un
      // raccourci.
      String traitementServiceRaccourci = (String) request.getSession()
          .getAttribute("traitementServiceRaccourci");

      // Valider si il y a eu un changement de service, si oui initialiser le
      // traitement du service raccourci.
      if ((traitementServiceRaccourci != null)
          && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
              .isAppelAjax(request)
          && (request.getParameter("sofi_memoriser_href_actuel") == null)) {
        if ((forward != null) && !forward.getRedirect()) {
          if (request.getSession().getAttribute("traitementServiceRaccourci")
              .equals("Action")) {
            RequestProcessorHelper.suppressionAttributsTemporairePourUneAction(
                ancienneAction, actionCourante, request, mapping);
          } else {
            RequestProcessorHelper.suppressionAttributsTemporairePourUnService(
                ancienneAction, actionCourante, request, mapping);
          }
        }

        traitementServiceRaccourci = (String) request.getSession()
            .getAttribute("traitementServiceRaccourci");

        // Si appel d'un service via un raccourci alors initialiser que le
        // traitement dans le
        // raccourci n'est pas en cours.
        if ((request.getParameter("raccourciAction") != null)
            || (request.getParameter("raccourciService") != null)) {
          traitementServiceRaccourci = null;
        }
      }

      // Indique si on appliquer le détail d'un service.
      boolean detailPourService = (forward != null)
          && ((forward.getPath().indexOf(".do") == -1) || ((request
              .getParameter("raccourciAction") != null) || (request
              .getParameter("raccourciService") != null)));

      if ((utilisateur != null) && detailPourService) {
        // Appeler la page directement si la surveillance du site est demandé
        if (surveillance.isActif()
            && (surveillance.getPathSiteSurveillance() != null)
            && (mapping.getPath().indexOf(
                surveillance.getPathSiteSurveillance()) == -1)
            && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
                .isRafraichissementAjax(request)) {
          // incrémenter une nouvelle page consulté.
          surveillance.ajouterUnePageConsulte();
        }

        if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
            .isAppelAjax(request)) {
          ObjetSecurisable serviceSelectionne = GestionSecurite.getInstance()
              .getObjetSecurisableParAdresse(actionCourante);

          if (serviceSelectionne == null) {
            serviceSelectionne = GestionSecurite.getInstance()
                .getObjetSecurisableParAdresse(
                    RequestProcessorHelper.getUrlCourant(request, mapping));
          }

          // Si l'url est dans le référentiel de SOFI, il faut spécifier la
          // navigation.
          if (serviceSelectionne != null) {
            // Ajuster le référentiel de l'utilisateur avec l'ajout, si
            // applicable, de nouveau composant(s) d'interface(s).
            RequestProcessorHelper.modificationReferentielUtiliateur(
                utilisateur, request);

            // Méthode qui doit être surchargé afin de préparer le traitement
            // des onglets.
            traitementAvantPreparationOnglets(request, response, action, form,
                mapping);

            // Spécifier le service sélectionnée.
            RequestProcessorHelper.specifierDetailPourService(
                traitementServiceRaccourci != null, request, mapping);

            // Méthode qui doit être surchargé afin de modifier le traitement
            // des onglets.
            traitementApresPreparationOnglets(request, response, action, form,
                mapping);
          }
        }
      }

      boolean sofi_memoriser_href_actuel = request
          .getParameter("sofi_memoriser_href_actuel") != null;

      boolean sofi_lien_retour = request.getParameter("sofi_lien_retour") != null;

      if (sofi_memoriser_href_actuel || sofi_lien_retour) {
        request.getSession().setAttribute("sofi_inter_service", Boolean.TRUE);
      }

      // Conserver un indicateur qui spécifie le début d'un traitement d'un
      // service appelé via
      // un raccourci.
      if ((request.getParameter("raccourciAction") != null)
          || (request.getParameter("raccourciService") != null)) {
        if (request.getParameter("raccourciAction") != null) {
          // Par défaut le raccourci est conservé pour l'action en cours.
          org.sofiframework.presentation.utilitaire.UtilitaireControleur
              .setAttributTemporairePourAction("traitementServiceRaccourci",
                  "Action", request);
        } else {
          org.sofiframework.presentation.utilitaire.UtilitaireControleur
              .setAttributTemporairePourService("traitementServiceRaccourci",
                  "Service", request);
        }
      }

      ObjetSecurisable serviceFin = UtilitaireSession
          .getServiceAJournaliser(request.getSession());

      if (serviceDebut == null) {
        // Appel de la journalisation lors de la première entrée dans
        // l'application
        if (serviceFin != null) {
          log.info("Accès au premier service no: "
              + serviceFin.getSeqObjetSecurisable());
          // Traitement seulement si la journalisation sur les services est
          // active.
          if (isJournalisationActive()) {

            journaliserDebutService(serviceFin, utilisateur,
                request.getSession());
          }
        } else {
          log.info("Il n'y a pas de service à journaliser");
        }
      }

      if ((serviceDebut != null)
          && (serviceFin != null)
          && (serviceDebut.getSeqObjetSecurisable().longValue() != serviceFin
              .getSeqObjetSecurisable().longValue())) {
        // Appel de la journalisation lors d'un changement de service.
        log.info("Changement de service de "
            + serviceDebut.getSeqObjetSecurisable() + " au "
            + serviceFin.getSeqObjetSecurisable());

        // Traitement seulement si la journalisation sur les services est
        // active.
        if (isJournalisationActive()) {
          journaliserFinService(serviceDebut, utilisateur, request.getSession());
        }

        RequestProcessorHelper.suppressionAttributsTemporairePourUneAction(
            ancienneAction, actionCourante, request, mapping);

        // Traitement personnalisé a traiter lors d'un changement de service.
        traitementChangementService(request, response, form, mapping);

        // Traitement seulement si la journalisation sur les services est
        // active.
        if (isJournalisationActive()) {
          journaliserDebutService(serviceFin, utilisateur, request.getSession());
        }
      }

      if (forward != null) {
        // Appeler la vue demandé et la garder en mémoire comme dernière
        // sélectionné.
        request.getSession().setAttribute("derniereVue", forward);

        // Traitement seulement si le forward n'est pas un redirect.
        if ((forward != null) && !forward.getRedirect()) {
          if (request.getSession().getAttribute("SOFI_ACTION_POST") != null) {
            // Initialisation de l'indicateur de POST.
            request.getSession().setAttribute("SOFI_ACTION_POST", null);
            request.setAttribute("SOFI_ACTION_POST", Boolean.TRUE);
          }
        }

        // Spécifier l'action sélectionner par un utilisateur avant redirect.
        if ((forward.getPath() != null)
            && (forward.getPath().indexOf(".page") == -1)) {
          // Fixer l'action qui est appelé avant le redirect vers la page
          // d'affichage.
          request.getSession().setAttribute(
              Constantes.ADRESSE_ACTION_SELECTIONNE_AVANT_REDIRECT,
              actionCourante);
        }
      }

      boolean parametreUrlRetour = request.getParameter("sofi_url_retour") != null;

      if (sofi_memoriser_href_actuel) {
        Href urlDerniermentSelectionne = (Href) request.getSession()
            .getAttribute(
                Constantes.AVANT_DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE);

        if (urlDerniermentSelectionne != null) {
          if (!parametreUrlRetour) {
            request.getSession().setAttribute("sofi_memoriser_href_actuel",
                urlDerniermentSelectionne.getUrlPourRedirection());
          }
        }

        if (parametreUrlRetour) {
          String urlRetour = request.getParameter("sofi_url_retour");
          request.getSession().setAttribute("sofi_memoriser_href_actuel",
              urlRetour);
        }
      }

      // Spécifier l'action affiché à l'utilisateur.
      if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
          .isAppelAjax(request)
          && !org.sofiframework.presentation.utilitaire.UtilitaireControleur
              .isRafraichissementAjax(request)
          && (forward != null)
          && (forward.getPath() != null)
          && (forward.getPath().indexOf(".page") != -1)) {
        // Supprimer tous les attributs qui ont logé temporairement pour une
        // action ou un service.
        RequestProcessorHelper.suppressionAttributsTemporairePourUneAction(
            ancienneAction, actionCourante, request, mapping);

        if (request.getSession().getAttribute("sofi_memoriser_href_actuel") != null) {
          String actionRetour = (String) request.getSession().getAttribute(
              Constantes.ADRESSE_ACTION_RETOUR);
          String actionAfficher = (String) request.getSession().getAttribute(
              Constantes.ADRESSE_ACTION_AFFICHE);

          if ((actionRetour == null) && (ancienneAction != null)) {
            actionRetour = ancienneAction;
          }

          // Mémoriser l'ancienne action
          if (!ignorerRetour
              && parametreUrlRetour
              || ((actionRetour == null || (actionRetour != null
                  && !actionRetour.equals(actionAfficher.substring(1)) && (actionCourante != null && !actionCourante
                  .equals(actionAfficher)))))) {
            org.sofiframework.presentation.utilitaire.UtilitaireControleur
                .setAttributTemporairePourService(
                    Constantes.ADRESSE_ACTION_RETOUR, request.getSession()
                        .getAttribute("sofi_memoriser_href_actuel"), request);
          }
        } else {
          /*
           * Si le retour est en cours on aura plus besoin de l'adresse de
           * l'action de retour.
           */
          boolean retourEnCours = request.getParameter("sofi_lien_retour") != null;
          if (retourEnCours) {
            request.getSession().removeAttribute(
                Constantes.ADRESSE_ACTION_RETOUR);
            request.getSession().removeAttribute(
                Constantes.ADRESSE_ACTION_AFFICHE);
            ancienneAction = null;
          }

          // Supprimer tous les attributs qui ont logé temporairement pour un
          // service.
          RequestProcessorHelper.suppressionAttributsTemporairePourUnService(
              ancienneAction, actionCourante, request, mapping);
        }

        request.getSession().setAttribute(Constantes.ADRESSE_ACTION_AFFICHE,
            actionCourante);

        // Initialiser la liste des attributs temporaires en traitement.
        request.getSession().setAttribute(
            Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT, new HashSet());

        // Initialiser les variables temporaires.
        request.getSession().setAttribute("SOFIignorerRetour", null);
        request.getSession().setAttribute("sofi_memoriser_href_actuel", null);
        request.getSession().setAttribute("sofi_inter_service", null);
      }

      // Fixer le formulaire en cours dans la request.
      request.getSession().setAttribute(Constantes.FORMULAIRE_EN_COURS, form);

      // Libérer le modèle
      libererModele(request, response);

      // Extraire un action forward secure au besoin.
      ForwardConfig forwardConfig = getSecureActionForward(forward, request);
      if (forwardConfig != null) {
        forward = new ActionForward(forwardConfig.getPath(),
            forwardConfig.getRedirect());
      }

      return forward;
    } catch (Exception e) {
      return processException(request, response, e, form, mapping);
    }
  }

  @Override
  protected ActionForward processException(HttpServletRequest request,
      HttpServletResponse response, Exception exception, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException {
    log.error(exception, exception);
    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    // Traiter l'exception.
    try {
      if (GestionSurveillance.getInstance().isActif()) {
        StringWriter writer = new StringWriter();
        PrintWriter prn = new PrintWriter(writer);
        exception.printStackTrace(prn);

        if (FiltreDiagnostic.getDiagnostic() != null) {
          prn.println();
          prn.println();
          prn.print(FiltreDiagnostic.getDiagnostic().getMessages());
        }

        prn.flush();

        String messageErreur = writer.toString();

        GestionSurveillance.ajouterPageEnEchec(request, mapping, messageErreur);
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error(
            "Erreur lors de l'ajout d'une exception dans la surveillance.", e);
      }
    }

    request.setAttribute(Constantes.MESSAGE_ERREUR, exception);

    libererModele(request, response);

    return super.processException(request, response, exception, form, mapping);
  }

  /**
   * Cette méthode doit être surchargé pour faire un traitement spécifique.
   * <p>
   * Cette méthode, si elle est surchargé par un parent, peux spécifier un
   * traitement spécifique tout juste avant l'execution de l'action
   * (Action.class). Méthode qui est appelé par la méthode
   * processActionPerform(...);
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param action
   *          L'action à lancer pour exécuter le controleur
   * @param form
   *          Le formulaire associé à la requête
   * @param mapping
   *          L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return ActionForward retourne la vue qui doit être affiché s'il y a lieu,
   *         null si le traitement continue normalement
   * @throws ServletException
   *           Exception générique
   * @throws IOException
   *           Exception générique
   * @throws Exception
   *           Exception générique
   */
  protected ActionForward traitementAvantAction(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
    return null;
  }

  /**
   * Cette méthode doit être surchargé pour faire un traitement spécifique.
   * <p>
   * Cette méthode, si elle est surchargé par un parent, peux spécifier un
   * traitement spécifique tout juste après l'execution de l'action
   * (Action.class). Méthode qui est appelé par la méthode
   * processActionPerform(...);
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param action
   *          L'action à lancer pour exécuter le controleur
   * @param form
   *          Le formulaire associé à la requête
   * @param mapping
   *          L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return ActionForward retourne la vue qui doit être affiché s'il y a lieu,
   *         null si le traitement continue normalement
   * @throws ServletException
   *           Exception générique
   * @throws IOException
   *           Exception générique
   * @throws Exception
   *           Exception générique
   */
  protected ActionForward traitementApresAction(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
    return null;
  }

  /**
   * Méthode est traité avant l'appel de la méthode validate du formulaire.
   * <p>
   * Prend pour acquis que le chemin logique permettant à un utilisation
   * d'ouvrir une session correspond à "/identification" ou "/authentification".
   * Cette méthode appelle son équivalent dans RequestProcessor afin de
   * déterminer si une erreur est survenue lors du validate, dans ce cas false
   * est retourne par la méthode du parent et le modèle est libéré.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param mapping
   *          L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return boolean Valeur identifiant si la requête à été exécutée ou si il
   *         faut continuer l'exécution
   * @throws ServletException
   *           Exception générique
   * @throws IOException
   *           Exception générique
   */
  @Override
  protected boolean processValidate(HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping)
      throws IOException, ServletException {
    /*
     * ATTENTION
     * 
     * Fixer le nom du formulaire courant comme le dernier sélectionné. Est
     * essentiel pour permettre une redirection comme input de formulaire. Si
     * non on pert les erreurs car le formulaire est réinitilisé.
     */
    if (!org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .isAppelAjax(request)) {
      // Fixer le nom du formulaire courant comme le dernier sélectionné.
      request.getSession().setAttribute(Constantes.NOM_DERNIER_FORMULAIRE,
          mapping.getName());
    }

    String adresseCourante = UtilitaireControleur
        .getAdresseActionCourante(request);

    ObjetSecurisable serviceSelectionne = GestionSecurite.getInstance()
        .getObjetSecurisableParAdresse(adresseCourante);

    Object actionSelectionneRequete = request
        .getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE);

    if (serviceSelectionne != null || actionSelectionneRequete == null) {
      request.setAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE,
          adresseCourante);
    }

    String parametre = mapping.getParameter();

    if (parametre == null) {
      parametre = "";
    }

    if ("POST".equalsIgnoreCase(request.getMethod())
        && (request.getAttribute("formulaireValidateExecute") == null)) {
      if (!((mapping.getPath().equals("/identification") || mapping.getPath()
          .equals("/authentification")) && request.getParameter(
          mapping.getParameter()).equals("quitter"))) {
        boolean isFormulaireValide = false;

        try {
          isFormulaireValide = super.processValidate(request, response, form,
              mapping);

          // Was an input path (or forward) specified for this mapping?
          String input = mapping.getInput();

          if (!isFormulaireValide) {
            if (moduleConfig.getControllerConfig().getInputForward()) {
              ForwardConfig forward = mapping.findForward(input);

              forward = getSecureActionForward(forward, request);

              if (forward != null && forward.getPath().indexOf("https:") != -1 && UtilitaireControleur.isAppelAjax(request)) {
                processForwardConfig(request, response, forward);
              }

            }

          }
        } catch (InvalidCancelException e) {
        } catch (Exception e) {
          ActionForward fwd = processException(request, response, e, form,
              mapping);

          if (fwd != null) {
            processForwardConfig(request, response, fwd);
          }
        }

        request.setAttribute("formulaireValidateExecute", Boolean.TRUE);

        if (!isFormulaireValide) {
          // Libérer le modèle si une erreur est survenue dans un ActionForm
          libererModele(request, response);
        }

        return isFormulaireValide;
      } else {
        return true;
      }
    }

    return true;
  }

  @Override
  protected ActionMapping processMapping(HttpServletRequest request,
      HttpServletResponse response, String path) throws IOException {
    ActionMapping mapping = super.processMapping(request, response, path);

    if (mapping.getName() != null) {
      request.getSession().setAttribute(Constantes.FORMULAIRE,
          mapping.getName());
    }

    return mapping;
  }

  /**
   * Méthode qui sert à overwriter la méthode héritée du parent.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement.
   * @param response
   *          La réponse HTTP à fournir.
   * @return le modèle de l'application.
   */
  protected Object getModele(HttpServletRequest request,
      HttpServletResponse response) {
    return null;
  }

  /**
   * Libère le modèle.
   * <p>
   * Cette méthode libère le module d'application BC4J. Ce dernier restera
   * réservé pour l'utilisateur tant et aussi longtemps que la session de ce
   * dernier ne sera pas morte.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   */
  protected void libererModele(HttpServletRequest request,
      HttpServletResponse response) {
  }

  /**
   * Permet a un formulaire d'avoir des accents lorsque le formulaire contient
   * des attribut pour upload.
   * <p>
   * 
   * @param request
   *          La requête HTTP en traitement
   */
  @Override
  protected HttpServletRequest processMultipart(HttpServletRequest request) {
    return RequestProcessorHelper.processMultipart(request);
  }

  /**
   * Populer les propriétés de l'instance spécifique ActionForm depuis les
   * paramètres de la requête. En plus, l'attribut
   * <code>Globals.CANCEL_KEY</code> est fixé si la requête qui est soumitté
   * avec le bouton créé avec <code>CancelTag</code>.
   *
   * @param request
   *          La requête HTTP en traitement
   * @param response
   *          La réponse HTTP à fournir
   * @param form
   *          l'instance du formulaire ActionForm qui est a populer
   * @param mapping
   *          Le ActionMapping qui est utilisé
   *
   * @exception ServletException
   *              si une exception par RequestUtils.populate()
   */
  @Override
  protected void processPopulate(HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping)
      throws ServletException {
    RequestProcessorHelper.processPopulate(this.servlet, request, response,
        form, mapping);
  }

  /**
   * Elle permet de générer les objets cache.
   * <p>
   * 
   * @param serviceParam
   *          Le service local.
   */
  protected void genererObjetsCache(Object serviceParam) {
    synchronized (GestionCache.class) {
      try {
        GestionCache.getInstance().construire(serviceParam);
      } catch (Exception e) {
        log.error(e.toString(), e);
      }
    }
  }

  /**
   * Appel de la journalisation afin de signifier le début d'utilisateur à un
   * service.
   * <p>
   * Nécessite une ré-écriture.
   * 
   * @param service
   *          le service en cours.
   * @param utilisateur
   *          l'utilisateur courant.
   * @param session
   *          la session en traitement.
   */
  protected void journaliserDebutService(ObjetSecurisable service,
      Utilisateur utilisateur, HttpSession session) {
  }

  /**
   * Appel de la journalisation afin de signifier la fin d'utilisation d'un
   * service.
   * <p>
   * Nécessite une ré-écriture.
   * 
   * @param service
   *          le service en cours.
   * @param utilisateur
   *          l'utilisateur courant.
   * @param session
   *          la session en traitement.
   */
  protected void journaliserFinService(ObjetSecurisable service,
      Utilisateur utilisateur, HttpSession session) {
  }

  /**
   * Est-ce que la journalisation est active?
   * 
   * @return true si le journalisation est active.
   */
  public boolean isJournalisationActive() {
    return org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .isJournalisationActive();
  }

  /**
   * Permet d'exclure un composant d'interface tel qu'un onglet correspondant à
   * son identifiant pour l'action courante.
   * 
   * @param identifiant
   *          l'identifiant du composant d'interface.
   * @param request
   *          la requête en traitement.
   */
  public void exclureComposantInterface(String identifiant,
      HttpServletRequest request) {
    org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .exclureComposantInterface(identifiant, request);
  }

  /**
   * Permet de d'inclure un composant d'interface qui avait au préalable été
   * exclus.
   * 
   * @param identifiant
   *          l'identifiant du composant d'interface.
   * @param request
   *          la requête en traitement.
   */
  public void inclureComposantInterface(String identifiant,
      HttpServletRequest request) {
    org.sofiframework.presentation.utilitaire.UtilitaireControleur
        .inclureComposantInterface(identifiant, request);
  }

  /**
   * Méthode qui permet de conserver les paramètres de la liste de valeurs.
   * 
   * @param request
   *          la requête en traitement.
   */
  private void conserverParametresListeValeur(HttpServletRequest request) {
    if (request.getParameter("attributRetour") != null) {
      request.getSession().setAttribute("attributRetour",
          request.getParameter("attributRetour"));
    }

    if (request.getParameter("formulaireValeurRetour") != null) {
      request.getSession().setAttribute("formulaireValeurRetour",
          request.getParameter("formulaireValeurRetour"));
    }
  }
  
	private String getUrlAuthentification(HttpServletRequest request) {
		String actionAuthentification = (String) GestionParametreSysteme.getInstance()
				.getParametreSysteme(ConstantesParametreSysteme.URL_AUTHENTIFICATION);

		if (actionAuthentification != null) {
		
			if (actionAuthentification.startsWith("/")) {
				actionAuthentification = actionAuthentification.substring(1);
			}
		
			String[] pathUrl = actionAuthentification.split("/");
			String actionAuthentificationPath = "";
			if (pathUrl != null && pathUrl.length > 1) {
				actionAuthentificationPath = pathUrl[0].trim();
			}
			String path = request.getContextPath();
			if (path.indexOf(actionAuthentificationPath) > 0) {

				actionAuthentification = actionAuthentification.substring(actionAuthentificationPath.length());
			} 
		}
		return actionAuthentification;
	}
  

  /**
   * Traitement a appliquer lors d'un changement de service.
   * 
   * @param request
   *          la requête en changement
   * @param response
   *          la réponse en traitement.
   * @param form
   *          le formulaire en traitement.
   * @param mapping
   *          l'instance de mapping struts.
   * @throws ServletException
   *           l'exception s'il y lieu.
   */
  protected void traitementChangementService(HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping)
      throws ServletException {
  }

  /**
   * Traitement a appliquer avant l'application de la sécurite
   * 
   * @param request
   *          la requete en changement
   * @param response
   *          la reponse en traitement.
   * @param form
   *          le formulaire en traitement.
   * @param mapping
   *          l'instance de mapping struts.
   * @throws ServletException
   *           l'exception s'il y lieu.
   */
  protected void traitementAvantSecurite(ObjetSecurisable serviceDebut,
      HttpServletRequest request, HttpServletResponse response,
      ActionForm form, ActionMapping mapping) throws ServletException {

  }

  public static ForwardConfig getSecureActionForward(ForwardConfig forward,
      HttpServletRequest request) {

//    Boolean inactiverSSL = GestionParametreSysteme.getInstance().getBoolean(
//       ConstantesParametreSysteme.SSL_IGNORER_REDIRECT);
//
//    String urlSite = GestionSecurite.getInstance().getApplication()
//        .getAdresseSite();
//
//    if (!inactiverSSL && forward != null && forward.getRedirect()
//        && urlSite != null && urlSite.indexOf("https") != -1) {
//
//      String path = forward.getPath();
//      StringBuffer url = new StringBuffer("https://");
//      url.append(request.getServerName());
//      url.append(request.getContextPath());
//      url.append(path);
//      forward = new ActionForward(url.toString(), true);
//    }

    return forward;
  }
}
