/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;

/**
 * Classe de base pour faciliter l'intégration avec l'outil de génération
 * de rapport PDF, JasperReports.
 * <p>
 * @author Jean-François Brassard
 * @version SOFI 2.1
 */
public abstract class BaseJasperReportsAction extends BaseDispatchAction {
  /**
   * Instance de journalisation.
   */
  private static final Log log =
      LogFactory.getLog(org.sofiframework.presentation.struts.controleur.BaseJasperReportsAction.class);

  /**
   * Nécessite de spécifier le nom du rapport à utiliser.
   * @return le nom du rapport a utiliser.
   */
  public abstract String getCheminNomRapport();

  /**
   * Le nom du fichier qui sera générer depuis le fichier de rapport pour
   * téléchargement.
   * @param form le formulaire en traitement.
   * @param request la requête en traitement.
   * @return le nom du fichier qui sera généré en PDF.
   */
  public abstract String getNomFichier(BaseForm form,
      HttpServletRequest request);

  /**
   * Clé de message par défaut pour la création avec succès d'un rapport PDF.
   * Utilise le paramètre système suivant : cleMessageCreationPdfSucces afin de spécifier
   * la clé du message à utiliser.
   * @return la clé du message de création de PDF avec succès.
   */
  public String getCleMessageSucces() {
    return GestionParametreSysteme.getInstance().getString("cleMessageCreationPdfSucces");
  }

  /**
   * Clé de message par défaut pour la création avec échec d'un rapport PDF.
   * Utilise le paramètre système suivant : cleMessageCreationPdfEchec afin de spécifier
   * la clé du message à utiliser.
   * @return la clé du message de création de PDF avec échec.
   */
  public String getCleMessageEchec() {
    return GestionParametreSysteme.getInstance().getString("cleMessageCreationPdfEchec");
  }

  /**
   * Retourne la collection d'objet à utiliser pour la création du fichier PDF.
   * @param formulaire le formulaire en traitement.
   * @param request la requête en traitement.
   * @return la collection d'objet à utiliser pour la création du fichier PDF.
   */
  public abstract Collection getCollectionRapport(BaseForm formulaire,
      HttpServletRequest request);

  /**
   * Retourne la vue a utiliser apres la génération du PDF.
   * @param mapping le mapping en traitement.
   * @return la vue a appeler.
   */
  public ActionForward getNomVueRetour(ActionMapping mapping) {
    return mapping.findForward("afficher");
  }

  /**
   * Retourne la valeur du document en byte[].
   * @param jasperReport Le rapport Jasper a utiliser.
   * @param formulaire le formulaire en traitement.
   * @param request la requête en traitement.
   * @return la valeur du document en byte[].
   */
  public byte[] getValeurDocument(JasperReport jasperReport,
      BaseForm formulaire, HttpServletRequest request) {
    return null;
  }

  /**
   * Retourne la connexion SQL en cours de traitement.
   * @param request la requête en traitement.
   * @return la connexion SQL en cours de traitement.
   */
  public Connection getConnexion(HttpServletRequest request) {
    return null;
  }

  /**
   * Méthode qui permet de générer un fichier PDF.
   * @param mapping le mapping en traitement.
   * @param form le formulaire en traitement.
   * @param request la requête en traitement.
   * @param response la réponse en traitement.
   * @return la vue a appeler après traitement.
   * @throws IOException l'exception I/0 lancé s'il y lieu.
   * @throws ServletException l'exception de servlet s'il y lieu.
   */
  public ActionForward genererPdf(ActionMapping mapping, ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    // Accéder au formulaire
    BaseForm formulaire = (BaseForm)form;

    // Le document électronique extrait de la génération du rapport.
    DocumentElectronique documentElectronique = null;

    Collection collectionRapport = getCollectionRapport(formulaire, request);
    try {
      if (collectionRapport != null) {
        // Extraire le document pour téléchargement.
        documentElectronique =
            getUtilitaireJasperReports().genererDocumentEletronique(getCheminNomRapport(),
                getParametres(new HashMap(), getUtilisateur(request), formulaire,
                    request), collectionRapport,
                    getNomFichier(formulaire, request));
      } else {
        // Traitement si nécessite utilisation d'une connexion Sql.
        if (getConnexion(request) != null) {
          // Extraire le document pour téléchargement.
          documentElectronique =
              getUtilitaireJasperReports().genererDocumentEletronique(getCheminNomRapport(),
                  getParametres(new HashMap(), getUtilisateur(request),
                      formulaire, request), getConnexion(request),
                      getNomFichier(formulaire, request));
        } else {
          // Extaire la définition du JasperReport.
          JasperReport jasperReport =
              getUtilitaireJasperReports().getJasperReport(getCheminNomRapport());
          // Extraire le document pour téléchargement.
          documentElectronique =
              getUtilitaireJasperReports().getDocumentEletronique(getValeurDocument(jasperReport,
                  formulaire, request), getNomFichier(formulaire, request));

        }
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error(e);
        throw new SOFIException(e);
      }
    }

    // Fixer le document pour téléchargement.
    setDocumentElectronique(documentElectronique, request);

    // Ajout du message informatif.
    formulaire.ajouterMessageInformatif(getCleMessageSucces(), request);

    return getNomVueRetour(mapping);
  }

  /**
   * Permet le téléchargement du PDF.
   * @param mapping le mapping en traitement.
   * @param form le formulaire en traitement.
   * @param request la requête en traitement.
   * @param response la réponse en traitement.
   * @return la vue a appeler après traitement.
   * @throws IOException l'exception I/0 lancé s'il y lieu.
   * @throws ServletException l'exception de servlet s'il y lieu.
   */
  public ActionForward telechargerPdf(ActionMapping mapping, ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    // Accéder au formulaire
    BaseForm formulaire = (BaseForm)form;

    try {
      // Envoi du document eletronique dans la reponse.
      envoyerDocumentElectroniqueDansReponse((DocumentElectronique)request.getSession().getAttribute(DocumentElectronique.DOCUMENT_PDF),
          request, response);

      // Supprimer le pdf de la session, car téléchargé.
      request.getSession().setAttribute(DocumentElectronique.DOCUMENT_PDF,
          null);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error(e);
        if (formulaire != null) {
          formulaire.ajouterMessageErreur(e.getMessage(), request);
        }
      }

    }

    return mapping.findForward(null);
  }

  /**
   * Méthode que vous pouvez surcharger afin de spécifier des paramètres supplémentaire
   * à votre JasperReport. Important de rappeler le super (super.getParametres(...).
   * @param parametres les parametres a envoyer au fichier de rapport.
   * @param utilisateur l'utilisateur en cours.
   * @param formulaire le formulaire en cours.
   * @param request la requête en traitement.
   * @return les parametres a fixer pour utilisation dans le rapport.
   */
  public HashMap getParametres(HashMap parametres, Utilisateur utilisateur,
      BaseForm formulaire, HttpServletRequest request) {
    if (parametres == null) {
      parametres = new HashMap();
    }

    parametres.put("UTILISATEUR", utilisateur);
    parametres.put("SUBREPORT_DIR", "/jasper/");

    return parametres;
  }
}

