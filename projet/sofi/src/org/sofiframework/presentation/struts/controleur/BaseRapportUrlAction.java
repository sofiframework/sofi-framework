/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.ConstantesMessage;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireHttp;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;


/**
 * Action qui généralise le traitement effectué dans les pages qui appelent
 * des rapports via une requête HTTP.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.1
 */
public abstract class BaseRapportUrlAction extends BaseDispatchAction {

  public final static String NOM_RAPPORT_DYNAMIQUE = "[NOM_RAPPORT]";
  public final static String NOM_RAPPORT_REQUETE = "nomRapportTraitement";

  /** Constructeur par défault */
  public BaseRapportUrlAction() {
  }

  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    request.getSession().removeAttribute(getNomDocumentDansSession(form, request));

    if (form != null) {
      ((BaseForm) form).initialiserFormulaire(request, mapping,
          new ObjetCleValeur(), true);
    }

    return mapping.findForward(this.getNomVueRetour());
  }

  /**
   * Action permettant de générer un rapport Oracle Rapport de format PDF.
   * C'est l'action qui doit être appelée par le bouton "Générer Rapport".
   * @param mapping ActionMapping utilisé pour trouver cette action.
   * @param form Formulaire correspondant à l'action (facultatif).
   * @param request Requête HTP qui est traitée.
   * @param response Réponse HTTP qui est traitée.
   * @return Nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward genererRapport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    this.specifierUrl(form, request);

    String urlStr = genererUrlRapport(request);
    BaseForm formulaire = (BaseForm) form;

    try {
      if (!formulaire.isFormulaireEnErreur()) {
        if (urlStr != null) {
          URL url = new URL(urlStr);
          UtilitaireHttp http = new UtilitaireHttp();
          http.setIsSecure(urlStr.indexOf("https") != -1);
          http.setUrlDomaine(url.getHost());

          String urlComplet = url.getPath() + "?" + url.getQuery();
          http.setUrlPath(urlComplet);
          http.setUrlPort(url.getPort());

          byte[] contenu = http.getContenuUrl();
          StringBuffer buf = new StringBuffer();

          String cleMessageErreur = GestionMessage.getInstance()
              .getFichierConfiguration().get(ConstantesMessage.ERREUR_OPERATION_RAPPORT_ECHEC, request);

          // Traiter le message d'opération général traité avec succès
          String cleMessageSucces = GestionMessage.getInstance()
              .getFichierConfiguration().get(ConstantesMessage.INFORMATION_OPERATION_RAPPORT_SUCCES, request);

          /* Si on a un contenu en retour, on doit vérifier
           * si le documet est une page d'erreur
           */
          if (contenu != null) {
            /* on récupère les premiers bytes. Pour ne pas transformer
             * tous le document pour rien...
             */
            for (int i = 0; (i < contenu.length) && (i < "<HTML>".length()); i++) {
              buf.append(new String(new byte[]{contenu[i]}));
            }

            // Si c'est un document html c'est une erreur
            if (buf.toString().equalsIgnoreCase("<HTML>")) {
              String contenuHTML = new String(contenu);

              log.error("Le serveur de rapport a retourné l'erreur suivante : " + contenuHTML);

              if (!UtilitaireString.isVide(cleMessageErreur)) {
                if (contenuHTML.indexOf("<pre>REP-") != -1) {
                  String messageErreurHtml = contenuHTML.substring(contenuHTML.indexOf(
                      "<pre>REP-"), contenuHTML.indexOf("</pre>"));
                  formulaire.ajouterMessageErreur(messageErreurHtml, request);
                } else {
                  formulaire.ajouterMessageErreur(cleMessageErreur, request);
                }
              }
            }
          } else {
            log.error("Le serveur de rapport n'a retourné aucune réponse.");

            if (!UtilitaireString.isVide(cleMessageErreur)) {
              formulaire.ajouterMessageErreur(cleMessageErreur, request);
            }
          }

          DocumentElectronique document = new DocumentElectronique();
          String nomFichierRapport = this.getNomFichierRapport() + this.getExtentionFichierRapport(form,request);
          if (nomFichierRapport.indexOf("[NOM_RAPPORT]") != -1) {
            String nomRapport = (String) request.getAttribute(NOM_RAPPORT_REQUETE);
            nomRapport = UtilitaireString.remplacerTous(nomFichierRapport, NOM_RAPPORT_DYNAMIQUE, nomRapport);
            document.setNomFichier(nomRapport);
          } else {
            document.setNomFichier(nomFichierRapport);
          }

          document.setCodeTypeMime(getCodeTypeMimeContenuDocument(form, request));
          document.setValeurDocument(contenu);

          if (request.getAttribute("rapportEcrireReponse") != null) {
            envoyerDocumentElectroniqueDansReponse(document, request, response);
          } else {
            // Fixer le document dans la session temporairement pour l'action.
            setDocumentElectronique(getNomDocumentDansSession(form,request), document, request);
          }

          if ((contenu != null) && (contenu.length > 0)) {


            if (!UtilitaireString.isVide(cleMessageSucces)) {
              formulaire.ajouterMessageInformatif(cleMessageSucces, request);
            }
          }
        }
      }
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(e.getMessage(), request);
    }

    return mapping.findForward(this.getNomVueRetour());
  }

  /**
   * Récupère le document dans la session sous le nom de document fournni.
   * Ensuite, écrit le document dans la réponse http.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @param nomDocument
   * @return
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward envoyerRapportDansReponse(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    String nomDocument = getNomDocumentDansSession(form, request);
    DocumentElectronique document = (DocumentElectronique) request.getSession().getAttribute(nomDocument);
    request.getSession().setAttribute(nomDocument, null);
    if (document != null) {
      envoyerDocumentElectroniqueDansReponse(document, request, response);
    }
    return null;
  }

  /**
   * Permet d'ajouter des paramètres à un URL.
   * @param parametre le nom de paramètre
   * @param valeur la valeur du paramètre.
   * @param request la requête HTTP en traitement.
   * @return l'url avec les paramètres associés.
   */
  public Href ajouterParametreUrl(String parametre, String valeur,
      HttpServletRequest request) {
    Href lienURLRapport = getUrlRapportTraitement(request);

    if (lienURLRapport == null) {
      throw new SOFIException(
          "Vous devez appeler setUrlRapport avant d'appeler ajouterParametreUrl");
    }

    if (!isVide(valeur)) {
      lienURLRapport.ajouterParametre(parametre,
          encoreParametreInvalideUrlRapport(valeur));
    }

    return lienURLRapport;
  }

  public String encoreParametreInvalideUrlRapport(String whereClause) {
    whereClause = UtilitaireString.remplacerTous(whereClause, "%", "%25");
    whereClause = UtilitaireString.remplacerTous(whereClause, "=", "%3d");
    whereClause = UtilitaireString.remplacerTous(whereClause, "'", "%27");
    whereClause = UtilitaireString.remplacerTous(whereClause, " ", "%20");

    return whereClause;
  }

  /**
   * Spécifier l'url de base qui permet d'appeler le rapport.
   * @param urlRapport l'url de base qui permet d'appeler le rapport.
   * @param avecSecurite true si vous désirez soit sécurisé pour l'utilisateur. Si vous
   * vous utilise un certficat, il sera automatiquement ajouter à l'url. Si vous utiliser un code
   * utilisateur, vous devez configurer en paramètre système le paramètre <code> paramRapportCodeUtilisateur</code> pour
   * spécifier le paramètre qui sera associé le code utilisateur.
   * @param request la requête HTTP en traitement.
   */
  public void setUrlRapport(String nomRapport, boolean avecSecurite,
      boolean ecrireDansReponse, HttpServletRequest request) {
    String urlRapport = GestionParametreSysteme.getInstance()
        .getParametreSystemeEJB(nomRapport);

    request.setAttribute("nomRapportTraitement", nomRapport);

    if (ecrireDansReponse) {
      request.setAttribute("rapportEcrireReponse", Boolean.TRUE);
    }

    //String urlRapport = "http://dv01.intrameq/reports/rwservlet?UNOU011011DEV01&destype=cache&desformat=pdf&PNU_NO_SESN_UTILS_RAPRT=420&report=UNO030403R.rep&paramform=NO&UNOU011011DEV01=&PNU_NO_SESN_UTILS=87";
    if (UtilitaireString.isVide(urlRapport)) {
      throw new SOFIException(
          "La clé du rapport n'existe pas dans les paramètres système");
    }

    Href href = new Href(urlRapport);

    request.setAttribute("urlRapportTraitement", href);

    if (avecSecurite) {
      String paramCodeUtilisateur = GestionParametreSysteme.getInstance()
          .getString(ConstantesParametreSysteme.PARAMETRE_RAPPORT_CODE_UTILISATEUR);

      if (!UtilitaireString.isVide(paramCodeUtilisateur)) {
        ajouterParametreUrl(paramCodeUtilisateur, getCertificat(request),
            request);
      } else {
        ajouterParametreUrl(GestionSecurite.getInstance()
            .getNomCookieCertificat(),
            getCertificat(request), request);
      }
    }
  }

  public void setUrlRapport(String nomRapport, boolean avecSecurite,
      HttpServletRequest request) {
    setUrlRapport(nomRapport, avecSecurite, false, request);
  }

  /**
   * Génère l'url pour appeller le rapport.
   * @param request la requête en traitement.
   * @return l'url pour appeller le rapport.
   */
  public String genererUrlRapport(HttpServletRequest request) {
    Href lienURLRapport = getUrlRapportTraitement(request);

    return lienURLRapport.getUrlPourRedirection();
  }

  private Href getUrlRapportTraitement(HttpServletRequest request) {
    Href lienURLRapport = (Href) request.getAttribute("urlRapportTraitement");

    if (lienURLRapport == null) {
    }

    return lienURLRapport;
  }

  /**
   * Cette méthode permet de retrouver le type MIME du contenu du document
   * dans le request. Cette méthode peut être surchargée pour changer le
   * mime du document.
   * 
   * @return le type MIME du contenu du document
   * @param request
   */
  protected String getCodeTypeMimeContenuDocument(ActionForm form, HttpServletRequest request) {
    return DocumentElectronique.TYPE_MIME_PDF;
  }

  /**
   * Cette méthode permet de retrouver le nom du document dans la session. Cette méthode
   * peut être réécrite pour changement le nom utilisé. (par exemple dans le cas d'un fichier csv).
   * 
   * Le nom documentPdf est utilisé par défaut.
   * 
   * @param form
   * @param request
   * @return
   */
  protected String getNomDocumentDansSession(ActionForm form, HttpServletRequest request) {
    return DocumentElectronique.DOCUMENT_PDF;
  }

  /**
   * Dans cette méthode, vous devez implémenter cette méthode afin de specifier l'url qui va
   * appeler le rapport.
   * @param form le formulaire en traitement
   * @param request la requête HTTP en traitement.
   * @return l'url qui va appeler le rapport.
   */
  protected abstract void specifierUrl(ActionForm form,
      HttpServletRequest request);

  /**
   * Dans cette méthode, vous devez spécifier le nom logique d'appel de la vue.
   * @return le nom logique de la vue a appeller.
   * @return
   */
  protected abstract String getNomVueRetour();

  /**
   * Dans cette méthode, vous devez spécifier le nom du fichier du rapport.
   * @return le nom du fichier du rapport.
   */
  protected abstract String getNomFichierRapport();

  /**
   * Dans cette méthode, vous pouvez spécifier l'extention du rapport.
   * @return
   * @param request
   * @param form
   */
  protected String getExtentionFichierRapport(ActionForm form, HttpServletRequest request) {
    return "";
  }
}
