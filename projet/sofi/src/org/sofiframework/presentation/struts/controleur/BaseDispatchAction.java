/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.sofiframework.application.journalisation.GestionJournalisation;
import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.ErreurApplication;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.composantweb.menu.RepertoireMenu;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.constante.ConstantesLibelle;
import org.sofiframework.constante.ConstantesMessage;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.DoubleSoumissionException;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.Modele;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.ajax.CorrespondancePropriete;
import org.sofiframework.presentation.ajax.GenerateurXmlAjax;
import org.sofiframework.presentation.ajax.ReponseListeValeurAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.InfoMultibox;
import org.sofiframework.presentation.composant.NavigationParEtape;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireListeDeroulante;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireJasperReports;
import org.sofiframework.utilitaire.UtilitaireNombre;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.UtilitaireTableau;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;
import org.sofiframework.utilitaire.documentelectronique.UtilitaireDocumentElectronique;

/**
 * Classe abstraite de base pour toutes les actions développés avec SOFI.
 * <p>
 * Elle offre la possibilité par exemple d'appliquer une liste de navigation page par page.
 * <p>
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.composantweb.liste.ListeNavigation
 */
public abstract class BaseDispatchAction extends DispatchAction {
  /**
   * Retourne le modèle de l'application.
   * <p>
   * 
   * @param request
   *          la requête présentement en cours.
   * @return le modèle de l'application
   */
  public Modele getModele(HttpServletRequest request) {
    return UtilitaireControleur.getModele(request);
  }

  public ListeNavigation appliquerListeNavigation(String listeId, HttpServletRequest request) {
    request.setAttribute("SOFI_listeEnCours", listeId);

    return appliquerListeNavigation(request);
  }

  /**
   * Prépare une instance de ListeNavigation afin d'appliquer un tri, un appel à une nouvelle page par exemple.
   * <p>
   * 
   * @param request
   *          la requête HTTP traitée
   * @return La liste de naviation prête à être envoyé au servide d'affaire
   */
  public ListeNavigation appliquerListeNavigation(HttpServletRequest request) {
    ListeNavigation liste = new ListeNavigation();

    String attributATrier = null;
    String noPage = null;
    String ancienOrdreTri = (String) request.getSession().getAttribute("d-12632-xo");
    String ordreTri = null;
    String ancienTri = (String) request.getSession().getAttribute("d-12632-tri");
    
    String maxPagePage = null;
    
    Enumeration paramNames = request.getParameterNames();

    String actionCourante = UtilitaireControleur.getNomActionCourant(request);

    // Accéder au répertoire des paramètres de tri d'une liste.
    HashMap repertoireParametresListe = (HashMap) request.getSession().getAttribute(
        actionCourante + "repertoireParametreListeNavigation");

    HashMap parametresListe = null;

    // Accès au nom de la liste en traitement s'il y a lieu.
    String listeId = request.getParameter("listeId");

    // Accès au nom de la liste en cours de traitement.
    String listeEnCours = (String) request.getAttribute("SOFI_listeEnCours");

    boolean appliquerAncienCritere = false;

    if ((listeEnCours != null) && !listeEnCours.equals(listeId)) {
      listeId = listeEnCours;
      appliquerAncienCritere = true;
    }

    if (repertoireParametresListe != null) {
      if (listeEnCours != null) {
        parametresListe = (HashMap) repertoireParametresListe.get(listeEnCours);
      } else {
        parametresListe = (HashMap) repertoireParametresListe.get("general");
      }
    } else {
      repertoireParametresListe = new HashMap();
    }
    
    String paramNombreMaxParPage = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.PARAM_NOMBRE_MAX_PAR_PAGE);
    
    if (!isVide(paramNombreMaxParPage)) {
      // Fixer le nombre maximum par page.
      maxPagePage = request.getParameter(paramNombreMaxParPage);
    }
    

    if (parametresListe == null) {
      parametresListe = new HashMap();
      parametresListe.put("maxPagePage", maxPagePage);
    } else {
      attributATrier = (String) parametresListe.get("attributAtrier");
      noPage = (String) parametresListe.get("noPage");
      ordreTri = (String) parametresListe.get("ordreTri");
      if (maxPagePage == null)  {
        maxPagePage = (String) parametresListe.get("maxPagePage");
      }else {
        parametresListe.put("maxPagePage", maxPagePage);
      }
    } 

    boolean parametreListeTrouve = false;

    if (!appliquerAncienCritere) {
      while (paramNames.hasMoreElements()) {
        String cle = (String) paramNames.nextElement();

        if ((cle != null) && cle.startsWith("d-") && cle.endsWith("-tri")) {
          attributATrier = request.getParameter(cle);

          setAttributTemporairePourService(cle + listeId, attributATrier, request);
          parametresListe.put("attributAtrier", attributATrier);
        }

        if ((cle != null) && cle.startsWith("d-") && cle.endsWith("-p")) {
          noPage = request.getParameter(cle);
          parametresListe.put("noPage", noPage);
          parametreListeTrouve = true;
        }

        if ((cle != null) && cle.startsWith("d-") && cle.endsWith("-o")) {
          ordreTri = request.getParameter(cle);
          parametresListe.put("ordreTri", ordreTri);
          setAttributTemporairePourService(cle + listeId, ordreTri, request);
        }

        if ((cle != null) && cle.startsWith("d-") && cle.endsWith("-s")) {
          String colonneTri = request.getParameter(cle);
          setAttributTemporairePourService(cle + listeId, colonneTri, request);
        }
      }
    }

    String initialiserListe = request.getParameter(ConstantesBaliseJSP.INITIALISER_PARAMETRES_LISTE_NAVIGATION);

    if (!parametreListeTrouve && (initialiserListe == null) && !appliquerAncienCritere) {
      if ((request.getSession().getAttribute("SOFI_ACTION_POST") != null)) {
        noPage = "1";
      }
    } else {
      if (initialiserListe != null) {
        noPage = "1";
      }
    }

    if (listeId != null) {
      repertoireParametresListe.put(listeId, parametresListe);
    } else {
      repertoireParametresListe.put("general", parametresListe);
    }

    UtilitaireControleur.setAttributTemporairePourService(actionCourante + "repertoireParametreListeNavigation",
        repertoireParametresListe, request);

    if (attributATrier == null) {
      attributATrier = ancienTri;
    }

    if (noPage == null) {
      liste.setNoPage(1);
    } else {
      liste.setNoPage((new Integer(noPage)).intValue());
    }

    liste.setTriAttribut(attributATrier);

    if ((ancienOrdreTri != null) && (ordreTri == null)) {
      ordreTri = ancienOrdreTri;
    }

    if ((ordreTri != null) && ordreTri.equals("2")) {
      liste.setOrdreTri("ASC");
    } else {
      liste.setOrdreTri("DESC");
    }
    
    if (maxPagePage != null) {
      Integer maxPerPage = new Integer(maxPagePage);
      liste.setMaxParPage(maxPerPage);
    }    

    return liste;
  }

  /**
   * Spécifier que vous désirez afficher la liste de navigation avec les paramètres lors du dernier affichage.
   * 
   * @param request
   *          la requête HTTP en traitement
   */
  public void afficherListeNavigationAvecAncienParametres(HttpServletRequest request) {
    request.getSession().setAttribute("consulterListeNavigationAvecAncienParam", Boolean.TRUE);
  }

  /**
   * Permet de générer un message de statut de la liste de navigation.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param liste
   *          la liste de navigation en traitement
   * @param nomTypeOccurence
   *          le nom correspondant au type d'occurence de la liste. Par exemple: client(s)
   * @param isInclusAvecPagination
   *          si le message est inclus avec les indicateurs de navigation page précédente et page suivante
   */
  public void genererMessagePourListeNavigation(HttpServletRequest request, ListeNavigation liste,
      String nomTypeOccurence, boolean isInclusAvecPagination) {
    StringBuffer message = new StringBuffer();
    message.append("<strong>");
    message.append(liste.getNbListe());
    message.append("</strong>");
    message.append(" ");

    String tempNomTypeOccurence = null;

    if (nomTypeOccurence == null) {
      String cleClient = GestionMessage.getInstance().getFichierConfiguration()
          .get(ConstantesLibelle.INFORMATION_RESULTAT_LISTE_TYPE, request);
      Libelle typeResultatDefaut = UtilitaireLibelle.getLibelle(cleClient, request, null);
      tempNomTypeOccurence = typeResultatDefaut.getMessage();
    } else {
      Libelle descriptionNomTypeOccurence = UtilitaireLibelle.getLibelle(nomTypeOccurence, request, null);
      tempNomTypeOccurence = descriptionNomTypeOccurence.getMessage();
    }

    message.append(tempNomTypeOccurence);

    if (isInclusAvecPagination) {
      UtilitaireVelocity velocity = new UtilitaireVelocity();
      velocity.ajouterAuContexte("nbListe", new Long(liste.getNbListe()));
      velocity.ajouterAuContexte("nomTypeOccurence", tempNomTypeOccurence);

      String gabarit = GestionParametreSysteme.getInstance().getString("velocity.gabarit.barre.listeNavigation");

      if (UtilitaireString.isVide(gabarit)) {
        gabarit = "org/sofiframework/presentation/velocity/gabarit/barreListeNavigation.vm";
      }

      String resultat = null;

      resultat = velocity.genererHTML(gabarit);

      request.getSession().setAttribute(Constantes.MESSAGE_LISTE_NAVIGATION, resultat.toString());
    } else {
      String cleClient = GestionMessage.getInstance().getFichierConfiguration()
          .get(ConstantesLibelle.INFORMATION_RESULTAT_NB_PAGE, request);
      Message messageNbPage = UtilitaireLibelle.getLibelle(cleClient, request,
          new Object[] { String.valueOf(liste.getNoPage()), String.valueOf(liste.getNbPage()) });
      message.append(" ,");
      message.append(messageNbPage.getMessage());
      request.getSession().setAttribute(Constantes.MESSAGE_LISTE_NAVIGATION, message.toString());
    }
  }

  /**
   * Permet de générer un message de statut de la liste de navigation.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param liste
   *          la liste de navigation en traitement
   * @param nomTypeOccurence
   *          le nom correspondant au type d'occurence de la liste. Par exemple: client(s).
   */
  public void genererMessagePourListeNavigation(HttpServletRequest request, ListeNavigation liste,
      String nomTypeOccurence) {
    genererMessagePourListeNavigation(request, liste, nomTypeOccurence, true);
  }

  /**
   * Permet de spécifier les attributs qui forme une clé unique pour une liste de navigation, afin de permettre la
   * sélection d'un ligne.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param nomListe
   *          le nom de la liste de navigation
   * @param objet
   *          l'objet de transfert qui est dans la liste de navigation
   * @param nomAttributCle
   *          un tableau d'attribut spécifiant la clé
   */
  public void specifierLigneSelectionneePourListeNavigation(HttpServletRequest request, String nomListe,
      ObjetTransfert objet, String[] nomAttributCle) {
    StringBuffer nomListeParam = new StringBuffer();
    nomListeParam.append(nomListe);
    nomListeParam.append("_ligneSelectionnee");

    if (nomAttributCle != null) {
      HashMap parametresCleListe = new HashMap(nomAttributCle.length);

      for (int i = 0; i < nomAttributCle.length; i++) {
        Object valeur = objet.getPropriete(nomAttributCle[i]);
        parametresCleListe.put(nomAttributCle[i], valeur);
        parametresCleListe.put(nomAttributCle[i], valeur);
      }

      // Spécifier dans la session les attributs spécifiant un ligne
      // sélectionnee d'une liste de navigation.
      this.setAttributTemporairePourService(nomListeParam.toString(), parametresCleListe, request);
    } else {
      request.getSession().removeAttribute(nomListeParam.toString());
    }
  }

  /**
   * Permet de spécifier les attributs qui forme une clé unique pour une liste de navigation, afin de permettre la
   * sélection d'un ligne.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param nomListe
   *          le nom de la liste de navigation
   * @param objet
   *          l'objet de transfert qui est dans la liste de navigation
   */
  public void specifierLigneSelectionneePourListeNavigation(HttpServletRequest request, String nomListe,
      ObjetTransfert objet) {
    String[] cle = null;

    if (objet != null) {
      cle = objet.getCle();
    } else {
      cle = null;
    }

    this.specifierLigneSelectionneePourListeNavigation(request, nomListe, objet, cle);
  }

  /**
   * Permet d'initialiser les paramètres de sélection de la liste navigation.
   * <p>
   * 
   * @param request
   *          la requête HTTP en traitement
   * @param nomListe
   *          le nom de la liste de navigation
   */
  public void initialiserLigneSelectionneePourListeNavigation(HttpServletRequest request, String nomListe) {
    this.specifierLigneSelectionneePourListeNavigation(request, nomListe, null, null);
  }

  /**
   * Est-ce une erreur de double soumission du formulaire
   * 
   * @param e
   *          une erreur du modèle.
   */
  public boolean isErreurDoubleSoumissionFormulaire(SOFIException e) {
    return e instanceof DoubleSoumissionException;
  }

  /**
   * Retourne l'utilisateur dans la session.
   * 
   * @param request
   * @return l'utilisateur dans la session.
   */
  public Utilisateur getUtilisateur(HttpServletRequest request) {
    return UtilitaireControleur.getUtilisateur(request.getSession());
  }

  /**
   * Traiter la sélection des boites a cocher multiple dans une liste navigation.
   * 
   * @param request
   *          la requete en traitement.
   * @param formulaire
   *          le formulaire en traitement.
   */
  public void traiterBoiteACocherMultiple(HttpServletRequest request, BaseForm formulaire) {
    HashMap listeInfoListeCouranteMultibox = (HashMap) request.getSession().getAttribute(
        "listeInfoListeCouranteMultibox");
    java.util.Iterator iterateur = listeInfoListeCouranteMultibox.keySet().iterator();

    while (iterateur.hasNext()) {
      InfoMultibox infoListeCouranteMultibox = (InfoMultibox) listeInfoListeCouranteMultibox.get(iterateur.next());
      HashSet listeSelectionnee = infoListeCouranteMultibox.getListeSelectionnee();

      if (formulaire != null) {
        String[] valeurs = null;

        valeurs = (String[]) UtilitaireObjet.getValeurAttribut(formulaire, infoListeCouranteMultibox.getNomAttribut());

        if (valeurs == null) {
          valeurs = new String[0];
        }

        java.util.Iterator iterateurValeurListeCourante = infoListeCouranteMultibox.getListeIdentifiantListeCourante()
            .iterator();

        while (iterateurValeurListeCourante.hasNext()) {
          Object valeurPourMultibox = iterateurValeurListeCourante.next();

          for (int i = 0; i < valeurs.length; i++) {
            if (valeurPourMultibox.toString().equals(valeurs[i])) {
              if (infoListeCouranteMultibox.getListeSelectionnee() != null) {
                listeSelectionnee.add(valeurPourMultibox.toString());
              }

              break;
            } else {
              if (listeSelectionnee != null) {
                listeSelectionnee.remove(valeurPourMultibox.toString());
              }
            }
          }

          if ((valeurs.length == 0) && (listeSelectionnee != null)) {
            listeSelectionnee.remove(valeurPourMultibox.toString());
          }
        }
      }
    }

    request.getSession().setAttribute("listeInfoListeCouranteMultiboxTraite", Boolean.TRUE);
  }

  /**
   * Mettre à jour la sélection des boites a cocher multiple.
   * 
   * @param request
   *          la requete en traitement.
   */
  public void majBoiteACocherMultiple(HttpServletRequest request) {
    HashMap listeInfoListeCouranteMultibox = (HashMap) request.getSession().getAttribute(
        "listeInfoListeCouranteMultibox");
    java.util.Iterator iterateur = listeInfoListeCouranteMultibox.keySet().iterator();
    boolean trouve = false;

    while (iterateur.hasNext() && !trouve) {
      InfoMultibox infoListeCouranteMultibox = (InfoMultibox) listeInfoListeCouranteMultibox.get(iterateur.next());
      HashSet listeSelectionnee = infoListeCouranteMultibox.getListeSelectionnee();
      String valeur = request.getParameter(infoListeCouranteMultibox.getNomParametreRequete());

      if (!UtilitaireString.isVide(valeur)) {
        if ((listeSelectionnee != null) && (listeSelectionnee.contains(valeur))) {
          listeSelectionnee.remove(valeur.toString());

          if (infoListeCouranteMultibox.getListeSelectionneeOriginal().contains(valeur)) {
            infoListeCouranteMultibox.ajouterListeDeselection(valeur);
          }

          infoListeCouranteMultibox.supprimerListeNouvelleSelection(valeur);
          infoListeCouranteMultibox.setDernierIdentifiantDeselection(valeur);
          infoListeCouranteMultibox.setDernierTraitementNouvelleSelection(false);
        } else {
          listeSelectionnee.add(valeur);
          infoListeCouranteMultibox.supprimerListeDeselection(valeur);
          infoListeCouranteMultibox.ajouterListeNouvelleSelection(valeur);
          infoListeCouranteMultibox.setDernierIdentifiantNouvelleSelection(valeur);
          infoListeCouranteMultibox.setDernierTraitementNouvelleSelection(true);
        }

        trouve = true;
      }
    }

    request.getSession().setAttribute("listeInfoListeCouranteMultiboxTraite", Boolean.TRUE);
  }

  /**
   * Permet d'initialiser la liste sélectionné d'une boite à cocher multiple.
   * 
   * @param attribut
   *          l'attribut de la boite à cocher multiple
   * @param listeSelectionnee
   *          la liste sélectionné pour l'initialisation.
   * @param request
   *          la requête en traitement.
   */
  public void initialiserBoiteACocherMultiple(String attribut, HashSet listeSelectionnee, HttpServletRequest request) {
    HashMap listeInfoListeCouranteMultibox = (HashMap) request.getSession().getAttribute(
        "listeInfoListeCouranteMultibox");

    if (listeInfoListeCouranteMultibox == null) {
      listeInfoListeCouranteMultibox = new HashMap();
      request.getSession().setAttribute("listeInfoListeCouranteMultibox", listeInfoListeCouranteMultibox);
    }

    InfoMultibox infoListeCouranteMultibox = (InfoMultibox) listeInfoListeCouranteMultibox.get(attribut);

    infoListeCouranteMultibox = new InfoMultibox();

    if (listeSelectionnee == null) {
      listeSelectionnee = new HashSet();
    }

    infoListeCouranteMultibox.setListeSelectionnee(listeSelectionnee);

    HashSet listeSelectionneOrigignal = null;

    try {
      listeSelectionneOrigignal = (HashSet) listeSelectionnee.clone();
    } catch (Exception e) {
    }

    infoListeCouranteMultibox.setListeSelectionneeOriginal(listeSelectionneOrigignal);
    infoListeCouranteMultibox.setNomAttribut(attribut);

    listeInfoListeCouranteMultibox.put(attribut, infoListeCouranteMultibox);
  }

  /**
   * Retourne le detail d'un groupe de boite à cocher multiple.
   * 
   * @param attribut
   *          l'attribut du groupe de boite à cocher multiple
   * @param request
   *          la requête en traitement
   */
  public InfoMultibox getDetailBoiteACocherMultiple(String attribut, HttpServletRequest request) {
    return UtilitaireControleur.getDetailBoiteACocherMultiple(attribut, request);
  }

  /**
   * Permet une écrire de journalisation dans l'infrastructure SOFI pour un traitement spécifique.
   * 
   * @param request
   *          la requête en traitement
   * @param mode
   *          le mode de traitement
   * @param detail
   *          le detail du traitement.
   * @param parametres
   *          les paramètres associé au détail du message.
   * @throws Exception
   *           java.lang.Exception
   */
  public void ecrireJournal(HttpServletRequest request, String mode, String detail, String[] parametres)
      throws Exception {
    ecrireJournal(request, mode, detail, parametres, null, null);
  }

  /**
   * Permet une écrire de journalisation dans l'infrastructure SOFI pour un traitement spécifique.
   * 
   * @param request
   *          la requête en traitement
   * @param mode
   *          le mode de traitement
   * @param codeClient
   *          la référence du client qui a réalisé l'action à journaliser.
   * @param detail
   *          le detail du traitement.
   * @param parametres
   *          les paramètres associé au détail du message.
   * @param reference
   *          la référence (ex. no de dossier) associé à la journalisation.
   * @throws Exception
   *           java.lang.Exception
   */
  public void ecrireJournal(HttpServletRequest request, String mode, String detail, String[] parametres,
      String codeClient, String reference) throws Exception {
    String codeLangueJournalisation = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.CODE_LANGUE_JOURNALISATION);
    Message message = null;

    if (!isVide(codeLangueJournalisation)) {
      message = GestionMessage.getInstance().get(detail, codeLangueJournalisation, parametres, null);
    } else {
      message = UtilitaireMessage.get(detail, parametres, request);
    }

    ecrireJournal(request, mode, message.getMessage(), codeClient, reference);
  }

  /**
   * Permet une écrire de journalisation dans l'infrastructure SOFI pour un traitement spécifique.
   * 
   * @param request
   *          la requête en traitement
   * @param mode
   *          le mode de traitement
   * @param codeClient
   *          Le codee du client qui a eu l'action a journaliser
   * @param detail
   *          le detail du traitement.
   * @param reference
   *          la référence (ex. no de dossier) associé à la journalisation.
   * @throws java.lang.Exception
   * @since SOFI 2.0.3 Ajout du code client et de la référence a journaliser.
   */
  public Journal ecrireJournal(HttpServletRequest request, String mode, String detail, String codeClient,
      String reference) throws Exception {
    // Utilisateur en traitement.
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
    // L'emplacement en traitement.
    ObjetSecurisable serviceCourant = UtilitaireControleur.getServiceCourant(request.getSession());

    Journal journal = null;
    if (GestionJournalisation.getInstance().isServiceJournalisationConfigure()) {
      try {
        journal = GestionJournalisation.getInstance().ecrire(utilisateur.getCodeUtilisateur(), mode, detail,
            codeClient, reference, serviceCourant.getSeqObjetSecurisable());
      } catch (Exception e) {
        throw e;
      }
    } else {
      journal = new Journal();
      journal.setCodeUtilisateur(utilisateur.getCodeUtilisateur());
      journal.setDateHeure(new java.util.Date());
      journal.setType(mode);
      journal.setDetail(detail);
      journal.setCodeClient(codeClient);
      journal.setReference(reference);
      journal.setSeqObjetSecurisable(serviceCourant.getSeqObjetSecurisable());

      // Appel de la méthode qui permet d'être surchargé pour traitement
      // spécifique.
      journal = appelServiceJournalisation(journal);
    }

    return journal;
  }

  /**
   * Méthode qui nécessite une ré-écriture. Cette ré-écriture permet de retourne le service de journalisation utilisé.
   * 
   * @return l'instance resultante de la journalisation.
   */
  public Journal appelServiceJournalisation(Journal journal) {
    return null;
  }

  /**
   * Retourne le certificat associé à l'utilisateur de la session ou dans son cookie d'authentification de session.
   * 
   * @param request
   *          la requête en traitement.
   * @return le certificat de l'utilisateur.
   */
  protected String getCertificat(HttpServletRequest request) {
    return UtilitaireControleur.getCertificat(request);
  }

  /**
   * Retourne le code utilisateur de l'utilisateur en traitement.
   * 
   * @param request
   *          la requête en traitement.
   * @return le code utilisateur de l'utilisateur en traitement.
   */
  public String getCodeUtilisateur(HttpServletRequest request) {
    Utilisateur utilisateur = getUtilisateur(request);

    return utilisateur.getCodeUtilisateur();
  }

  /**
   * Retourne l'adresse de l'action courante.
   * 
   * @param request
   *          la requête en traitement.
   * @return l'adresse de l'action courante.
   */
  public String getAdresseActionCourante(HttpServletRequest request) {
    return (String) request.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE);
  }

  /**
   * Permet d'exclure un composant d'interface tel qu'un onglet correspondant à son identifiant pour l'action courante.
   * 
   * @param identifiant
   *          l'identifiant du composant d'interface.
   * @param request
   *          la requête en traitement.
   */
  public void exclureComposantInterface(String identifiant, HttpServletRequest request) {
    UtilitaireControleur.exclureComposantInterface(identifiant, request);
  }

  /**
   * Permet de d'inclure un composant d'interface qui avait au préalable été exclus.
   * 
   * @param identifiant
   *          l'identifiant du composant d'interface.
   * @param request
   *          la requête en traitement.
   */
  public void inclureComposantInterface(String identifiant, HttpServletRequest request) {
    UtilitaireControleur.inclureComposantInterface(identifiant, request);
  }

  /**
   * Permet de d'ajouter dynamiquement un composant d'interface.
   * <p>
   * ATTENTION non implementé complétement, NE PAS UTILISER!!!!
   * 
   * @param identifiant
   *          l'identifiant du composant d'interface.
   * @param request
   *          la requête en traitement.
   */
  public void ajouterComposantInterface(String identifiant, String identifiantParent, String adresseWeb,
      String aideContextuelle, Integer ordreAffichage, Class typeObjet, HttpServletRequest request) {
    HashSet repertoireComposantInterfaceAjoute = (HashSet) request.getSession().getAttribute(
        Constantes.REPERTOIRE_COMPOSANT_INTERFACE_AJOUTE);
    HashMap repertoireComposantInterfaceAAjouter = (HashMap) request.getSession().getAttribute(
        Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_AJOUTER);

    if (repertoireComposantInterfaceAAjouter == null) {
      repertoireComposantInterfaceAAjouter = new HashMap();
      request.getSession().setAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_AJOUTER,
          repertoireComposantInterfaceAAjouter);
    }

    if (repertoireComposantInterfaceAjoute == null) {
      repertoireComposantInterfaceAjoute = new HashSet();
      request.getSession().setAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_AJOUTE,
          repertoireComposantInterfaceAjoute);
    }

    if (!repertoireComposantInterfaceAjoute.contains(identifiant)) {
      List listeComposantAAjoute = (List) repertoireComposantInterfaceAAjouter.get(identifiantParent);
      Long seqObjetSecurisableDisponible = (Long) request.getSession().getAttribute("seqObjetSecurisableDynamique");

      if (seqObjetSecurisableDisponible == null) {
        seqObjetSecurisableDisponible = new Long(-1);
        request.getSession().setAttribute("seqObjetSecurisableDynamique", seqObjetSecurisableDisponible);
      } else {
        seqObjetSecurisableDisponible = new Long(seqObjetSecurisableDisponible.longValue() - 1);
      }

      request.getSession().setAttribute("seqObjetSecurisableDynamique", seqObjetSecurisableDisponible);

      if (listeComposantAAjoute == null) {
        listeComposantAAjoute = new ArrayList();
      }

      ObjetSecurisable objetSecurisable = null;

      try {
        objetSecurisable = (ObjetSecurisable) typeObjet.newInstance();
        objetSecurisable.setSeqObjetSecurisable(seqObjetSecurisableDisponible);
        objetSecurisable.setNom(identifiant);
        objetSecurisable.setAdresseWeb(adresseWeb);
        objetSecurisable.setAideContextuelle(aideContextuelle);
        objetSecurisable.setOrdreAffichage(ordreAffichage);
        objetSecurisable.setListeObjetSecurisableEnfants(new ArrayList());
        listeComposantAAjoute.add(objetSecurisable);
      } catch (InstantiationException e) {
      } catch (IllegalAccessException e) {
      }

      repertoireComposantInterfaceAjoute.add(identifiant);
      repertoireComposantInterfaceAAjouter.put(identifiantParent, listeComposantAAjoute);
    }
  }

  /**
   * Fixer un attribut qui doit être logé temporairement dans la session pour une action bien précise.
   * 
   * @param identifiant
   *          l'identifiant de la valeur a mettre dans la session.
   * @param valeur
   *          la valeur a mettre dans la session.
   * @param request
   *          la requête en traitement.
   */
  public void setAttributTemporairePourAction(String identifiant, Object valeur, HttpServletRequest request) {
    UtilitaireControleur.setAttributTemporairePourAction(identifiant, valeur, request);
  }

  /**
   * Fixer un attribut de type ListeNavigation qui doit être logé temporairement dans la session pour une action bien
   * précise.
   * 
   * @param identifiant
   *          l'identifiant de la valeur a mettre dans la session.
   * @param valeur
   *          la valeur a mettre dans la session.
   * @param request
   *          la requête en traitement.
   */
  public void setAttributTemporaireListeNavigationPourAction(String identifiant, Object valeur,
      HttpServletRequest request) {
    // Accès à la liste des actions qui demande que la recherche soit traité.
    HashSet listeActionRechercheLance = (HashSet) request.getSession().getAttribute("SOFI_ListeActionRecherche");

    if (listeActionRechercheLance == null) {
      listeActionRechercheLance = new HashSet();
      request.getSession().setAttribute("SOFI_ListeActionRecherche", listeActionRechercheLance);
    }

    if (!isAjax(request) || (isAjax(request) && isInitialiserListeValeur(request))) {
      setAttributTemporairePourAction(identifiant, valeur, request);
    } else {
      request.setAttribute(identifiant, valeur);
    }

    if (valeur != null) {
      listeActionRechercheLance.add(identifiant);
    } else {
      listeActionRechercheLance.remove(identifiant);
    }
  }

  /**
   * Est-ce que l'on doit traiter un résultat de recherche? Normalement dans le cas affirmatif un appel doit être fait
   * vers la méthode qui charge la liste de recherche.
   * 
   * @return true si on doit traiter un résultat de recherche.
   * @param request
   *          la requête de l'utilisateur.
   * @param identifiantListe
   *          l'identifiant de la liste de navigation.
   */
  public boolean isTraiterResultatRecherche(String identifiantListe, HttpServletRequest request) {
    // Accès à la liste des actions qui demande que la recherche soit traité.
    HashSet listeActionRechercheLance = (HashSet) request.getSession().getAttribute("SOFI_ListeActionRecherche");

    // Accès au formulaire en cours.
    BaseForm formulaire = RequestProcessorHelper.getFormulaireCourant(request);

    boolean listeDansRequest = request.getAttribute(identifiantListe) != null;
    boolean listeDansSession = request.getSession().getAttribute(identifiantListe) != null;

    // Indicateur qui spécifie s'il est nécessaire de lancer une nouvelle
    // recherche.
    boolean actionRechercherRequis = (listeActionRechercheLance != null)
        && listeActionRechercheLance.contains(identifiantListe);

    if ((formulaire != null) && !formulaire.isFormulaireEnErreur()) {
      if (!listeDansRequest && !listeDansSession && actionRechercherRequis) {
        return true;
      }
    }

    return false;
  }

  /**
   * Fixer un attribut temporairement dans la session pour un service
   * 
   * @param identifiant
   *          l'identifiant de la valeur a mettre dans la session.
   * @param valeur
   *          la valeur a mettre dans la session.
   * @param request
   *          la requête en traitement.
   */
  public void setAttributTemporairePourService(String identifiant, Object valeur, HttpServletRequest request) {
    UtilitaireControleur.setAttributTemporairePourService(identifiant, valeur, request);
  }

  /**
   * Fixer un attribut temporairement dans la session pour un service et pour l'index en cours si formulaire imbriqué.
   * <p>
   * A utiliser lors de traitement ajax de liste déroulante par exemple.
   * 
   * @param identifiant
   *          l'identifiant de la valeur a mettre dans la session.
   * @param valeur
   *          la valeur a mettre dans la session.
   * @param traiterIndex
   *          ajouter l'index au nom de l'identifiant à fixer dans la session.
   * @param request
   *          la requête en traitement.
   */
  public void setAttributTemporairePourService(String identifiant, Object valeur, boolean traiterIndex,
      HttpServletRequest request) {
    if (traiterIndex) {
      String index = request.getParameter("index");

      if (index != null) {
        // Traiter la fixation d'attribut pour liste de valeur imbriqués.
        if (identifiant.indexOf(index) == -1) {
          identifiant += index;
        }
      }
    }

    setAttributTemporairePourService(identifiant, valeur, request);
  }

  /**
   * Fixer un document électronique pour affichage au client. Le type de document par défaut est PDF, pour un autre type
   * de document utiliser la méthode qui accepte le nom du document dans la session.
   * 
   * @param documentEletronique
   *          le document électronique.
   * @param request
   *          la requête en traitement.
   */
  public void setDocumentElectronique(DocumentElectronique documentEletronique, HttpServletRequest request) {
    // Fixer le document électronique temporaire pour l'action en traitement.
    setAttributTemporairePourAction(DocumentElectronique.DOCUMENT_PDF, documentEletronique, request);
  }

  /**
   * Fixer un document Pdf pour affichage au client.
   * 
   * @param valeur
   *          la valeur de PDF en byte[].
   * @param request
   *          la requête en traitement.
   * @since SOFI 2.0.2
   */
  public void setDocumentPdf(String nomFichier, byte[] valeur, HttpServletRequest request) {
    // Création d'un document électronique de type PDF.
    DocumentElectronique documentpdf = new DocumentElectronique();
    documentpdf.setNomFichier(nomFichier);
    documentpdf.setValeurDocument(valeur);
    documentpdf.setCodeTypeMime(UtilitaireDocumentElectronique.TYPE_CONTENU_PDF);

    // Fixer le document électronique temporaire pour l'action en traitement.
    setAttributTemporairePourAction(DocumentElectronique.DOCUMENT_PDF, documentpdf, request);
  }

  /**
   * Fixer un document électronique pour affichage au client.
   * 
   * @param nomDocumentDansSession
   *          Nom du document dans la session
   * @param documentEletronique
   *          le document électronique.
   * @param request
   *          la requête en traitement.
   */
  public void setDocumentElectronique(String nomDocumentDansSession, DocumentElectronique documentEletronique,
      HttpServletRequest request) {
    // Fixer le document électronique temporaire pour l'action en traitement.
    setAttributTemporairePourAction(nomDocumentDansSession, documentEletronique, request);
  }

  /**
   * Permet de modifier un libellé de composant du menu à partir d'une action.
   * 
   * @param request
   *          Requête Http
   * @param libelle
   *          Libellé qui sera affecté au composant de menu
   * @param identifiant
   *          le nom de l'identifiant du référentiel dont on désire modifié son nom.
   */
  public void modifierLibelleMenu(String identifiant, String libelle, HttpServletRequest request) {
    // Si un identifiant, extraire son numéro
    if (identifiant.indexOf(".") != -1) {
      ObjetSecurisable objetSecurisable = GestionSecurite.getInstance().getObjetSecurisable(identifiant);
      identifiant = objetSecurisable.getSeqObjetSecurisable().toString();
    }

    RepertoireMenu menu = RepertoireMenu.getRepertoireMenu(request);

    if (menu != null) {
      if (identifiant.indexOf("menu") == -1) {
        identifiant = "menu" + identifiant;
      }

      menu.modifierLibelleMenu(identifiant, libelle);
    }
  }

  /**
   * Permet de modifier un libelle et un url du service sélectionné du fil de navigation.
   * 
   * @param request
   *          Requête Http en traitement
   * @param parametresLibelleRemplacement
   *          les paramètres dynamiques au libellé.
   * @param url
   *          l'url du service
   * @param libelleRemplacement
   *          Libellé de remplacement pour le service.
   * @param identifiant
   *          le nom de l'identifiant du service dans le référentiel.
   */
  public void modifierServiceFilNavigation(String identifiant, String libelleRemplacement, String url,
      String[] parametresLibelleRemplacement, HttpServletRequest request) {
    // Fixer un hashmap avec les informations pour modifier dynamiquement le
    // service.
    HashMap libelleFilNavigation = new HashMap();
    libelleFilNavigation.put("libelle", libelleRemplacement);
    libelleFilNavigation.put("url", url);
    libelleFilNavigation.put("parametresLibelle", parametresLibelleRemplacement);
    setAttributTemporairePourService("LIBELLE_A_MODIFIER_" + identifiant, libelleFilNavigation, request);
  }

  /**
   * Spécifier si le traitement courant a traiter est Ajax.
   * 
   * @return true si le traitement courant a traiter est Ajax.
   * @param request
   *          la requête présentement en cours.
   */
  protected boolean isAjax(HttpServletRequest request) {
    if ((request.getParameter("ajax") != null)
        || (request.getSession().getAttribute(Constantes.AFFICHAGE_AJAX) != null)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Spécifier si le traitement courant est un rafraichissement automatique ajax.
   * 
   * @return true si le traitement courant est un rafraichissement automatique ajax.
   * @param request
   *          la requête présentement en cours.
   */
  protected boolean isRafraichissementAjax(HttpServletRequest request) {
    return UtilitaireControleur.isRafraichissementAjax(request);
  }

  /**
   * Spécifier si le traitement courant a été déclenché par un div persistant.
   * 
   * @return true si le traitement courant a été déclenché par un div persistant.
   * @param request
   *          la requête présentement en cours.
   */
  protected boolean isDivPersistantAjax(HttpServletRequest request) {
    return UtilitaireControleur.isDivPersistantAjax(request);
  }

  /**
   * Retourne l'emplacement courant des fichiers.
   * 
   * @return l'emplacement courant des fichiers.
   */
  protected String getEmplacementFichier() {
    return getServlet().getServletContext().getRealPath("");
  }

  /**
   * Retourne la page de saisie.
   * 
   * @return la page de saisie.
   * @param mapping
   *          l'instance de mapping.
   */
  protected ActionForward getPageSaisie(ActionMapping mapping) {
    return new ActionForward(mapping.getInput());
  }

  /**
   * Est-ce que la chaine de caratères est vide.
   * 
   * @return true si la chaine de caratères est vide.
   * @param chaine
   *          la chaine de caractères.
   */
  protected boolean isVide(String chaine) {
    return (UtilitaireString.isVide(chaine));
  }

  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax. Cette méthode permet aussi de redéfinir
   * les proprietes de la liste déroulante. La propriete classname,disabled sont supportés...
   * 
   * @param response
   *          la réponse de l'utilisateur.
   * @param request
   *          la requête de l'utilisateur.
   * @param libelleLigneVide
   *          le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide
   *          le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param itemValeur
   *          la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param itemNom
   *          la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection
   *          la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   * @param proprieteValeur
   *          la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom
   *          la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param proprietesListeDeroulante
   *          la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   */
  protected void genererReponseListeDeroulante(Collection collection, String itemNom, String itemValeur,
      String libelleLigneVide, String libelleResultatListeVide, Collection proprietesListeDeroulante,
      String proprieteNom, String proprieteValeur, HttpServletRequest request, HttpServletResponse response) {
    UtilitaireListeDeroulante.genererReponseListeDeroulante(collection, itemNom, itemValeur, libelleLigneVide,
        libelleResultatListeVide, proprietesListeDeroulante, proprieteNom, proprieteValeur, request, response);
  }
  
  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * 
   * @param response
   *          la réponse de l'utilisateur.
   * @param request
   *          la requête de l'utilisateur.
   * @param libelleLigneVide
   *          le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide
   *          le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param proprieteValeur
   *          la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom
   *          la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection
   *          la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   */
  protected void genererReponseListeDeroulante(Collection collection, String proprieteNom, String proprieteValeur,
      String libelleLigneVide, String libelleResultatListeVide, HttpServletRequest request, HttpServletResponse response) {
    UtilitaireListeDeroulante.genererReponseListeDeroulante(collection, proprieteNom, proprieteValeur,
        libelleLigneVide, libelleResultatListeVide, false, request, response);
  }  

  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * 
   * @param response
   *          la réponse de l'utilisateur.
   * @param request
   *          la requête de l'utilisateur.
   * @param libelleLigneVide
   *          le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide
   *          le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param proprieteValeur
   *          la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom
   *          la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection
   *          la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   * @param utiliserDescriptionLocale 
   *          boolean indiquant si on doit utiliser la description locale ou non.
   */
  protected void genererReponseListeDeroulante(Collection collection, String proprieteNom, String proprieteValeur,
      String libelleLigneVide, String libelleResultatListeVide, Boolean utiliserDescriptionLocale, HttpServletRequest request, HttpServletResponse response) {
    UtilitaireListeDeroulante.genererReponseListeDeroulante(collection, proprieteNom, proprieteValeur,
        libelleLigneVide, libelleResultatListeVide, utiliserDescriptionLocale, request, response);
  }

  /**
   * Ajouter une valeur associé à un nom d'identifiant HTML.
   * 
   * @param response
   *          la réponse de l'utilisateur.
   * @param request
   *          la requête de l'utilisateur.
   * @param valeur
   *          la valeur spécifié.
   * @param identifiant
   *          l'identifiant spécifié.
   */
  protected void ajouterValeurDansReponse(String identifiant, String valeur, HttpServletRequest request,
      HttpServletResponse response) {
    ajouterValeurDansReponse(identifiant, valeur, true, request, response);
  }

  /**
   * Ajouter une valeur associé à un nom d'identifiant d'une page HTML.
   * 
   * @param identifiant
   *          l'identifiant spécifié.
   * @param valeur
   *          la valeur spécifié.
   * @param contientDonnees
   *          true si la valeur contient des données de type XML
   * @param request
   *          la requête en traitement.
   * @param response
   *          la réponse en traitement.
   */
  protected void ajouterValeurDansReponse(String identifiant, String valeur, boolean contientDonnees,
      HttpServletRequest request, HttpServletResponse response) {
    GenerateurXmlAjax generateurXmlAjax = (GenerateurXmlAjax) request.getAttribute("sofiGenerateurXmlAjax");

    if (generateurXmlAjax == null) {
      generateurXmlAjax = new GenerateurXmlAjax();
      request.setAttribute("sofiGenerateurXmlAjax", generateurXmlAjax);
    }

    if (contientDonnees) {
      generateurXmlAjax.ajouterItemAvecDonnees(identifiant, valeur);
    } else {
      generateurXmlAjax.ajouterItem(identifiant, valeur);
    }
  }

  /**
   * Générer un document XML incluant des valeurs multiplue dans la réponse afin de rafrachir plusieurs champs dans la
   * page en mode ajax.
   * 
   * @param response
   *          la réponse de l'utilisateur.
   * @param request
   *          la requête de l'utilisateur.
   */
  protected void genererReponseXMLAvecValeurs(HttpServletRequest request, HttpServletResponse response) {
    // Envoie du contenu xml
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    GenerateurXmlAjax generateurXmlAjax = (GenerateurXmlAjax) request.getAttribute("sofiGenerateurXmlAjax");

    if (generateurXmlAjax != null) {
      try {
        // Accéder au document XML.
        String documentXml = generateurXmlAjax.genererEnXml();

        // Écrire la réponse avec le document XML.
        PrintWriter pw = response.getWriter();
        pw.write(documentXml);
        pw.close();
      } catch (IOException e) {
      }
    }
  }
  
  /**
   * Permet d'écrire une valeur brut dans la réponse.
   * @param valeur la valeur brut.
   * @param response la reponse en cours de traitement.
   * @since 3.2
   */
  protected void ecrireReponse(String valeur, HttpServletResponse response) {
    // Envoie du contenu xml
    response.setContentType("text/html; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    
    try {
      // Écrire la réponse avec le document XML.
      PrintWriter pw = response.getWriter();
      pw.write(valeur);
      pw.close();
    } catch (IOException e) {
    }
  }

  /**
   * Générer une réponse avec une valeur devant rafrachir un champ dans un page en mode ajax.
   * 
   * @param response
   *          la réponse à l'utilisateur.
   * @param valeur
   *          la valeur à écrire dans la réponse.
   */
  protected void genererReponseAvecValeur(String valeur, boolean reponseHtml, HttpServletResponse response) {
    // Envoie du contenu xml
    if (reponseHtml) {
      response.setContentType("text/html");
      valeur = UtilitaireString.convertirEnHtml(valeur);
    } else {
      response.setContentType("text/plain; charset=UTF-8");
    }

    response.setHeader("Cache-Control", "no-cache");

    try {
      PrintWriter pw = response.getWriter();
      pw.write(valeur);
      pw.close();
    } catch (IOException e) {
    }
  }

  /**
   * Envoyer le document électronique dans la réponse de l'utilisateur.
   * 
   * @param documentEletronique
   *          le document électronique.
   * @param request
   *          la requête en traitement.
   * @param response
   *          la réponse à l'utilisateur
   */
  public void envoyerDocumentElectroniqueDansReponse(DocumentElectronique documentEletronique,
      HttpServletRequest request, HttpServletResponse response) throws IOException {
    envoyerDocumentElectroniqueDansReponse(documentEletronique.getValeurDocument(),
        documentEletronique.getNomFichier(), request, response);
  }

  /**
   * Envoyer le document électronique dans la réponse de l'utilisateur.
   * 
   * @param response
   *          la requête en traitement.
   * @param request
   *          la réponse à l'utilisateur
   * @param nomFichier
   *          le nom du fichier du document.
   * @param document
   *          le document.
   */
  public void envoyerDocumentElectroniqueDansReponse(byte[] document, String nomFichier, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    UtilitaireDocumentElectronique utilDE = new UtilitaireDocumentElectronique(request, response, document, nomFichier);
    utilDE.ecrireContenuFichierAuClient();
  }

  /**
   * Fixer la navigation par étape.
   * 
   * @param request
   *          la requête en traitement.
   * @param urlEnregistre
   *          l'url qui permet l'enregistrement.
   * @param suivantInactifSiFormulaireLectureSeulement
   *          Est-ce que l'étape suivante doit être inactivé si le formulaire en lecture seulement.
   * @param urlSuivant
   *          l'url de l'étape suivante.
   * @param precedentInactifSiFormulaireLectureSeulement
   *          Est-ce que l'étape précédente doit être inactivé si le formulaire en lecture seulement.
   * @param urlPrecedent
   *          l'url de l'étape précédente.
   */
  protected void fixerNavigationParEtape(String urlPrecedent, boolean precedentInactifSiFormulaireLectureSeulement,
      String urlSuivant, boolean suivantInactifSiFormulaireLectureSeulement, String urlEnregistre,
      HttpServletRequest request) {
    NavigationParEtape navigation = (NavigationParEtape) request.getSession().getAttribute("sofi_navigation");

    if (navigation == null) {
      // Création d'une nouvelle navigation par étape.
      navigation = new NavigationParEtape();
    }

    navigation.setUrlPrecedent(urlPrecedent);
    navigation.setUrlSuivant(urlSuivant);
    navigation.setUrlEnregistrer(urlEnregistre);
    navigation.setPrecedentInactifSiFormulaireLectureSeulement(precedentInactifSiFormulaireLectureSeulement);
    navigation.setSuivantInactifSiFormulaireLectureSeulement(suivantInactifSiFormulaireLectureSeulement);

    // Fixer la navigation temporaire à l'action en cours.
    request.getSession().setAttribute("sofi_navigation", navigation);
  }

  /**
   * Fixer des libellés temporaire pour une action d'un formulaire avec la navigation par étape.
   * 
   * @param request
   *          la requête en traitement.
   * @param libelleEnregistre
   *          l'url qui permet l'enregistrement.
   * @param libelleSuivant
   *          l'url de l'étape suivante.
   * @param libellePrecedent
   *          l'url de l'étape précédente.
   */
  protected void fixerLibellePourActionNavigationParEtape(String libellePrecedent, String libelleSuivant,
      String libelleEnregistre, HttpServletRequest request) {
    // Fixer le libellé précédent temporaire.
    request.setAttribute("sofi_navigation_etape_libelle_precedent", libellePrecedent);

    // Fixer le libellé suivant temporaire.
    request.setAttribute("sofi_navigation_etape_libelle_suivant", libelleSuivant);

    // Fixer le libellé d'enregistrement temporaire.
    request.setAttribute("sofi_navigation_etape_libelle_enregistre", libelleEnregistre);
  }

  /**
   * Permet d'initialiser la navigation d'un formulaire étape par étape.
   * 
   * @param request
   *          la requête en traitement.
   * 
   * @param libelleSuivant
   *          le libellé de l'étape suivante.
   * @param libellePrecedent
   *          le libellé de l'étape précédente.
   * @param libelleEnregistre
   *          le libellé du bouton enregistrer.
   * @param urlQuitter
   *          l'url qui permet de quitter le formulaire étape par étape.
   * @param libelleQuitter
   *          le libellé a spécifié pour l'action de quitter l'assistant étape par étape.
   */
  protected void initialiserNavigationParEtape(String libellePrecedent, String libelleSuivant,
      String libelleEnregistre, String libelleQuitter, String urlQuitter, HttpServletRequest request) {
    // Accès au composant de navigation étape par étape.
    NavigationParEtape navigation = (NavigationParEtape) request.getSession().getAttribute("sofi_navigation");

    if (navigation == null) {
      // Création d'une nouvelle navigation par étape.
      navigation = new NavigationParEtape();
    }

    if (navigation.getLibelleUrlPrececent() == null) {
      if (libellePrecedent != null) {
        navigation.setLibelleUrlPrececent(libellePrecedent);
      }

      if (libelleSuivant != null) {
        navigation.setLibelleUrlSuivant(libelleSuivant);
      }

      if (libelleEnregistre != null) {
        navigation.setLibelleUrlEnregistre(libelleEnregistre);
      }

      if (urlQuitter != null) {
        navigation.setUrlQuitter(urlQuitter);
      }

      if (libelleQuitter != null) {
        navigation.setLibelleQuitter(libelleQuitter);
      }

      // Fixer la navigation temporaire.
      request.getSession().setAttribute("sofi_navigation", navigation);
    }
  }

  /**
   * Initialiser les libellés par défaut lorsque le formulaire est en mode lecture seulement.
   * 
   * @param request
   *          la requête en traitement.
   * @param libelleSuivantLectureSeulement
   *          le libellé de l'étape suivante en mode lecture seulement.
   * @param libellePrecedentLectureSeulement
   *          le libellé de l'étape précédente en mode lecture seulement.
   */
  protected void initialiserNavigationParEtapeEnLectureSeulement(String libellePrecedentLectureSeulement,
      String libelleSuivantLectureSeulement, HttpServletRequest request) {
    // Accès au composant de navigation étape par étape.
    NavigationParEtape navigation = (NavigationParEtape) request.getSession().getAttribute("sofi_navigation");

    if (navigation == null) {
      // Création d'une nouvelle navigation par étape.
      navigation = new NavigationParEtape();
    }

    if (navigation.getLibelleUrlPrecedentLectureSeulement() == null) {
      if (libellePrecedentLectureSeulement != null) {
        navigation.setLibelleUrlPrecedentLectureSeulement(libellePrecedentLectureSeulement);
      }

      if (libelleSuivantLectureSeulement != null) {
        navigation.setLibelleUrlSuivantLectureSeulement(libelleSuivantLectureSeulement);
      }

      // Fixer la navigation temporaire.
      request.getSession().setAttribute("sofi_navigation", navigation);
    }
  }

  /**
   * Initialiser la navigation étape par étape afin d'appliquer une nouvelle initialisation avec des valeurs par défaut.
   * 
   * @param request
   *          la requête en traitement.
   */
  protected void initialiserNavigationParEtape(HttpServletRequest request) {
    request.getSession().setAttribute("sofi_navigation", null);
  }

  /**
   * Retourne la prochaine étape à suivre.
   * 
   * @return la prochaine étape à suivre.
   * @param request
   *          la requête en traitement.
   */
  protected ActionForward getProchaineNavigationParEtape(HttpServletRequest request) {
    NavigationParEtape navigation = (NavigationParEtape) request.getSession().getAttribute("sofi_navigation");
    String prochainePage = request.getParameter("prochainePage");
    ActionForward retour = null;

    if (prochainePage != null) {
      if ("suivant".equals(prochainePage)) {
        retour = new ActionForward(navigation.getUrlSuivant());
        retour.setRedirect(true);
      }

      if ("precedent".equals(prochainePage)) {
        retour = new ActionForward(navigation.getUrlPrecedent());
        retour.setRedirect(true);
      }
    }

    return retour;
  }

  /**
   * Retourne une référence sur l'utilitaire qui permet la manipulation de tableau.
   * 
   * @return une référence sur l'utilitaire qui permet la manipulation de tableau.
   */
  protected UtilitaireTableau getUtilitaireTableau() {
    UtilitaireTableau utilitaireTableau = null;

    return utilitaireTableau;
  }

  /**
   * Retourne une référence sur l'utilitaire qui permet la manipulation des nombres.
   * 
   * @return une référence sur l'utilitaire qui permet la manipulation des nombres.
   */
  protected UtilitaireNombre getUtilitaireNombre() {
    UtilitaireNombre utilitaireNombre = null;

    return utilitaireNombre;
  }

  /**
   * Fixer le locale dans la session pour SOFI et JSTL
   * 
   * @param locale
   *          le locale à fixer
   * @param request
   *          la requête en traitement.
   */
  @Override
  public void setLocale(HttpServletRequest request, Locale locale) {
    UtilitaireControleur.setLocale(locale, request.getSession());
  }

  /**
   * Retourne la dernière vue affiche a l'utilisateur selon le mode désiré, soit en redirection ou pas en redirection.
   * 
   * @return la dernière vue affiche a l'utilisateur
   * @param request
   *          la requête de l'utilisateur.
   */
  public ActionForward getDerniereVueAffiche(boolean modeRedirection, HttpServletRequest request) {
    String derniereAdresse = (String) request.getSession().getAttribute(Constantes.ADRESSE_ACTION_AFFICHE);
    ActionForward forward = new ActionForward(derniereAdresse);

    if (modeRedirection) {
      forward.setRedirect(true);
    }

    return forward;
  }

  /**
   * Commit de la transaction en cours
   * <p>
   * Si la transaction est associé un formulaire transactionnel, utiliser plutot
   * <code>commit(request, formulaire);</code>
   * 
   * @param request
   *          la requête en traitement
   */
  public void commit(HttpServletRequest request) {
    this.getModele(request).commit();
    saveToken(request);
    request.setAttribute("commitAvecToken", Boolean.TRUE);
  }

  /**
   * Commit de la transaction en cours avec ou sans confirmation que l'opération a été effectué avec succès.
   * 
   * @param request
   *          la requête en traitement
   * @param formulaire
   *          le formulaire
   */
  public void commit(HttpServletRequest request, BaseForm formulaire, boolean confirmation) {
    commit(request, formulaire, null, null, confirmation);
  }

  /**
   * Commit de la transaction en cours avec ou sans confirmation que l'opération a été effectué avec succès.
   * 
   * @param request
   *          la requête en traitement
   * @param formulaire
   *          le formulaire
   */
  public void commit(HttpServletRequest request, BaseForm formulaire, String cleMessage, Object[] parametres,
      boolean confirmation) {
    if (!formulaire.isModeLectureSeulement()) {
      this.commit(request);

      if (confirmation) {
        String cleClient = null;

        if (formulaire.isModifie() || formulaire.isNouveauFormulaire()) {
          Message message = (Message) request.getSession().getAttribute(Constantes.MESSAGE_BARRE_STATUT);

          if (message == null && cleMessage == null) {
            // Traiter le message d'opération général traité avec succès
            cleClient = GestionMessage.getInstance().getFichierConfiguration()
                .get(ConstantesMessage.INFORMATION_OPERATION_EFFECTUE_SUCCES, request);

            if (!UtilitaireString.isVide(cleClient)) {
              formulaire.ajouterMessageInformatif(cleClient, request);
            }
          } else {
            if (cleMessage != null) {
              formulaire.ajouterMessageInformatif(cleMessage, parametres, request);
            } else {
              formulaire.ajouterMessageInformatif(message.getCleMessage(), request);
            }
          }
        } else {
          if ((formulaire.getLesInformations() == null) || (formulaire.getLesInformations().size() == 0)) {
            // Traiter le message d'opération général traité avec succès
            cleClient = GestionMessage.getInstance().getFichierConfiguration()
                .get(ConstantesMessage.INFORMATION_OPERATION_TRANSACTION_NON_MODIFIE, request);

            // Extraire si paramètre système.
            String cleMessageInformationAucuneModificationFormulaire = GestionParametreSysteme.getInstance().getString(
                ConstantesParametreSysteme.CLE_MESSAGE_INFORMATION_AUCUNE_MODIFICATION_FORMULAIRE);

            if (UtilitaireString.isVide(cleMessageInformationAucuneModificationFormulaire)) {
              cleClient = GestionMessage.getInstance().getFichierConfiguration()
                  .get(ConstantesMessage.INFORMATION_OPERATION_TRANSACTION_NON_MODIFIE, request);
            } else {
              cleClient = GestionMessage.getInstance().getFichierConfiguration()
                  .get(cleMessageInformationAucuneModificationFormulaire, request);
            }

            Message messageModification = UtilitaireMessage.get(cleClient, request);

            if (!UtilitaireString.isVide(cleClient)) {
              formulaire.ajouterMessageInformatif(cleClient, request);
            }
          }
        }
      }

      formulaire.setIndModification("0");
      formulaire.setNouveauFormulaire(false);

      if (formulaire.getObjetTransfert() != null) {
        formulaire.fixerObjetTransfertOriginal();
      }
    }
  }

  /**
   * Commit de la transaction en cours avec confirmation que l'opération a été effectué avec succès.
   * 
   * @param request
   *          la requête en traitement
   */
  public void commit(HttpServletRequest request, BaseForm formulaire) {
    this.commit(request, formulaire, true);
  }

  /**
   * Commit de la transaction en cours avec confirmation que l'opération a été effectué avec succès.
   * 
   * @param request
   *          la requête en traitement
   */
  public void commit(HttpServletRequest request, BaseForm formulaire, String cleMessage, Object[] parametres) {
    this.commit(request, formulaire, cleMessage, parametres, true);
  }

  /**
   * Commit de la transaction avec message personnalisé.
   * 
   * @param request
   *          a requête en traitement
   * @param formulaire
   *          le formulaire en traitement
   * @param cleMessage
   *          la clé du message
   * @param parametres
   *          tableau des paramètres dynamique.
   */
  public void commit(HttpServletRequest request, String cleMessage, Object[] parametres) {
    this.commit(request);

    Message message = UtilitaireMessage.get(cleMessage, parametres, request);
    request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT, message);
  }

  /**
   * Rollback de la transaction en cours
   * 
   * @param request
   *          la requête en traitement
   */
  public void rollback(HttpServletRequest request) {
    this.getModele(request).rollback();
  }

  /**
   * Rollback de la transaction en cours
   * 
   * @param request
   *          la requête en traitement
   */
  public void rollback(HttpServletRequest request, String cleMessage, Object[] parametres) {
    this.rollback(request);

    Message message = UtilitaireMessage.get(cleMessage, parametres, request);
    request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT, message);
  }

  /**
   * Est-ce que l'on doit traiter un div en mode Ajax.
   * 
   * @return true si on doit traiter un div en mode Ajax.
   * @param request
   *          la requête de l'utilisateur.
   */
  public boolean isTraiterDivAjax(HttpServletRequest request) {
    if (request.getParameter(ConstantesBaliseJSP.PARAM_AJAX_DIV) != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le traitement est cours est une soumission de formulaire?
   * 
   * @return true si le traitement est cours est une soumission de formulaire.
   * @param request
   *          la requête de l'utilisateur.
   */
  public boolean isTraitementPost(HttpServletRequest request) {
    return UtilitaireControleur.isTraitementPost(request);
  }

  /**
   * Ajout d'un message d'erreur dans la console de surveillance.
   * 
   * @param erreur
   *          l'exception
   * @param request
   *          la requête en traitement.
   */
  protected void ajouterErreurSurveillance(HttpServletRequest request, Throwable erreur) {
    StringWriter writer = new StringWriter();
    PrintWriter prn = new PrintWriter(writer);
    erreur.printStackTrace(prn);
    prn.flush();

    String messageErreur = writer.toString();
    ErreurApplication erreurApplication = new ErreurApplication();
    erreurApplication.setMethode((String) request.getAttribute("adresseActionSelectionne"));
    erreurApplication.setDateHeure(new Date(UtilitaireDate.getDateJourAvecHeure().getTime()));
    erreurApplication.setMessage(messageErreur);
    GestionSurveillance.getInstance().ajouterPageEnEchec(erreurApplication);
  }

  /**
   * Permet de générer la réponse qui doit être affiché à l'utilisateur lors d'un traitement de liste de valeur en mode
   * ajax.
   * 
   * @return La vue a afficher à l'utilisateur.
   * @param response
   *          la réponse en traitement.
   * @param request
   *          la requête en traitement.
   * @param form
   *          le formulaire en traitement.
   * @param mapping
   *          le mapping de l'action en traitement.
   * @param liste
   *          la liste résultante à traiter par la liste de valeurs.
   * @param cleMessageErreur
   *          la clé du message d'erreur à afficher lorsque le résultat de la liste est vide.
   * @param listeAttributListeNavigation
   *          la liste des attributs correpondant dans la liste de navigation.
   * @param nomVueAjax
   *          le nom de la vue ajax a afficher s'il existe plusieurs résultat de la liste de valeurs.
   */
  protected ActionForward genererReponseListeValeurAjax(String nomVueAjax, String[] listeAttributListeNavigation,
      String cleMessageErreur, ListeNavigation liste, ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) {
    // Extraire la liste des attributs qui doivent être retourné vers le
    // formulaire.
    String attributRetour = request.getParameter("attributRetour");

    if (attributRetour != null) {
      request.getSession().setAttribute("attributRetourListeNavigation", attributRetour);
    } else {
      attributRetour = (String) request.getSession().getAttribute("attributRetourListeNavigation");
    }

    CorrespondancePropriete[] listeCorrespondancePropriete = new CorrespondancePropriete[listeAttributListeNavigation.length];

    StringTokenizer listeAttributRetour = new StringTokenizer(attributRetour, ",");
    int i = 0;

    while (listeAttributRetour.hasMoreTokens()) {
      String token = listeAttributRetour.nextToken();
      CorrespondancePropriete correspondancePropriete = new CorrespondancePropriete(token,
          listeAttributListeNavigation[i]);
      listeCorrespondancePropriete[i] = correspondancePropriete;
      i++;
    }

    // Générer la réponse ajax.
    ReponseListeValeurAjax reponseListeParentAjax = new ReponseListeValeurAjax(nomVueAjax,
        listeCorrespondancePropriete, cleMessageErreur);

    String nomFormulaireRetour = request.getParameter("formulaireValeurRetour");

    if (nomFormulaireRetour != null) {
      form = (BaseForm) request.getSession().getAttribute(nomFormulaireRetour);
    }

    ActionForward retourAjax = reponseListeParentAjax.generer(liste, mapping, (BaseForm) form, request, response);

    return retourAjax;
  }

  /**
   * Est-ce que le div est ouvert?
   * 
   * @return true si le div est ouvert.
   * @param request
   *          la requête en traitement.
   * @param idDiv
   *          l'identifiant du div.
   */
  protected boolean isDivOuvert(String idDiv, HttpServletRequest request) {
    if ((UtilitaireControleur.isDivOuvert(idDiv, request) != null)
        && UtilitaireControleur.isDivOuvert(idDiv, request).booleanValue()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le div est initialisé? Si la page n'a jamais été consulté le DIV ne devrait pas avoir été initialisé
   * 
   * @return true si le div est initialisé.
   * @param request
   *          la requête en traitement.
   * @param idDiv
   *          l'identifiant du div.
   */
  protected boolean isDivInitialise(String idDiv, HttpServletRequest request) {
    if (UtilitaireControleur.isDivOuvert(idDiv, request) == null) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Est-ce l'initialisation d'une nouvelle demande de liste de valeurs.
   * 
   * @return true si une initialisation d'une nouvelle demande de liste de valeurs.
   * @param request
   *          la requête en traitement.
   */
  protected boolean isInitialiserListeValeur(HttpServletRequest request) {
    return request.getParameter("sofi_initialiser_liste") != null;
  }

  /**
   * Cette méthode permet d'ajouter un message d'erreur au niveau de la request.
   * <p>
   * 
   * @param request
   *          request la requête en traitement
   * @param parametres
   *          les paramètres du message d'erreur
   * @param cleMessage
   *          la clé du message d'erreur
   */
  public void ajouterMessageErreur(String cleMessage, String[] parametres, HttpServletRequest request) {
    Message message = UtilitaireMessage.get(cleMessage, parametres, request);
    request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT, message);
  }

  /**
   * Permet d'appliquer une liste de navigation associé avec un objet filtre. Cette méthode est la plupart du temps
   * utiliser avec une liste de valeurs. Les paramètres recu par la liste de valeurs en GET sont automatiquement populé
   * dans l'objet filtre. Il est de plus possible de fixer un prefixe que vous utiliser pour tous vos paramètre de la
   * liste de valeurs.
   * <p>
   * Pour que le traitement puisse fonctionner, il est important que les noms de paramètre soient identitique au
   * propriété correspondante dans l'objet filtre. De plus, assurez-vous de bien fixer, au minimum, la liste de
   * navigation dans la portée Action (setAttributTemporairePourAction).
   * 
   * @return l'objet filtre associé à la liste de navigation.
   * @param request
   *          la requête en traitement.
   * @param prefixParametre
   *          le préfix utiliser pour les paramètres qui doit être populé dans l'objet filtre.
   * @param typeObjetFiltre
   *          le type d'objet filtre a traiter.
   * @param idListeNavigation
   *          l'identifiant de la liste navigation dans la session.
   * @since SOFI 2.0.1
   */
  protected ListeNavigation appliquerListeNavigationAvecObjetFiltre(String idListeNavigation, Class typeObjetFiltre,
      String prefixParametre, HttpServletRequest request) {
    // Appliquer la liste de navigation page par page.
    ListeNavigation liste = (ListeNavigation) request.getSession().getAttribute(idListeNavigation);
    ObjetFiltre filtre = null;

    if (isInitialiserListeValeur(request) || (liste == null)) {
      try {
        filtre = (ObjetFiltre) typeObjetFiltre.newInstance();
      } catch (InstantiationException e) {
      } catch (IllegalAccessException e) {
      }
    } else {
      filtre = liste.getObjetFiltre();
    }

    if (request.getParameter("listeId") == null) {
      Enumeration listeParametre = request.getParameterNames();

      while (listeParametre.hasMoreElements()) {
        String parametre = (String) listeParametre.nextElement();
        String valeurParametre = request.getParameter(parametre);

        if ((prefixParametre != null) && (parametre.indexOf(prefixParametre) != -1)) {
          parametre = parametre.substring(5);

          if ((parametre != null) && (parametre.length() > 1)) {
            parametre = UtilitaireString.convertirPremiereLettreEnMinuscule(parametre);
          }
        }

        // Populer la correspondance dans l'objet filtre.
        try {
          if (!isVide(valeurParametre)) {
            UtilitaireObjet.setPropriete(filtre, parametre, valeurParametre);
          } else {
            // Initialiser la valeur du filtre si champ à blanc.
            UtilitaireObjet.setPropriete(filtre, parametre, null);
          }
        } catch (InvocationTargetException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
          try {
            // Valider aussi les types Long.
            if (UtilitaireObjet.getClasse(filtre, parametre).isAssignableFrom(Long.class)) {
              try {
                UtilitaireObjet.setPropriete(filtre, parametre, new Long(valeurParametre));
              } catch (Exception e2) {
                UtilitaireObjet.setPropriete(filtre, parametre, new Long(-1));
              }
            }

            // Valider aussi les types Integer.
            if (UtilitaireObjet.getClasse(filtre, parametre).isAssignableFrom(Integer.class)) {
              try {
                UtilitaireObjet.setPropriete(filtre, parametre, new Integer(valeurParametre));
              } catch (Exception e3) {
                UtilitaireObjet.setPropriete(filtre, parametre, new Integer(-1));
              }
            }

            // Valider aussi les types String[].
            if (UtilitaireObjet.getClasse(filtre, parametre).isAssignableFrom(String[].class)) {
              try {
                String[] valeur = new String[1];
                valeur[0] = valeurParametre;
                UtilitaireObjet.setPropriete(filtre, parametre, valeur);
              } catch (Exception e3) {
              }
            }

            // Valider aussi les types Integer[].
            if (UtilitaireObjet.getClasse(filtre, parametre).isAssignableFrom(Integer[].class)) {
              try {
                Integer[] valeur = new Integer[1];
                valeur[0] = new Integer(valeurParametre);
                UtilitaireObjet.setPropriete(filtre, parametre, valeur);
              } catch (Exception e3) {
              }
            }

            // Valider aussi les types Long[].
            if (UtilitaireObjet.getClasse(filtre, parametre).isAssignableFrom(Long[].class)) {
              try {
                Long[] valeur = new Long[1];
                valeur[0] = new Long(valeurParametre);
                UtilitaireObjet.setPropriete(filtre, parametre, valeur);
              } catch (Exception e3) {
              }
            }
          } catch (Exception e4) {
          }
        }
      }
    }

    // Appliquer la liste de navigation.
    liste = appliquerListeNavigation(request);

    if (request.getParameter("initialiserNoPage") != null) {
      liste.setNoPage(1);
    }

    liste.setObjetFiltre(filtre);

    // Fixer la liste de navigation pour le traitement.
    setAttributTemporaireListeNavigationPourAction(idListeNavigation, liste, request);

    return liste;
  }

  /**
   * Permet d'appliquer une liste de navigation associé avec un objet filtre qui lui est normalement associé à une liste
   * de navigation.
   * <p>
   * En utilisant cette méthode vous être assuré que le formulaire sera synchronisé avec l'objet filtre, et ce malgré
   * une fin de session de l'utilisateur. Dans ce cas, l'objet filtre sera instancié et populer dans le formulaire.
   * 
   * @param request
   *          la requête Http en traitement.
   * @param typeObjetFiltre
   *          le type d'objet filtre a traiter.
   * @param liste
   *          la liste qui doit inclure l'objet filtre.
   * @param formulaire
   *          le formulaire a traiter.
   * @since SOFI 2.0.1
   */
  protected void appliquerFormulaireAvecObjetFiltre(BaseForm formulaire, ListeNavigation liste, Class typeObjetFiltre,
      HttpServletRequest request) {
    // Extraire l'objet filtre du formulaire.
    ObjetFiltre objetFiltre = (ObjetFiltre) formulaire.getObjetTransfert();

    if (objetFiltre == null) {
      try {
        objetFiltre = (ObjetFiltre) typeObjetFiltre.newInstance();
      } catch (InstantiationException e) {
        throw new SOFIException("Le type de l'objet filtre est non valide", e);
      } catch (IllegalAccessException e) {
        throw new SOFIException("Le type de l'objet filtre est non valide", e);
      }
    }

    formulaire.populerFormulaire(objetFiltre, request);
    liste.setObjetFiltre(objetFiltre);
  }

  /**
   * Méthode permettant prendre le document pdf conservé en session et de l'écrire dans la répponse à l'utilisateur. Il
   * pourra ensuite le consulter directement ou l'enregistrer.
   * <p>
   * 
   * @since SOFI 2.0.2
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donnée à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward ecrireReponsePdf(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    // Écrire le document dans la réponse.
    this.envoyerDocumentElectroniqueDansReponse(
        (DocumentElectronique) request.getSession().getAttribute(DocumentElectronique.DOCUMENT_PDF), request, response);

    // Après écriture au complet, supprimer de la session.
    request.getSession().setAttribute(DocumentElectronique.DOCUMENT_PDF, null);

    return mapping.findForward(null);
  }

  /**
   * Spécifie si l'utilisateur accède à un composant qui nécessiste la lecture seulement.
   * <p>
   * 
   * @since SOFI 2.0.2
   */
  protected boolean isAccesEnLectureSeulement(HttpServletRequest request) {
    return UtilitaireControleur.isAccesEnLectureSeulement(request);
  }

  /**
   * Retourne une instance de l'utilitaire sur JasperReports.
   * 
   * @return une instance de l'utilitaire sur JasperReports.
   * @since SOFI 2.1
   */
  public UtilitaireJasperReports getUtilitaireJasperReports() {
    return UtilitaireJasperReports.getInstance();
  }

  /**
   * Retourne une instance de l'utilitaire sur les traitements dans le contrôleur.
   * 
   * @return une instance de l'utilitaire sur les traitements dans le contrôleur.
   * @since SOFI 2.1
   */
  public UtilitaireControleur getUtilitaireControleur() {
    return UtilitaireControleur.getInstance();
  }

  /**
   * Retourne la vue à selectionner selon le résultat de recherche.
   * 
   * @param mapping
   *          le mapping en cours.
   * @param liste
   *          la liste en traitement.
   * @param vueResultatMultiple
   *          la vue a appeler si résultat multiple.
   * @param vueResultatSimple
   *          la vue a appeler si résultat unique.
   * @param cleParam
   *          la clé
   * @param valeurParam
   * @return la vue a appeler (en redirection).
   * @since 3.0.2
   */
  public ActionForward traiterResultatRecherche(ListeNavigation liste, String vueResultatMultiple,
      String vueResultatSimple, String cleParam, String valeurParam, ActionMapping mapping, HttpServletRequest request) {
    HashMap<String, String> parametres = new HashMap<String, String>();
    parametres.put(cleParam, valeurParam);
    return traiterResultatRecherche(liste, vueResultatMultiple, vueResultatSimple, parametres, mapping, request);
  }

  /**
   * Retourne la vue à selectionner selon le résultat de recherche. le mapping en cours.
   * 
   * @param liste
   *          la liste en traitement.
   * @param vueResultatMultiple
   *          la vue a appeler si résultat multiple.
   * @param vueResultatSimple
   *          la vue a appeler si résultat unique.
   * @param parametres
   *          la liste des paramètres pour la redirection.
   * @returnj la vue a appeler (en redirection).
   * @since 3.0.2
   */
  public ActionForward traiterResultatRecherche(ListeNavigation liste, String vueResultatMultiple,
      String vueResultatSimple, HashMap<String, String> parametres, ActionMapping mapping, HttpServletRequest request) {
    if (liste.getNbListe() == 1 && UtilitaireControleur.isTraitementPost(request)) {

      Object resultat = liste.getListe().get(0);
      Iterator<String> iterateur = parametres.keySet().iterator();
      while (iterateur.hasNext()) {
        String cle = iterateur.next();
        String attribut = parametres.get(cle);
        String valeurAttribut = UtilitaireObjet.getValeurAttribut(resultat, attribut).toString();
        parametres.remove(cle);
        parametres.put(cle, valeurAttribut);
      }

      ActionForward forward = mapping.findForward(vueResultatSimple);
      return new ForwardParameters().add(parametres).forward(forward);
    } else {
      return mapping.findForward(vueResultatMultiple);
    }
  }

  /**
   * Indique si on doit appliquer un traitement spécifique sur un lien retour inter service.
   * 
   * @param request
   *          la requête en traitement.
   * @return true si l'appel vient d'un lien d'un autre service via retour à la tâche précédente.
   * @since 3.0.2
   */
  public boolean isTraitementLienRetour(HttpServletRequest request) {
    if (request.getParameter("sofi_lien_retour") != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne la locale en cours de traitement par l'utilisateur.
   * 
   * @since 3.0.2
   */
  @Override
  public Locale getLocale(HttpServletRequest request) {
    return UtilitaireControleur.getLocale(request.getSession());
  }
}
