/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.ErreurApplication;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.application.surveillance.InfoUtilisateur;
import org.sofiframework.application.surveillance.UtilisateurSession;


/**
 * Action qui gère la surveillance d'une application SOFI.
 *
 * @author Jean-Maxime Pelletier
 */
public class SurveillanceAction extends BaseDispatchAction {
  /**
   * L'instance de journalisation pour cette classe.
   */
  private static final Log log = LogFactory.getLog(SurveillanceAction.class);

  /**
   * Accéder à l'unité de traitement.
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    GestionSurveillance.getInstance().initialiserListeUtilisateur();

    return mapping.findForward("afficher");
  }

  /**
   * Réafficher la page.
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    String departApplication = GestionSurveillance.getInstance()
        .getDateDepartApplication();
    int nbPageConsulte = GestionSurveillance.getInstance().getNbPageConsulte();
    int nbPageEchec = GestionSurveillance.getInstance().getNbPageEnEchec();
    int nbUtilisateur = GestionSurveillance.getInstance()
        .getNbUtilisateurAuthentifierEnLigne();
    int nbUtilisateurTotal = GestionSurveillance.getInstance()
        .getNbTotalUtilisateurAuthentifie();

    request.setAttribute("departApplication", departApplication);
    request.setAttribute("nbPageConsulte", new Integer(nbPageConsulte));
    request.setAttribute("nbPageEchec", new Integer(nbPageEchec));
    request.setAttribute("nbUtilisateur", new Integer(nbUtilisateur));
    request.setAttribute("nbUtilisateurTotal", new Integer(nbUtilisateurTotal));

    return mapping.findForward("page");
  }

  /**
   * Afficher la liste des utilisateurs.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficherListeUtilisateur(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    List listeUS = GestionSurveillance.getInstance().getListeUtilisateur();
    List listeUtilisateur = null;

    /**
     * On doit refaire une nouvelle liste car la référence à la session
     * dans la session qui est dans UtilisateurSession cause des crash
     * sur certains serveurs.
     */
    if ((listeUS != null) && (listeUS.size() > 0)) {
      listeUtilisateur = new ArrayList();

      /*
       * On reconstruit une nouvelle liste sans les sessions.
       */
      for (Iterator i = listeUS.iterator(); i.hasNext();) {
        UtilisateurSession us = (UtilisateurSession) i.next();

        try {
          InfoUtilisateur temp = new InfoUtilisateur();
          temp.setUtilisateur(us.getUtilisateur());
          temp.setAdresseIP(us.getAdresseIP());
          temp.setDateDerniereAcces(us.getSession().getLastAccessedTime());
          temp.setDateCreationSession(us.getSession().getCreationTime());
          temp.setDerniereAction(us.getDerniereAction());
          temp.setNavigateur(us.getNavigateur());
          temp.setCleUtilisateur(us.getCleUtilisateur());

          if (log.isDebugEnabled() && (us.getUtilisateur() != null)) {
            Utilisateur utilisateur = (Utilisateur) us.getUtilisateur();
            log.debug(utilisateur.getCodeUtilisateur() + "||" +
                utilisateur.getNom() + "||" + utilisateur.getPrenom());

            log.debug("Date dernier accès : " + us.getDernierAcces());
          }

          listeUtilisateur.add(temp);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    if (log.isDebugEnabled() && (listeUtilisateur != null)) {
      log.debug("Nb. utilisateur dans la liste : " + listeUtilisateur.size());
    }

    request.setAttribute("listeUtilisateur", listeUtilisateur);

    if (isAjax(request)) {
      if (request.getParameter("div") != null) {
        return mapping.findForward("ajax_div_liste_utilisateur");
      } else {
        return mapping.findForward("ajax_liste_utilisateur");
      }
    } else {
      return mapping.findForward("page_liste_utilisateur");
    }
  }

  /**
   * Afficher la liste des erreurs.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficherListeErreur(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    ArrayList listeErreur = GestionSurveillance.getInstance()
        .getListePageEnEchec();

    if (log.isDebugEnabled() && (listeErreur != null)) {
      log.debug("Nb. erreur dans la liste : " + listeErreur.size());

      if (listeErreur.size() > 0) {
        log.debug("Contenu de la liste : " + listeErreur.toString());
      }
    }

    request.setAttribute("listeErreur", listeErreur);

    return mapping.findForward("page_liste_erreur");
  }

  /**
   * Initialiser la page d'erreur.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward initialiserPageErreur(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    GestionSurveillance.getInstance().initialiserListePageEnEchec();

    return mapping.findForward("afficher");
  }

  /**
   * Afficher l'erreur.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficherErreur(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    String no = request.getParameter("no");

    if (no != null) {
      setAttributTemporairePourAction("noErreurSurveillance", no, request);
    } else {
      no = (String) request.getSession().getAttribute("noErreurSurveillance");
    }

    ErreurApplication erreur = (ErreurApplication) GestionSurveillance.getInstance()
        .getListeHMPageEnEchec()
        .get(new Integer(
            no));

    request.setAttribute("erreurDetail", erreur);

    return mapping.findForward("page_detail_erreur");
  }

  /**
   * Afficher l'utilisateur.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficherUtilisateur(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    String codeUtilisateur = request.getParameter("codeUtilisateur");

    if (codeUtilisateur != null) {
      setAttributTemporairePourAction("codeUtilisateurEnTraitement",
          codeUtilisateur, request);
    } else {
      codeUtilisateur = (String) request.getSession().getAttribute("codeUtilisateurEnTraitement");
    }

    String codeErreur = request.getParameter("codeErreur");
    Utilisateur utilisateur = null;
    UtilisateurSession utilisateurSession = null;

    if (codeErreur == null) {
      utilisateurSession = (UtilisateurSession) GestionSurveillance.getInstance()
          .getListeHMUtilisateur()
          .get(codeUtilisateur);
      utilisateur = (Utilisateur) utilisateurSession.getUtilisateur();
      request.setAttribute("utilisateurSession", utilisateurSession);
    } else {
      ErreurApplication erreur = (ErreurApplication) GestionSurveillance.getInstance()
          .getListeHMPageEnEchec()
          .get(new Integer(
              codeErreur));
      utilisateurSession = (UtilisateurSession) erreur.getUtilisateur();
      utilisateur = (Utilisateur) utilisateurSession.getUtilisateur();
    }

    try {
      request.setAttribute("utilisateurDetail", utilisateur);
      request.setAttribute("utilisateurSession", utilisateurSession);

      ArrayList listeRole = (ArrayList) utilisateur.getListeRole(GestionSecurite.getInstance()
          .getCodeApplication());

      ArrayList listeRoleAvecClientDetaille = completerListeRole(listeRole,
          request);

      if (listeRoleAvecClientDetaille != null) {
        listeRole = listeRoleAvecClientDetaille;
      }

      // Fixer la liste des Rôles.
      request.setAttribute("listeRoles", listeRole);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour obtenir la liste des roles de l'utilisateur.", e);
      }
    }

    return mapping.findForward("page_detail_utilisateur");
  }

  public ArrayList completerListeRole(List listeRole, HttpServletRequest request) {
    return null;
  }

  /**
   * Afficher la liste des utilisateur pour l'entête.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward afficherUtilisateurEntete(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    request.getSession().setAttribute("listeUtilisateurAccueil",
        GestionSurveillance.getInstance().getListeUtilisateurEnLigneUnique());

    return mapping.findForward("ajax_liste_utilisateur_entete");
  }

  /**
   * Raffraichir l'entête des utilisateurs.
   *
   * @param mapping Mappings struts
   * @param form Formulaire
   * @param request Requête HTTP
   * @param response Réponse HTTP
   * @return Direction
   * @throws IOException Exception entrée sortie
   * @throws ServletException Erreur du servlet
   */
  public ActionForward rafraichirUtilisateurEntete(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    String id = String.valueOf(GestionSurveillance.getInstance()
        .getListeUtilisateurEnLigneUnique()
        .size());
    ajouterValeurDansReponse("idNbUtilisateurEnLigne", id, request, response);
    genererReponseXMLAvecValeurs(request, response);

    return null;
  }
}
