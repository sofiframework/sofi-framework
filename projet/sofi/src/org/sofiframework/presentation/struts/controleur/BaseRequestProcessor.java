/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.InvalidCancelException;
import org.apache.struts.action.RequestProcessor;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.ErreurApplication;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Objet servant de base pour un requestProcessor de Struts.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class BaseRequestProcessor extends RequestProcessor {
  /** Variable désignant le log à utiliser pour cette classe */
  protected static Log log = LogFactory.getLog(org.sofiframework.presentation.struts.controleur.BaseRequestProcessor.class);

  /** Variable désignant le nom du chemin par défaut pour accéder au site de surveillance du site actuel */
  private static final String INFO_ACTIVITE_SITE_URL = "/infoSite";

  /**
   * Cette méthode s'exécute avant tout appel de controleur (Action.class).
   * <p>
   * Cette méthode se charge aussi de capter les erreurs java dans un controleur et de l'inscrire dans un journal.
   * Ainsi si une erreur survient, la page d'erreur correspondant à l'exception attrapée est lancée.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @param action L'action à lancer pour exécuter le controleur
   * @param form Le formulaire associé à la requête
   * @param mapping L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return ActionForward La redirection vers la page à visiter une fois le traitement terminé
   * @throws ServletException Exception générique
   * @throws IOException Exception générique
   */
  @Override
  protected ActionForward processActionPerform(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException {
    GestionSurveillance surveillance = org.sofiframework.application.surveillance.GestionSurveillance.getInstance();

    // Permet de traiter une requête provenant de la persistance d'un DIV
    // afin de générer son propre contenu.
    RequestProcessorHelper.appliquerPersistanceDiv(request);

    // Spécifier si l'utilisateur est authentifié.
    // Spécifier si l'utilisateur est authentifié.
    boolean isAuthentifie = false;

    // Accéder à l'utilisateur en cours
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    if (utilisateur != null) {
      // Traiter sécurité sur adresse sélectionné
      try {
        RequestProcessorHelper.appliquerSecuritePourNavigation(utilisateur,
            request, (BaseForm) form, mapping);
      } catch (SecuriteException e) {
        // Faites la gestion de la tentative de l'utilisateur d'accéder
        // à une fonctionnalité non accessible.
        return new ActionForward("acces_interdit");
      }
    }

    // Si l'utilisateur existe, spécifier qu'il est authentifier.
    if (utilisateur != null) {
      isAuthentifie = true;
    }

    String isNouvelleSession = (String) request.getSession().getAttribute(Constantes.NOUVELLE_AUTHENTIFICATION);

    if (isNouvelleSession != null) {
      request.getSession().setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION,
          null);
    }

    //Spécifier le service sélectionnée.
    RequestProcessorHelper.specifierDetailPourService(false, request, mapping);

    try {
      ActionForward forward = null;

      // Appeler la page directement si monitoring du site est demandé
      if (mapping.getPath().equals(INFO_ACTIVITE_SITE_URL) ||
          (mapping.getPath().equals("/patch"))) {
        forward = action.execute(mapping, form, request, response);

        // Libérer le modèle
        libererModele(request, response);

        return forward;
      } else {
        org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
        .ajouterUnePageConsulte();
      }

      forward = traitementAvantAction(request, response, action, form, mapping);

      // L'action est traité seulement si le forward est égale a null
      if (forward == null) {
        forward = action.execute(mapping, form, request, response);
      }

      ActionForward forwardApresAction = traitementApresAction(request,
          response, action, form, mapping);

      // Traitement si nouvelle authentification
      //      org.sofiframework.presentation.struts.controleur.RequestProcessorHelper.appliquerNouvelleAuthentification(request,
      //        mapping, getServletContext());
      // L'action est traité seulement si le forward est égale a null
      if (forwardApresAction != null) {
        forward = forwardApresAction;
      }

      // Indique que la session vient d'être fermé
      if (isAuthentifie && (utilisateur == null)) {
        GestionSurveillance.fermerSession(request);
      }

      // Indique si c'est une nouvelle session.
      if (!isAuthentifie && (utilisateur != null)) {
        if (utilisateur != null) {
          String attributCle = surveillance.getParamCleUtilisateur();
          Object valeurCle = org.sofiframework.utilitaire.UtilitaireObjet.getValeurAttribut(utilisateur,
              attributCle);

          try {
            if (valeurCle.getClass().isAssignableFrom(Integer.class)) {
              surveillance.ajouterUtilisateur((Integer) valeurCle, utilisateur,
                  request.getSession());
            } else {
              surveillance.ajouterUtilisateur((String) valeurCle, utilisateur,
                  request.getSession());
            }
          } catch (Exception e) {
            log.warn(
                "Aucun clé de l'utilisateur de spécifier pour la surveillance");
          }
        }
      }

      libererModele(request, response);

      return forward;
    } catch (Exception e) {
      StringWriter writer = new StringWriter();
      PrintWriter prn = new PrintWriter(writer);
      e.printStackTrace(prn);
      prn.flush();

      String messageErreur = writer.toString();
      System.out.println(messageErreur);
      log.error(messageErreur);

      try {
        String methode = request.getParameter("methode");
        ErreurApplication erreurApplication = new ErreurApplication();
        erreurApplication.setMethode(methode);
        erreurApplication.setDateHeure(new Date(UtilitaireDate.getDateJourAvecHeure()
            .getTime()));
        erreurApplication.setMessage(messageErreur);
        erreurApplication.setNomPageErreur(mapping.getPath());
        org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
        .ajouterPageEnEchec(erreurApplication);

        if (utilisateur != null) {
          erreurApplication.setUtilisateur(utilisateur);
        } else {
          erreurApplication.setUtilisateur(null);
        }

        libererModele(request, response);
      } catch (Exception e2) {
      }

      request.setAttribute(Constantes.MESSAGE_ERREUR, e);

      return (processException(request, response, e, form, mapping));
    }
  }

  /**
   * Cette méthode doit être surchargé pour faire un traitement spécifique.
   * <p>
   * Cette méthode, si elle est surchargé par un parent, peux spécifier un traitement spécifique
   * tout juste avant l'execution de l'action (Action.class).
   * Méthode qui est appelé par la méthode processActionPerform(...);
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @param action L'action à lancer pour exécuter le controleur
   * @param form Le formulaire associé à la requête
   * @param mapping L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return ActionForward retourne la vue qui doit être affiché s'il y a lieu, null si le traitement continue normalement
   * @throws ServletException Exception générique
   * @throws IOException Exception générique
   * @throws Exception Exception générique
   */
  protected ActionForward traitementAvantAction(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
    return null;
  }

  /**
   * Cette méthode doit être surchargé pour faire un traitement spécifique.
   * <p>
   * Cette méthode, si elle est surchargé par un parent, peux spécifier un traitement spécifique
   * tout juste après l'execution de l'action (Action.class).
   * Méthode qui est appelé par la méthode processActionPerform(...);
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @param action L'action à lancer pour exécuter le controleur
   * @param form Le formulaire associé à la requête
   * @param mapping L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return ActionForward retourne la vue qui doit être affiché s'il y a lieu, null si le traitement continue normalement
   * @throws ServletException Exception générique
   * @throws IOException Exception générique
   * @throws Exception Exception générique
   */
  protected ActionForward traitementApresAction(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
    return null;
  }

  /**
   * Méthode est traité avant l'appel de la méthode validate du formulaire.
   * <p>
   * Prend pour acquis que le chemin logique permettant à un utilisation d'ouvrir une session correspond
   * à "/identification" ou "/authentification".
   * Cette méthode appelle son équivalent dans RequestProcessor afin de déterminer si une erreur est survenue
   * lors du validate, dans ce cas false est retourne par la méthode du parent et le modèle est libéré.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @param mapping L'objet ActionMapping utilisé pour obtenir ce contrôleur
   * @return boolean Valeur identifiant si la requête à été exécutée ou si il faut continuer l'exécution
   * @throws ServletException Exception générique
   * @throws IOException Exception générique
   */
  @Override
  protected boolean processValidate(HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping)
          throws IOException, ServletException {
    if ("POST".equalsIgnoreCase(request.getMethod())) {
      if (!((mapping.getPath().equals("/identification") ||
          mapping.getPath().equals("/authentification")) &&
          request.getParameter("methode").equals("quitter"))) {
        boolean isFormulaireValide = false;

        try {
          isFormulaireValide = super.processValidate(request, response, form,
              mapping);
        } catch (InvalidCancelException e) {
          log.error("Erreur lors de la validation.", e);
        }

        if (!isFormulaireValide) {
          // Libérer le modèle si une erreur est survenue dans un ActionForm
          libererModele(request, response);
        }

        return isFormulaireValide;
      } else {
        return true;
      }
    }

    return true;
  }

  /**
   * Méthode qui sert à overwriter la méthode héritée du parent.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @return le modèle de l'application
   */
  protected Object getModele(HttpServletRequest request,
      HttpServletResponse response) {
    return null;
  }

  /**
   * Libère le module d'application BC4J.
   * <p>
   * Cette méthode libère le module d'application BC4J. Ce dernier restera réservé pour l'utilisateur
   * tant et aussi longtemps que la session de ce dernier ne sera pas morte.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   */
  protected void libererModele(HttpServletRequest request,
      HttpServletResponse response) {
  }

  /**
   * Permet a un formulaire d'avoir des accents lorsque le formulaire contient des
   * attribut pour upload.
   * <p>
   * @param request La requête HTTP en traitement
   * @return la requête HTTP modifiée
   */
  @Override
  protected HttpServletRequest processMultipart(HttpServletRequest request) {
    return RequestProcessorHelper.processMultipart(request);
  }

  /**
   * Populer les propriétés de l'instance spécifique ActionForm depuis
   * les paramètres de la requête. En plus, l'attribut <code>Globals.CANCEL_KEY</code>
   * est fixé si la requête qui est soumitté avec le bouton créé avec <code>CancelTag</code>.
   *
   * @param request La requête HTTP en traitement
   * @param response  La réponse HTTP à fournir
   * @param form l'instance du formulaire ActionForm qui est a populer
   * @param mapping Le ActionMapping qui est utilisé
   *
   * @exception ServletException si une exception par RequestUtils.populate()
   */
  @Override
  protected void processPopulate(HttpServletRequest request,
      HttpServletResponse response, ActionForm form, ActionMapping mapping)
          throws ServletException {
    RequestProcessorHelper.processPopulate(this.servlet, request, response,
        form, mapping);
  }
}
