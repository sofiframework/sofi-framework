/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.notification.ejb.RemoteServiceNotification;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

public class NotificationAction extends BaseDispatchAction {
  public NotificationAction() {
  }

  public ActionForward getListeNotification(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    return mapping.findForward("liste_ajax");
  }

  public ActionForward indLectureNotification(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    try{
      String slNoNotification = request.getParameter("noNotification");
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
      RemoteServiceNotification service = (RemoteServiceNotification)GestionServiceDistant.getServiceDistant("ServiceNotificationBean");
      service.lireNotification(new Long(slNoNotification),utilisateur.getCodeUtilisateur());
    }catch(Exception e){}

    return null;
  }
}
