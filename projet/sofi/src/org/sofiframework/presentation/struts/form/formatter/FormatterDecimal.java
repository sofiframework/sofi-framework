/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireNombre;


/**
 * Un formatter des valeurs correspondant à des nombres avec décimal.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterDecimal extends Formatter {
  /** La précision de la devise */
  public final static int PRECISION_DEVISE = 2;

  /**  Les formats par défaut */
  public final static String FORMAT_DEVISE = "######0.00";
  public final static String FORMAT_AUTRES = "##0###";

  /** La clé utilisé pour extraire le message d'erreur concernant le type devise */
  public final static String CLE_ERREUR_DEVISE = "erreur.format.devise";
  private Class typeClasse = null;

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return CLE_ERREUR_DEVISE;
  }

  @Override
  public Object unformat(String target) {
    return unformat(target, null, null);
  }

  public Object unformat(String target, Locale locale) {
    return unformat(target, locale, null);
  }

  /**
   * Retourne une instance Float pour un String.
   * <p>
   * @param valeurForm la String à transformer en devise
   * @param format le format a valider s'il y lieu, sinon null.
   * @return un BigDecimal initaliser avec la String donnée
   */
  @Override
  public Object unformat(String valeurForm, Locale locale,
      TimeZone fuseauHoraireClient) {
    String errMsg = "Incapable de traiter la valeur Big Decimal de : " +
        valeurForm;

    if ((valeurForm == null) || (valeurForm.trim().length() < 1)) {
      return null;
    }

    BigDecimal valeurFormat = null;

    try {
      String format = null;

      if (getFormatMessage() != null) {
        format = getFormatMessage().getFormat();
      } else {
        format = valeurForm;
      }

      // test de la validité du nombre.
      valeurFormat = new BigDecimal(UtilitaireNombre.standardiserNombre(
          valeurForm, locale));

      valeurFormat = UtilitaireNombre.getBigDecimal(valeurForm, locale);

      if (getFormatMessage() != null) {
        format = getFormatMessage().getFormat();
      } else {
        format = valeurFormat.toString();
      }

      String messageErreur = "Incapable de formateur en Float" + valeurForm;
      String decimalFormat = "";
      int positionFormat = 0;

      if (format.indexOf(".") != -1) {
        positionFormat = format.indexOf(".") + 1;
        decimalFormat = format.substring(positionFormat);

        if (decimalFormat != null) {
          positionFormat = decimalFormat.length();
        }
      }

      if ((valeurForm != null) && (valeurForm.indexOf(".") != -1)) {
        int position = valeurForm.indexOf(".") + 1;
        String decimal = valeurForm.substring(position);

        if (decimal.length() > decimalFormat.length()) {
          throw new FormatterException(messageErreur);
        }
      }

      if ((getTypeClasse() != null) &&
          getTypeClasse().isAssignableFrom(Float.class)) {
        Float valeur = Float.valueOf(valeurForm);

        return valeur;
      }

      if ((getTypeClasse() != null) &&
          getTypeClasse().isAssignableFrom(Double.class)) {
        Double valeur = Double.valueOf(UtilitaireNombre.getBigDecimal(
            valeurForm, locale).toString());

        return valeur;
      }

      return valeurFormat;
    } catch (NumberFormatException e) {
      throw new FormatterException(errMsg, e);
    }
  }

  /**
   * Retourne un String representant une devise de type Float.
   * <p>
   * @param value la devise à transformer en String commune
   * @return une string correctement formattée
   */
  @Override
  public String format(Object value, Locale locale, TimeZone fuseauHoraireClient) {
    //    String format = null;
    //    String nombreFormatte = null;
    //
    //    if (getFormatMessage() != null) {
    //      format = getFormatMessage().getFormat();
    //    } else {
    //      format = FORMAT_AUTRES;
    //    }
    //
    //    if (value == null) {
    //      return "";
    //    } else {
    //      nombreFormatte = value.toString();
    //    }
    //
    //    if ((format != null) && (format.length() > 0)) {
    //      DecimalFormat df = new DecimalFormat(format);
    //
    //      // Décorer pour appliquer le format demandé.
    //      try {
    //        nombreFormatte = df.format(value);
    //      } catch (Exception e) {
    //        throw new FormatterException("Incapable de formatter " + value +
    //          "avec le format " + format, e);
    //      }
    //    }
    return value.toString();
  }

  @Override
  public String format(Object value, Locale locale) {
    return format(value, locale, null);
  }

  public void setTypeClasse(Class typeClasse) {
    this.typeClasse = typeClasse;
  }

  public Class getTypeClasse() {
    return typeClasse;
  }
}
