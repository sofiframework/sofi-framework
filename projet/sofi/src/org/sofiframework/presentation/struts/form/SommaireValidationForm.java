/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.sofiframework.presentation.struts.form.formatter.FormatMessage;


/**
 * Classe permettant de loger le sommaire des validations pour un formulaire.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.4
 */
public class SommaireValidationForm implements Serializable {

  private static final long serialVersionUID = 6927219499359680809L;

  private ArrayList listeObligatoire;
  private HashMap listeFormat;

  public SommaireValidationForm() {
    this.listeFormat = new HashMap();
    this.listeObligatoire = new ArrayList();
  }

  public String[] getAttributObligatoire() {
    String[] attributObligatoire = new String[this.listeObligatoire.size()];

    for (int i = 0; i < listeObligatoire.size(); i++) {
      attributObligatoire[i] = (String) listeObligatoire.get(i);
    }

    return attributObligatoire;
  }

  public ArrayList getListeObligatoire() {
    return listeObligatoire;
  }

  public void setListeObligatoire(ArrayList listeObligatoire) {
    this.listeObligatoire = listeObligatoire;
  }

  public void ajouterObligatoire(String attribut) {
    attribut = getNomAttribut(attribut);

    // Si formulaire imbriqué, prendre seulement le nom d'attribut
    if (!this.listeObligatoire.contains(attribut)) {
      this.listeObligatoire.add(attribut);
    }
  }

  public HashMap getListeFormat() {
    return listeFormat;
  }

  public void setListeFormat(HashMap listeFormat) {
    this.listeFormat = listeFormat;
  }

  public void ajouterFormat(String nomAttribut, String format,
      String formatSaisie, String formatSortie, String message) {
    this.listeFormat.put(getNomAttribut(nomAttribut),
        new FormatMessage(format, formatSaisie, formatSortie, message));
  }

  public void ajouterFormat(String nomAttribut, String type,
      Integer tailleMaximale, String messageErreur) {
    this.listeFormat.put(getNomAttribut(nomAttribut),
        new FormatMessage(type, tailleMaximale, messageErreur));
  }

  private String getNomAttribut(String attribut) {
    String attributRetour = null;
    StringTokenizer stAttribut = new StringTokenizer(attribut, ".");

    while (stAttribut.hasMoreTokens()) {
      attributRetour = stAttribut.nextToken();
    }

    return attributRetour;
  }
}
