/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;



/**
 * Classe d'exception lors d'un formatage.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = -172309773670325639L;
  /** le formateur en erreur */
  private Formatter formatter;

  /** Constructeur */
  public FormatterException(Formatter formatter) {
    this.formatter = formatter;
  }

  /** Constructeur */
  public FormatterException(String message) {
    this(message, null);
  }

  /** Constructeur */
  public FormatterException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Fixer la cause de l'erreur.
   * <p>
   * @param cause la cause de l'erreur
   */
  public void setCause(Throwable cause) {
    super.initCause(cause);
  }

  public Formatter getFormatter() {
    return formatter;
  }

  /**
   * Fixer le formatter en erreur.
   * <p>
   * @param formatter le formatter en erreur
   */
  public void setFormatter(Formatter formatter) {
    this.formatter = formatter;
  }
}
