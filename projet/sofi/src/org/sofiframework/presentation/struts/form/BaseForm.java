/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.UtilitaireMessageFichier;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageAvertissement;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.message.objetstransfert.MessageInformatif;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesMessage;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.Identifiant;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.balisesjsp.utilitaire.InfoMultibox;
import org.sofiframework.presentation.struts.controleur.RequestProcessorHelper;
import org.sofiframework.presentation.struts.form.comparateur.Comparateur;
import org.sofiframework.presentation.struts.form.comparateur.ComparateurDate;
import org.sofiframework.presentation.struts.form.formatter.FormatMessage;
import org.sofiframework.presentation.struts.form.formatter.Formatter;
import org.sofiframework.presentation.struts.form.formatter.FormatterException;
import org.sofiframework.presentation.struts.form.utilitaire.UtilitaireBaseForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Ceci est une classe abstraite de base pour un ActionForm qui permet
 * d'ajouter un support pour le formatage automatique d'un format Java en String
 * et vice et versa. Et permet aussi le transfert des valeurs du formulaire à
 * l'objet de transfert et aussi vice et versa.
 * <p>
 * La méthode <code>populate()</code> offert le point d'entrée pour cette fonctionnalité
 * , tandis que la méthode <code>nomAttributsAExclure()</code> permet à une sous-classegetListeFormulaireImbriqueATraite
 * de spécifier les attributs qui ne doivent pas être populé automatiquement.
 * <p>
 * Si vous transférer un objet de transfert qui contient d'autre objet de transfert
 * dans le formulaire, vos objets de transfert doivent étendre org.sofiframework.objetstransfet.ObjetTransfert
 * Exemple:
 * public class Client extends org.sofiframework.objetstransfert.ObjetTransfert {
 * }
 * <p>
 * La méthode <code>setValeurParDefaut()</code> permet à l'appellant
 * de spécifier les String par défaut pour remplacer les valeurs
 * a <code>null</code>. Similairement, la méthode <code>setTypeFormatter()</code>
 * permet à l'appellant de spécifier un type de formatage autre que
 * celui par défaut correspondant à l'attribut.
 * <p>
 * Le développeur peut aussi spécifier des règles de validations en appelant
 * <code>ajouterAttributObligatoire()</code> pour ajouter un ou des attributs qui sont obligatoires,
 * spécifier par un tableau de array.
 * <p>
 * Pour les champs de formulaire checkbox et multibox aucun type de Formatter n'est
 * appliqué. Pour un checkbox, utiliser le type boolean et pour le multibox,
 * utiliser le type String[]. Tous les autres type d'attribut du formulaire doivent être en String.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version $Revision: 642 $ ($Author: pellje $)
 */
public abstract class BaseForm extends ActionForm {
  /**
   * 
   */
  private static final long serialVersionUID = -4059776488462466252L;

  /** Instance commune de journalisation */
  protected static Log log = LogFactory.getLog(BaseForm.class);

  /** Indique que le formulaire a été modifié */
  public static final String IND_MODIFIE = "1";

  /** Indique que le formulaire n'a été modifié */
  public static final String IND_NON_MODIFIE = "0";

  /** Indique que la conversion soit faite du formulaire (ActionForm) vers l'objet de transfert */
  public static final int VERS_OBJET_TRANSFERT = 0;

  /** Indique que la conversion soit faite de l'objet de transfert vers le formulaire (ActionForm) */
  public static final int VERS_FORMULAIRE = 1;

  /** Constante indiquant que le formulaire n'est pas un formulaire imbriqué */
  public static final int PAS_FORMULAIRE_IMBRIQUE = -1;

  /**
   * Constante indiquant que le formulaire est un nested, si superieure
   * ou egale a 0, il indique que le formulaire est dans une liste de formulaire
   */
  public static final int NESTED_OBJECT = -2;

  /** Classe de formatter a utiliser pour une propriete donné */
  private Map formatMap = new HashMap();

  /** L'objet de transfert associé au formulaire */
  private transient Object objetTransfert;

  /** L'objet de transfert orignal utilisé lorsque le mode de traitement des champs modifie seulement est actif et le formulaire est en erreur */
  private transient ObjetTransfert objetTransfertOriginal;

  /** Les validations pour ce formulaire */
  private Map validationMap = new HashMap();

  /** Les validations pour seulement un nouveau formulaire */
  private Map validationNouveauFormulaireMap = new HashMap();

  /** Tableau clé-valeur correpondant au type de formulaire associé à un type d'objet de transfert */
  private HashMap typeFormulaireEnfants = new HashMap();

  /** Les attributs a exclure dans la conversion automatique */
  private HashMap attributsAExclure = null;

  /** Les attributs a exclure dans la conversion automatique */
  private ArrayList attributsOrdreValidations = new ArrayList();

  /** Les attributs a exclure dans les validations */
  private Map attributsSansValidations = new HashMap();

  /** Les attributs associé à leur message d'erreur */
  private Map attributsAvecMessagesErreur = new HashMap();

  /** Les erreurs dans le formulaires **/
  protected LinkedHashMap lesErreurs = new LinkedHashMap();

  /** Les avertissements dans le formulaires **/
  protected LinkedHashMap lesAvertissements = new LinkedHashMap();

  /** Les informations dans le formulaires **/
  protected LinkedHashMap lesInformations = new LinkedHashMap();

  /** Les confirmations dans le formulaires **/
  protected LinkedHashMap lesConfirmations = new LinkedHashMap();

  /** Les erreurs générales dans le formulaires **/
  protected LinkedHashMap lesErreursGenerales = new LinkedHashMap();

  /** la liste des erreurs */
  protected transient ActionErrors listeErreurs = null;

  /**
   * La chaine de caractere a afficher lorsque la valeur est a null.
   * La clé du HashMap correspond a un attribut du formulaire.
   * Sa presence indique de prendre la valeur specifié.
   */
  protected Map valeurParDefautMap = new HashMap();

  /**
   * Attribut permettant d'accéder à un paramètre se nommant "valeur".
   */
  private String valeur;

  /** Le numéro d'indice nested */
  private int indiceNested;

  /** Nom de la liste imbriqués Nested */
  private String nomListe;

  /** Nom de la méthode appellante via l'URL */
  private String methode;

  /** Spécifie si le formulaire doit éxécuter la validation */
  private boolean executerValidation = true;

  /** Spécifie si le formulaire est éligible pour une validation */
  private boolean formulaireValidation;

  /** Indicateur afin de spécifier si le formulaire a été modifié */
  private String indModification;

  /** Est-ce que le formulaire est en lecture seulement */
  private boolean modeLectureSeulement;

  /** Est-ce que le formulaire est en affichage seulement  */
  private boolean modeAffichageSeulement;

  /** Mapping de l'action Struts */
  private ActionMapping mapping;

  /** Spéficie l'attribut du formulaire qui est un identifiant unique */
  private String formulaireIdentifiant;

  /** Indicateur si les attributs en validation du formulaire sont mit en cache **/
  private boolean attributsEnValidationEnCache = true;

  /** Indicateur qui indique que le formulaire est nouveau */
  private boolean nouveauFormulaire;

  /** Indicateur que le formulaire est utilisé par plusieurs pages */
  private boolean formulaireSurPlusieursPages = false;

  /** Indicateur que le formulaire est initlisé avec un objet de transfert */
  private boolean formulaireInitialiseAvecObjetTransfert;

  /** Indicateur que la population de l'objet de transfert a été traité */
  private boolean populerObjetTransfertTraite = false;

  /** Indicateur que le formulaire a déjà eu des erreurs lors de la transaction */
  private boolean formulaireContenaitDesErreurs = false;

  /** La liste des formulaires imbriqués a traiter **/
  private LinkedHashMap listeFormulaireImbriqueATraiter = null;

  /** La liste attribut a traiter répertorier par formulaire **/
  private LinkedHashMap listeAttributATraiter = null;

  /** La liste attribut a traiter répertorier par formulaire imbriqué s'il y lieu **/
  private LinkedHashMap listeAttributATraiterParFormulaire = null;

  /** La liste attribut a traiter répertorier par formulaire imbriqué s'il y lieu **/
  private HashSet listeAttributTraite = null;

  /** La liste des attributs avec des formats spécifiques **/
  private HashMap listeAttributAvecFormat = new HashMap();

  /** Indique si le formulaire est transactionnel **/
  private boolean transactionnel;

  /**
   * Traiter seulement les attributs affichés à l'utilisateur lors de la population
   * de l'objet transfert depuis le formulaire.
   * @since SOFI 1.8.2 Le défaut est true.
   */
  private boolean traiterAttributsAfficheSeulement = true;

  /** Nom d'une méthode qui ignore les validations, seul les erreurs de format sont lancé **/
  private String methodeIgnoreValidation;

  /** Le formulaire parent si formulaire imbrique **/
  private BaseForm formulaireParent;

  /** Le formulaire qui est directement parent **/
  private BaseForm formulaireDirectementParent;

  /** Traiter seulement les attributs qui ont été modifié dans le formulaire. **/
  private boolean traiterSeulementAttributModifie = false;

  /** Spécifie si la correspondance des formulaires enfants sont traité **/
  private boolean correspondanceFormulaireEnfantTraite;

  /** Spécifie si on désire instancié tout les objet de transfert imbriqué
   * associé au formulaire **/
  private boolean instancierObjetsTransfertImbrique = false;

  /**
   * Le formulaire n'est pas associé à un objet de transfert.
   * <p>
   * Par défaut false, spécifier true dans le constructeur de votre
   * formulaire.
   */
  private boolean formulaireSansObjetTransfert = false;

  /**
   * Permet de populer le formulaire avec la liste des attributs du formulaire.
   * Ceci limite le nombre d'itérations dans le cas ou les objets de transferts
   * possèdent une grande quantité d'attributs. Peut augmenter considérablement
   * les performances dans cette situation. Cette propriété doit être a false
   * lors de l'utilisation du BaseDynaForm. Le formulaire dynamique a besoin
   * des propriétés de l'objet de transfert pour créer la représentation interne
   * du formulaire dynamique.
   * @since SOFI 2.0.2
   */
  private boolean populerAvecAttributsFormulaire = false;

  /**
   * La clé du message d'erreur global à utiliser pour ce formulaire.
   */
  private String cleMessageErreurGeneral;

  /**
   * Méthode qui sert à effectuer les validations sur les champs du formulaire.
   * <p>
   * @param mapping l'objet mapping dans lequel est contenu le formulaire
   * @param request la requete HTTP en traitement
   * @return la liste des erreurs de validation
   */
  @Override
  public ActionErrors validate(ActionMapping mapping,
      HttpServletRequest request) {
    this.mapping = mapping;

    if ((getObjetTransfert() == null) && isFormulaireSansObjetTransfert()) {
      setObjetTransfert(new ObjetTransfertVide());
      setObjetTransfertOriginal(null);
      setNouveauFormulaire(true);
      setFormulaireInitialiseAvecObjetTransfert(true);
    }

    this.formulaireParent = getFormulaire(request.getSession());

    if (!isIgnorerValidation(request)) {
      if (isFormulaireContenaitDesErreurs() &&
          !isFormulaireSurPlusieursPages()) {
        if ((objetTransfertOriginal != null) &&
            (isTraiterSeulementAttributModifie() && !isNouveauFormulaire())) {
          setObjetTransfert(objetTransfertOriginal.clone());
          ((ObjetTransfert)getObjetTransfert()).reinitialiserLesAttributsModifies();
        }
      }

      if ((!isModeLectureSeulement() && !isModeAffichageSeulement()) ||
          !isTransactionnel()) {
        try {
          populerObjetTransfert(request);

          if (isExecuterValidation()) {
            try {
              this.validerFormulaire(request);
            } catch (RuntimeException e) {
              log.error("Erreur fatale lors de la validation", e);
              throw e;
            }
          }
        } catch (RuntimeException e) {
          log.error("Erreur fatale lors de la validation", e);
          throw e;
        }
      }

      if (isFormulaireEnErreurOuConfirmation() ||
          (request.getAttribute("rappelFormulaire") != null)) {
        listeErreurs.add("indicateurErreurStruts",
            new ActionMessage("indicateurErreurStruts"));

        return listeErreurs;
      } else {
        return null;
      }
    }

    return null;
  }

  /**
   * Permet de rappeler le formulaire de saisi même s'il n'y a aucune
   * erreur de spécifié.
   * @param request la requête présentement en traitement.
   */
  public void rappelerFormulaire(HttpServletRequest request) {
    request.setAttribute("rappelFormulaire", Boolean.TRUE);
  }

  /**
   * Méthode abstraite qui sert à valider les champs d'un formulaire.
   * <p>
   * Cette méthode doit être overwrité par la formulaire. C'est dans cette méthode
   * que s'effectueront toutes les validations auprès des champs du formulaire en
   * cours.
   * <p>
   * @param request la requete HTTP en traitement
   */
  public abstract void validerFormulaire(HttpServletRequest request);

  /**
   * Popule l'objet de transfert dans le formulaire et retourne
   * ActionErrors si des erreurs se produise de la population.
   * <p>
   * @param request la requête HTTP en traitement.
   * @param objetTransfert l'objet de transfert avec lequel on fait le mapping lors
   * de l'utilisation de la méthode populerObjetTransfert
   */
  public void validate(HttpServletRequest request,
      ObjetTransfert objetTransfert) {
    listeAttributTraite = new HashSet();
    traiterLesErreurs(request, objetTransfert, null);
  }

  /**
   * Permet de traite les message pour les objets simple ou encore
   * les objets imbriqués (Nested).
   * <p>
   * @param request la requête HTTP en cours
   * @param nomListe le nom de la liste d'objet imbriqués.
   */
  public void traiterLesErreurs(HttpServletRequest request,
      ObjetTransfert objeTransfert,
      String nomListe) {
    this.traiterLesErreurs(request, objeTransfert, nomListe, false);
  }

  /**
   * Permet d'effectuer toutes les validations du formulaire.
   */
  public void forcerValidations(HttpServletRequest request,
      ObjetTransfert objeTransfert) {
    this.traiterLesErreurs(request, objeTransfert, null, true);
    this.validerFormulaire(request);
  }

  /**
   * On doit utiliser cette méthode lorsque l'on veut forcer les validations du
   * formulaire même lors d'une méthde GET ou lorsque les validations on déjà
   * été effectuées.
   * <p>
   * @param request la requête HTTP en cours
   * @param nomListe le nom de la liste d'objet imbriqués.
   * @param forcerValidations si l'on désire forcer les validations
   * @param objeTransfert Objet de transfert utilisé pour populer
   */
  public void traiterLesErreurs(HttpServletRequest request,
      ObjetTransfert objetTransfert, String nomListe,
      boolean forcerValidations) {
    if (getIndiceNested() == -1) {
      // l'objet n'est pas imbriqué, populer l'objet de transfert.
      lesErreurs =
          populate(objetTransfert, VERS_OBJET_TRANSFERT, false, request);
    }

    if (forcerValidations ||
        ("POST".equalsIgnoreCase(request.getMethod()) && isExecuterValidation())) {
      if (lesErreurs.size() > 0) {
        MessageErreur messageErreur = null;
        MessageErreur premierMessageErreur = null;

        // Traite la liste courante des messages pour le formulaire.
        LinkedHashMap listeCouranteErreur = (LinkedHashMap)lesErreurs.clone();
        Iterator iterateurMessage = listeCouranteErreur.keySet().iterator();

        while (iterateurMessage.hasNext()) {
          String nomAttribut = (String)iterateurMessage.next();

          if (!(lesErreurs.get(nomAttribut) instanceof MessageErreur)) {
            Object[] messageAssocie = null;
            String[] parametres = null;
            messageAssocie = (Object[])lesErreurs.get(nomAttribut);
            messageErreur = (MessageErreur)messageAssocie[0];
            parametres = (String[])messageAssocie[1];

            UtilitaireMessageFichier.getInstance().setMessageErreur(messageErreur, parametres, request,
                false);
          } else {
            messageErreur = (MessageErreur)lesErreurs.get(nomAttribut);
          }

          // Ajouter l'erreur dans le formulaire parent.
          ajouterErreurAuFormulaireParent(request, messageErreur);

          if (premierMessageErreur == null) {
            premierMessageErreur = messageErreur;
          }
        }
      }
    }
  }

  /**
   * Popule le formulaire avec les valeurs de l'objet de transert donné,
   * convertit les types Java en valeur formatté en String pour la présentation
   * de l'interface utilisateur. Les conversions sont appellées automatiquement
   * grace a l'introspection de chacun des types d'attributs et recherche
   * ainsi la classe de formatage approprié.
   * <p>
   * Ce comportement peut être personnalisé permet en appellant
   * <code>setTypeFormatter(String nomAttribut, Class type)</code>
   * de donner un Formatter alternatif pour un nom d'attribut donné.
   * <p>
   * Si une valeur est a null, BaseForm va populer avec sa valeur par defaut du
   * Formatter, a moins qu'une valeur par defaut soit spécifier par
   * <code>setValeurParDefaut(String nomAttribut, String valeur)</code> avec
   * un String alternatif.
   * <p>
   * @param objetTransfert L'objet de transfert avec lequel populer le formulaire
   * @param request la requête HTTP en traitement
   * @return les erreurs de validation
   */
  public synchronized LinkedHashMap populerFormulaire(ObjetTransfert objetTransfert,
      HttpServletRequest request) {
    // Initialiser la liste des attributs traité par le formulaire.
    listeAttributTraite = new HashSet();

    // Si l'appel de population du formulaire n'est pas directement sur le parent
    // alors initialiser les attributs traité.
    // @since SOFI 2.0.1
    if (!getFormulaireParent().getClass().getName().equals(this.getClass().getName())) {
      getFormulaireParent().setListeAttributTraite(new HashSet());
    }

    if (!getFormulaireParent().isFormulaireEnErreur()) {
      // Instancier toutes les instances de l'objet de transfert qui sont null.
      if (isInstancierObjetsTransfertImbrique()) {
        UtilitaireObjet.instancierInstancesImbrique(objetTransfert);
      }

      // Fixer aue le formulaire n'est pas nouveau.
      setNouveauFormulaire(false);

      // Initialiser l'indicateur de modification.
      setFormulaireModifie(false);

      // Indiquer que le formulaire a été initialisé avec un objet de transfert.
      setFormulaireInitialiseAvecObjetTransfert(true);

      // Fixer l'objet de transfert et il est fixé comme étant l'original.
      setObjetTransfert(objetTransfert);
      fixerObjetTransfertOriginal();

      LinkedHashMap listeErreurs =
          populate(objetTransfert, VERS_FORMULAIRE, false, request);

      // Appel d'une méthode qui permet de personnaliser l'affichage du formulaire si surchargé.
      preparerAffichage(request);

      return listeErreurs;
    } else {
      return lesErreurs;
    }
  }

  /**
   * Fixer l'objet de transfert original avec l'objet de transfert en cours.
   */
  public void fixerObjetTransfertOriginal() {
    try {
      ObjetTransfert objetTransfert = (ObjetTransfert)getObjetTransfert();
      setObjetTransfertOriginal((ObjetTransfert)objetTransfert.clone());
    } catch (Exception e) {
    }
  }

  /**
   * Appel la population d'un objet de transfert tout en initialisant
   * le formulaire à neuf avant la population.
   * <p>
   * Par défaut, un formulaire placé dans la session n'est pas remis à blanc
   * avant la population du formulaire.
   * @param objetTransfert L'objet de transfert avec lequel populer le formulaire
   * @param request la requête HTTP en traitement
   * @param initialiserFormulaireDansSession true si on désire initialiser le formulaire dans la session
   * @return  les erreurs de validation
   */
  public LinkedHashMap populerFormulaire(ObjetTransfert objetTransfert,
      HttpServletRequest request,
      boolean initialiserFormulaireDansSession) {
    if (initialiserFormulaireDansSession) {
      initialiserTousLesAttributs(false);
    }

    if (getMapping() != null) {
      initialiserFormulaire(request, getMapping());
    }

    return populerFormulaire(objetTransfert, request);
  }

  /**
   * Popule le formulaire avec les valeurs de l'objet de transert donné,
   * convertit les types Java en valeur formatté en String pour la présentation
   * de l'interface utilisateur. Les conversions sont appellées automatiquement
   * grace a l'introspection de chacun des types d'attributs et recherche
   * ainsi la classe de formatage approprié.
   * <p>
   * Ce comportement peut être personnalisé permet en appellant
   * <code>setTypeFormatter(String nomAttribut, Class type)</code>
   * de donner un Formatter alternatif pour un nom d'attribut donné.
   * <p>
   * @param objetTransfert L'objet de transfert
   * @param request la requête HTTP en traitement
   */
  public synchronized void populerObjetTransfert(ObjetTransfert objetTransfert,
      HttpServletRequest request) {
    if (!isPopulerObjetTransfertTraite()) {
      setNomFormulaire(request, getMapping());

      //Appeler la validation du formulaire.
      validate(request, objetTransfert);
    }

    setPopulerObjetTransfertTraite(true);
  }

  public void populerObjetTransfertRequeteAjax(ObjetTransfert objetTransfert, HttpServletRequest request) {
    boolean affichageSeulement = this.isTraiterAttributsAfficheSeulement();
    boolean populerAvecFormulaire = this.isPopulerAvecAttributsFormulaire();
    boolean populerObjetTransfertTraite = this.isPopulerObjetTransfertTraite();

    this.setTraiterAttributsAfficheSeulement(false);
    this.setPopulerAvecAttributsFormulaire(true);
    this.setPopulerObjetTransfertTraite(false);
    this.populerObjetTransfert(objetTransfert, request);

    // remettre les valeurs originales
    this.setTraiterAttributsAfficheSeulement(affichageSeulement);
    this.setPopulerAvecAttributsFormulaire(populerAvecFormulaire);
    this.setPopulerObjetTransfertTraite(populerObjetTransfertTraite);
  }

  /**
   * Popule un objet de transfert.
   * <p>
   * Utiliser cette méthode si vous désirez comparer un ancien objet de transfert
   * associé à l'instance de formulaire placé dans la session et le nouveau
   * objet de transfert généré avec les modifications de l'utilisateur.
   * À noter qu'il est important que le formulaire doit être placé dans la session,
   * afin que cette fonctionnalité soit disponible.
   * @param objetTransfert L'objet de transfert
   * @param mapping instance du ActionMapping.
   * @param request la requête HTTP en traitement
   */
  public void populerObjetTransfert(ObjetTransfert objetTransfert,
      ActionMapping mapping,
      HttpServletRequest request) {
    this.mapping = mapping;

    this.populerObjetTransfert(objetTransfert, request);
  }

  /**
   * Populer l'objet de transfert courant
   * @param request la requête présentement en traitement.
   */
  private void populerObjetTransfert(HttpServletRequest request) {
    // Si le formulaire est initialisé
    if (isFormulaireInitialiseAvecObjetTransfert() &&
        !isPopulerObjetTransfertTraite()) {
      ObjetTransfert clone = getObjetTransfertClone();
      populerObjetTransfert(clone, request);
    }
  }

  /**
   * Supprimer une regle de validation dans un type Map.
   * <p>
   * @param nomAttribut le nom de l'attribut dont la règle ne s'applique plus
   * @param regle le nom de la règle
   */
  public void supprimerRegleValidation(String nomAttribut, String regle) {
    Map valeurs = (Map)validationMap.get(nomAttribut);

    if (valeurs != null) {
      valeurs.remove(regle);
    }
  }

  /**
   * Supprimer tous les validations pour une règle.
   * @param regle la règle a supprimer
   */
  public void supprimerRegleValidation(String regle) {
    Iterator iterateur = validationMap.keySet().iterator();

    while (iterateur.hasNext()) {
      Object cle = iterateur.next();
      Map valeur = (Map)validationMap.get(cle);

      if (valeur != null) {
        valeur.remove(regle);
      }
    }
  }

  /**
   * Ajouter des attributs qui sont obligatoire a saisir.
   * <p>
   * @param nomsAttributs Les noms d'attributs obligatoires
   */
  public void ajouterAttributObligatoire(String[] nomsAttributs) {
    attributsOrdreValidations = new ArrayList();

    if (!isAttributsObligatoireEnCache()) {
      supprimerRegleValidation("required");
    }

    if (nomsAttributs != null) {
      for (int i = 0; i < nomsAttributs.length; i++) {
        UtilitaireBaseForm.ajouterRegleValidation(validationMap,
            nomsAttributs[i], "required",
            Boolean.TRUE);
        attributsOrdreValidations.add(nomsAttributs[i]);
      }
    }
  }

  /**
   * Ajouter des attributs qui sont obligatoire a saisir.
   * <p>
   * @param nomsAttributs Les noms d'attributs obligatoires
   */
  public void ajouterAttributObligatoire(String nomAttribut) {
    UtilitaireBaseForm.ajouterRegleValidation(validationMap, nomAttribut,
        "required", Boolean.TRUE);
  }

  public void ajouterAttributObligatoire(String nomFormulaireImbrique,
      String nomAttribut) {
    BaseForm formulaire = null;

    if (nomFormulaireImbrique != null) {
      formulaire =
          (BaseForm)getValeurAttribut(this, nomFormulaireImbrique, true);
    } else {
      formulaire = this;
    }

    try {
      formulaire.ajouterAttributObligatoire(UtilitaireBaseForm.getNomAttributSansImbrique(nomAttribut));
    } catch (Exception e) {
      throw new SOFIException("Le formulaire suivant n'est pas instancié" +
          nomFormulaireImbrique, e.getCause());
    }
  }

  /**
   * Supprimer des attributs qui ne sont plus obligatoire.
   * <p>
   * @param nomsAttributs Les noms d'attributs non obligatoires
   */
  public void supprimerAttributObligatoire(String[] nomsAttributs) {
    for (int i = 0; i < nomsAttributs.length; i++) {
      supprimerRegleValidation(nomsAttributs[i], "required");
      attributsOrdreValidations.remove(nomsAttributs[i]);
    }
  }

  /**
   * Initialiser les attibuts obligatoire pour un formulaire imbrique.
   * @param nomsAttributs les attributs a initialiser.
   * @param nomFormulaireImbrique le formulaire imbrique a traiter
   */
  public void initialiserAttributsObligatoire(String nomFormulaireImbrique,
      String[] nomsAttributs) {
    BaseForm formulaire = null;

    if (nomFormulaireImbrique != null) {
      formulaire =
          (BaseForm)getValeurAttribut(this, nomFormulaireImbrique, true);
    } else {
      formulaire = this;
    }

    if (nomsAttributs != null) {
      for (int i = 0; i < nomsAttributs.length; i++) {
        formulaire.supprimerRegleValidation(nomsAttributs[i], "required");
      }
    } else {
      formulaire.initialiserAttributObligatoire();
    }
  }

  /**
   * Initialiser les attributs obligatoire spécifiés
   * @param nomsAttributs la liste des attributs a traiter
   */
  public void initialiserAttributsObligatoire(String[] nomsAttributs) {
    if (nomsAttributs != null) {
      for (int i = 0; i < nomsAttributs.length; i++) {
        supprimerRegleValidation(nomsAttributs[i], "required");
      }
    } else {
      initialiserAttributObligatoire();
    }
  }

  /**
   * Initialiser les attributs obligatoire pour un formulaire imbriqués.
   * @param nomFormulaireImbrique
   */
  public void initialiserAttributsObligatoire(String nomFormulaireImbrique) {
    initialiserAttributsObligatoire(nomFormulaireImbrique, null);
  }

  /**
   * Initialiser tous les attributs obligatoires.
   * <p>
   * @param nomsAttributs Les noms d'attributs non obligatoires
   */
  public void initialiserAttributObligatoire() {
    supprimerRegleValidation("required");
  }

  /**
   * Ajouter un attribut qui doit avoir une valeur incluse dans un intervalle de valeur.
   * <p>
   * @param nomAttribut Les noms d'attributs obligatoires
   * @param messageErreur le message d'erreur à afficher en cas d'erreur de la validation
   * @param valeurMinimal la valeur minimale qui identifie si la valeur est en erreur ou pas
   * @param valeurMaximal la valeur maximale qui identifie si la valeur est en erreur ou pas
   */
  public void ajouterRegleInclusDansIntervalle(String nomAttribut,
      String messageErreur,
      Comparable valeurMinimal,
      Comparable valeurMaximal) {
    Comparateur comparateur = new Comparateur(valeurMinimal, valeurMaximal);
    UtilitaireBaseForm.ajouterRegleValidation(validationMap, nomAttribut,
        "inclusDansIntervalle",
        comparateur);
    attributsAvecMessagesErreur.put(nomAttribut, messageErreur);
  }

  /**
   * Ajouter un attribut qui doit avoir une valeur incluse dans un intervalle de valeur de date
   * sans heure.
   * <p>
   * @param nomAttribut Les noms d'attributs obligatoires
   * @param messageErreur le message d'erreur à afficher en cas d'erreur de la validation
   * @param valeurMinimal la valeur minimale qui identifie si la valeur est en erreur ou pas
   * @param valeurMaximal la valeur maximale qui identifie si la valeur est en erreur ou pas
   * @param seulementPourNouveauFormulaire true si validation pour nouveau formulaire.
   */
  public void ajouterRegleInclusDansIntervalleDate(String nomAttribut,
      String messageErreur,
      Comparable valeurMinimal,
      Comparable valeurMaximal,
      boolean seulementPourNouveauFormulaire) {
    ComparateurDate comparateur =
        new ComparateurDate(valeurMinimal, valeurMaximal);
    UtilitaireBaseForm.ajouterRegleValidation(validationMap, nomAttribut,
        "inclusDansIntervalle",
        comparateur);
    attributsAvecMessagesErreur.put(nomAttribut, messageErreur);

    if (seulementPourNouveauFormulaire) {
      validationNouveauFormulaireMap.put(nomAttribut, Boolean.TRUE);
    }
  }

  /**
   * Est-ce une validation pour nouveau formulaire seulement.
   * @param nomAttribut le nom attribut à valider
   * @return true si une validation pour nouveau formulaire seulement.
   */
  public boolean isValidationNouveauFormulaireSeulement(String nomAttribut) {
    return this.validationNouveauFormulaireMap.containsKey(nomAttribut);
  }

  /**
   * Ajouter les attributs qui vont servir a specifier l'ordre que les validations vont être faites.
   * <p>
   * @param nomsAttributs Les noms d'attributs obligatoires
   */
  public void specifierOrdreValidation(String[] nomsAttributs) {
    attributsOrdreValidations = new ArrayList();

    for (int i = 0; i < nomsAttributs.length; i++) {
      attributsOrdreValidations.add(nomsAttributs[i]);
    }
  }

  /**
   * Configure la valeur par defaut d'un attribut en particulier
   * lorsque l'attribut associe a un objet de transfert
   * est a <code>null</code>.
   * <p>
   * @param nomAttribut le nom de l'attribut
   * @param valeur la valeur à afficher
   */
  public void setValeurParDefaut(String nomAttribut, Object valeur) {
    valeurParDefautMap.put(nomAttribut, valeur);
  }

  /**
   * Configure le formatage par défaut pour un attribut en particulier.
   * <p>
   * @param nomAttribut le nom de l'attribut
   * @param type le type de classe
   */
  public void ajouterTypeFormatter(String nomAttribut, Class type) {
    if (!org.sofiframework.presentation.struts.form.formatter.Formatter.class.isAssignableFrom(type)) {
      throw new FormatterException(type + "Doit être un formatter");
    }

    formatMap.put(nomAttribut, type);
  }

  /**
   * Configure le formatage d'une date pour que l'heure et seconde soit spécifié.
   * <p>
   * @param nomAttribut le nom de l'attribut
   * @param type le type de classe
   */
  public void ajouterFormatterDateHeureSeconde(String nomAttribut) {
    formatMap.put(nomAttribut,
        org.sofiframework.presentation.struts.form.formatter.FormatterDateHeureSeconde.class);
  }

  /**
   * Configure le formatage d'une date pour que l'heure soit spécifié.
   * <p>
   * @param nomAttribut le nom de l'attribut
   * @param type le type de classe
   */
  public void ajouterFormatterDateHeure(String nomAttribut) {
    formatMap.put(nomAttribut,
        org.sofiframework.presentation.struts.form.formatter.FormatterDateHeure.class);
  }

  /**
   * Ajouter un format spécique pour un attribut
   * @param message un message spécifique si le format n'est pas respecté.
   * @param format un format spécifique pour l'attribut spécifié.
   * @param nomAttribut le nom d'attribut qui requiert un format spécifique.
   */
  public void ajouterFormat(String nomFormulaireImbrique, String nomAttribut,
      String format, String formatSaisie,
      String formatSortie, String message) {
    if (format != null) {
      BaseForm formulaire =
          getFormulairePourValidation(nomFormulaireImbrique, nomAttribut);

      // Ajouter le format spécifique.
      formulaire.getListeAttributAvecFormat().remove(UtilitaireBaseForm.getNomAttributSansImbrique(nomAttribut));
      formulaire.getListeAttributAvecFormat().put(UtilitaireBaseForm.getNomAttributSansImbrique(nomAttribut),
          new FormatMessage(format,
              formatSaisie,
              formatSortie,
              message));
    }
  }

  /**
   * Traiter les format a respecter lorsque qu'on utilise un fichier.
   * @param messageErreur le message d'erreur
   * @param tailleMaximale la taille maximale.
   * @param type le type de fichier supporté.
   * @param nomAttribut le nom d'attribut a traiter.
   * @param nomFormulaireImbrique le nom de formulaire imbriqué s'il y a lieu.
   */
  public void ajouterFormat(String nomFormulaireImbrique, String nomAttribut,
      String type, Integer tailleMaximale,
      String messageErreur) {
    BaseForm formulaire =
        getFormulairePourValidation(nomFormulaireImbrique, nomAttribut);

    // Ajouter le format spécifique.
    formulaire.getListeAttributAvecFormat().remove(UtilitaireBaseForm.getNomAttributSansImbrique(nomAttribut));
    formulaire.getListeAttributAvecFormat().put(UtilitaireBaseForm.getNomAttributSansImbrique(nomAttribut),
        new FormatMessage(type,
            tailleMaximale,
            messageErreur));
  }

  private BaseForm getFormulairePourValidation(String nomFormulaireImbrique,
      String nomAttribut) {
    BaseForm formulaire = null;

    if (nomFormulaireImbrique != null) {
      formulaire = (BaseForm)getValeurAttribut(this, nomFormulaireImbrique, true);
    } else {
      formulaire = this;
    }

    return formulaire;
  }

  /**
   * Retourne l'objet de transfert correspondant au formulaire.
   * <p>
   * IMPORTANT: vous devez au préalable avoir populer l'objet de transfert
   * avec l'aide d'un formulaire ou encore le formulaire réside dans la portée
   * session.
   * @return l'objet de transfert correspondant au formulaire
   */
  public Object getObjetTransfert() {
    return this.objetTransfert;
  }

  /**
   * Spécifie un objet de transfert au formulaire.
   * <p>
   * @param Object l'objet de transfert avec lequel le formulaire doit interagir
   */
  public void setObjetTransfert(Object objetTransfert) {
    if (ObjetTransfert.class.isInstance(objetTransfert)) {
      this.objetTransfert = objetTransfert;
    }
  }

  /**
   * Obtenir la liste des attributs à populer.
   * @param objet Objet qui est la source de la population
   * @param mode Mode vers le formulaire ou vers l'objet de transfert
   * @return Liste d'attributs
   */
  private Iterator getIterateurAttribut(Object objet, int mode) {
    Map attributs = null;

    boolean versFormulaire = (mode == VERS_FORMULAIRE);

    if ((versFormulaire || !isFormulaireInitialiseAvecObjetTransfert()) &&
        !populerAvecAttributsFormulaire) {
      /*
       * Si il s'agit d'une population normale du formulaire à partir
       * des attributs de l'objet de transfert.
       */
      attributs = representation(objet, mode);
    } else if (versFormulaire && populerAvecAttributsFormulaire) {
      /*
       * Si l'on popule le formulaire avec un objet de
       * tranfert en se basant sur la liste des attributs du formulaire.
       */
      attributs = representation(this, mode);

      /*
       * On doit enlever les attributs qui ne sont pas dans l'objet de transfert.
       */
      Map attributsOt = representation(objet, mode);
      Set attributSupprimer = new HashSet();

      for (Iterator i = attributs.keySet().iterator(); i.hasNext(); ) {
        Object cle = i.next();
        if (!attributsOt.containsKey(cle)) {
          attributSupprimer.add(cle);
        }
      }

      for (Iterator i = attributSupprimer.iterator(); i.hasNext(); ) {
        Object cle = i.next();
        attributs.remove(cle);
      }
    } else {
      if (isTraiterAttributsAfficheSeulement()) {
        if (nomListe == null) {
          if (getListeAttributATraiter() != null) {
            attributs = getListeAttributATraiter();
          }
        } else {
          attributs =
              formulaireParent.getListeAttributATraiterParFormulaireImbrique(nomListe);
        }
      } else {
        attributs = representation(objet, mode);
      }
    }

    return (attributs == null) ? null : attributs.keySet().iterator();
  }

  /**
   * Popule le formulaire en appliquant les validations et les formatters.
   * <p>
   * @param objetTransfert l'objet transfert à populer
   * @param mode le mode de population à utiliser (en aval ou en amont)
   * @param isAppelleDepuisNested valeur indiquant si c'est un Nested qui fait l'appel ou pas
   * @param request la requête HTTP en traitement
   * @return une liste d'erreurs
   */
  protected synchronized LinkedHashMap populate(Object objetTransfert, int mode,
      boolean isAppelleDepuisNested, HttpServletRequest request) {

    long debut = System.currentTimeMillis();

    // Le formulaire principal.
    BaseForm formulaireParent =
        getFormulaireParent(); //BaseForm.getFormulaire(request.getSession());

    if (log.isDebugEnabled()) {
      log.debug("Populer : " + this.getClass().getName());
      log.debug("Form parent : " + formulaireParent.getClass().getName());
    }

    if ((formulaireParent.getObjetTransfert() == null) &&
        objetTransfert instanceof ObjetTransfert) {
      formulaireParent.setObjetTransfert(objetTransfert);
    }

    // Indicateur pour savoir si le formulaire doit appliquer des validations.
    formulaireValidation = false;

    Object target = ((mode == VERS_FORMULAIRE) ? objetTransfert : this);

    if (log.isDebugEnabled()) {
      log.debug("Source des valeurs ('target') : " +
          target.getClass().getName());
    }

    //Map listeAttributsAvecValeur = representation(target, mode);
    Iterator listeAttributsAvecValeurIter = getIterateurAttribut(target, mode);

    if (GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.TRAITER_CHAMP_MODIFIE_SEULEMENT).booleanValue() &&
        (mode == VERS_OBJET_TRANSFERT)) {
      if (log.isDebugEnabled()) {
        log.debug("->Traiter seulement les champs modifiés du formulaire");
      }

      /* Si le formulaire ne contient pas d'erreurs on doit réinitialiser
       * les attributs modifiés de l'objet
       */
      if ((this.getObjetTransfert() != null) &&
          !isFormulaireContenaitDesErreurs()) {
        if (log.isDebugEnabled()) {
          log.debug("->Initialiser les attributs modifiés OT");
        }

        ((ObjetTransfert)this.getObjetTransfert()).reinitialiserLesAttributsModifies();

        if (log.isDebugEnabled()) {
          log.debug("Done!");
        }
      }
    }

    // Si un attribut identifiant un formulaire et type imbriqué (nested) est spécifier
    // vérifier si l'identifiant de l'objet de transfert original est à null,
    // sinon faire la validation du formulaire.
    if (getFormulaireIdentifiant() != null) {
      Object identifiant =
          getValeurAttribut(this.getObjetTransfert(), getFormulaireIdentifiant(),
              true);

      if (log.isDebugEnabled()) {
        log.debug("Identifiant OT : " + identifiant);
      }

      if ((identifiant != null) && !identifiant.equals("")) {
        formulaireValidation = true;
      } else {
        formulaireValidation = false;
      }

      if (log.isDebugEnabled()) {
        log.debug("Validation == " + formulaireValidation);
      }
    }

    //Avant populer des champs

    if (log.isDebugEnabled()) {
      long fin = System.currentTimeMillis();
      log.debug("T avant populer attributs : " + (fin - debut));
    }

    if (listeAttributsAvecValeurIter != null) {
      while (listeAttributsAvecValeurIter.hasNext()) {
        long debutAttribut = System.currentTimeMillis();

        String nomAttribut = (String)listeAttributsAvecValeurIter.next();

        if (log.isDebugEnabled()) {
          log.debug("  - Populer attribut " + nomAttribut);
        }

        // Générer l'attribut unique complet
        String nomAttributComplet = nomAttribut;

        if (getNomListe() != null) {
          StringBuffer nomAttributImbrique = new StringBuffer();
          nomAttributImbrique.append(getNomListe());
          nomAttributImbrique.append(".");
          nomAttributImbrique.append(nomAttribut);
          nomAttributComplet = nomAttributImbrique.toString();
        }

        boolean nbRecursifMaximum = false;

        if (nomAttributComplet != null) {
          if (UtilitaireString.getNbMotDansChaine(nomAttributComplet,
              nomAttribut) > 4) {
            nbRecursifMaximum = true;
          }
        }

        if (!getFormulaireParent().getListeAttributTraite().contains(nomAttributComplet) &&
            !nbRecursifMaximum) {
          boolean attributNested = false;
          boolean attributListeNested = false;
          String nomFormulaire = "";
          String nomListe = null;

          int nbNiveau = 0;

          if (nomAttribut != null) {
            StringTokenizer listeNiveau =
                new StringTokenizer(nomAttribut, ".");
            nbNiveau = listeNiveau.countTokens();
          }

          if (isTraiterAttributsAfficheSeulement() && (nomAttribut != null) &&
              (nomAttribut.indexOf(".") != -1)) {
            if ((nomAttribut.lastIndexOf("[") != -1) && (nbNiveau >= 2)) {
              // traitement des listes de formulaires imbriqués
              nomAttribut = getNomListeAttribut(nomAttribut);
              nomListe = nomAttribut;
              attributListeNested = true;
            } else {
              // Traiter les champs imbriqués
              nomFormulaire =
                  nomAttribut.substring(0, nomAttribut.lastIndexOf("."));

              //String attribut = nomAttribut.substring(nomAttribut.lastIndexOf(".") +
              //    1);
              nomAttribut = nomFormulaire;
            }

            attributNested = true;
          }

          if (!attributNested ||
              (attributNested && ((listeAttributATraiterParFormulaire ==
              null) ||
              (listeAttributATraiterParFormulaire.get(nomAttribut) ==
              null)))) {
            if (attributListeNested) {
              nomAttribut = nomListe;
            }

            Object valeurAttribut =
                getValeurAttribut(target, nomAttribut, false);

            // Si la population se fait de l'objet de transfert vers la formulaire
            // Vérifier que si l'objet a traiter est un objet avec getter et setter
            // Si c'est le cas, alors ajouter les valeurs de cet objet dans le formulaire.
            if (mode == VERS_FORMULAIRE) {
              try {
                populateProperty(objetTransfert, nomAttribut, valeurAttribut,
                    VERS_FORMULAIRE, null, request);
              } catch (FormatterException e) {
                e.printStackTrace();
              } catch (Exception e) {
                // Ce n'est pas une propriété supporté par le Formulaire
              }
            } else {
              try {
                if (!isFormulaireValidation()) {
                  Class type = null;

                  try {
                    type =
                        UtilitaireObjet.getClasse(objetTransfert, nomAttribut);
                  } catch (Exception e) {
                    type = String.class;
                  }

                  if (type == null) {
                    type = UtilitaireObjet.getClasse(this, nomAttribut);
                  }

                  if ((Boolean.class.isAssignableFrom(type) ||
                      boolean.class.isAssignableFrom(type)) ||
                      ((String[].class == type) ||
                          (type.isAssignableFrom(FormFile.class) ||
                              (type.isArray() &&
                                  type.getComponentType().isAssignableFrom(byte.class))))) {
                    if (!"isModifie".equals(nomAttribut) &&
                        boolean.class.isAssignableFrom(type) &&
                        ((Boolean)valeurAttribut).booleanValue()) {
                      setFormulaireValidation(true);
                    }
                  } else {
                    if (!isAttributSansValidation(nomAttribut) &&
                        (valeurAttribut != null) &&
                        !isListOfNestedObjects(valeurAttribut,
                            valeurAttribut.getClass()) &&
                            !UtilitaireBaseForm.isNestedObject(valeurAttribut.getClass()) &&
                            ((nomAttribut != null) &&
                                !nomAttribut.equals("nomListe"))) {
                      setFormulaireValidation(true);
                    }
                  }
                }

                populateProperty(objetTransfert, nomAttribut, valeurAttribut,
                    VERS_OBJET_TRANSFERT, nomListe, request);
              } catch (Exception ie) {
                if (ie instanceof SOFIException) {
                  throw (SOFIException)ie;
                } else {
                  log.debug(ie.toString(), ie);
                }

                // Rien faire
              }
            }
          }
        }

        long finAttribut = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
          log.debug("T populer attibut : " + (finAttribut - debutAttribut));
        }
      }
    }

    if (mode == VERS_OBJET_TRANSFERT) {
      // Préparation de l'objet de transfert après population du formulaire.
      preparerObjetTransfert((ObjetTransfert)objetTransfert, request);
    }

    if (getNomListe() != null) {
      setObjetTransfert(objetTransfert);
    }

    return lesErreurs;
  }

  /**
   * Permet d'obtenir la valeur d'un attribut de l'objet cible de la population
   * (peut être un formulaire ou un objet de transfert).
   * @return Valeur de l'attribut
   * @param nomAttribut Nom de l'attribut dont on désire obtenir la valeur
   * @param cible L'objet dont on désire obtenir la valeur d'un attribut.
   */
  protected Object getValeurAttribut(Object cible, String nomAttribut,
      boolean obtenirFormulaire) {
    try {
      Object valeur = UtilitaireObjet.getValeurAttribut(cible, nomAttribut);

      return valeur;
    } catch (Exception e) {
      log.info(e);
    }

    return null;
  }

  /**
   * Permet de retourner le dernier nom de liste
   * s'il y a plusieur niveau.
   * 
   * @author Sebastien Cayer
   * @version SOFI 2.1
   * @return le dernier nom de la liste a traiter.
   * @param nomListeAttribut
   */
  protected String getNomListeAttribut(String nomListeAttribut) {
    if ((nomListeAttribut.lastIndexOf("[") != -1)) {
      nomListeAttribut =
          nomListeAttribut.substring(0, nomListeAttribut.lastIndexOf("["));
      if ((nomListeAttribut.lastIndexOf("[") != -1)) {
        nomListeAttribut = getNomListeAttribut(nomListeAttribut);
      }
    }
    return nomListeAttribut;
  }

  /**
   * Si la liste offert est non-null, retourne une liste d'objets
   * populé avec les valeurs des objets offert par la liste.
   * <p>
   * @param source Le type de classe à tester
   * @param mode Le mode qui spécifier vers le formlaire ou vers l'objet de transfert
   * @return une liste d'objets
   * @throws java.lang.InstantiationException Exception générique
   * @throws java.lang.IllegalAccessException Exception générique
   */
  protected List populateList(List source, int mode, String nomListe,
      HttpServletRequest request) throws InstantiationException,
      IllegalAccessException {
    if (source == null) {
      return null;
    }

    ArrayList target = new ArrayList();

    Iterator sourceIter = source.iterator();
    int indiceNested = 0;

    while (sourceIter.hasNext()) {
      Object objetCourant = sourceIter.next();

      if (objetCourant instanceof ObjetTransfert ||
          objetCourant instanceof BaseForm) {
        // Créer le nom de la liste imbriqué à plus de 1 niveau.
        StringBuffer nomListeImbrique = new StringBuffer();
        nomListeImbrique.append(nomListe);
        nomListeImbrique.append("[");
        nomListeImbrique.append(indiceNested);
        nomListeImbrique.append("]");

        if (objetCourant instanceof BaseForm) {
          setFormulaireParent(this.getFormulaireParent());
        }

        Class targetClass = getTypeClasseATraiter(objetCourant.getClass());
        Object objetNouveau =
            populateNestedObject(objetCourant, indiceNested, mode,
                nomListeImbrique.toString(), targetClass,
                request);
        target.add(objetNouveau);
        indiceNested++;
      }
    }

    return target;
  }

  public Object populerFormulaireNested(Object objetTransfert, String nomNested,
      Class classeFormulaire, HttpServletRequest request) throws InstantiationException, IllegalAccessException {
    return this.populateNestedObject(objetTransfert, -1, VERS_FORMULAIRE, nomNested, classeFormulaire, request);
  }

  /**
   * Populer un objet de type Nested.
   * <p>
   * @param source Le type de classe à tester
   * @param indiceNested le numéro de l'indice de l'objet Nested
   * @param mode le monde de transfert des données (en aval ou en amont)
   * @param nomListe le nom de la liste qui correspond à la liste d'éléments nested
   * @param request la requete HTTP en traitement
   * @return l'objet populé d'un formulaire imbriqué (NESTED)
   * @throws java.lang.InstantiationException Exception générique
   * @throws java.lang.IllegalAccessException Exception générique
   */
  private Object populateNestedObject(Object source, int indiceNested,
      int mode, String nomListe,
      Class targetClass,
      HttpServletRequest request) throws InstantiationException,
      IllegalAccessException {
    Object target = null;
    Class sourceClass = null;

    if ((mode == VERS_FORMULAIRE) && (source == null)) {
      target = targetClass.newInstance();

      if (BaseForm.class.isInstance(target)) {
        BaseForm nouveauFormulaire = (BaseForm)target;
        Class typeSource =
            this.getTypeClasseATraiter(nouveauFormulaire.getClass());
        sourceClass = typeSource;
        source = sourceClass.newInstance();
      }
    }

    if (ObjetTransfert.class.isInstance(source) && (mode == VERS_FORMULAIRE)) {
      target = targetClass.newInstance();

      // Accéder au nouveau formulaire.
      BaseForm formulaire = ((BaseForm)target);

      // Ne pas ajouter la liste précédente
      if (getNomListe() != null) {
        nomListe = getNomListe() + "." + nomListe;
      }

      formulaire.setNomListe(nomListe);

      // Accéder à l'objet de transfert source.
      ObjetTransfert original = (ObjetTransfert)source;

      // Fixer l'objet de transfert avec l'original.
      formulaire.setObjetTransfert(original);

      // Fixer l'objet de transfert original avec une copie l'original.
      formulaire.setObjetTransfertOriginal((ObjetTransfert)original.clone());

      // Initialiser l'indicateur de modification
      formulaire.setFormulaireModifie(false);

      formulaire.setFormulaireParent(this.getFormulaireParent());

      // Populer le formulaire avec l'objet de transfert.
      formulaire.populate(source, mode, true, request);

      // Appel d'une méthode qui permet de personnaliser l'affichage du formulaire si surchargé.
      formulaire.preparerAffichage(request);
    } else {
      // Accéder au formulaire en traitement.
      BaseForm formulaire = ((BaseForm)source);

      if (getNomListe() != null) {
        nomListe = getNomListe() + "." + nomListe;
      }

      formulaire.setNomListe(nomListe);

      // Prendre l'objet de transfert existant pour débuter la population du formulaire vers l'objet de transfert.
      target = ((BaseForm)source).getObjetTransfert();

      if ((target == null) && (targetClass != null)) {
        if (isTraiterAttributsAfficheSeulement() &&
            (formulaire.getObjetTransfertOriginal() != null)) {
          target = formulaire.getObjetTransfertOriginal().clone();
        } else {
          // Si l'objet de transfert n'existe pas, alors le créer.
          target = targetClass.newInstance();
        }

        // Fixer dans le nouvel objet de transfert dans celui du formulaire principal.
        UtilitaireBaseForm.fixerProprieteDansObjetTransfert(this, null,
            nomListe, target,
            request);
      }

      formulaire.setIndiceNested(indiceNested);

      formulaire.setMapping(getMapping());
      formulaire.setExecuterValidation(isExecuterValidation());
      formulaire.setFormulaireInitialiseAvecObjetTransfert(isFormulaireInitialiseAvecObjetTransfert());
      formulaire.setTraiterAttributsAfficheSeulement(isTraiterAttributsAfficheSeulement());

      formulaire.lesErreurs = new LinkedHashMap();
      formulaire.setFormulaireParent(this.getFormulaireParent());
      formulaire.setFormulaireDirectementParent(this);
      formulaire.setListeAttributTraite(new HashSet());

      // Population de l'objet de transfert avec le formulaire.
      formulaire.populate(target, mode, true, request);

      // Preparer l'objet de transfert après population par le formulaire.
      formulaire.preparerObjetTransfert((ObjetTransfert)target, request);

      try {
        if (isExecuterValidation()) {
          formulaire.validerFormulaire(request);
        }
      } catch (Exception e) {
        throw new SOFIException("Erreur dans " +
            formulaire.getClass().getName() +
            ".validerFormulaire() : " + e.getMessage(), e);
      }

      formulaire.traiterLesErreurs(request, (ObjetTransfert)target, nomListe);
    }

    return target;
  }

  /**
   * Retourne <code>true</code si l'objet reçu correspond à une liste d'objets
   * imbriqués (Nested).
   * <p>
   * @param object l'objet à vérifier
   * @param type le type de classe à tester
   * @return valeur indiquant si l'objet correspond à une liste d'objets imbriqués
   */
  private static boolean isListOfNestedObjects(Object object, Class type) {
    if (type == null) {
      return false;
    }

    if (List.class.isAssignableFrom(type)) {
      if (object == null) {
        return true;
      }

      Iterator listIter = ((List)object).iterator();

      if (listIter.hasNext()) {
        Object obj = listIter.next();

        if (UtilitaireBaseForm.isNestedObject(obj.getClass())) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Méthode qui dois être réécris par la sous-classes qui contient des objets nested
   * afin qu'il retourne le type approprié.
   * <p>
   * Exemple de methode a utiliser:<br>
   * public Class getTypeClasseATraiter(Class type) {
   * &nbsp;&nbsp;if (type.isAssignableFrom(ObjetTransfert.class)) {
   * &nbsp;&nbsp;&nbsp;&nbsp;return xxxForm.class;
   * &nbsp;&nbsp;}else {
   * &nbsp;&nbsp;&nbsp;&nbsp;return ObjetTransfert.class;
   * &nbsp;&nbsp;}
   * }
   * @param type Le type de classe à tester
   * @return la classe correspondant au type donné
   */
  protected Class getTypeClasseATraiter(Class type) {
    Iterator iterateurTypeFormulaire =
        getFormulaireParent().typeFormulaireEnfants.keySet().iterator();

    Class typeClasseRetour = null;

    while (iterateurTypeFormulaire.hasNext() && (typeClasseRetour == null)) {
      Class objetTransfert = (Class)iterateurTypeFormulaire.next();
      Class formulaire =
          (Class)getFormulaireParent().typeFormulaireEnfants.get(objetTransfert);

      if (type.getName().equals(objetTransfert.getName())) {
        typeClasseRetour = formulaire;
      } else {
        if (type.getName().equals(formulaire.getName())) {
          typeClasseRetour = objetTransfert;
        }
      }
    }

    return typeClasseRetour;
  }

  /**
   * Si le formulaire contient des formulaires enfants, vous devez ajouter
   * le ou les correspondance avec son objet de transfert associé.
   * <p>
   * @param formulaire le nom du formulaire enfants
   * @param objetTransfert le nom de l'objet de transfert associé au formulaire enfant.
   */
  public void ajouterCorrespondanceFormulaireEnfants(Class formulaire,
      Class objetTransfert) {
    if (!BaseForm.class.isAssignableFrom(formulaire)) {
      throw new SOFIException("La classe " + formulaire.getName() +
          " doit hériter de BaseForm");
    }

    if (!ObjetTransfert.class.isAssignableFrom(objetTransfert)) {
      throw new SOFIException("La classe " + objetTransfert.getName() +
          " doit hériter de ObjetTransfert");
    }

    this.typeFormulaireEnfants.put(objetTransfert, formulaire);

    setFormulaireSurPlusieursPages(true);
  }

  /**
   * Popule l'attribut correspondant.
   * <p>
   * @param objetTransfert l'objet de transfert contenant l'attribut
   * @param nomAttribut le nom de l'attribut
   * @param obj L'ancienne valeur
   * @param mode spécifie le mode de transfert
   * @throws java.lang.InstantiationException Exception générique
   * @throws java.lang.IllegalAccessException Exception générique
   */
  private void populateProperty(Object objetTransfert, String nomAttribut,
      Object obj, int mode, String nomListe,
      HttpServletRequest request) throws InstantiationException,
      IllegalAccessException,
      NoSuchMethodException,
      InvocationTargetException {
    // Générer l'attribut unique complet
    String nomAttributComplet = nomAttribut;

    if (getNomListe() != null) {
      StringBuffer nomAttributImbrique = new StringBuffer();
      nomAttributImbrique.append(getNomListe());
      nomAttributImbrique.append(".");
      nomAttributImbrique.append(nomAttribut);
      nomAttributComplet = nomAttributImbrique.toString();
    }

    Object target = ((mode == VERS_FORMULAIRE) ? this : objetTransfert);

    Class type = null;

    if (objetTransfert instanceof BaseForm) {
      type = UtilitaireObjet.getClasse(objetTransfert, nomAttribut);
    } else {
      ObjetTransfert objetTransfertOriginal =
          (ObjetTransfert)getFormulaire(request.getSession()).getObjetTransfert();

      if (this.getNomListe() != null) {
        type = UtilitaireObjet.getClasse(objetTransfert, nomAttribut);
      } else {
        try {
          type =
              UtilitaireObjet.getClasse(objetTransfertOriginal, nomAttribut);

          if (type == null) {
            // Prendre le type de l'objet de transfet en traitement.
            type = UtilitaireObjet.getClasse(objetTransfert, nomAttribut);
          }
        } catch (Exception e) {
          // Dans le cas qu'il n'existe pas de type de classe dans le formulaire ou encore dans l'O.T.
          // et que la valeur est différent de null, alors prendre le type de la valeur. Utilisé
          // lors du formulaire BaseDynaForm.
          if (obj != null) {
            type = obj.getClass();
          }
        }
      }
    }

    if (type == null) {
      // Prendre le type du formulaire en traitement.
      type = UtilitaireObjet.getClasse(this, nomAttribut);
    }

    Object value = null;

    boolean isNestedObject = UtilitaireBaseForm.isNestedObject(type);
    boolean isListOfNestedObjects = isListOfNestedObjects(obj, type);

    if (isNestedObject && !isAttributAExclure(nomAttribut)) {
      // -1 specifie que c'est un nested object
      Class typeATraiter = null;

      if (mode == VERS_OBJET_TRANSFERT) {
        typeATraiter = UtilitaireObjet.getClasse(this, nomAttribut);

        BaseForm formulaireATraiter =
            (BaseForm)getValeurAttribut(this, nomAttribut, false);

        if (!formulaireATraiter.isCorrespondanceFormulaireEnfantTraite()) {
          copierCorrespondanceEnfantsDansFormulaireParent(nomAttribut,
              request);
        }
      } else {
        typeATraiter = UtilitaireObjet.getClasse(objetTransfert, nomAttribut);

        if (!isCorrespondanceFormulaireEnfantTraite()) {
          copierCorrespondanceEnfantsDansFormulaireParent(nomAttribut,
              request);
        }

        /*
         * Cette affectation du formulaire parent ne semble servir a rien
         */
        //setFormulaireParent(getFormulaireParent());
      }

      Class targetClass = getTypeClasseATraiter(typeATraiter);

      if (nomListe != null) {
        StringBuffer nouveauNomListe = new StringBuffer();
        nouveauNomListe.append(nomListe);
        nouveauNomListe.append(".");
        nouveauNomListe.append(nomAttribut);
        nomAttribut = nouveauNomListe.toString();
      }

      // Traiter seulement s'il existe une correspondance de formulaire d'associer
      value =
          populateNestedObject(obj, PAS_FORMULAIRE_IMBRIQUE, mode, nomAttribut,
              targetClass, request);
    }

    if (isListOfNestedObjects) {
      String nouveauNomListe = nomAttribut;

      copierCorrespondanceEnfantsDansFormulaireParent(nomAttribut, request);

      value = populateList((List)obj, mode, nouveauNomListe, request);
    }

    boolean erreur = false;

    if (!(isNestedObject || isListOfNestedObjects)) {
      try {
        String cleErreur = "";

        if (type != null) {
          if (isConvertirPropriete(this, (ObjetTransfert)objetTransfert,
              nomAttribut)) {
            // Faire seulement les validations si c'est le transfert du formulaire vers un
            // objet de transfert et qu'il n'est pas un attribut qui exclus les validations.
            if (!List.class.isAssignableFrom(type)) {
              value =
                  UtilitaireBaseForm.convertir(this, type, nomAttribut, obj,
                      mode, getLocale(request),
                      getTimeZone(request));
            } else {
              List liste = (List)obj;

              if (liste.size() == 0) {
                value = new ArrayList();
              } else {
                value = obj;
              }
            }

            if ((mode == VERS_OBJET_TRANSFERT) &&
                !isAttributSansValidation(nomAttribut) &&
                isExecuterValidation() &&
                !(!isFormulaireValidation() && (indiceNested > 0))) {
              if (isValiderObligatoires(request) &&
                  !isExclureValidation(request)) {
                if ((type != null) && type.isArray() &&
                    type.getComponentType().isAssignableFrom(byte.class)) {
                  FormFile fichier = (FormFile)obj;

                  if (fichier.getFileName().equals("") ||
                      (fichier.getFileSize() == 0)) {
                    cleErreur = ConstantesMessage.ERREUR_CHAMP_OBLIGATOIRE;
                    erreur = true;
                  }
                } else {
                  if (!UtilitaireBaseForm.validateRequired(this, nomAttribut,
                      (String)obj)) {
                    cleErreur = ConstantesMessage.ERREUR_CHAMP_OBLIGATOIRE;

                    erreur = true;
                  }
                }
              }

              if ((isValidationNouveauFormulaireSeulement(nomAttribut) &&
                  isNouveauFormulaire()) ||
                  !isValidationNouveauFormulaireSeulement(nomAttribut)) {
                if (!isExclureValidation(request) &&
                    !UtilitaireBaseForm.validerInclusDansIntervalle(this,
                        nomAttribut,
                        value)) {
                  erreur = true;
                  cleErreur =
                      (String)attributsAvecMessagesErreur.get(nomAttribut);
                }
              }
            }
          } else {
            if (isValiderObligatoires(request) &&
                !isExclureValidation(request) && isRequis(nomAttribut)) {
              if (obj == null ||
                  String.class.isInstance(obj) && "".equals(obj)) {
                cleErreur = ConstantesMessage.ERREUR_CHAMP_OBLIGATOIRE;
                erreur = true;
              }
            }

            value = obj;
          }

          // Si message d'erreur, ajouter l'erreur dans la liste des erreurs.
          if (erreur) {
            MessageErreur messageErreur = new MessageErreur();

            messageErreur.setFocus(nomAttributComplet);

            if (cleErreur == null) {
              cleErreur = "non défini";
            }

            messageErreur.setCleMessage(cleErreur);

            if (getNomListe() != null) {
              messageErreur.setNomListe(getNomListe());
              messageErreur.setNoLigne(new Integer(getIndiceNested()));
            }

            // Ajouter l'erreur dans la collection d'erreurs.
            Object[] messagesAssocies = new Object[] { messageErreur, null };

            lesErreurs.put(nomAttribut, messagesAssocies);
          }
        }
      } catch (FormatterException e) {
        String errorKey = e.getFormatter().getCleErreur();

        if (UtilitaireString.isVide(errorKey)) {
          errorKey = e.getMessage();
        }

        MessageErreur messageErreur = new MessageErreur();

        messageErreur.setFocus(nomAttributComplet);
        messageErreur.setCleMessage(errorKey);

        if (nomListe != null) {
          messageErreur.setNomListe(getNomListe());
          messageErreur.setNoLigne(new Integer(getIndiceNested()));
        }

        String[] parametres = null;

        String format = null;

        if (e.getFormatter().getFormatMessage() != null) {
          format = e.getFormatter().getFormatMessage().getFormat();
        }

        if (format != null) {
          parametres = new String[] { format };
        }

        Object[] messagesAssocies = new Object[] { messageErreur, parametres };

        lesErreurs.put(nomAttributComplet, messagesAssocies);
      }
    }

    getFormulaireParent().getListeAttributTraite().add(nomAttributComplet);

    if (target instanceof ObjetTransfert) {
      if (getNomListe() != null) {
        PropertyUtils.setSimpleProperty(target, nomAttribut, value);
      }

      UtilitaireBaseForm.fixerProprieteDansObjetTransfert(this, getNomListe(),
          nomAttribut, value,
          request);
    } else {
      PropertyUtils.setSimpleProperty(target, nomAttribut, value);
    }

    try {
      if (GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.TRAITER_CHAMP_MODIFIE_SEULEMENT).booleanValue() ||
          getFormulaire(request).isTraiterSeulementAttributModifie()) {
        // Lors de la copie d'un attribut à l'objet de transfert,
        // si l'attribut a été modifié, il sera ajouté dans la liste de modifications.
        if (mode == VERS_OBJET_TRANSFERT) {
          ObjetTransfert objetTransfertParent =
              (ObjetTransfert)getFormulaireParent().getObjetTransfert();

          if (isNouveauFormulaire()) {
            // Dans le cas d'un nouveau formulaire, tous les attributs possédant
            // une valeur doivent être ajoutés â la liste de modifications
            ((ObjetTransfert)objetTransfert).ajouterAttributModifie(nomAttribut);

            // Ajouter l'attribut modifié au formulaire parent si pas déjà spécifié.
            if (!objetTransfertParent.getAttributsModifies().containsKey(nomAttribut)) {
              objetTransfertParent.ajouterAttributModifie(nomAttribut);
            }
          } else {
            if (isAttributeModifie(nomAttribut)) {
              ((ObjetTransfert)objetTransfert).ajouterAttributModifie(nomAttribut);

              // Ajouter l'attribut modifié au formulaire parent si pas déjà spécifié.
              if (!objetTransfertParent.getAttributsModifies().containsKey(nomAttribut)) {
                objetTransfertParent.ajouterAttributModifie(nomAttribut);
              }
            }
          }
        }
      } else {
        ((ObjetTransfert)objetTransfert).ajouterAttributModifie("TOUS_LES_ATTRIBUTS");
      }
    } catch (Exception e) {
      // la propriété ne fait pas partie du formulaire.
    }
  }

  protected boolean isConvertirPropriete(BaseForm formulaire,
      ObjetTransfert objetTransfert,
      String nomPropriete) {

    Class typeFormulaire = UtilitaireObjet.getClasse(formulaire, nomPropriete);
    Class typeObjetTransfert = null;

    try {
      typeObjetTransfert =
          UtilitaireObjet.getClasse(objetTransfert, nomPropriete);
    } catch (Exception e) {
    }

    boolean traiterConversionParFormatter = true;
    if (typeObjetTransfert == null ||
        ((String[].class.isAssignableFrom(typeFormulaire) ||
            boolean.class.isAssignableFrom(typeFormulaire) ||
            Boolean.class.isAssignableFrom(typeFormulaire) ||
            (typeObjetTransfert != null && typeFormulaire.isArray() &&
            typeObjetTransfert.isArray())))) {
      traiterConversionParFormatter = false;
    }

    return traiterConversionParFormatter;
  }

  protected boolean isRequis(String nomAttribut) {
    Map rules = (Map)validationMap.get(nomAttribut);

    return (rules == null) ? false :
      (!Boolean.FALSE.equals(rules.get("required")));
  }

  /**
   * Retourne un Map contenant les valeurs provenant de
   * l'objetTransfert, correspond au nom des champs. Les noms d'attributs
   * qui correspondent au nom d'attributs a exclure <code>setAttributsAExclure()</code>
   * sont retire du tableau.
   * <p>
   * @param objetTransfert l'objet de transfert
   * @param mode le mode de transfert des données (en aval ou en amont)
   * @return Map contenant les noms d'attributs et valeur de l'objet de transfert.
   * @trows
   */
  protected Map representation(Object objetTransfert,
      int mode) throws FormatterException {
    String messageErreur =
        "Incapable de formatter cette valeur de l'objet de transfert : " +
            objetTransfert.getClass().getName();

    Map listeValeursObjetTransfertOriginal = null;

    try {
      listeValeursObjetTransfertOriginal =
          UtilitaireObjet.decrire(objetTransfert);
    } catch (IllegalAccessException iae) {
      throw new FormatterException(messageErreur, iae);
    } catch (InvocationTargetException ite) {
      throw new FormatterException(messageErreur, ite);
    } catch (NoSuchMethodException nsme) {
      throw new FormatterException(messageErreur, nsme);
    }

    //Map temporaire seulement pour aider a placer dans l'ordre désiré
    //les attributs qui ont une validation.
    Map listeAttributsValidationOriginal = new HashMap();

    // Retirer les attributs qui ont une validation.
    Iterator validationsIterateur = attributsOrdreValidations.iterator();

    while (validationsIterateur.hasNext()) {
      String nomAttribut = (String)validationsIterateur.next();
      Object attributValue =
          listeValeursObjetTransfertOriginal.get(nomAttribut);
      listeAttributsValidationOriginal.put(nomAttribut, attributValue);
      listeValeursObjetTransfertOriginal.remove(nomAttribut);
    }

    LinkedHashMap listeAttributObjetTransfert = new LinkedHashMap();

    Iterator nomAttributValueMapIter =
        listeValeursObjetTransfertOriginal.keySet().iterator();

    while (nomAttributValueMapIter.hasNext()) {
      String nomAttribut = (String)nomAttributValueMapIter.next();

      if (!isAttributAExclure(nomAttribut)) {
        Object valeurAttribut =
            listeValeursObjetTransfertOriginal.get(nomAttribut);
        listeAttributObjetTransfert.put(nomAttribut, valeurAttribut);
      }
    }

    validationsIterateur = attributsOrdreValidations.iterator();

    //Ajouter les attributs qui ont une validation dans l'ordre désiré
    while (validationsIterateur.hasNext()) {
      String nomAttribut = (String)validationsIterateur.next();
      Object valeurAttribut =
          listeAttributsValidationOriginal.get(nomAttribut);
      listeAttributObjetTransfert.put(nomAttribut, valeurAttribut);
    }

    return listeAttributObjetTransfert;
  }

  /**
   * Specifie les attributs qui ne seront pas valide dans le formulaire.
   * <p>
   * @param nomAttributs tableau qui qui correspond au attribut qui ne seront pas valide dans le formulaire
   */
  public void ajouterAttributSansValidation(String[] nomAttributs) {
    for (int i = 0; i < nomAttributs.length; i++) {
      attributsSansValidations.put(nomAttributs[i], Boolean.TRUE);
    }
  }

  /**
   * Retourne true si l'attribut ne doit pas être validé
   * sinon retourne false.
   * <p>
   * @param nomAttribut le nom de l'attribut à vérifier
   * @return valeur indiquant si l'attribut possède des validations ou pas
   */
  private boolean isAttributSansValidation(String nomAttribut) {
    Boolean isExiste = (Boolean)attributsSansValidations.get(nomAttribut);

    if (isExiste != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne le tableau de propriété qui ne doit
   * pas être populé automatiquement dans le formulaire.
   * Faite une ré-écriture pour spécifier des propriétés additionnal
   * Mais assurez-vous d'appeller <code>super</code>.
   * <p>
   * @return Un tableau de propriété qui doit être exclut
   */
  protected HashMap getAttributsAExclure() {
    return UtilitaireBaseForm.fixerAttributAExclure(this.attributsAExclure);
  }

  /**
   * Est-ce que l'attribut est à exclure ?
   * @param nomAttribut le nom d'attribut
   * @return true si l'attribut est à exclure.
   */
  public boolean isAttributAExclure(String nomAttribut) {
    if (getAttributsAExclure().get(nomAttribut) != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Spécifier des attributs supplémentaire à exclure lors de la
   * population de l'objet de transfert vers le formulaire.
   * <p>
   * @param newAttributAExclure le nom de l'attribut du formulaire à exclure de toutes validation
   */
  protected void ajouterAttributAExclure(String newAttributAExclure) {
    if (this.attributsAExclure == null) {
      this.attributsAExclure = new HashMap();
    }

    this.attributsAExclure.put(newAttributAExclure, Boolean.TRUE);
  }

  /**
   * Retourne un Formatter pour un type donné. Si le nom d'attribut
   * correspond a l'entrée du formatMap, le type de Formatter spécifié
   * va être utilisé au lieu du défaut correspondant au type du nom d'attribut.
   * <p>
   * @param nomAttribut le nom de l'attribut à être formatté
   * @param type le type de l'attribut à être formatté
   * @return un Formatter
   */
  public Formatter getFormatter(String nomAttribut, Class type) {
    Class typeFormat = (Class)formatMap.get(nomAttribut);

    if (typeFormat == null) {
      return org.sofiframework.presentation.struts.form.formatter.Formatter.getFormatter(type);
    }

    return org.sofiframework.presentation.struts.form.formatter.Formatter.getFormatter(typeFormat);
  }

  /**
   * Obtenir la liste des formatters.
   * @return Map de formatter
   */
  protected Map getFormatMap() {
    return this.formatMap;
  }

  /**
   * Initialise tous les attributs à leur valeur par défaut.
   * <p>
   * @param mapping le mapping utilisée pour obtenir le formulaire en cours
   * @param request la requête HTTP en traitement
   */
  @Override
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    boolean messageConfirmation =
        (getLesConfirmations() != null) && (getLesConfirmations().size() > 0);

    if (!RequestProcessorHelper.isFormulaireDejaInitialise(request, mapping) &&
        !messageConfirmation) {
      // Initialiser la liste des attributs traité par le formulaire.
      this.listeAttributTraite = new HashSet();
      this.listeErreurs = new ActionErrors();
      this.formulaireValidation = false;

      this.lesErreurs = new LinkedHashMap();
      this.lesAvertissements = new LinkedHashMap();
      this.lesErreursGenerales = new LinkedHashMap();
      this.lesInformations = new LinkedHashMap();

      setPopulerObjetTransfertTraite(false);
      this.indiceNested = -1;

      if (!isModeLectureSeulement() && !isModeAffichageSeulement()) {
        if ("POST".equals(request.getMethod()) &&
            isTraiterAttributsAfficheSeulement()) {
          initialiserAttributAffiche();
        }
      }

      // Modifié par JMP. Doit être enlevé sinon provoque conflit avec les nouveaux formulaires.
      request.getSession().removeAttribute(Constantes.PREMIER_MESSAGE_ERREUR);
    }

    //    if (messageConfirmation) {
    //      // Initialiser le message de confirmation seulement après affichage de ce message.
    //      this.lesConfirmations = new LinkedHashMap();
    //    }
    super.reset(mapping, request);
  }

  /**
   * Initialise les attributs qui sont affichés dans la page.
   * Est exécuté juste avant la population des données du
   * POST du formulaire. On initilise les attributs pour éviter que les
   * valeurs s'accumulent
   * dans le cas des groupes de checkbox. Initilise quand même les attributs
   * boolean a false et les autres propriétés a null.
   */
  public void initialiserAttributAffiche() {
    if (getListeAttributATraiter() != null) {
      for (Iterator iterateur = getListeAttributATraiter().keySet().iterator();
          iterateur.hasNext(); ) {
        String nomAttribut = (String)iterateur.next();
        Object valeur = null;

        try {
          Class type = UtilitaireObjet.getClasse(this, nomAttribut);

          /*
           * On doit seulement réinitialiser les boolean types de base.
           * Quand il s'agit d'un objet (incluant Boolean), il faut laisser les valeurs a null.
           */
          if (boolean.class.isAssignableFrom(type)) {
            valeur = Boolean.FALSE;
          }

          UtilitaireObjet.setPropriete(this, nomAttribut, valeur);
        } catch (Exception e) {
          if (log.isDebugEnabled()) {
            log.debug("La propriété " + nomAttribut +
                " ne ppeut être populée. Une erreur se produit.", e);
          }
        }
      }
    }
  }

  /**
   * Initialise tous les attributs à leur valeur par défaut.
   */
  protected void initialiserTousLesAttributs(boolean memeInstance) {
    UtilitaireBaseForm.initialiserTousLesAttributs(this, memeInstance);
  }

  /**
   * Initialise tous les attributs de type boolean et String[].
   */
  public void initialiserAttributBooleanEtTableau() {
    UtilitaireBaseForm.initialiserAttributBooleanEtTableau(this);
  }

  /**
   * Initialise l'état du formulaire, par exemple les erreurs.
   * <p>
   * @param request la requête HTTP en traitement
   */
  public void initialiserFormulaire(HttpServletRequest request,
      ActionMapping mapping) {
    initialiserFormulaire(request, mapping, false);
  }

  /**
   * Initialise l'état du formulaire, par exemple les erreurs.
   * <p>
   * @param request la requête HTTP en traitement
   * @param initialiserTousLesAttributs true si vous désirez tous les attributs du formulaire.
   */
  public void initialiserFormulaire(HttpServletRequest request,
      ActionMapping mapping,
      boolean initialiserTousLesAttributs) {
    reset(mapping, request);
    setMapping(mapping);

    if (initialiserTousLesAttributs) {
      initialiserTousLesAttributs(false);
    }
  }

  /**
   * Initialise l'état du formulaire, par exemple les erreurs, tout en spécifiant
   * un objet de transfert.
   * <p>
   * Vous pouvez spécifier d'initialiser tout les attributs ou de garder ceux qui sont
   * déjà spécifier.
   * @param request la requête HTTP en traitement
   * @param initialiserTousLesAttributs true si vous désirez tous les attributs du formulaire.
   */
  public void initialiserFormulaire(HttpServletRequest request,
      ActionMapping mapping,
      ObjetTransfert objetTransfert,
      boolean initialiserTousLesAttributs) {
    initialiserFormulaire(request, mapping);

    if (initialiserTousLesAttributs) {
      initialiserTousLesAttributs(false);
    }

    // Instancier tout les types ObjetTransfert imbriqué.
    if (getFormulaireParent().isInstancierObjetsTransfertImbrique()) {
      UtilitaireObjet.instancierInstancesImbrique(objetTransfert);
    }

    // Fixer l'objet de transfert.
    setObjetTransfert(objetTransfert);
    setObjetTransfertOriginal(null);
    setNouveauFormulaire(true);

    setFormulaireInitialiseAvecObjetTransfert(true);
  }

  /**
   * Méthode pouvant être surchargé afin de préparer la population d'un formulaire.
   * <p>
   * Cette méthode sera appellé avant la population Struts du formulaire.
   * @throws javax.servlet.ServletException
   * @param request la requête en traitement.
   * @param mapping l'instance de mapping traité.
   */
  public void preparerPopulation(ActionMapping mapping,
      HttpServletRequest request) throws ServletException {
  }

  /**
   * Méthode pouvant être surchargé afin de préparer l'affichage du formulaire
   * dans la page JSP.
   * <p>
   * Cette méthode sera appellé avant l'affichage du formulaire dans la page JSP.
   * @param request la requête en traitement.
   * @since SOFI 2.0.1
   */
  public void preparerAffichage(HttpServletRequest request) {
  }

  /**
   * Méthode pouvant être surchargé afin de préparer le contenu d'un
   * objet de transfert reçu par le formulaire.
   * <p>
   * Cette méthode sera appellé après la population du formulaire.
   * @param objetTransfert l'objet de transfert en traitement.
   * @param request la requête en traitement.
   * @since SOFI 2.1
   */
  public void preparerObjetTransfert(ObjetTransfert objetTransfert,
      HttpServletRequest request) {
  }

  /**
   * Retourne la valeur, utilisé par la balise ListeValeursTag.java.
   * <p>
   * @see org.sofiframework.presentation.balisesjsp.html.ListeValeursTag
   * @see org.sofiframework.presentation.balisesjsp.nested.NestedListeValeursTag
   * @return la valeur retourné
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur utilisé par la balise ListeValeursTag.java.
   * <p>
   * @param newValeur la valeur utilisé par la balise ListeValeursTag.java
   */
  public void setValeur(String newValeur) {
    valeur = newValeur;
  }

  /**
   * Retourne l'indice de formulaire imbriqué a traiter (NESTED).
   * <p>
   * @return l'indice du formulaire imbriqué a traiter (NESTED)
   */
  public int getIndiceNested() {
    return indiceNested;
  }

  /**
   * Fixer l'indice de formulaire imbriqué (NESTED).
   * <p>
   * @param newIndiceNested l'indice de formulaire imbriqué (NESTED)
   */
  public void setIndiceNested(int newIndiceNested) {
    indiceNested = newIndiceNested;
  }

  /**
   * Retourne la liste des erreurs.
   * <p>
   * @return la liste des erreurs
   */
  public LinkedHashMap getLesErreurs() {
    return this.lesErreurs;
  }

  /**
   * Fixer les erreurs du formulaire.
   * @param lesErreurs les erreurs du formulaire.
   */
  public void setLesErreurs(LinkedHashMap lesErreurs) {
    this.lesErreurs = lesErreurs;
  }

  /**
   * Retourne la liste des avertissements.
   * <p>
   * @return la liste des avertissements
   */
  public LinkedHashMap getLesAvertissements() {
    return this.lesAvertissements;
  }

  /**
   * Retourne la liste des erreurs générales
   * @return la liste des erreurs générales
   */
  public LinkedHashMap getLesErreursGenerales() {
    return this.lesErreursGenerales;
  }

  /**
   * Retourne la liste des informations.
   * <p>
   * @return la liste des informations
   */
  public LinkedHashMap getLesInformations() {
    return this.lesInformations;
  }

  /**
   * Retourne la liste des confirmations.
   * <p>
   * @return la liste des confirmations
   */
  public LinkedHashMap getLesConfirmations() {
    return this.lesConfirmations;
  }

  /**
   * Retourne le nom de la liste qui correspond à une liste d'objets imbriqués (NESTED).
   * <p>
   * @return le nom de liste de formulaire imbriqué (NESTED)
   */
  public String getNomListe() {
    return nomListe;
  }

  /**
   * Spécifie le nom de l'attribut qui correspond à une liste d'objets imbriqués (NESTED).
   * <p>
   * @param newNomListe le nom de l'attribut qui correspond à une liste d'objets imbriqués (NESTED)
   */
  private void setNomListe(String newNomListe) {
    nomListe = newNomListe;
  }

  /**
   * Retourne le nom de méthode appelé avec l'URI.
   * <p>
   * @return le nom de méthode
   */
  public String getMethode() {
    return methode;
  }

  /**
   * Avec l'aide du nom de méthode, spécifie si le formulaire doit appliquer
   * les validations automatisé.
   * <p>
   * A noter que si le début du nom de méthode débute par "modifier", il n'y aura
   * pas de validation automatisé.
   * <p>
   * @param newMethode le nom de la méthode
   */
  public void setMethode(String newMethode) {
    this.executerValidation =
        !((newMethode.trim().length() > 8) && newMethode.trim().substring(0,
            8).equals("modifier"));
    this.methode = newMethode;
  }

  /**
   * Retourne la valeur qui indique si le formulaire doit traiter les validations.
   * <p>
   * @return la valeur qui indique si le formulaire doit traiter les validations
   */
  private boolean isExecuterValidation() {
    return this.executerValidation;
  }

  /**
   * Fixer la valeur qui indique si le formulaire doit traiter les validations.
   * <p>
   * @param newIsExecuterValidation la valeur qui indique si le formulaire doit traiter les validations
   */
  private void setExecuterValidation(boolean newExecuterValidation) {
    this.executerValidation = newExecuterValidation;
  }

  /**
   * Fixer la valeur qui indique si le formulaire est disposé a exécuter ces validations.
   * <p>
   * @return la valeur qui indique si le formulaire est disposé a exécuter ces validations
   */
  private boolean isFormulaireValidation() {
    return this.formulaireValidation;
  }

  /**
   * Fixer la valeur qui indique si le formulaire est disposé a exécuter ces validations.
   * @param newFormulaireValidation la valeur qui indique si le formulaire est disposé a exécuter ces validations
   */
  private void setFormulaireValidation(boolean newFormulaireValidation) {
    this.formulaireValidation = newFormulaireValidation;
  }

  /**
   * Retourne un indicateur qui permet de savoir si le formulaire a été modifié.
   * <p>
   * La valeur est transféré à l'objet de transfert lors du passage du formulaire
   * à l'objet de transfert.
   * <p>
   * La valeur "1" indique que le formulaire est modifié.
   * @return caractère spécifiant que le formulaire à été modifié.
   */
  protected String getIndModification() {
    return indModification;
  }

  /**
   * Fixer un indicateur permettant de spécifier si le formulaire a été modifié.
   * <p>
   * La valeur "1" indique que le formulaire est modifié.
   * @param indModification l'indicateur permettant de spécifier si le formulaire a été modifié
   */
  public void setIndModification(String indModification) {
    this.indModification = indModification;
  }

  /**
   * Retourne la valeur indiquant si le formulaire a été modifié.
   * <p>
   * @return la valeur indiquant si le formulaire a été modifié
   */
  public boolean isModifie() {
    if ((getIndModification() != null) &&
        getIndModification().equals(BaseForm.IND_MODIFIE)) {
      if (GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.TRAITER_CHAMP_MODIFIE_SEULEMENT).booleanValue() &&
          isPopulerObjetTransfertTraite()) {
        // Si traitement des champs modifié seulement.
        ObjetTransfert objetTransfert = (ObjetTransfert)getObjetTransfert();

        if (objetTransfert != null) {
          if ((objetTransfert.getAttributsModifies() != null) &&
              (objetTransfert.getAttributsModifies().size() > 0)) {
            return true;
          } else {
            return false;
          }
        }
      }

      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer true si le formulaire a été modifié.
   * @param modifie true si le formulaire a été modifié.
   */
  public void setFormulaireModifie(boolean modifie) {
    if (modifie) {
      setIndModification(BaseForm.IND_MODIFIE);
    } else {
      setIndModification(BaseForm.IND_NON_MODIFIE);
    }
  }

  /**
   * Est-ce que le formulaire est en lecture seulement.
   * <p>
   * @return true si le formulaire est en lecture seulement.
   */
  public boolean isModeLectureSeulement() {
    return modeLectureSeulement;
  }

  /**
   * Fixer la valeur indiquant si le formulaire est en mode de lecture seulement.
   * @param modeLectureSeulement la valeur indiquant si le formulaire est en mode de lecture seulement
   */
  public void setModeLectureSeulement(boolean modeLectureSeulement) {
    this.modeLectureSeulement = modeLectureSeulement;
  }

  /**
   * Est-ce que le formulaire est en affichage seulement.
   * <p>
   * @return true si le formulaire est en affichage seulement.
   */
  public boolean isModeAffichageSeulement() {
    return modeAffichageSeulement;
  }

  /**
   * Fixer la valeur indiquant si le formulaire est en mode affichage seulement.
   * @param modeAffichageSeulement la valeur indiquant si le formulaire est en mode affichage seulement
   */
  public void setModeAffichageSeulement(boolean modeAffichageSeulement) {
    this.modeAffichageSeulement = modeAffichageSeulement;
  }

  /**
   * Retourne le nom d'attribut qui est l'identifiant du formulaire.
   * <p>
   * Utilisé lors de l'utilisation de formulaire de type imbriqué (Nested).
   * <p>
   * @return l'attribut identifiant le formulaire
   */
  public String getFormulaireIdentifiant() {
    return formulaireIdentifiant;
  }

  /**
   * Spécifie l'attribut qui est l'identifiant du formulaire. Utilisé afin
   * de pouvoir spécifier facilement si le formulaire est nouveau.
   * <p>
   * Il est obligatoire de spécifier un attribut identifiant un formulaire
   * de type imbriqué (Nested)
   * <p>
   * @param formulaireIdentifiant attribut identifiant le formulaire
   */
  public void setFormulaireIdentifiant(String formulaireIdentifiant) {
    this.formulaireIdentifiant = formulaireIdentifiant;
  }

  /**
   * Est-ce que le formulaire est utilisé par plusieurs pages?
   * @return true si le formulaire est utilisé par plusieurs pages
   */
  public boolean isFormulaireSurPlusieursPages() {
    return formulaireSurPlusieursPages;
  }

  /**
   * Fixer true si le formulaire est utilisé par plusieurs pages
   */
  public void setFormulaireSurPlusieursPages(boolean formulaireSurPlusieursPages) {
    setTraiterAttributsAfficheSeulement(true);
    setValidationEnCache(false);
    this.formulaireSurPlusieursPages = formulaireSurPlusieursPages;
  }

  /**
   * Retourne le messageErreur correspondant à un nom d'attribut.
   * <p>
   * @param attribut le nom d'attribut que l'on désire avoir l'erreur.
   * @return le message d'erreur.
   */
  public MessageErreur getMessageErreur(String attribut) {
    if (getLesErreurs() != null) {
      return (MessageErreur)getLesErreurs().get(attribut);
    } else {
      return null;
    }
  }

  /**
   * Retourne le messageErreur correspondant à un nom d'attribut d'une
   * liste imbriqués.
   * <p>
   * @param attribut le nom d'attribut que l'on désire avoir l'erreur.
   * @param nomListe le nom de la liste dont l'objet imbriqués est inclus.
   * @return le message d'erreur.
   */
  public MessageErreur getMessageErreur(String attribut, String nomListe,
      String indice) {
    if (getLesErreurs() != null) {
      StringBuffer cle = new StringBuffer();
      cle.append(nomListe);
      cle.append(attribut);
      cle.append(indice);

      return (MessageErreur)getLesErreurs().get(cle.toString());
    } else {
      return null;
    }
  }

  /**
   * Retourne le messageErreur correspondant à un nom d'attribut d'une
   * liste imbriqués.
   * <p>
   * @param attribut le nom d'attribut que l'on désire avoir l'erreur.
   * @param nomListe le nom de la liste dont l'objet imbriqués est inclus.
   * @return le message d'erreur.
   */
  public boolean isMessageErreur(String attribut, String nomListe) {
    if (getLesErreurs() != null) {
      if (nomListe != null) {
        Iterator iterateur = getLesErreurs().keySet().iterator();

        while (iterateur.hasNext()) {
          String cle = (String)iterateur.next();

          if ((cle.length() > nomListe.length()) &&
              cle.substring(0, nomListe.length()).equals(nomListe)) {
            MessageErreur erreur = (MessageErreur)getLesErreurs().get(cle);

            if (erreur.getFocus().equals(attribut)) {
              return true;
            }
          }
        }
      } else {
        MessageErreur erreur = getMessageErreur(attribut);

        if (erreur != null) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Permet d'ajouter correctement une erreur aux erreurs deja présentes dans un formulaire.
   * <p>
   * @param cleErreur Clé du message
   * @param attribut l'attribut qui est en erreur
   * @param parametres parametre a ajouter au message
   * @param request Requete HTTP en cour de validation.
   * @return suite d'erreur struts (indique a struts qui il a une erreur).
   */
  public void ajouterMessageErreur(String cleErreur, String attribut,
      Object[] parametres,
      HttpServletRequest request) {
    UtilitaireBaseForm.ajouterMessageErreur(this, cleErreur, attribut,
        parametres, request);
  }

  /**
   * Permet d'ajouter correctement une erreur aux erreurs deja présentes dans un formulaire.
   * <p>
   * @param cleErreur Clé du message
   * @param attribut qui est en erreur
   * @param request Requete HTTP en cour de validation.
   * @return Suite d'erreur struts (indique a struts qui il a une erreur).
   */
  public void ajouterMessageErreur(String cleErreur, String attribut,
      HttpServletRequest request) {
    ajouterMessageErreur(cleErreur, attribut, null, request);
  }

  /**
   * Permet d'ajouter correctement une erreur aux erreurs deja présentes dans un formulaire.
   * <p>
   * @param cleErreur Clé du message
   * @param request Requete HTTP en cour de validation.
   * @return Suite d'erreur struts (indique a struts qui il a une erreur).
   */
  public void ajouterMessageErreur(String cleErreur,
      HttpServletRequest request) {
    ajouterMessageErreur(cleErreur, null, null, request);
  }

  /**
   * Permet d'ajouter correctement une erreur aux erreurs deja présentes dans un formulaire.
   * <p>
   * @param exceptionErreur L'exception provenant du modèle.
   * @param request Requete HTTP en cour de validation.
   * @return Suite d'erreur struts (indique a struts qui il a une erreur).
   */
  public void ajouterMessageErreur(ModeleException exceptionErreur,
      HttpServletRequest request) {
    ajouterMessageErreurBD(exceptionErreur, request);
  }

  /**
   * Méthode qui peut être implantée par une classe parent.
   * @param exceptionErreur
   * @param request
   */
  public void ajouterMessageErreurBD(ModeleException exceptionErreur,
      HttpServletRequest request) {
    String messageErreurBD = exceptionErreur.getMessage();

    if (messageErreurBD != null) {
      if ((exceptionErreur.getDetail() != null) &&
          (exceptionErreur.getDetail().length != 0) &&
          exceptionErreur.getDetail()[0] instanceof SQLException) {
        SQLException sqlException =
            (SQLException)exceptionErreur.getDetail()[0];
        messageErreurBD = sqlException.getMessage();

        if (GestionSurveillance.getInstance().isActif()) {
          // Ajouter la page en échec au gestionnaire de la surveillance.
          GestionSurveillance.ajouterPageEnEchec(request, mapping,
              messageErreurBD);
        }
      }

      if (messageErreurBD.indexOf("JBO-") != -1) {
        if (GestionSurveillance.getInstance().isActif()) {
          // Ajouter la page en échec au gestionnaire de la surveillance.
          GestionSurveillance.ajouterPageEnEchec(request, mapping,
              messageErreurBD);
        }
      }
    } else {
      throw new SOFIException(exceptionErreur);
    }

    ajouterMessageErreur(messageErreurBD, null, null, request);
  }

  /**
   * Permet d'ajouter correctement un message d'erreur général.
   * @param cleMessage Clé du message
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageErreurGeneral(String cleMessage,
      HttpServletRequest request) {
    ajouterMessageErreurGeneral(cleMessage, null, request);
  }

  /**
   * Permet d'ajouter correctement un message d'erreur général.
   * @param cleMessage Clé du message
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageErreurGeneral(String cleMessage,
      Object[] parametres,
      HttpServletRequest request) {
    MessageErreur messageErreur =
        UtilitaireMessage.getMessageErreur(cleMessage, parametres, request);

    // Ajouter le message d'erreur dans la liste générale.
    this.lesErreursGenerales.put(cleMessage, messageErreur);

    request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT,
        messageErreur);

    // Journaliser le message d'erreur si demandé.
    ecrireJournalMesageErreur(messageErreur);

    //Fixer le nom du formulaire
    setNomFormulaire(request, getMapping());
  }

  /**
   * Permet d'ajouter correctement un message d'avertissement.
   * @param cleMessage Clé du message
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageAvertissement(String cleMessage,
      HttpServletRequest request) {
    ajouterMessageAvertissement(cleMessage, null, request);
  }

  /**
   * Permet d'ajouter correctement un message d'avertissement.
   * @param cleMessage Clé du message
   * @param parametres Liste de paramètre à intégrer au message.
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageAvertissement(String cleMessage,
      Object[] parametres,
      HttpServletRequest request) {
    Message message = UtilitaireMessage.get(cleMessage, parametres, request);
    this.lesAvertissements.put(cleMessage, message);

    if (MessageAvertissement.class.isInstance(message)) {
      MessageAvertissement avertissement = (MessageAvertissement)message;

      request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT,
          avertissement);

      // Journaliser le message d'avertissement si demandé.
      ecrireJournalMessageAvertissement(avertissement);
    }

    //Fixer le nom du formulaire
    setNomFormulaire(request, getMapping());
  }

  /**
   * Permet d'ajouter correctement un message de confirmation.
   * @param cleMessage Clé du message
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageConfirmation(String cleMessage,
      HttpServletRequest request) {
    ajouterMessageConfirmation(cleMessage, null, request);
  }

  /**
   * Permet d'ajouter correctement un message de confirmation.
   * @param cleMessage Clé du message
   * @param parametres Liste de paramètre à intégrer au message.
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageConfirmation(String cleMessage,
      Object[] parametres,
      HttpServletRequest request) {
    Message confirmation =
        UtilitaireMessage.get(cleMessage, parametres, request);
    this.lesConfirmations.put(cleMessage, confirmation);
    setNomFormulaire(request, getMapping());
  }

  /**
   * Permet d'ajouter correctement un message d'information.
   * @param cleMessage Clé du message
   * @param request Requete HTTP en cour de validation.
   */
  public void ajouterMessageInformatif(String cleMessage,
      HttpServletRequest request) {
    ajouterMessageInformatif(cleMessage, null, request);
  }

  /**
   * Permet d'ajouter correctement un message d'information.
   * @param cleMessage Clé du message
   * @param parametres Liste de paramètre à intégrer au message.
   * @param request Requete HTTP en cours de validation.
   */
  public void ajouterMessageInformatif(String cleMessage, Object[] parametres,
      HttpServletRequest request) {
    if (!this.lesInformations.containsKey(cleMessage)) {
      Message message = UtilitaireMessage.get(cleMessage, parametres, request);
      request.getSession().setAttribute(Constantes.MESSAGE_BARRE_STATUT,
          message);
      this.lesInformations.put(cleMessage, message);
      if (MessageInformatif.class.isInstance(message)) {
        MessageInformatif messageInformatif = (MessageInformatif) message;

        // Journaliser le message d'informatif si demandé.
        ecrireJournalMessageInformatif(messageInformatif);
      }
    }
    setNomFormulaire(request, getMapping());
  }

  /**
   * Obtenir le mapping de l'action Struts.
   * <p>
   * @return le mapping de l'action Struts
   */
  public ActionMapping getMapping() {
    return this.mapping;
  }

  /**
   * Fixer le mapping de l'action Struts.
   * <p>
   * @param mapping le mapping de l'action Struts
   */
  public void setMapping(ActionMapping mapping) {
    this.mapping = mapping;
  }

  /**
   * Est-ce un nouveau formulaire ?
   * @return true si un nouveau formulaire.
   */
  public boolean isNouveauFormulaire() {
    if (nouveauFormulaire) {
      return true;
    }

    if (getFormulaireIdentifiant() != null) {
      Object identifiant =
          getValeurAttribut(this, getFormulaireIdentifiant(), true);

      if ((identifiant != null) && !identifiant.equals("")) {
        return false;
      } else {
        return true;
      }
    } else {
      // Traitement si l'objet de transfert original existe sinon automatiquement
      // c'est un nouveau formulaire.
      if (getObjetTransfertOriginal() != null) {
        String[] cleObjetTransfert =
            getObjetTransfertOriginal().getCle();

        if (cleObjetTransfert != null) {
          boolean nouveauFormulaire = true;

          for (int i = 0; i < cleObjetTransfert.length; i++) {
            Object valeurCle =
                getValeurAttribut(getObjetTransfertOriginal(), cleObjetTransfert[i],
                    false);

            if (valeurCle != null) {
              nouveauFormulaire = false;
            } else {
              nouveauFormulaire = true;
            }
          }

          if (nouveauFormulaire) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return true;
      }
    }

    return nouveauFormulaire;
  }

  /**
   * Fixer true si un nouveau formulaire
   * @param nouveauFormulaire true si un nouveau formulaire.
   */
  public void setNouveauFormulaire(boolean nouveauFormulaire) {
    this.nouveauFormulaire = nouveauFormulaire;
  }

  /**
   * Retourne true si le formulaire est initialisé avec un objet de transfert.
   * @return true si le formulaire est initialisé avec un objet de transfert.
   */
  private boolean isFormulaireInitialiseAvecObjetTransfert() {
    return formulaireInitialiseAvecObjetTransfert;
  }

  /**
   * Fixer true si le formulaire est initialiser avec un objet de transfert.
   * @param true si le formulaire est initialiser avec un objet de transfert.
   */
  protected void setFormulaireInitialiseAvecObjetTransfert(boolean formulaireInitialiseAvecObjetTransfert) {
    this.formulaireInitialiseAvecObjetTransfert =
        formulaireInitialiseAvecObjetTransfert;
  }

  /**
   * Retourne un objet de transfert cloné.
   */
  private ObjetTransfert getObjetTransfertClone() {
    ObjetTransfert refObjetTransfert = (ObjetTransfert)this.objetTransfert;
    ObjetTransfert nouveauObjetTransfert = null;

    if (refObjetTransfert != null) {
      nouveauObjetTransfert = (ObjetTransfert)refObjetTransfert.clone();
    }

    return nouveauObjetTransfert;
  }

  /**
   * Retourne le formulaire contient un message de tout type (Erreur, avertissement ou informations)
   * @return true si le formulaire contient un message de tout type (Erreur, avertissement ou informations)
   */
  public boolean isMessageDansFormulaire() {
    boolean retour =
        isFormulaireEnErreur() || (getLesAvertissements().size() > 0) ||
        (getLesInformations().size() > 0);

    return retour;
  }

  /**
   * Est-ce que le formulaire contient des messages d'erreurs.
   * @return true si le formulaire contient des messages d'erreurs.
   */
  public boolean isFormulaireEnErreur() {
    boolean retour =
        (getLesErreurs().size() > 0) || (getLesErreursGenerales().size() > 0);

    return retour;
  }

  /**
   * Est-ce que le formulaire contient des messages d'erreurs ou des message de confirmations.
   * @return true si le formulaire contient des messages d'erreurs ou des message de confirmations.
   */
  public boolean isFormulaireEnErreurOuConfirmation() {
    boolean retour =
        isFormulaireEnErreur() || (getLesConfirmations().size() > 0);

    return retour;
  }

  /**
   *  Est-ce que le formulaire contient des messages d'erreurs.
   * @return true si le formulaire contient des messages d'erreurs.
   * @deprecated Utiliser plutot isFormulaireEnErreur(request);
   */
  @Deprecated
  public boolean getIsFormulaireEnErreur() {
    return isFormulaireEnErreur();
  }

  public boolean isPopulerObjetTransfertTraite() {
    return populerObjetTransfertTraite;
  }

  public void setPopulerObjetTransfertTraite(boolean populerObjetTransfertTraite) {
    this.populerObjetTransfertTraite = populerObjetTransfertTraite;
  }

  public void setFormulaireContenaitDesErreurs(boolean formulaireContenaitDesErreurs) {
    this.formulaireContenaitDesErreurs = formulaireContenaitDesErreurs;
  }

  /**
   * Spécifie que le formulaire contenait des erreurs lors du dernier traitement.
   * @return true si le formulaire contenait des erreurs lors du dernier traitement.
   */
  public boolean isFormulaireContenaitDesErreurs() {
    return formulaireContenaitDesErreurs;
  }

  /**
   * Fixer le nom du formulaire ne cours.
   * @param mapping l'instance de mapping en traitement.
   * @param request la requête présentement en traitement.
   */
  public void setNomFormulaire(HttpServletRequest request,
      ActionMapping mapping) {

    if (mapping != null && mapping.getName() != null) {
      request.getSession().setAttribute(Constantes.FORMULAIRE,
          mapping.getName());
    }
  }

  /**
   * Fixer à true si vous ne désirez pas mettre les attributs obligatoire en
   * cache.
   * <p>
   * À utiliser si le formulaire est très dynamique sur des champs obligatoire.
   * @param attributsObligatoireEnCache
   */
  public void setAttributsObligatoireEnCache(boolean attributsEnValidationEnCache) {
    this.attributsEnValidationEnCache = attributsEnValidationEnCache;
  }

  /**
   * Fixer à true si vous ne désirez pas mettre les attributs en validation en
   * cache.
   * <p>
   * À utiliser si le formulaire est très dynamique sur des champs en validation.
   * @param attributsObligatoireEnCache
   */
  public void setValidationEnCache(boolean attributsEnValidationEnCache) {
    this.attributsEnValidationEnCache = attributsEnValidationEnCache;
  }

  /**
   * Est-ce que les attributs obligatoire du formulaire sont mise en cache.
   * @return true si les attributs obligatoire du formulaire sont mise en cache.
   */
  public boolean isAttributsObligatoireEnCache() {
    return attributsEnValidationEnCache;
  }

  /**
   * Est-ce que les attributs en validation du formulaire sont mise en cache.
   * @return true si les attributs en validation du formulaire sont mise en cache.
   */
  public boolean isAttributsValidationEnCache() {
    return attributsEnValidationEnCache;
  }

  /**
   * Retourne le modèle de l'application.
   * <p>
   * @param request la requête présentement en cours.
   * @return le modèle de l'application
   */
  public Object getModele(HttpServletRequest request) {
    return request.getAttribute(Constantes.MODELE);
  }

  /**
   * Retourne un message d'erreur pour un attribut d'une liste imbriqués.
   * @param session la session de l'utilisateur
   * @param nomListe le nom de liste imbriqué
   * @param nomAttribut le nom de l'attribut
   * @return le message d'erreur.
   */
  public MessageErreur getMessageErreur(HttpSession session, String nomListe,
      String nomAttribut) {
    Iterator iterateur = getLesErreurs().keySet().iterator();

    while (iterateur.hasNext()) {
      String cle = (String)iterateur.next();
      MessageErreur messageErreur = (MessageErreur)getLesErreurs().get(cle);

      if ((messageErreur.getFocus().indexOf(nomAttribut) != -1) &&
          (messageErreur.getNomListe() != null) &&
          (messageErreur.getNomListe().indexOf(nomListe) != -1)) {
        return messageErreur;
      }
    }

    return null;
  }

  /**
   * Retourne un formulaire par son nom.
   * @param nomFormulaire le nom du formulaire.
   * @param request la requête en traitement.
   * @return le formulaire demandé.
   */
  public static BaseForm getFormulaire(String nomFormulaire,
      HttpServletRequest request) {
    if (nomFormulaire == null) {
      nomFormulaire =
          (String)request.getSession().getAttribute(Constantes.FORMULAIRE);
    }

    BaseForm formulaireEnCours =
        (BaseForm)request.getSession().getAttribute(nomFormulaire);

    if (formulaireEnCours == null) {
      formulaireEnCours = (BaseForm)request.getAttribute(nomFormulaire);
    }

    return formulaireEnCours;
  }

  /**
   * Retourne le formulaire principal.
   * @param request la requête en traitement.
   * @return le formulaire demandé.
   */
  public static BaseForm getFormulaire(HttpServletRequest request) {
    return getFormulaire(null, request);
  }

  /**
   * Retourne le formulaire principal.
   * @param session la session de l'utilisateur.
   * @return le formulaire demandé.
   */
  public static BaseForm getFormulaire(HttpSession session) {
    String nomFormulaire = (String)session.getAttribute(Constantes.FORMULAIRE);

    if (nomFormulaire == null) {
      return null;
    }

    return (BaseForm)session.getAttribute(nomFormulaire);
  }

  /**
   * Est-ce que la liste de formulaire demandé est à traiter?
   * @return true si la liste de formulaire demandé est à traiter?
   * @param nomListe le nom de la liste à traiter.
   */
  public boolean isListeFormulaireImbriqueATraiter(HttpServletRequest request,
      String nomListe) {
    BaseForm formulaireParent = getFormulaire(request.getSession());

    if (formulaireParent.getListeFormulaireImbriqueATraiter() != null) {
      return formulaireParent.getListeFormulaireImbriqueATraiter().containsKey(nomListe);
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le formulaire contient une ou des liste(s) de formulaire imbriqué.
   * @return true si le formulaire contient une ou des liste(s) de formulaire imbriqué.
   */
  public boolean isListeFormulaireImbrique() {
    if ((this.listeFormulaireImbriqueATraiter != null) &&
        (this.listeFormulaireImbriqueATraiter.size() > 0)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Ajouter un nom de liste de formulaire imbriqués
   * à traiter.
   * @param nomListe le nom de la liste de formulaire imbriqué à traiter.
   */
  public void ajouterListeFormulaireImbriqueATraiter(String nomListe) {
    this.getListeFormulaireImbriqueATraiter().put(nomListe, null);
  }

  /**
   * Retourne la liste des formulaire imbriqué a traiter.
   * @return la liste des formulaire imbriqué a traiter.
   */
  public HashMap getListeFormulaireImbriqueATraiter() {
    if (listeFormulaireImbriqueATraiter == null) {
      listeFormulaireImbriqueATraiter = new LinkedHashMap();
    }

    return this.listeFormulaireImbriqueATraiter;
  }

  /**
   * Fixer un sommaire de validation pour formulaire imbriqué.
   * @param sommaireValidationForm le sommaire de validatoin pour un formulaire imbriqué
   * @param nomListe le nom de la liste imbriqué a traiter.
   */
  public void setSommaireValidationPourFormulaireImbrique(String nomListe,
      SommaireValidationForm sommaireValidationForm) {
    if (sommaireValidationForm != null) {
      this.listeFormulaireImbriqueATraiter.put(nomListe,
          sommaireValidationForm);
    }
  }

  /**
   * Retourne le sommaire de validation pour un formulaire imbriqué.
   * @return Le sommaire de validation pour un formulaire imbriqué.
   * @param nomListe le nom de la liste de formulaire imbriqué.
   */
  public static SommaireValidationForm getSommaireValidationPourFormulaireImbrique(String nomListe,
      HttpSession session) {
    BaseForm formulaireParent = getFormulaire(session);

    SommaireValidationForm sommaire =
        (SommaireValidationForm)formulaireParent.listeFormulaireImbriqueATraiter.get(nomListe);

    if (sommaire == null) {
      sommaire = new SommaireValidationForm();
    }

    return sommaire;
  }

  /**
   * Fixer l'objet de transfert orignal utilisé.
   * @param objetTransfertOrignal l'objet de transfert orignal utilisé.
   */
  protected void setObjetTransfertOriginal(ObjetTransfert objetTransfertOriginal) {
    this.objetTransfertOriginal = objetTransfertOriginal;
  }

  /**
   * Retourne l'objet de transfert orignal utilisé lorsque le mode de traitement des champs
   * modifié seulement est actif et le formulaire est en erreur
   * @return l'objet de transfert orignal
   */
  public ObjetTransfert getObjetTransfertOriginal() {
    return objetTransfertOriginal;
  }

  /**
   * Fixer la liste des attributs avec un format spécifique.
   * @param listeAttributAvecFormat la liste des attributs avec un format spécifique.
   */
  public void setListeAttributAvecFormat(HashMap listeAttributAvecFormat) {
    this.listeAttributAvecFormat = listeAttributAvecFormat;
  }

  /**
   * Retourne la liste des attributs avec un format spécifique.
   * @return la liste des attributs avec un format spécifique.
   */
  public HashMap getListeAttributAvecFormat() {
    return listeAttributAvecFormat;
  }

  /**
   * Méthode qui doit être ré-écrite par une spécialisation de cette classe
   * pour implémenter la journalisation des messages erreurs.
   * @param message
   */
  public void ecrireJournalMesageErreur(MessageErreur message) {
  }

  /**
   * Méthode qui doit être ré-écrite par une spécialisation de cette classe
   * pour implémenter la journalisation des messages informatifs.
   * @param message
   */
  public void ecrireJournalMessageInformatif(MessageInformatif message) {
  }

  /**
   * Méthode qui doit être ré-écrite par une spécialisation de cette classe
   * pour implémenter la journalisation des messages d'avertissements.
   * @param message
   */
  public void ecrireJournalMessageAvertissement(MessageAvertissement message) {
  }

  /**
   * Est-ce une formulaire transactionnel?
   * @return true si le formulaire est transactionnel.
   */
  public boolean isTransactionnel() {
    return transactionnel;
  }

  /**
   * Fixer true si le formulaire est transactionnel
   * @param transactionnel true si le formulaire est transactionnel.
   */
  public void setTransactionnel(boolean transactionnel) {
    this.transactionnel = transactionnel;
  }

  /**
   * Est-ce que le formulaire en traitement est imbriqué?
   * @return true si le formulaire en traitement est imbriqué.
   */
  public boolean isFormulaireImbrique() {
    if (getIndiceNested() != -1) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que l'on doit exclure les validations des champs obligatoires,
   * de cohérence automatisé.
   * @return true si l'on doit exclure les validations des champs obligatoires,
   * de cohérence automatisé.
   */
  public boolean isExclureValidation(HttpServletRequest request) {
    BaseForm formulaire = RequestProcessorHelper.getFormulaireCourant(request);

    String methodeExclusValidation = formulaire.getMethodeIgnoreValidation();

    // YT 2005-08-08 Ajouté la validation (formulaire.getMethode() != null)
    if ((methodeExclusValidation != null) &&
        !methodeExclusValidation.equals("") &&
        (formulaire.getMethode() != null) &&
        (formulaire.getMethode().indexOf(methodeExclusValidation) != -1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le formulaire traite seulement les champs qui sont affichés dans la couche
   * de présentation? Défaut à false.
   * @return false par défaut.
   */
  public boolean isTraiterAttributsAfficheSeulement() {
    if (GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.TRAITER_ATTRIBUTS_AFFICHE_SEULEMENT).booleanValue()) {
      return true;
    } else {
      return traiterAttributsAfficheSeulement;
    }
  }

  /**
   * Fixer si vous désirez que le formulaire traite seulement les champs qui sont affichés dans la couche
   * de présentation? Défaut à false.
   * @param traiterAttributsAfficheSeulement true si vous désirez traiter seulement les champs qui sont affichés dans la couche
   * de présentation
   */
  public void setTraiterAttributsAfficheSeulement(boolean traiterAttributsAfficheSeulement) {
    this.traiterAttributsAfficheSeulement = traiterAttributsAfficheSeulement;
  }

  /**
   * Ajouter un attribut qui doit être traité par le formulaire.
   * @param nomAttribut le nom d'attribut qui doit être traité par le formulaire.
   */
  public void ajouterAttributATraiter(String nomAttribut) {
    if (listeAttributATraiter == null) {
      listeAttributATraiter = new LinkedHashMap();
      listeAttributATraiter.put("modifie", Boolean.TRUE);
    }

    if ((nomAttribut != null) && !"null".equals(nomAttribut)) {
      listeAttributATraiter.put(nomAttribut, Boolean.TRUE);
    }
  }

  /**
   * Retour la liste des attributs à traiter par le formulaire.
   * @return la liste des attributs à traiter par le formulaire.
   */
  public LinkedHashMap getListeAttributATraiter() {
    return listeAttributATraiter;
  }

  /**
   * Fixer la liste des attributs à traiter par le formulaire.
   * @param listeAttributATraiter la liste des attributs à traiter par le formulaire.
   */
  public void setListeAttributATraiter(LinkedHashMap listeAttributATraiter) {
    this.listeAttributATraiter = listeAttributATraiter;

    if (listeAttributATraiter == null) {
      this.listeAttributATraiterParFormulaire = null;
    }
  }

  /**
   * Initialiser les attribut à traiter lorsque le mode utilisé est
   * de traiter les attributs affichés seulement.
   */
  public void initialiserListeAttributATraiter() {
    if (isTraiterAttributsAfficheSeulement()) {
      setListeAttributATraiter(null);
    }
  }

  public void setListeAttributATraiterParFormulaire(LinkedHashMap listeAttributATraiterParFormulaire) {
    this.listeAttributATraiterParFormulaire =
        listeAttributATraiterParFormulaire;
  }

  public LinkedHashMap getListeAttributATraiterParFormulaire() {
    if (listeAttributATraiterParFormulaire == null) {
      listeAttributATraiterParFormulaire = new LinkedHashMap();
    }

    return listeAttributATraiterParFormulaire;
  }

  public LinkedHashMap getListeAttributATraiterParFormulaireImbrique(String nomFormulaire) {
    LinkedHashMap listeAttributsParFormulaire =
        getListeAttributATraiterParFormulaire();
    LinkedHashMap listeAttributs =
        (LinkedHashMap)listeAttributsParFormulaire.get(nomFormulaire.trim());

    if (listeAttributs == null) {
      // Générer les attributs à traiter pour ce formulaire si pas déjà généré.
      genererListeAttributATraiterParFormulaireImbrique(nomFormulaire);
      listeAttributs =
          (LinkedHashMap)listeAttributsParFormulaire.get(nomFormulaire);
    }

    return listeAttributs;
  }

  public void genererListeAttributATraiterParFormulaireImbrique(String nomFormulaire) {
    String indFormulaire = nomFormulaire + ".";
    LinkedHashMap listePourFormulaireImbrique = new LinkedHashMap();
    listePourFormulaireImbrique.put("modifie", Boolean.TRUE);

    Iterator iterateur = getListeAttributATraiter().keySet().iterator();

    while (iterateur.hasNext()) {
      String cle = (String)iterateur.next();

      if (cle.indexOf(indFormulaire) != -1) {
        String attribut =
            cle.substring(cle.indexOf(indFormulaire) + nomFormulaire.length() +
                1);

        // Lorsque l'on est en présence d'une sous-liste, on ne doit pas aller chercher
        // le dernier attribut
        if (attribut.indexOf("[") == -1) {
          while (attribut.indexOf(".") != -1) {
            attribut = attribut.substring(0, attribut.indexOf("."));
          }
        }

        if (cle.indexOf(indFormulaire) != -1) {
          listePourFormulaireImbrique.put(attribut, Boolean.TRUE);
        }
      }
    }

    getListeAttributATraiterParFormulaire().put(nomFormulaire.trim(),
        listePourFormulaireImbrique);
  }

  /**
   * Retourne le locale utilisateur.
   * @return le locale utilisateur.
   * @param request la requête utilisateur.
   */
  public Locale getLocale(HttpServletRequest request) {
    return UtilitaireSession.getLocale(request.getSession());
  }

  /**
   * Retourne le fuseau horaire de l'utilisateur.
   * @return Fuseau horaire
   * @param request Requête HTTP
   */
  public TimeZone getTimeZone(HttpServletRequest request) {
    return UtilitaireSession.getTimeZone(request.getSession());
  }

  /**
   * Initialiser à vide tous les messages contenu dans le formulaire.
   */
  public void initialiserLesMessages() {
    if (isFormulaireEnErreurOuConfirmation()) {
      setFormulaireContenaitDesErreurs(true);
    } else {
      setFormulaireContenaitDesErreurs(false);
    }

    this.lesErreurs = new LinkedHashMap();
    this.lesAvertissements = new LinkedHashMap();
    this.lesErreursGenerales = new LinkedHashMap();
    this.lesInformations = new LinkedHashMap();
    this.lesConfirmations = new LinkedHashMap();
  }

  /**
   * Initialiser à vide tous les messages généraux qui sont
   * affiché dans la barre de statut.
   */
  public void initialiserLesMessagesGeneraux() {
    if (isFormulaireEnErreurOuConfirmation()) {
      setFormulaireContenaitDesErreurs(true);
    } else {
      setFormulaireContenaitDesErreurs(false);
    }

    this.lesAvertissements = new LinkedHashMap();
    this.lesErreursGenerales = new LinkedHashMap();
    this.lesInformations = new LinkedHashMap();
    this.lesConfirmations = new LinkedHashMap();
  }

  /**
   * Retourne le nom de la méthode que le formulaire va ignorer les erreurs
   * de validations, seul les erreurs de format seront traité pour cette méthode.
   * @return
   */
  public String getMethodeIgnoreValidation() {
    return this.methodeIgnoreValidation;
  }

  /**
   * Fixer le nom de la méthode que le formulaire va ignorer les erreurs
   * de validations, seul les erreurs de format seront traité pour cette méthode.
   * @param methodeIgnoreValidation le nom la méthode qui ignore les validations.
   */
  public void setMethodeIgnoreValidation(String methodeIgnoreValidation) {
    this.methodeIgnoreValidation = methodeIgnoreValidation;
  }

  /**
   * Est-ce que la clé de l'objet de transfert associé au formulaire est modifiee?
   * @return true si la clé de l'objet de transfert associé au formulaire est modifiee.
   */
  public boolean isCleObjetTransfertModifie() {
    if (isNouveauFormulaire()) {
      return true;
    } else {
      if (getObjetTransfertOriginal() != null) {
        Identifiant idAncien =
            new Identifiant((ObjetTransfert)this.getObjetTransfert());
        Identifiant idCourant =
            new Identifiant(this.getObjetTransfertOriginal());

        return !idAncien.equals(idCourant);
      } else {
        return false;
      }
    }
  }

  private void ajouterErreurAuFormulaireParent(HttpServletRequest request,
      MessageErreur erreur) {
    BaseForm formulaireParent =
        RequestProcessorHelper.getFormulaireCourant(request);

    if (formulaireParent.getLesErreurs().get(erreur.getFocus()) != null) {
      formulaireParent.getLesErreurs().remove(erreur.getFocus());
    }

    // Ajouter l'erreur dans le formulaire parent.
    formulaireParent.getLesErreurs().put(erreur.getFocus(), erreur);
  }

  /**
   * Est-ce que le nom attribut est à valider?
   * return true si le nom d'attribut est à valider.
   */
  public boolean isAttributAValider(String nomAttribut) {
    if (isAttributATraiter(nomAttribut) && !isAttributEnErreur(nomAttribut)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le nom attribut est en erreur ?
   * return true si le nom d'attribut est en erreur.
   */
  public boolean isAttributEnErreur(String nomAttribut) {
    if (getLesErreurs() != null) {
      return getLesErreurs().containsKey(nomAttribut);
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le nom attribut est à traiter, car il est affiché dans la page?
   * return true si le nom d'attribut est à traiter car il est affiché dans la page.
   */
  public boolean isAttributATraiter(String nomAttribut) {
    BaseForm formulaire = getFormulaireParent();

    if (formulaire.getListeAttributATraiter() != null) {
      if (getNomListe() != null) {
        return formulaire.getListeAttributATraiter().containsKey(getNomListe() +
            "." +
            nomAttribut);
      } else {
        return formulaire.getListeAttributATraiter().containsKey(nomAttribut);
      }
    } else {
      return false;
    }
  }

  /**
   * Retourne le formulaire parent du formulaire imbrique.
   * @return le formulaire parent.
   */
  public BaseForm getFormulaireParent() {
    if (formulaireParent == null) {
      return this;
    } else {
      return formulaireParent;
    }
  }

  /**
   * Fixer le formulaire parent
   * @param formulaireParent le formulaire parent
   */
  public void setFormulaireParent(BaseForm formulaireParent) {
    this.formulaireParent = formulaireParent;
  }

  /**
   * Retourne le formulaire directement parent du formulaire imbrique.
   * @return le formulaire directement parent.
   */
  public BaseForm getFormulaireDirectementParent() {
    return formulaireDirectementParent;
  }

  /**
   * Fixer le formulaire directement parent
   * @param formulaireDirectementParent le formulaire directement parent
   */
  public void setFormulaireDirectementParent(BaseForm formulaireDirectementParent) {
    this.formulaireDirectementParent = formulaireDirectementParent;
  }

  /**
   * Est-ce que la chaine de caratères est vide.
   * @return true si la chaine de caratères est vide.
   * @param chaine la chaine de caractères.
   */
  protected boolean isVide(String chaine) {
    return (UtilitaireString.isVide(chaine));
  }

  /**
   * Déterminer si le formulaire est vide.
   * @return vrai il est vide faux il ne l'est pas.
   * @since SOFI 2.0.1
   */
  public boolean isVide() {
    return UtilitaireObjet.isVide(this, BaseForm.class);
  }

  /**
   * Est-ce que l'attribut spécifié à été modifié dans le formulaire.
   * @return true si l'attribut spécifié a été modifié dans le formulaire.
   * @param nomAttribut a valider s'il a été modifié.
   */
  public boolean isAttributeModifie(String nomAttribut) {
    return UtilitaireBaseForm.isModifie(this, nomAttribut);
  }

  /**
   * Est-ce que l'un des attributs par la liste spécifié à été modifié dans le formulaire.
   * @return true si l'un des attributs par la liste spécifié à été modifié dans le formulaire.
   * @param nomAttributs la liste des attribut a vérifié si l'un a été modifié.
   */
  public boolean isAttributsModifie(String[] nomAttributs) {
    for (int i = 0, n = nomAttributs.length; i < n; i++) {
      if (isAttributeModifie(nomAttributs[i])) {
        return true;
      }
    }

    return false;
  }

  /**
   * Est-ce que formulaire doit traiter seulement les attributs qui ont été modifié.
   * @return true si formulaire doit traiter seulement les attributs qui ont été modifié.
   */
  public boolean isTraiterSeulementAttributModifie() {
    return traiterSeulementAttributModifie;
  }

  /**
   * Fixer true si vous désirez que le formulaire spécifie les attributs modifiés.
   * @param traiterSeulementAttributModifie le formulaire true si vous désirez que le formulaire doit traiter seulement les attributs modifiés.
   */
  public void setTraiterSeulementAttributModifie(boolean traiterSeulementAttributModifie) {
    this.traiterSeulementAttributModifie = traiterSeulementAttributModifie;
  }

  /**
   * Retourne la liste des validations du formulaire.
   * @return la liste des validations du formulaire.
   */
  public Map getValidationMap() {
    return validationMap;
  }

  /**
   * Retourne la liste des valeurs par défaut du formulaire.
   * @return la liste des valeurs par défaut du formulaire.
   */
  public Map getValeurParDefaut() {
    return valeurParDefautMap;
  }

  /**
   * Retourne le nom du formulaire
   * @return le nom de formulaire complet.
   * @param nomAttribut l'attribut a traiter.
   */
  public String getNomFormulaireComplet(String nomAttribut) {
    return UtilitaireBaseForm.getNomFormulaireComplet(getNomListe(),
        nomAttribut);
  }

  /**
   * Est-ce que les validations obligatoires doivent être traité.
   * @return true si les validations obligatoires doivent être traité.
   * @param request la requête en traitement.
   */
  public boolean isValiderObligatoires(HttpServletRequest request) {
    boolean ignorer1 =
        "true".equalsIgnoreCase(request.getParameter("ignorerValidationObligatoire"));
    boolean ignorer2 =
        "true".equalsIgnoreCase(request.getParameter("sofi.ignore.obligatoire"));

    return !(ignorer1 || ignorer2);
  }

  /**
   * Est-ce que les validations et la population de l'objet de transfert doit
   * être ignoré.
   * @return true si les validations et la population de l'objet de transfert doit
   * être ignoré.
   * @param request la requête en traitement.
   */
  protected boolean isIgnorerValidation(HttpServletRequest request) {
    return Boolean.valueOf(request.getParameter("ignorerValidation")).booleanValue() ||
        Boolean.valueOf(request.getParameter("ajax")).booleanValue();
  }

  /**
   * Fixe le formulaire ainsi que son objet de transfert comme étant modifé.
   * L'appel est récursif sur les formulaires parents.
   */
  public void indiquerFormulaireModifie() {
    setIndModification(BaseForm.IND_MODIFIE);

    if (objetTransfert != null) {
      ((ObjetTransfert)objetTransfert).setModifie(true);
    }

    // TODO: Obtenir le parent direct plutot que le
    //       formulaire principale lorsque disponible
    BaseForm parent = getFormulaireDirectementParent();

    if ((parent != null) && (parent != this)) {
      parent.indiquerFormulaireModifie();
    }
  }

  /**
   * Retourne le detail d'un groupe de boite à cocher multiple.
   * @param attribut l'attribut du groupe de boite à cocher multiple
   * @param request la requête en traitement
   */
  public InfoMultibox getDetailBoiteACocherMultiple(String attribut,
      HttpServletRequest request) {
    return UtilitaireControleur.getDetailBoiteACocherMultiple(attribut,
        request);
  }

  /**
   * Copier les correspondances d'un formulaire enfant dans le formulaire parent.
   * @param request la requete en cours.
   * @param nomAttribut le formulaire a traiter.
   */
  private void copierCorrespondanceEnfantsDansFormulaireParent(String nomAttribut,
      HttpServletRequest request) {
    BaseForm formulaireParent = getFormulaire(null, request);

    while ((nomAttribut.indexOf(".") != -1) ||
        !this.isCorrespondanceFormulaireEnfantTraite()) {
      BaseForm formulaireCourant = null;

      if (nomAttribut.indexOf(".") != -1) {
        formulaireCourant =
            (BaseForm)getValeurAttribut(this, nomAttribut.substring(0,
                nomAttribut.indexOf(".")),
                false);
      } else {
        formulaireCourant = this;
      }

      if (!formulaireCourant.isCorrespondanceFormulaireEnfantTraite()) {
        Iterator iterateurTypeFormulaire =
            formulaireCourant.typeFormulaireEnfants.keySet().iterator();

        while (iterateurTypeFormulaire.hasNext()) {
          Class objetTransfert = (Class)iterateurTypeFormulaire.next();
          Class formulaire =
              (Class)formulaireCourant.typeFormulaireEnfants.get(objetTransfert);

          formulaireParent.typeFormulaireEnfants.put(objetTransfert,
              formulaire);
        }

        formulaireCourant.setCorrespondanceFormulaireEnfantTraite(true);

        if (nomAttribut.indexOf(".") != -1) {
          nomAttribut = nomAttribut.substring(nomAttribut.indexOf("."));
        }
      } else {
        return;
      }
    }
  }

  protected boolean isCorrespondanceFormulaireEnfantTraite() {
    return correspondanceFormulaireEnfantTraite;
  }

  protected void setCorrespondanceFormulaireEnfantTraite(boolean correspondanceFormulaireEnfantTraite) {
    this.correspondanceFormulaireEnfantTraite =
        correspondanceFormulaireEnfantTraite;
  }

  public HashSet getListeAttributTraite() {
    if (listeAttributTraite == null) {
      return new HashSet();
    }

    return listeAttributTraite;
  }

  public void setListeAttributTraite(HashSet listeAttributTraite) {
    this.listeAttributTraite = listeAttributTraite;
  }

  protected boolean isInstancierObjetsTransfertImbrique() {
    return instancierObjetsTransfertImbrique;
  }

  protected void setInstancierObjetsTransfertImbrique(boolean instancierObjetsTransfertImbrique) {
    this.instancierObjetsTransfertImbrique = instancierObjetsTransfertImbrique;
  }

  /**
   * Spécifie si le formulaire n'est pas associé à un objet de transfert.
   * @return true si le formulaire n'est pas associé à un objet de transfert.
   */
  protected boolean isFormulaireSansObjetTransfert() {
    return formulaireSansObjetTransfert;
  }

  /**
   * Fixer true si le formulaire n'est pas associé à un objet de transfert.
   * @param formulaireSansObjetTransfert est-ce que formulaire n'est pas associé à un objet de transfert.
   */
  protected void setFormulaireSansObjetTransfert(boolean formulaireSansObjetTransfert) {
    this.formulaireSansObjetTransfert = formulaireSansObjetTransfert;
  }

  /**
   * Identifie que l'on popule le formulaire avec la liste des attributs du formulaire.
   * Ceci limite le nombre d'itérations dans le cas ou les objets de transferts
   * possèdent une grande quantité d'attributs. Peut augmenter considérablement
   * les performances dans cette situation. Cette propriété doit être a false
   * lors de l'utilisation du BaseDynaForm. Le formulaire dynamique a besoin
   * des propriétés de l'objet de transfert pour créer la représentation interne
   * du formulaire dynamique.
   */
  public boolean isPopulerAvecAttributsFormulaire() {
    return populerAvecAttributsFormulaire;
  }

  /**
   * Permet de populer le formulaire avec la liste des attributs du formulaire.
   * Ceci limite le nombre d'itérations dans le cas ou les objets de transferts
   * possèdent une grande quantité d'attributs. Peut augmenter considérablement
   * les performances dans cette situation. Cette propriété doit être a false
   * lors de l'utilisation du BaseDynaForm. Le formulaire dynamique a besoin
   * des propriétés de l'objet de transfert pour créer la représentation interne
   * du formulaire dynamique.
   */
  public void setPopulerAvecAttributsFormulaire(boolean populerAvecAttributsFormulaire) {
    this.populerAvecAttributsFormulaire = populerAvecAttributsFormulaire;
  }

  /**
   * Retourne la clé du message d'erreur général à utiliser pour le formulaire.
   * @return la clé du message d'erreur général à utiliser pour le formulaire.
   */
  public String getCleMessageErreurGeneral() {
    return cleMessageErreurGeneral;
  }

  /**
   * Fixer la clé du message d'erreur général à utiliser pour ce formulaire.
   * @param cleMessageErreurGeneral la clé du message d'erreur général à utiliser pour le formulaire.
   */
  public void setCleMessageErreurGeneral(String cleMessageErreurGeneral) {
    this.cleMessageErreurGeneral = cleMessageErreurGeneral;
  }
}
