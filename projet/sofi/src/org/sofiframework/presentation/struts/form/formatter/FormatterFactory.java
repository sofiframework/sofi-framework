/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.struts.upload.FormFile;
import org.sofiframework.objetstransfert.DateTZ;


/**
 * Produit le formatter qui est nécessaire au formatage d'un type de classe.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class FormatterFactory {
  /** Constructeur par défault */
  public FormatterFactory() {
  }

  /**
   * Produit un certain type de formatter de données correspondant au type de classe fournit.
   * @param type une classe d'objet. Peut être un type de base (Long, Integer, etc) ou un type de formatter.
   * @return un formatter
   */
  public static Formatter get(Class type) {
    Formatter formatter = null;

    // Traiter les Float.
    if (Float.class.isAssignableFrom(type) ||
        Double.class.isAssignableFrom(type) ||
        BigDecimal.class.isAssignableFrom(type)) {
      FormatterDecimal formatterDecimal = new FormatterDecimal();
      formatterDecimal.setTypeClasse(type);
      formatter = formatterDecimal;
    }
    // Traiter Date-Heure-Seconde
    else if (FormatterDateHeureSeconde.class.isAssignableFrom(type)) {
      formatter = new FormatterDateHeureSeconde();
    } else if (DateTZ.class.isAssignableFrom(type)) {
      formatter = new FormatterDateHeureTZ();
    }
    // Traiter java.util.Date
    else if (Date.class.isAssignableFrom(type)) {
      formatter = new FormatterDate();
    }
    // Traiter les Integer
    else if (Integer.class.isAssignableFrom(type)) {
      formatter = new FormatterInteger();
    }
    // Traiter les Long
    else if (Long.class.isAssignableFrom(type)) {
      formatter = new FormatterLong();
    }
    // Traiter les Long[]
    else if (Long[].class.isAssignableFrom(type)) {
      formatter = new FormatterLongArray();
    }
    // Traiter les Integer[]
    else if (Integer[].class.isAssignableFrom(type)) {
      formatter = new FormatterIntegerArray();
    }
    // Traiter les String[]
    else if (String[].class.isAssignableFrom(type)) {
      formatter = new FormatterStringArray();
    }
    // Traiter les FormFile
    else if ((type.isArray() &&
        type.getComponentType().isAssignableFrom(byte.class)) ||
        type.isAssignableFrom(FormFile.class)) {
      formatter = new FormatterFile();
    }
    // Traiter les Boolean
    else if (Boolean.class.isAssignableFrom(type)) {
      formatter = new FormatterBoolean();
    } else {
      // Traiter les autres types de formats, si une exception survient lors
      // de la construction du formatter, un type "Formatter" est construit.
      try {
        formatter = (Formatter) type.newInstance();
      } catch (Exception ex) {
        formatter = new Formatter();
      }
    }

    return formatter;
  }
}
