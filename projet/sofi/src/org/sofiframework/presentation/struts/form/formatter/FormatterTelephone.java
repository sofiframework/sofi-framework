/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Un formatter pour ajouter un masque sur les numéros de téléphone.
 * Format du masque: (999)999-9999.
 * <p>
 * @author Guillaume Poirier (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterTelephone extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type Telephone */
  public final static String TELEPHONE_ERROR_KEY = "erreur.format.telephone";
  private static Pattern patternBd = Pattern.compile(
      "^(\\d{3})(\\d{3})(\\d{4})$"); // 4186272001
  private static Pattern[] patterns = {
    patternBd, Pattern.compile("^\\((\\d{3})\\) ?(\\d{3})-(\\d{4})$"), // (418) 627-2001
    Pattern.compile("^(\\d{3})-(\\d{3})-(\\d{4})$") // 418-627-2001
  };

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return TELEPHONE_ERROR_KEY;
  }

  /**
   * Convertit un String en une String représentant un numéro de téléphone.
   * <p>
   * @param string la String à convertir en numéro de téléphone
   * @return une String ayant le format d'un numéro de téléphone
   */
  @Override
  public Object unformat(String tel, Locale locale, TimeZone fuseauHoraireClient) {
    if ((tel == null) || ((tel = tel.trim()).length() == 0)) {
      return null;
    }

    for (int i = 0, n = patterns.length; i < n; i++) {
      Matcher m = patterns[i].matcher(tel);

      if (m.matches()) {
        String formatTelephone = GestionParametreSysteme.getInstance()
            .getString(ConstantesParametreSysteme.FORMAT_BD_TELEPHONE);

        if (UtilitaireString.isVide(formatTelephone)) {
          formatTelephone = "$1$2$3";
        }

        return m.replaceFirst(formatTelephone);
      }
    }

    throw new FormatterException(
        "Incapable de traiter le numero de telephone : " + tel);
  }

  public static String formatTelephone(String tel) {
    if ((tel == null) || ((tel = tel.trim()).length() == 0)) {
      return "";
    }

    Matcher m = patternBd.matcher(tel.toString());

    String formatTelephone = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FORMAT_AFFICHAGE_TELEPHONE);

    if (UtilitaireString.isVide(formatTelephone)) {
      formatTelephone = "($1)$2-$3";
    }

    if (m.matches()) {
      return m.replaceFirst(formatTelephone);
    } else {
      return tel;
    }
  }

  /**
   * Convertit un String en une String représentant un numéro de téléphone.
   * <p>
   * @param telephoneNonFormatte la String à convertir en numéro de téléphone
   * @return une String ayant le format d'un numéro de téléphone
   */
  @Override
  public String format(Object obj, Locale locale, TimeZone fuseauHoraireClient) {
    return (obj == null) ? "" : formatTelephone(obj.toString());
  }
}
