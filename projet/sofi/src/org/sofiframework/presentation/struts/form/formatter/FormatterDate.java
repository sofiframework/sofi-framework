/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Un formatter des valeurs correspondant a des dates de type
 * java.util.Date.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterDate extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type Date */
  public final static String DATE_ERROR_KEY = "erreur.format.date";

  /** Constructeur par défaut */
  public FormatterDate() {
  }

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return DATE_ERROR_KEY;
  }

  @Override
  public Object unformat(String target) {
    return unformat(target, null, null);
  }

  public Object unformat(String target, Locale locale) {
    return unformat(target, locale, null);
  }

  /**
   * Convertit un String en une instance du type java.util.Date.
   * <p>
   * @param target la string à transformer en date
   * @return une date sous forme de java.util.Date
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone timezone) {
    if ((target == null) || (target.trim().length() < 1)) {
      return null;
    }

    Date date = null;

    try {
      // Supprimer les blancs s'il y a lieu
      target = UtilitaireString.remplacerTous(target, " ", "");

      if (target.indexOf("/") != -1) {
        target = UtilitaireString.remplacerTous(target, "/", "-");
      }

      date = UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate(target);
    } catch (Exception e) {
      try {
        date = UtilitaireDate.convertir_AAAAMMJJ_En_UtilDate(target);
      } catch (Exception e2) {
        throw new FormatterException(
            "Incapable de traiter cette valeur de date: " + target);
      }
    }

    return date;
  }

  /**
   * Convertit un instance du type java.util.Date en String du format AAAA-MM-JJ.
   * <p>
   * @param value la date à convertir en String
   * @param format le format a appliquer.
   * @return un date formattée
   */
  @Override
  public String format(Object value, Locale locale, TimeZone timezone) {
    if (value == null) {
      return "";
    }

    return UtilitaireDate.convertirEn_AAAA_MM_JJ((Date) value);
  }

  @Override
  public String format(Object value, Locale locale) {
    return format(value, locale, null);
  }
}
