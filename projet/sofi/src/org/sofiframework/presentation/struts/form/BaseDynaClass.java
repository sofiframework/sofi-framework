/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.io.Serializable;
import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;


/**
 * Objet qui décrit les attributs qui caractérisent une classe dynamique.  Dans
 * le cas présent l'objet BaseDynaClass est populé avec les description  des
 * attributs de l'objet de transfert lors de l'appel de la méthode
 * setObjetTransfert.
 */
public class BaseDynaClass implements DynaClass, Serializable {

  private static final long serialVersionUID = 1294669898743935140L;

  /**
   * Nom de la classe qui a servi a construire les descriptions d'attributs.
   */
  private String nom = null;

  /**
   * Structure qui héberge les description des attributs  de l'objet de
   * tranfert associé au formulaire.
   */
  private HashMap proprietes = new HashMap();

  /**
   * Création d'un object BaseDynaClass.
   */
  public BaseDynaClass() {
  }

  /**
   * Fixer le nom de la classe qui est décrite par la classe dynamique
   *
   * @param nom Nom de classe
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le nom de la classe qui est décrite par la classe dynamique
   *
   * @return Nom de classe
   */
  public String getNom() {
    return nom;
  }

  /**
   * Permet d'ajouter une nouvelle description d'attribut dynamique.
   *
   * @param proprieteDynamiqe Nouvelle propriété dynamique
   */
  public void addDynaProperty(DynaProperty proprieteDynamiqe) {
    this.proprietes.put(proprieteDynamiqe.getName(), proprieteDynamiqe);
  }

  // -------------------- Implantation de l'interface DynaClass --------------------------

  /**
   * Obtenir le nom de la classe dynamique.
   *
   * @return Nom de classe
   */
  @Override
  public String getName() {
    return this.getClass().getName();
  }

  /**
   * Obtenir une description de propriété dynamique de nom spécifique
   *
   * @param nomPropriete Nomde propriété dont on désire obtenir une
   *        description
   *
   * @return Objet de description d'une propriété
   */
  @Override
  public DynaProperty getDynaProperty(String nomPropriete) {
    DynaProperty propriete = (DynaProperty) this.proprietes.get(nomPropriete);

    if (propriete == null) {
      propriete = new DynaProperty(nomPropriete, String.class);
    }

    return propriete;
  }

  /**
   * Obtenir un tableau qui dontient les descriptions de toutes les
   * propriétés dynamiques de la classe.
   *
   * @return liste de propriétés dynamiques
   */
  @Override
  public DynaProperty[] getDynaProperties() {
    return (DynaProperty[]) this.proprietes.values().toArray(new DynaProperty[proprietes.size()]);
  }

  /**
   * Créer une nouvelle instance de l'objet dynamique décrit par la classe.
   * Cette  méthode reste non implantée car elle n'est pas utilisé dans le
   * cas de la  population des formulaires dynamiques.
   *
   * @return Instance de classe qui implante l'interface DynaBean décrite
   *         par la classe dynamique courante
   *
   * @throws InstantiationException Erreur survenu lors d'une instanciation
   * @throws IllegalAccessException Erreur survenu lors de l'accès à  une
   *         propriété ou une méthode
   */
  @Override
  public DynaBean newInstance()
      throws IllegalAccessException, InstantiationException {
    return null;
  }
}
