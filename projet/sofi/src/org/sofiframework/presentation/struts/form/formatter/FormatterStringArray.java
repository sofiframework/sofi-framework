/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;


/**
 * Un formatter des valeurs correspondant à des tableaux de type Long.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.1
 */
public class FormatterStringArray extends Formatter {
  /**
   * Convertit un String en une instance du type String[].
   * <p>
   * @param valeurForm la string à convertir en un String[]
   * @return un tableau de String.
   */
  @Override
  public Object unformat(String valeurForm, Locale locale,
      TimeZone fuseauHoraireClient) {
    if ((valeurForm == null) || (valeurForm.trim().length() < 1)) {
      return null;
    }

    try {
      String[] retour = new String[1];

      retour[0] = valeurForm;

      return retour;
    } catch (Exception e) {
      throw new FormatterException("", e);
    }
  }

  /**
   * Convertit un instance du type String[] en String.
   * <p>
   * @param valeur l'objet Long à transformer en String
   * @param format le format a appliquer.
   * @return un Long sous forme de String
   */
  @Override
  public String format(Object valeur, Locale locale,
      TimeZone fuseauHoraireClient) {
    String[] valeurRetour = (String[]) valeur;

    return (((valeurRetour == null) || (valeurRetour.length == 0)) ? ""
        : valeurRetour[0].toString());
  }
}
