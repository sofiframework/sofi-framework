/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.utilitaire;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.upload.FormFile;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.struts.form.comparateur.Comparateur;
import org.sofiframework.presentation.struts.form.formatter.FormatMessage;
import org.sofiframework.presentation.struts.form.formatter.Formatter;
import org.sofiframework.presentation.struts.form.formatter.FormatterException;
import org.sofiframework.presentation.struts.form.formatter.FormatterExpressionReguliere;
import org.sofiframework.presentation.struts.form.formatter.FormatterFile;
import org.sofiframework.utilitaire.UtilitaireCleMessage;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe d'aide à la classe BaseForm.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.5
 */
public class UtilitaireBaseForm {
  /** Instance commune de journalisation */
  protected static Log log = LogFactory.getLog(UtilitaireBaseForm.class);

  private static Object annulerChaine(Object valeur) {
    if (valeur instanceof String) {
      String chaine = valeur.toString().trim();

      return (chaine.length() == 0) ? null : chaine;
    } else {
      return valeur;
    }
  }

  /**
   * Compare une propriété dans l'objet de tranfert original et dans l'objet
   * de transfert modifié. Le résultat est "true" si l'appel de la méthode
   * equals retourne "true" lors de la comparaison des deux objets de tranfert.
   * La méthode retourne aussi vrai si l'objet de transfert original est null.
   *
   * @return Si l'attribut spécifié est modifié
   * @param nomAttribut Nom de l'attribut dont on veux savoir si l'attribut est
   *        modifié
   * @param formulaire Formulaire
   */
  public static boolean isModifie(BaseForm formulaire, String nomAttribut) {
    boolean modifie = false;
    Object valeur = UtilitaireObjet.getValeurAttribut(formulaire.getObjetTransfert(),
        nomAttribut);
    valeur = annulerChaine(valeur);

    if ((valeur != null) &&
        !valeur.getClass().isAssignableFrom(ArrayList.class) &&
        !valeur.getClass().isAssignableFrom(ObjetTransfert.class)) {
      if (formulaire.getObjetTransfertOriginal() != null) {
        Object valeurOriginal = UtilitaireObjet.getValeurAttribut(formulaire.getObjetTransfertOriginal(),
            nomAttribut);
        valeurOriginal = annulerChaine(valeurOriginal);

        if (valeur != null) {
          modifie = !valeur.equals(valeurOriginal);
        } else {
          modifie = valeurOriginal == null;
        }
      } else {
        modifie = true;
      }
    }

    return modifie;
  }

  /**
   * Fixer les attributs qui ne doivent pas êter initilisés et populés
   * par le formulaire.
   *
   * @return Map des attributs à exclure
   * @param attributsAExclure Attributs personnalisés à exclure. Si le Mape est null,
   * la méthode crée une nouvelle instance de Map.
   */
  public static HashMap fixerAttributAExclure(HashMap attributsAExclure) {
    if (attributsAExclure == null) {
      attributsAExclure = new HashMap();
    }

    attributsAExclure.put("class", Boolean.TRUE);
    attributsAExclure.put("servletWrapper", Boolean.TRUE);
    attributsAExclure.put("multipartRequestHandler", Boolean.TRUE);
    attributsAExclure.put("objetTransfert", Boolean.TRUE);
    attributsAExclure.put("indiceNested", Boolean.TRUE);
    attributsAExclure.put("fichierMessageSuppl", Boolean.TRUE);
    attributsAExclure.put("executerValidation", Boolean.TRUE);
    attributsAExclure.put("methode", Boolean.TRUE);
    attributsAExclure.put("isLigneErreur", Boolean.TRUE);
    attributsAExclure.put("indModification", Boolean.TRUE);
    attributsAExclure.put("formulaireIdentifiant", Boolean.TRUE);
    attributsAExclure.put("lesErreurs", Boolean.TRUE);
    attributsAExclure.put("lesAvertissements", Boolean.TRUE);
    attributsAExclure.put("lesErreursGenerales", Boolean.TRUE);
    attributsAExclure.put("lesInformations", Boolean.TRUE);
    attributsAExclure.put("isFormulaireValidation", Boolean.TRUE);
    attributsAExclure.put("isFormulaireEnErreur", Boolean.TRUE);
    attributsAExclure.put("modeLectureSeulement", Boolean.TRUE);
    attributsAExclure.put("nouveauFormulaire", Boolean.TRUE);
    attributsAExclure.put("populerObjetTransfertTraite", Boolean.TRUE);
    attributsAExclure.put("formulaireContenaitDesErreurs", Boolean.TRUE);
    attributsAExclure.put("formulaireSurPlusieursPages", Boolean.TRUE);
    attributsAExclure.put("messageDansFormulaire", Boolean.TRUE);
    attributsAExclure.put("attributsEnValidationEnCache", Boolean.TRUE);
    attributsAExclure.put("formulaireModifie", Boolean.TRUE);
    attributsAExclure.put("mapping", Boolean.TRUE);
    attributsAExclure.put("formulaireEnErreurOuConfirmation", Boolean.TRUE);
    attributsAExclure.put("formulaireEnErreur", Boolean.TRUE);
    attributsAExclure.put("listeAttributAvecFormat", Boolean.TRUE);
    attributsAExclure.put("attributsValidationEnCache", Boolean.TRUE);
    attributsAExclure.put("attributsObligatoireEnCache", Boolean.TRUE);
    attributsAExclure.put("listeFormulaireImbrique", Boolean.TRUE);
    attributsAExclure.put("listeFormulaireImbriqueATraiter", Boolean.TRUE);
    attributsAExclure.put("listeAttributATraiter", Boolean.TRUE);
    attributsAExclure.put("validerFormulaire", Boolean.TRUE);
    attributsAExclure.put("traiterAttributsAfficheSeulement", Boolean.TRUE);
    attributsAExclure.put("traiterSeulementAttributModifie", Boolean.TRUE);
    attributsAExclure.put("listeAttributATraiterParFormulaire", Boolean.TRUE);
    attributsAExclure.put("methodeIgnoreValidation", Boolean.TRUE);
    attributsAExclure.put("formulaireParent", Boolean.TRUE);
    attributsAExclure.put("objetTransfertOriginal", Boolean.TRUE);
    attributsAExclure.put("modeAffichageSeulement", Boolean.TRUE);
    attributsAExclure.put("valeurParDefautMap", Boolean.TRUE);
    attributsAExclure.put("attributsAvecMessagesErreur", Boolean.TRUE);
    attributsAExclure.put("attributsSansValidations", Boolean.TRUE);
    attributsAExclure.put("typeFormulaireEnfants", Boolean.TRUE);
    attributsAExclure.put("lesConfirmations", Boolean.TRUE);
    attributsAExclure.put("validationMap", Boolean.TRUE);
    attributsAExclure.put("formulaireDirectementParent", Boolean.TRUE);
    attributsAExclure.put("formulaireParent", Boolean.TRUE);
    attributsAExclure.put("populerAvecAttributsFormulaire", Boolean.TRUE);

    return attributsAExclure;
  }

  /**
   * Retourne <code>true</code> si l'objet pour un type donné
   * est considéré comme un objet de type nested, sinon false.
   * <p>
   * @param type Le type de classe à tester
   * @return valeur indiquant si l'objet est imbriqués (Nested)
   */
  public static boolean isNestedObject(Class type) {
    if (type == null) {
      return false;
    }

    if (ObjetTransfert.class.isAssignableFrom(type) ||
        BaseForm.class.isAssignableFrom(type)) {
      return true;
    }

    return false;
  }

  /**
   * Ajouter une regle de validation dans un type Map.
   * <p>
   * @param validationMap une liste de validation du formulaire.
   * @param nomAttribut le nom de l'attribut que la règle s'applique
   * @param regle le nom de la règle
   * @param valeur La valeur ou les valeurs a traite par la règle
   */
  public static void ajouterRegleValidation(Map validationMap,
      String nomAttribut, String regle, Object valeur) {
    Map valeurs = (Map) validationMap.get(nomAttribut);

    if (valeurs == null) {
      valeurs = new HashMap();
      validationMap.put(nomAttribut, valeurs);
    }

    valeurs.put(regle, valeur);
  }

  /**
   * Initialise tous les attributs de type boolean et String[].
   */
  public static void initialiserAttributBooleanEtTableau(BaseForm formulaire) {
    try {
      Iterator listeAttributsAvecValeurIter = null;

      if ((formulaire.getListeAttributATraiter() != null) &&
          (formulaire.getListeAttributATraiter().size() > 0)) {
        listeAttributsAvecValeurIter = formulaire.getListeAttributATraiter()
            .keySet().iterator();
      } else {
        Map valeurs = UtilitaireObjet.decrire(formulaire);
        listeAttributsAvecValeurIter = valeurs.keySet().iterator();
      }

      while (listeAttributsAvecValeurIter.hasNext()) {
        String nomAttribut = (String) listeAttributsAvecValeurIter.next();

        if (!formulaire.isAttributAExclure(nomAttribut)) {
          try {
            // Vérifier le type d'attribut
            Class type = PropertyUtils.getPropertyType(formulaire, nomAttribut);

            if (boolean.class.isAssignableFrom(type)) {
              UtilitaireObjet.setPropriete(formulaire, nomAttribut,
                  Boolean.FALSE);
            }

            if (type.getName().equals("[Ljava.lang.String;")) {
              UtilitaireObjet.setPropriete(formulaire, nomAttribut, null);
            }

            if (java.util.ArrayList.class.isAssignableFrom(type)) {
              java.util.ArrayList liste = (ArrayList) PropertyUtils.getProperty(formulaire,
                  nomAttribut);
              Iterator iterateur = liste.iterator();

              while (iterateur.hasNext()) {
                BaseForm formulaireEnfant = (BaseForm) iterateur.next();
                formulaireEnfant.initialiserAttributBooleanEtTableau();
              }
            }
          } catch (Exception e) {
            // ignorer
          }
        }
      }
    } catch (Exception e) {
      log.warn("", e);
    }
  }

  public static void initialiserTousLesAttributs(BaseForm formulaire,
      boolean memeInstance) {
    try {
      Map valeurs = UtilitaireObjet.decrire(formulaire);

      Iterator listeAttributsAvecValeurIter = valeurs.keySet().iterator();

      while (listeAttributsAvecValeurIter.hasNext()) {
        String nomAttribut = (String) listeAttributsAvecValeurIter.next();

        if (!formulaire.isAttributAExclure(nomAttribut)) {
          // Vérifier le type d'attribut
          Class type = PropertyUtils.getPropertyType(formulaire, nomAttribut);

          try {
            if (type == boolean.class) {
              UtilitaireObjet.setPropriete(formulaire, nomAttribut,
                  Boolean.FALSE);
            } else {
              if (BaseForm.class.isAssignableFrom(type)) {
                BaseForm nouveauFormulaire = (BaseForm) type.newInstance();
                UtilitaireObjet.setPropriete(formulaire, nomAttribut,
                    nouveauFormulaire);

                boolean nouveauMemeInstance = false;
                Class classeFormulaire = formulaire.getClass();

                if (type == classeFormulaire) {
                  nouveauMemeInstance = true;
                }

                if (!memeInstance) {
                  initialiserTousLesAttributs(nouveauFormulaire,
                      nouveauMemeInstance);
                }
              } else {
                // Spécifier valeur défaut s'il y a lieu.
                if (formulaire.getValeurParDefaut().containsKey(nomAttribut)) {
                  String valeurAttribut = (String) formulaire.getValeurParDefaut()
                      .get(nomAttribut);
                  UtilitaireObjet.setPropriete(formulaire, nomAttribut,
                      valeurAttribut);
                } else {
                  UtilitaireObjet.setPropriete(formulaire, nomAttribut, null);
                }
              }
            }
          } catch (Exception e) {
            // ignorer
          }
        }
      }
    } catch (Exception e) {
      log.warn(e.toString(), e);
    }
  }

  /**
   * Permet d'ajouter correctement une erreur aux erreurs deja présentes dans un formulaire.
   * <p>
   * @param cleErreur Clé du message
   * @param attribut l'attribut qui est en erreur
   * @param parametres parametre a ajouter au message
   * @param request Requete HTTP en cour de validation.
   * @return suite d'erreur struts (indique a struts qui il a une erreur).
   */
  static public void ajouterMessageErreur(BaseForm formulaire,
      String cleErreur, String attribut, Object[] parametres,
      HttpServletRequest request) {
    int i = 0;
    boolean trouve = false;

    // Créer le nouveau message
    MessageErreur erreur = new MessageErreur();
    erreur.setCleMessage(cleErreur);

    if (attribut == null) {
      attribut = UtilitaireCleMessage.getAttributEnFocus(cleErreur);

      if (attribut != null) {
        LinkedHashMap attributsATraiter = formulaire.getListeAttributATraiter();

        if ((attributsATraiter != null) && !attributsATraiter.isEmpty()) {
          trouve = attributsATraiter.containsKey(attribut);
        } else {
          Field[] fieldNames = formulaire.getClass().getDeclaredFields();

          for (i = 0; i < fieldNames.length; i++) {
            if (fieldNames[i].getName().compareTo(attribut) == 0) {
              trouve = true;
            }
          }
        }
      }

      //Si l'attribut n'est pas un attribut du formulaire,
      //alors le message d'erreur sera general
      if (!trouve) {
        formulaire.ajouterMessageErreurGeneral(cleErreur, parametres, request);

        // Ajouter l'erreur aux erreurs du formulaire principal.
        BaseForm formulairePrincipal = BaseForm.getFormulaire(request);
        formulairePrincipal.ajouterMessageErreurGeneral(cleErreur, parametres, request);

        return;
      }
    }

    if (formulaire.getNomListe() != null) {
      erreur.setNomListe(formulaire.getNomListe());
      erreur.setNoLigne(new Integer(formulaire.getIndiceNested()));

      StringBuffer attributComplet = new StringBuffer();
      attributComplet.append(formulaire.getNomListe());
      attributComplet.append(".");
      attributComplet.append(attribut);
      erreur.setFocus(attributComplet.toString());
    } else {
      if (attribut.indexOf(".") != -1) {
        erreur.setNomListe(attribut.substring(0, attribut.indexOf(".")));
      }

      erreur.setFocus(attribut);
    }

    // Spécifier le message d'erreur.
    MessageErreur messageErreur = UtilitaireMessage.getMessageErreur(erreur,
        parametres, request);
    erreur.setMessage(messageErreur.getMessage());

    // Ajouter l'erreur aux erreurs du formulaire principal.
    BaseForm formulairePrincipal = BaseForm.getFormulaire(request);
    formulairePrincipal.getLesErreurs().put(erreur.getFocus(), erreur);

    // Journaliser le message d'erreur si demandé.
    formulairePrincipal.ecrireJournalMesageErreur(erreur);

    //Fixer le nom du formulaire
    formulairePrincipal.setNomFormulaire(request, formulaire.getMapping());
  }

  /**
   * Convertir l'objet donné d'un type String en un type Java
   * ou encore vice versa, tout dependant de la valeur du
   * <code>mode</code>.
   * <p>
   * @param type La classe associer a l'attribut
   * @param nomAttribut Le nom de l'attribut
   * @param obj L'objet a convertir
   * @param mode le mode de conversion, en String pour le formulaire
   * ou en type Java pour l'objet de transfert
   * @return l'objet convertit
   */
  public static Object convertir(BaseForm formulaire, Class type,
      String nomAttribut, Object obj, int mode, Locale localeUtilisateur,
      TimeZone fuseauHoraireUtilisateur)
          throws InstantiationException, IllegalAccessException,
          NoSuchMethodException, InvocationTargetException {
    Object convertedObj = null;

    FormatMessage formatMessage = (FormatMessage) formulaire.getListeAttributAvecFormat()
        .get(nomAttribut);

    Formatter formatter = null;

    if ((formatMessage != null) && formatMessage.isExpressionReguliere()) {
      formatter = new FormatterExpressionReguliere();
    } else {
      formatter = formulaire.getFormatter(nomAttribut, type);
    }

    if (formatMessage != null) {
      formatter.setFormatMessage(formatMessage);
    }

    String message = null;

    if (formatMessage != null) {
      message = formatMessage.getMessage();
    }

    try {
      switch (mode) {
      case BaseForm.VERS_OBJET_TRANSFERT:

        if (formatter instanceof FormatterFile) {
          convertedObj = ((FormatterFile) formatter).formatFichier((FormFile) obj);
        } else {
          if (!boolean.class.isAssignableFrom(type)) {
            convertedObj = formatter.unformat((String) obj, localeUtilisateur,
                fuseauHoraireUtilisateur);

            if ((formatMessage != null) &&
                formatMessage.isExpressionReguliere()) {
              // Si expression régulière, nous devons reconvertir dans le bon type d'objet de transfert.
              formatter = formulaire.getFormatter(nomAttribut, type);
              convertedObj = formatter.unformat((String) convertedObj,
                  localeUtilisateur, fuseauHoraireUtilisateur);
            }

            if (!UtilitaireString.isVide((String) obj)) {
              // Formatter immédiatement dans le formulaire.
              Object valeurFormatte = null;

              /**
               * @since SOFI 2.0.1 Support expression régulière pour autre type que String dans l'objet de transfert.
               * @author Jean-François Brassard
               */
              if ((formatMessage != null) &&
                  formatMessage.isExpressionReguliere()) {
                FormatterExpressionReguliere formatterExpReg = new FormatterExpressionReguliere();
                formatterExpReg.setFormatMessage(formatMessage);
                valeurFormatte = formatterExpReg.format(convertedObj,
                    localeUtilisateur, fuseauHoraireUtilisateur);
              } else {
                valeurFormatte = formatter.format(convertedObj,
                    localeUtilisateur, fuseauHoraireUtilisateur);
              }

              UtilitaireObjet.setPropriete(formulaire, nomAttribut,
                  valeurFormatte);
            }
          }
        }

        break;

      case BaseForm.VERS_FORMULAIRE:

        if (obj == null) {
          convertedObj = formulaire.getValeurParDefaut().get(nomAttribut);
        } else {
          convertedObj = formatter.format(obj, localeUtilisateur,
              fuseauHoraireUtilisateur);
        }

        break;

      default:
        throw new RuntimeException("Mode inconnu: " + mode);
      }
    } catch (FormatterException e) {
      if (message != null) {
        formatter.setCleMessageErreur(message);
      }

      e.setFormatter(formatter);
      throw e;
    }

    return convertedObj;
  }

  /**
   * Retourne <code>false</code> si la valeur donné est un champ
   * minimum au formulaire pour faire les validations et la valeur est a blanc ou a null,
   * sinon <code>true</code> est retourné.
   * <p>
   * @param formulaire le formulaire a traiter.
   * @param nomAttribut Le nom de l'attribut a valider
   * @param value La valeur a valider
   * @return Le résultat de la validation
   */
  static public boolean validerAttributValeurMinimum(BaseForm formulaire,
      String nomAttribut, String value) {
    Map rules = (Map) formulaire.getValidationMap().get(nomAttribut);

    if (rules == null) {
      return true;
    }

    Boolean required = (Boolean) rules.get("validerAttributValeurMinimum");

    if (required == null) {
      return true;
    }

    boolean isRequired = required.booleanValue();
    boolean isBlank = ((value == null) || value.trim().equals(""));

    return !(isRequired && isBlank);
  }

  /**
   * Permet de valider si la valeur présente est incluse dans un intervalle donné.
   * <p>
   * @param formulaire le formulaire a traiter.
   * @param nomAttribut le nom d'attribut a valider.
   * @param valeur la valeur a valider.
   * @return true/false si la valeur est en erreur.
   */
  static public boolean validerInclusDansIntervalle(BaseForm formulaire,
      String nomAttribut, Object valeur) {
    Map regles = (Map) formulaire.getValidationMap().get(nomAttribut);

    if (regles == null) {
      return true;
    }

    Comparateur inclusDansIntervalle = (Comparateur) regles.get(
        "inclusDansIntervalle");

    return (inclusDansIntervalle == null) ||
        inclusDansIntervalle.isInclusDansIntervalle((Comparable) valeur);
  }

  /**
   * Retourne <code>false</code> si la valeur donné est un champ
   * requis et la valeur est a blanc ou a null, sinon <code>true</code>
   * est retourné.
   * <p>
   * @param formulaire le formulaire a traiter.
   * @param nomAttribut Le nom de l'attribut a valider
   * @param value La valeur a valider
   * @return Le résultat de la validation
   */
  static public boolean validateRequired(BaseForm formulaire,
      String nomAttribut, String value) {
    Map rules = (Map) formulaire.getValidationMap().get(nomAttribut);

    if (rules == null) {
      return true;
    }

    Boolean required = (Boolean) rules.get("required");

    if (required == null) {
      return true;
    }

    boolean isRequired = required.booleanValue();
    boolean isBlank = ((value == null) || value.trim().equals(""));

    return !(isRequired && isBlank);
  }

  static public void fixerProprieteDansObjetTransfert(BaseForm formulaire,
      String nomListe, String nomAttribut, Object valeur,
      HttpServletRequest request) {
    // Le formulaire principal.
    BaseForm formulaireParent = BaseForm.getFormulaire(request.getSession());
    StringBuffer proprieteObjetTransfert = new StringBuffer();

    try {
      Object cible = formulaireParent.getObjetTransfert();
      if (formulaire.getNomListe() != null) {
        proprieteObjetTransfert.append(formulaire.getNomListe());
        proprieteObjetTransfert.append(".");
      }
      proprieteObjetTransfert.append(nomAttribut);

      Class classeSource = UtilitaireObjet.getClasse(cible, proprieteObjetTransfert.toString());


      /*
       * On doit seulement réinitialiser les boolean types de base.
       * Quand il s'agit d'un objet (incluant Boolean), il faut laisser les valeurs a null.
       */
      if (boolean.class.isAssignableFrom(classeSource) && valeur == null) {
        valeur = Boolean.FALSE;
      }

      /*
       * Permet de conserver l'instance de la collection de
       * l'objet de transfert plutôt que d'écraser sa référence
       * avec une nouvelle liste. Il est important de conserver la
       * même instance de liste lorsque l'on utilise un outil ORM.
       * Les ORM utilisent des proxies de liste pour conserver les
       * relations entre les objets enfants d'un parent. Ainsi l'outils
       * de persistence est capable de supprimer insérer ou mettre à jours
       * les instances des entités enfant correctement.
       * 
       * AVERTISSEMENT : Il semble que la population s'occupe déjà d'ajouter
       * les nouveaux objets dans la collection de l'objet de trafert. Il
       * semble que c'est seulement l'affectation d'une nouvelle collection qui
       * cause un problème. Il faut s'assurer que la population fait toutes les
       * opérations dans la liste originale. Ajout - Suppression. Est-ce que des
       * listes sont utilisées pour autre chose que des nested dans les
       * formulaires?
       * 
       * Dans cette version on s'assure que la collection retournée pour l'affectation
       * est la même que l'objet original ce qui respecte les instances de
       * proxy instanciés par les ORM.
       */
      if (List.class.isAssignableFrom(classeSource)) {
        List source = (List) UtilitaireObjet.getValeurAttribut(cible, proprieteObjetTransfert.toString());
        valeur = UtilitaireListe.synchroniserListe(source, (List) valeur);
      }

      /*
       * Par la suite la valeur qui est l'instance source mais contenant
       * les modifications le la valeur généré lors de la population
       * peut être réaffectée à l'entité.
       */
      UtilitaireObjet.setPropriete(cible, proprieteObjetTransfert.toString(), valeur);
    } catch (InvocationTargetException e) {
    } catch (NoSuchMethodException e) {
    } catch (IllegalAccessException e) {
    } catch (IllegalArgumentException e) {
      if (log.isInfoEnabled()) {
        log.info("IllegalArgumentException : La propriété " +
            proprieteObjetTransfert.toString() +
            " ne peut être fixée à la valeur " + valeur);
      }
    }
  }

  /**
   *
   * @return
   * @param nomAttibut
   * @param nomFormulaireParent
   */
  static public String getNomFormulaireComplet(String nomFormulaireParent,
      String nomAttibut) {
    StringBuffer nomListeComplet = new StringBuffer();

    if (nomFormulaireParent != null) {
      nomListeComplet.append(nomFormulaireParent);
      nomListeComplet.append(".");
      nomListeComplet.append(nomAttibut);
    } else {
      nomListeComplet.append(nomAttibut);
    }

    return nomListeComplet.toString();
  }

  /**
   * Retourne le nom d'attribut sans la référence au formulaire imbriqué.
   * @return le nom d'attribut sans la référence au formulaire imbriqué.
   * @param nomAttribut le nom d'attribut complet.
   */
  static public String getNomAttributSansImbrique(String nomAttribut) {
    if (nomAttribut.indexOf(".") != -1) {
      nomAttribut = nomAttribut.substring(nomAttribut.lastIndexOf(".") + 1);
    }

    return nomAttribut;
  }
}
