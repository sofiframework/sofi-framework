/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.util.HashMap;
import java.util.Map;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;


/**
 * Classe abstraite spécialisant BaseForm afin qu'il puisse traiter
 * de la sécurité dans le formulaire pour ces attributs.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 */
public abstract class BaseSecureForm extends BaseForm {
  /**
   * 
   */
  private static final long serialVersionUID = -5417706600207366151L;
  /** Les attributs du formulaire à sécurisé */
  private Map attributsASecuriser = new HashMap();

  /**
   * Permet d'ajouter un attribut qui peut être sécurisé.
   * La clé de l'objet sécurisable correspondant à son nom.
   * <p>
   * @param nomAttribut le nom de l'attribut à sécuriser
   * @param cleObjetSecurisable l'identifiant unique de l'objet sécurisable
   * @see org.sofiframework.application.securite.objetstransfert.ObjetSecurisable
   */
  public void ajouterAttributASercurise(String nomAttribut,
      String cleObjetSecurisable) {
    attributsASecuriser.put(nomAttribut, cleObjetSecurisable);
  }

  /**
   * Retourne la clé de l'objet sécurisable.
   * <p>
   * @param nomAttribut le nom de l'attribut sécurisable
   * @return la clé de l'objet sécurisable.
   */
  public String getCleObjetSecurisable(String nomAttribut) {
    return (String) attributsASecuriser.get(nomAttribut);
  }

  /**
   * Indique si l'attribut est sécurisé pour l'utilisateur courant.
   * <p>
   * @param nomAttribut le nom de l'attribut à vérifier
   * @param utilisateur l'utilisateur pour qui on vérifie les droits
   * @return la valeur indiquant si l'attribut est sécurisé pour l'utilisateur courant.
   */
  public boolean isAttributSecurisable(String nomAttribut,
      Utilisateur utilisateur) {
    // A Implémenter...
    return false;
  }
}
