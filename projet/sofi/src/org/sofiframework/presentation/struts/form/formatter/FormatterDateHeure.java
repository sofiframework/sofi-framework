/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Un formatter des valeurs correspondant a un type sql.Timestampé.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterDateHeure extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type date avec heure */
  public final static String DATE_ERROR_KEY = "erreur.format.date.heure";

  /** Constructeur par défaut */
  public FormatterDateHeure() {
  }

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return DATE_ERROR_KEY;
  }

  /**
   * Convertit un String en une instance du type java.sql.Timestamp.
   * <p>
   * @param target la String qui représente une date
   * @return une date avec les heure et seconde.
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone fuseauHoraireClient) {
    if ((target == null) || (target.trim().length() < 1)) {
      return null;
    }

    String messageErreur =
        "Incapable de traiter cette valeur de date avec heure: " + target;
    Date dateAvecHeure;

    try {
      dateAvecHeure = UtilitaireDate.convertir_AAAA_MM_JJ_HH_MM_En_DATE(target.substring(0,10), target.substring(11,16));
    } catch (Exception e) {
      throw new FormatterException(messageErreur);
    }

    return dateAvecHeure;
  }

  /**
   * Convertit un instance du type java.sql.Timestamp en String du format AAAA-MM-JJ HH:MM:SS.
   * <p>
   * @param value le java.sql.Timestamp à convertir
   * @param format le format a appliquer.
   * @return une date formattée
   */
  @Override
  public String format(Object value, Locale locale, TimeZone fuseauHoraireClient) {
    if (value == null) {
      return "";
    }

    return UtilitaireDate.convertirEn_AAAA_MM_JJHeure((java.util.Date) value);
  }
}
