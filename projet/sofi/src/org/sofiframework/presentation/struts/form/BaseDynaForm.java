/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.formatter.FormatterBoolean;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Formulaire de base qui gère les champs dynamiquement en fonction de l'objet
 * de  transfert. Le formulaire implante l'interface DynaBean qui est reconnu
 * par les  classes  qui popule le formulaire. Le développeur peut surcharger
 * la méthode  validerFormulaire() pour ajouter des validations du côté
 * formulaire. Les  méthode getString(), getBoolean() et get() permettent de
 * récupérer les  attributs qui sont dans le formulaire.
 */
public class BaseDynaForm extends BaseMultiLigneForm implements org.apache.commons.beanutils.DynaBean {

  /**
   * 
   */
  private static final long serialVersionUID = -3663252045232079340L;

  /**
   * Cette classe est utilisée pour populer les données de la requête dans le
   * formulaire.
   */
  protected BaseDynaClass classeDynamique = new BaseDynaClass();

  /** Contient les attributs et leur valeurs. */
  private HashMap attributs = new HashMap();

  /**
   * Création d'un object BaseDynaForm.
   */
  public BaseDynaForm() {
    // Assigner le nom de la classe de formulaire comme nom de classe dynamique
    this.classeDynamique.setNom(this.getClass().getName());
    // Propriété qui désigne une modification au formulaire.
    this.ajouterAttributDynamiqueSupplementaire("indModification", String.class);

    // Spécifier que l'on désire seulement traiter les attributs qui sont affichés dans la page
    this.setTraiterAttributsAfficheSeulement(true);
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir la valeur d'un attribut d'une cible spécifique (formulaire ou
   * objet de transfert) avec un nom d'attribut spécifique.
   *
   * @param cible Formulaire ou objet de transfert
   * @param nomAttribut Nom de l'attribut désiré
   *
   * @return Valeur de l'attribut
   */
  @Override
  protected Object getValeurAttribut(Object cible, String nomAttribut,
      boolean obtenirFormulaire) {
    Object valeur = null;

    /* Si l'on désire obtenir la valeur d'un attribut du formulaire, on doit
     * aller chercher la valeur dans le hashmap des attributs.
     */
    if (cible == this) {
      if (obtenirFormulaire && nomAttribut != null &&
          nomAttribut.indexOf("[") != -1) {
        /* Si on est dans une liste nested on doit aller chercher
         * le sous-form et appeler getValeur sur le form
         */
        String nomListeSousFormulaire =
            nomAttribut.substring(0, nomAttribut.indexOf("["));
        String index =
            nomAttribut.substring(nomAttribut.indexOf("[") + 1, nomAttribut.indexOf("]"));

        // Permet de valider que la fin de la chaine n'est pas atteinte
        int iDebutNomAttribut = nomAttribut.indexOf("]") + 2;
        if (nomAttribut.indexOf("]") + 2 > nomAttribut.length()) {
          iDebutNomAttribut = nomAttribut.length();
        }
        String nomProchainAttribut =
            nomAttribut.substring(iDebutNomAttribut, nomAttribut.length());
        List listeFormulaire =
            (List)this.attributs.get(nomListeSousFormulaire);

        if (listeFormulaire != null && !listeFormulaire.isEmpty()) {
          BaseForm sousForm =
              (BaseForm)listeFormulaire.get(Integer.parseInt(index));
          if (!UtilitaireString.isVide(nomProchainAttribut)) {
            valeur =
                sousForm.getValeurAttribut(sousForm, nomProchainAttribut, obtenirFormulaire);
          } else {
            valeur = sousForm;
          }
        }
      } else if (obtenirFormulaire && nomAttribut != null &&
          nomAttribut.indexOf(".") != -1) {
        /* Si on est dans un nested on doit aller chercher
         * le sous-form et appeler getValeur sur le form
         */
        String nomSousFormulaire =
            nomAttribut.substring(0, nomAttribut.indexOf("."));
        String nomAttributSousFormulaire =
            nomAttribut.substring(nomAttribut.indexOf(".") + 1);
        BaseForm sousForm = (BaseForm)this.attributs.get(nomSousFormulaire);
        if (sousForm != null) {
          valeur =
              sousForm.getValeurAttribut(sousForm, nomAttributSousFormulaire,
                  obtenirFormulaire);
        }
      } else {
        valeur = this.attributs.get(nomAttribut);
      }

      /* Si la cible est l'objet de transfert la logique
       * pour obtenir la valeur ne change pas.
       */
    } else {
      valeur = super.getValeurAttribut(cible, nomAttribut, obtenirFormulaire);
    }

    return valeur;
  }

  /**
   * Surcharge de la méthode pour obtenir la valeur de la propriété de
   * formulaire indModification.  Comme les données de request sont populés
   * par les méthode de dynabean il faut surcharger  cette méthode pour
   * obtenir la bonne valeur.
   *
   * @return Valeur de l'attribut permettant de savoir si le formulaire est
   *         modifié
   */
  @Override
  protected String getIndModification() {
    return (String)this.attributs.get("indModification");
  }

  /**
   * Surcharge pour permettre d'assigner la valeur de l'indication  de
   * modification par les fonctionnalité de dynabean.
   *
   * @param indModification Valeur de l'attribut permettant de savoir si le
   *        formulaire est modifié
   */
  @Override
  public void setIndModification(String indModification) {
    this.attributs.put("indModification", indModification);
  }

  /**
   * Permet de savoir si le formulaire est nouveau. Vérifie l'attribut
   * nouveau formulaire  et la clé du formulaire (si la clé est identifiée).
   *
   * @return si le formulaire est nouveau
   */
  @Override
  public boolean isNouveauFormulaire() {
    Boolean attributNouveauFormulaire =
        (Boolean)this.attributs.get("nouveauFormulaire");
    boolean nouveau =
        (attributNouveauFormulaire != null) && attributNouveauFormulaire.booleanValue();

    return nouveau || super.isNouveauFormulaire();
  }

  /**
   * Fixer true si un nouveau formulaire
   *
   * @param nouveauFormulaire true si un nouveau formulaire.
   */
  @Override
  public void setNouveauFormulaire(boolean nouveauFormulaire) {
    if (nouveauFormulaire) {
      setFormulaireModifie(true);
    }

    this.attributs.put("nouveauFormulaire", new Boolean(nouveauFormulaire));
  }

  /**
   * Remplace la structure qui contient les attributs et leur valeurs par la
   * structure spécifiée en paramètre.
   *
   * @param attributs Structure qui contient les attributs et leurs valeurs.
   */
  public void setAttributs(HashMap attributs) {
    this.attributs = attributs;
  }

  /**
   * Obtenir la structure qui contient les attributs et leurs valeurs.
   *
   * @return Struture qui contient les attributs
   */
  public HashMap getAttributs() {
    return attributs;
  }

  /**
   * Assigne un nouvel objet de transfert au formulaire. En plus d'assigner
   * l'objet au formulaire,  l'objet sert à construire la description
   * (Classe DynaClass) qui est utilisé lors de la population.
   *
   * @param objetTransfert Nouvel objet de tranfert qui doit être assigné au
   *        formulaire
   */
  @Override
  public void setObjetTransfert(Object objetTransfert) {
    super.setObjetTransfert(objetTransfert);
    this.decrireClasseDynamique(this.classeDynamique, objetTransfert);
  }

  /**
   * Permet de décrire les attributs d'un objet de transfert et d'ajouter
   * leur description dans  une classe dynamique (DynaClass). Permet aux
   * utilitaire de population de convertir les  données.
   *
   * @param classeDynamique Classe dynamqiue qui décrit les champs d'un objet
   *        dynamique
   * @param ot Objet de transfert
   */
  protected void decrireClasseDynamique(BaseDynaClass classeDynamique,
      Object ot) {
    /* Traiter la description des champs seulement si ily a un objet de tranfert
     */
    if (ot != null) {
      Field[] champs = ot.getClass().getDeclaredFields();

      /* Pour tous les champs déclarés de cet objet
       */
      for (int i = 0; i < champs.length; i++) {
        // Obtenir le champ
        Field champ = champs[i];

        // La classe de la propriété en cours
        Class type = champ.getType();

        // Le nom de la propriété en cours
        String nomAttribut = champ.getName();

        /* Ajouter un formateur Boolean si le champ de l'objet de transfert est de type Boolean.
         * Ce formateur gère la transformation de string à Boolean. Les autres types supportés
         * sont des String dans le formulaire (exemple Date, Long , Integer) et ils sont
         * traités automatiquement par le BaseForm.
         */
        if ((type != null) && type.equals(Boolean.class) &&
            !possedeFormatter(nomAttribut)) {
          this.ajouterTypeFormatter(nomAttribut, FormatterBoolean.class);
        }

        // Ajouter la description de l'attribut à la classe dynamique
        classeDynamique.addDynaProperty(new DynaProperty(nomAttribut,
            String.class));
      }
    }
  }

  public boolean possedeFormatter(String nomAttribut) {
    return this.getFormatMap() != null &&
        this.getFormatMap().containsKey(nomAttribut);
  }

  /**
   * Permet d'ajouter le type de données d'un attribut supplémentaire
   * qui n'est pas décrit dans l'objet de transfert.
   *
   * @param type Classe de la valeur de l'attribut supplémentaire
   * @param nomAttribut Nom de l'attribut du formulaire
   */
  public void ajouterAttributDynamiqueSupplementaire(String nomAttribut,
      Class type) {
    this.classeDynamique.addDynaProperty(new DynaProperty(nomAttribut, type));
  }

  /**
   * Initialise tous les attributs à leur valeur par défaut.
   */
  @Override
  protected void initialiserTousLesAttributs(boolean memeInstance) {
    ArrayList listeAttributsAinitiliser = new ArrayList();

    for (Iterator i = this.attributs.keySet().iterator(); i.hasNext(); ) {
      String nomAttribut = (String)i.next();

      // Seul les attributs qui ne sont pas à exclure doivent être initialisés
      if (!this.isAttributAExclure(nomAttribut)) {
        listeAttributsAinitiliser.add(nomAttribut);
      }
    }

    /* Initialiser la liste des attributs qui ne sont pas exclus
     */
    for (Iterator i = listeAttributsAinitiliser.iterator(); i.hasNext(); ) {
      this.attributs.remove(i.next());
    }
  }

  // -------------- Implantation de l'interface DynaBean ------------------------

  /**
   * Permet de vérifier si un attribut de type Map contient un attribut  de
   * nom spécifique.
   *
   * @param nomMap Nom de la struct5ure Map qui doit être présent  comme
   *        attribut du formulaire
   * @param cleMap Clé du map qui doit exister dans le Map
   * @return Si un attribut Map de nomMap contient la clé de nom cleMap
   * @throws NullPointerException Erreur lors de l'accès à la méthode d'une
   * déclaration qui n'est pas associé à un objet (objet Null).
   * @throws IllegalArgumentException Erreur qui survient lorsque la propriété
   * du formulaire spéficié ne correspond pas à un objet de type Map.
   */
  @Override
  public boolean contains(String nomMap, String cleMap) {
    Object valeur = attributs.get(nomMap);

    if (valeur == null) {
      throw new NullPointerException("Aucun attribut Map de nom '" + nomMap +
          "(" + cleMap + ")'");
    } else if (valeur instanceof Map) {
      return (((Map)valeur).containsKey(cleMap));
    } else {
      throw new IllegalArgumentException("Aucun attribut Map de nom '" +
          nomMap + "(" + cleMap + ")'");
    }
  }

  /**
   * Obtenir la valeur d'un attribut de nom spécifique.
   *
   * @param nomAttribut Nom de l'attribut désiré
   *
   * @return Valeur de l'attribut
   */
  @Override
  public Object get(String nomAttribut) {
    Object valeur = null;

    if (nomAttribut.equals("modifie")) {
      valeur = new Boolean(this.isModifie());
    } else {
      valeur = this.attributs.get(nomAttribut);
    }

    return (valeur == null) ? "" : valeur;
  }

  /**
   * Obtenir la valeur chaîne de caractère d'un attribut de nom spécifique
   *
   * @param nomAttribut nom de l'atrribut
   *
   * @return Valeur chaîne de caractère de l'attribut
   */
  public String getString(String nomAttribut) {
    String valeurString = null;

    if (this.get(nomAttribut) instanceof String) {
      valeurString = (String)this.get(nomAttribut);
    } else if (this.get(nomAttribut) != null) {
      valeurString = this.get(nomAttribut).toString();
    }

    return valeurString;
  }

  /**
   * Obtenir la valeur de attribut indexé (l'attribut implante l'interface
   * List ou doit être un tableau (isArray == vrai))
   *
   * @param nomAttribut Nom de l'attribut qui est indexé ()
   * @param index position à laquel on veux obtenir une valeur
   *
   * @return Valeur à l'indexe de l'attribut
   *
   * @throws NullPointerException Erreur lors de l'accès à la méthode d'une
   * déclaration qui n'est pas associé à un objet (objet Null).
   * @throws IllegalArgumentException Erreur qui survient lorsque la propriété
   * du formulaire spéficié ne correspond pas à un objet de type Map.
   */
  @Override
  public Object get(String nomAttribut, int index) {
    Object valeur = attributs.get(nomAttribut);

    if (valeur == null) {
      throw new NullPointerException("Aucun attribut indexé de nom '" +
          nomAttribut + "[" + index + "]'");
    } else if (valeur.getClass().isArray()) {
      return (Array.get(valeur, index));
    } else if (valeur instanceof List) {
      return ((List)valeur).get(index);
    } else {
      throw new IllegalArgumentException("Aucun attribut indexé de nom '" +
          nomAttribut + "[" + index + "]'");
    }
  }

  /**
   * Obtenir la valeur d'un attribut de type Map.
   * @return Valeur de l'entrée de Map
   * @param cle Clé correspondant à une entrée de l'attribut de type Map
   * @param nomAttribut Nom de l'attribut qui est un type de Map
   */
  @Override
  public Object get(String nomAttribut, String cle) {
    Object valeur = attributs.get(nomAttribut);

    if (valeur == null) {
      throw new NullPointerException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    } else if (valeur instanceof Map) {
      return (((Map)valeur).get(cle));
    } else {
      throw new IllegalArgumentException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    }
  }

  /**
   * Obtenir la classe dynamique qui décrit les attributs valides du formulaire courant
   * @return Instance de classe dynamique
   */
  @Override
  public DynaClass getDynaClass() {
    return this.classeDynamique;
  }

  /**
   * 
   * @param classe
   */
  protected void setDynaClass(BaseDynaClass classe) {
    this.classeDynamique = classe;
  }

  /**
   * Enlever une entrée d'un attributà l'aide de sa clé.
   * @param nomAttribut Nom de l'attribut de type Map
   * @param cle Clé de l'entrée de Map que l'on désire enlever
   */
  @Override
  public void remove(String nomAttribut, String cle) {
    Object valeur = attributs.get(nomAttribut);

    if (valeur == null) {
      throw new NullPointerException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    } else if (valeur instanceof Map) {
      ((Map)valeur).remove(cle);
    } else {
      throw new IllegalArgumentException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    }
  }

  /**
   * Ajouter/modfier un attribut dynamique avec une valeur spécifique.
   * @param nomAttribut Nom de l'attribut que l'on désire ajouter
   * @param valeur Valeur de l'attribut à ajouter
   */
  @Override
  public void set(String nomAttribut, Object valeur) {
    /**
     * Attributs qui ne sont pas décrits par la
     * classe dynamique mais qui doivent être de type Boolean
     */
    if (nomAttribut.equals("nouveauFormulaire")) {
      if (valeur instanceof String) {
        valeur = new Boolean((String)valeur);
      }
    }

    // Assigner l'attribut et sa valeur dans la structure Map
    attributs.put(nomAttribut, valeur);
  }

  /**
   * Obtenir la description d'une propriété dynamique.
   *
   * @return Description de propriété dynamique
   * @param nomAttribut Nom de l'attribut désiré
   */
  protected DynaProperty getDynaProperty(String nomAttribut) {
    DynaProperty description = getDynaClass().getDynaProperty(nomAttribut);

    if (description == null) {
      throw new IllegalArgumentException("Nom de l'atrtibut '" + nomAttribut +
          "' invalide.");
    }

    return (description);
  }

  /**
   * Obtenir la classe d'une propriété du formulaire.
   * @param nomAttribut Nom de l'attribut
   * @return Classe de l'attribut spécifié
   */
  protected Class getTypeAttribut(String nomAttribut) {
    DynaProperty propriete = this.getDynaProperty(nomAttribut);
    return propriete.getType();
  }

  /**
   * Fixer la valeur d'un attribut indexé à une position spécifique.
   * @param valeur Valeur de l'attribut à ajouter
   * @param index Indexe de liste ou doit être ajouté la nouvelle valeur
   * @param nomAttribut Nom de l'attribut indexé à modifier
   */
  @Override
  public void set(String nomAttribut, int index, Object valeur) {
    Object attribut = attributs.get(nomAttribut);

    if (attribut == null) {
      throw new NullPointerException("Aucun attribut indexé de nom '" +
          nomAttribut + "[" + index + "]'");
    } else if (attribut.getClass().isArray()) {
      Array.set(attribut, index, valeur);
    } else if (attribut instanceof List) {
      try {
        ((List)attribut).set(index, valeur);
      } catch (ClassCastException e) {
        throw new ConversionException(e.getMessage());
      }
    } else {
      throw new IllegalArgumentException("Aucun attribut indexé de nom '" +
          nomAttribut + "[" + index + "]'");
    }
  }

  /**
   * Assigner une entrée (clé, valeur) à un attribut de type Map.
   *
   * @param valeur Valeur de l'entrée de Map à ajouter
   * @param clé Clé de l'entrée de Map à ajouter/modifier
   * @param nomAttribut Nom de l'attribut de type Map
   */
  @Override
  public void set(String nomAttribut, String cle, Object valeur) {
    Object attribut = attributs.get(nomAttribut);

    if (attribut == null) {
      throw new NullPointerException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    } else if (attribut instanceof Map) {
      ((Map)attribut).put(cle, valeur);
    } else {
      throw new IllegalArgumentException("Aucun attribut Map de nom '" +
          nomAttribut + "(" + cle + ")'");
    }
  }

  /**
   * Déterminer si on doit appliquer un formatter lors de la population de l'attribut.
   */
  @Override
  protected boolean isConvertirPropriete(BaseForm formulaire,
      ObjetTransfert objetTransfert, String nomPropriete) {

    Class typeAttributObjetTransfert = null;

    try {
      typeAttributObjetTransfert =
          UtilitaireObjet.getClasse(objetTransfert, nomPropriete);
    } catch (Exception e) {
    }

    Class typeAttributFormulaire = this.getTypeAttribut(nomPropriete);

    // Traiter les types array [] qui ne sont pas de type array
    // dans objet de transfert mais pas dans le formulaire.
    boolean convertirArrayEnString =
        (typeAttributObjetTransfert != null &&
        typeAttributObjetTransfert.isArray() &&
        typeAttributFormulaire != null &&
        !typeAttributFormulaire.isArray())
        ||
        (typeAttributObjetTransfert != null &&
        !boolean.class.isAssignableFrom(typeAttributObjetTransfert) &&
        !String[].class.isAssignableFrom(typeAttributObjetTransfert));

    return convertirArrayEnString;
  }
}
