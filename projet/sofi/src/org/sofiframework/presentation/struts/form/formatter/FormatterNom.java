/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Un formatter pour les noms communs.
 * <p>
 * Format du masque: Xxxx-Xxxx
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterNom extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type nom */
  public final static String NOM_ERROR_KEY = "";

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return NOM_ERROR_KEY;
  }

  /**
   * Méthode qui sert à convertir une string en le nom d'une personne.
   * <p>
   * Cette méthode place la première lettre de tous les noms en majuscule. De plus
   * elle gère les traits d'unions, ainsi elle ajoute une lettre majuscule à tous les
   * mots qui suivent immédiatement un trait d'union
   * <p>
   * @param nom le nom à mettre en page
   * @return le nom formatté
   */
  @Override
  public Object unformat(String nom, Locale locale, TimeZone fuseauHoraireClient) {
    if ((nom == null) || (nom.trim().length() < 1)) {
      return null;
    } else {
      return UtilitaireString.convertirPremieresLettresEnMajuscule(nom);
    }
  }

  /**
   * Méthode qui sert à convertir une string en le nom d'une personne.
   * <p>
   * Cette méthode place la première lettre de tous les noms en majuscule. De plus
   * elle gère les traits d'unions, ainsi elle ajoute une lettre majuscule à tous les
   * mots qui suivent immédiatement un trait d'union
   * <p>
   * @param nom le nom à mettre en page
   * @param format le format a appliquer.
   * @return le nom formatté
   */
  @Override
  public String format(Object nom, Locale locale, TimeZone fuseauHoraireClient) {
    if (nom == null) {
      return "";
    }

    return UtilitaireString.convertirPremieresLettresEnMajuscule((String) nom);
  }
}
