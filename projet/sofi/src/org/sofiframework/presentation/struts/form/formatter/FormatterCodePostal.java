/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Un formatter pour les code postaux canadiens.
 * <p>
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterCodePostal extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type CodePostal */
  public final static String CODE_POSTAL_ERROR_KEY = "erreur.format.code.postal";

  /** constructeur par défaut */
  public FormatterCodePostal() {
  }

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return CODE_POSTAL_ERROR_KEY;
  }

  /**
   * Convertit un String en un code postal valide prêt à être affiché.
   * <p>
   * @param target la String à formatter.
   * @return un code postal
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone timezone) {
    if ((target == null) || (target.trim().length() == 0)) {
      return null;
    }

    String messageErreur = "Cette String n'est pas un code postal valide.";

    if (!UtilitaireString.isCodePostal(target)) {
      throw new FormatterException(messageErreur, new Exception());
    }

    target = UtilitaireString.enleveTousLesEspaces(target);
    target = target.substring(0, 3) + " " + target.substring(3);

    return target.toUpperCase();
  }

  /**
   * Convertit un instance du type string en code postal de format X9X 9X9.
   * <p>
   * Cette méthode ajoute un espace après trois charactères et fait un UpperCase
   * sur le code postal.
   * <p>
   * @param value le code postal à transformer en String commune
   * @return une string commune
   */
  @Override
  public String format(Object value, Locale locale, TimeZone timezone) {
    if (value == null) {
      return "";
    }

    String target = (String) value;
    target = UtilitaireString.enleveTousLesEspaces(target);
    target = target.substring(0, 3) + " " + target.substring(3);

    return target.toUpperCase();
  }
}
