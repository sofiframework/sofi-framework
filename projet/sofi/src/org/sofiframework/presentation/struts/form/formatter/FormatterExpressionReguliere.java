/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.oro.text.perl.Perl5Util;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Un formatter pour les expressions régulières.
 * <p>
 * @author Jean-François Brassard, Nurun inc.
 * @since SOFI 1.2
 */
public class FormatterExpressionReguliere extends Formatter {
  /** constructeur par défaut */
  public FormatterExpressionReguliere() {
  }

  /**
   *
   * @return
   * @param format
   * @param valeur
   */
  @Override
  public Object unformat(String valeur, Locale locale,
      TimeZone fuseauHoraireClient) {
    FormatMessage formatMessage = getFormatMessage();

    if (UtilitaireString.isVide(valeur)) {
      return null;
    }

    String expressionReguliere = null;
    String formatAccepte = formatMessage.getFormat();

    boolean formatValide = false;

    StringTokenizer iterateur = new StringTokenizer(formatAccepte, ",");

    while (iterateur.hasMoreTokens() && !formatValide) {
      expressionReguliere = iterateur.nextToken().trim();
      formatValide = valideExpressionReguliere(valeur, expressionReguliere);
    }

    if (!formatValide) {
      throw new FormatterException("Incapable de traiter la valeur: " +
          valeur);
    } else {
      String formatRetour = null;

      try {
        if (!UtilitaireString.isVide(formatMessage.getFormatSortie())) {
          formatRetour = traiterExpressionReguliere(expressionReguliere,
              formatMessage.getFormatSortie(), valeur);
        } else {
          formatRetour = valeur;
        }
      } catch (Exception e) {
        return valeur;
      }

      return formatRetour;
    }
  }

  /**
   *
   * @return
   * @param obj
   */
  @Override
  public String format(Object obj, Locale locale, TimeZone fuseauHoraireClient) {
    String valeur = "";

    if (obj != null) {
      valeur = obj.toString();
    }

    FormatMessage formatMessage = getFormatMessage();

    if (valeur == null) {
      return null;
    }

    String expressionReguliere = null;
    String formatAccepte = formatMessage.getFormat();

    boolean formatValide = false;

    StringTokenizer iterateur = new StringTokenizer(formatAccepte, ",");

    while (iterateur.hasMoreTokens() && !formatValide) {
      expressionReguliere = iterateur.nextToken().trim();
      formatValide = valideExpressionReguliere(valeur, expressionReguliere);
    }

    String formatResultat = "";

    if (formatValide) {
      if (!UtilitaireString.isVide(formatMessage.getFormatSaisie())) {
        formatResultat = traiterExpressionReguliere(expressionReguliere,
            formatMessage.getFormatSaisie(), valeur);
      } else {
        formatResultat = valeur;
      }
    }

    return formatResultat;
  }

  /**
   * Valide que la valeur est valide selon l'expression régulière demandé.
   * @return true si la valeur est valide selon l'expression régulière demandé.
   * @param expressionReguliere l'expression régulière à utiliser pour valider
   * @param valeur la valeur a valider.
   */
  public boolean valideExpressionReguliere(String valeur,
      String expressionReguliere) {
    if ((expressionReguliere == null) || (expressionReguliere.length() <= 0)) {
      return false;
    }

    Perl5Util matcher = new Perl5Util();

    return matcher.match("/" + expressionReguliere + "/", valeur);
  }

  /**
   *
   * @return
   * @param valeurSaisie
   * @param expressionReguliere
   */
  public String traiterExpressionReguliere(String expressionReguliere,
      String format, String valeurSaisie) {
    Pattern patternBd = Pattern.compile(expressionReguliere);

    String retour = patternBd.matcher(valeurSaisie).replaceFirst(format);

    return retour;
  }
}
