/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.struts.upload.FormFile;
import org.sofiframework.utilitaire.fichier.UtilitaireFichier;


/**
 * Un formatter pour les FormFile (L'upload de fichier à partir d'un formulaire)
 */
public class FormatterFile extends Formatter {
  public final static String FICHIER_ERROR_KEY = "erreur.format.fichier";
  public final static String TYPE_ERROR_KEY = "erreur.format.fichier.type";
  public final static String TAILLE_ERROR_KEY = "erreur.format.fichier.taille";

  public FormatterFile() {
  }

  /**
   * Retourne la clé de l'erreur par défaut.
   * <p>
   * @return la clé de l'erreur par défaut
   */
  @Override
  public String getCleErreurDefaut() {
    return FICHIER_ERROR_KEY;
  }

  /**
   * Converti en tableau de byte et valide la taille et le type du fichier
   * @param target Le fichier à valider
   * @return Le fichier validé en tableau byte.
   */
  public Object formatFichier(FormFile target) {
    if ((getFormatMessage() != null) &&
        ((target.getFileName() != null) &&
            !target.getFileName().trim().equals(""))) {
      String type = getFormatMessage().getTypeFichier();
      Integer tailleMaximale = getFormatMessage().getTailleFichier();
      String messageErreur = getFormatMessage().getMessage();

      try {
        if (type != null) {
          if (!UtilitaireFichier.isFormatValide(type, target.getFileData())) {
            if (messageErreur != null) {
              setCleMessageErreur(TYPE_ERROR_KEY);
            } else {
              setCleMessageErreur(messageErreur);
            }

            throw new FormatterException("Le format spécifié n'est pas valide",
                new Exception());
          }
        }

        int tailleEnK = target.getFileSize() / 1024;

        if (tailleMaximale != null) {
          if (tailleEnK > tailleMaximale.intValue()) {
            if (messageErreur != null) {
              setCleMessageErreur(TAILLE_ERROR_KEY);
            } else {
              setCleMessageErreur(messageErreur);
            }

            throw new FormatterException("La taille du fichier est trop grande",
                new Exception());
          }
        }
      } catch (FileNotFoundException e) {
      } catch (IOException e) {
      }
    }

    byte[] data = null;

    try {
      if (target != null) {
        data = target.getFileData();
      }
    } catch (FileNotFoundException e) {
    } catch (IOException e) {
    }

    return data;
  }
}
