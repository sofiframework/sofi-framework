/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireObjet;


/**
 * Classe de base pour le traitement d'un formulaire incluant une ou
 * des listes imbriqués.
 * <p>
 * Si vous n'avez qu'une seule liste de formulaire dans votre formulaire,
 * il n'est pas nécessaire de vous créez une propriété dans le formulaire, vous
 * pouvez utiliser l'attribut inclut dans cette classe qui se nomme 'liste'.
 * Donc, dans votre page JSP, vous n'avez qu'a invoquer la propriété 'liste'
 * pour l'itération des formulaires imbriqués.
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.6
 */
public abstract class BaseMultiLigneForm extends BaseForm {
  /**
   * 
   */
  private static final long serialVersionUID = 4798556421879826697L;
  private ArrayList liste = null;

  /**
   * Fixer la liste de formulaire multi-ligne.
   * @param liste la liste de formulaire multi-ligne.
   */
  public void setListe(ArrayList liste) {
    this.liste = liste;
  }

  /**
   * Retourne la liste de formulaire multi-ligne.
   * @return la liste de formulaire multi-ligne.
   */
  public ArrayList getListe() {
    return liste;
  }

  /**
   * Permet de populer une liste d'objet de transfert dans le formulaire.
   * <p>
   * Utiliser seulement si vous n'avez qu'une seule liste de formulaire et que
   * vous n'avez pas d'objet de transfert qui inclus cette liste.
   * @param request la requête en cours.
   * @param liste la liste à populer.
   */
  public void populerListeFormulaire(ArrayList liste, HttpServletRequest request) {
    ObjetTransfertPourListe objetTransfertPourListe = new ObjetTransfertPourListe();

    if (liste == null) {
      liste = new ArrayList();
    }

    objetTransfertPourListe.setListe(liste);
    setNouveauFormulaire(false);
    setFormulaireInitialiseAvecObjetTransfert(true);
    setObjetTransfert(objetTransfertPourListe);

    setObjetTransfertOriginal((ObjetTransfert) UtilitaireObjet.clonerObjet(
        objetTransfertPourListe));

    populate(objetTransfertPourListe, VERS_FORMULAIRE, false, request);
  }

  /**
   * Retourne la liste d'objet de transfert associé au formulaire correspondant
   * à la propriété 'liste'.
   * <p>
   * À utiliser lorsque vous avez utiliser au préalable la méthode 'populerListeFormulaire'.
   * @return la liste d'objet de transfert associé au formulaire.
   */
  public ArrayList getListeObjetTransfert() {
    try {
      ObjetTransfertPourListe objetTransfertPourListe = (ObjetTransfertPourListe) getObjetTransfert();

      return objetTransfertPourListe.getListe();
    } catch (Exception e) {
      throw new SOFIException(
          "Vous devez avoir fixer une liste dans le formulaire avec la méthode 'populerListeFormulaire'.");
    }
  }

  /**
   * Permet d'ajouter une nouvel ligne au formulaire.
   * @param request la requête en traitement.
   * @param nouveauObjetTransfert nouvelle instance d'objet de transfert a populer dans le formulaire.
   */
  public void ajouterLigne(ObjetTransfert nouveauObjetTransfert,
      HttpServletRequest request) {
    ObjetTransfertPourListe objetTransfert = (ObjetTransfertPourListe) getObjetTransfert();
    objetTransfert.getListe().add(nouveauObjetTransfert);
    populerListeFormulaire(objetTransfert.getListe(), request);
  }

  /**
   * Permet d'ajouter une ligne dans une liste de formulaire spécifique.
   * @param request la requête en traitement.
   * @param nouveauObjetTransfert nouvelle instance d'objet de transfert a populer dans le formulaire.
   * @param nomListe la liste à traiter.
   */
  public void ajouterLigne(String nomListe,
      ObjetTransfert nouveauObjetTransfert, HttpServletRequest request) {
    this.ajouterLigne(nomListe, -1, nouveauObjetTransfert, request);
  }

  /**
   * Ajouter un objet de transfert et converti l'objet en un nouveau
   * formulaire qui sera ajouté dans une liste de formulaires
   * à un index spécifique.
   * <p/>
   * Si l'index est spécifié à -1 l'item sera placé à la
   * fin de la liste. Si la position est 0, il sera placé au
   * début comme premier élément.
   * 
   * @param nomListe Nom de la propriété liste de formulaire
   * @param index position ou on veut ajouter un formulaire
   * @param nouveauObjetTransfert Nouvel objet de transfert
   * @param request Requête HTTP
   */
  public void ajouterLigne(String nomListe, int index,
      ObjetTransfert nouveauObjetTransfert, HttpServletRequest request) {
    try {
      List listeFormulaire = (List) UtilitaireObjet.getValeurAttribut(getFormulaireParent(),
          getNomFormulaireComplet(nomListe));
      // Initialiser la liste des attributs traité.
      setListeAttributTraite(new HashSet());
      if (listeFormulaire == null) {
        listeFormulaire = new ArrayList();
      }
      Class typeClasseFormulaire = getTypeClasseATraiter(nouveauObjetTransfert.getClass());
      // Instancier le formulaire correspond à l'objet de transfert à ajouter.
      BaseForm nouveauFormulaire = (BaseForm) typeClasseFormulaire.newInstance();
      /*
       * Le formulaire parent du nouveau formulaire nested est le parent de this.
       *'This' est le formulaire qui contiendra l'instance du nouveau formulaire.
       * this.getFormulaireParent() est donc toujours le bon formulaire parent.
       */
      nouveauFormulaire.setFormulaireParent(this.getFormulaireParent());
      // fixer le mapping avec celui du fomulaire principal
      nouveauFormulaire.setMapping(this.getFormulaireParent().getMapping());

      // Populer le formulaire avec le nouvel objet de transfert à ajouter.
      nouveauFormulaire.populerFormulaire(nouveauObjetTransfert, request, true);

      if (index != -1) {
        UtilitaireListe.insererItem(listeFormulaire, index, nouveauFormulaire);
      } else {
        listeFormulaire.add(nouveauFormulaire);
      }

      // Fixer la nouvelle liste.
      UtilitaireObjet.setPropriete(getFormulaireParent(),
          getNomFormulaireComplet(nomListe), listeFormulaire);
    } catch (Exception e) {
      if (e instanceof InvocationTargetException ||
          e instanceof NoSuchMethodException) {
        throw new SOFIException("Le nom de la liste suivante : " + nomListe +
            " est invalide", e);
      } else {
        throw new SOFIException(e);
      }
    }
  }

  /**
   * Permet de supprimer une ligne dans le formulaire correspondant
   * à l'attribut générique 'liste' inclut dans le formulaire de base.
   * @param request la requête en traitement.
   * @param noLigne le numéro de ligne a supprimer.
   */
  public void supprimerLigne(int noLigne, HttpServletRequest request) {
    ObjetTransfertPourListe objetTransfert = (ObjetTransfertPourListe) getObjetTransfert();
    try {
      getListe().remove(noLigne);
      objetTransfert.getListe().remove(noLigne);
    } catch (Exception e) {
    }
  }

  /**
   * Permet de supprimer une ligne d'une liste de formulaire spécifique.
   * @param request la requête en traitement.
   * @param noLigne le numéro de ligne a supprimer.
   * @param nomListe le nom de liste à traiter.
   */
  public void supprimerLigne(String nomListe, int noLigne,
      HttpServletRequest request) {
    List listeFormulaireASupprimer = (List) UtilitaireObjet.getValeurAttribut(this,
        getNomFormulaireComplet(nomListe));

    if (listeFormulaireASupprimer == null) {
      throw new SOFIException("Le nom de la liste suivante : " + nomListe +
          " est invalide");
    }

    listeFormulaireASupprimer.remove(noLigne);

    List listeObjetTransfertASupprimer = null;

    String nomListeASupprimer = (getNomListe() != null)
        ? (getNomListe() + nomListe) : nomListe;

        listeObjetTransfertASupprimer = (List) UtilitaireObjet.
            getValeurAttribut(getObjetTransfert(), nomListeASupprimer);

        if ((listeObjetTransfertASupprimer != null) &&
            (noLigne < listeObjetTransfertASupprimer.size())) {
          listeObjetTransfertASupprimer.remove(noLigne);
        }
  }
}
