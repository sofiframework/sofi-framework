/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;


/**
 * Permet de convertir une donnée binaire (1 ou 0) en objet Boolean.
 * @version 3.1
 * @author jfbrassard
 */
public class FormatterBooleanBinary extends Formatter {
  /**
   * Création d'un object FormatterBoolean.
   */
  public FormatterBooleanBinary() {
  }

  /**
   * Tranforme une données chaîne de caractère en Objet Boolean.
   * Si la chaîne est différante de Null la valeur est Boolean.TRUE
   * si non elle est Boolean.FALSE.
   *
   * @return Boolean
   * @param target Chaîne de caractère
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone timezone) {
    if (!StringUtils.isEmpty(target)) {
      if ("1".equals(target.toString())) {
        return Boolean.TRUE;
      }else {
        return Boolean.FALSE;
      }
    }
    return null;
  }

  /**
   * Transforme un objet Boolean en chaîne de caractère.
   *
   * Si la bonnées est null la chaîne sera null, si la données est un objet
   * la valeur sera 1 ou 0 en String;
   *
   * @return Valeur en chaîne de caractère de l'objet boolean
   * @param locale Données de localisation de l'utilisateur
   * @param target Objet Boolean
   */
  @Override
  public String format(Object target, Locale locale, TimeZone timezone) {
    String retour = null;
    if (target != null) {
      Boolean valeur = (Boolean)target;
      if (valeur.booleanValue()) {
        retour = "1";
      }else {
        retour = "0";
      }
    }
    return retour;
  }

  /**
   * Clé de message d'erreur qui sera affichée.
   * @return Clé de message d'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return "ref.erreur.format.boolean";
  }
}
