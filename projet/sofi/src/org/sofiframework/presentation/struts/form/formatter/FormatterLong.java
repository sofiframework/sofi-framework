/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireNombre;


/**
 * Un formatter des valeurs correspondant à des Long.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterLong extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type Integer */
  public final static String INTEGER_ERROR_KEY = "erreur.format.integer";

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return INTEGER_ERROR_KEY;
  }

  /**
   * Convertit un String en une instance du type Integer.
   * <p>
   * @param valeurForm la string à convertir en un Integer
   * @return un Integer
   */
  @Override
  public Object unformat(String valeurForm, Locale locale,
      TimeZone fuseauHoraireClient) {
    if ((valeurForm == null) || (valeurForm.trim().length() < 1)) {
      return null;
    }

    String errMsg = "Incapable de traiter la valeur Long de : " + valeurForm;

    // test de la validité du nombre.
    valeurForm = UtilitaireNombre.standardiserNombre(valeurForm, locale);

    try {
      // test de la validité du nombre.
      valeurForm = UtilitaireNombre.standardiserNombre(valeurForm, locale);

      return Long.valueOf(valeurForm);
    } catch (NumberFormatException e) {
      throw new FormatterException(errMsg, e);
    }
  }

  /**
   * Convertit un instance du type Long en String.
   * <p>
   * @param valeur l'objet Long à transformer en String
   * @param format le format a appliquer.
   * @return un Long sous forme de String
   */
  @Override
  public String format(Object valeur, Locale locale,
      TimeZone fuseauHoraireClient) {
    return ((valeur == null) ? "" : valeur.toString());
  }
}
