/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.io.Serializable;


/**
 *
 */
public class FormatMessage implements Serializable {

  private static final long serialVersionUID = -6173230249662795422L;

  private String format;
  private String formatSaisie;
  private String formatSortie;
  private String message;
  private String attribut;
  private boolean expressionReguliere;

  //Paramètres pour valider le format d'un fichier
  private String typeFichier;
  private Integer tailleMaximaleFichier;

  /**
   *
   * @param message
   * @param formatSortie
   * @param formatSaisie
   * @param format
   */
  public FormatMessage(String format, String formatSaisie, String formatSortie,
      String message) {
    this.format = format;
    this.formatSaisie = formatSaisie;
    this.formatSortie = formatSortie;
    this.message = message;

    if ((getFormat() != null) &&
        (getFormat().substring(0, 1).indexOf("^") != -1) && (message != null)) {
      this.expressionReguliere = true;
    }
  }

  /**
   * Constructeur du format a respecter lorsque qu'on utilise un fichier.
   * @param tailleMaximale
   * @param type
   * @param messageErreur
   */
  public FormatMessage(String type, Integer tailleMaximale, String messageErreur) {
    this.typeFichier = type;
    this.tailleMaximaleFichier = tailleMaximale;
    this.message = messageErreur;
  }

  /**
   *
   * @return
   */
  public String getFormat() {
    return format;
  }

  /**
   *
   * @param format
   */
  public void setFormat(String format) {
    this.format = format;
  }

  /**
   *
   * @param formatSortie
   */
  public void setFormatSortie(String formatSortie) {
    this.formatSortie = formatSortie;
  }

  /**
   *
   * @return
   */
  public String getFormatSortie() {
    return formatSortie;
  }

  /**
   *
   * @return
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   *
   * @return
   */
  public String getAttribut() {
    return attribut;
  }

  /**
   *
   * @param attribut
   */
  public void setAttribut(String attribut) {
    this.attribut = attribut;
  }

  /**
   *
   * @param expressionReguliere
   */
  public void setExpressionReguliere(boolean expressionReguliere) {
    this.expressionReguliere = expressionReguliere;
  }

  /**
   *
   * @return
   */
  public boolean isExpressionReguliere() {
    return expressionReguliere;
  }

  /**
   *
   * @param formatSaisie
   */
  public void setFormatSaisie(String formatSaisie) {
    this.formatSaisie = formatSaisie;
  }

  /**
   *
   * @return
   */
  public String getFormatSaisie() {
    return formatSaisie;
  }

  /**
   *
   * @param typeFichier
   */
  public void setTypeFichier(String typeFichier) {
    this.typeFichier = typeFichier;
  }

  /**
   *
   * @return
   */
  public String getTypeFichier() {
    return typeFichier;
  }

  /**
   *
   * @param tailleMaximale
   */
  public void setTailleFichier(Integer tailleMaximale) {
    this.tailleMaximaleFichier = tailleMaximale;
  }

  /**
   *
   * @return
   */
  public Integer getTailleFichier() {
    return tailleMaximaleFichier;
  }
}
