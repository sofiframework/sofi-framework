/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.comparateur;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * La classe d'aide afin de comparer deux valeurs de type Date sans heure.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class ComparateurDate extends Comparateur {
  /**
   * 
   */
  private static final long serialVersionUID = -8782491974050957323L;

  /**
   * Constructeur du comparateur entre deux valeurs.
   * <p>
   * @param valeurMinimal La valeur minimale acceptable pour la comparaison
   * @param valeurMaximal La valeur maximale acceptable pour la comparaison
   */
  public ComparateurDate(Comparable valeurMinimal, Comparable valeurMaximal) {
    super(valeurMinimal, valeurMaximal);
  }

  /**
   * Méthode qui sert à déterminer si la valeur passée en paramètre est inclus
   * dans l'intervalle déterminé par les valeurs minimales et maximales.
   * <p>
   * @param valeur la valeur à vérifier
   * @return la valeur qui indique si la valeur est inclus dans l'intervalle.
   */
  @Override
  public boolean isInclusDansIntervalle(Comparable valeur) {
    if (getValeurMinimal() != null) {
      setValeurMinimal(UtilitaireDate.enleverHeures(
          (java.util.Date) getValeurMinimal()));
    }

    if (getValeurMaximal() != null) {
      setValeurMaximal(UtilitaireDate.enleverHeures(
          (java.util.Date) getValeurMaximal()));
    }

    if (valeur == null) {
      return true;
    }

    valeur = UtilitaireDate.enleverHeures((java.util.Date) valeur);

    Comparable valeurMinimal = getValeurMinimal();

    boolean valeurMinimalAtteint = (valeurMinimal == null) ||
        (valeur.compareTo(valeurMinimal) >= 0);
    boolean valeurMaximalAtteint = (valeurMaximal == null) ||
        (valeur.compareTo(valeurMaximal) <= 0);

    if (valeurMinimalAtteint && valeurMaximalAtteint) {
      return true;
    } else {
      return false;
    }
  }
}
