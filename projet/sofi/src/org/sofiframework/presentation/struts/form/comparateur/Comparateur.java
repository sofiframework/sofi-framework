/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.comparateur;

import java.io.Serializable;


/**
 * La classe d'aide afin de comparer deux valeurs de tout type.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.form.BaseForm
 */
public class Comparateur implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 4870207226865227571L;

  /** La valeur minimale acceptable pour la comparaison */
  Comparable valeurMinimal;

  /** La valeur maximale acceptable pour la comparaison */
  Comparable valeurMaximal;

  /**
   * Constructeur du comparateur entre deux valeurs.
   * <p>
   * @param valeurMinimal La valeur minimale acceptable pour la comparaison
   * @param valeurMaximal La valeur maximale acceptable pour la comparaison
   */
  public Comparateur(Comparable valeurMinimal, Comparable valeurMaximal) {
    setValeurMinimal(valeurMinimal);
    setValeurMaximal(valeurMaximal);
  }

  /**
   * Obtenir la valeur minimale acceptable pour la comparaison.
   * <p>
   * @return La valeur minimale acceptable pour la comparaison
   */
  public Comparable getValeurMinimal() {
    return valeurMinimal;
  }

  /**
   * Fixer la valeur minimale acceptable pour la comparaison.
   * <p>
   * @param valeurMinimal la valeur minimale acceptable pour la comparaison
   */
  public void setValeurMinimal(Comparable valeurMinimal) {
    this.valeurMinimal = valeurMinimal;
  }

  /**
   * Obtenir la valeur maximale acceptable pour la comparaison.
   * <p>
   * @return La valeur maximale acceptable pour la comparaison
   */
  public Comparable getValeurMaximal() {
    return valeurMaximal;
  }

  /**
   * Fixer la valeur maximale acceptable pour la comparaison.
   * <p>
   * @param valeurMaximal la valeur maximale acceptable pour la comparaison
   */
  public void setValeurMaximal(Comparable valeurMaximal) {
    this.valeurMaximal = valeurMaximal;
  }

  /**
   * Méthode qui sert à déterminer si la valeur passée en paramètre est inclus
   * dans l'intervalle déterminé par les valeurs minimales et maximales.
   * <p>
   * @param valeur la valeur à vérifier
   * @return la valeur qui indique si la valeur est inclus dans l'intervalle.
   */
  public boolean isInclusDansIntervalle(Comparable valeur) {
    if (valeur == null) {
      return true;
    }

    boolean valeurMinimalAtteint = (valeurMinimal == null) ||
        (valeur.compareTo(valeurMinimal) >= 0);
    boolean valeurMaximalAtteint = (valeurMaximal == null) ||
        (valeur.compareTo(valeurMaximal) <= 0);

    if (valeurMinimalAtteint && valeurMaximalAtteint) {
      return true;
    } else {
      return false;
    }
  }
}
