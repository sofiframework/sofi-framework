/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.sql.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Un formatter des valeurs correspondant a un type sql.Date.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterSqlDate extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type Date */
  public final static String DATE_ERROR_KEY = "erreur.format.date";

  /** Construction par défaut */
  public FormatterSqlDate() {
  }

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreur() {
    return DATE_ERROR_KEY;
  }

  /**
   * Convertit un String en une instance du type java.sql.Date.
   * <p>
   * @param target la String à convertir
   * @return une java.sql.Date
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone fuseauHoraireClient) {
    if ((target == null) || (target.trim().length() < 1)) {
      return null;
    }

    String messageErreur = "Incapable de traiter cette valeur de date: " +
        target;

    Date date = null;

    if (target.length() != 10) {
      throw new FormatterException(messageErreur);
    }

    try {
      date = UtilitaireDate.convertir_AAAA_MM_JJ_En_SqlDate(target);
    } catch (Exception e) {
      throw new FormatterException(messageErreur);
    }

    return date;
  }

  /**
   * Convertit un instance du type java.util.Date en String du format AAAA-MM-JJ.
   * <p>
   * @param value la java.sql.Date à convertir en String
   * @return une date formattée
   */
  @Override
  public String format(Object value, Locale locale, TimeZone fuseauHoraireClient) {
    if (value == null) {
      return "";
    }

    return UtilitaireDate.convertirEn_AAAA_MM_JJ((java.sql.Date) value);
  }
}
