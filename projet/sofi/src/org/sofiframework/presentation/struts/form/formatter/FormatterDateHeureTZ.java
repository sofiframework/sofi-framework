/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.sofiframework.objetstransfert.DateTZ;

/**
 * Formate une date avec le fuseau horaire de l'utilisateur.
 */
public class FormatterDateHeureTZ extends FormatterDateHeure {
  /**
   * Constructeur par défaut
   */
  public FormatterDateHeureTZ() {
  }

  /**
   * 
   * @return
   * @param fuseauHoraire
   * @param locale
   * @param value
   */
  @Override
  public String format(Object value, Locale locale, TimeZone fuseauHoraireClient) {
    String dateFormatee = null;

    if (value != null) {
      DateTZ date = (DateTZ) value;
      //ajustee = date.getDate(fuseauHoraireClient);

      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", locale);
      format.setTimeZone(fuseauHoraireClient);
      dateFormatee = format.format(date);
    }

    return dateFormatee;
  }

  /**
   * 
   * @return
   * @param fuseauHoraire
   * @param locale
   * @param target
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone fuseauHoraireClient) {
    DateTZ dateTZ = null;

    if (target != null && (target.trim().length() > 0)) {
      try {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", locale);
        format.setTimeZone(fuseauHoraireClient);
        Date date = format.parse(target);
        dateTZ = new DateTZ(date);

      } catch (ParseException e) {
        throw new FormatterException(getCleMessageErreur());
      }
    }

    return dateTZ;
  }

  /*public static void main(String[] args) {
    DateTZ date = new DateTZ();
    FormatterDateHeureTZ fmt = new FormatterDateHeureTZ();
    String format = fmt.format(date, new Locale("fr", "ca"), TimeZone.getTimeZone("GMT-2:00"));
    Date date2 = (DateTZ) fmt.unformat(format, new Locale("fr", "ca"), TimeZone.getTimeZone("GMT-2:00"));
    boolean convertie = date.equals(date2);//manque els secondes..mais cest normal
  }*/
}