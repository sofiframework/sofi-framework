/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;


/**
 * Un formatter pour le courriel.
 * <p>
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.0
 */
public class FormatterCourriel extends Formatter {
  /** La clé utilisé pour extraire le message d'erreur concernant le type CodePostal */
  public final static String COURRIEL_ERROR_KEY = "erreur.format.courriel";

  private static final String ATOM = "[a-z0-9!#$%&'*+/=?^_`{|}~-]";
  private static final String DOMAIN = "(" + ATOM + "+(\\." + ATOM + "+)*";
  private static final String IP_DOMAIN = "\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\]";

  private static final java.util.regex.Pattern EMAIL_PATTERN = java.util.regex.Pattern
      .compile("^" + ATOM + "+(\\." + ATOM + "+)*@" + DOMAIN + "|"
          + IP_DOMAIN + ")$", java.util.regex.Pattern.CASE_INSENSITIVE);

  /** constructeur par défaut */
  public FormatterCourriel() {
  }

  /**
   * Retourne la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  @Override
  public String getCleErreurDefaut() {
    return COURRIEL_ERROR_KEY;
  }

  /**
   * Convertit un String en un courriel valide prêt à être affiché.
   * <p>
   * @param target le courriel à formatter
   * @return Object un courriel valide
   */
  @Override
  public Object unformat(String target, Locale locale, TimeZone timezone) {
    if ((target == null) || (target.trim().length() == 0)) {
      return null;
    }

    String messageErreur = "Le courriel n'est pas valide.";

    if (!isValide(target)) {
      throw new FormatterException(messageErreur, new Exception());
    }

    return target;
  }

  /**
   * Convertit un instance du type String en courriel prêt à être affiché.
   * <p>
   * @param value le courriel à formatter
   * @param format le format a appliquer.
   * @return String la courriel valide
   */
  @Override
  public String format(Object value, Locale locale, TimeZone timezone) {
    return (String) value;
  }

  public boolean isValide(final String value) {
    boolean ret = true;
    if (StringUtils.isNotEmpty(value)) {

      final Matcher matcher = EMAIL_PATTERN.matcher(value);
      ret = matcher.matches();
    }
    return ret;
  }
}
