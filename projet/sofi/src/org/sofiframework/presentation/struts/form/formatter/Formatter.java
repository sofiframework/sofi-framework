/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.form.formatter;

import java.util.Locale;
import java.util.TimeZone;


/**
 * La classe de base pour tous les types de formatters. Offre un formatage
 * par défaut pour tous les types qui n'ont pas un Formatter spécifie par
 * une sous classe.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class Formatter {
  /** Indique la conversion d'un String en un type Objet */
  public final static int STRING_EN_OBJET = 0;

  /** Indique la conversion d'un type Objet en String */
  public final static int OBJET_EN_STRING = 1;

  /** La clé du message d'erreur **/
  private String cleMessageErreur;

  /** Sommaire du formatage demandé **/
  private FormatMessage formatMessage;

  /**
   * Obtenir la clé de l'erreur.
   * <p>
   * @return la clé de l'erreur
   */
  public String getCleErreur() {
    if (getCleMessageErreur() != null) {
      return getCleMessageErreur();
    } else {
      return getCleErreurDefaut();
    }
  }

  /**
   * Retourne la clé par défaut de l'erreur survenu du formatage.
   * @return
   */
  public String getCleErreurDefaut() {
    return "";
  }

  /**
   * Retourne la classe formatter pour le bon type de classe donné. La classe
   * fournie peut être une classe à formatter si l'on veut obtenir le
   * formatter par défaut pour ce type de classe. Si non, on doit fournir en
   * paramètre le type de formatter désiré.
   * <p>
   * Exemple 1 : getFormatter(Long.class);
   *             Retourne un FormatterLong
   *
   * Exemple 2 : getFormatter(MonNouveauFormatter.class);
   *             Retourne un MonNouveauFormatter
   * <p>
   * @param type classe a fomratter ou type de formatter (voir les exemple plus haut)
   * @return un instance de Formatter
   */
  public static Formatter getFormatter(Class type) {
    return FormatterFactory.get(type);
  }

  /**
   * Retourne un objet qui est le resultat d'un formatage
   * de la valeur. Le type de formatage est traite en specifiant
   * un <code>mode</code>, qui peut être <code>STRING_EN_OBJET</code>,
   * or <code>OBJET_EN_STRING</code>.
   * <p>
   * @param target un objet qui contient une valeur a formatter
   * @param type la classe de l'objet a produire si le mode est
   *             <code>STRING_EN_OBJET</code>; otherwise the class
   *             of the provided object
   * @param mode le mode de formatage
   * @param locale Données de localisation
   * @param timezone Fuseau horaire
   * @return une version formatte ou non formatte de l'objet donnee
   */
  public Object format(Object target, Class type, Locale locale,
      TimeZone timezone, int mode) {
    if (mode == OBJET_EN_STRING) {
      return format(target, locale, timezone);
    } else if (mode == STRING_EN_OBJET) {
      return unformat((String) target, locale, timezone);
    } else {
      throw new FormatterException("mode invalide: " + mode);
    }
  }

  /**
   * Retourne la reprensation en String de l'objet donné.
   * Faite une réécriture de cette méthode afin d'offrir un traitement
   * différent.
   * <p>
   * @param target l'objet a formatter
   * @param locale Données de localisation
   * @return un représentation formatter de l'objet donnée.
   */
  public String format(Object target, Locale locale, TimeZone timezone) {
    return (target == null) ? "" : target.toString();
  }

  /**
   * Retourne un objet correspondant à un String reçu.
   * Faite une réécriture de cette méthode afin d'offrir un traitement
   * différent.
   * <p>
   * @param target la String non formatte.
   * @param locale Données de localisation
   * @param timezone Fuseau horaire
   * @return Un objet représentant la valeur en String donnée.
   */
  public Object unformat(String target, Locale locale, TimeZone timezone) {
    return (target == null) ? "" : target.trim();
  }

  /**
   * Retourne la reprensation en String de l'objet donné.
   * Faite une réécriture de cette méthode afin d'offrir un traitement
   * différent.
   * <p>
   * @param target l'objet a formatter
   * @return un représentation formatter de l'objet donnée.
   */
  public String format(Object target, Locale locale) {
    return (target == null) ? "" : target.toString();
  }

  /**
   * Retourne un objet correspondant à un String reçu.
   * Faite une réécriture de cette méthode afin d'offrir un traitement
   * différent.
   * <p>
   * @param target la String non formatte.
   * @param format le format a valider.
   * @return Un objet représentant la valeur en String donnée.
   */
  public Object unformat(String target) {
    return (target == null) ? "" : target.trim();
  }

  /**
   * Fixer la clé du message d'erreur d'un échec de formatter.
   * @param cleMessageErreur la clé du message d'erreur d'un échec de formatter.
   */
  public void setCleMessageErreur(String cleMessageErreur) {
    this.cleMessageErreur = cleMessageErreur;
  }

  /**
   * Retourne la clé du message d'erreur d'un échec de formatter.
   * @return la clé du message d'erreur d'un échec de formatter.
   */
  public String getCleMessageErreur() {
    return cleMessageErreur;
  }

  /**
   *
   * @param formatMessage
   */
  public void setFormatMessage(FormatMessage formatMessage) {
    this.formatMessage = formatMessage;
  }

  /**
   *
   * @return
   */
  public FormatMessage getFormatMessage() {
    return formatMessage;
  }
}
