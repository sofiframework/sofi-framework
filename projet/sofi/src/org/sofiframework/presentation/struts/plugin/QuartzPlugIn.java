/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin;

//import com.jgsullivan.quartz.SchedulerBuilder;

import java.io.File;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.sofiframework.presentation.struts.plugin.base.BasePlugIn;
import org.sofiframework.presentation.struts.plugin.exception.PlugInException;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;


/**
 * Adapteur pour Struts afin configurer le framework de céduleur Quartz.
 * <p>
 * Voici un exemple d'appel dans le fichier quartz-config.xml : <br>
 * <code>
 * &lt;plug-in className="org.sofiframework.presentation.struts.plugin.QuartzPlugIn"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="fichierConfiguration" value="/WEB-INF/quartz-config.xml"/&gt; <br>
 * &lt;/plug-in&gt;
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.plugin.base.BasePlugIn
 * @deprecated Cette classe ne devrait plus être utilisée. Plutôt
 * utiliser l'intégration de quartz avec Spring.
 */
@Deprecated 
public class QuartzPlugIn extends BasePlugIn {
  /** La clé pour le plug-in Quartz */
  public static final String CLE_PLUGIN = org.sofiframework.presentation.struts.plugin.QuartzPlugIn.class.getName();

  /** L'instance de journalisation pour cette classe */
  private Log log = LogFactory.getLog(org.sofiframework.presentation.struts.plugin.QuartzPlugIn.class);

  /** le céduleur */
  protected Scheduler ceduleur;

  /**
   * Initialisation du plugIn.
   * <p>
   * @param servlet le servlet à traiter
   * @param config la configuration du plugIn
   * @throws javax.servlet.ServletException Exception générique
   */
  @Override
  public void init() throws ServletException, PlugInException {
    if (log.isDebugEnabled()) {
      log.debug("Initialisation de Quartz plugin");
    }

    /*
     * ATTENTION Utiliser le nouveau module Quartz intégré à l'infrastructure SOFI
     */

    try {
      servlet.getServletContext().setAttribute(CLE_PLUGIN, this);
    } catch (Exception e) {
      throw new PlugInException("Fichier de configuration du céduleur Quartz introuvable", e);
    }
  }

  /**
   * Retourne l'instance du céduleur .
   * <p>
   * @return Scheduler le céduleur
   */
  public Scheduler getCeduleur() {
    return this.ceduleur;
  }

  /**
   * Fermeture du plug-in Quartz.
   */
  @Override
  public void destroy() {
    try {
      getCeduleur().shutdown();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
