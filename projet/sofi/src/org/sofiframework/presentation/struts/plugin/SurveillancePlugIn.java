/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.presentation.struts.plugin.base.BasePlugIn;


/**
 * Adapteur pour Struts afin de configurer de la surveillance
 * sur l'application.
 * <p>
 * Voici un exemple d'appel dans le fichier struts-config.xml : <br>
 * <code>
 * &lt;plug-in className="org.sofiframework.presentation.struts.plugin.SurveillancePlugIn"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="utilisateur" value="utilisateur"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="cleUtilisateur" value="codeUtilisateur"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="sessionEnMinutes" value="30"/&gt; <br>
 * &lt;/plug-in&gt;
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.plugin.base.BasePlugIn
 * @see org.sofiframework.application.surveillance.GestionSurveillance
 */
public class SurveillancePlugIn extends BasePlugIn {
  /** L'instance de journalisation pour cette classe */
  private Log log = LogFactory.getLog(org.sofiframework.presentation.struts.plugin.SurveillancePlugIn.class);

  /** le code d'utilisateur */
  private String utilisateur;

  /** la clé de l'utilisateur */
  private String cleUtilisateur;

  /** le temps en minutes que la session est ouvert */
  private String sessionEnMinutes;

  /** Chemin d'accès au traitement de surveillance **/
  private String path;

  /**
   * Obtenir le code d'utilisateur.
   * <p>
   * @return le code d'utilisateur
   */
  public String getUtilisateur() {
    return utilisateur;
  }

  /**
   * Fixer le code d'utilisateur.
   * <p>
   * @param utilisateur le code d'utilisateur
   */
  public void setUtilisateur(String utilisateur) {
    this.utilisateur = utilisateur;
  }

  /**
   * Obtenir la clé de l'utilisateur.
   * <p>
   * @return la clé de l'utilisateur
   */
  public String getCleUtilisateur() {
    return cleUtilisateur;
  }

  /**
   * Fixer la clé de l'utilisateur.
   * <p>
   * @param cleUtilisateur la clé de l'utilisateur
   */
  public void setCleUtilisateur(String cleUtilisateur) {
    this.cleUtilisateur = cleUtilisateur;
  }

  /**
   * Obtenir le temps en minutes que la session est ouvert.
   * <p>
   * @return le temps en minutes que la session est ouvert
   */
  public String getSessionEnMinutes() {
    return sessionEnMinutes;
  }

  /**
   * Fixer le temps en minutes que la session est ouvert.
   * <p>
   * @param sessionEnMinutes le temps en minutes que la session est ouvert
   */
  public void setSessionEnMinutes(String sessionEnMinutes) {
    this.sessionEnMinutes = sessionEnMinutes;
  }

  /**
   * Retourne le chemin d'accès des traitements de surveillance.
   * @return le chemin d'accès des traitements de surveillance.
   */
  public String getPath() {
    return path;
  }

  /**
   * Fixer le chemin d'accès des traitements de surveillance.
   * @param path le chemin d'accès des traitements de surveillance.
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Initialisation du plugIn.
   * <p>
   * @throws javax.servlet.ServletException Exception générique
   * @throws org.sofiframework.presentation.struts.plugin.exception.PlugInException exception du plugIn
   */
  @Override
  public void init() throws ServletException {
    if (log.isDebugEnabled()) {
      log.debug("Initialisation de Message plugin");
    }

    GestionSurveillance surveillance = org.sofiframework.application.surveillance.GestionSurveillance.getInstance();
    surveillance.setParamUtilisateur(getUtilisateur());
    surveillance.setParamCleUtilisateur(getCleUtilisateur());
    surveillance.setSessionEnMinutes(new Integer(getSessionEnMinutes()));
    surveillance.setPathSiteSurveillance(getPath());

    surveillance.setDateDepartApplication();
    surveillance.setActif(true);

  }
}
