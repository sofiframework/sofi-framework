/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.sofiframework.composantweb.menu.RepertoireMenu;


/**
 * Adapteur Struts afin de configurer les menus d'une application
 * dynamiquement.
 *
 * @author  Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class MenuPlugIn implements PlugIn {
  /**
   * L'instance commune de journalisation pour cette classe.
   */
  private static Log log = LogFactory.getLog(MenuPlugIn.class);
  private RepertoireMenu repertoireMenu;
  private String fichierConfiguration = "/WEB-INF/application-menu.xml";
  private HttpServlet servlet;

  /**
   * Obtenir le fichier de configuration du plugIn.
   * <p>
   * @return le fichier de configuration du plugIn
   */
  public String getFichierConfiguration() {
    return fichierConfiguration;
  }

  /**
   * Fixer le fichier de configuration du plugIn.
   * <p>
   * @param newFichierConfiguration le fichier de configuration du plugIn
   */
  public void setFichierConfiguration(String fichierConfiguration) {
    this.fichierConfiguration = fichierConfiguration;
  }

  /**
   * Initialisation du plugIn.
   * <p>
   * @param servlet le servlet à traiter
   * @param config la configuration du plugIn
   * @throws javax.servlet.ServletException Exception générique
   */
  @Override
  public void init(ActionServlet servlet, ModuleConfig config)
      throws ServletException {
    if (log.isDebugEnabled()) {
      log.debug("Initialisation de Menu PlugIn");
    }

    this.servlet = servlet;
    repertoireMenu = new RepertoireMenu();
    repertoireMenu.setLoadParam(getFichierConfiguration());
    repertoireMenu.setServletContext(servlet.getServletContext());

    try {
      repertoireMenu.load();
      servlet.getServletContext().setAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU,
          repertoireMenu);

      if (log.isDebugEnabled()) {
        log.debug("Initialisation des menus (MenuPlugIn) avec succès");
      }
    } catch (Exception e) {
      log.error(e);
      throw new ServletException(
          "Echec de l'initialisation des menu (MenuPlugIn): " + e.getMessage());
    }
  }

  /**
   * Libération des ressources utilisés par le plug-in des menus.
   */
  @Override
  public void destroy() {
    repertoireMenu = null;
    servlet.getServletContext().removeAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);
    fichierConfiguration = null;
    servlet = null;
  }
}
