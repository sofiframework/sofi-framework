/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;
import org.sofiframework.presentation.struts.plugin.base.BasePlugIn;
import org.sofiframework.presentation.struts.plugin.exception.PlugInException;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;


/**
 * Adapteur pour Struts afin configurer le framework Velocity.
 * <p>
 * Voici un exemple d'appel dans le fichier struts-config.xml : <br>
 * <code>
 * &lt;plug-in className="org.sofiframework.presentation.struts.plugin.VelocityPlugIn"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="fichierConfiguration" value="/WEB-INF/velocity.properties"/&gt; <br>
 * &lt;/plug-in&gt;
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class VelocityPlugIn extends BasePlugIn {
  /** L'instance de journalisation pour cette classe */
  private Log log = LogFactory.getLog(org.sofiframework.presentation.struts.plugin.VelocityPlugIn.class);

  /** le fichier de configuration du plugIn */
  private String fichierConfiguration;

  /**
   * Obtenir le fichier de configuration du plugIn.
   * <p>
   * @return le fichier de configuration du plugIn
   */
  @Override
  public String getFichierConfiguration() {
    return fichierConfiguration;
  }

  /**
   * Fixer le fichier de configuration du plugIn.
   * <p>
   * @param newFichierConfiguration le fichier de configuration du plugIn
   */
  @Override
  public void setFichierConfiguration(String newFichierConfiguration) {
    this.fichierConfiguration = newFichierConfiguration;
  }

  /**
   * Initialisation du plugIn.
   * <p>
   * @param servlet le servlet à traiter
   * @param config la configuration du plugIn
   * @throws javax.servlet.ServletException Exception générique
   * @throws org.sofiframework.presentation.struts.plugin.exception.PlugInException exception du plugIn
   */
  @Override
  public void init() throws ServletException, PlugInException {
    if (log.isDebugEnabled()) {
      log.debug("Initialisation de Velocity plugin");
    }

    String cheminReelApp = UtilitaireRequest.getEmplacementWebInfReel(getServletContext());

    // Configurer velocity
    String nomFichier = getFichierConfiguration();

    try {
      Velocity.init(cheminReelApp + nomFichier);
    } catch (Exception e) {
      throw new PlugInException(e);
    }
  }
}
