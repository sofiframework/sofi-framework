/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin.exception;

import org.sofiframework.exception.SOFIException;


/**
 * Exception de base pour les toutes exceptions causé
 * dans les plug-in Struts.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class PlugInException extends SOFIException {
  /**
   * 
   */
  private static final long serialVersionUID = 2374409475424622124L;

  public PlugInException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur d'un plug-in.
   * @param message le message de base.
   */
  public PlugInException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur dans
   * un plug-in.
   * @param message le message de base.
   * @param cause la cause.
   */
  public PlugInException(String message, Throwable cause) {
    super(message, cause);
  }
}
