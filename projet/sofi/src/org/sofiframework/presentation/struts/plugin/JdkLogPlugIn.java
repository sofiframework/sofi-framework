/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.util.logging.LogManager;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.struts.plugin.base.BasePlugIn;
import org.sofiframework.presentation.struts.plugin.exception.PlugInException;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;


/**
 * Adapteur pour Struts afin de configurer la journalisation de la JDK 1,4.
 * <p>
 * Voici un exemple d'appel dans le fichier struts-config.xml : <br>
 * <code>
 * &lt;plug-in className="org.sofiframework.presentation.struts.plugin.JdkLogPlugIn"/&gt; <br>
 * &nbsp;&nbsp;&lt;set-property property="fichierConfiguration" value="/WEB-INF/logging.properties"/&gt; <br>
 * &lt;/plug-in&gt;
 * </code>
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.presentation.struts.plugin.base.BasePlugIn
 */
public class JdkLogPlugIn extends BasePlugIn {
  /** L'instance de journalisation pour cette classe */
  private static final Log log = LogFactory.getLog(JdkLogPlugIn.class);

  /**
   * Initialisation du plugIn.
   * <p>
   * @throws javax.servlet.ServletException Exception générique
   */
  @Override
  public void init() throws ServletException, PlugInException {
    if (log.isDebugEnabled()) {
      log.debug("Initialisation de la jdk logging plug-in");
    }

    String cheminReelApp = UtilitaireRequest.getEmplacementWebInfReel(getServletContext());

    File fichier = new File(cheminReelApp + getFichierConfiguration());
    FileInputStream inputStream;

    LogManager logManager = LogManager.getLogManager();

    try {
      inputStream = new FileInputStream(fichier);
      logManager.readConfiguration(inputStream);
    } catch (Exception e) {
      throw new PlugInException(e);
    }
  }
}
