/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin.base;

import java.util.Hashtable;

import javax.ejb.EJBHome;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.modele.ejb.EjbProxy;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Adapteur abstrait pour Struts afin configurer un service local ou distant en EJB.
 * <p>
 * Chacun des adapteurs Struts qui permet de se configurer
 * sur un service local ou un service distant de type EJB doit étendre
 * de cette classe.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BaseServicePlugIn extends BasePlugIn {

  /** le nom d'usager pour le EJB */
  private String EJBNomUsager;

  /** le mot de passe pour le EJB */
  private String EJBMotPasse;

  /** l'URL pour accéder aux EJB */
  private String EJBUrl;

  /** le nom du EJB */
  private String EJBNomBean;

  /** Classe qui construit le context initial. */
  private String contextInitialFactory = "com.evermind.server.rmi.RMIInitialContextFactory";

  /** Nom du service EJB **/
  private String nomServiceEJB;

  /** Le code d'application en traitement **/
  private String codeApplication;

  /**
   * Obtenir le nom d'usager pour le EJB.
   * <p>
   * @return le nom d'usager pour le EJB
   */
  public String getEJBNomUsager() {
    return EJBNomUsager;
  }

  /**
   * Fixer le nom d'usager pour le EJB.
   * <p>
   * @param EJBNomUsager le nom d'usager pour le EJB
   */
  public void setEJBNomUsager(String EJBNomUsager) {
    this.EJBNomUsager = EJBNomUsager;
  }

  /**
   * Obtenir le mot de passe pour le EJB.
   * <p>
   * @return le mot de passe pour le EJB
   */
  public String getEJBMotPasse() {
    return EJBMotPasse;
  }

  /**
   * Fixer le mot de passe pour le EJB.
   * <p>
   * @param EJBMotPasse le mot de passe pour le EJB
   */
  public void setEJBMotPasse(String EJBMotPasse) {
    this.EJBMotPasse = EJBMotPasse;
  }

  /**
   * Obtenir l'URL pour accéder aux EJB.
   * <p>
   * @return l'URL pour accéder aux EJB
   */
  public String getEJBUrl() {
    return EJBUrl;
  }

  /**
   * Fixer l'URL pour accéder aux EJB.
   * <p>
   * @param EJBUrl l'URL pour accéder aux EJB
   */
  public void setEJBUrl(String EJBUrl) {
    this.EJBUrl = EJBUrl;
  }

  /**
   * Obtenir le nom du EJB.
   * <p>
   * @return le nom du EJB
   */
  public String getEJBNomBean() {
    return EJBNomBean;
  }

  /**
   * Fixer le nom du EJB.
   * <p>
   * @param EJBNomBean le nom du EJB
   */
  public void setEJBNomBean(String EJBNomBean) {
    this.EJBNomBean = EJBNomBean;
  }

  /**
   * Extraire le contexte EJB.
   * <p>
   * @return le contexte du EJB
   * @throws javax.naming.NamingException Exception générique
   */
  public Context getInitialContext() throws NamingException {
    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY, getContextInitialFactory());
    env.put(Context.SECURITY_PRINCIPAL, getEJBNomUsager());
    env.put(Context.SECURITY_CREDENTIALS, getEJBMotPasse());
    env.put(Context.PROVIDER_URL, getEJBUrl());
    env.put("dedicated.connection", "true");

    return new InitialContext(env);
  }

  /**
   * Retourne le ContextInitialFactory pour le plugin distant.
   * <p>
   * Par défaut, il est : com.evermind.server.rmi.RMIInitialContextFactory
   * @return le ContextInitialFactory pour le plugin distant.
   */
  public String getContextInitialFactory() {
    return contextInitialFactory;
  }

  /**
   * Fixer le ContextInitialFactory pour le plugin distant.
   * <p>
   * Par défaut, il est : com.evermind.server.rmi.RMIInitialContextFactory
   * @param contextInitialFactory le ContextInitialFactory pour le plugin distant.
   */
  public void setContextInitialFactory(String contextInitialFactory) {
    this.contextInitialFactory = contextInitialFactory;
  }

  /**
   * Retourne le nom du service EJB que va utiliser le gestionnaire de sécurité
   * @return le nom du service EJB
   */
  public String getNomServiceEJB() {
    return nomServiceEJB;
  }

  /**
   * Fixer le nom du service EJB que va utiliser le gestionnaire de sécurité
   * @param nomServiceEJB
   */
  public void setNomServiceEJB(String nomServiceEJB) {
    this.nomServiceEJB = nomServiceEJB;
  }

  public void initialisation(BaseGestionService baseGestionService) {
    baseGestionService.setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
  }

  protected void setService(BaseGestionService gestionService,
      Class ejbHomeClass) throws Exception {
    Object service = getService(ejbHomeClass);

    if (service == null) {
      String messageErreur = "Le service distant suivant : " +
          getNomServiceEJB() +
          " n'est pas configuré dans vos services distants.";
      throw new SOFIException(messageErreur);
    }

    gestionService.setServiceRemote(service);
  }

  protected Object getService(Class ejbHomeClass) throws Exception {
    if (!UtilitaireString.isVide(getNomServiceEJB())) {
      return GestionServiceDistant.getServiceDistant(getNomServiceEJB());
    } else {
      if (UtilitaireString.isVide(getEJBUrl())) {
        throw new SOFIException(
            "Vous devez configurer les parametres EJB du plugin " +
                getClass().getName());
      } else {
        Context context = getInitialContext();
        EJBHome home = (EJBHome) PortableRemoteObject.narrow(context.lookup(
            getEJBNomBean()), ejbHomeClass);
        return EjbProxy.createProxy(home);
      }
    }
  }

  /**
   * Obtenir le code de l'application.
   * <p>
   * @return le code de l'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code de l'application.
   * <p>
   * @param codeApplication le code de l'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }
}
