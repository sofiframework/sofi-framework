/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.plugin.base;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.sofiframework.presentation.struts.plugin.exception.PlugInException;


/**
 * Classe abstraite de base d'un plugIn.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BasePlugIn implements PlugIn {
  /** Le servlet en traitement */
  protected ActionServlet servlet = null;

  /** La configuration du plugIn */
  protected ModuleConfig moduleConfig = null;

  /** le fichier de configuration du plugIn */
  private String fichierConfiguration;

  /**
   * Méthode qui sert à libérer les ressource prises par le plugIn
   */
  @Override
  public void destroy() {
    this.servlet = null;
    this.moduleConfig = null;
  }

  /**
   * Initialisation du plugIn par défaut de Struts Plug-In
   * <p>
   * @param servlet le servlet à traiter
   * @param moduleConfig la configuration du plugIn
   * @throws javax.servlet.ServletException Exception générique
   */
  @Override
  public void init(ActionServlet servlet, ModuleConfig moduleConfig)
      throws javax.servlet.ServletException {
    this.servlet = servlet;
    this.moduleConfig = moduleConfig;

    try {
      init();
    } catch (PlugInException e) {
      // Attraper l'exception du plug-in et la lancer dans le ServletException.
      throw new ServletException(e.toString(), e);
    }
  }

  /**
   * Initialisation de plugIn appelant.
   * <p>
   * @throws javax.servlet.ServletException Exception générique du Servlet
   * @throws org.sofiframework.presentation.struts.exception.PlugInException
   */
  protected void init() throws javax.servlet.ServletException, PlugInException {
  }

  /**
   * Retourne le contexte servlet en cours d'utilisation
   * @return le contexte du servlet en cours d'utilisation
   */
  public ServletContext getServletContext() {
    return servlet.getServletContext();
  }

  /**
   * Retourne l'emplacement du fichier de configuration.
   * <p>
   * @return l'emplacement du fichier de configuration
   */
  public String getFichierConfiguration() {
    return fichierConfiguration;
  }

  /**
   * Fixer l'emplacement du fichier de configuration.
   * <p>
   * @param newFichierConfiguration l'emplacement du fichier de configuration
   */
  public void setFichierConfiguration(String newFichierConfiguration) {
    this.fichierConfiguration = newFichierConfiguration;
  }
}
