/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.utilitaire;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Classe permettant de spécifier si l'utilisateur utilise Internet Explorer ou le moteur Gecko.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireNavigateur {
  /** Variable désignant le code d'acceptation */
  public static final String ACCEPT = "ACCEPT";

  /** Variable utilisée par le fureteur internet */
  public static final String USER_AGENT = "USER-AGENT";

  /** L'instance unique de l'objet fournissant les services de "logging" */
  private static Log log = LogFactory.getLog(org.sofiframework.presentation.utilitaire.UtilitaireNavigateur.class);

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Internet Explorer.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Internet Explorer, false sinon
   */
  public static boolean isInternetExplorer(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (agent.indexOf("msie") != -1) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Internet Explorer version 4.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Internet Explorer version 4, false sinon
   */
  public static boolean isInternetExplorer_4(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (isInternetExplorer(req) && (agent.indexOf("msie 4") != -1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Internet Explorer version 5.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Internet Explorer version 5, false sinon
   */
  public static boolean isInternetExplorer_5(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (isInternetExplorer(req) && (agent.indexOf("msie 5.0") != -1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Internet Explorer version 5,5.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Internet Explorer version 5.5, false sinon
   */
  public static boolean isInternetExplorer_55(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (isInternetExplorer(req) && (agent.indexOf("msie 5.5") != -1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Internet Explorer version 5,5 et supérieur.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Internet Explorer version 5.5 et supérieur, false sinon
   */
  public static boolean isInternetExplorer_55EtPlus(HttpServletRequest req) {
    if (isInternetExplorer(req) && !isInternetExplorer_4(req) &&
        !isInternetExplorer_55(req)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Mozilla.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Mozilla, false sinon
   */
  public static boolean isMozilla(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if ((agent.indexOf("mozilla") != -1) && (agent.indexOf("spoofer") == -1) &&
        (agent.indexOf("compatible") == -1) && (agent.indexOf("opera") == -1) &&
        (agent.indexOf("webtv") == -1) && (agent.indexOf("hotjava") == -1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Mozilla version 1,3.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Mozilla version 1.3, false sinon
   */
  public static boolean isMozzila_13EtPlus(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (agent.indexOf("mozilla") != -1) {
      int pos = agent.lastIndexOf("/");

      String releaseDate = agent.substring(pos + 1, agent.length());

      if (releaseDate.compareTo("20030210") > 0) {
        return true;
      }
    }

    return false;
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Safari.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Safari, false sinon
   */
  public static boolean isSafari(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (agent.indexOf("safari") != -1) {
      return true;
    }

    return false;
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur Safari version mobile.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si Safari version mobile, false sinon
   */
  public static boolean isSafariMobile(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return false;
    }

    agent = agent.toLowerCase();

    if (isSafari(req)) {
      if (agent.indexOf("mobile") != -1) {
        return true;
      }
    }

    return false;
  }

  /**
   * Cette méthode permet de connaître si la requête provient d'un fureteur WML.
   * <p>
   * @param req La requête qui permettra de connaître le fureteur utilisé
   * @return true si WML, false sinon
   */
  public static boolean isWml(HttpServletRequest req) {
    if (req == null) {
      return false;
    }

    String accept = req.getHeader(ACCEPT);

    if (accept == null) {
      return false;
    }

    accept = accept.toLowerCase();

    if (accept.indexOf("wap.wml") != -1) {
      return true;
    } else {
      return false;
    }
  }

  static public String getDescriptionTypeNavigateur(HttpServletRequest req) {
    String agent = req.getHeader(USER_AGENT);

    if (agent == null) {
      return "inconnu";
    }

    agent = agent.toLowerCase();

    StringBuffer typeNavigateur = new StringBuffer();

    if (isInternetExplorer(req)) {
      typeNavigateur.append("Internet Explorer ");

      int positionDepart = agent.indexOf("msie") + 5;

      String partieVersion = agent.substring(positionDepart);
      int positionFin = partieVersion.indexOf(";");
      String version = partieVersion.substring(0, positionFin);

      typeNavigateur.append(version);

      return typeNavigateur.toString();
    }

    if (isMozilla(req)) {
      if (agent.indexOf("firefox") != -1) {
        typeNavigateur.append("Firefox ");

        int positionDepart = agent.indexOf("firefox") + 5;

        String partieVersion = agent.substring(positionDepart);
        int positionFin = partieVersion.indexOf("/");
        String version = partieVersion.substring(positionFin + 1);

        typeNavigateur.append(version);
      } else {
        if (isSafariMobile(req)) {
          typeNavigateur.append("Safari ");

          if (isSafariMobile(req)) {
            typeNavigateur.append("mobile ");
          }

          try {
            int positionDepart = agent.toLowerCase().indexOf("safari") - 6;

            String partieVersion = agent.substring(positionDepart);
            int positionFin = partieVersion.indexOf("safari");

            String version = partieVersion.substring(partieVersion.indexOf("/") +
                1, positionFin - 1);

            typeNavigateur.append(version);
          } catch (Exception e) {
            if (log.isWarnEnabled()) {
              log.warn(agent);
            }
          }
        } else {
          typeNavigateur.append(agent);
        }
      }

      return typeNavigateur.toString();
    }

    return "inconnu";
  }
}
