/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.utilitaire;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe utilitaire permettant la gestion de divers éléments du request tel
 * que les cookies par exemple.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireRequest {
  /** L'instance unique de l'objet fournissant les services de "logging" */
  private static Log log =
      LogFactory.getLog(org.sofiframework.presentation.utilitaire.UtilitaireRequest.class);

  /**
   * Methode qui configure un cookie d'une durée de vie de 30 jours.
   * <p>
   * @param response L'objet réponse faisant partie de la requête
   * @param nom Le nom du cookie
   * @param valeur La valeur du cookie
   * @param chemin Le chemin d'accès du cookie
   * @param domaine le domaine que sera accessible le cookie.
   * @param age l'age maximum en seconde de la vie du cookie, défaut est -1, donc non persistant.
   * @param secure Le cookie sera disponible seulement sur https.
   */
  public static void setCookie(HttpServletResponse response, String name,
      String valeur, String chemin, String domaine, Integer age, boolean secure) {
    Cookie cookie = new Cookie(name, valeur);
    cookie.setSecure(false);

    if (!UtilitaireString.isVide(chemin)) {
      cookie.setPath(chemin);
    } else {
      cookie.setPath(null);
    }

    if (age != null) {
      cookie.setMaxAge(age.intValue());
    } else {
      cookie.setMaxAge(-1);
    }

    if (!UtilitaireString.isVide(domaine) && !domaine.equalsIgnoreCase("localhost")) {
      cookie.setDomain(domaine);
    }

    if (log.isInfoEnabled()) {
      log.info("Ajout du cookie : " + cookie.getName() + " - " +
          cookie.getValue());
    }
    
    if (secure) {
      cookie.setSecure(true);
    }

    response.addCookie(cookie);
  }
  
  public static void setCookie(HttpServletResponse response, String name,
      String valeur, String chemin, String domaine, Integer age) {
    setCookie(response, name, valeur, chemin, domaine, age, false);
  }

  /**
   * Methode qui configure un cookie ou de modifier une valeur si cookie déjà présent.
   * <p>
   * @param request la requete
   * @param response L'objet réponse faisant partie de la requête
   * @param nom Le nom du cookie
   * @param valeur La valeur du cookie
   * @param chemin Le chemin d'accès du cookie
   * @param domaine le domaine que sera accessible le cookie.
   * @param age l'age maximum de la vie du cookie, défaut est -1, donc non persistant.
   */
  public static void setCookie(HttpServletRequest request,
      HttpServletResponse response, String nom,
      String valeur, String chemin, String domaine,
      Integer age) {

    // Vérifier si cookie existe déjà

    Cookie cookie = getCookie(request, nom);

    boolean nouveauCookie = false;

    if (cookie == null) {
      cookie = new Cookie(nom, valeur);
      nouveauCookie = true;
    }else {
      cookie.setValue(valeur);
    }

    cookie.setSecure(false);

    if (!UtilitaireString.isVide(chemin)) {
      cookie.setPath(chemin);
    } else {
      cookie.setPath(null);
    }

    if (age != null) {
      cookie.setMaxAge(age.intValue());
    } else {
      cookie.setMaxAge(-1);
    }

    if (!UtilitaireString.isVide(domaine) && !domaine.equalsIgnoreCase("localhost")) {
      cookie.setDomain(domaine);
    }

    if (log.isDebugEnabled() && cookie != null) {
      if (nouveauCookie) {
        log.debug("Ajout du cookie : " + cookie.getName() + " pour la valeur " +
            cookie.getValue());
      } else {
        log.debug("Modification du cookie : " + cookie.getName() + " pour la valeur " +
            cookie.getValue());
      }
    }

    response.addCookie(cookie);
  }

  /**
   * Retourne un cookie pour son nom.
   * <p>
   * @param request la requete
   * @param name le nom du cookie à trouver
   * @return le cookie (si trouvé), sinon null si pas trouvé
   */
  public static Cookie getCookie(HttpServletRequest request, String name) {
    Cookie[] cookies = request.getCookies();
    Cookie returnCookie = null;

    if (cookies == null) {
      return returnCookie;
    }

    for (int i = 0; i < cookies.length; i++) {
      Cookie thisCookie = cookies[i];

      if (thisCookie.getName().equals(name)) {
        // Cookies avec aucune valeur, pas bon!!!
        if (!thisCookie.getValue().equals("") &&
            (thisCookie.getMaxAge() != 0)) {
          returnCookie = thisCookie;
          break;
        }
      }
    }

    return returnCookie;
  }

  /**
   * Permet de supprimer un cookie.
   * <p>
   * @param response La réponse courante
   * @param cookie Le nom du cookie
   */
  public static void supprimerCookie(HttpServletResponse response,
      Cookie cookie, String path) {
    if (cookie != null) {
      // Supprimer le cookie en spécifiant 0 comme age
      cookie.setMaxAge(0);
      cookie.setPath(path);
      response.addCookie(cookie);

      if (log.isInfoEnabled()) {
        log.info("Suppression du cookie : " + cookie.getName() + " - " +
            cookie.getValue());
      }
    }
  }

  /**
   * Permet de supprimer un cookie par son nom.
   * <p>
   * @param request La requête
   * @param response La réponse
   * @param nom Le nom du cookie
   * @param path
   */
  public static void supprimerCookie(HttpServletRequest request,
      HttpServletResponse response, String nom,
      String chemin) {
    Cookie cookie = getCookie(request, nom);

    if (cookie != null) {
      // Supprimer le cookie en spécifiant 0 comme age
      cookie.setMaxAge(0);
      cookie.setPath(chemin);
      response.addCookie(cookie);
    }
  }

  /**
   * Retourne url de l'application.
   * @param request la requête en traitement.
   */
  public static String getUrlApplicationWeb(HttpServletRequest request) {
    String urlComplete = request.getRequestURL().toString();
    String urlApplication =
        urlComplete.substring(0, urlComplete.lastIndexOf("/"));

    return urlApplication;
  }

  /**
   * Retourne l'emplacement du repertoire WEB-INF reel
   * @param servletContext le contexte du Servlet.
   * @return l'emplacement reel du repetoire WEB-FIN du module web.
   */
  public static String getEmplacementWebInfReel(javax.servlet.ServletContext servletContext) {
    return getEmplacementReel(servletContext, "/");
  }


  /**
   * Retourne l'emplacement reel d'un fichier.
   * @param servletContext le contexte du Servlet.
   * @param emplacement emplacement du fichier.
   * @return l'emplacement reel du fichier.
   */
  public static String getEmplacementReel(javax.servlet.ServletContext servletContext,
      String emplacementFichier) {
    String cheminReelApp = servletContext.getRealPath(emplacementFichier);
    // WebLogic ne supporte pas getRealPath.
    if (cheminReelApp == null) {
      try {
        URL resourcePath = servletContext.getResource(emplacementFichier);
        if (resourcePath.getProtocol().equals("file")) {
          cheminReelApp = resourcePath.getPath();
        }
      } catch (MalformedURLException e) {
        // fallback to other mechanisms
      }
    }

    return cheminReelApp;
  }
}
