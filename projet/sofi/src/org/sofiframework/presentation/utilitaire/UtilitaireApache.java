package org.sofiframework.presentation.utilitaire;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe utilitaire pour extraire des valeurs de l'header de l'Apache.
 * @author jfbrassard
 * @since 3.0.2
 */

public class UtilitaireApache {

  /**
   * Retourne l'IP de l'utilisateur soit par l'adresse recu directement ou via l'header de apache.
   * @param request
   * @return l'ip de l'utilisateur.
   */
  public static String getUserIp(HttpServletRequest request) {

    String userIp = request.getHeader("x-forwarded-for");

    if ((userIp == null) || (userIp.length() == 0)) {
      userIp = request.getRemoteAddr();
    }
    
    if (userIp != null && userIp.indexOf(",") != -1) {
      userIp = userIp.substring(0,userIp.indexOf(","));
    }

    return userIp;
  }

  /**
   * Retourne le user agent de apache.
   * @param request la requête en traitement.
   * @return le user agent.
   */
  public static String getUserAgent(HttpServletRequest request) {

    String userAgent = request.getHeader("User-Agent");

    return userAgent;

  }

}
