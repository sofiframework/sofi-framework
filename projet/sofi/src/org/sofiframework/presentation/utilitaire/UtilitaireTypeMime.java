package org.sofiframework.presentation.utilitaire;

import java.util.HashMap;
import java.util.Map;

import org.sofiframework.utilitaire.documentelectronique.UtilitaireDocumentElectronique;

public class UtilitaireTypeMime {

  private static Map iconeMimeType = new HashMap();

  static {
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_WORD, "icone_doc.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_PDF, "icone_pdf.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_ZIP, "icone_zip.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_EXCEL, "icone_xls.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_POWERPOINT, "icone_ppt.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_TEXTE, "icone_txt.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_XML, "icone_xml.gif");
    iconeMimeType.put(UtilitaireDocumentElectronique.TYPE_CONTENU_DEFAUT, "icone_octet_stream.gif");
    iconeMimeType.put("image/gif", "icone_image.gif");
    iconeMimeType.put("image/bmp", "icone_image.gif");
    iconeMimeType.put("image/ief", "icone_image.gif");
    iconeMimeType.put("image/jpeg", "icone_image.gif");
    iconeMimeType.put("image/tiff", "icone_image.gif");
    iconeMimeType.put("image/gif", "icone_image.gif");
    iconeMimeType.put("image/gif", "icone_image.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "icone_doc.gif");
    iconeMimeType.put("application/vnd.ms-word.document.macroEnabled.12", "icone_doc.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.wordprocessingml.template", "icone_doc.gif");
    iconeMimeType.put("application/vnd.ms-powerpoint.template.macroEnabled.122", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.presentationml.template ", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.ms-powerpoint.addin.macroEnabled.12", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.ms-powerpoint.slideshow.macroEnabled.12", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.presentationml.slideshow", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.ms-powerpoint.presentation.macroEnabled.12", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.presentationml.presentation", "icone_ppt.gif");
    iconeMimeType.put("application/vnd.ms-excel.addin.12application/xml", "icone_xls.gif");
    iconeMimeType.put("application/vnd.ms-excel.addin.macroEnabled.12", "icone_xls.gif");
    iconeMimeType.put("application/vnd.ms-excel.sheet.binary.macroEnabled.12", "icone_xls.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "icone_xls.gif");
    iconeMimeType.put("application/vnd.ms-excel.template.macroEnabled.12", "icone_xls.gif");
    iconeMimeType.put("application/vnd.openxmlformats-officedocument.spreadsheetml.template", "icone_xls.gif");
  }

  /**
   * En mettant le constructeur privé, on ne peut instancier cette classes
   */
  private UtilitaireTypeMime() {
  }

  /**
   * Fonction qui permet de faire le parrallèle entre un mime type et son icone
   * 
   * @return le nom du fichier à afficher
   * @param mymeType le mimetype rechercher.
   */
  public static String getFichierIcone(String mymeType) {
    return (String) iconeMimeType.get(mymeType);
  }

}
