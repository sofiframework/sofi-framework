/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.utilitaire;

import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.constante.Constantes;


/**
 * Classe utilitaire permettant la gestion de divers éléments de la session
 * utilisateur.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.3
 */
public class UtilitaireSession {
  public static final String TIMEZONE_KEY = "TIMEZONE";

  /** L'instance unique de l'objet fournissant les services de "logging" */
  private static Log log = LogFactory.getLog(org.sofiframework.presentation.utilitaire.UtilitaireSession.class);

  /**
   * Retourne le locale de la session utilisateur.
   * @return le locale de la session utilisateur.
   * @param session la session utilisateur.
   */
  public static Locale getLocale(HttpSession session) {
    return (Locale) session.getAttribute(Constantes.LOCALE_JSTL);
  }

  /**
   * Retourne le fuseau horaire contenu dans la session HTTP.
   * @return Fuseau horaire
   * @param session Session HTTP en cours
   */
  public static TimeZone getTimeZone(HttpSession session) {
    return (TimeZone) session.getAttribute(TIMEZONE_KEY);
  }

  /**
   * Fixer un locale dans la session utilisateur.
   * @param session la session de l'utilisateur.
   * @param locale le locale a fixer.
   */
  public static void setLocale(Locale locale, HttpSession session) {
    session.setAttribute(Constantes.LOCALE_JSTL, locale);
  }

  /**
   * Fixe un fuseau horaire dans la session spécifiée.
   * @param fuseauHoraire Fuseau horaire
   */
  public static void setTimeZone(TimeZone fuseauHoraire, HttpSession session) {
    session.setAttribute(TIMEZONE_KEY, fuseauHoraire);
  }

  /**
   * Retourne la dernière adresse d'un action sélectionné par l'utilisateur.
   * @return la dernière adresse d'un action sélectionné par l'utilisateur.
   * @param session la session de l'utilisateur.
   */
  public static String getDerniereAdresseActionSelectionnne(HttpSession session) {
    String action = (String) session.getAttribute(Constantes.ADRESSE_ACTION_SELECTIONNE_AVANT_REDIRECT);

    return action;
  }

  /**
   * Retourne le service courant utilisé par l'utilisateur.
   * @param session le service courant utilisé par l'utilisateur.
   * @return le service courant utilisé par l'utilisateur.
   */
  public static ObjetSecurisable getServiceCourant(HttpSession session) {
    ObjetSecurisable serviceCourant = (ObjetSecurisable) session.getAttribute(Constantes.SERVICE_SELECTIONNEE);

    return serviceCourant;
  }

  /**
   * Retourne le service à journaliser.
   * @param session la session de l'utilisateur
   * @return le service courant utilisé par l'utilisateur.
   */
  public static ObjetSecurisable getServiceAJournaliser(HttpSession session) {
    ObjetSecurisable serviceCourant = getServiceCourant(session);

    if (serviceCourant != null) {
      while (!(serviceCourant instanceof Service)) {
        serviceCourant = serviceCourant.getObjetSecurisableParent();
      }
    }

    return serviceCourant;
  }
}
