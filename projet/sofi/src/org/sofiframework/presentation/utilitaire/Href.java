/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.utilitaire;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet représentant une adresse Web (Plus précisément les paramètres du URL).
 * <p>
 * Offre des méthodes pour insérer et supprimer des paramètres d'un adresse web.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class Href implements Serializable {

  private static final long serialVersionUID = -7238299377691175909L;

  /**
   * Adressse de base de l'adresse.
   */
  protected String url;

  /**
   * Les paramètres de l'adresse Web.
   */
  protected Map parametres;

  /**
   * Un ancre pouvant être ajouté à la fin de l'adresse web.
   */
  protected String ancre;

  /**
   * Attribut Spécifier un paramètres sans valeur;
   */
  private String parametreSansValeur;

  /**
   * Construit un nouvelle adresse Web en passant l'URL. Les paramètres sont extrait de l'adresse.
   * et sauvegarder dans la variables <parametres>.
   * @param baseUrl l'adresse web de base
   */
  public Href(String baseUrl) {
    traiterConstructionHref(baseUrl);
  }

  /**
   * Constructeur pour le Href.
   * <p>
   * @param href l'adresse web qui possède les attributs à copier
   */
  public Href(Href href) {
    this.url = href.url;
    this.ancre = href.ancre;

    // seulement une copies des paramètres est retourné.
    this.parametres = href.getListeParametres();
  }

  /**
   * Constructeur du href en préservant le suivi de session.
   * @param url l'url de base.
   * @param context le contexte de la page.
   */
  public Href(String url, javax.servlet.jsp.PageContext context) {
    // Encoder l'url afin de préservé la session de l'utilisateur refuse les cookies.
    String hrefEncode = ((javax.servlet.http.HttpServletResponse) context).encodeURL(StringUtils.defaultString(
        url));
    traiterConstructionHref(hrefEncode);
  }

  public Href(String url, HttpServletResponse response) {
    // Encoder l'url afin de préservé la session de l'utilisateur refuse les cookies.
    String hrefEncode = response.encodeURL(StringUtils.defaultString(url));
    traiterConstructionHref(hrefEncode);
  }

  private void traiterConstructionHref(String baseUrl) {
    if (baseUrl == null) {
      throw new SOFIException("Vous devez spécifier un url");
    }

    this.parametres = new LinkedHashMap();

    int positionAncre;
    String UrlSansAncre;

    // extraire l'ancre du URL
    if ((positionAncre = baseUrl.indexOf("#")) != -1) {
      UrlSansAncre = baseUrl.substring(0, positionAncre);
      this.ancre = baseUrl.substring(positionAncre + 1);
    } else {
      UrlSansAncre = baseUrl;
    }

    if (UrlSansAncre.indexOf("?") == -1) {
      // Simple URl sans paramètres
      this.url = UrlSansAncre;
    } else {
      // L'url contient déjà des paramètres, mettre dans la liste des paramètres
      StringTokenizer tokenizer = new StringTokenizer(UrlSansAncre, "?");

      // l'url de base (avant le ?)
      this.url = tokenizer.nextToken();

      // Traiter si un paramètre sans valeur après le ?
      if (baseUrl.indexOf("&") != -1) {
        parametreSansValeur = baseUrl.substring(baseUrl.indexOf("?") + 1,
            baseUrl.indexOf("&"));

        if (parametreSansValeur.indexOf("=") != -1) {
          // Si paramètre avec valeur, remettre à null.
          parametreSansValeur = null;
        }
      }

      if (tokenizer.hasMoreTokens()) {
        StringTokenizer paramTokenizer = new StringTokenizer(tokenizer.nextToken(),
            "&");

        // extaire les paramètres (key=value)
        while (paramTokenizer.hasMoreTokens()) {
          // extraire les clé et les valeurs...
          String[] keyValue = StringUtils.split(paramTokenizer.nextToken(), "=");

          // encoder les nom et valeur en html
          String escapedKey = StringEscapeUtils.escapeHtml(keyValue[0]);
          String escapedValue = (keyValue.length > 1)
              ? StringEscapeUtils.escapeHtml(keyValue[1]) : "";

              if (!this.parametres.containsKey(escapedKey)) {
                // ... et ajouter le dans la liste
                this.parametres.put(escapedKey, escapedValue);
              } else {
                // les valeurs additionnels dans les paramètres existant
                Object previousValue = this.parametres.get(escapedKey);

                if ((previousValue != null) && previousValue.getClass().isArray()) {
                  Object[] previousArray = (Object[]) previousValue;
                  Object[] newArray = new Object[previousArray.length + 1];

                  int j;

                  for (j = 0; j < previousArray.length; j++) {
                    newArray[j] = previousArray[j];
                  }

                  newArray[j] = escapedValue;
                  this.parametres.put(escapedKey, newArray);
                } else {
                  this.parametres.put(escapedKey,
                      new Object[] { previousValue, escapedValue });
                }
              }
        }
      }
    }
  }

  /**
   * Supprimer un paramètres de l'adresse web.
   * <p>
   * @param nom le nom du paramètre à enlever
   */
  public void supprimerParametre(String nom) {
    this.parametres.remove(nom);
  }

  /**
   * Ajouter un paramètre de l'adresse web.
   * <p>
   * Cette méthode sert à ajouter un paramètre comme l'exemple suivant :<br>
   * nom = methode<br>
   * valeur = accederTraitement<br>
   * le résultat sera le suivant :<br>
   * <b>...URL?methode=accederTraitement</b>
   * <p>
   * @param nom le nom du paramètre à ajouter à l'URL
   * @param valeur la valeur du paramètre à ajouter
   */
  public void ajouterParametre(String nom, Object valeur) {
    this.parametres.put(nom, valeur);
  }

  /**
   * Ajouter un paramètres de type int dans l'adresse web.
   * <p>
   * @param name le nom du paramètre à ajouter à l'URL
   * @param value int la valeur du paramètre
   */
  public void ajouterParametre(String nom, int value) {
    this.parametres.put(nom, new Integer(value));
  }

  /**
   * Retourne le tableau des paramètres. Le retour est toujours un copie des paramètres original.
   * <p>
   * @return un objet Map représentant la totalité des paramètres de l'URL
   */
  public Map getListeParametres() {
    Map copie = new LinkedHashMap(this.parametres.size());
    copie.putAll(this.parametres);

    return copie;
  }

  /**
   * Méthode qui sert à ajouter toutes les valeurs du Map de paramètre dans l'Url.
   * <p>
   * Tous les paramètres présents sont effacés
   * <p>
   * @param l'objet Map qui contient les paramètres
   */
  public void setListeParametres(Map listeParametres) {
    this.parametres = new LinkedHashMap(listeParametres.size());
    ajouterListeParametres(listeParametres);
  }

  /**
   * Méthode qui sert à ajouter toutes les valeurs du Map de paramètre dans l'Url.
   * <p>
   * Tous les paramètres sont conservés
   * <p>
   * @param listeParametres l'objet Map qui contient les paramètres
   */
  public void ajouterListeParametres(Map listeParametres) {
    if (listeParametres == null) {
      return;
    }

    // copie la valeur
    Iterator iterateur = listeParametres.entrySet().iterator();

    while (iterateur.hasNext()) {
      Map.Entry entry = (Map.Entry) iterateur.next();
      String key = StringEscapeUtils.escapeHtml((String) entry.getKey());

      // ne pas écraser les paramètres courant
      if (!this.parametres.containsKey(key)) {
        Object value = entry.getValue();

        if (value != null) {
          if (value.getClass().isArray()) {
            String[] values = (String[]) value;

            for (int i = 0; i < values.length; i++) {
              values[i] = StringEscapeUtils.escapeHtml(values[i]);
            }
          } else {
            value = StringEscapeUtils.escapeHtml(value.toString());
          }
        }

        this.parametres.put(key, value);
      }
    }
  }

  /**
   * Obtenir l'url de base.
   * <p>
   * @return l'url de base
   */
  public String getBaseUrl() {
    return this.url;
  }

  /**
   * Obtenir l'ancre de l'URI.
   * <p>
   * @return l'ancre de l'URI
   */
  public String getAncre() {
    return this.ancre;
  }

  /**
   * Fixer l'encre de l'URI.
   * <p>
   * @param name l'encre de l'URI
   */
  public void setAncre(String ancre) {
    this.ancre = ancre;
  }

  /**
   * Méthode qui transforme l'URL complet en une chaine complete.
   * <p>
   * @return l'URL complet sous forme de chaine complete
   */
  @Override
  public String toString() {
    return genererUrl(true);
  }

  /**
   * Retourne un url afin d'être utilisé pour une redirection.
   * @return un url afin d'être utilisé pour une redirection.
   */
  public String getUrlPourRedirection() {
    return genererUrl(false);
  }

  private String genererUrl(boolean isUrlHtml) {
    StringBuffer buffer = new StringBuffer(30);

    buffer.append(this.url);

    if (this.parametres.size() > 0) {
      buffer.append('?');

      if (!UtilitaireString.isVide(parametreSansValeur)) {
        buffer.append(parametreSansValeur);
        // Non conforme au W3C. Les & ne sont pas tolérés en XHTML.
        // REF: http://www.w3.org/TR/xhtml1/#C_12
        //        buffer.append("&");
        if (isUrlHtml) {
          buffer.append("&amp;");
        } else {
          buffer.append("&");
        }
      }

      Set parameterSet = this.parametres.entrySet();

      Iterator iterator = parameterSet.iterator();

      while (iterator.hasNext()) {
        Map.Entry entry = (Map.Entry) iterator.next();

        Object key = entry.getKey();
        Object value = entry.getValue();

        if (value == null) {
          buffer.append(key).append('=').append("");
        } else if (value.getClass().isArray()) {
          Object[] values = (Object[]) value;

          for (int i = 0; i < values.length; i++) {
            if (i > 0) {
              if (isUrlHtml) {
                buffer.append("&amp;");
              } else {
                buffer.append("&");
              }
            }

            buffer.append(key).append('=').append(values[i]);
          }
        } else {
          buffer.append(key).append('=').append(value);
        }

        if (iterator.hasNext()) {
          if (isUrlHtml) {
            buffer.append("&amp;");
          } else {
            buffer.append("&");
          }
        }
      }
    }

    if (this.ancre != null) {
      buffer.append("#");
      buffer.append(this.ancre);
    }

    return buffer.toString();
  }
}
