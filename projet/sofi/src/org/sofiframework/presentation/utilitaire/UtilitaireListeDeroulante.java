package org.sofiframework.presentation.utilitaire;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.presentation.ajax.GenerateurXmlAjax;
import org.sofiframework.utilitaire.UtilitaireString;

public class UtilitaireListeDeroulante {

  private static final Log log = LogFactory.getLog(UtilitaireListeDeroulante.class);

  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * Cette méthode permet aussi de redéfinir les proprietes de la liste déroulante.
   * La propriete classname,disabled sont supportés...
   *
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param libelleLigneVide le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param itemValeur la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param itemNom  la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   * @param proprieteValeur la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom  la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param proprietesListeDeroulante la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   */
  public static void genererReponseListeDeroulante(Collection collection,
      String itemNom, String itemValeur, String libelleLigneVide,
      String libelleResultatListeVide, Collection proprietesListeDeroulante,
      String proprieteNom, String proprieteValeur, HttpServletRequest request,
      HttpServletResponse response) {
    GenerateurXmlAjax generateurXmlAjax = new GenerateurXmlAjax();

    if (libelleLigneVide != null) {
      if (!UtilitaireString.isVide(libelleLigneVide)) {
        Libelle libelle =
            UtilitaireLibelle.getLibelle(libelleLigneVide, request, null);
        generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
      } else {
        generateurXmlAjax.ajouterItem("", "");
      }
    }

    if ((libelleResultatListeVide != null) &&
        ((collection == null) || (collection.size() == 0))) {
      Libelle libelle =
          UtilitaireLibelle.getLibelle(libelleResultatListeVide, request, null);
      generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
    }

    if (collection != null) {
      generateurXmlAjax.ajouterListeItem(collection, itemNom, itemValeur,
          false, request);
    }

    if (proprietesListeDeroulante != null) {
      generateurXmlAjax.ajouterListeProrietes(proprietesListeDeroulante,
          proprieteNom, proprieteValeur, request);
    }

    // Envoie du contenu xml
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    try {
      PrintWriter pw = response.getWriter();
      String xml = generateurXmlAjax.genererEnXml();
      pw.write(xml);
      pw.close();
    } catch (IOException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour écrire les valeurs de liste déroulante ajax.", e);
      }
    }
  }
  
  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param libelleLigneVide le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param proprieteValeur la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom  la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   * @param utiliserDescriptionLocale boolean indiquant si on doit utiliser la description locale ou non.
   */
  public static void genererReponseListeDeroulante(Collection collection,
      String proprieteNom, String proprieteValeur, String libelleLigneVide,
      String libelleResultatListeVide, HttpServletRequest request,
      HttpServletResponse response) {
    
   genererReponseListeDeroulante(collection, proprieteNom, proprieteValeur, libelleLigneVide, libelleResultatListeVide, false, request, response);
  }

  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param libelleLigneVide le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide le libelle si le résultat de la liste est vide, s'il y a lieu.
   * @param proprieteValeur la propriété à utiliser dans la liste pour spécifier la valeur.
   * @param proprieteNom  la propriété à utiliser dans la liste pour spécifier le libellé de la liste déroulante.
   * @param collection la liste à générer en XML et à envoyer dans la réponse vers le poste client.
   * @param utiliserDescriptionLocale boolean indiquant si on doit utiliser la description locale ou non.
   */
  public static void genererReponseListeDeroulante(Collection collection,
      String proprieteNom, String proprieteValeur, String libelleLigneVide,
      String libelleResultatListeVide, Boolean utiliserDescriptionLocale, HttpServletRequest request,
      HttpServletResponse response) {
    GenerateurXmlAjax generateurXmlAjax = new GenerateurXmlAjax();

    if (libelleLigneVide != null) {
      if (!UtilitaireString.isVide(libelleLigneVide)) {
        Libelle libelle =
            UtilitaireLibelle.getLibelle(libelleLigneVide, request, null);
        generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
      } else {
        generateurXmlAjax.ajouterItem("", "");
      }
    }

    if ((libelleResultatListeVide != null) &&
        ((collection == null) || (collection.size() == 0))) {
      Libelle libelle =
          UtilitaireLibelle.getLibelle(libelleResultatListeVide, request, null);
      generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
    }

    if (collection != null) {
      generateurXmlAjax.ajouterListeItem(collection, proprieteNom,
          proprieteValeur, utiliserDescriptionLocale, request);
    }

    // Envoie du contenu xml
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    try {
      PrintWriter pw = response.getWriter();
      pw.write(generateurXmlAjax.genererEnXml());
      pw.close();
    } catch (IOException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour écrire les valeurs de liste déroulante ajax.", e);
      }
    }
  }
}
