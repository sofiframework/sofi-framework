/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.utilitaire;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.modele.Modele;
import org.sofiframework.presentation.balisesjsp.utilitaire.InfoMultibox;

/**
 * Classe utilitaire pour accéder divers éléments comme l'utilisateur par
 * exemple via la couche du controleur.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireControleur {
  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory.getLog(UtilitaireControleur.class);

  /**
   * Instance du singleton
   */
  private static UtilitaireControleur instance = new UtilitaireControleur();

  /**
   * Constructeur de l'utilitaire.
   */
  protected UtilitaireControleur() {
    super();
  }

  /**
   * Retourne l'instance du singleton.
   */
  public static UtilitaireControleur getInstance() {
    return instance;
  }

  /**
   * Retourne l'utilisateur courant
   * @param request la requêtre présentement en traitement.
   * @return l'utilisateur courant.
   */
  public static Utilisateur getUtilisateur(HttpSession session) {
    Utilisateur utilisateur =
        (Utilisateur)session.getAttribute(Constantes.UTILISATEUR);

    if ((utilisateur == null) && GestionSecurite.isApplicationGrandPublic()) {
      // Créer un utilisateur avec tous les objets java.
      utilisateur = new Utilisateur();

      utilisateur.setCodeUtilisateur("GRAND_PUBLIC");

      if (GestionSecurite.getInstance().getCodeApplication() != null) {
        GestionSecurite.genererAutorisationUtilisateur(utilisateur);
        session.setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION,
            Boolean.TRUE);
      }

      ajouterUtilisateurDansSession(session, utilisateur);
    }

    return utilisateur;
  }

  /**
   * Fixer l'utilisateur dans la session.
   * @param request la requêtre présentement en traitement
   * @param utilisateur l'utilisateur courant.
   */
  public static void ajouterUtilisateurDansSession(HttpSession session,
      Utilisateur utilisateur) {
    session.setAttribute(Constantes.UTILISATEUR, utilisateur);
  }

  /**
   * Fixer l'utilisateur dans la session.
   * @param request la requêtre présentement en traitement
   * @param utilisateur l'utilisateur courant.
   */
  public static void ajouterUtilisateurDansSession(HttpServletRequest request,
      Utilisateur utilisateur) {
    ajouterUtilisateurDansSession(request.getSession(), utilisateur);
  }

  /**
   * Supprimer l'utilisateur dans la session.
   * @param request la requêtre présentement en traitement
   */
  public static void supprimerUtilisateur(HttpSession session) {
    session.setAttribute(Constantes.UTILISATEUR, null);
  }

  /**
   * Spécifie qu'un nouvelle authentification d'un utilisateur à été faite.
   * @param request la requêtre présentement en traitement
   */
  public static void specifierNouvelleAuthentification(HttpServletRequest request) {
    // Spécifier une nouvelle authentification.
    request.getSession().setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION,
        Boolean.TRUE);
  }

  /**
   * Est-ce une nouvelle authentification?
   */
  public static boolean isNouvelleAuthentification(HttpServletRequest request) {
    boolean nouvelleAuthentification =
        request.getSession().getAttribute(Constantes.NOUVELLE_AUTHENTIFICATION) !=
        null;

    return nouvelleAuthentification;
  }

  /**
   * Est-ce une nouvelle authentification?
   */
  public static void terminerNouvelleAuthentification(HttpServletRequest request) {
    request.getSession().setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION,
        null);
  }

  /**
   * Retourne le service présentement actif.
   * @param request la requête présentement en traitement.
   * @return le service présentement actif.
   */
  public static ObjetSecurisable getServiceCourant(HttpSession session) {
    ObjetSecurisable objetCourant =
        UtilitaireSession.getServiceCourant(session);

    return objetCourant;
  }

  /**
   * Retourne le service présentement parent actif.
   * @param request la requête présentement en traitement.
   * @return le service présentement actif.
   */
  public static ObjetSecurisable getServiceCourantParent(HttpSession session) {
    ObjetSecurisable objetCourant = getServiceCourant(session);

    while ((objetCourant != null) && !(objetCourant instanceof Service)) {
      objetCourant = objetCourant.getObjetSecurisableParent();
    }

    return objetCourant;
  }

  /**
   * Retourne le répertoire de composants d'interface exclus.
   * @param session la session de l'utilisateur
   * @return le répertoire de composants d'interface exclus.
   */
  public static HashSet getRepertoireComposantInterfaceExclus(HttpSession session) {
    return (HashSet)session.getAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL);
  }

  /**
   * Est-ce que le composant d'interface est exclus lors de l'affichage.
   * @return true si le composant d'interface est exclus lors de l'affichage.
   * @param identifiant l'identifiant du composant d'interface.
   * @param request la requête en traitement.
   */
  public static boolean isComposantInterfaceExclus(HttpServletRequest request,
      String identifiant) {
    HashSet repertoireOngletExlure =
        getRepertoireComposantInterfaceExclus(request.getSession());

    if (repertoireOngletExlure == null) {
      return false;
    }

    return repertoireOngletExlure.contains(identifiant);
  }

  /**
   * Est-ce une action sécurisé?
   * @param request la requête en traitement.
   * @return true l'url accéder est sécurisé.
   */
  public static boolean isActionSecurise(HttpServletRequest request) {
    return isActionSecurise(request, getNomActionCourant(request));
  }

  /**
   * Est-ce une action sécurisé?
   * @param request la requête en traitement.
   * @return true l'url accéder est sécurisé.
   */
  public static boolean isActionSecurise(HttpServletRequest request,
      String action) {

    Object actions =
        GestionParametreSysteme.getInstance().getListeValeursAvecSystemeCommun(ConstantesParametreSysteme.ACTION_NON_SECURISE);

    if (actions != null && actions instanceof String &&
        actions.toString().indexOf(",") != -1) {
      actions =
          GestionParametreSysteme.getInstance().getListeValeurAvecChaineMultiple(actions.toString());
    }

    if (!String.class.isInstance(actions)) {
      HashSet listeActionNonSecurise = (HashSet)actions;

      if (listeActionNonSecurise != null) {
        if (listeActionNonSecurise.contains(action)) {
          return false;
        } else {
          return true;
        }
      }

      return true;
    } else {
      if (actions.equals(action)) {
        return false;
      } else {
        return true;
      }
    }
  }

  /**
   * Retourne le nom de l'action courant.
   * @param request la requête en traitement.
   * @return le nom de l'action courant.
   */
  public static String getNomActionCourant(HttpServletRequest request) {
    String adresseCourante = request.getRequestURI();
    String action = adresseCourante.substring(1);
    int indexDebut = action.indexOf("/") + 1;
    action = action.substring(indexDebut);

    int indexFin = action.indexOf(".");
    action = action.substring(0, indexFin);

    return action;
  }


  /**
   * Retourne la localisation de l'utilisateur selon sa langue.
   * @return la localisation de l'utilisateur selon sa langue.
   * @param contexte le contexte de la page.
   */
  public static void setLocalePourUtilisateur(HttpSession session) {
    Utilisateur utilisateur =
        (Utilisateur)session.getAttribute(Constantes.UTILISATEUR);

    if (utilisateur != null) {
      if (utilisateur.getLocale() != null) {
        setLocale(GestionLibelle.getInstance().getLocaleDisponible(utilisateur.getLocale().toString()), session);
      }else {
        setLocale(GestionLibelle.getInstance().getLocaleParDefaut(), session);
      }
    }else {
      setLocale(GestionLibelle.getInstance().getLocaleParDefaut(), session);
    }
  }

  public static String getLanguePourUtilisateur(HttpSession session) {
    // Extraire l'utilisateur en cours de session.
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(session);

    Locale locale = null;

    // Trouver la langue du client.
    if ((utilisateur == null) || (utilisateur.getLangue() == null) ||
        utilisateur.getLangue().equals("")) {
      locale = (Locale) session.getAttribute(Constantes.LOCALE_JSTL);

      try {
        locale.getLanguage();
      } catch (Exception e) {
        locale = GestionLibelle.getInstance().getLocaleParDefaut();
      }
    }

    // Extraire la langue de l'utilisateur
    if ((utilisateur != null) && (utilisateur.getLocale() != null)) {
      locale = utilisateur.getLocale();
    }

    return GestionLibelle.getInstance().getLocaleDisponible(locale.toString()).toString();
  }

  public static Locale getLocale(String codeLocale) {
    return GestionLibelle.getInstance().getLocaleDisponible(codeLocale);
  }

  /**
   * Fixer un locale spécifique
   * @param locale le locale a appliquer.
   */
  public static void setLocale(Locale locale, HttpSession session) {
    session.setAttribute(Constantes.LOCALE_JSTL, locale);
    session.setAttribute(Constantes.CODE_LANGUE_EN_COURS,
        locale.toString());
    session.setAttribute("language", locale.getLanguage());    
  }

  /**
   * Retourne le locale de l'utilisateur.
   * @param session la session de l'utilisateur.
   */
  public static Locale getLocale(HttpSession session) {
    Locale locale = (Locale)session.getAttribute(Constantes.LOCALE_JSTL);
    if (locale != null) {
      return getLocale(((Locale)session.getAttribute(Constantes.LOCALE_JSTL)).toString());
    }else {
      // Si pas de locale selon navigateur retourne locale par défaut du système.
      return GestionLibelle.getInstance().getLocaleParDefaut();
    }
  }
  
  /**
   * Retourne le locale de l'utilisateur.
   * @param session la session de l'utilisateur.
   */
  public static Locale getLocale(HttpServletRequest request) {
    Locale locale = (Locale)request.getSession().getAttribute(Constantes.LOCALE_JSTL);
    if (locale != null) {
      return getLocale(((Locale)request.getSession().getAttribute(Constantes.LOCALE_JSTL)).toString());
    }else {
      // Si pas de locale selon navigateur retourne locale par défaut du système.
      return GestionLibelle.getInstance().getLocaleParDefaut();
    }
  }

  /**
   * Est-ce que la journalisation est active?
   * @return true si le journalisation est active.
   */
  public static boolean isJournalisationActive() {
    Boolean journalisationActive =
        GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.TRAITER_JOURNALISATION);

    return journalisationActive.booleanValue();
  }


  /**
   * Retourne le modèle en cours d'utilisation.
   * @return le modèle en cours d'utilisation.
   * @param request la requête présentement en traitement.
   */
  public static Modele getModele(HttpServletRequest request) {
    return (Modele)request.getAttribute(Constantes.MODELE);
  }

  /**
   * Permet d'exclure un composant d'interface tel qu'un onglet correspondant
   * à son identifiant pour l'action courante.
   * @param identifiant l'identifiant du composant d'interface.
   * @param request la requête en traitement.
   */
  public static void exclureComposantInterface(String identifiant,
      HttpServletRequest request) {
    HashSet repertoireOngletExlure =
        (HashSet)request.getSession().getAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL);

    if (repertoireOngletExlure == null) {
      repertoireOngletExlure = new HashSet();
      request.getSession().setAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL,
          repertoireOngletExlure);
    }

    if (!repertoireOngletExlure.contains(identifiant)) {
      repertoireOngletExlure.add(identifiant);
    }
  }

  /**
   * Permet de d'inclure un composant d'interface qui avait au préalable été exclus.
   * @param identifiant l'identifiant du composant d'interface.
   * @param request la requête en traitement.
   */
  public static void inclureComposantInterface(String identifiant,
      HttpServletRequest request) {
    HashSet repertoireOngletExlure =
        (HashSet)request.getSession().getAttribute(Constantes.REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL);

    if (repertoireOngletExlure != null) {
      if (repertoireOngletExlure.contains(identifiant)) {
        repertoireOngletExlure.remove(identifiant);
      }
    }
  }

  /**
   * Est-ce que la requête traité est un rafrachissement d'un div en ajax avec SOFI
   * @param request la requête en traitement.
   */
  public static boolean isRafraichissementAjax(HttpServletRequest request) {
    if (request.getParameter("ajax_rafraichir") != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que la requête a été provoqué par une génération d'un div persistant
   * @param request la requête en traitement.
   */
  public static boolean isDivPersistantAjax(HttpServletRequest request) {
    if (request.getParameter("persistant") != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que la requête traité est un appel ajax avec SOFI.
   * @param request la requête en traitement.
   */
  public static boolean isAppelAjax(HttpServletRequest request) {
    if (request.getParameter("ajax") != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne le dernier href avec paramètres sélectionnné.
   * @return le dernier href avec paramètres sélectionnné.
   * @param request la requête en traitement.
   */
  public static Href getDernierHrefAvecParametresSelectionne(HttpServletRequest request) {
    return (Href)request.getSession().getAttribute(Constantes.DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE);
  }

  /**
   * Retourne le detail d'un groupe de boite à cocher multiple.
   * @param attribut l'attribut du groupe de boite à cocher multiple
   * @param request la requête en traitement
   */
  public static InfoMultibox getDetailBoiteACocherMultiple(String attribut,
      HttpServletRequest request) {
    HashMap listeInfoListeCouranteMultibox =
        (HashMap)request.getSession().getAttribute("listeInfoListeCouranteMultibox");

    if (listeInfoListeCouranteMultibox == null) {
      listeInfoListeCouranteMultibox = new HashMap();
      request.getSession().setAttribute("listeInfoListeCouranteMultibox",
          listeInfoListeCouranteMultibox);
    }

    InfoMultibox infoListeCouranteMultibox =
        (InfoMultibox)listeInfoListeCouranteMultibox.get(attribut);

    return infoListeCouranteMultibox;
  }

  /**
   * Fixer un attribut temporairement dans la session pour une action bien précise
   * @param identifiant l'identifiant de la valeur a mettre dans la session.
   * @param valeur la valeur a mettre dans la session.
   * @param request la requête en traitement.
   */
  public static void setAttributTemporairePourAction(String identifiant,
      Object valeur, HttpServletRequest request) {
    request.getSession().setAttribute(identifiant, valeur);

    HashSet listeAttributs =
        (HashSet)request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_ACTION);

    if (listeAttributs == null) {
      listeAttributs = new HashSet();
      request.getSession().setAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_ACTION,
          listeAttributs);
    }

    listeAttributs.add(identifiant);

    HashSet listeAttributTemporaire =
        (HashSet)request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT);
    listeAttributTemporaire.add(identifiant);
  }

  /**
   * Fixer un attribut temporairement dans la session pour un service.
   * @param identifiant l'identifiant de la valeur a mettre dans la session.
   * @param valeur la valeur a mettre dans la session.
   * @param request la requête en traitement.
   */
  public static void setAttributTemporairePourService(String identifiant,
      Object valeur, HttpServletRequest request) {
    String index = request.getParameter("index");

    if (index != null) {
      // Traiter la fixation d'attribut pour liste de valeur imbriqués.
      if (identifiant.indexOf(index) == -1) {
        identifiant += index;
      }
    }

    request.getSession().setAttribute(identifiant, valeur);

    HashSet listeAttributs =
        (HashSet)request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_SERVICE);

    if (listeAttributs == null) {
      listeAttributs = new HashSet();
      request.getSession().setAttribute(Constantes.ATTRIBUTS_TEMP_SESSION_SERVICE,
          listeAttributs);
    }

    listeAttributs.add(identifiant);

    HashSet listeAttributTemporaire =
        (HashSet)request.getSession().getAttribute(Constantes.ATTRIBUTS_TEMP_EN_TRAITEMENT);
    listeAttributTemporaire.add(identifiant);
  }

  /**
   * Est-ce que le traitement est cours est une soumission de formulaire?
   * @return true si le traitement est cours est une soumission de formulaire.
   * @param request la requête de l'utilisateur.
   */
  public static boolean isTraitementPost(HttpServletRequest request) {
    if (request.getSession().getAttribute("SOFI_ACTION_POST") != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Est-ce que le div est ouvert?
   * @return true si le div est ouvert.
   * @param request la requête en traitement.
   * @param idDiv l'identifiant du div.
   */
  public static Boolean isDivOuvert(String idDiv, HttpServletRequest request) {
    HashMap<String,Boolean> repertoire = getRepertoireDiv(request);
    if (repertoire != null && repertoire.get(idDiv) != null) {
      return (Boolean)getRepertoireDiv(request).get(idDiv);
    }else {
      return Boolean.FALSE;
    }

  }
  
  public static Boolean isDivInitialise(String idDiv, HttpServletRequest request) {
    HashMap<String,Boolean> repertoire = getRepertoireDiv(request);
    if (repertoire != null && repertoire.get(idDiv) != null) {
      return Boolean.TRUE;
    }else {
      return Boolean.FALSE;
    }
  }

  /**
   * Fermer un DIV.
   * @param request la requête en traitement.
   * @param idDiv l'identifiant du div.
   */
  public static void fermerDiv(String idDiv, HttpServletRequest request) {
    getRepertoireDiv(request).remove(idDiv);
    getRepertoireDiv(request).put(idDiv, Boolean.FALSE);
  }

  /**
   * Ouvrir un DIV.
   * @param request la requête en traitement.
   * @param idDiv l'identifiant du div.
   */
  public static void ouvrirDiv(String idDiv, HttpServletRequest request) {
    getRepertoireDiv(request).remove(idDiv);
    getRepertoireDiv(request).put(idDiv, Boolean.TRUE);
  }

  /**
   * Répertoire des DIV utilisé par l'utilisateur avec état ouvert ou fermé.
   * @return le répertoire des DIV utilisé par l'utilisateur avec état ouvert ou fermé.
   * @param request la requête en traitement.
   */
  public static HashMap getRepertoireDiv(HttpServletRequest request) {
    HashMap ensembleDivOuvert =
        (HashMap)request.getSession().getAttribute("tableau_div_persistant");

    if (ensembleDivOuvert == null) {
      ensembleDivOuvert = new HashMap();

      request.getSession().setAttribute("tableau_div_persistant",
          ensembleDivOuvert);
    }

    return ensembleDivOuvert;
  }

  /**
   * Retourne le certificat d'authentification unique en cours.
   * @return le certificat d'authentification unique en cours.
   * @param request la requête en traitement.
   */
  public static String getCertificat(HttpServletRequest request) {
    String certificat =
        (String)request.getSession().getAttribute(Constantes.CERTIFICAT_AUTHENTIFICATION);

    if (certificat == null) {
      // Consulter dans le cookie
      Cookie cookie =
          UtilitaireRequest.getCookie(request, GestionSecurite.getInstance().getNomCookieCertificat());

      if ((cookie != null) && log.isInfoEnabled()) {
        log.debug("La valeur du cookie d'authentification est:" +
            cookie.getValue());
      }

      if (cookie != null) {
        certificat = cookie.getValue();
      }
    }

    return certificat;
  }

  /**
   * Spécifie si l'utilisateur accède à un composant qui nécessiste
   * la lecture seulement.
   * <p>
   * @since SOFI 2.0.2
   */
  public static boolean isAccesEnLectureSeulement(HttpServletRequest request) {
    // Accès à l'action courante.
    String urlCourant = UtilitaireControleur.getAdresseActionAffiche(request);

    // Acces à l'utilisateur.
    Utilisateur utilisateur = getUtilisateur(request.getSession());

    // Accès à la liste des objets associés à l'adresse courant.
    List listeObjetSecurisables =
        (List)utilisateur.getListeObjetSecurisablesParAdresse().get(urlCourant);

    // Accès la l'objet directement associé à l'url courant.
    ObjetSecurisable objetSecurisableSecure =
        GestionSecurite.getInstance().getObjetSecurisableParAdresse(urlCourant,
            listeObjetSecurisables);

    if ((objetSecurisableSecure != null) &&
        objetSecurisableSecure.isLectureSeulement()) {
      return true;
    } else {
      return false;
    }
  }

  public static String getAdresseActionAffiche(HttpServletRequest request) {
    return (String)request.getSession().getAttribute(Constantes.ADRESSE_ACTION_AFFICHE);
  }
}
