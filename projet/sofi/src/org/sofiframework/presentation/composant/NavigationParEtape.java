/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.composant;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe d'aide pour faire la gestion de la navigation de formulaire
 * étape par étape.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.8
 */
public class NavigationParEtape extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -2028749516124167869L;
  private String urlPrecedent;
  private String urlSuivant;
  private String urlEnregistrer;
  private String urlQuitter;
  private String libelleUrlPrececent;
  private String libelleUrlSuivant;
  private String libelleUrlEnregistre;
  private String libelleQuitter;
  private boolean precedentInactifSiFormulaireLectureSeulement;
  private boolean suivantInactifSiFormulaireLectureSeulement;
  private String libelleUrlPrecedentLectureSeulement;
  private String libelleUrlSuivantLectureSeulement;

  public void initialiser(String libelleUrlPrecedent,
      String libelleUrlEnregistre, String libelleUrlSuivant) {
  }

  public void setUrlPrecedent(String urlPrecedent) {
    this.urlPrecedent = urlPrecedent;
  }

  public String getUrlPrecedent() {
    return urlPrecedent;
  }

  public void setUrlSuivant(String urlSuivant) {
    this.urlSuivant = urlSuivant;
  }

  public String getUrlSuivant() {
    return urlSuivant;
  }

  public void setLibelleUrlPrececent(String libelleUrlPrececent) {
    this.libelleUrlPrececent = libelleUrlPrececent;
  }

  public String getLibelleUrlPrececent() {
    return libelleUrlPrececent;
  }

  public void setLibelleUrlSuivant(String libelleUrlSuivant) {
    this.libelleUrlSuivant = libelleUrlSuivant;
  }

  public String getLibelleUrlSuivant() {
    return libelleUrlSuivant;
  }

  public void setPrecedentInactifSiFormulaireLectureSeulement(
      boolean precedentInactifSiFormulaireLectureSeulement) {
    this.precedentInactifSiFormulaireLectureSeulement = precedentInactifSiFormulaireLectureSeulement;
  }

  public boolean isPrecedentInactifSiFormulaireLectureSeulement() {
    return precedentInactifSiFormulaireLectureSeulement;
  }

  public void setSuivantInactifSiFormulaireLectureSeulement(
      boolean suivantInactifSiFormulaireLectureSeulement) {
    this.suivantInactifSiFormulaireLectureSeulement = suivantInactifSiFormulaireLectureSeulement;
  }

  public boolean isSuivantInactifSiFormulaireLectureSeulement() {
    return suivantInactifSiFormulaireLectureSeulement;
  }

  public void setLibelleUrlEnregistre(String libelleUrlEnregistre) {
    this.libelleUrlEnregistre = libelleUrlEnregistre;
  }

  public String getLibelleUrlEnregistre() {
    return libelleUrlEnregistre;
  }

  public void setUrlQuitter(String urlQuitter) {
    this.urlQuitter = urlQuitter;
  }

  public String getUrlQuitter() {
    return urlQuitter;
  }

  public void setLibelleQuitter(String libelleQuitter) {
    this.libelleQuitter = libelleQuitter;
  }

  public String getLibelleQuitter() {
    return libelleQuitter;
  }

  public void setUrlEnregistrer(String urlEnregistrer) {
    this.urlEnregistrer = urlEnregistrer;
  }

  public String getUrlEnregistrer() {
    return urlEnregistrer;
  }

  public void setLibelleUrlPrecedentLectureSeulement(
      String libelleUrlPrecedentLectureSeulement) {
    this.libelleUrlPrecedentLectureSeulement = libelleUrlPrecedentLectureSeulement;
  }

  public String getLibelleUrlPrecedentLectureSeulement() {
    return libelleUrlPrecedentLectureSeulement;
  }

  public void setLibelleUrlSuivantLectureSeulement(
      String libelleUrlSuivantLectureSeulement) {
    this.libelleUrlSuivantLectureSeulement = libelleUrlSuivantLectureSeulement;
  }

  public String getLibelleUrlSuivantLectureSeulement() {
    return libelleUrlSuivantLectureSeulement;
  }
}
