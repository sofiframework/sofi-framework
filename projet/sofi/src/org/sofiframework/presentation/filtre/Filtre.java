/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Filtre qui possède une référence au contexte "Root" Spring.
 * 
 * Ce filtre permet aussi d'utiliser des expressions régulières pour effectuer
 * ou non le traitement du filtre.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class Filtre implements Filter {

  /**
   * Cache pour les patterns d'expressions.
   */
  HashMap patterns = new HashMap();

  /**
   * Contexte d'application en cours
   */
  private WebApplicationContext contexte = null;

  /**
   * Permet de filtrer en excluant les urls qui matchent les expressions
   * spécifiées. Chanque expression doit être séparée par des vigules
   */
  private String exclureExpressions = null;

  /**
   * Permet de filtrer en incluant seulement les urls qui matchent les
   * expressions spécifiées. Chanque expression doit être séparée par des
   * vigules
   */
  private String inclureExpressions = null;

  /**
   * Initialisation
   */
  @Override
  public void init(FilterConfig configuration) throws ServletException {
    if (configuration != null) {
      exclureExpressions = configuration.getInitParameter("exclureExpressions");
      inclureExpressions = configuration.getInitParameter("inclureExpressions");
    }
    this.initContexte(configuration.getServletContext());
  }

  /**
   * Effectuer le traitement du filtre
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response,
      FilterChain chaine) throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    // On doit matcher les includes et ne pa matcher les excludes
    // --> Si on spécifie pas de exclude on exclus rien
    // --> Si on spécifie pas de include on inclus tout
    if (match(httpRequest, inclureExpressions, true)
        && !match(httpRequest, exclureExpressions, false)) {
      this.doFilter(httpRequest, httpResponse, chaine);
    } else {
      chaine.doFilter(request, response);
    }
  }

  /**
   * Valide si l'url de la requête HTTP possède une séquence qui match les
   * expressions.
   * 
   * @param request
   *          Requête HTTP
   * @param expressions
   *          Liste d'expressions régulières
   * @param defaut
   *          Si la liste des expressions est null retour cette valeur comme
   *          défaut.
   * 
   * @return true si il y a correspondance entre une des expressions et l'url,
   *         sinon false
   */
  private boolean match(HttpServletRequest request, String expressions,
      boolean defaut) {
    boolean match = false;

    if (expressions != null) {
      String url = request.getRequestURL().toString() + "?" + request.getQueryString();
      for (StringTokenizer tk = new StringTokenizer(expressions, ","); tk
          .hasMoreTokens()
          && !match;) {
        String expression = tk.nextToken().trim();
        match = this.match(expression, url);
      }
    } else {
      match = defaut;
    }

    return match;
  }

  /**
   * Valide si il y a correspondance entre une expression et la chaîne de
   * caractère visée. Effectue un "find" de l'expression dans la chaîne.
   * 
   * @param expression
   *          Expression régulière
   * @param sujet
   *          sujet de la recherche
   * @return Si une portion de la chaîne correxpond à l'expression
   */
  private boolean match(String expression, String sujet) {
    Pattern p = (Pattern) this.patterns.get(expression);
    if (p == null) {
      p = Pattern.compile(expression);
      this.patterns.put(expression, p);
    }

    return p.matcher(sujet).find();
  }

  /**
   * Méthode qui doit être implémentée par les filtres enfant.
   */
  public abstract void doFilter(HttpServletRequest request,
      HttpServletResponse response, FilterChain chaine) throws IOException,
      ServletException;

  /**
   * Nettoyage des ressources utilisées par le filtre.
   */
  @Override
  public void destroy() {
    this.contexte = null;
  }

  /**
   * Obtenir le contexte Spring
   * 
   * @param servletContexte
   *          Contexte Servlet
   */
  private void initContexte(ServletContext servletContexte) {
    this.contexte = WebApplicationContextUtils
        .getWebApplicationContext(servletContexte);
  }

  /**
   * Obtenir le contexte
   * 
   * @return Contexte Spring
   */
  public WebApplicationContext getContexte() {
    return contexte;
  }

  /**
   * Obtenir le Bean par son nom.
   * 
   * @param nom
   *          Nom du bean
   * @return Bean recherché
   */
  public Object getBean(String nom) {
    return this.getContexte().getBean(nom);
  }

  /**
   * Expressions qui permettrons d'exclure certains URLs du traitement du
   * filtre.
   * 
   * @return expressions Expressions
   */
  public String getExclureExpressions() {
    return exclureExpressions;
  }

  /**
   * Fixer les expressions qui permettrons d'exclure certains URLs du traitement
   * du filtre.
   * 
   * @param exclureExpressions
   *          expressions régulières
   */
  public void setExclureExpressions(String exclureExpressions) {
    this.exclureExpressions = exclureExpressions;
  }

  /**
   * Expressions qui permettrons d'appliquer le filtre seulement aux URLs qui
   * correspondent à une des expression spécifiées.
   * 
   * @return Liste d'expressions
   */
  public String getInclureExpressions() {
    return inclureExpressions;
  }

  /**
   * Fixer la liste des expressions qui permettrons d'appliquer le filtre
   * seulement aux URLs qui correspondent à une des expression spécifiées.
   * 
   * @param inclureExpressions
   *          liste d'expressions
   */
  public void setInclureExpressions(String inclureExpressions) {
    this.inclureExpressions = inclureExpressions;
  }

  public static void main(String[] args) {
    String url = "http://localhost:8080/java.ajax4jsf/aaabb.faces";
    Pattern p = Pattern.compile(".faces");
    Matcher m = p.matcher(url);
    System.out.print("match : " + m.find());
  }
}
