/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Filtre qui permet d'extraire l'information inclut dans le Http Header de
 * Apache lorsque l'utilisation d'un reverse proxy.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 3.0
 */
public class FiltreHttpHeaderApache implements Filter {

  /** Variable désignant le fichier de log */
  private static Log log = LogFactory
      .getLog(org.sofiframework.presentation.filtre.FiltreHttpHeaderApache.class);

  /**
   * Applique le filtre.
   * <p>
   * 
   * @param pRequest
   *          l'objet request de la requête en cours
   * @param pResponse
   *          la réponse qui sera acheminée à l'utilisateur suite au traitement
   *          de la requête
   * @param pChain
   *          l'objet FilterChain à utiliser
   * @throws java.io.IOException
   *           Exception générique
   * @throws javax.servlet.ServletException
   *           Exception générique
   */
  @Override
  public void doFilter(ServletRequest pRequest, ServletResponse pResponse,
      FilterChain pChain) throws IOException, ServletException {
    if (pRequest instanceof HttpServletRequest) {
      pChain.doFilter(new FiltreHttpHeaderApache.IpChangeRequest(
          (HttpServletRequest) pRequest), pResponse);
    } else {
      pChain.doFilter(pRequest, pResponse);
    }
  }

  /**
   * Méthode qui sert à initialiser le filtre.
   * <p>
   * 
   * @param filterConfig
   *          la configuration du filtre
   */
  @Override
  public void init(FilterConfig filterConfig) {
  }

  /**
   * Méthode qui sert à détruire le filtre.
   */
  @Override
  public void destroy() {
  }

  /**
   * Classe interne qui permet d'extraire l'IP de l'utilisateur via le HTTP
   * Header de Apache.
   * 
   * @author Jean-François Brassard (Nurun inc.)
   * @version 3.0
   */
  private class IpChangeRequest extends HttpServletRequestWrapper {
    /** Constructeur par défaut */
    public IpChangeRequest(HttpServletRequest req) {
      super(req);
    }

    /**
     * Méthode qui sert à retourver l'adresse IP du client.
     * <p>
     * 
     * @return l'adresse IP du client
     */
    @Override
    public String getRemoteAddr() {

      String clientIp = getHeader("True-Client-IP");

      if (StringUtils.isEmpty(clientIp)) {

        clientIp = getHeader("x-forwarded-for");

        if (StringUtils.isEmpty(clientIp)) {
          clientIp = super.getRemoteAddr();
        }

      }

      return clientIp;
    }
  }
}
