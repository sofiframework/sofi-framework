/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;

/**
 * Filtre qui permet de vérifier si la session en cours est toujours cohérente
 * par rapport aux certificat présent dans le cookie d'authentification et de
 * paramètre de requête.
 * 
 * Dans le cas ou le certificat dans la session n'est plus à jour on doit
 * invalider la session. Ceci permet de recharger le données utilisateur à neuf.
 * Cette vérification est importante car elle permet de ne jamais se retrouveer
 * avec des données d'un autre utilisateur dans le cas ou des postes de travail
 * sont partagées et que l'utilisateur ne quitte pas correctement par un quitter
 * (logout).
 * 
 * @author Jean-Maxime Pelletier
 */
public class FiltreCertificat extends Filtre {

  @Override
  public void doFilter(HttpServletRequest request,
      HttpServletResponse response, FilterChain chaine) throws IOException,
      ServletException {

    /*
     * si on a changé de certificat par la requête ou par un cookie on doit
     * refaire l'authentification
     */
    if (this.isChangementCertificatCookie(request)
        || this.isChangementCertificatRequete(request)) {
      request.getSession().invalidate();
    }

    chaine.doFilter(request, response);
  }

  /**
   * Est-ce que le certificat est toujours le même entre celui dans la session
   * et en paramètre de requête.
   * 
   * @param request
   *          Requête HTTP
   * @return Valeur booléenne
   */
  protected Boolean isChangementCertificatRequete(HttpServletRequest request) {
    String nomCookie = GestionSecurite.getInstance().getNomCookieCertificat();
    String certificatRequete = request.getParameter(nomCookie);
    return this.isCertificatDifferent(certificatRequete, request);
  }

  /**
   * Est-ce que le certificat dans la session est toujours le même que celui qui
   * est dans le cookie.
   * 
   * @param request
   *          Requête HTTP
   * @return Valeur booléenne
   */
  protected Boolean isChangementCertificatCookie(HttpServletRequest request) {
    String certificatCookie = this.getCertificatCookie(request);
    return this.isCertificatDifferent(certificatCookie, request);
  }

  /**
   * Est-ce que le certificat fourni en entrée est le même que celui présent
   * dans la session.
   * 
   * @param certificat
   *          Certificat d'authentification
   * @param request
   *          Requête HTTP
   * @return Valeur booléenne
   */
  protected Boolean isCertificatDifferent(String certificat,
      HttpServletRequest request) {
    String certificatEnCours = (String) request.getSession().getAttribute(
        Constantes.CERTIFICAT_AUTHENTIFICATION);
    boolean different = false;
    if (certificatEnCours != null && certificat != null
        && !certificatEnCours.equals(certificat)) {
      different = true;
    }
    return different;
  }

  /**
   * Obtenir le cookie d'authentification.
   * 
   * @param request
   *          Requête HTTP
   * @return Cookie d'auehentification
   */
  protected Cookie getCookie(HttpServletRequest request) {
    Cookie cookie = null;
    String nomCookie = GestionSecurite.getInstance().getNomCookieCertificat();
    // Vérifier si le cookie existe
    if (nomCookie != null) {
      cookie = UtilitaireRequest.getCookie(request, nomCookie);
    }
    return cookie;
  }

  /**
   * Obtenir le certificat qui se trouve dans le cookie d'authentification.
   * 
   * @param request
   *          Requête HTTP
   * @return Certificat d'authentification
   */
  protected String getCertificatCookie(HttpServletRequest request) {
    Cookie cookie = UtilitaireRequest.getCookie(request, GestionSecurite
        .getInstance().getNomCookieCertificat());
    return cookie != null ? cookie.getValue() : null;
  }
}