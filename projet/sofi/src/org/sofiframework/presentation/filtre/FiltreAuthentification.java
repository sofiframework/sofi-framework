/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.parametresysteme.UtilitaireParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.presentation.filtre.exception.AuthentifierErreurException;
import org.sofiframework.presentation.filtre.exception.AuthentifierUtilisateurException;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.presentation.utilitaire.UtilitaireApache;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Filtre qui véfifie si l'utilisateur est présentement authentifier
 * l'application. Si l'utilisateur n'est pas présentement authentifié, il doit
 * retourner à une page d'authentification.
 * <p>
 * Vous devez le configurer dans le fichier web.xml, voici un exemple:<br>
 * <code>
 * &lt;filter&gt;<br>
 * &nbsp;&nbsp;&lt;filter-name&gt;FiltreAuthentification&lt;/filter-name&gt;<br>
 * &nbsp;&nbsp;&lt;filter-class&gt;org.sofiframework.filtre.FiltreAuthentification&lt;/filter-class&gt;<br>
 * </code>
 * <p>
 * Si vous utilisez Oracle SSO, initialiser le paramètre suivants:<br>
 * <code>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name&gt;oracleSSO&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value&gt;false&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * </code>
 * <p>
 * Ce paramètre est obligatoire, vous devez spécifiez quel attribut sert pour
 * identifier<br>
 * un utilisateur authentifié.<br>
 * <code>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name&gt;cleUtilisateur&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value&gt;codeUtilisateur&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * </code>
 * <p>
 * Si vous utilisez une authentification personnalisé initialiser ce paramètre:<br>
 * <code>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name&gt;url&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value&gt;/portail/authentification.do?methode=authentification&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name>nomParamUrlRetour&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value>urlRetour&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name>nomCookie&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value>gsa_login_meq&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name>nomServeur&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value>URL_RETOUR&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name>port&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value>7777&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &nbsp;&nbsp;&lt;init-param&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-name>codeUtilisateurTest&lt;/param-name&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;param-value>test&lt;/param-value&gt;<br>
 * &nbsp;&nbsp;&lt;/init-param&gt;<br>
 * &lt;/filter&gt;
 * </code>
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FiltreAuthentification implements Filter {
  public static final String CAUSE_CERTIFICAT_INVALIDE = "certificat_invalide";
  public static final String CAUSE_AUCUN_UTILISATEUR = "aucun_utilisateur";

  /** Variable désignant le fichier de log */
  private static Log log = LogFactory
      .getLog(org.sofiframework.presentation.filtre.FiltreAuthentification.class);

  /** la configuration du filtre */
  private FilterConfig config = null;

  /** variable indiquant si le lien exécuté est sécure ou pas */
  private Boolean secure = null;

  /** l'url à appeler pour effectuer l'authentification */
  private String urlAuthentification;

  /** variable indiquant la présence de SSO d'Oracle */
  private boolean oracleSSO = false;

  /** le nom du serveur */
  private String nomServeur;

  /** le port de communication avec le serveur */
  private String port;

  /** le nom du paramètre qui spécifie l'adresse de retour */
  private String nomParamUrlRetour;

  /**
   * le nom du cookie à ajouter à l'utilisateur pour prouver son
   * authentification
   */
  private String nomCookie;

  /** le code utilisateur pour des fins de test */
  private String codeUtilisateurTest;

  /**
   * Domaine dont le cookie sera accessible.
   */
  private String domaineCookie;

  /**
   * La liste des domaines dont le cookie sera accessible.
   */
  private ArrayList listeDomaineCookie;

  /**
   * Permet de spécifier un age maximal de vie du cookie.
   */
  private Integer ageCookie;

  /**
   * Retourne si le serveur d'authentifcation est Oracle SSO.
   * <p>
   * 
   * @return true si Oracle SSO est utilisé.
   */
  public boolean isOracleSSO() {
    return oracleSSO;
  }

  /**
   * Fixer si le serveur d'authentification est Oracle SSO.
   * <p>
   * 
   * @param oracleSSO
   *          valeur indiquant si le serveur d'authentification est Oracle SSO
   */
  public void setIsOracleSSO(boolean oracleSSO) {
    this.oracleSSO = oracleSSO;
  }

  /**
   * Retourne le nom du serveur d'authentification.
   * <p>
   * 
   * @return le nom du serveur d'authentification
   */
  public String getNomServeur() {
    return nomServeur;
  }

  /**
   * Fixer le nom du serveur d'authentification.
   * <p>
   * 
   * @param nomServeur
   *          le nom du serveur d'authentification
   */
  public void setNomServeur(String nomServeur) {
    this.nomServeur = nomServeur;
  }

  /**
   * Retourne le numéro de port du serveur d'authentification.
   * <p>
   * 
   * @return le numéro de port du serveur d'authentification
   */
  public String getPort() {
    return port;
  }

  /**
   * Fixer le numéro de port du serveur d'authentification.
   * <p>
   * 
   * @param port
   *          le numéro de port du serveur d'authentification
   */
  public void setPort(String port) {
    this.port = port;
  }

  /**
   * Retourne le nom du paramètre qui spécifie l'adresse de retour.
   * <p>
   * 
   * @return le nom du paramètre qui spécifie l'adresse de retour
   */
  public String getNomParamUrlRetour() {
    return nomParamUrlRetour;
  }

  /**
   * Fixer le nom du paramètre qui spécifie l'adresse de retour.
   * <p>
   * 
   * @param nomParamUrlRetour
   *          le nom du paramètre qui spécifie l'adresse de retour
   */
  public void setNomParamUrlRetour(String nomParamUrlRetour) {
    this.nomParamUrlRetour = nomParamUrlRetour;
  }

  /**
   * Retourne le nom du cookie qui contient la référence sur l'authentifiant de
   * l'utilisateur.
   * 
   * @return le nom du cookie utilisé pour l'authentification
   */
  public String getNomCookie() {
    return nomCookie;
  }

  /**
   * Fixer le nom du cookie qui est contenu la référence sur l'authentifiant de
   * l'utilisateur.
   * 
   * @param nomCookie
   *          le nom du cookie utilisé pour l'authentification
   */
  public void setNomCookie(String nomCookie) {
    this.nomCookie = nomCookie;
  }

  /**
   * Retourne un code utilisateur de test, afin de permettre au développeur de
   * tester son application sans passer par une page d'authentification.
   * 
   * @return un code utilisateur de test
   */
  public String getCodeUtilisateurTest() {
    String codeUtilisateur = GestionParametreSysteme.getInstance().getString(
        ConstantesParametreSysteme.CODE_UTILISATEUR_TEST);

    if (!codeUtilisateur.equals("")) {
      return codeUtilisateur;
    } else {
      return codeUtilisateurTest;
    }
  }

  /**
   * Fixer un code utilisateur de test, afin de permettre au développeur de
   * tester son application sans passer par une page d'authentification.
   * 
   * @param codeUtilisateurTest
   *          un code utilisateur de test
   */
  public void setCodeUtilisateurTest(String codeUtilisateurTest) {
    this.codeUtilisateurTest = codeUtilisateurTest;
  }

  /**
   * Retourne le nom du domaine dont le cookie peut être accéder.
   * 
   * @return le nom du domaine dont le cookie peut être accéder.
   */
  public String getDomaineCookie() {
    return domaineCookie;
  }

  /**
   * Fixer le nom du domaine dont le cookie peut être accéder.
   * 
   * @param domaineCookie
   *          le nom du domaine dont le cookie peut être accéder.
   */
  public void setDomaineCookie(String domaineCookie) {
    this.domaineCookie = domaineCookie;
  }

  /**
   * Compare la valeur de certificat de la requête avec celle déjà présente dans
   * la session. On doit toujours garder la version qui provient de la requête
   * si elle est différente de la session en cours.
   * 
   * @param req
   * @param resp
   */
  protected void validerCertificat(ServletRequest req, ServletResponse resp) {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) resp;

    Cookie cookie = this.getCookie(request);
    String certificatCookie = (cookie != null) ? cookie.getValue() : null;
    String certificatRequest = null;

    if (getNomCookie() != null) {
      certificatRequest = request.getParameter(getNomCookie());
    }

    if (certificatCookie != null || certificatRequest != null) {
      String certificatSession = (String) request.getSession().getAttribute(
          Constantes.CERTIFICAT_AUTHENTIFICATION);

      // Si il a une session en cours, on doit regarder si il y a un changement
      // et recharger tout
      if (certificatSession != null) {
        // Le bon certificat est toujours celui du cookie ou de la request
        String certificat = (certificatRequest != null) ? certificatRequest
            : certificatCookie;
        if (certificat != null) {
          Boolean nouveauCertificat = !certificat.equals(certificatSession);
          if (nouveauCertificat) {
            request.getSession().setAttribute(
                Constantes.NOUVELLE_AUTHENTIFICATION, Boolean.TRUE);
            request.getSession().removeAttribute(Constantes.UTILISATEUR);
            request.getSession().removeAttribute(
                Constantes.CERTIFICAT_AUTHENTIFICATION);
            request.getSession().removeAttribute(
                Constantes.CODE_UTILISATEUR_SESSION);
          }
        }
      }
    }
  }

  /**
   * Applique le filtre authentification.
   * <p>
   * 
   * @param req
   *          l'objet request de la requête en cours
   * @param resp
   *          la réponse qui sera acheminée à l'utilisateur suite au traitement
   *          de la requête
   * @param chain
   *          l'objet FilterChain à utiliser
   * @throws java.io.IOException
   *           Exception générique
   * @throws javax.servlet.ServletException
   *           Exception générique
   */
  @Override
  public void doFilter(ServletRequest req, ServletResponse resp,
      FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;

    this.validerCertificat(req, resp);

    HttpServletResponse response = (HttpServletResponse) resp;

    String certificat = (String) request.getSession().getAttribute(
        Constantes.CERTIFICAT_AUTHENTIFICATION);

    boolean quitterSession = false;

    // Le nom du paramètre désignant la méthode.
    String methodeParam = UtilitaireParametreSysteme.getMethodeParam();

    // L'url en traitement.
    String url = request.getRequestURI();

    // La méthode en traitement.
    String methode = request.getParameter(methodeParam);

    // Déterminer le nom du paramètre pour quitter l'application
    String paramQuitter = this.getParametreQuitter();
    
    String paramCertificat = request.getParameter(getNomCookie());

    // Est-ce un url securisé, si ou vérifie si un utilisateur est authentifié
    boolean urlSecurise = false;

    // Est-ce un action de fermeture de session qui est demandé.
    boolean actionQuitter = false;

    String locale = null;

    if (UtilitaireSession.getLocale(request.getSession()) != null) {

      locale = UtilitaireSession.getLocale(request.getSession()).toString();
    }

    if (url.indexOf(".do") != -1) {
      // Besoint d'etre une application Struts sinon on continue
      urlSecurise = UtilitaireControleur.isActionSecurise(request);
      actionQuitter = ((url.indexOf("authentification") != -1)
          && (methode != null) && methode.equals(paramQuitter));
    }

    // Est-ce qu'il y a un certificat de reçu en paramètre, si oui, on doit
    // créer initialement le cookie.
    boolean certificatPresent = false;

    if (getNomCookie() != null) {
      certificatPresent = paramCertificat != null;

      if ((paramCertificat != null)) {
        // Si certificat valide alors on le spécifie dans un cookie.
        ajouterCookies(request.getParameter(getNomCookie()), request, response);
      }
    }

    if (paramCertificat != null || urlSecurise || actionQuitter) {
      try {
        this.traitementAvantFiltre(req, resp);

        String adresseUrlAuthentification = GestionParametreSysteme
            .getInstance().getString(
                ConstantesParametreSysteme.URL_AUTHENTIFICATION);
        String actionCourante = request.getRequestURI();

        Utilisateur utilisateur = null;
        Cookie cookie = null;
        String identifiant = null;
        

        if (certificat != null) {
          identifiant = certificat;
        }        

        // Est-ce qu'il y a accès à l'url d'authentification.
        boolean accesUrlAuthentification = (!UtilitaireString
            .isVide(adresseUrlAuthentification))
            && (actionCourante.indexOf(adresseUrlAuthentification) != -1);

        // S'il ne s'agit pas d'une nouvelle authentification, on récupère
        // l'utilisateur de la session
        if ((request.getSession().getAttribute(
            Constantes.NOUVELLE_AUTHENTIFICATION) == null)
            && !accesUrlAuthentification
            && (request.getParameter(Constantes.NOUVELLE_AUTHENTIFICATION) == null)) {
          utilisateur = UtilitaireControleur.getUtilisateur(request
              .getSession());

          if (utilisateur != null) {
            this.traitementUtilisateurExiste(utilisateur, req, resp);
          }
        }

        if (!isOracleSSO()) {
          cookie = this.getCookie(request);

          if (cookie != null && cookie.getMaxAge() == 0) {
            cookie = null;
          }

          if ((cookie != null) && (cookie.getValue() != null)) {
            if (log.isInfoEnabled() && utilisateur == null) {
              log.info("Authentification via cookie : " + cookie.getValue()
                  + " - age : " + cookie.getMaxAge());
            }
          }
        } else {
          if (!accesUrlAuthentification) {
            // Traitement si le serveur d'authentification propriétaire Oracle
            // SSO.
            if (log.isWarnEnabled()) {
              log.info("Traitement Oracle SSO");
            }

            try {
              if (request.getRemoteUser() != null) {
                identifiant = request.getRemoteUser().toLowerCase();
              } else {
                if (this.getCodeUtilisateurTest() != null) {
                  identifiant = this.getCodeUtilisateurTest();
                } 
              }

              if (log.isWarnEnabled()) {
                log.info("Authentification SSO de l'utilisateur: "
                    + identifiant);
              }
            } catch (Exception e) {
              identifiant = null;
            }

            // Si l'usager n'est pas authentifié, demander la page
            // d'identification
            if ((identifiant == null) || identifiant.equals("")) {
              traitementAppelOracleSSO(response, getUrlRetourCourant(request));

              return;
            }
          }
        }

		if (identifiant == null && request.getSession().getAttribute(
		         Constantes.CODE_UTILISATEUR_SESSION) != null) {
		       identifiant = (String) request.getSession().getAttribute(
		           Constantes.CODE_UTILISATEUR_SESSION);
		}
		
        // Vérifier si l'utilisateur ferme la session, si oui
        // supprimer le cookie d'authentification.
        if ((url.indexOf("authentification") != -1) && (methode != null)
            && methode.equals(paramQuitter)) {
          if (log.isWarnEnabled()) {
            log.info("Fermeture de session de l'utilisateur: " + identifiant);
          }

          // Indicateur de fermeture de session.
          quitterSession = true;

          request.getSession().removeAttribute(
              Constantes.CODE_UTILISATEUR_SESSION);
          request.getSession().removeAttribute(Constantes.CERTIFICAT);

          if (utilisateur != null) {
            traitementQuitter(utilisateur, req, resp);
          }

          if (isOracleSSO()) {
            traitementQuitterOracleSSO(response,
                getUrlRetourPourPageAccueil(request));

            return;
          }
        }

        // Si l'utilisateur n'est pas dans la session et qu'on possède ce qu'il
        // faut pour l'avoir,
        // alors on va le chercher avec le serviceSecurité et on le place dans
        // la session
        boolean utilisateurInexistant = ((utilisateur == null) || (utilisateur
            .getCodeUtilisateur() == null));

        Utilisateur utilisateurTemporaire = null;

        // Fixer l'utilisteur authentifié mais non abonné.
        if ((utilisateur != null) && (utilisateur.getCodeUtilisateur() == null)) {
          utilisateurTemporaire = utilisateur;
          utilisateur = null;
          UtilitaireControleur.ajouterUtilisateurDansSession(request,
              utilisateur);
        }

        // Est-ce un utilisateur test spécifie dans le web.xml
        boolean codeUtilisateurTest = !UtilitaireString
            .isVide(getCodeUtilisateurTest());

        // Fixer l'identifiant avec le code utilisateur dans la session s'il y
        // lieu.
        if (request.getSession().getAttribute(
            Constantes.CODE_UTILISATEUR_SESSION) != null) {
          identifiant = (String) request.getSession().getAttribute(
              Constantes.CODE_UTILISATEUR_SESSION);
        }

        boolean cookieCertificat = ((cookie != null) && !UtilitaireString
            .isVide(cookie.getValue()));

        Boolean certificatValide = null;

        if (!isOracleSSO()) {
          if (codeUtilisateurTest) {
            identifiant = getCodeUtilisateurTest();
          } else {
            /* Si on doit créer le ou les cookies */
            if ((cookieCertificat || certificatPresent) && (identifiant == null)) {

              /* Si le cookie a un nom */
              if ((request.getParameter(getNomCookie()) != null)) {
                identifiant = request.getParameter(getNomCookie());

                request.setAttribute(getNomCookie(), identifiant);
              } else {
                identifiant = getValeurCertificat(cookie.getValue());
              }

              if (!codeUtilisateurTest) {
                // Vérifier si le certificat est valide.
                certificatValide = isCertificatValide(identifiant);
              }

              if ((certificatValide != null)
                  && !certificatValide.booleanValue()) {
                // Si le certificat n'est pas bon, retourner à la page
                // d'authentification.
                throw new AuthentifierUtilisateurException(
                    "Le certificat n'est pas valide.", quitterSession);
              }

              /*
               * Il est important de vérifier si le certificat a changé. Si oui,
               * il s'agit d'une nouvelle authentification.
               */
              if (identifiant != null) {
                if (certificat != null && !identifiant.equals(certificat)) {
                  if (log.isInfoEnabled()) {
                    log.info("Certificat actuel : " + certificat);
                    log.info("Certificat invalide! Nouvelle authentification avec le certificat suivant : "
                        + identifiant);
                  }
                  request.getSession().invalidate();
                  utilisateurInexistant = true;
                }
              }

              // Conserver le certificat dans la session.
              request.getSession().setAttribute(
                  Constantes.CERTIFICAT_AUTHENTIFICATION, identifiant);
            }
          }

          // Traitement lors de la fermeture de la session utilisateur.
          if (quitterSession && !isIgnorerFermetureSession()
              && !codeUtilisateurTest) {
            detruireCookies(cookie, identifiant, request, response);
          }
        } else {
          if (codeUtilisateurTest) {
            identifiant = getCodeUtilisateurTest();
          }
        }

        if (utilisateurInexistant && !accesUrlAuthentification
            && !quitterSession && ((identifiant != null) || isOracleSSO())) {
          if (GestionSecurite.getInstance().isSecuriteActif()) {
            // Traitement exclusivement pour Oracle SSO si l'authentification
            // force l'accès par la page d'accueil
            if (isOracleSSO()
                && (GestionSecurite.getInstance().getComposantMenuAccueil() != null)
                && GestionParametreSysteme
                .getInstance()
                .getBoolean(
                    ConstantesParametreSysteme.AUTHENTIFICATION_VIA_PAGE_ACCUEIL_SEULEMENT)
                    .booleanValue()) {
              // Est-ce que l'appel se fait via la page d'accueil,
              // sinon appel la page d'accueil c'est c'est une fin de session.
              String pageAccueil = getUrlRetourPourPageAccueil(request);

              if (pageAccueil.indexOf(actionCourante) == -1) {
                throw new AuthentifierUtilisateurException(
                    "L'utilisateur n'est pas authentifié.", quitterSession);
              }
            }

            certificat = identifiant;

            // Spécifie si le service sécurité doit être appelé.
            boolean appelService = true;

            // Traiter si l'identifiant correspond à un code utilisateur.
            try {
              String codeUtilisateur = getCodeUtilisateur(identifiant);

              if (codeUtilisateur != null) {
                identifiant = codeUtilisateur;
              }
            } catch (Exception e) {
              if (log.isWarnEnabled()) {
                log.warn(
                    "Erreur dans l'extration du code utilisateur depuis l'identifiant : ",
                    e);
              }
              appelService = false;
            }

            // Extraire l'utilisateur via un service d'affaires de securité.
            if (appelService) {
              try {

                if (log.isInfoEnabled()) {
                  log.info("Demande de l'utilisateur pour l'identifiant : "
                      + identifiant);
                }

                utilisateur = GestionSecurite.getInstance().getUtilisateur(
                    identifiant);

                if (GestionSecurite.isApplicationGrandPublic()) {
                  List listeObjet = GestionSecurite.getInstance()
                      .getListeObjetSecurisablePourApplication();
                  utilisateur.setListeObjetSecurisables(listeObjet);
                }

                if (utilisateur != null) {
                  if (log.isInfoEnabled()) {
                    log.info("Generation des droits pour l'utilisateur : "
                        + utilisateur.getCodeUtilisateur());
                    log.info("Accès de l'adresse I.P. suivante : "
                        + request.getRemoteAddr());
                  }

                  GestionSecurite.genererAutorisationUtilisateur(utilisateur);
                }

                traitementApresGetUtilisateur(utilisateur, req, resp);

                Utilisateur utilisateurModifie = UtilitaireControleur
                    .getUtilisateur(request.getSession());

                // Si l'utilisateur a été modifié par un traitement surchargé
                // alors le fixer comme utilisateur
                // a traiter.
                if (utilisateurModifie != null) {
                  utilisateur = utilisateurModifie;
                }

                UtilitaireControleur.specifierNouvelleAuthentification(request);

                // Si ne possède pas de rôles, on doit le refuser.
                if ((utilisateur != null)
                    && ((utilisateur.getListeRole(GestionSecurite.getInstance()
                        .getCodeApplication()) == null) || (utilisateur
                            .getListeRole(
                                GestionSecurite.getInstance().getCodeApplication())
                                .size() == 0))) {
                  String messageErreur = "L'utilisateur suivant de possède pas de rôle pour s'authentifier à l'application : "
                      + identifiant;

                  if (log.isWarnEnabled()) {
                    log.warn(messageErreur);
                  }

                  if (GestionSurveillance.getInstance().isActif()) {
                    GestionSurveillance.ajouterUtilisateurDansSurveillance(
                        utilisateur, request);
                    GestionSurveillance.ajouterPageEnEchec(utilisateur,
                        messageErreur, request);
                  }

                  detruireCookies(cookie, identifiant, request, response);

                  throw new AuthentifierUtilisateurException(
                      "Aucun accès pour l'application", true);
                }
              } catch (Exception e) {
                if (!AuthentifierUtilisateurException.class.isAssignableFrom(e
                    .getClass())) {
                  throw new SOFIException(e);
                } else {
                  throw new AuthentifierUtilisateurException(
                      "Aucun accès pour l'application", true);
                }
              }
            }

            if (utilisateur != null) {
              if (log.isInfoEnabled()) {
                log.info(utilisateur.toString());
              }
            }

            if (codeUtilisateurTest && (utilisateur == null)) {
              throw new SOFIException(
                  "Le code utilisateur spécifié n'existe pas!");
            }

            if (utilisateur != null) {
              // Ajouter l'utilisateur dans la session.
              UtilitaireControleur.ajouterUtilisateurDansSession(request,
                  utilisateur);

              if (utilisateurTemporaire != null) {
                request.getSession().setAttribute("utilisateur_temporaire",
                    utilisateurTemporaire);
                modifierUtilisateurAvecUtilisateurTemporaire(request, response);
              }

              if (GestionParametreSysteme
                  .getInstance()
                  .getBoolean(
                      ConstantesParametreSysteme.AUTHENTIFICATION_UNIQUE)
                      .booleanValue()
                      && !cookieCertificat) {
                String nouveauCerfificat = genererCertificat(identifiant, request);
                ajouterCookies(nouveauCerfificat, request, response);
                certificat = nouveauCerfificat;
              }

              // Spécifier que c'est une nouvelle authentification.
              UtilitaireControleur.specifierNouvelleAuthentification(request);

              // Fixer le locale avec la langue de préférence de l'utilisateur.
              UtilitaireControleur.setLocalePourUtilisateur(request
                  .getSession());

              // @toto Traitement Oracle SSO
              // if (utilisateurInexistant && (identifiant != null) &&
              // isOracleSSO()) {
              // throw new
              // AuthentifierUtilisateurException("La session de l'utililisateur est expirée.",
              // false);
              // }
            } else {
              if (certificatValide == null || !certificatValide.booleanValue()) {
                // Si le certificat n'est pas bon, retourner à la page
                // d'authentification.
                throw new AuthentifierUtilisateurException(
                    "L'utilisateur est null.", quitterSession);
              } else {
                String cleMessage = "Vous n'avez pas accès à ce système, contactez l'administrateur du système";
                Message message = UtilitaireMessage.getMessage(cleMessage,
                    "FR", null);
                throw new SecuriteException(message.getMessage());
              }
            }
          } else {
            throw new SOFIException(
                "Vous ne pouvez pas utiliser le FiltreAuthentification (web.xml) sans avoir configué SecuritePlugIn (struts-config.xml)");
          }
        }

        /*
         * Si la sécurité est active et qu'il ne s'agit pas d'une application
         * utilisant Oracle SSO
         */
        if ((GestionSecurite.getInstance().isSecuriteActif() && !isOracleSSO())
            || quitterSession) {
          // Si l'utilisateur n'est pas dans la session et qu'on ne possède pas
          // ce qu'il faut pour l'avoir,
          // alors on appelle un serveur d'authentification distant
          if ((((utilisateur == null) || (utilisateur.getCodeUtilisateur() == null))
              && (getCodeUtilisateurTest() == null) && ((this.urlAuthentification == null) || (this.urlAuthentification
                  .indexOf(url) == -1))) || quitterSession) {
            throw new AuthentifierUtilisateurException(
                "L'utilisateur n'est pas authentifié.", quitterSession);
          }

          String codeUtilisateur = (utilisateur != null) ? utilisateur
              .getCodeUtilisateur() : getCodeUtilisateurTest();

              if (GestionParametreSysteme
                  .getInstance()
                  .getBoolean(
                      ConstantesParametreSysteme.AUTHENTIFICATION_VIA_PAGE_ACCUEIL_SEULEMENT)
                      .booleanValue()
                      && (GestionSecurite.getInstance().getComposantMenuAccueil() != null)) {
                boolean nouvelleAuthentification = UtilitaireControleur
                    .isNouvelleAuthentification(request);
                String pageAccueilSite = getUrlRetourPourPageAccueil(request);
                Href hrefAccueil = new Href(pageAccueilSite);

                if (certificatPresent) {
                  hrefAccueil.ajouterParametre(getNomCookie(), certificat);
                }

                String actionEnCours = null;
                
                try {
                  actionEnCours = UtilitaireControleur
                      .getNomActionCourant(request);
                } catch (Exception e) {
                  // TODO: handle exception
                }
                if (nouvelleAuthentification
                    && (actionEnCours == null || pageAccueilSite.indexOf(actionEnCours) == -1)) {
                  response.sendRedirect(hrefAccueil.getUrlPourRedirection());
                } else {
                  chain.doFilter(
                      new FiltreAuthentification.CodeAccesUtilisateurRequest(
                          request, codeUtilisateur), response);
                }
              } else {
                chain.doFilter(
                    new FiltreAuthentification.CodeAccesUtilisateurRequest(request,
                        codeUtilisateur), response);
              }
        } else {
          chain.doFilter(req, resp);
        }

        /*
         * On retourne l'utilisateur au serveur d'authentification si
         * l'exception est lancée
         */
      } catch (AuthentifierUtilisateurException e) {
        if (request.getParameter("ajax") == null) {
          String urlAccueil = preparerURLAuthentification(request,
              e.isQuitterSession());

          log.info("Appel de la page d'accueil suivante: " + urlAccueil);

          if (e.isQuitterSession()
              && (urlAccueil.toLowerCase().equals("") || isTraiterActionQuitter())) {
            chain.doFilter(req, resp);
          } else {
            // Si l'utilisateur doit s'authentifier
            log.info("Redirection au serveur d'authentification");
            if (locale != null) {
              Href hrefAccueil = new Href(urlAccueil);
              hrefAccueil.ajouterParametre("locale", locale);
              response.sendRedirect(hrefAccueil.getUrlPourRedirection());
            }else {
              response.sendRedirect(urlAccueil);
            }
          }
        } else {
          // Spécifier fermeture de session ajax.
          String reponseVide = "Ajax : fermeture de session";
          response.getWriter().write(reponseVide);
        }
      } catch (AuthentifierErreurException e) {
        // Appel de url qui va traitemetn l'erreur d'authentification.
        if (log.isInfoEnabled()) {
          log.info("Redirection au serveur d'authentification");
        }

        response.sendRedirect(getUrlAuthentificationErreur(request));
      }
    } else {
      chain.doFilter(req, resp);
    }
  }

  /**
   * Permet d'ajouter du traitement au filtre. Le traitement sera exécuté après
   * l'obtention d'un nouvel utilisateur.
   * 
   * @param resp
   *          requête HTTP
   * @param req
   *          réponse HTTP
   */
  protected void traitementApresGetUtilisateur(Utilisateur utilisateur,
      ServletRequest req, ServletResponse resp) {
  }

  /**
   * Permet d'appliquer un traitement personnalisé lors de la fermeture session.
   * 
   * @param resp
   *          requête HTTP
   * @param req
   *          réponse HTTP
   * @param utilisateur
   *          l'utilisateur en traitement.
   */
  protected void traitementQuitter(Utilisateur utilisateur, ServletRequest req,
      ServletResponse resp) {
    HttpServletRequest request = (HttpServletRequest) req;
    request.getSession().invalidate();
  }

  /**
   * Permet d'ajouter du traitement au filtre. Le traitement sera exécuté avant
   * tout autre action lors de l'exécution de la méthode doFilter.
   * 
   * @param resp
   *          requête HTTP
   * @param req
   *          réponse HTTP
   */
  protected void traitementAvantFiltre(ServletRequest req, ServletResponse resp)
      throws SOFIException {
  }

  /**
   * Permet d'ajouter un traitement lorsque l'utilisateur est valide et qu'il
   * possède déjà un utilisateur dans la session.
   * 
   * @throws org.sofiframework.exception.SOFIException
   *           Erreur survenu lors de traitement
   * @param resp
   *          Réponse HTTP
   * @param req
   *          Requête HTTP
   * @param utilisateur
   *          Objet Utilisateur récupéré de la session.
   */
  protected void traitementUtilisateurExiste(Utilisateur utilisateur,
      ServletRequest req, ServletResponse resp) throws SOFIException {
  }

  /**
   * Permet de modifier un utilisateur recu par le service avec un autre
   * utilisateur fixer avant l'appel du service.
   */
  protected void modifierUtilisateurAvecUtilisateurTemporaire(
      HttpServletRequest request, HttpServletResponse response)
          throws SOFIException {
  }

  /**
   * Obtenir le cookie de la requête de l'utilisateur
   * 
   * @return Objet Cookie
   * @param request
   *          Requête HTTP
   */
  protected Cookie getCookie(HttpServletRequest request) {
    Cookie cookie = null;

    // Vérifier si le cookie existe
    if (this.getNomCookie() != null) {
      cookie = UtilitaireRequest.getCookie(request, getNomCookie());

      if ((cookie != null) && log.isInfoEnabled()) {
        log.debug("le cookie est créé, la valeur est:" + cookie.getValue());
      }
    }

    return cookie;
  }

  /**
   * Ajoute un cookie pour chaque domaine spécifié à la déclaration du filtre.
   * 
   * @param response
   *          Réponse HTTP
   * @param request
   *          Requête HTTP
   * @param certificat
   *          certificat de l'utilisateur
   */
  protected void ajouterCookies(String certificat, HttpServletRequest request,
      HttpServletResponse response) {
    /*
     * Si le filtre est configuré pour supporter une groupe de domaines. On
     * ajoute un cookie pour chaque domaine
     */
    if (listeDomaineCookie != null) {
      for (Iterator iterateurListeDomaine = listeDomaineCookie.iterator(); iterateurListeDomaine
          .hasNext();) {
        String domaine = (String) iterateurListeDomaine.next();
        UtilitaireRequest.setCookie(request, response, getNomCookie(),
            this.getNouvelleValeurCookie(certificat), "/", domaine,
            getAgeCookie());
      }
    } else {
      UtilitaireRequest.setCookie(request, response, getNomCookie(),
          this.getNouvelleValeurCookie(certificat), "/", null, getAgeCookie());
    }
  }

  protected String getNouvelleValeurCookie(String certificat) {
    return certificat;
  }

  protected String getValeurCertificat(String certificat) {
    return certificat;
  }

  /**
   * Détruit les cookies et invalide le certificat de l'utilisateur
   * 
   * @param response
   *          Réponse HTTP
   * @param request
   *          Requête HTTP
   * @param certificat
   *          Certificat de l'utilisateur
   * @param cookie
   *          Cookie de l'utilisateur
   */
  protected void detruireCookies(Cookie cookie, String certificat,
      HttpServletRequest request, HttpServletResponse response) {
    if ((listeDomaineCookie != null) && (cookie != null)) {
      for (Iterator iterateurListeDomaine = listeDomaineCookie.iterator(); iterateurListeDomaine
          .hasNext();) {
        String domaine = (String) iterateurListeDomaine.next();

        cookie.setDomain(domaine);

        if (log.isInfoEnabled()) {
          log.info("Appel de suppression du cookie : " + cookie.getName()
              + " - " + cookie.getValue() + " pour le domaine :" + domaine);
        }

        // Supprime le cookie
        UtilitaireRequest.supprimerCookie(response, cookie, "/");
      }
    }

    // Termine le certificat
    try {
      terminerCertificat(certificat);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour terminer le certificat " + certificat + ".", e);
      }
    }
  }

  /**
   * Permet d'obtenir la valeur du paramètre signalant que l'utilisateur quitte
   * l'application
   * 
   * @return Nom du paramètre
   */
  protected String getParametreQuitter() {
    String paramQuitter = GestionParametreSysteme.getInstance().getString(
        "paramQuitter");

    // if (paramQuitter == null) {
    if (paramQuitter.equals("")) {
      paramQuitter = "quitter";

      // Ajouter le paramètre quitter au gestionnaire
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(
          "paramQuitter", paramQuitter);
    }

    return paramQuitter;
  }

  /**
   * Permet d'obtenir la valeur du paramètre signalant que l'on doit suivre
   * l'action quitter au lieu de l'url de retour.
   */
  protected boolean isTraiterActionQuitter() {
    Boolean traiterActionQuitter = GestionParametreSysteme.getInstance()
        .getBoolean(ConstantesParametreSysteme.TRAITER_ACTION_QUITTER);

    return traiterActionQuitter.booleanValue();
  }

  /**
   * Extrait les paramètres qui spécifie le filtre authentification.
   * <p>
   * 
   * @param filterConfig
   *          l'objet spécifiant la configuration du filtre d'authentification
   */
  @Override
  public void init(FilterConfig filterConfig) {
    this.config = filterConfig;

    String paramSecure = GestionParametreSysteme.getInstance()
        .getParametreSystemeEJB(
            ConstantesParametreSysteme.AUTHENTIFICATION_SECURE);

    if (paramSecure == null) {
      paramSecure = config.getInitParameter("secure");
    }

    secure = Boolean.valueOf(paramSecure);

    String paramOracleSSO = config.getInitParameter("oracleSSO");
    oracleSSO = Boolean.valueOf(paramOracleSSO).booleanValue();

    // Extraire l'url d'authentification des paramètres systèmes si disponible.
    urlAuthentification = GestionParametreSysteme
        .getInstance()
        .getParametreSystemeEJB(ConstantesParametreSysteme.AUTHENTIFICATION_URL);

    if (urlAuthentification == null) {
      urlAuthentification = config.getInitParameter("url");
    }

    // Extraire le serveur d'authentification des paramètres systèmes si
    // disponible.
    nomServeur = GestionParametreSysteme.getInstance().getParametreSystemeEJB(
        ConstantesParametreSysteme.AUTHENTIFICATION_SERVEUR);

    if (nomServeur == null) {
      nomServeur = config.getInitParameter("nomServeur");
    }

    port = config.getInitParameter("port");

    nomParamUrlRetour = GestionParametreSysteme.getInstance()
        .getParametreSystemeEJB(
            ConstantesParametreSysteme.AUTHENTIFICATION_NOM_PARAM_URL_RETOUR);

    if (nomParamUrlRetour == null) {
      nomParamUrlRetour = config.getInitParameter("nomParamUrlRetour");
    }

    GestionSecurite.getInstance().setNomParametreUrlRetour(nomParamUrlRetour);

    // Extraire le dommaine (dont le cookie sera accessible par
    // l'authentification) des paramètres systèmes si disponible.
    nomCookie = GestionParametreSysteme.getInstance().getParametreSystemeEJB(
        ConstantesParametreSysteme.AUTHENTIFICATION_NOM_COOKIE);

    if (nomCookie == null) {
      nomCookie = config.getInitParameter("nomCookie");
    }

    GestionSecurite.getInstance().setNomCookieCertificat(getNomCookie());

    // Extraire le dommaine (dont le cookie sera accessible par
    // l'authentification) des paramètres systèmes si disponible.
    domaineCookie = GestionParametreSysteme.getInstance()
        .getParametreSystemeEJB(
            ConstantesParametreSysteme.AUTHENTIFICATION_DOMAINE_COOKIE);

    if (domaineCookie == null) {
      domaineCookie = config.getInitParameter("domaineCookie");
    }

    if (domaineCookie != null) {
      listeDomaineCookie = getListeNomDomaineCookie(domaineCookie);
    }

    GestionSecurite.getInstance().setNomDomaineCookie(domaineCookie);

    codeUtilisateurTest = config.getInitParameter("codeUtilisateurTest");

    String ageCookie = config.getInitParameter("ageCookie");

    if (ageCookie != null) {
      ageCookie = GestionParametreSysteme.getInstance().getParametreSystemeEJB(
          ConstantesParametreSysteme.AUTHENTIFICATION_AGE_COOKIE);

      if (ageCookie != null) {
        setAgeCookie(Integer.valueOf(ageCookie));
      }
    }
  }

  /**
   * Méthode qui sert à détruire le filtre.
   */
  @Override
  public void destroy() {
  }

  /**
   * Préparer l'URL qui permet de faire l'authentification.
   * <p>
   * 
   * @param request
   *          la requête HTTP en cours d'exécution
   * @return l'URL complète qui permet de faire l'authentification.
   */
  public String preparerURLAuthentification(HttpServletRequest request,
      boolean quitterSession) {
    StringBuffer url = new StringBuffer();

    String urlAuthentificationComplete = "";

    if (quitterSession && (getPageQuitter(request) != null)) {
      // Traitement pour un retour à la page d'accueil d'un portail externe.
      return getPageQuitter(request);
    }

    Boolean authentificationSecure = GestionParametreSysteme.getInstance()
        .getBoolean(ConstantesParametreSysteme.AUTHENTIFICATION_SECURE);
    String authentificationUrl = GestionParametreSysteme.getInstance()
        .getString(ConstantesParametreSysteme.AUTHENTIFICATION_URL);

    if (authentificationUrl.equals("")) {
      authentificationUrl = this.urlAuthentification;
    }

    String authentificationNomServeur = GestionParametreSysteme.getInstance()
        .getString(ConstantesParametreSysteme.AUTHENTIFICATION_SERVEUR);

    if (authentificationNomServeur.equals("")) {
      authentificationNomServeur = this.nomServeur;
    }

    if (!UtilitaireString.isVide(authentificationUrl)) {
      urlAuthentificationComplete = authentificationUrl;

      if (authentificationSecure.booleanValue()) {
        url.append("https://");
      } else {
        url.append("http://");
      }

      if (!UtilitaireString.isVide(authentificationNomServeur)) {
        url.append(authentificationNomServeur);

        if (getPort() != null) {
          url.append(":");
          url.append(getPort());
        }
      } else {
        url.append(request.getServerName());
        url.append(":");
        url.append(request.getServerPort());
      }

      url.append(urlAuthentificationComplete);
    } else {
      // Extraire l'url d'authentification via un paramètre système.
      String paramUrlAuthentification = getUrlAuthentification();

      if (!UtilitaireString.isVide(paramUrlAuthentification)) {
        urlAuthentification = paramUrlAuthentification;
        url.append(paramUrlAuthentification);
      }
    }

    StringBuffer urlRetour = new StringBuffer();

    if (!UtilitaireString.isVide(this.getNomParamUrlRetour())) {
      String adresseUrlRetour = null;

      if (!quitterSession) {
        adresseUrlRetour = getUrlRetourCourant(request);
      } else {
        adresseUrlRetour = getUrlRetourPourPageAccueil(request);
      }

      if (adresseUrlRetour != null) {
        // Spécifier l'url de retour après l'authentification.
        urlRetour.append(this.getNomParamUrlRetour());
        urlRetour.append("=");
        urlRetour.append(adresseUrlRetour);

        // On doit seulement ajouter l'url retour si il y en a un & a la fin de
        // lurl.
        url.append(((url.indexOf("?") != -1) ? "&" : "?")
            + urlRetour.toString());
      }
    }

    if (quitterSession && !isIgnorerFermetureSession()) {
      // Le retour après une fermeture de session.
      url.append(((url.indexOf("?") != -1) ? "&" : "?")
          + "fermetureSession=true");

      String langue = request.getParameter("langue");

      if (langue != null) {
        if (langue != null) {
          Locale locale = new Locale(langue.toLowerCase());
          UtilitaireControleur.setLocale(locale, request.getSession());
        }

        url.append("&langue=");
        url.append(langue.toLowerCase());
      }
    }

    // url.append(((url.indexOf("?") != -1) ? "&" : "?") +
    // urlRetour.toString());
    return url.toString();
  }

  /**
   * 
   * @param urlRetour
   * @param response
   */
  private void traitementAppelOracleSSO(HttpServletResponse response,
      String urlRetour) {
    response.setHeader("Osso-Paranoid", "false");

    try {
      response.setHeader("Osso-Return-Url", urlRetour);

      // Appel de l'écran d'authentification.
      response.sendError(499, "Oracle SSO");
    } catch (IOException e) {
    }
  }

  /**
   * Permet d'effectuer un traitement lorsqu'un utilisateur quitte l'application
   * et que l'authentification est de type SSO.
   * 
   * @param urlRetour
   *          URL de retour destiné au serveur d'authentification
   * @param response
   *          Réponse HTTP de l'utilisateur
   */
  private void traitementQuitterOracleSSO(HttpServletResponse response,
      String urlRetour) {
    try {
      if (log.isWarnEnabled()) {
        log.warn("Quitter Session SSO avec l'url:" + urlRetour);
      }

      response.setHeader("Osso-Return-Url", urlRetour);
      response.sendError(470, "Oracle SSO");
    } catch (IOException e) {
    }
  }

  protected Boolean isCertificatValide(String identifiant) {
    String codeUtilisateur = null;

    if (GestionSecurite.getInstance().isServiceAuthentificationExiste()) {
      try {
        codeUtilisateur = GestionSecurite.getInstance()
            .getServiceAuthentification().getCodeUtilisateur(identifiant);
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error(
              "Erreur pour obtenir le code utilisateur à partir du certificat.",
              e);
        }
      }
    }

    return codeUtilisateur != null
        && !Boolean.valueOf(identifiant.equals(codeUtilisateur));
  }

  /**
   * Obtenir le code utilisateur à partir du certificat.
   * 
   * @throws java.lang.Exception
   *           Erreur survenu lors du traitement
   * @return Code utilisateur
   * @param identifiant
   *          certificat
   */
  protected String getCodeUtilisateur(String identifiant) throws Exception {
    String codeUtilisateur = null;

    if (GestionSecurite.getInstance().isServiceAuthentificationExiste()) {
      try {
        codeUtilisateur = GestionSecurite.getInstance()
            .getServiceAuthentification().getCodeUtilisateur(identifiant);
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error(
              "Erreur pour obtenir le code utilisateur à partir du certificat.",
              e);
        }
      }
    }

    return codeUtilisateur;
  }

  /**
   * Terminer le certificat.
   * 
   * @param identifiant
   *          Certiticat
   */
  protected void terminerCertificat(String identifiant) {
    if (GestionSecurite.getInstance().isServiceAuthentificationExiste()) {
      try {
        GestionSecurite.getInstance().getServiceAuthentification()
        .terminerCertificat(identifiant);
      } catch (Exception e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur pour terminer le certificat.", e);
        }
      }
    }
  }

  protected String genererCertificat(String identifiant, HttpServletRequest request) {
    String certificat = null;

    try {
      certificat = GestionSecurite.getInstance().getServiceAuthentification()
          .genererCertificat(identifiant, null, UtilitaireApache.getUserIp(request), UtilitaireApache.getUserAgent(request), null);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur dans la génération du certificat.", e);
      }
    }

    return certificat;
  }

  /**
   * Méthode à être surchager pour spécifier un url d'authentification.
   */
  protected String getUrlAuthentification() {
    return null;
  }

  protected String getCookiePath() {
    return "/";
  }

  /**
   * Retourne la liste des nom de domaine que le cookie sera accessible.
   * 
   * @param nomDomaineCookie
   *          la liste des nom de domaine que le cookie sera accessible.
   */
  private ArrayList getListeNomDomaineCookie(String nomDomaineCookie) {
    ArrayList liste = new ArrayList();

    if (nomDomaineCookie.indexOf(",") != -1) {
      // Plusieurs paramètres y sont associés.
      StringTokenizer listeToken = new StringTokenizer(nomDomaineCookie, ",");

      while (listeToken.hasMoreTokens()) {
        String valeur = listeToken.nextToken().trim();
        liste.add(valeur);
      }
    } else {
      liste.add(nomDomaineCookie);
    }

    return liste;
  }

  /* Besoin de protected pour override. */
  protected String getUrlRetourCourant(HttpServletRequest request) {
    StringBuffer urlRetour = new StringBuffer();
    urlRetour.append(request.getRequestURL().toString());

    if (log.isInfoEnabled()) {
      log.info("URL Courant request.getRequestURL() : "
          + request.getRequestURL());
    }

    Enumeration enumerationParametre = request.getParameterNames();
    boolean isPremier = true;

    while (enumerationParametre.hasMoreElements()) {
      String nomParametre = (String) enumerationParametre.nextElement();
      String valeurParametre = request.getParameter(nomParametre);

      // Ajouter les paramètres dans l'url de retour
      if (isPremier) {
        urlRetour.append("?");
        isPremier = false;
      } else {
        urlRetour.append("&");
      }

      urlRetour.append(nomParametre);
      urlRetour.append("=");
      urlRetour.append(valeurParametre);
    }

    if (urlRetour.indexOf("ajax=true") == -1) {
      request.getSession().setAttribute("authentificationUrlRetour", urlRetour);
    } else {
      StringBuffer ancienUrlRetour = (StringBuffer) request.getSession()
          .getAttribute("authentificationUrlRetour");

      if (ancienUrlRetour != null) {
        urlRetour = ancienUrlRetour;
      }
    }

    return urlRetour.toString();
  }

  /**
   * Retourne l'url de retour après que l'utilisateur s'est authentifié.
   * 
   * @return l'url de retour après que l'utilisateur s'est authentifié.
   */
  protected String getUrlRetourPourPageAccueil(HttpServletRequest request) {
    StringBuffer urlRetour = new StringBuffer();

    if (GestionSecurite.getInstance().getComposantMenuAccueil() != null) {
      urlRetour.append(UtilitaireRequest.getUrlApplicationWeb(request));
      urlRetour.append("/");

      urlRetour.append(GestionSecurite.getInstance().getComposantMenuAccueil()
          .getAdresseWeb());

      if (log.isInfoEnabled()) {
        log.info("URL composant menu accueil UtilitaireRequest.getUrlApplicationWeb(request) : "
            + UtilitaireRequest.getUrlApplicationWeb(request));
        log.info("URL composant menu accueil GestionSecurite.getInstance().getComposantMenuAccueil().getAdresseWeb() : "
            + GestionSecurite.getInstance().getComposantMenuAccueil()
            .getAdresseWeb());
      }
    } else {
      if (log.isWarnEnabled()) {
        log.warn("Aucun composant d'accueil de spécifier");
      }

      urlRetour.append(UtilitaireRequest.getUrlApplicationWeb(request));
      urlRetour.append("/");

      if (log.isInfoEnabled()) {
        log.info("Aucun composant accueil UtilitaireRequest.getUrlApplicationWeb(request) : "
            + UtilitaireRequest.getUrlApplicationWeb(request));
      }
    }

    return urlRetour.toString();
  }

  protected String getPageAccueilPortail(HttpServletRequest request) {
    return null;
  }

  /**
   * La page
   * 
   * @return
   * @param request
   */
  protected String getPageQuitter(HttpServletRequest request) {
    String authentificationQuitterUrl = GestionParametreSysteme.getInstance()
        .getString("authentificationQuitterUrl");
    return authentificationQuitterUrl;
  }

  /**
   * Méthode à être surchager pour spécifier l'url qui appel une page d'erreur
   * lors d'un tentative d'authentification.
   * 
   * @return la page a appeler
   * @param request
   *          la requête en traitement.
   */
  protected String getUrlAuthentificationErreur(HttpServletRequest request) {
    return null;
  }

  public void setListeDomaineCookie(ArrayList listeDomaineCookie) {
    this.listeDomaineCookie = listeDomaineCookie;
  }

  public ArrayList getListeDomaineCookie() {
    return listeDomaineCookie;
  }

  public Utilisateur getUtilisateurTemporaire(HttpServletRequest request) {
    return (Utilisateur) request.getSession().getAttribute(
        "utilisateur_temporaire");
  }

  /**
   * Retourne si le filtre doit appliquer https
   * 
   * @return true si le filtre doit appliquer https
   */
  protected Boolean isSecure() {
    return secure;
  }

  /**
   * Fixer si le filtre doit appliquer https
   * 
   * @param secure
   *          si le filtre doit appliquer https
   */
  protected void setSecure(Boolean secure) {
    this.secure = secure;
  }

  public void setAgeCookie(Integer ageCookie) {
    this.ageCookie = ageCookie;
  }

  public Integer getAgeCookie() {
    return ageCookie;
  }

  public boolean isIgnorerFermetureSession() {
    Boolean ignorerFermetureSession = new Boolean(false);

    if ((GestionParametreSysteme.getInstance().getParametreSystemeEJB(
        ConstantesParametreSysteme.AUTHENTIFICATION_IGNORER_FERMETURE_SESSION) != null)
        && GestionParametreSysteme
        .getInstance()
        .getParametreSystemeEJB(
            ConstantesParametreSysteme.AUTHENTIFICATION_IGNORER_FERMETURE_SESSION)
            .equals("true")) {
      ignorerFermetureSession = new Boolean(true);
    }

    return ignorerFermetureSession.booleanValue();
  }

  /**
   * Classe interne qui permet d'aujouter au request un accès au contenu du
   * cookie d'authentification.
   * <p>
   * 
   * @author Jean-François Brassard (Nurun inc.)
   * @version 1.0
   */
  private class CodeAccesUtilisateurRequest extends HttpServletRequestWrapper {
    private String codeAccesUtilisateur;

    public CodeAccesUtilisateurRequest(HttpServletRequest req,
        String codeAccesUtilisateur) {
      super(req);
      this.codeAccesUtilisateur = codeAccesUtilisateur;
    }

    @Override
    public String getRemoteUser() {
      return codeAccesUtilisateur;
    }
  }
}
