/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Permet de détecter si une requête ajax est envoyé alors que la session de
 * l'utilisateur est expirée.
 * 
 * @author Jean-Maxime Pelletier
 */
public class FiltreRequeteAjax implements Filter {

  @Override
  public void init(FilterConfig arg0) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp,
      FilterChain chaine) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) resp;

    Boolean ajax = UtilitaireControleur.isAppelAjax(request);
    Boolean utilisateur = request.getSession().getAttribute(
        Constantes.UTILISATEUR) != null;

    /**
     * Si la requête est ajax et qu'on va faire un redir on doit transferer le
     * ajax=true Une requête qui commence avec ajax même suite à une redirection
     * doit continuer à être identifié comme une requête ajax.
     */
    Boolean expire = Boolean.FALSE;

    if (ajax) {
      response = new AjaxResponseWrapper(request, response);

      if (!utilisateur) {
        String host = request.getRequestURL().toString()
            .substring(0, request.getRequestURL().toString().lastIndexOf("/"));
        String url = host + "/session_expiree.jsp";
        response.sendRedirect(url);
        expire = Boolean.TRUE;
      }
    }

    if (!expire) {
      chaine.doFilter(request, response);
    }
  }

  @Override
  public void destroy() {
  }

  /**
   * Lors d'une requête ajax on doit signaler qu'il s'agit d'une requête ajax
   * même suivant une redirection.
   * 
   * @author j-m.pelletier
   */
  class AjaxResponseWrapper extends HttpServletResponseWrapper {

    private HttpServletRequest request;

    public AjaxResponseWrapper(HttpServletRequest request,
        HttpServletResponse response) {
      super(response);
      this.setRequest(request);
    }

    @Override
    public void sendRedirect(String location) throws IOException {
      String urlSite = GestionSecurite.getInstance().getApplication()
          .getAdresseSite();

      boolean secure = false;
      
      //secure = urlSite != null && urlSite.indexOf("https") != -1;

      boolean ajaxRedirect = (secure && location.indexOf("https") != -1)
          || !secure;

      if (ajaxRedirect) {
        if (location.indexOf("?") == -1) {
          location = location + "?ajax=true";
        } else {
          location = location + "&ajax=true";
        }
        super.sendRedirect(location);

      }

    }

    public void setRequest(HttpServletRequest request) {
      this.request = request;
    }

    public HttpServletRequest getRequest() {
      return request;
    }
  }
}
