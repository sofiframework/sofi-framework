/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.util.RequestUtils;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Filtre qui permet de changer la langue en cours
 *  de l'utilisateur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class FiltreLangue implements Filter {

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chaine)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) resp;
    String nouvelleLangue = req.getParameter("langueAffichage");
    if (!UtilitaireString.isVide(nouvelleLangue)) {
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
      if (utilisateur != null) {
        utilisateur.setLangue(nouvelleLangue);
        UtilitaireControleur.setLocalePourUtilisateur(request.getSession());
      } else {
        UtilitaireControleur.setLocale(UtilitaireControleur.getLocale(nouvelleLangue), request.getSession());
      }

      URL url = RequestUtils.requestURL(request);
      String params = request.getQueryString();
      int index = params.indexOf("langueAffichage=");
      String parametresRestant = "";

      if (index > 0) {
        parametresRestant += params.substring(0, (index -1));
      }

      if ((index + 18) < params.length()) {
        parametresRestant += params.substring(index + 18);
      }

      String urlSansLangue = url.toString();

      if (!UtilitaireString.isVide(parametresRestant)) {
        urlSansLangue += '?' + parametresRestant;
      }

      response.sendRedirect(urlSansLangue);
    } else {
      chaine.doFilter(req, resp);
    }
  }

  @Override
  public void destroy() {
  }
}
