/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Filtre qui permet de changer le locale en traitement par l'utilisateur.
 * 
 * @author Jean-François Brassard
 */
public class FiltreLocale implements Filter {

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chaine)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response =  (HttpServletResponse)resp;

    boolean localeConfigure = request.getSession().getAttribute(Constantes.CODE_LANGUE_EN_COURS) != null;

    Locale localeUtilisateur = null;

    //Extraire le cookie de localisation.
    Cookie localeCookie = UtilitaireRequest.getCookie(request, "sofi_locale");
    if (localeCookie != null) {
      localeUtilisateur = UtilitaireControleur.getLocale(localeCookie.getValue());
    }else {
      Locale localeNavigateur = request.getLocale();
      localeUtilisateur = UtilitaireControleur.getLocale(localeNavigateur.toString());
    }

    if (!localeConfigure && localeUtilisateur != null) {
      Locale locale = UtilitaireControleur.getLocale(localeUtilisateur.toString());
      UtilitaireControleur.setLocale(locale, request.getSession());
    }

    String localeParam = req.getParameter("locale");

    if (!UtilitaireString.isVide(localeParam) && !"POST".equalsIgnoreCase(request.getMethod())) {
      Locale locale = UtilitaireControleur.getLocale(localeParam);
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());
      if (utilisateur != null) {
        utilisateur.setCodeLocale(localeParam);
        UtilitaireControleur.setLocalePourUtilisateur(request.getSession());
      } else {
        UtilitaireControleur.setLocale(locale, request.getSession());
      }

      fixerLocale(request, locale);
      //Fixer le cookie de localisation.
      UtilitaireRequest.setCookie(request, response, "sofi_locale", locale.toString(), "/", null, new Integer(1*24*60*30));
      chaine.doFilter(req, resp);
    } else {
      fixerLocale(request, localeUtilisateur);
      chaine.doFilter(req, resp);
    }
  }

  /**
   * Méthode a surcharger.
   * @param locale la locale en cours.
   */
  public void fixerLocale(HttpServletRequest request, Locale locale) {

  }

  @Override
  public void destroy() {
  }
}
