/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.exception.SecuriteException;
import org.sofiframework.utilitaire.UtilitaireXss;

/**
 * Filtre qui permet de détecter les tentatives d'attaque "xss".
 * 
 * @author Jean-Maxime Pelletier
 */
public class FiltreXss implements Filter {

  public FiltreXss() {
  }

  @Override
  public void init(FilterConfig filterConfig) {
  }

  @Override
  public void doFilter(ServletRequest servletRequest,
      ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest request = (HttpServletRequest) servletRequest;

    for (Enumeration noms = request.getParameterNames(); noms.hasMoreElements(); )  {
      String nom = (String) noms.nextElement();
      String[] valeurs = request.getParameterValues(nom);
      if (valeurs != null) {
        for (int i = 0; i < valeurs.length; i++) {
          String valeur = valeurs[i];
          if (valeur instanceof String) {
            boolean valide = UtilitaireXss.validerInclusionXss(valeur);

            if(!valide) {
              throw new SecuriteException("Tentative d'exécution de script xss, votre action est journalisée.");
            }
          }
        }
      }
    }

    filterChain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {
  }
}