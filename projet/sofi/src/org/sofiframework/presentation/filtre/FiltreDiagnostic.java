/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;


public class FiltreDiagnostic implements Filter {
  private static final Log log = LogFactory.getLog(FiltreDiagnostic.class);
  private static Diagnostic diagnostic;

  @Override
  public void init(FilterConfig config) throws ServletException {
    log.debug("Initialisation du filtre Diagnostic");

    if (diagnostic == null) {
      throw new UnavailableException(
          "Aucune instance de diagnostic n'est définit");
    }

    config.getServletContext().setAttribute("diagnostic", diagnostic);
  }

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain fc)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    String level = request.getParameter("diagnostic");

    if (level == null) {
      level = request.getParameter("diagnosticSession");

      if ("off".equalsIgnoreCase(level) || "false".equalsIgnoreCase(level)) {
        level = null;
        request.getSession().removeAttribute("sofi.filtre.diagnostic");
      } else if (level == null) {
        level = (String) request.getSession().getAttribute("sofi.filtre.diagnostic");
      } else {
        request.getSession().setAttribute("sofi.filtre.diagnostic", level);
      }
    }

    request.setAttribute("sofi_filtre_diagnostic", level);
    diagnostic.start(level);

    try {
      String qs = request.getQueryString();
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

      if (utilisateur != null) {
        log.info("Utilisateur : " + utilisateur.getCodeUtilisateur());
      }

      log.info("RemoteHost : " + request.getRemoteHost());
      log.info("URL: " + request.getRequestURL() +
          ((qs == null) ? "" : ("?" + qs)));
      fc.doFilter(req, resp);
    } finally {
      diagnostic.stop();
    }
  }

  public static void setDiagnostic(Diagnostic diagnostic) {
    FiltreDiagnostic.diagnostic = diagnostic;
  }

  public static Diagnostic getDiagnostic() {
    return diagnostic;
  }
}
