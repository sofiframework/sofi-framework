/**
 *
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;


public class DiagnosticAppender extends AppenderSkeleton implements Diagnostic {
  private ThreadLocal logs = new ThreadLocal();
  private ThreadLocal contextThreshold = new ThreadLocal();

  public DiagnosticAppender() {
    FiltreDiagnostic.setDiagnostic(this);
  }

  public DiagnosticAppender(Layout layout) {
    setLayout(layout);
    FiltreDiagnostic.setDiagnostic(this);
  }

  @Override
  protected void append(LoggingEvent event) {
    Collection logs = getLogs();

    if (logs != null) {
      logs.add(event);
    }
  }

  @Override
  public boolean isAsSevereAsThreshold(Priority priority) {
    Priority limit = (Priority) contextThreshold.get();

    return ((limit == null) || priority.isGreaterOrEqual(limit));
  }

  protected Level getDefaultLevel() {
    return (threshold == null) ? null : Level.toLevel(threshold.toInt());
  }

  /* (non-Javadoc)
   * @see org.sofiframework.presentation.filtre.Diagnostic#start()
   */
  @Override
  public void start() {
    start(null);
  }

  /* (non-Javadoc)
   * @see org.sofiframework.presentation.filtre.Diagnostic#start(String)
   */
  @Override
  public void start(String level) {
    contextThreshold.set(Level.toLevel(level, getDefaultLevel()));
    logs.set(new ArrayList());
  }

  /* (non-Javadoc)
   * @see org.sofiframework.presentation.filtre.Diagnostic#stop()
   */
  @Override
  public void stop() {
    logs.set(null);
    contextThreshold.set(null);
  }

  /* (non-Javadoc)
   * @see org.sofiframework.presentation.filtre.Diagnostic#getMessages()
   */
  @Override
  public String getMessages() {
    StringBuffer buffer = new StringBuffer();

    for (Iterator it = getLogs().iterator(); it.hasNext();) {
      LoggingEvent event = (LoggingEvent) it.next();
      buffer.append(layout.format(event));

      if (layout.ignoresThrowable()) {
        String[] s = event.getThrowableStrRep();

        if (s != null) {
          int len = s.length;

          for (int i = 0; i < len; i++) {
            buffer.append(s[i]);
            buffer.append(Layout.LINE_SEP);
          }
        }
      }
    }

    return buffer.toString();
  }

  private Collection getLogs() {
    return (Collection) logs.get();
  }

  @Override
  public void close() {
  }

  @Override
  public boolean requiresLayout() {
    return true;
  }
}
