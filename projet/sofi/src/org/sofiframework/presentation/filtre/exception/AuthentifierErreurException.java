/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre.exception;

import org.sofiframework.exception.SOFIException;


/**
 * Exception qui permet de signaler au filtre d'authentification que
 * l'utilisateur doit recevoir une exception concernant une erreur lors
 * de l'authentification.
 *
 * @author Jean-François Brassard
 * @version $Revision$
 */
public class AuthentifierErreurException extends SOFIException {
  /**
   * 
   */
  private static final long serialVersionUID = 2103060740235503410L;

  /**
   * Création d'un objet AuthentifierPageErreurException.
   *
   * @param message Message qui détaille l'origine de l'erreur
   */
  public AuthentifierErreurException(String message) {
    super(message);
  }

  /**
   * Création d'un objet AuthentifierPageErreurException.
   *
   * @param message Message qui détaille l'origine de l'erreur
   * @param e Exception survenu lors du traitement
   */
  public AuthentifierErreurException(String message, Throwable e) {
    super(message, e);
  }
}
