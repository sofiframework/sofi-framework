/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre.exception;

import org.sofiframework.exception.SOFIException;


/**
 * Exception qui permet de signaler au filtre d'authentification que
 * l'utilisateur en cours doit être dirigé vers le serveur d'authentification.
 *
 * @author Jean-Maxime Pelletier
 * @version $Revision$
 */
public class AuthentifierUtilisateurException extends SOFIException {
  /**
   * 
   */
  private static final long serialVersionUID = 7194741544596290249L;
  /**
   * Identifie si l'on désire signaler au serveur d'authentification
   * que la session dans être quitté.
   */
  private boolean quitterSession;

  /**
   * Création d'un object AuthentifierUtilisateurException.
   *
   * @param message Message qui détaille l'origine de l'erreur
   * @param quitterSession indique si l'on désire signaler au serveur
   * d'authentification que l'on veux quitter la session.
   */
  public AuthentifierUtilisateurException(String message, boolean quitterSession) {
    super(message);
    this.quitterSession = quitterSession;
  }

  /**
   * Création d'un object AuthentifierUtilisateurException.
   *
   * @param message Message qui détaille l'origine de l'erreur
   * @param e Exception survenu lors du traitement
   * @param quitterSession indique si l'on désire signaler au serveur
   * d'authentification que l'on veux quitter la session.
   */
  public AuthentifierUtilisateurException(String message, Throwable e,
      boolean quitterSession) {
    super(message, e);
    this.quitterSession = quitterSession;
  }

  /**
   * Obtenir si l'on désire signaler au serveur
   * d'authentification que l'on veux quitter la session.
   *
   * @return Si l'on quitte la session
   */
  public boolean isQuitterSession() {
    return this.quitterSession;
  }
}
