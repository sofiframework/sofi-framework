/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Filtre qui compresse le résultat de sortie avec gzip (assume que le navigateur supporte cette fonctionnalité).<br>
 * Code basé sur: <a href="http://www.onjava.com/pub/a/onjava/2003/11/19/filters.html"><br>
 * http://www.onjava.com/pub/a/onjava/2003/11/19/filters.html</a>.<br>
 * <p>
 * Voici un exemple d'utilisation dans le fichier web.xml<br>
 * <code>
 * &lt;filter&gt;<br>
 * &nbsp;&nbsp;&lt;filter-name&gt;FiltreCompression&lt;/filter-name&gt;<br>
 * &nbsp;&nbsp;&lt;filter-class&gt;org.sofiframework.filtre.FiltreZip&lt;/filter-class&gt;<br>
 * &lt;/filter&gt;<br>
 * </code>
 * et<br>
 * <code>
 * &lt;filter-mapping&gt;<br>
 * &nbsp;&nbsp;&lt;filter-name&gt;FiltreCompression&lt;/filter-name&gt;<br>
 * &nbsp;&nbsp;&lt;url-pattern&gt;*.do&lt;/url-pattern&gt;<br>
 * &lt;/filter-mapping&gt;<br>
 * </code>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 *
 */
public class FiltreZip extends Filtre {

  /** Variable désignant l'objet de log pour cette classe */
  private static Log log = LogFactory.getLog(org.sofiframework.presentation.filtre.FiltreZip.class);

  @Override
  public void doFilter(HttpServletRequest request,
      HttpServletResponse response, FilterChain chaine) throws IOException, ServletException {
    String navigateur = request.getHeader("User-Agent");
    if (isGZIPSupported(request)) {
      if (log.isDebugEnabled()) {
        log.debug("GZIP est supporté, la réponse est compressé.");
      }
      FiltreZip.GZIPResponseWrapper wrappedResponse = new FiltreZip.GZIPResponseWrapper(response, navigateur);
      chaine.doFilter(request, wrappedResponse);
      wrappedResponse.finishResponse();
      return;
    }
    chaine.doFilter(request, response);
  }

  /**
   * Méthode afin de tester les capacités de GZip.
   * <p>
   * @param req la requete de l'utilisateur
   * @return boolean indique le support de GZip
   */
  private boolean isGZIPSupported(HttpServletRequest req) {
    String browserEncodings = req.getHeader("accept-encoding");
    boolean supported = ((browserEncodings != null) &&
        (browserEncodings.indexOf("gzip") != -1));

    String userAgent = req.getHeader("user-agent");

    if (userAgent == null || userAgent.startsWith("httpunit")) {
      if (log.isDebugEnabled()) {
        log.debug("httpunit ou userAgent vide détecté, désactivé le filtre...");
      }

      return false;
    } else {
      return supported;
    }
  }

  /**
   * Méthode qui sert à initialiser le filtre.
   * <p>
   * @param filterConfig la configuration du filtre
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    super.init(filterConfig);
  }

  /**
   * Méthode qui sert à détruire le filtre.
   */
  @Override
  public void destroy() {
    super.destroy();
  }

  /**
   * Classe interne afin de traiter le flu d'une réponse GZip.
   * <p>
   * @author Jean-François Brassard (Nurun inc.)
   * @version 1.0
   */
  private class GZIPResponseStream extends ServletOutputStream {

    /** le flu de sortie utilisé pour la compression */
    protected OutputStream bufferedOutput = null;

    /** état pour indiquer si la méthode close() est appelée */
    protected boolean closed = false;

    protected GZIPResponseWrapper zipResponse = null;

    /** référence au flu de sortie au navigateur du client */
    protected ServletOutputStream output = null;

    /** le volume par défaut du buffer en mémoire */
    private int bufferSize = 50000;

    /**
     * Constructeur par défaut
     * <p>
     * @param response la réponse HTTP à zipper
     * @throws IOException Exception générique
     */
    public GZIPResponseStream(GZIPResponseWrapper zipResponse)
        throws IOException {
      super();
      closed = false;
      this.zipResponse = zipResponse;
      this.output = zipResponse.getResponse().getOutputStream();
      bufferedOutput = new ByteArrayOutputStream();
    }

    /**
     * Méthode qui sert à fermer le flu de données.
     * <p>
     * @throws IOException Exception générique
     */
    @Override
    public void close() throws IOException {
      if (closed) {
        throw new IOException("Le flu de sortie est déjà fermé");
      }

      // prendre le contenu
      ByteArrayOutputStream baos = (ByteArrayOutputStream) bufferedOutput;
      byte[] content = baos.toByteArray();

      /* On ne doit pas zipper les éléments qui ne sont pas texte.
       * Avec certains navigateurs les fichiers se retrouvent coupés.
       */
      if (zipResponse.getContentType() != null && (zipResponse.getContentType().indexOf("text") != -1
          || (zipResponse.getContentType().indexOf("javascript") != -1
          && zipResponse.navigateur != null
          && zipResponse.navigateur.indexOf("MSIE 6.0") == -1))) {
        zipResponse.getHttpResponse().addHeader("Content-Encoding", "gzip");
        // prépare le flu de compression GZip
        ByteArrayOutputStream compressedContent = new ByteArrayOutputStream();
        GZIPOutputStream gzipstream = new GZIPOutputStream(compressedContent);
        gzipstream.write(content);
        gzipstream.finish();

        // prendre le contenu compressé
        content = compressedContent.toByteArray();
      }

      // Configurer l'entete pour indiquer au navigateur le contenu reçu
      // est zippé.
      zipResponse.getHttpResponse().setContentLength(content.length);

      output.write(content);
      output.flush();
      output.close();
      closed = true;
    }

    /**
     * Méthode qui sert à faire un flush sur le flu de données.
     * <p>
     * @throws IOException Exception générique
     */
    @Override
    public void flush() throws IOException {
      if (closed) {
        throw new IOException("Impossible de traité le flu de sortie");
      }

      bufferedOutput.flush();
    }

    /**
     * Méthode qui sert à écrire le flu de données dans la réponse utilisateur.
     * <p>
     * @param b
     * @throws IOException Exception générique
     */
    @Override
    public void write(int b) throws IOException {
      if (closed) {
        throw new IOException(
            "Impossible d'écrire pour fermer le flu de sortie");
      }

      // écrire les octet vers une sortie temporaire.
      bufferedOutput.write((byte) b);
    }

    /**
     * Méthode qui sert à écrire un tableau de bytes.
     * <p>
     * @param b le tableau de bytes à écrire
     * @throws IOException Exception générique
     */
    @Override
    public void write(byte[] b) throws IOException {
      write(b, 0, b.length);
    }

    /**
     * Méthode qui sert à écrire un tableau de bytes.
     * <p>
     * @param b le tableau de bytes à écrire
     * @param off le offset à partir duquel écrire
     * @param len la longueur de bytes à écrire
     * @throws IOException Exception générique
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
      if (closed) {
        throw new IOException(
            "Impossible d'écrire pour fermer le flu de sortie");
      }

      // écrire les octet vers le buffer.
      bufferedOutput.write(b, off, len);
    }

    /**
     * Méthode qui sert à indique si le flu de données est fermé.
     * <p>
     * @return la valeur indiquant si le flu de données est fermé ou pas
     */
    public boolean closed() {
      return (this.closed);
    }

    /**
     * Méthode qui ne fait rien.
     */
    public void reset() {
    }
  }

  /**
   * Classe interne qui permet de préparer la réponse du client en GZip.
   * <p>
   * @author Jean-François Brassard
   * @version 1.0
   */
  private class GZIPResponseWrapper extends HttpServletResponseWrapper {
    protected HttpServletResponse origResponse = null;
    protected ServletOutputStream stream = null;
    protected PrintWriter writer = null;
    protected int error = 0;
    protected String navigateur = null;
    protected String contentType = null;

    public GZIPResponseWrapper(HttpServletResponse response, String agent) {
      super(response);
      origResponse = response;
      navigateur = agent;
    }

    public ServletOutputStream createOutputStream() throws IOException {
      return (new FiltreZip.GZIPResponseStream(this));
    }

    public void finishResponse() {
      try {
        if (writer != null) {
          writer.close();
        } else {
          if (stream != null) {
            stream.close();
          }
        }
      } catch (IOException e) {
      }
    }

    @Override
    public void flushBuffer() throws IOException {
      stream.flush();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
      if (writer != null) {
        throw new IllegalStateException("getWriter() a déjà été appelé!");
      }

      if (stream == null) {
        stream = createOutputStream();
      }

      return (stream);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
      // Si l'accès est refusé, ne pas créer un nouveau ou d'écrire un flu car
      // il empeche le traitement de la page 403 spécifier dans le web.xml
      if (this.error == HttpServletResponse.SC_FORBIDDEN) {
        return super.getWriter();
      }

      if (writer != null) {
        return (writer);
      }

      if (stream != null) {
        throw new IllegalStateException("getOutputStream() a déjà été appelé!");
      }

      stream = createOutputStream();
      writer = new PrintWriter(new OutputStreamWriter(stream,
          origResponse.getCharacterEncoding()));

      return (writer);
    }

    @Override
    public void setContentLength(int length) {
    }

    /**
     * On doit concerver le content type dans un wrapper car l'implémentation
     * servelt 2.3 ne permet pas de récupérer cette valeur.
     */
    @Override
    public void setContentType(String type) {
      this.contentType = type;
      super.setContentType(type);
    }

    public String getContentType() {
      return this.contentType;
    }

    public HttpServletResponse getHttpResponse() {
      return this.origResponse;
    }

    /**
     * @see javax.servlet.http.HttpServletResponse#sendError(int, java.lang.String)
     */
    @Override
    public void sendError(int error, String message) throws IOException {
      super.sendError(error, message);
      this.error = error;

      if (log.isDebugEnabled()) {
        log.debug("erreur: " + error + " [" + message + "]");
      }
    }
  }
}
