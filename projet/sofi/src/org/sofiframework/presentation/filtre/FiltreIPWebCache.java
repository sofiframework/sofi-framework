/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.filtre;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


/**
 * Filtre qui permet d'extraire l'IP de l'utilisateur en utilisant la web cache
 * d'Oracle 9ias ou 10g.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FiltreIPWebCache implements Filter {
  /**
   * Applique le filtre.
   * <p>
   * @param pRequest l'objet request de la requête en cours
   * @param pResponse la réponse qui sera acheminée à l'utilisateur suite au traitement de la requête
   * @param pChain l'objet FilterChain à utiliser
   * @throws java.io.IOException Exception générique
   * @throws javax.servlet.ServletException Exception générique
   */
  @Override
  public void doFilter(ServletRequest pRequest, ServletResponse pResponse,
      FilterChain pChain) throws IOException, ServletException {
    if (pRequest instanceof HttpServletRequest) {
      pChain.doFilter(new FiltreIPWebCache.IpChangeRequest(
          (HttpServletRequest) pRequest), pResponse);
    } else {
      pChain.doFilter(pRequest, pResponse);
    }
  }

  /**
   * Méthode qui sert à initialiser le filtre.
   * <p>
   * @param filterConfig la configuration du filtre
   */
  @Override
  public void init(FilterConfig filterConfig) {
  }

  /**
   * Méthode qui sert à détruire le filtre.
   */
  @Override
  public void destroy() {
  }

  /**
   * Classe interne qui permet d'extraire l'IP de l'utilisateur via
   * la Web Cache d'Oracle 9ias ou 10g
   * @author Jean-François Brassard (Nurun inc.)
   * @version 1.0
   */
  private class IpChangeRequest extends HttpServletRequestWrapper {
    /** Constructeur par défaut */
    public IpChangeRequest(HttpServletRequest req) {
      super(req);
    }

    /**
     * Méthode qui sert à retourver l'adresse IP du client.
     * <p>
     * @return l'adresse IP du client
     */
    @Override
    public String getRemoteAddr() {
      String ipWebCache = getHeader("CLIENTIP");

      if ((ipWebCache == null) || (ipWebCache.length() == 0)) {
        ipWebCache = super.getRemoteAddr();
      }

      return ipWebCache;
    }

    /**
     * Méthode qui sert à obtenir le port de communication.
     * <p>
     * @return le numéro de port de communication
     */
    @Override
    public int getServerPort() {
      if (super.getServerPort() == 7778) {
        return 80;
      } else {
        return super.getServerPort();
      }
    }
  }
}
