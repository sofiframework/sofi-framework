/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.exception;

import org.sofiframework.exception.SOFIException;


/**
 * SecuriteException permet de spécifier que l'utilisateur
 * tente d'accéder à un service qui lui est non accessible.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class SecuriteException extends SOFIException {
  /**
   * 
   */
  private static final long serialVersionUID = 7753746754181085936L;

  /**
   * Constructeur d'une exception spécifiant une erreur de la couche
   * de présentation.
   * @param message le message de base.
   */
  public SecuriteException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur de la couche
   * de présentation avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public SecuriteException(String message, Throwable cause) {
    super(message, cause);
  }
}
