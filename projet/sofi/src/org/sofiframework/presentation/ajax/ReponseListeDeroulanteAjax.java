/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.utilitaire.UtilitaireString;


public class ReponseListeDeroulanteAjax extends ReponseAjax {
  private String proprieteNom;
  private String proprieteValeur;
  private String libelleLigneVide;
  private String libelleResultatListeVide;

  /**
   * Construit une nouvelle réponse Ajax de liste déroulante.
   *
   * @param libelleResultatListeVide Message qui sera présenté lorsque l'on
   * présente une liste vide.
   * @param libelleLigneVide Libellé de la valeur vide dans la liste
   * @param proprieteValeur Valeur qui doit être utilisé dans la liste
   * @param proprieteNom Nom qui sera présenté dans la liste
   */
  public ReponseListeDeroulanteAjax(String proprieteNom,
      String proprieteValeur, String libelleLigneVide,
      String libelleResultatListeVide) {
    this.proprieteNom = proprieteNom;
    this.proprieteValeur = proprieteValeur;
    this.libelleLigneVide = libelleLigneVide;
    this.libelleResultatListeVide = libelleResultatListeVide;
  }

  /**
   * Permet de générer le document XML afin de remplir un liste déroulante ajax.
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param libelleLigneVide le libelle si on désire ajouter un ligne vide.
   * @param libelleResultatVide le libelle si le résultat de la liste est vide,
   * s'il y a lieu.
   * @param proprieteValeur la propriété à utiliser dans la liste pour spécifier
   * la valeur.
   * @param proprieteNom  la propriété à utiliser dans la liste pour spécifier
   * le libellé de la liste déroulante.
   * @param collection la liste à générer en XML et à envoyer dans la réponse
   * vers le poste client.
   */
  public void generer(Collection collection, HttpServletRequest request,
      HttpServletResponse response) {
    GenerateurXmlAjax generateurXmlAjax = new GenerateurXmlAjax();

    if (libelleLigneVide != null) {
      if (!UtilitaireString.isVide(libelleLigneVide)) {
        Libelle libelle = UtilitaireLibelle.getLibelle(libelleLigneVide,
            request, null);
        generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
      } else {
        generateurXmlAjax.ajouterItem("", "");
      }
    }

    if ((libelleResultatListeVide != null) &&
        ((collection == null) || (collection.size() == 0))) {
      Libelle libelle = UtilitaireLibelle.getLibelle(libelleResultatListeVide,
          request, null);
      generateurXmlAjax.ajouterItem(libelle.getMessage(), "");
    }

    if (collection != null) {
      generateurXmlAjax.ajouterListeItem(collection, proprieteNom,
          proprieteValeur, false, request);
    }

    // Envoie du contenu xml
    setTypeContenu(response, TYPE_CONTENU_TEXT);
    setSansCache(response);
    ecrireReponse(response, generateurXmlAjax.genererEnXml());
  }
}
