/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;

import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.struts.form.utilitaire.UtilitaireBaseForm;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet ui permet de générer une réponse lors du traitement d'une
 * liste de valeur en Ajax.
 *
 * @author Jean-Maxime Pelletier
 * @version SOFI 2.0
 */
public class ReponseListeValeurAjax extends ReponseAjax {
  /**
   * Direction que doit prendre la réponse pour afficher le fragment de page
   * de liste en Ajax.
   *
   */
  private String nomDirectionListeAjax = null;

  /**
   * Liste des propriétés qui doivent être mappés dans la réponse Ajax dans le
   * cas ou un seul résultat est trouvé.
   */
  private LinkedHashSet listeProprietesAjax = new LinkedHashSet();

  /**
   * Clé du message d'erreur
   */
  private String cleErreur = null;

  /**
   * Constructeur par défaut
   */
  public ReponseListeValeurAjax() {
    super();
  }

  /**
   * Constructeur pour générer une réponse de liste de navigation Ajax.
   * @param formulaire Formulaire
   * @param correspondances Tableau des correspondance à faire
   * @param nomDirectionListeAjax
   */
  public ReponseListeValeurAjax(String nomDirectionListeAjax,
      CorrespondancePropriete[] correspondances, String cleErreur) {
    this.setNomDirectionListeAjax(nomDirectionListeAjax);

    for (int i = 0; i < correspondances.length; i++) {
      this.listeProprietesAjax.add(correspondances[i]);
    }

    this.cleErreur = cleErreur;
  }

  /**
   * Ajouter une nouvelle propriété qui doit être mappée dans le formulaire
   * lorsque l'on trouve un seul résultat.
   *
   * @param nomProprieteObjet Nom de la propriété dans l'objet de transfert.
   * @param nomChampFormulaire Nom du champ à populer dan sle formulaire.
   */
  public void ajouterProprieteListeValeur(String nomChampFormulaire,
      String nomProprieteObjet) {
    this.listeProprietesAjax.add(new CorrespondancePropriete(
        nomChampFormulaire, nomProprieteObjet));
  }

  /**
   * Ajouter un champde formulaire qui doit être populé par la liste de valeur.
   * Dans le cas présent le nom de champ du formulaire doit être le même
   * que le nom de la propriété.
   *
   * @param nomChampFormulaire Le nom du champ de formulaire
   */
  public void ajouterProprieteListeValeur(String nomChampFormulaire) {
    this.listeProprietesAjax.add(new CorrespondancePropriete(nomChampFormulaire));
  }

  /**
   * Fixer la direction à prendre pour afficher le fragment de liste de la liste.
   * @param direction Direction (Mapping Struts)
   */
  public void setNomDirectionListeAjax(String direction) {
    this.nomDirectionListeAjax = direction;
  }


  /**
   * Générer une réponse de liste de valeurs Ajax.
   * @return Direction Struts que doit prendre la réponse
   * @param response Réponse HTTP
   * @param request Requête HTTP
   * @param mapping Mapping Struts
   * @param formulaire Formulaire qui possède les champs qui seront populés par la
   * liste de valeurs.
   * @param liste Liste des résultats affiché dans la liste de navigation
   */
  public ActionForward generer(ListeNavigation liste, ActionMapping mapping,
      BaseForm formulaire, HttpServletRequest request,
      HttpServletResponse response) {
    String direction = null;

    try {
      /*
       * Lorsqu'on a un seul résultat il sera immédiatement populé dans le formulaire.
       */
      if ((liste != null) && (liste.getNbListe() != 0)) {
        if (liste.getNbListe() == 1) {
          Object resultat = liste.getListe().get(0);

          for (Iterator i = this.listeProprietesAjax.iterator(); i.hasNext();) {
            CorrespondancePropriete correspondance = (CorrespondancePropriete) i.next();
            String nomChampFormulaire = UtilitaireString.enleveTousLesEspaces(correspondance.getNomChampFormulaire());
            String nomPropriete = correspondance.getNomProprieteObjet();
            Object valeur = UtilitaireObjet.getValeurAttribut(resultat,
                nomPropriete);

            try {
              if (valeur != null) {
                String valeurString = (String) UtilitaireBaseForm.convertir(formulaire,
                    valeur.getClass(), nomPropriete, valeur,
                    BaseForm.VERS_FORMULAIRE, formulaire.getLocale(request),
                    formulaire.getTimeZone(request));
                ajouterValeur(nomChampFormulaire, valeurString);
              } else {
                ajouterValeur(nomChampFormulaire, "");
              }
            } catch (Exception e) {
              throw new SOFIException("La propriété : " + nomPropriete +
                  " doit être présente dans le formulaire en traitement");
            }
          }

          this.genererXML(response);
        } else {
          /* Dans le cas d'une liste on présente la fenêtre de résultats
           */
          direction = this.nomDirectionListeAjax;
        }
      } else {
        if (cleErreur != null) {
          this.genererErreur(cleErreur, null, request, response);
        }else {
          /*
           * Si on désire afficher la liste lorsque vide au lieu d'afficher un message d'erreur.
           */
          direction = this.nomDirectionListeAjax;
        }
      }
    } catch (Exception e) {
      throw new SOFIException("Erreur pour générer la réponse XML de la liste de valeur.",
          e);
    }

    return mapping.findForward(direction);
  }

  /**
   *
   * @param response
   */
  private void genererErreur(String cleMessage, String[] parametres,
      HttpServletRequest request, HttpServletResponse response) {
    StringBuffer xmlErreur = new StringBuffer();
    MessageErreur erreur = UtilitaireMessage.getMessageErreur(cleMessage,
        parametres, request);
    xmlErreur.append("<message_erreur>");
    xmlErreur.append((erreur.getMessage() != null) ? erreur.getMessage()
        : cleMessage);
    xmlErreur.append("</message_erreur>");
    xmlErreur.append("\n<listeChamp>");

    for (Iterator i = this.listeProprietesAjax.iterator(); i.hasNext();) {
      CorrespondancePropriete correspondance = (CorrespondancePropriete) i.next();
      String nomChamp = correspondance.getNomChampFormulaire();
      xmlErreur.append("\n<champ>");
      xmlErreur.append(UtilitaireString.supprimerTousLesBlancs(nomChamp));
      xmlErreur.append("</champ>");
    }

    xmlErreur.append("\n</listeChamp>");

    this.generer(this.getGenerateurXmlAjax().completerReponseAjax(xmlErreur),
        TYPE_CONTENU_XML, response);
  }
}
