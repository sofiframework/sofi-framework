/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Méthodes de génération d'une réponse Ajax en XMl / texte / XML. Permet
 * d'ajouter des propriété sous la forme de XML ou de generer une réponse en
 * HTML.
 *
 * @author Jean-Maxime Pelletier
 * @version SOFI 2.0
 */
public class ReponseAjax {
  public static final String NOM_REPONSE_AJAX_DEFAUT = "sofiReponseAjax";
  public static final String TYPE_CONTENU_XML = "text/xml; charset=UTF-8";
  public static final String TYPE_CONTENU_HTML = "text/html";
  public static final String TYPE_CONTENU_TEXT = "text/plain; charset=UTF-8";
  private GenerateurXmlAjax generateurAjax = new GenerateurXmlAjax();

  /**
   * Ajouter une valeur associé à un nom d'identifiant HTML.
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param valeur la valeur spécifié.
   * @param identifiant l'identifiant spécifié.
   */
  public void ajouterValeur(String identifiant, String valeur) {
    generateurAjax.ajouterItemAvecDonnees(identifiant, valeur);
  }

  /**
   * Ajouter une valeur et convertie en HTML.
   * @param response la réponse de l'utilisateur.
   * @param request la requête de l'utilisateur.
   * @param valeur la valeur spécifié.
   * @param identifiant l'identifiant spécifié.
   */
  public void ajouterValeurHTML(String identifiant, String valeur) {
    this.ajouterValeur(identifiant, UtilitaireString.convertirEnHtml(valeur));
  }

  /**
   * Générer une réponse avec une valeur devant rafrachir un champ
   * dans un page en mode ajax.
   * @param response la réponse de l'utilisateur.
   * @param valeur la valeur spécifié.
   */
  public void generer(String valeur, String typeContenu,
      HttpServletResponse response) {
    setTypeContenu(response, typeContenu);

    // Envoie du contenu html
    if (typeContenu.equals(TYPE_CONTENU_HTML)) {
      valeur = UtilitaireString.convertirEnHtml(valeur);
    }

    setSansCache(response);
    ecrireReponse(response, valeur);
  }

  /**
   * Générer un document XML incluant des valeurs multiplue dans la  réponse afin
   * de rafrachir plusieurs champs dans la page en mode ajax.
   * @param response la réponse de l'utilisateur.
   */
  public void genererXML(HttpServletResponse response) {
    this.generer(generateurAjax.genererEnXml(), TYPE_CONTENU_XML, response);
  }

  /**
   * Générer un document HTML dans la réponse.
   *
   * @param response la réponse de l'utilisateur.
   * @param html HTML
   */
  public void genererHTML(String html, HttpServletResponse response) {
    this.generer(html, TYPE_CONTENU_HTML, response);
  }

  /**
   * Générer un document Texte dans la réponse
   * @param response Réponse HTTP
   * @param texte Texte
   */
  public void genererTexte(String texte, HttpServletResponse response) {
    this.generer(texte, TYPE_CONTENU_TEXT, response);
  }

  /**
   * Modifie la réponse en spécifiant l'attribut d'entête http no-cache
   * @param response Réponse HTTP
   */
  protected void setSansCache(HttpServletResponse response) {
    response.setHeader("Cache-Control", "no-cache");
  }

  /**
   * Modifie le type de contenu de la réponse HTTP
   * @param typeContenu Nouveau type de contenu
   * @param response Réponse HTTP
   */
  protected void setTypeContenu(HttpServletResponse response, String typeContenu) {
    response.setContentType(typeContenu);
  }

  /**
   * Ecrire la valeur dans la réponse HTTP
   * @param valeur Nouvelle valeur à écrire
   * @param response Réponse HTTP
   */
  protected void ecrireReponse(HttpServletResponse response, String valeur) {
    try {
      PrintWriter pw = response.getWriter();
      pw.write(valeur);
      pw.close();
    } catch (IOException e) {
      throw new SOFIException("Erreur pour générer une réponse Ajax", e);
    }
  }

  /**
   * Crée une nouvelle réponse Ajax s'il n'en existe pas dans la requête
   * sinon réutilise l'instance déjà présente.
   * @param request Requête HTTP
   */
  public static ReponseAjax getReponseAjaxParDefaut(HttpServletRequest request) {
    ReponseAjax reponse = (ReponseAjax) request.getAttribute(NOM_REPONSE_AJAX_DEFAUT);

    if (reponse == null) {
      reponse = new ReponseAjax();
      request.setAttribute(NOM_REPONSE_AJAX_DEFAUT, reponse);
    }

    return reponse;
  }

  /**
   * Obtenir le générateur XML.
   * @return generateur
   */
  public GenerateurXmlAjax getGenerateurXmlAjax() {
    return this.generateurAjax;
  }
}
