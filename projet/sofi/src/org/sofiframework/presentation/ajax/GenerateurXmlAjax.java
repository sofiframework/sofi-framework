/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.modele.entite.DescriptionLocaleMap;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Generateur de document xml pour traitement Ajax.
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.5
 */
public class GenerateurXmlAjax {
  private String encoding = "UTF-8";
  private List items = new ArrayList();
  private List properties = new ArrayList();

  public GenerateurXmlAjax() {
  }

  /**
   * Ajouter une propriete au document XML.
   *
   * @param nom le nom de l'item.
   * @param valeur la valeur de l'item.
   * @return
   */
  public void ajouterPropriete(String nom, String valeur) {
    properties.add(new ItemXmlAjax(nom, valeur, false));
  }

  /**
   * Ajouter un itemm au document XML.
   *
   * @param nom le nom de l'item.
   * @param valeur la valeur de l'item.
   * @return
   */
  public void ajouterItem(String nom, String valeur) {
    items.add(new ItemXmlAjax(nom, valeur, false));
  }

  /**
   * Ajouter un item avec un élément de données(CDATA) é l'intérieur.
   *
   * @param nom le nom de l'item.
   * @param valeur la valeur de l'item.
   * @return
   */
  public void ajouterItemAvecDonnees(String nom, String valeur) {
    items.add(new ItemXmlAjax(nom, valeur, true));
  }

  /**
   * Ajouter des items avec l'aide d'une collection.
   *
   * @param collection la liste d'items utilisé pour générer le xml.
   * @param proprieteNom la propriété utilisé pour le libelle de l'élément xml.
   * @param proprieteValeur la propriété utilisé pour la valeur de l'élément xml.
   * @param utiliserDescriptionLocale boolean indiquant si on doit utiliser la description locale ou non.
   * @param request la requéte en cours.
   * @return une instance du générateur Xml Ajax.
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   */
  public void ajouterListeItem(Collection collection, String proprieteNom,
      String proprieteValeur, Boolean utiliserDescriptionLocale, HttpServletRequest request) {
    try {
      for (Iterator iter = collection.iterator(); iter.hasNext();) {
        Object element = iter.next();

        Object nom = UtilitaireObjet.getValeurAttribut(element, proprieteNom);

        if (request != null) {
          if (utiliserDescriptionLocale != null && utiliserDescriptionLocale) {
            String langueUtilisateur = UtilitaireControleur.getLanguePourUtilisateur(request.getSession());
            DescriptionLocaleMap description = (DescriptionLocaleMap) nom;
            nom = description.get(langueUtilisateur);
          } else {
            Libelle libelle = UtilitaireLibelle.getLibelle(nom.toString(),
                request, null);
            nom = libelle.getMessage();
          }          
        }

        Object valeur = UtilitaireObjet.getValeurAttribut(element,
            proprieteValeur);

        if ((nom != null) && (valeur != null)) {
          items.add(new ItemXmlAjax(nom.toString(), valeur.toString(), true));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Ajouter des proprietes avec l'aide d'une collection.
   *
   * @param collection la liste d'items utilisé pour générer le xml.
   * @param proprieteLibelle la propriété utilisé pour le libelle de l'élément xml.
   * @param proprieteValeur la propriété utilisé pour la valeur de l'élément xml.
   * @param request la requéte en cours.
   * @return une instance du générateur Xml Ajax.
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   */
  public void ajouterListeProrietes(Collection collection, String proprieteLibelle,
      String proprieteValeur, HttpServletRequest request) {
    try {
      for (Iterator iter = collection.iterator(); iter.hasNext();) {
        Object element = iter.next();

        Object nom = UtilitaireObjet.getValeurAttribut(element, proprieteLibelle);

        if (request != null) {
          Libelle libelle = UtilitaireLibelle.getLibelle(nom.toString(),
              request, null);
          nom = libelle.getMessage();
        }

        Object valeur = UtilitaireObjet.getValeurAttribut(element,
            proprieteValeur);

        if ((nom != null) && (valeur != null)) {
          ajouterPropriete(nom.toString(), valeur.toString());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Ajouter des items avec l'aide d'une collection.
   *
   * @param collection la liste d'items utilisé pour générer le xml.
   * @param proprieteNom la propriété utilisé pour le libelle de l'élément xml.
   * @param proprieteValeur la propriété utilisé pour la valeur de l'élément xml.
   * @return une instance du générateur Xml Ajax.
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   */
  public void ajouterListeItem(Collection collection, String proprieteLibelle,
      String proprieteValeur) {
    ajouterListeItem(collection, proprieteLibelle, proprieteValeur, false, null);
  }

  /**
   * Générer un document XML.
   */
  public String genererEnXml() {
    StringBuffer xml = new StringBuffer();

    for (Iterator iter = items.iterator(); iter.hasNext();) {
      ItemXmlAjax item = (ItemXmlAjax) iter.next();
      xml.append("<item>");
      xml.append("<nom>");

      if (item.isContientDonnees()) {
        xml.append("<![CDATA[");
      }

      xml.append(item.getNom());

      if (item.isContientDonnees()) {
        xml.append("]]>");
      }

      xml.append("</nom>");
      xml.append("<valeur>");

      if (item.isContientDonnees()) {
        xml.append("<![CDATA[");
      }

      if (!UtilitaireString.isVide(item.getValeur())) {
        xml.append(item.getValeur());
      }

      if (item.isContientDonnees()) {
        xml.append("]]>");
      }

      xml.append("</valeur>");
      xml.append("</item>");
    }

    for (Iterator iterProprietes = properties.iterator(); iterProprietes.hasNext();) {
      ItemXmlAjax proriete = (ItemXmlAjax) iterProprietes.next();
      xml.append("<" + proriete.getNom() + ">");
      xml.append(proriete.getValeur());
      xml.append("</" + proriete.getNom() + ">");
    }

    return completerReponseAjax(xml);
  }

  /**
   * Obtenir les balises de début d'une réponse Ajax.
   * @return début d'une réponse Ajax
   */
  public String completerReponseAjax(StringBuffer contenuXML) {
    StringBuffer xml = new StringBuffer().append("<?xml version=\"1.0\"");

    if (encoding != null) {
      xml.append(" encoding=\"");
      xml.append(encoding);
      xml.append("\"");
    }

    xml.append(" ?>");

    xml.append("<reponse-ajax>");

    xml.append(contenuXML);

    xml.append("</reponse-ajax>");

    return xml.toString();
  }
}
