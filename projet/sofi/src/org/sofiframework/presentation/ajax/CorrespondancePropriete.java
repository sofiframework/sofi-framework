/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;


/**
 * Correspondance entre le nom de champ dans le formulaire
 * et le nom de propriété de l'objet qui contient la valeur.
 *
 * @author Jean-Maxime Pelletier
 */
public class CorrespondancePropriete {
  /**
   * Nom du champ du formulaire
   */
  private String nomChampFormulaire;

  /**
   * Nom dela propriété de l'objet
   */
  private String nomProprieteObjet;

  /**
   * Contruit une nouvelle correspondance ou le nom du champde formulaire est
   * le même que le nom de la propriété de l'objet.
   * @param nomChampFormulaire Nom du champ
   */
  public CorrespondancePropriete(String nomChampFormulaire) {
    this.nomChampFormulaire = nomChampFormulaire;
    this.nomProprieteObjet = nomChampFormulaire;
  }

  /**
   * Contruit une nouvelle correspondance entre un objet et un champ de formulaire
   * @param nomProprieteObjet Nom de la propriété de l'objet qui contient la valeur
   * @param nomChampFormulaire Nom du champ du formulaire
   */
  public CorrespondancePropriete(String nomChampFormulaire,
      String nomProprieteObjet) {
    this.nomChampFormulaire = nomChampFormulaire;
    this.nomProprieteObjet = nomProprieteObjet;
  }

  /**
   * Fixer le nom du champ du formulaire
   * @param nomChampFormulaire le nom du champ du formulaire
   */
  public void setNomChampFormulaire(String nomChampFormulaire) {
    this.nomChampFormulaire = nomChampFormulaire;
  }

  /**
   * Obtenir le nom dela propriété de l'objet
   * @return le nom dela propriété de l'objet
   */
  public String getNomChampFormulaire() {
    return nomChampFormulaire;
  }

  /**
   * Fixer le nom dela propriété de l'objet
   * @param nomProprieteObjet le nom dela propriété de l'objet
   */
  public void setNomProprieteObjet(String nomProprieteObjet) {
    this.nomProprieteObjet = nomProprieteObjet;
  }

  /**
   * Obtenir le nom dela propriété de l'objet
   * @return le nom dela propriété de l'objet
   */
  public String getNomProprieteObjet() {
    return nomProprieteObjet;
  }
}
