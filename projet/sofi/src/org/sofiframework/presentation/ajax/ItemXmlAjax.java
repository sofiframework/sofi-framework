/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.ajax;

import org.sofiframework.objetstransfert.ObjetTransfert;


public class ItemXmlAjax extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 7526159736226965367L;
  private String nom;
  private String valeur;
  private boolean contientDonnees;

  public ItemXmlAjax() {
  }

  public ItemXmlAjax(String nom, String valeur, boolean contientDonnees) {
    this.nom = nom;
    this.valeur = valeur;
    this.contientDonnees = contientDonnees;
  }

  public String getNom() {
    return this.nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getValeur() {
    return this.valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public boolean isContientDonnees() {
    return this.contientDonnees;
  }

  public void setContientDonnees(boolean contientDonnees) {
    this.contientDonnees = contientDonnees;
  }
}
