/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.velocity;

import java.io.InputStream;

import org.apache.velocity.exception.ResourceNotFoundException;


/**
 *  ClasspathResourceLoader est une classe de chargement de template de
 *  velocity via le classpath disponible.
 *  <br>
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 */
public class ClasspathResourceLoader
extends org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader {
  /**
   * Prendre le InputStream du Runtime afin de batir le template.
   *
   * @param name le nom du template a prendre
   * @return InputStream contenant le template
   * @throws ResourceNotFoundException si le template n'est pas trouvé dans le classpath.
   */
  @Override
  public synchronized InputStream getResourceStream(String name)
      throws ResourceNotFoundException {
    InputStream result = null;

    if ((name == null) || (name.length() == 0)) {
      throw new ResourceNotFoundException("Aucun nom de template de fourni");
    }

    while (name.startsWith("/")) {
      name = name.substring(1);
    }

    ClassLoader classLoader = null;

    try {
      classLoader = Thread.currentThread().getContextClassLoader();

      if (classLoader == null) {
        classLoader = this.getClass().getClassLoader();
      }

      result = classLoader.getResourceAsStream(name);
    } catch (Exception fnfe) {
      throw new ResourceNotFoundException(fnfe.getMessage());
    }

    return result;
  }
}
