/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.velocity;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.sofiframework.exception.SOFIException;


/**
 * Classe utilitaire permettant l'intégration simple dans SOFI de
 * template Velocity.
 * <p>
 * Il est fortement conseillé d'utiliser des templates ayant une extension .xml.
 * <p>
 * Par défaut, l'emplacement des templates est basé sur le classpath du projet.
 * <p>
 * Voici un exemple d'utilisation :
 * <code>
 * UtilitaireVelocity velocity = new UtilitaireVelocity();
 * velocity.ajouterAuContexte("variableScript1", "test");
 * String resultat = velocity.generer("controleur/produit/exemple_gabarit.xml");
 * </code>
 * Si vous utilisez velocity au mode autonome, vous devez appelé cette méthode:
 * <code>
 * UtilitaireVelocity.initialiser();
 * </code>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @version SOFI 2.0.3 Ajout d'une méthode plus générique <code>generer</code> qui permet la génération depuis tout type de gabarit (xml, html, java, etc.).
 */
public class UtilitaireVelocity {
  /** L'instance de journalisation pour cette classe */
  private static final Log log = LogFactory.getLog(UtilitaireVelocity.class);

  /** le contexte de Velocity */
  private VelocityContext contexte = null;

  /**
   * Construteur qui créer un contexte velocity.
   */
  public UtilitaireVelocity() {
    creerContexte();
  }

  /**
   * Méthode permettant de générer un contexte Velocity.
   * <p>
   * @return Le contexte velocity nouvellement créé
   */
  public VelocityContext creerContexte() {
    contexte = new VelocityContext();

    return contexte;
  }

  /**
   * Méthode permettant d'obtenir le contexte.
   * <p>
   * @return Le contexte velocity nouvellement créé
   */
  public VelocityContext getContexte() {
    return contexte;
  }

  /**
   * Méthode permettant de fixer un contexte Velocity.
   * <p>
   * @param newContexte le contexte de Velocity
   */
  public void setContexte(VelocityContext newContexte) {
    this.contexte = newContexte;
  }

  /**
   * Méthode permettant d'ajouter une clé et valeur un contexte Velocity.
   * <p>
   * @param cle le nom de l'attribut à ajouter au contexte Velocity
   * @param valeur la valeur de l'attribut à ajouter au contexte Velocity
   */
  public void ajouterAuContexte(String cle, Object valeur) {
    this.contexte.put(cle, valeur);
  }

  /**
   * Méthode permettant de générer un fusion entre template et des objets dans le contexte Velocity.
   * <p>
   * L'encoding par défaut est UTF-8.
   * @param fichierTemplate Le chemin d'accès ainsi que le nom du template Velocity
   * @return le résultat généré
   */
  public String generer(String fichierTemplate) {
    return generer(fichierTemplate, "UTF-8");
  }

  /**
   * Méthode permettant de générer un fusion entre template et des objets dans le contexte Velocity.
   * @param fichierTemplate
   * @param encoding l'encoding du template.
   * @return le résulat généré.
   */
  public String generer(String fichierTemplate, String encoding) {
    Template template = null;

    StringWriter writer = new StringWriter();

    try {
      template = Velocity.getTemplate(fichierTemplate,encoding);

      if (template != null) {
        template.merge(contexte, writer);
      }
    } catch (ResourceNotFoundException rnfe) {
      throw new SOFIException("Ne trouve pas le gabarit " + fichierTemplate +
          ". Si le gabarit est appelé par une application autonome, veuillez appeler la méthode UtilitaireVelocity.initialiser()");
    } catch (ParseErrorException pee) {
      throw new SOFIException("Erreur de syntaxe dans " + fichierTemplate +
          ":" + pee.getMessage());
    } catch (Exception e) {
      throw new SOFIException("Erreur de fusion du gabarit : ", e);
    }

    String resultat = writer.toString();

    return resultat;
  }

  /**
   * Méthode permettant de générer du HTML basé sur un template.
   * @return le HTML généré
   * @param fichierTemplate Le chemin d'accès ainsi que le nom du template Velocity
   */
  public String genererHTML(String fichierTemplate) {
    return generer(fichierTemplate);
  }

  /**
   * Initialiser velocity pour une application automne, avec un Main.
   */
  static public void initialiser() {
    try {
      String ressourceLoader = (String) Velocity.getProperty(RuntimeConstants.RESOURCE_LOADER);

      if (ressourceLoader == null) {
        // Initialiser Velocity, s'il n'a pas été créer au préalable.
        Properties properties = new Properties();
        properties.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        properties.setProperty("classpath." + RuntimeConstants.RESOURCE_LOADER +
            ".class", ClasspathResourceLoader.class.getName());
        properties.setProperty( RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
            "org.apache.velocity.runtime.log.Log4JLogChute");
        properties.setProperty("runtime.log.logsystem.log4j.logger",
            "velocity");


        try {
          Velocity.init(properties);
        } catch (Exception e) {
        }
      }
    } catch (Exception e) {
    }
  }
}
