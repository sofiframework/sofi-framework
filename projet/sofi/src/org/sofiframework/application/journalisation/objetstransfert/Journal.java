/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.journalisation.objetstransfert;

import java.util.Date;
import java.util.HashMap;

import org.sofiframework.application.securite.objetstransfert.ComposantReferentiel;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Objet de transfert traitant la journalisation d'un traitement.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 * @since SOFI 2.0.3 Ajout du détail de l'utilisateur, de la référence sur le client associé à la journalisation et
 *        d'une autre référence externe (tel qu'un numéro de dossier) s'il y a lieu.
 */
public class Journal extends ObjetTransfert {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -4181518100399896112L;
  // L'identifiant de la journalisation.
  private Long idJournalisation;
  // Le code utilisateur associé à la journalisation.
  private String codeUtilisateur;
  
  // L'id de l'utilisateur;
  private String idUtilisateur;
  
  // L'identifiant du référentiel dont associé la journalisation.
  private Long seqObjetSecurisable;
  // La date et l'heure de la journalisation.
  private Date dateHeure;
  // Le détail de la journalisation.
  private String detail;
  // Le type de journalisation.
  private String type;
  // Information supplémentaire à celle incluse dans SOFI.
  @SuppressWarnings("rawtypes")
  private HashMap infoSupplementaire;
  // Le code application de la journalisation.
  private String codeApplication;
  // Le code client associé à la journalisation
  private String codeClient;
  // Détail de l'utilisateur associé au détail de la journalisation.
  private Utilisateur utilisateur;
  // Le détail du composant du référentil journalisé.
  private ComposantReferentiel composantReferentiel;
  // La référence spécifique à la journalisation (ex. No dossier)
  private String reference;

  private String codeTypeUtilisateur;
  private String codeModule;
  private String codeSystemeExterne;
  private String messageErreur;
  
  // @since 3.2
  private String infoComplementaire;
  private String urlLocation;
  private String ip;
  private String userAgent;
  private String creePar;
  private java.util.Date dateCreation;


  public Journal() {
  }

  /**
   * Retourne le code utilisateur
   * 
   * @return le code utilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer le code utilisateur
   * 
   * @param codeUtilisateur
   *          le code utilisateur
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Retourne la séquence de l'objet sécurisable en traitement.
   * 
   * @return la séquence de l'objet sécurisable en traitement.
   */
  public Long getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  /**
   * Fixer la séquence de l'objet sécurisable en traitement.
   * 
   * @param seqObjetSecurisable
   *          la séquence de l'objet sécurisable en traitement.
   */
  public void setSeqObjetSecurisable(Long seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  /**
   * Retourne la date et heure du traitement.
   * 
   * @return la date et heure du traitement.
   */
  public Date getDateHeure() {
    return dateHeure;
  }

  /**
   * Fixer la date et l'heure du traitement
   * 
   * @param dateHeure
   *          la date et l'heure du traitement.
   */
  public void setDateHeure(Date dateHeure) {
    this.dateHeure = dateHeure;
  }

  /**
   * Retourne le détail du message de journalisation
   * 
   * @return le détail du message de journalisation
   */
  public String getDetail() {
    return detail;
  }

  /**
   * Fixer le détail du message de la journalisation
   * 
   * @param detail
   *          le détail du message de la journalisation
   */
  public void setDetail(String detail) {
    this.detail = detail;
  }

  /**
   * Retourne le type du message de la journalisation
   * 
   * @return le type du message de la journalisation
   */
  public String getType() {
    return type;
  }

  /**
   * Fixer le type du message de la journalisation.
   * 
   * @param type
   *          le type du message de la journalisation.
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Retourne les informations supplémentaires à traiter pour la journalisation.
   * 
   * @return les informations supplémentaires à traiter pour la journalisation.
   */
  @SuppressWarnings("rawtypes")
  public HashMap getInfoSupplementaire() {
    return infoSupplementaire;
  }

  /**
   * Fixer les informations supplémentaires à traiter pour la journalisation.
   * 
   * @param infoSupplementaire
   *          les informations supplémentaires à traiter pour la journalisation.
   */
  public void setInfoSupplementaire(@SuppressWarnings("rawtypes") HashMap infoSupplementaire) {
    this.infoSupplementaire = infoSupplementaire;
  }

  /**
   * Fixer l'identifiant unique de la journalisation
   * 
   * @param IdJournalisation
   *          l'identifiant unique de la journalisation
   */
  public void setIdJournalisation(Long IdJournalisation) {
    this.idJournalisation = IdJournalisation;
  }

  /**
   * Retourne l'identifiant unique de la journalisation
   * 
   * @return l'identifiant unique de la journalisation
   */
  public Long getIdJournalisation() {
    return idJournalisation;
  }

  /**
   * Fixer le code client associé à la journalisation.
   * 
   * @param codeClient
   *          le code client associé à la journalisation.
   */
  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * Retourne le code client associé à la journalisation.
   * 
   * @return le code client associé à la journalisation.
   */
  public String getCodeClient() {
    return codeClient;
  }

  /**
   * Fixer l'utilisateur associé à la journalisation.
   * 
   * @param utilisateur
   */
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  /**
   * Retourne l'utilisateur associé à la journalisation.
   * 
   * @return l'utilisateur associé à la journalisation.
   */
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  /**
   * Fixer le code application assoicié à la journalisation.
   * 
   * @param codeApplication
   *          le code application assoicié à la journalisation.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code application associé à la journalisation.
   * 
   * @return le code application associé à la journalisation.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le composant du référentiel
   * 
   * @param composantReferentiel
   *          le composant du référentiel.
   */
  public void setComposantReferentiel(ComposantReferentiel composantReferentiel) {
    this.composantReferentiel = composantReferentiel;
  }

  /**
   * Retourne le composant du référentiel.
   * 
   * @return le composant du référentiel.
   */
  public ComposantReferentiel getComposantReferentiel() {
    return composantReferentiel;
  }

  /**
   * Fixer la référence (ex. no de dossier) associé à la journalisation.
   * 
   * @param reference
   *          la référence (ex. no de dossier) associé à la journalisation.
   */
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Retourne la référence (ex. no de dossier) associé à la journalisation.
   * 
   * @return la référence (ex. no de dossier) associé à la journalisation.
   */
  public String getReference() {
    return reference;
  }

  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  /**
   * 
   * @return
   * @since 3.2
   */
  public String getCodeModule() {
    return codeModule;
  }

  public void setCodeModule(String codeModule) {
    this.codeModule = codeModule;
  }

  /**
   * 
   * @return
   * @since 3.2
   */
  public String getCodeSystemeExterne() {
    return codeSystemeExterne;
  }

  public void setCodeSystemeExterne(String codeSystemeExterne) {
    this.codeSystemeExterne = codeSystemeExterne;
  }

  /**
   * 
   * @return
   * @since 3.2
   */
  public String getMessageErreur() {
    return messageErreur;
  }

  public void setMessageErreur(String messageErreur) {
    this.messageErreur = messageErreur;
  }

  /**
   * 
   * @return
   * @since 3.2
   */
  public String getCreePar() {
    return creePar;
  }

  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  public String getInfoComplementaire() {
    return infoComplementaire;
  }

  public void setInfoComplementaire(String infoComplementaire) {
    this.infoComplementaire = infoComplementaire;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getUrlLocation() {
    return urlLocation;
  }

  public void setUrlLocation(String urlLocation) {
    this.urlLocation = urlLocation;
  }

  public String getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(String idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

}
