/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.journalisation.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre de recherche de la journalisation de l'infrastructure SOFI.
 * <p>
 * Il est possible de filtrer sur la journalisation.
 * 
 * @author Jean-François Brassard
 * @version SOFI 2.0.3
 */
public class FiltreJournalisation extends ObjetFiltre {
  private static final long serialVersionUID = 4414743954583400723L;

  // L'identifiant de journalisation;
  private Long idJournalisation;
  // Liste de code utilisateur a extraire.
  private String[] codeUtilisateur;
  // le nom a extraire.
  private String nom;
  // Le prénom a extraire.
  private String prenom;
  // Le code utilisateur applicatif s'il y a lieu a extraire.
  private String codeUtilisateurApplicatif;
  // Liste des codes de statut de l'utilisateur a extraire.
  private String[] codeStatutUtilisateur;
  // Liste des composants a extraire.
  private Long[] seqObjetSecurisable;
  // Le détail de la journalisation.
  private String detail;
  // Liste des types d'actions
  private String[] type;
  // La date et l'heure de début de période a extraire.
  private java.util.Date dateHeureDebutPeriode;
  // La date et l'heure de début de période a extraire.
  private java.util.Date dateHeureFinPeriode;
  // La date et l'heure courante de la journalisation.
  private java.util.Date dateHeure;
  // Liste des codes clients a extraire.
  private String[] codeClient;
  // Liste des codes d'applications a extraire.
  private String[] codeApplication;
  // La référence (ex. no dossier) associé à la journalisation.
  private String reference;
  private String codeTypeUtilisateur;
  private String codeModule;
  private String codeSystemeExterne;
  private String messageErreur;
  
  // @since 3.2
  private String infoComplementaire;
  private String urlLocation;
  private String ip;
  private String userAgent;
  private String creePar;
  private java.util.Date dateCreation;

  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  public String getCodeModule() {
    return codeModule;
  }

  public void setCodeModule(String codeModule) {
    this.codeModule = codeModule;
  }

  public String getCodeSystemeExterne() {
    return codeSystemeExterne;
  }

  public void setCodeSystemeExterne(String codeSystemeExterne) {
    this.codeSystemeExterne = codeSystemeExterne;
  }

  public String getMessageErreur() {
    return messageErreur;
  }

  public void setMessageErreur(String messageErreur) {
    this.messageErreur = messageErreur;
  }

  /**
   * Constructeur.
   */
  public FiltreJournalisation() {
    ajouterIntervalle("dateHeureDebutPeriode", "dateHeureFinPeriode", "dateHeure");
    ajouterRechercheAvantApresValeur(new String[] { "detail", "nom", "prenom", "codeUtilisateur",
    "codeUtilisateurApplicatif", "infoComplementaire", "urlLocation", "messageErreur", "userAgent" });
  }

  /**
   * Fixer l'identifiant de journalisation a rechercher.
   * 
   * @param idJournalisation
   *          l'identifiant de journalisation a rechercher.
   */
  public void setIdJournalisation(Long idJournalisation) {
    this.idJournalisation = idJournalisation;
  }

  /**
   * Retourne l'identifiant de journalisation a rechercher.
   * 
   * @return l'identifiant de journalisation a rechercher.
   */
  public Long getIdJournalisation() {
    return idJournalisation;
  }

  /**
   * Fixer le code utilisateur a rechercher.
   * 
   * @param codeUtilisateur
   *          le code utilisateur a rechercher.
   */
  public void setCodeUtilisateur(String[] codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Retourne le code utilisateur a rechercher.
   * 
   * @return le code utilisateur a rechercher.
   */
  public String[] getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer le nom d'utilisateur a rechercher.
   * 
   * @param nom
   *          le nom d'utilisateur a rechercher.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom d'utilisateur a rechercher.
   * 
   * @return le nom d'utilisateur a rechercher.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le prenom d'utilisateur a rechercher.
   * 
   * @param prenom
   *          le prenom d'utilisateur a rechercher.
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Retourne le prenom d'utilisateur a rechercher.
   * 
   * @return le prenom d'utilisateur a rechercher.
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le code utilisateur applicatif a rechercher.
   * 
   * @param codeUtilisateurApplicatif
   *          le code utilisateur applicatif a rechercher.
   */
  public void setCodeUtilisateurApplicatif(String codeUtilisateurApplicatif) {
    this.codeUtilisateurApplicatif = codeUtilisateurApplicatif;
  }

  /**
   * Retourne le code utilisateur applicatif a rechercher.
   * 
   * @return le code utilisateur applicatif a rechercher.
   */
  public String getCodeUtilisateurApplicatif() {
    return codeUtilisateurApplicatif;
  }

  /**
   * Fixer le code de statut d'utilisateur a rechercher.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @param codeStatutUtilisateur
   *          le code de statut d'utilisateur a rechercher.
   */
  public void setCodeStatutUtilisateur(String[] codeStatutUtilisateur) {
    this.codeStatutUtilisateur = codeStatutUtilisateur;
  }

  /**
   * Retourne le code de statut d'utilisateur a rechercher.
   * <p>
   * Il est possible d'en fixer plusieurs dans le tableau.
   * 
   * @return
   */
  public String[] getCodeStatutUtilisateur() {
    return codeStatutUtilisateur;
  }

  /**
   * Fixer l'identifiant du composant du référentiel a rechercher.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @param seqObjetSecurisable
   *          l'identifiant du composant du référentiel a rechercher
   */
  public void setSeqObjetSecurisable(Long[] seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  /**
   * Retourne l'identifiant du composant du référentiel a rechercher.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @return l'identifiant du composant du référentiel a rechercher.
   */
  public Long[] getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  /**
   * Fixer le détail du message de la journalisation a rechercher.
   * 
   * @param detail
   *          le détail du message de la journalisation a rechercher.
   */
  public void setDetail(String detail) {
    this.detail = detail;
  }

  /**
   * Retourne le détail du message de la journalisation a rechercher.
   * 
   * @return le détail du message de la journalisation a rechercher.
   */
  public String getDetail() {
    return detail;
  }

  /**
   * Fixer le type d'action de journalisation a rechercher.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @param typeAction
   *          le type d'action de journalisation a rechercher.
   */
  public void setType(String[] type) {
    this.type = type;
  }

  /**
   * Retourne le type d'action de journalisation a rechercher.
   * <p>
   * Il est possible d'en fixer plusieurs dans le tableau.
   * 
   * @return le type d'action de journalisation a rechercher.
   */
  public String[] getType() {
    return type;
  }

  /**
   * Fixer la date et l'heure du début de la période de journalisation a rechercher.
   * 
   * @param dateHeureDebutPeriode
   *          la date et l'heure du début de la période de journalisation a rechercher.
   */
  public void setDateHeureDebutPeriode(Date dateHeureDebutPeriode) {
    this.dateHeureDebutPeriode = dateHeureDebutPeriode;
  }

  /**
   * Retourne la date et l'heure du début de la période de journalisation a rechercher.
   * 
   * @return la date et l'heure du début de la période de journalisation a rechercher.
   */
  public Date getDateHeureDebutPeriode() {
    return dateHeureDebutPeriode;
  }

  /**
   * Fixer la date et l'heure de la fin de la période de journalisation a rechercher.
   * 
   * @param dateHeureFinPeriode
   *          la date et l'heure de la fin de la période de journalisation a rechercher.
   */
  public void setDateHeureFinPeriode(Date dateHeureFinPeriode) {
    this.dateHeureFinPeriode = dateHeureFinPeriode;
  }

  /**
   * Retourne la date et l'heure de la fin de la période de journalisation a rechercher.
   * 
   * @return sla date et l'heure de la fin de la période de journalisation a rechercher.
   */
  public Date getDateHeureFinPeriode() {
    return dateHeureFinPeriode;
  }

  /**
   * Fixer la date et l'heure dont on désire rechercher de la journalisation.
   * 
   * @param dateHeure
   *          la date et l'heure dont on désire rechercher de la journalisation.
   */
  public void setDateHeure(Date dateHeure) {
    this.dateHeure = dateHeure;
  }

  /**
   * Retourne la date et l'heure dont on désire rechercher de la journalisation.
   * 
   * @return la date et l'heure dont on désire rechercher de la journalisation.
   */
  public Date getDateHeure() {
    return dateHeure;
  }

  /**
   * Fixer le code du client dont on désire rechercher de la journalisation.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @param codeClient
   */
  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * Retourne le code du client dont on désire rechercher de la journalisation.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @return le code du client dont on désire filtrer.
   */
  public String[] getCodeClient() {
    return codeClient;
  }

  /**
   * Fixer le code application dont on désire rechercher de la journalisation.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @param codeApplication
   *          le code application dont on désire filtrer.
   */
  public void setCodeApplication(String[] codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code application dont on désire rechercher de la journalisation.
   * <p>
   * Il est possible de fixer plusieurs valeurs.
   * 
   * @return le code application dont on désire rechercher de la journalisation.
   */
  public String[] getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer la référence (ex. no dossier) associé à la journalisation.
   * 
   * @param reference
   *          la référence (ex. no dossier) associé à la journalisation dont on désire filtrer.
   */
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Retourne la référence (ex. no dossier) associé à la journalisation.
   * 
   * @return la référence (ex. no dossier) associé à la journalisation dont on désire filtrer.
   */
  public String getReference() {
    return reference;
  }

  public String getCreePar() {
    return creePar;
  }

  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  public String getInfoComplementaire() {
    return infoComplementaire;
  }

  public void setInfoComplementaire(String infoComplementaire) {
    this.infoComplementaire = infoComplementaire;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getUrlLocation() {
    return urlLocation;
  }

  public void setUrlLocation(String urlLocation) {
    this.urlLocation = urlLocation;
  }
}
