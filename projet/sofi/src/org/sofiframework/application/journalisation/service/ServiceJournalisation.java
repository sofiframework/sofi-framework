/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.journalisation.service;

import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Interface sur le service de journalisation de l'infrastructure.
 * <p>
 * 
 * @author Jean-François Brassard
 * @version SOFI 1.0
 * @since SOFI 2.0.3 Ajout de méthode pour extraire le détail d'une instance de journalisation et une autre méthode pour
 *        extraire un résultat de recherche.
 */

public interface ServiceJournalisation {
  /**
   * Inscription d'une instance de journalisation dans l'infrastructure.
   * 
   * @param journal
   *          l'instance a journaliser
   * @return l'instance écrite dans l'infrastructure.
   */
  public Journal ecrire(Journal journal);

  /**
   * Extraire une instance de journalisation depuis l'infrastructure.
   * 
   * @param idJournalisation
   *          l'identifiant du journal.
   * @return l'instance de journalisation.
   */
  public Journal getJournal(Long idJournalisation);

  /**
   * Extraire un résultat de recherche sur la journalisation de l'infrastructure.<br>
   * La liste de navigation résultat devra contenir des objets de type {@link Journal}.
   * 
   * @param liste
   *          la définition de la recherche.
   * @return le résultat de la recherche.
   */
  public ListeNavigation getListeJournalisation(ListeNavigation liste);
}
