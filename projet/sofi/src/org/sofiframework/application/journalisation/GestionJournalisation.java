/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.journalisation;

import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.application.journalisation.service.ServiceJournalisation;
import org.sofiframework.application.securite.GestionSecurite;

/**
 * Objet permettant la gestion de la journalisation.
 * <p>
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 * @since SOFI 2.0.1 La méthode ecrire retourne maintenant une instance de journalisation afin de pouvoir extraire par
 *        exemple la séquence.
 */
public class GestionJournalisation extends BaseGestionService {
  /**
   * Référence à l'instance de gestion de la journalisation
   */
  private static GestionJournalisation gestionJournalisationRef = new GestionJournalisation();
  private ServiceJournalisation serviceJournalisation = null;

  private GestionJournalisation() {
    super(org.sofiframework.application.journalisation.service.ServiceJournalisation.class);
  }

  /**
   * Obtenir l'instance unique de gestion de la journalisation.
   * 
   * @return l'instance unique de gestion de la journalisation
   */
  public static GestionJournalisation getInstance() {
    return gestionJournalisationRef;
  }

  /**
   * Permet l'écriture au journal
   * 
   * @param journal
   *          une instance de journal
   * @throws java.lang.Exception
   *           lance une exception si echec de la journalisation.
   */
  public Journal ecrire(Journal journal) throws Exception {
    try {
      if (journal != null && journal.getCodeApplication() == null) {
        journal.setCodeApplication(GestionSecurite.getInstance().getCodeApplication());
      }
      return serviceJournalisation.ecrire(journal);
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Permet l'écriture au journal.
   * 
   * @param codeUtilisateur
   *          le code utilisateur
   * @param mode
   *          le mode de journalisation
   * @param detail
   *          le détail de la journalisation
   * @param codeClient
   *          le code client associé à la journalisation s'il y lieu.
   * @param reference
   *          la référence (ex. no dossier) associé à la journalisation s'il y lieu.
   * @param idComposantReferentiel
   *          le composant du référentiel dont la journalisation a eu lieu.
   * @return l'instance de journalisation complété.
   * @throws Exception
   *           lance une exception si echec de la journalisation.
   */
  public Journal ecrire(String codeUtilisateur, String mode, String detail, String codeClient, String reference,
      Long idComposantReferentiel) throws Exception {
    Journal journal = new Journal();
    journal.setCodeUtilisateur(codeUtilisateur);
    journal.setDateHeure(new java.util.Date());
    journal.setType(mode);
    journal.setDetail(detail);
    journal.setCodeClient(codeClient);
    journal.setReference(reference);
    journal.setSeqObjetSecurisable(idComposantReferentiel);
    try {
      return ecrire(journal);
    } catch (Exception e) {
      throw e;
    }

  }

  public boolean isServiceJournalisationConfigure() {
    return this.serviceJournalisation != null;
  }

  public ServiceJournalisation getServiceJournalisation() {
    return serviceJournalisation;
  }

  public void setServiceJournalisation(ServiceJournalisation serviceJournalisation) {
    this.serviceJournalisation = serviceJournalisation;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceJournalisation((ServiceJournalisation) service);
  }
}
