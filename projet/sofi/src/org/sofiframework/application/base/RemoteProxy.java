/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.base;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;

public class RemoteProxy implements InvocationHandler {

  private Object target;

  private HashMap methodCache = new HashMap();

  private RemoteProxy(Object target, Class[] interfaces) {
    this.target = target;
    Class clazz = target.getClass();
    for (int i = 0, n = interfaces.length; i < n; i++) {
      Class api = interfaces[i];
      if (!api.isInterface()) {
        throw new IllegalArgumentException(api.getName()
            + " n'est pas une interface");
      }
      Method[] methods = api.getMethods();
      for (int j = 0, m = methods.length; j < m; j++) {
        Method apiMethod = methods[j];
        try {
          Method targetMethod = getAnalogMethod(apiMethod, clazz);
          if (apiMethod.getReturnType() != targetMethod.getReturnType()) {
            throw new IllegalArgumentException("La méthode " + targetMethod
                + " n'a pas le bon type de retour, attendu: "
                + apiMethod.getReturnType());
          }
          this.methodCache.put(apiMethod, targetMethod);
        } catch (NoSuchMethodException e) {
          //          throw new IllegalArgumentException(
          //                  "Impossible de trouvé une méthode analogue à " + apiMethod
          //                          + " dans la classe " + clazz.getName());
        }
      }
    }
  }

  private static Method getAnalogMethod(Method method, Class clazz)
      throws NoSuchMethodException {
    return clazz.getMethod(method.getName(), method.getParameterTypes());
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args)
      throws Throwable {
    Method targetMethod = (Method) methodCache.get(method);
    if (targetMethod == null) {
      throw new UnsupportedOperationException(
          "Impossible de trouvé une méthode analogue à " + targetMethod
          + " dans la classe " + target.getClass().getName());
    }
    try {
      return targetMethod.invoke(target, args);
    } catch (InvocationTargetException e) {
      throw e.getTargetException();
    }
  }

  private static ClassLoader defautClassLoader() {
    return Thread.currentThread().getContextClassLoader();
  }

  public static Object creerProxy(Object target, ClassLoader cl, Class[] api) {
    return Proxy.newProxyInstance(cl, api, new RemoteProxy(target, api));
  }

  public static Object creerProxy(Object target, ClassLoader cl, Class api) {
    return creerProxy(target, cl, new Class[] { api });
  }

  public static Object creerProxy(Object target, Class api) {
    return creerProxy(target, defautClassLoader(), new Class[] { api });
  }

  public static Object creerProxy(Object target, Class[] api) {
    return creerProxy(target, defautClassLoader(), api);
  }

}
