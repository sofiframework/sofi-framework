/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.base;

import java.util.HashSet;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Objet abstrait de base permettant la gestion des services pour tous les classes
 * de type Gestion (singleton).
 * <p>
 * Une classe de type GestionXXXX pour se connecter ? un
 * service doit étendre de cette classe.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class BaseGestionService {
  /**
   * Constante pour différentier les valeurs null dans
   * la cache des valeurs non définis.
   */
  public static final Object NULL = new Object();

  /**
   * Date de dernière mise à jour.
   */
  private String dateDerniereMaj = UtilitaireDate.getDateJourEn_AAAA_MM_JJ();

  /**
   * Indique si le système de cache est utilisé.
   */
  private boolean isViderCache = false;

  /** Liste des services qui sont configurés dans le gestionnaire  **/
  private HashSet servicesConfigures = new HashSet();
  private Class classeServiceLocal;

  protected BaseGestionService(Class classeServiceLocal) {
    this.classeServiceLocal = classeServiceLocal;
  }

  protected BaseGestionService() {
  }

  /**
   * Obtenir si le cache est utilisé.
   * <p>
   * @return si le cache est utilisé
   */
  public boolean getIsViderCache() {
    return isViderCache;
  }

  /**
   * Fixer si la cache doit d'ètre re-initialiser.
   * <p>
   * @param newIsViderCache si le cache est utilisé
   */
  public void setIsViderCache(boolean newIsViderCache) {
    isViderCache = newIsViderCache;
  }

  /**
   * Retourne la date de dernière mise à jour de la cache des messages
   * provenant de la base de données.
   * @return la date de la dernière mise à jour de la cache
   */
  public String getDateDerniereMaj() {
    return dateDerniereMaj;
  }

  /**
   * Fixer la date de dernière mise à jour de la cache des messages
   * provenant de la base de données.
   * @param dateDerniereMaj la dernière date de mise à jour
   */
  public void setDateDerniereMaj(String dateDerniereMaj) {
    this.dateDerniereMaj = dateDerniereMaj;
  }

  public void setServicesConfigures(HashSet servicesConfigures) {
    this.servicesConfigures = servicesConfigures;
  }

  public HashSet getServicesConfigures() {
    return servicesConfigures;
  }

  protected abstract void setServiceLocal(Object service);

  public void setServiceRemote(Object service) {
    if (classeServiceLocal == null) {
      throw new NullPointerException("classeServiceLocal");
    }

    setServiceLocal(RemoteProxy.creerProxy(service, classeServiceLocal));
  }

  protected static Object unmaskNull(Object valeur) {
    return (valeur == NULL) ? null : valeur;
  }

  protected static Object maskNull(Object valeur) {
    return (valeur == null) ? NULL : valeur;
  }
}
