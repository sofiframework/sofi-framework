/*
 * $URL: https://svn.nurunquebec.com/sofi/dev/v1.6.0/sources/SOFI/src/org/sofiframework/application/cache/ListeFuseauHoraire.java $
 * $LastChangedBy: j-m.pelletier $
 * $LastChangedDate: 2006-02-09 16:20:19 -0500 (jeu., 09 fÃ©vr. 2006) $
 * $LastChangedRevision: 617 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TimeZone;
import java.util.TreeSet;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;

public class ListeFuseauHoraire extends ObjetCache {

  /**
   * 
   */
  private static final long serialVersionUID = -5569864112189603272L;

  /**
   * Constructeur par défaut
   */
  public ListeFuseauHoraire() {
  }

  @Override
  public void chargerDonnees() {
    super.chargerDonnees();

    String pays = (String) GestionParametreSysteme.getInstance().getParametreSysteme("paysFuseauHoraire");
    String[] listeID = TimeZone.getAvailableIDs();

    /* On doit trier toutes les timezone en ordre de
     * décalage par rapport au GMT.
     */
    TreeSet trier = new TreeSet();

    for (int i = 0; i < listeID.length; i++) {
      TimeZone tz = TimeZone.getTimeZone(listeID[i]);
      int offset = tz.getRawOffset();
      trier.add(new Integer(offset));
    }

    /* On récupère tous les fuseaux
     * qui correspondent au même offset.
     */
    for (Iterator i = trier.iterator(); i.hasNext(); ) {
      Integer offset = (Integer) i.next();

      /* On doit présenter chaque zone même si elles ont le même décalage.
       * Les zones peuvent aussi varier en fonction de leur heure d'été.
       */
      String[] listeParTZ = TimeZone.getAvailableIDs(offset.intValue());

      for (int j = 0; j < listeParTZ.length; j++) {
        String id = listeParTZ[j];
        if (id.indexOf("/") != -1) {
          String paysEnCours = id.substring(0, id.indexOf("/"));
          if (pays == null || pays.equals("") || pays.indexOf(paysEnCours) != -1) {
            this.put(id, "commun.libelle.timezone." + id.toLowerCase());
          }
        }
      }
    }
  }

  public static void main(String[] args) {

    ListeFuseauHoraire liste = new ListeFuseauHoraire();

    liste.chargerDonnees();

    LinkedHashMap ll = liste.collection;

    for (Iterator i = ll.keySet().iterator(); i.hasNext(); ) {
      Object o = i.next();
      System.out.println(o  + " " + ((String) ll.get(o)));
    }
  }

}