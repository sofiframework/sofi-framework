/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.text.MessageFormat;

/**
 * Clé composée du gestionnaire de cache.
 */
class CleCache {

  private String nomCache;
  private Object sousCache;

  public CleCache(String nomCache) {
    this.nomCache = nomCache;
    this.sousCache = null;
  }

  public CleCache(String nomCache, Object sousCache) {
    this.nomCache = nomCache;
    this.sousCache = sousCache;
  }

  public void setNomCache(String nomCache) {
    this.nomCache = nomCache;
  }

  public String getNomCache() {
    return nomCache;
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  public Object getSousCache() {
    return sousCache;
  }

  @Override
  public String toString() {
    return (sousCache != null)
        ? MessageFormat.format("{0}[{1}]", new Object[]{nomCache, sousCache.toString()})
            : nomCache;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nomCache == null) ? 0 : nomCache.hashCode());
    result = prime * result + ((sousCache == null) ? 0 : sousCache.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CleCache other = (CleCache) obj;
    if (nomCache == null) {
      if (other.nomCache != null) {
        return false;
      }
    } else if (!nomCache.equals(other.nomCache)) {
      return false;
    }
    if (sousCache == null) {
      if (other.sousCache != null) {
        return false;
      }
    } else if (!sousCache.equals(other.sousCache)) {
      return false;
    }
    return true;
  }
}
