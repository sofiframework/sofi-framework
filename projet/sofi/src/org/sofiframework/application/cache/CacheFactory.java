/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.utilitaire.UtilitaireException;


/**
 * Permet d'instancier des objets caches.
 * <p>
 * @author Jean-François Brassard Nurun inc.
 * @author Jean-Maxime Pelletier
 * @version SOFI 1.0
 * @since SOFI 2.0.2 Ajout du mode lazy pour les objets cache
 */
public class CacheFactory {
  /**
   * L'instance de journalisation pour cette classe
   */
  static private Log log = LogFactory.getLog(CacheFactory.class);

  /** Constructeur par défaut */
  public CacheFactory() {
    super();
  }

  /**
   * Retourne un instanciation d'un objet cache.
   * @param metadonneeCache l'objet possédant les méta-données pour les objets cache.
   * @return un objet cache.
   */
  public static synchronized ObjetCache get(MetaDonneeCache metadonneeCache, boolean chargerDonnees)
      throws NamingException, ClassNotFoundException, NoSuchMethodException,
      IllegalAccessException, InvocationTargetException {
    ObjetCache objetCache = null;

    // Instancier la classe ObjetCache.
    try {
      if ((metadonneeCache.getClasse() == null) &&
          (metadonneeCache.getNomDomaineValeur() != null)) {
        // Spécifier une classe de gestion des domaines de valeurs,
        // si pas d'autres de spécifié.
        metadonneeCache.setClasse(
            "org.sofiframework.application.domainevaleur.cache.ListeDomaineValeur");
      }

      Class objetCacheClass = Thread.currentThread().getContextClassLoader()
          .loadClass(metadonneeCache.getClasse());
      objetCache = (ObjetCache) objetCacheClass.newInstance();
      objetCache.setNom(metadonneeCache.getNom());
      objetCache.setContexte(metadonneeCache);
      if (chargerDonnees) {
        objetCache.chargerDonnees();
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error(UtilitaireException.getMessageExceptionAuComplet(e));
      }
    }

    // Retourner l'objet cache.
    return objetCache;
  }

  /**
   * Obtenir un ou plusieurs objets cache a partir d'une liste de contextes.
   * Les données des objets caches sont chargées immédiatement
   * @param contextesCaches liste de contexte des objets cache
   * @return un ou plusieurs objets cache
   */
  public static Hashtable get(ArrayList contextesCaches)
      throws Exception {
    return get(contextesCaches, true);
  }

  /**
   * Obtenir un ou plusieurs objets cache a partir d'une liste de contextes
   * @param contextesCaches liste de contexte des objets cache
   * @param chargerDonnees détermine si les données de l'objet cache sont
   * chargés systématiquement ou non (mode lazy)
   * @return un ou plusieurs objets cache
   */
  public static Hashtable get(ArrayList contextesCaches, boolean chargerDonnees)
      throws Exception {
    Hashtable caches = new Hashtable();

    for (Iterator i = contextesCaches.iterator(); i.hasNext();) {
      MetaDonneeCache contexte = (MetaDonneeCache) i.next();
      ObjetCache cache = CacheFactory.get(contexte, chargerDonnees);

      if ((cache != null) && !cache.isVide()) {
        caches.put(contexte.getNom(), cache);
      }
    }

    return caches;
  }

}
