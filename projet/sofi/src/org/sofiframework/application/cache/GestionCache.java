/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.domainevaleur.cache.ListeDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;
import org.w3c.dom.Node;

/**
 * Centralise la gestion des objets pouvant être mit en cache dans un singleton.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class GestionCache extends BaseGestionService {
  /**
   * L'instance de journalisation pour cette classe
   */
  private static final Log log = LogFactory.getLog(GestionCache.class);

  /** Instance unique (Singleton) */
  public static GestionCache gestionCache = new GestionCache();
  public Object serviceLocal = null;

  /** Une collection d'objet de cache identifier par une clé */
  private HashMap caches = new HashMap();

  /** Liste des contextes du fichier de configuration des objets caches **/
  private List listeContexte = null;

  /**
   * Est-ce que la gestion des objets cache est actif?
   */
  private boolean objetCacheActif = false;

  /**
   * Permet de charger les object cache en mode "lazy", c'est à dire sur demande.
   */
  private boolean chargementSurDemande = false;

  private GestionCache() {
  }

  /**
   * Retourner l'instance unique de GestionCache
   * 
   * @return GestionCache
   */
  public static GestionCache getInstance() {
    return gestionCache;
  }

  /**
   * Retourner une valeur valeur d'un objet de cache.
   * 
   * @param nomObjetCache
   * @param cle
   * @return la valeur d'un objet de cache.
   */
  public Object getObjetCache(String nomObjetCache, Object cle) {
    ObjetCache cache = getCache(nomObjetCache);
    return cache.get(cle);
  }

  public boolean isPresent(String nomObjetCache, String sousCache) {
    ObjetCache cache = (ObjetCache) caches.get(new CleCache(nomObjetCache, sousCache));
    boolean cacheNull = (cache == null);
    return !cacheNull;
  }

  /**
   * Ajouter une définition d'un nouvel objet cache a l'exécution.
   * 
   * @param nomObjetCache
   *          le nom de l'objet de cache.
   */
  public synchronized void ajouterDefinitionObjetCache(String nomObjetCache) {
    // Ajouter une nouvelle cache.
    if (!isPresent(nomObjetCache, null)) {
      caches.put(new CleCache(nomObjetCache), new ObjetCache(nomObjetCache));
    }
  }

  /**
   * Ajouter une valeur à un objet de cache.
   * 
   * @param nomObjetCache
   *          le nom de l'objet de cache.
   * @param cle
   *          la clé de la valeur.
   * @param valeur
   *          la valeur.
   */
  public synchronized ObjetCache ajouterObjetCache(String nomObjetCache, Object cle, Object valeur) {
    ObjetCache cache = getCache(nomObjetCache);
    cache.setDateCreation(new java.util.Date());
    cache.put(cle, valeur);
    return cache;
  }

  /**
   * Permet d'ajouter un nouvel objet cache avec une liste.
   * 
   * @param proprieteValeur
   *          la propriété de la valeur d'un élément de l'objet cache.
   * @param proprieteCle
   *          la clé de la valeur d'un élémemnt de l'objet cache.
   * @param liste
   *          la liste à ajouter dans l'objet cache.
   * @param nomObjetCache
   *          le nom de l'objet cache.
   */
  public synchronized void ajouterListeCache(String nomObjetCache, List liste, String proprieteCle,
      String proprieteValeur) {
    if (liste != null) {
      ObjetCache cache = new ObjetCache(nomObjetCache);

      Iterator iterateur = liste.iterator();

      while (iterateur.hasNext()) {
        ObjetTransfert objet = (ObjetTransfert) iterateur.next();
        Object cle = UtilitaireObjet.getValeurAttribut(objet, proprieteCle);
        Object valeur = UtilitaireObjet.getValeurAttribut(objet, proprieteValeur);
        cache.put(cle, valeur);
      }

      // Ajouter une nouvelle cache.
      caches.put(new CleCache(nomObjetCache), cache);
    }
  }

  /**
   * Charge un objet cache avec la liste des valeurs d'un domaine.
   * 
   * @param nomObjetCache
   *          Nom du domaine
   * @param liste
   *          Liste des valeurs
   */
  public synchronized void ajouterListeCachePourDomaineValeur(String nomObjetCache, List liste) {
    this.ajouterListeCachePourDomaineValeur(nomObjetCache, liste, null);
  }

  /**
   * Charge un objet cache avec la liste des valeurs d'un domaine.
   * 
   * @param nomObjetCache
   *          Nom du domaine
   * @param liste
   *          Liste des valeurs
   * @since SOFI 2.0.3
   */
  @SuppressWarnings("unchecked")
  public synchronized void ajouterListeCachePourDomaineValeur(String nomObjetCache, List liste, Object sousCache) {
    if (liste != null) {
      ObjetCache cache = getCache(nomObjetCache, false);
      if (cache == null) {
        cache = new ObjetCache(nomObjetCache);
      }else {
        cache.setDateCreation(new java.util.Date());
      }

      for (Iterator i = liste.iterator(); i.hasNext();) {
        DomaineValeur valeur = (DomaineValeur) i.next();
        cache.put(valeur.getValeur(), valeur);
      }

      // Ajouter une nouvelle cache. Gérer les sous Caches

      caches.put(new CleCache(nomObjetCache), cache);
    }
  }

  /**
   * Retourner un objet de cache qui contient les valeurs de la cache.
   * 
   * @param nomObjetCache
   * @return un objet de cache
   */
  public ObjetCache getCacheSansRechargement(String nomObjetCache) {
    return getCache(nomObjetCache, null, null, true, true);
  }

  /**
   * Retourner un objet de cache qui contient les valeurs de la cache.
   * 
   * @param nomObjetCache
   * @return un objet de cache
   */
  public ObjetCache getCache(String nomObjetCache) {
    return getCache(nomObjetCache, null, null, true, false);
  }

  /**
   * @since 2.0.3
   */
  public ObjetCache getCache(String nomObjetCache, Object sousCache) {
    return getCache(nomObjetCache, sousCache, null, true, false);
  }

  public ObjetCache getCache(String nomObjetCache, Object sousCache, String codeClient) {
    return getCache(nomObjetCache, sousCache, codeClient, true, false);
  }

  /**
   * Retourner un objet de cache qui contient les valeurs de la cache.
   * 
   * @param nomObjetCache
   * @return un objet de cache
   */
  public ObjetCache getCache(String nomObjetCache, boolean chargementSiVide) {
    return getCache(nomObjetCache, null, null, chargementSiVide, false);
  }

  public ObjetCache getCache(String nomObjetCache, Object sousCache, boolean chargementSiVide) {
    return getCache(nomObjetCache, null, null, chargementSiVide, false);
  }

  @SuppressWarnings("unchecked")
  public ObjetCache getCache(String nomObjetCache, Object sousCache, String codeClient, boolean chargementSiVide, boolean jamaisRecharge) {

    ObjetCache cache = (ObjetCache) caches.get(new CleCache(nomObjetCache, sousCache));
    
    if (!jamaisRecharge && cache != null && cache.isDirty()) {
      // Rafraichir la cache si changement de journée.
      rechargerCache(nomObjetCache, sousCache);
    }
    
    boolean cacheNull = (cache == null);

    if (cacheNull && chargementSiVide) {
      /*
       * Il faut gérer le cas ou on a pas la liste pour le sous-cache mais on a le cache lui meme sans sous-cache
       */
      if (sousCache != null) {
        /*
         * Obtenir le cache ... le cloner car le cache parent est le prototype des enfants
         */
        // MetaDonneeCache contexte = this.getContexte(nomObjetCache);
        ObjetCache parent = (ObjetCache) caches.get(new CleCache(nomObjetCache));

        if (parent != null) {
          try {
            cache = CacheFactory.get(parent.getContexte(), false);
            cache.setSousCache(sousCache);
            parent.ajouterEnfant(cache);
            cacheNull = false;
          } catch (Exception e) {
            if (log.isErrorEnabled()) {
              log.error("Erreur lors du chargement d'une cache.", e);
            }
          }
        }
      } else {
        // On fait une instance de cache vide
        cache = new ObjetCache(nomObjetCache);
      }

      if (!cacheNull) {
        caches.put(new CleCache(nomObjetCache, sousCache), cache);
      }
    }

    if (!cacheNull) {
      if (!cache.isEnErreur()) {
        if (cache.isVide() && chargementSiVide) {
          try {
            if (cache.getContexte() != null && cache.getContexte().getNomDomaineValeur() != null) {
              cache = CacheFactory.get(cache.getContexte(), false);
            }
            cache.effectuerChargement();
          } catch (Exception e) {
            cache.setEnErreur(true);
            if (log.isErrorEnabled()) {
              log.error("Erreur lors du chargement d'une cache.", e);
            }
          }
        }
      } else {
        if (log.isWarnEnabled()) {
          log.warn("Cache " + cache.getNom() + " en erreur.");
        }
      }

      // Cloisonnement par client pour les domaines de valeur
      if (cache instanceof ListeDomaineValeur) {
        // On clone l'objet cache pour ne toucher à celui qui est dans le cache.
        cache = (ObjetCache) UtilitaireObjet.clonerObjet(cache);
        LinkedHashMap collection = filtrerParCodeClient(cache.getCollection(), codeClient);
        cache.setCollection(collection);
      }
    }

    return cache;
  }

  private LinkedHashMap filtrerParCodeClient(LinkedHashMap collection, String codeClient) {
    if (collection != null && !collection.isEmpty()) {
      for (Iterator i = collection.keySet().iterator(); i.hasNext();) {
        DomaineValeur domaineValeur = (DomaineValeur) collection.get(i.next());
        if (domaineValeur.getCodeClient() != null
            && !GestionSecurite.getInstance().isClientAscendant(domaineValeur.getCodeClient(), codeClient)) {
          i.remove();
        }
      }
    }
    return collection;
  }

  /**
   * Obtenir un contexte.
   * 
   * @param nomCache
   *          Nom de cache
   * @return Meta données de cache
   */
  public MetaDonneeCache getContexte(String nomCache) {
    MetaDonneeCache meta = null;

    if (this.listeContexte != null) {
      boolean trouve = false;
      for (Iterator i = this.listeContexte.iterator(); i.hasNext() && !trouve;) {
        meta = (MetaDonneeCache) i.next();
        if (meta.getNom().equals(nomCache)) {
          trouve = true;
        }
      }
    }

    return meta;
  }

  /**
   * Permet de recharger un cache.
   */
  public void rechargerCache(String nomObjetCache) {
    this.rechargerCache(nomObjetCache, null);
  }

  /**
   * Permet de recharger un cache.
   * 
   * @since SOFI 2.0.3
   */
  public void rechargerCache(String nomObjetCache, Object sousCache) {
    ObjetCache cache = (ObjetCache) this.caches.get(new CleCache(nomObjetCache, sousCache));
    LinkedHashMap collectionBase = null;
    
    if (cache != null) {
      collectionBase = cache.getCollection();
      cache.initialiser();
    }
    // effectue la charge des données
    cache = this.getCache(nomObjetCache, sousCache, true);
    
    if (cache.isEnErreur()) {
      cache.setEnErreur(false);
      if (collectionBase != null) {
        cache.setCollection(collectionBase);
      }
    }
  }

  /**
   * Obtenir la liste des noms des caches configurées.
   * 
   * @return la liste des noms des caches configurées, sous forme d'ArrayList de chaînes de caractères.
   */
  public ArrayList getListeCachesConfigurees() {
    ArrayList listeNomCache = new ArrayList();
    for (Iterator i = caches.keySet().iterator(); i.hasNext();) {
      listeNomCache.add(i.next().toString());
    }
    return listeNomCache;
  }

  /**
   * Retourne une collection des valeurs de l'objet cache.
   * <p>
   * La collection contient des occurences d'une instance de type ObjetCleValeur.
   * 
   * @param nomObjetCache
   * @return la liste des valeurs de l'objet cache.
   * @see org.sofiframework.objetstransfert.ObjetCleValeur
   */
  public static ArrayList getListeCleValeurs(String nomObjetCache) {
    return getListeCleValeurs(nomObjetCache, null);
  }

  /**
   * Retourne une collection des valeurs de l'objet cache.
   * <p>
   * La collection contient des occurences d'une instance de type ObjetCleValeur.
   * 
   * @param nomObjetCache
   * @param sousCache
   * @return la liste des valeurs de l'objet cache.
   * @see org.sofiframework.objetstransfert.ObjetCleValeur
   * @since SOFI 2.0.3
   */
  public static ArrayList getListeCleValeurs(String nomObjetCache, Object sousCache) {
    ArrayList listeValeursCache = new ArrayList();

    // Utilisation de la cache
    ObjetCache cache = GestionCache.getInstance().getCache(nomObjetCache, sousCache);

    Iterator iterateursCles = cache.iterateurCles();
    Iterator iterateurValeurs = cache.iterateurValeurs();

    while (iterateurValeurs.hasNext()) {
      Object valeur = iterateurValeurs.next();
      Object cle = iterateursCles.next();

      ObjetCleValeur objetCleValeur = new ObjetCleValeur();
      objetCleValeur.setObjetCle(cle);
      objetCleValeur.setObjetValeur(valeur);

      listeValeursCache.add(objetCleValeur);
    }

    return listeValeursCache;
  }

  public synchronized void lireFichierCache(String fichierConfiguration) throws Exception {
    // Obtenir tous les contextes qui sont dans le fichier de configuration
    listeContexte = MetaDonneeCache.getContextes(fichierConfiguration);
  }

  public synchronized void chargerDocumentXml(Node document) throws Exception {
    // Obtenir tous les contextes qui sont dans le document XML.
    listeContexte = MetaDonneeCache.getContextes(document);
  }

  public synchronized void chargerDocumentXml(InputStream in) throws Exception {
    // Obtenir tous les contextes qui sont dans le document XML.
    listeContexte = MetaDonneeCache.getContextes(in);
  }

  /**
   * Construire les méta-données pour les objets qui sont gérés par GestionCache.
   * 
   * @param fichierConfiguration
   * @throws java.lang.Exception
   */
  public synchronized void construire() throws Exception {
    if (log.isWarnEnabled()) {
      log.info("\n\nCHARGEMENT DES OBJETS CACHE!!\n\n");
    }

    if (this.chargementSurDemande && log.isWarnEnabled()) {
      log.info("Chargement des objets cache sur demande.");
    }

    caches.clear();

    // Construire un objet cache pour tous les ceux qui n'existent pas déjà
    if (listeContexte != null) {
      for (java.util.Iterator i = listeContexte.iterator(); i.hasNext();) {
        MetaDonneeCache contexte = (MetaDonneeCache) i.next();

        // Contruire un nouvel objet cache.
        if (!caches.containsKey(new CleCache(contexte.getNom())) || !objetCacheActif) {
          ObjetCache cache = CacheFactory.get(contexte, !this.chargementSurDemande);

          // Fixer le contexte associé à l'objet cache.
          cache.setContexte(contexte);

          ObjetCache cacheCourante = this.getCache(contexte.getNom(), !this.chargementSurDemande);

          if (cacheCourante != null) {
            cacheCourante.setContexte(contexte);
          }

          if ((cache != null) && ((cacheCourante == null) || (cacheCourante.getCollection().size() == 0))) {
            caches.put(new CleCache(contexte.getNom()), cache);
          }
        }
      }
    }

    if (log.isWarnEnabled()) {
      log.info("\n\nCHARGEMENT DES OBJETS CACHE TERMINÉ\n\n");
    }

    objetCacheActif = true;
  }

  public synchronized void construire(Object serviceLocal) throws Exception {
    // Si on est en chargement sur demande le modele est toujours bon on
    // ne doit pas le remplacer.
    // Sinon on doit toujours mettre a jour le modele.
    if (this.serviceLocal == null || !this.chargementSurDemande) {
      this.serviceLocal = serviceLocal;
    }

    this.construire();
  }

  /**
   * Est-ce que le gestionnaire de cache est actif.
   * 
   * @return true si le gestionnaire de cache est actif.
   */
  public boolean isObjetCacheActif() {
    return objetCacheActif;
  }

  /**
   * Initialiser à neuf les objets cache.
   */
  public synchronized void initialiserObjetCache() {
    this.objetCacheActif = false;

    if (log.isWarnEnabled()) {
      log.info("\n\nATTENTION!! Les objets caches seront rechargés par le prochain utilisateur...\n\n");
    }
  }

  /**
   * Retourne true si le gestionnaire de cache est configure.
   * 
   * @param nomService
   *          le nom du service qui instancie les objets cache.
   * @return true si le gestionnaire de cache est configuré.
   */
  public boolean isGestionGestionCacheConfigue(String nomService) {
    HashSet listeServiceConfigures = getServicesConfigures();

    if (listeServiceConfigures.contains(nomService)) {
      return true;
    }

    listeServiceConfigures.add(nomService);

    return false;
  }

  @Override
  public void setServiceLocal(Object serviceLocal) {
    this.serviceLocal = serviceLocal;
  }

  public Object getServiceLocal() {
    return serviceLocal;
  }

  /**
   * Vérifier si l'objet cache existe dans les caches disponibles.
   * 
   * @param nomObjetCache
   *          le nom de la cache a vérifier
   * @return vrai si la cache a été trouvée, sinon faux
   */
  public boolean isObjetCacheExiste(String nomObjetCache) {
    Object objet = caches.get(nomObjetCache);
    boolean trouver = false;

    if (objet == null) {
      // verifier pour un souscache
      for (Iterator cles = this.caches.keySet().iterator(); cles.hasNext() && !trouver;) {
        CleCache cle = (CleCache) cles.next();
        if (cle.getNomCache().equals(nomObjetCache)) {
          trouver = true;
        }
      }
    } else {
      trouver = true;
    }

    return trouver;
  }

  /**
   * Méthode permettant de récupérer un domaine de valeur dans la cache, et de contruire une liste navigation avec.
   * 
   * @param cache
   *          Cache que l'on désire récupérer
   * @param description
   *          Si jamais on ne veut que certaines valeurs de la cache, on peut spécifier une description, on ne gardera
   *          que les domaines de valeur dont la description contienne celle passée en paramètre.
   * @param liste
   *          {@link ListeNavigation} à populer
   * @param codeLangue
   *          Code langue de la description voule. Si non spécifié, on prend les valeurs FR
   * @throws ClassCastException
   *           Si jamais la cache spécifiée ne correspond pas à un domaine de valeur, on lance une exception.
   */
  @SuppressWarnings("unchecked")
  public void getListeNavigation(String cache, String description, ListeNavigation liste, String codeLangue)
      throws ClassCastException {
    ListeDomaineValeur listeDomaine;
    try {
      listeDomaine = (ListeDomaineValeur) GestionCache.getInstance().getCache(cache);
    } catch (ClassCastException e) {
      throw new ClassCastException("La cache désirée ne correspond pas à un domaine de valeur.");
    }
    List<DomaineValeur> listeDomaineValeur = listeDomaine.getListeValeur();

    if (UtilitaireString.isVide(codeLangue)) {
      codeLangue = "fr";
    }

    // Raffiner la liste
    if (!UtilitaireString.isVide(description)) {
      for (int index = 0; index < listeDomaineValeur.size(); index++) {
        DomaineValeur domaine = listeDomaineValeur.get(index);
        domaine.setDescription(domaine.getDescription(codeLangue.toLowerCase()));
        if (!StringUtils.contains(StringUtils.upperCase(domaine.getDescription()), StringUtils.upperCase(description))) {
          listeDomaineValeur.remove(index);
          index--;
        }
      }
    }

    int debutTranche = liste.getPositionListe();
    int finTranche = debutTranche + liste.getMaxParPage();

    if (finTranche > listeDomaineValeur.size()) {
      finTranche = listeDomaineValeur.size();
    }

    List<DomaineValeur> listeNav = new ArrayList<DomaineValeur>();
    for (int index = debutTranche; index < finTranche; index++) {
      DomaineValeur domaine = listeDomaineValeur.get(index);
      domaine.setDescription(domaine.getDescription(codeLangue.toLowerCase()));
      listeNav.add(domaine);
    }
    liste.setListe(listeNav);
    liste.setNbListe(listeDomaineValeur.size());
  }

  public boolean isChargementSurDemande() {
    return chargementSurDemande;
  }

  public void setChargementSurDemande(boolean chargementSurDemande) {
    this.chargementSurDemande = chargementSurDemande;
  }

  /**
   * Fixer une liste de contextes (MetaDonneeCache) pour le chargement d'objets cache.
   * 
   * @param listeContexte
   *          Liste de contextes
   */
  public void setListeContexte(List listeContextes) {
    this.listeContexte = listeContextes;
  }

  /**
   * Obtenir la liste des contextes meta données cache.
   * 
   * @return Liste des contextes
   */
  public List getListeContexte() {
    return this.listeContexte;
  }
}
