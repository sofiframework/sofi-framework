package org.sofiframework.application.cache;

import java.util.Iterator;

import org.sofiframework.application.locale.objetstransfert.LocaleApplication;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Application;


/**
 * Liste en cache des locales disponibles pour l'application en cours.
 * @author jfbrassard
 * @since 3.0.2
 */

public class ListeLocaleCache extends ObjetCache {

  private static final long serialVersionUID = -2960356889054856626L;

  @Override
  public void chargerDonnees() {

    Application application = GestionSecurite.getInstance().getApplication();

    Iterator<LocaleApplication> iterateur = application.getListeLocales().iterator();
    while (iterateur.hasNext()) {
      LocaleApplication localeApplication = iterateur.next();
      put(localeApplication.getCodeLocale(),localeApplication.getCleLibelle());
    }

  }

}

