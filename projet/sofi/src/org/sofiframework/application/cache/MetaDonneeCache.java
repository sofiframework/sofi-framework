/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireXml;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


/**
 * Classe spécifiant les méta-données d'une configuration d'un objet cache.
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class MetaDonneeCache extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 3358855828741988710L;

  /**
   * Nom de la balise qui délimite la liste des noeuds qui représentent les
   * objets cache.
   */
  public static final String NOM_LISTE_NOEUD_XML = "listeCache";

  /**
   * Nom de l'objet cache.
   */
  private String nom;

  /**
   * Le nom de classe de l'objet transfert en cache.
   */
  private String classe;

  /**
   * Le nom du domaine de valeur
   */
  private String nomDomaineValeur;

  /**
   * La valeur du domaine de valeur;
   */
  private String valeurDomaineValeur;
  
  /**
   * Permet de désactiver le rechargement de la cache après une journée.
   * <p>
   * Par défaut, la cache est rechargé au bout de 24h, mais si ce parametre est setter à false, la cache n'expire jamais.
   */
  private String expirationCache;

  /**
   * Contructeur par défault
   */
  public MetaDonneeCache() {
    ajouterAliasXml("cache", MetaDonneeCache.class);
  }

  /**
   * Obtenir le nom de l'objet cache.
   *
   * @return Nom de l'objet cache.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'objet de cache.
   *
   * @param nom
   *          le nouveau nom de l'objet cache.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le nom de classe de l'objet transfert en cache.
   *
   * @return le nom de classe de l'objet transfert en cache
   */
  public String getClasse() {
    return classe;
  }

  /**
   * Fixer le nom de classe de l'objet transfert en cache.
   *
   * @param classe
   *          le nom de classe de l'objet transfert en cache
   */
  public void setClasse(String classe) {
    this.classe = classe;
  }

  /**
   * Permet de produire une liste de contextes à partir d'un fichier de
   * configuration contenant un ou plusieurs configurations de services distants
   *
   * @param fichierConfiguration
   *          Emplacement d'un fichier de configuration d'objets de cache.
   * @return Liste de contextes permettant de construire les objets de cache.
   */
  public static List getContextes(String fichierParametres)
      throws IOException {
    FileReader reader = new FileReader(fichierParametres);

    try {
      return getContextes(reader);
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
      }
    }
  }

  /**
   * Permet de produire une liste de contextes d'un document XML contenant un ou
   * plusieurs configurations de services distants.
   *
   * @param listeNoeuds
   *          Document XML.
   * @return Liste de contextes permettant de construire les objets de cache.
   */
  public static List getContextes(Node parentNode)
      throws SAXException, IOException, ParserConfigurationException {
    ArrayList listeNoeuds = UtilitaireXml.getListeNodeDansUneNode(parentNode,
        NOM_LISTE_NOEUD_XML);

    // liste de contextes
    ArrayList listeContexte = new ArrayList();

    for (Iterator i = listeNoeuds.iterator(); i.hasNext();) {
      Node noeud = (Node) i.next();
      MetaDonneeCache contexte = new MetaDonneeCache();
      contexte.ajouterAliasXml("cache", MetaDonneeCache.class);
      contexte.lireXml(noeud);
      listeContexte.add(contexte);
    }

    return listeContexte;
  }

  public static List getContextes(InputStream in) throws IOException {
    return getContextes(new InputStreamReader(in));
  }

  public static List getContextes(Reader reader) throws IOException {
    XStream xstream = new XStream(new DomDriver());
    xstream.alias(NOM_LISTE_NOEUD_XML, List.class);
    xstream.alias("cache", MetaDonneeCache.class);

    return (List) xstream.fromXML(reader);
  }

  /**
   * Fixer le nom du domaine de valeur.
   * @param nomDomaineValeur le nom du domaine de valeur.
   */
  public void setNomDomaineValeur(String nomDomaineValeur) {
    this.nomDomaineValeur = nomDomaineValeur;
  }

  /**
   * Retourne le nom du domaine de valeur.
   * @return le nom du domaine de valeur.
   */
  public String getNomDomaineValeur() {
    return nomDomaineValeur;
  }

  /**
   * Fixer la valeur du domaine de valeur.
   * @param valeurDomaineValeur la valeur du domaine de valeur.
   */
  public void setValeurDomaineValeur(String valeurDomaineValeur) {
    this.valeurDomaineValeur = valeurDomaineValeur;
  }

  /**
   * Retourne la valeur du domaine de valeur.
   * @return la valeur du domaine de valeur.
   */
  public String getValeurDomaineValeur() {
    return valeurDomaineValeur;
  }

  public String getExpirationCache() {
    return expirationCache;
  }

  public void setExpirationCache(String expirationCache) {
    this.expirationCache = expirationCache;
  }
}
