/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;


public class UtilitaireCache {


  /**
   * Retourne la description d'un domaine de valeur selon la langue du locale
   * de l'utilisateur.
   * @return la description d'un domaine.
   * @param locale le locale de l'utilisateur.
   * @param code le code du domaine de valeur désiré.
   * @param nomCache le nom de la cache du domaine de valeur.
   */
  static public String getDescriptionDomaineValeur(String nomCache,
      String code, Locale locale) {
    // Utilisation de la cache
    ObjetCache cache = GestionCache.getInstance().getCache(nomCache);

    // Extraire la valeur du code de valeur.
    Object valeur = cache.get(code);


    Locale localeDisponible = GestionLibelle.getInstance().getLocaleDisponible(locale.toString());

    String description = null;

    if (DomaineValeur.class.isInstance(valeur)) {
      DomaineValeur domaineValeur = (DomaineValeur) valeur;

      if ((domaineValeur.getListeDescriptionLangue() != null) &&
          (domaineValeur.getListeDescriptionLangue().size() > 0)) {
        description = (String) domaineValeur.getListeDescriptionLangue().get(localeDisponible.toString());
      }
    }

    return description;
  }
  
  /**
   * Retourne la description d'un domaine de valeur selon la langue du locale
   * de l'utilisateur.
   * @return la description d'un domaine.
   * @param locale le locale de l'utilisateur.
   * @param code le code du domaine de valeur désiré.
   * @param nomCache le nom de la cache du domaine de valeur.
   * @param nomSousCache le nom de la sous cache du domaine de valeur
   */
  static public String getDescriptionDomaineValeur(String nomCache, String nomSousCache,
      String code, Locale locale) {
    // Utilisation de la cache
    ObjetCache cache = GestionCache.getInstance().getCache(nomCache, nomSousCache);

    // Extraire la valeur du code de valeur.
    Object valeur = cache.get(code);


    Locale localeDisponible = GestionLibelle.getInstance().getLocaleDisponible(locale.toString());

    String description = null;

    if (DomaineValeur.class.isInstance(valeur)) {
      DomaineValeur domaineValeur = (DomaineValeur) valeur;

      if ((domaineValeur.getListeDescriptionLangue() != null) &&
          (domaineValeur.getListeDescriptionLangue().size() > 0)) {
        description = (String) domaineValeur.getListeDescriptionLangue().get(localeDisponible.toString());
      }
    }

    return description;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlListeNomCache(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlListeNom", true);
    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlListeNom");
    }

    return parametre;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlMaj(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlMaj", true);

    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlMaj");
    }

    return parametre;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlMajLibelle(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlMajLibelle", true);

    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlMajLibelle");
    }

    return parametre;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlMajMessage(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlMajMessage",true);

    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlMajMessage");
    }

    return parametre;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlMajParametreSysteme(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlMajParametreSysteme",true);

    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlMajParametreSysteme");
    }

    return parametre;
  }

  /**
   * 
   * @param codeApplication
   * @return
   */
  static public String getUrlRecharger(String codeApplication) {
    String parametre = GestionParametreSysteme.getInstance().getParametreSysteme(codeApplication, "cacheUrlRecharger",true);

    if (StringUtils.isEmpty(parametre)) {
      parametre = GestionParametreSysteme.getInstance().getParametreSysteme("INFRA_SOFI", "cacheUrlRecharger");
    }

    return parametre;
  }
}
