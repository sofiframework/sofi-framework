/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.composantweb.liste.AttributTri;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.utilitaire.ComparateurPaire;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet spécifiant une instance de cache.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class ObjetCache implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -1882719017929389285L;

  /**
   * Instance commune de journalisation.
   */
  protected static Log log = LogFactory.getLog(ObjetCache.class);

  /**
   * Le nom de la cache
   */
  private String nom = null;

  /**
   * L'identifiant du sous-cache.
   */
  private Object sousCache = null;

  /**
   * La collection de cache.
   */
  protected LinkedHashMap collection = new LinkedHashMap();

  /**
   * Un set pour déterminer si les valeurs sont actives.
   * <p>
   * Pour ne pas avoir à modifier trop sofi, on travaille avec une deuxième entrée.
   */
  protected Set entreesInactives = new HashSet();

  /**
   * Le contexte associé à l'objet cache.
   */
  MetaDonneeCache contexte = null;

  /**
   * Est-ce que cette instance de cache est en erreur.
   */
  boolean enErreur = false;

  /**
   * @since SOFI 2.0.3
   */
  private Set enfants = null;
  
  private Date dateCreation = new java.util.Date();

  /**
   * Constructeur par défaut.
   */
  public ObjetCache() {
    setDateCreation(new java.util.Date());
  }

  /**
   * Constructeur avec nom de cache spécifié
   * 
   * @param nomCache
   *          le nom de la cache
   */
  public ObjetCache(String nomCache) {
    this.nom = nomCache;
  }

  /**
   * Constructeur avec nom de cache et un sous-cache
   * 
   * @param nomCache
   *          le nom de la cache
   * @param sousCache
   *          l'identifiant du sous-cache
   */
  public ObjetCache(String nomCache, Object sousCache) {
    this.nom = nomCache;
    this.sousCache = sousCache;
  }

  /**
   * Vide les valeurs courantes de l'objet cache. Réinitialise la valeur enErreur à false.
   */
  public void initialiser() {
    this.collection = new LinkedHashMap();
    this.enErreur = false;
    /*
     * Initialiser les enfants aussi
     */
    if (this.enfants != null) {
      for (Iterator i = this.enfants.iterator(); i.hasNext();) {
        ObjetCache enfant = (ObjetCache) i.next();
        enfant.initialiser();
      }
    }
  }

  /**
   * Ajouter une clé, valeur à l'objet de cache
   * 
   * @param cle
   *          la clé
   * @param valeur
   *          la valeur
   */
  public void put(Object cle, Object valeur) {
    collection.put(cle, valeur);
  }

  /**
   * Retourne la valeur de la cache pour la clé
   * 
   * @param cle
   *          la clé
   * @return la valeur de la cache
   */
  public Object get(Object cle) {
    return collection.get(cle);
  }

  /**
   * Un itérateur sur les valeurs de la cache.
   * 
   * @return un itérateur sur les valeurs de la cache.
   */
  public Iterator iterateurValeurs() {
    return collection.values().iterator();
  }

  /**
   * Un itérateur sur les clés de la cache.
   * 
   * @return un itérateur sur les clés de la cache.
   */
  public Iterator iterateurCles() {
    return collection.keySet().iterator();
  }

  /**
   * Retourne la clé par défaut de la cache.
   * <p>
   * La clé par défaut correspond à la première clé insérer dans la cache.
   * <p>
   * SOFI 2.0.3 - Si un domaine doit etre utilisé en spécifiant une valeur par defaut surcharger cette méthode en
   * héritant de ListeDomaineValeur. On doit aussi utiliser le parametre de la balise select activerCleDefaut="true".
   * </p>
   */
  public Object getCleDefaut() {
    if ((collection != null) && (collection.size() > 0)) {
      return collection.keySet().iterator().next();
    }
    return null;
  }

  /**
   * Retourne le nom de la cache.
   * 
   * @return le nom de la cache
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de la cache.
   * 
   * @param nom
   *          le nom de la cache
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Cette classe peut être réécris par une sous-classe. Elle doit être implantée pour obtenir et placer les données
   * dans le cache.
   */
  public void chargerDonnees() {
  }

  /**
   * Cette classe peut être réécris par une sous-classe. Elle doit être implantée pour obtenir et placer les données
   * dans le cache, lorsqu'un identifiant de sous-cache est utilisé.
   * 
   * @since SOFI 2.0.3
   */
  public void chargerDonnees(Object sousCache) {
  }

  /**
   * Execute le chargement du cache avec ou sans sous-cache.
   * 
   * @since SOFI 2.0.3
   */
  public void effectuerChargement() {
    if (this.sousCache != null) {
      this.chargerDonnees(sousCache);
    } else {
      this.chargerDonnees();
    }
  }

  /**
   * Retourne le service distant
   * 
   * @param nom
   *          le nom du service distant
   * @return EJBObject un service distant
   */
  protected Object getServiceDistant(String nom) {
    return GestionServiceDistant.getServiceDistant(nom);
  }

  /**
   * Retourne le service local (soit le modèle principal)
   * 
   * @return le service local (soit le modèle principal)
   */
  protected Object getServiceLocal() {
    return GestionCache.getInstance().getServiceLocal();
  }

  /**
   * Retourne true si la collection de l'objet cache est vide, sinon retourne false
   * 
   * @return boolean
   */
  public boolean isVide() {
    return collection.isEmpty();
  }

  /**
   * Retourne la collection de l'objet cache.
   * 
   * @return la collection de l'objet cache.
   */
  public LinkedHashMap getCollection() {
    return this.collection;
  }

  /**
   * Fixer la collection de l'objet cache.
   * 
   * @param collection
   *          La collection de l'objet cache.
   */
  public void setCollection(LinkedHashMap collection) {
    this.collection = collection;
  }

  /**
   * On crée une copie de la collection où on y filtre les inactifs.
   * 
   * @param valeursAGarder
   *          Spécifie une ou des valeurs qui doivent être présents dans la collection peu importe s'il sont inactifs.
   * @return Une copie de la collection avec les inactifs retirés.
   */
  public LinkedHashMap getCollectionSansInactifs(String... valeursAGarder) {
    LinkedHashMap map = new LinkedHashMap(collection);

    List<String> listeValeursAGarder = Arrays.asList(valeursAGarder);

    // Pour chacune des valeurs inactives...
    for (Object o : entreesInactives) {
      // ... on la retire sauf s'il faut la garder (via le paramètre).
      if (!listeValeursAGarder.contains(o)) {
        map.remove(o);
      }
    }
    return map;
  }

  /**
   * Ajoute une valeur de domaine de valeur (clé) comme valeur inactive.
   * 
   * @param cle
   *          La valeur à ajouter dans la liste des inactives.
   */
  public void ajouterValeurInactive(Object cle) {
    entreesInactives.add(cle);
  }

  public void retirerValeurInactive(Object cle) {
    if (entreesInactives.contains(cle)) {
      entreesInactives.remove(cle);
    }
  }

  /**
   * Obtenir la liste des valeurs.
   * 
   * @return valeurs
   * @since SOFI 2.0.1
   */
  public List getListeValeur() {
    ArrayList liste = null;

    for (Iterator i = this.iterateurValeurs(); i.hasNext();) {
      if (liste == null) {
        liste = new ArrayList();
      }

      liste.add(i.next());
    }

    return liste;
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   */
  public void trierListeValeur() {
    this.trierListeValeur((Comparator) null);
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel avec l'aide d'un collator. Le collator permet de
   * configurer la précision du tri pour l'ordonnement des String.
   */
  public void trierListeValeur(Collator collator) {
    this.trierLinkedHashSet(new ComparateurPaire(collator));
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   */
  public void trierListeValeur(final AttributTri[] tri) {
    this.trierListeValeur(tri, null);
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel. Le collator permet de configurer la précision du tri pour
   * l'ordonnement des String.
   */
  public void trierListeValeur(final AttributTri[] tri, final Collator collator) {
    /*
     * On crée un comparateur qui évalue chaque propriété jusqu'a ce qu'on obtienne une différence.
     */
    Comparator c = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        /*
         * On compare chaque propriété jusqu'a ce qu'une soit différente de l'autre.
         */
        int resultat = 0;

        for (int i = 0; (i < tri.length) && (resultat == 0); i++) {
          AttributTri attribut = tri[i];
          String nom = UtilitaireString.convertirPremiereLettreEnMinuscule(attribut.getNom());
          boolean descendant = !attribut.isAscendant();
          Object val1 = UtilitaireObjet.getValeurAttribut(o1, nom);
          Object val2 = UtilitaireObjet.getValeurAttribut(o2, nom);
          resultat = UtilitaireListe.compareValeur(val1, val2, collator);
          resultat = (descendant ? (-resultat) : resultat);
        }

        return resultat;
      }
    };

    /*
     * Trier avec ce comparateur
     */
    this.trierListeValeur(c);
  }

  /**
   * Ajouter un objet cache enfant.
   * 
   * @param cacheEnfant
   *          Objet Cache enfant
   */
  public void ajouterEnfant(ObjetCache cacheEnfant) {
    if (this.enfants == null) {
      this.enfants = new HashSet();
    }
    this.enfants.add(cacheEnfant);
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   * 
   * @param c
   *          Comparator qui doit être utilisé pour le tri
   */
  public void trierListeValeur(final Comparator comparateur) {
    this.trierLinkedHashSet(new ComparateurPaire(comparateur));
  }

  /*
   * Tri des éléments de la collection du tri.
   */
  private void trierLinkedHashSet(ComparateurPaire comparateur) {
    if ((this.collection != null) && (this.collection.size() > 1)) {
      /*
       * Composer les paires clé valeur
       */
      List listeValeur = new ArrayList();

      for (Iterator i = this.collection.keySet().iterator(); i.hasNext();) {
        Object cle = i.next();
        listeValeur.add(new Object[] { cle, this.collection.get(cle) });
      }

      Collections.sort(listeValeur, comparateur);

      /*
       * Refaire une LinkedHashMap dans le nouvel ordre
       */
      LinkedHashMap apresTri = new LinkedHashMap();

      for (Iterator i = listeValeur.iterator(); i.hasNext();) {
        Object[] paire = (Object[]) i.next();
        Object cle = paire[0];
        Object valeur = paire[1];
        apresTri.put(cle, valeur);
      }

      this.collection = apresTri;
    }
  }

  /**
   * Fixer le contexte de l'objet cache chargé par le fichier de configuration XML.
   * 
   * @param contexte
   *          le contexte de l'objet cache chargé par le fichier de configuration XML.
   */
  public void setContexte(MetaDonneeCache contexte) {
    this.contexte = contexte;
  }

  /**
   * Retourne le contexte de l'objet cache chargé par le fichier de configuration XML.
   * 
   * @return le contexte de l'objet cache chargé par le fichier de configuration XML.
   */
  public MetaDonneeCache getContexte() {
    return contexte;
  }

  public boolean isEnErreur() {
    return enErreur;
  }

  public void setEnErreur(boolean enErreur) {
    this.enErreur = enErreur;
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  public Object getSousCache() {
    return sousCache;
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  
  public Date getProchaineDateInvalidation() {
    java.util.Date dateCacheCourante = UtilitaireDate.enleverHeures(this.dateCreation);
    Calendar dateProchaineInvalidation = Calendar.getInstance();
    dateProchaineInvalidation.setTime(dateCacheCourante);
    dateProchaineInvalidation.add(Calendar.DATE, 1);
    dateProchaineInvalidation.add(Calendar.MINUTE, 5);
    return dateProchaineInvalidation.getTime();
  }
  
  public boolean isDirty() {
    
    if (this.contexte != null
        && StringUtils.isNotBlank(this.contexte.getExpirationCache())
        && !Boolean.parseBoolean(this.contexte.getExpirationCache())) {
      return false;
    }
    
    java.util.Date now = new java.util.Date();
    java.util.Date dateInvalidation = getProchaineDateInvalidation();
    boolean dirty = now.after(dateInvalidation);
    if (dirty) {
      this.dateCreation = new java.util.Date();
    }
    return dirty;
  }

}
