/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.surveillance;

import java.util.Date;

import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Une erreur produite par une application.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class ErreurApplication extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 1939994660012135521L;

  /**
   * le code de l'erreur
   */
  private String code;

  /**
   * La description de l'erreur.
   */
  private String message;

  /**
   * L'utilisateur qui a produit l'erreur.
   */
  private Object utilisateur;

  /**
   * La date de l'erreur.
   */
  private Date dateHeure;

  /**
   * Le nom de la page ou s'est produit l'erreur.
   */
  private String nomPageErreur;

  /**
   * La méthode qui a produit l'erreur.
   */
  private String methode;

  /**
   * Le numéro unique de l'erreur
   */
  private Integer no;

  /**
   * Le nom de l'utilisateur ayant eu l'erreur.
   */
  private String nomUtilisateur;

  /**
   * Constructeur par défault
   */
  public ErreurApplication() {
  }

  /**
   * Obtenir le code d'erreur.
   * @return le code d'erreur
   */
  public String getCode() {
    return code;
  }

  /**
   * Fixer le code d'erreur.
   * @param newCode le nouveau code d'erreur.
   */
  public void setCode(String newCode) {
    code = newCode;
  }

  /**
   * Obtenir la description de l'erreur.
   * @return la description de l'erreur
   */
  public String getMessage() {
    return message;
  }

  /**
   * Fixer la description de l'erreur
   * @param newMessage la nouvelle description de l'erreur
   */
  public void setMessage(String newMessage) {
    message = newMessage;
  }

  public String getMessageCourt() {
    if (message.length() > 90) {
      return message.substring(0, 90);
    }

    return message;
  }

  /**
   * Obtenir l'utilisateur qui a produit l'erreur.
   * @return l'utilisateur qui a produit l'erreur
   */
  public Object getUtilisateur() {
    return utilisateur;
  }

  /**
   * Fixer l'utilisateur qui a produit l'erreur
   * @param newUtilisateur le nouvel utilisateur qui a produit l'erreur
   */
  public void setUtilisateur(Object utilisateur) {
    if (utilisateur instanceof UtilisateurSession) {
      UtilisateurSession utilisateurSession = (UtilisateurSession) utilisateur;
      this.setNomUtilisateur(utilisateurSession.getNomComplet());
    }

    this.utilisateur = utilisateur;
  }

  /**
   * Obtenir la date de l'erreur.
   * @return la date de l'erreur
   */
  public Date getDateHeure() {
    return dateHeure;
  }

  /**
   * Fixer la date de l'erreur.
   * @return la date de l'erreur
   */
  public String getDateHeureString() {
    if (getDateHeure() != null) {
      return UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(getDateHeure());
    } else {
      return "";
    }
  }

  /**
   * Fixer la date de l'erreur.
   * @param newDateHeure la nouvelle date de l'erreur
   */
  public void setDateHeure(Date newDateHeure) {
    dateHeure = newDateHeure;
  }

  /**
   * Le nom de la page ou s'est produit l'erreur.
   * @return le nom de la page ou s'est produit l'erreur
   */
  public String getNomPageErreur() {
    return nomPageErreur;
  }

  /**
   * Fixer le nom de la page ou s'est produit l'erreur.
   * @param newNomPageErreur le nouveau nom de la page ou s'est produit l'erreur
   */
  public void setNomPageErreur(String newNomPageErreur) {
    nomPageErreur = newNomPageErreur;
  }

  /**
   * Obtenir la méthode qui a produit l'erreur.
   * @return la méthode qui a produit l'erreur
   */
  public String getMethode() {
    return methode;
  }

  /**
   * Fixer la méthode qui a produit l'erreur
   * @param newMethode la nouvelle méthode qui a produit l'erreur
   */
  public void setMethode(String newMethode) {
    methode = newMethode;
  }

  public void setNo(Integer no) {
    this.no = no;
  }

  public Integer getNo() {
    return no;
  }

  public void setNomUtilisateur(String nomUtilisateur) {
    this.nomUtilisateur = nomUtilisateur;
  }

  public String getNomUtilisateur() {
    return nomUtilisateur;
  }
}
