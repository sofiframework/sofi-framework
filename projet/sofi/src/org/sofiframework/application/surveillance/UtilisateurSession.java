/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.surveillance;

import javax.servlet.http.HttpSession;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Classe représentant un utilisateur qui est présentement
 * en ligne dans l'application.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilisateurSession extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 4487540175829675981L;

  /**
   * L'objet décrivant un utilisateur.
   */
  private Object utilisateur;

  /**
   * La session HTTP de l'utilisateur.
   */
  transient HttpSession session;

  /**
   * La clé unique qui identifie l'utilisateur.
   */
  private Object cleUtilisateur;

  /**
   * Adresse IP de l'utilisateur.
   */
  private String adresseIP;

  /**
   * Feuille de style sélectionné.
   */
  private String styleCSS;

  /**
   * Le navigateur utilisé par l'utilisateur
   */
  private String navigateur;

  /**
   * Le nombre d'utilisateur authentifié sur ce code utilisateur.
   */
  private int nbUtilisateur;

  /**
   * La date de la création de la session
   */
  private long dateCreationSession;

  /**
   * La date de la dernière accès.
   */
  private long dateDerniereAcces;

  /**
   * La dernière action traité par l'utilisateur.
   */
  private String derniereAction;

  /**
   * Contructeur par défault.
   */
  public UtilisateurSession() {
    setNbUtilisateur(1);
  }

  /**
   * L'objet décrivant un utilisateur.
   * @return L'objet décrivant un utilisateur.
   */
  public Object getUtilisateur() {
    return utilisateur;
  }

  /**
   * L'objet décrivant un utilisateur.
   * @param newUtilisateur L'objet décrivant un utilisateur.
   */
  public void setUtilisateur(Object newUtilisateur) {
    utilisateur = newUtilisateur;
  }

  /**
   * La session HTTP de l'utilisateur.
   * @return La session HTTP de l'utilisateur.
   */
  public HttpSession getSession() {
    return session;
  }

  /**
   * La session HTTP de l'utilisateur.
   * @param newSession La session HTTP de l'utilisateur.
   */
  public void setSession(HttpSession newSession) {
    session = newSession;
  }

  /**
   * Obtenir la date formattée du dernier accès de l'utilisateur.
   * @return la date formattée du dernier accès de l'utilisateur
   */
  public String getDernierAcces() {
    java.util.Date dernierAcces = null;

    try {
      if (getSession() != null) {
        dernierAcces = new java.util.Date(getSession().getLastAccessedTime());
      } else {
        dernierAcces = new java.util.Date(getDateDerniereAcces());
      }

      return UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(dernierAcces);
    } catch (Exception e) {
      return "session invalidé";
    }
  }

  /**
   * Obtenir la date de création de l'accès initiale.
   * @return la date de création de l'accès initiale.
   */
  public String getCreationAcces() {
    long heureCreation = 0;

    try {
      if (getSession() != null) {
        heureCreation = getSession().getCreationTime();
      } else {
        heureCreation = getDateCreationSession();
      }

      return UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(new java.util.Date(
          heureCreation));
    } catch (Exception e) {
      return "session invalidé";
    }
  }

  /**
   * Obtenir la durée de l'accès de l'utilisatueur à l'application.
   * @return la durée de l'accès de l'utilisatueur à l'application
   */
  public String getDureeAcces() {
    long dureeAcces = getSession().getLastAccessedTime() -
        getSession().getCreationTime();

    return UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(new java.util.Date(
        dureeAcces)).substring(10);
  }

  /**
   * La clé unique qui identifie l'utilisateur.
   * @return La clé unique qui identifie l'utilisateur.
   */
  public Object getCleUtilisateur() {
    return cleUtilisateur;
  }

  /**
   * La clé unique qui identifie l'utilisateur.
   * @param newCle La clé unique qui identifie l'utilisateur.
   */
  public void setCleUtilisateur(Object newCleUtilisateur) {
    cleUtilisateur = newCleUtilisateur;
  }

  /**
   * Adresse IP de l'utilisateur.
   * @return Adresse IP de l'utilisateur.
   */
  public String getAdresseIP() {
    return adresseIP;
  }

  /**
   * Adresse IP de l'utilisateur.
   * @param adresseIP Adresse IP de l'utilisateur.
   */
  public void setAdresseIP(String adresseIP) {
    this.adresseIP = adresseIP;
  }

  /**
   * Retourne la derniere action de l'utilisateur.
   * @return la derniere action de l'utilisateur.
   */
  public String getDerniereAction() {
    try {
      String derniereAction = UtilitaireSession.getDerniereAdresseActionSelectionnne(this.session);
      return derniereAction;
    } catch (Exception e) {
      return derniereAction;
    }
  }

  public void setDerniereAction(String derniereAction) {
    this.derniereAction = derniereAction;
  }

  /**
   * Retourne le nom complet de l'utilisateur en ligne.
   * @return le nom complet de l'utilisateur en ligne.
   */
  public String getNomComplet() {
    Utilisateur utilisateur = (Utilisateur) getUtilisateur();

    return utilisateur.getNom() + ", " + utilisateur.getPrenom();
  }

  public String getStyleCSS() {
    return styleCSS;
  }

  public void setStyleCSS(String styleCSS) {
    this.styleCSS = styleCSS;
  }

  /**
   * Retourne le type de navigation de l'utilisateur.
   * @return le type de navigation de l'utilisateur.
   */
  public String getNavigateur() {
    return navigateur;
  }

  /**
   * Fixer le type de navigateur de l'utilisateur.
   * @param navigateur le type de navigateur de l'utilisateur.
   */
  public void setNavigateur(String navigateur) {
    this.navigateur = navigateur;
  }

  public void setNbUtilisateur(int nbUtilisateur) {
    this.nbUtilisateur = nbUtilisateur;
  }

  public int getNbUtilisateur() {
    return nbUtilisateur;
  }

  public void setDateCreationSession(long dateCreationSession) {
    this.dateCreationSession = dateCreationSession;
  }

  public long getDateCreationSession() {
    return dateCreationSession;
  }

  public void setDateDerniereAcces(long dateDerniereAcces) {
    this.dateDerniereAcces = dateDerniereAcces;
  }

  public long getDateDerniereAcces() {
    return dateDerniereAcces;
  }
}
