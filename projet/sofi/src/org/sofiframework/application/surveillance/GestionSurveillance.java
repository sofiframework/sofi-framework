/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.surveillance;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet permettant la surveillance du site.
 * Correspond à un SINGLETON(Une seule dans la JVM).
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since 3.1 Retirer la dépendance à ServletContext.
 */
public class GestionSurveillance {
  /**
   * Le nom du paramètre de la liste des utilisateurs actifs.
   */
  public static final String LISTE_UTILISATEUR_ACTIF = "surveillanceListeUtilisateurActif";

  /**
   * Le nom du paramètre de la liste des erreurs.
   */
  public static final String LISTE_ERREURS = "surveillanceListeErreurs";

  /**
   * L'instance unique du singleton de gestion de la surveillance.
   */
  private static GestionSurveillance refSurveillance = new GestionSurveillance();

  /**
   * L'instance de journalisation pour cette classe.
   */
  private static final Log log = LogFactory.getLog(GestionSurveillance.class);

  /**
   * Le nombre total de page consulté.
   */
  private int nbPageConsulte = 0;

  /**
   * Le nombre d'erreur produite.
   */
  private int nbPageEchec = 0;

  /**
   * L'indicateur de page en erreur.
   */
  private int indPageErreur = 0;

  /**
   * Le paramètre utilisateur.
   */
  private String paramUtilisateur;

  /**
   * Le paramètre de clé utilisateur.
   */
  private String paramCleUtilisateur;

  /**
   * La durée de la session en minute.
   */
  private Integer sessionEnMinutes;

  /**
   * Le Path correspondant au site de surveillance de l'application.
   */
  private String pathSiteSurveillance;
  private boolean actif;

  private  HashMap listeErreurs = new HashMap();
  private  HashMap listeUtilisateur = new HashMap();

  private int nbUserTotal = 0;

  private int nbUserActif = 0;

  private int nbUserOnlineActive = 0;

  private String dateDepartApplication = null;

  /**
   * Obtenir l'instance unique du singleton de gestinod e la surveillance.
   * @return l'ionstance de gestion de la surveillance
   */
  public static GestionSurveillance getInstance() {
    return refSurveillance;
  }

  /**
   * Gestion du nombre de page consulté.
   */
  public void ajouterUnePageConsulte() {
    nbPageConsulte++;
  }

  /**
   * Obtenir le nombre de page consulté.
   * @return le nombre de page consulté
   */
  public int getNbPageConsulte() {
    return nbPageConsulte;
  }

  /**
   * Obtenir le nombre de page en échec.
   * @return le nombre de page en échec
   */
  public int getNbPageEnEchec() {
    return nbPageEchec;
  }

  /**
   * Ajouter une erreur de page en échec.
   * @param erreurApplication erreur d'application
   */
  public void ajouterPageEnEchec(ErreurApplication erreurApplication) {
    try {
      // incrementer le nombre de page en echec
      nbPageEchec++;

      try {
        getListePageEnEchec().remove(new Integer(indPageErreur));
      } catch (Exception e) {
        //existe deja
      }

      Integer no = new Integer(indPageErreur);
      erreurApplication.setNo(no);
      getListeHMPageEnEchec().put(no, erreurApplication);
      indPageErreur++;

      if (indPageErreur > 39) {
        indPageErreur = 0;
      }
    } catch (Exception e) {
      //existe deja
    }
  }

  /**
   * Spécifier la liste en hashmap des erreurs les plus récentes de l'application
   * @return liste des pages en échec
   */
  public HashMap getListeHMPageEnEchec() {

    return listeErreurs;
  }

  /**
   * Spécifier la liste des erreurs les plus récentes de l'application.
   * @return la liste des erreurs les plus récentes de l'application
   */
  public ArrayList getListePageEnEchec() {
    return new ArrayList(getListeHMPageEnEchec().values());
  }

  /**
   * Initialiser a vide la liste des erreurs de l'application.
   */
  public void initialiserListePageEnEchec() {
    listeErreurs.clear();
    nbPageEchec = 0;
  }

  /**
   * Retourne le nombre d'utilisateur en ligne présentement.
   * @return le nombre d'utilisateur en ligne présentement
   */
  public int getNbUtilisateurEnLigne() {
    return this.nbUserActif;
  }

  /**
   * Retourne le nombre total d'utilisateur qui ont été sur le site.
   * @return le nombre total d'utilisateur qui ont été sur le site
   */
  public int getNbTotalUtilisateur() {
    return nbUserTotal;
  }

  /**
   * Retourne le nombre d'utilisateur authentifier en ligne présentement.
   * @return le nombre d'utilisateur authentifier en ligne présentement
   */
  public int getNbUtilisateurAuthentifierEnLigne() {
    int nbUtilisateurEnLigne = 0;

    initialiserListeUtilisateur();

    if (getListeHMUtilisateur() != null) {
      nbUtilisateurEnLigne = getListeHMUtilisateur().size();
    }

    return nbUtilisateurEnLigne;
  }

  /**
   * Retourne le nombre total d'utilisateur authentifier qui ont été sur le site.
   * @return le nombre total d'utilisateur authentifier qui ont été sur le site
   */
  public int getNbTotalUtilisateurAuthentifie() {
    return nbUserTotal;
  }

  /**
   * Retourne la date et le heure de départ de l'application.
   * @return la date et le heure de départ de l'application
   */
  public String getDateDepartApplication() {
    return dateDepartApplication;
  }

  /**
   * Initialiser la date de départ de l'application.
   */
  public void setDateDepartApplication() {
    java.util.Date dateHeure = new java.util.Date();
    this.dateDepartApplication = UtilitaireDate.convertirEn_AAAA_MM_JJHeureSeconde(dateHeure);
  }

  /**
   * Spécifier la liste des utilisateurs présentement actif sur le site.
   * @return la liste des utilisateurs présentement actif sur le site
   */
  public HashMap getListeHMUtilisateur() {
    return listeUtilisateur ;
  }

  /**
   * Obtenir la liste des utilisateurs.
   * @return la liste des utilisateurs
   */
  public ArrayList getListeUtilisateur() {
    initialiserListeUtilisateur();

    return new ArrayList(getListeHMUtilisateur().values());
  }

  /**
   * Ajouter un utilisateur à la liste.
   * @param cle Identifiant unique de l'utilisateur
   * @param utilisateur Objet utilisateur
   * @param sessionUtilisateur référence à la session de l'utilisateur
   */
  public void ajouterUtilisateur(Integer cle, Object utilisateur,
      HttpSession sessionUtilisateur) {
    HashMap listeHMUtilisteur = getListeHMUtilisateur();
    Object utilisateurTest = listeHMUtilisteur.get(cle);

    if (utilisateurTest == null) {
      UtilisateurSession utilisateurSession = new UtilisateurSession();

      String typeNavigateur = (String) sessionUtilisateur.getAttribute(Constantes.DESC_TYPE_NAVIGATEUR);
      utilisateurSession.setNavigateur(typeNavigateur);
      utilisateurSession.setUtilisateur(utilisateur);
      utilisateurSession.setCleUtilisateur(cle);
      utilisateurSession.setSession(sessionUtilisateur);
      getListeHMUtilisateur().put(cle, utilisateurSession);
      incrementerCompteurUsagerAuthentifier();

      if (log.isInfoEnabled()) {
        log.info("Nombre d'utilisateur authentifie en ligne:" +
            getNbUtilisateurAuthentifierEnLigne());
        log.info("Nombre d'utilisateur en ligne:" + getNbTotalUtilisateur());
      }
    }
  }

  /**
   * Ajouter un utilisateur à la liste.
   * @param cle Identifiant unique de l'utilisateur
   * @param utilisateur Objet utilisateur
   * @param sessionUtilisateur référence à la session de l'utilisateur
   */
  public void ajouterUtilisateur(String cle, Object utilisateur,
      HttpSession sessionUtilisateur) {
    if (getListeHMUtilisateur().containsKey(cle)) {
      UtilisateurSession utilisateurCourant = (UtilisateurSession) getListeHMUtilisateur()
          .get(cle);
      utilisateurCourant.setNbUtilisateur(utilisateurCourant.getNbUtilisateur() +
          1);

      StringBuffer nouvelleCle = new StringBuffer();
      nouvelleCle.append(cle);
      nouvelleCle.append("-");
      nouvelleCle.append(utilisateurCourant.getNbUtilisateur());
      cle = nouvelleCle.toString();
    }

    UtilisateurSession utilisateurSession = new UtilisateurSession();
    String typeNavigateur = (String) sessionUtilisateur.getAttribute(Constantes.DESC_TYPE_NAVIGATEUR);
    utilisateurSession.setNavigateur(typeNavigateur);

    String ip = (String) sessionUtilisateur.getAttribute(Constantes.UTILISATEUR_IP);
    utilisateurSession.setAdresseIP(ip);

    utilisateurSession.setUtilisateur(utilisateur);
    utilisateurSession.setSession(sessionUtilisateur);
    utilisateurSession.setCleUtilisateur(cle);

    getListeHMUtilisateur().put(cle, utilisateurSession);

    incrementerCompteurUsagerAuthentifier();

    if (log.isInfoEnabled()) {
      log.info("Nombre d'utilisateur authentifie en ligne:" +
          getNbUtilisateurAuthentifierEnLigne());
      log.info("Nombre d'utilisateur en ligne:" + getNbUtilisateurEnLigne());
    }
  }

  /**
   * Supprimer un utilisateur de la liste.
   * @param cle identifiant unique de l'utilisateur
   */
  public void supprimerUtilisateurDeLaListe(Object cle) {
    getListeHMUtilisateur().remove(cle);
  }

  /**
   * Supprimer les utilisateurs dont leur session est inactive.
   */
  public void initialiserListeUtilisateur() {
    Integer maxSessionMinutes = getSessionEnMinutes();

    if (maxSessionMinutes == null) {
      // Specifier 30 minutes comme defaut.
      maxSessionMinutes = new Integer(30);
    }

    // Transfert du maximum d'une session de minute en milliseconde.
    long maxSession = maxSessionMinutes.intValue() * 60000;
    long presentement = (new Date()).getTime();
    ArrayList listeUtilisateurs = new ArrayList(getListeHMUtilisateur().values());

    for (int i = 0; i < listeUtilisateurs.size(); i++) {
      UtilisateurSession utilisateurSession = (UtilisateurSession) listeUtilisateurs.get(i);

      try {
        long ecartSession = presentement -
            utilisateurSession.getSession().getLastAccessedTime();
        boolean supprimerUtilisateur = false;

        if (ecartSession > maxSession) {
          supprimerUtilisateur = true;
        }

        if (supprimerUtilisateur) {
          supprimerUtilisateurDeLaListe(utilisateurSession.getCleUtilisateur());
        }
      } catch (Exception e) {
        supprimerUtilisateurDeLaListe(utilisateurSession.getCleUtilisateur());
      }
    }
  }

  /**
   * Augmenter le compte des utilisateurs de 1.
   */
  public synchronized void incrementerCompteurUsager() {
    this.nbUserActif++;
    this.nbUserTotal++;
  }

  /**
   * Augmente le compte des utilisateur authentifiés de 1.
   */
  public synchronized void incrementerCompteurUsagerAuthentifier() {
    this.nbUserOnlineActive++;
    this.nbUserTotal++;
  }

  /**
   * Diminue le compte des utilisatuers de 1.
   */
  public synchronized void decrementerCompteurUsager() {
    this.nbUserActif--;
  }

  /**
   * Diminue le compte total des utilisateurs de 1.
   */
  public synchronized void decrementerCompteurTotalUsager() {
    this.nbUserTotal--;
  }

  /**
   * Diminue le compte des utilisateurs authentifiés de 1.
   */
  public synchronized void decrementerCompteurUsagerAuthentifier() {
    this.nbUserOnlineActive--;
  }

  /**
   * Diminue le compte total des utilisateurs authentifiés de 1.
   */
  public synchronized void decrementerCompteurTotalUsagerAuthentifier() {
    this.nbUserTotal--;
  }

  /**
   * Ferme la session d'un utilisateur.
   * @param request requête http
   */
  static public void fermerSession(HttpServletRequest request) {
    request.setAttribute(Constantes.IS_FERMER_SESSION, "oui");
  }

  /**
   * Indique dans le request que l'utilisateur est authentifié.
   * @param request requête http
   */
  static public void specifierNouvelleSessionAuthentifiee(HttpServletRequest request) {
    request.setAttribute(Constantes.NOUVELLE_AUTHENTIFICATION, "oui");
  }

  /**
   * Obtenir le paramètre utilisateur.
   * @return le paramètre utilisateur
   */
  public String getParamUtilisateur() {
    return paramUtilisateur;
  }

  /**
   * Fixer le paramètre utilisateur.
   * @param paramUtilisateur le paramètre utilisateur
   */
  public void setParamUtilisateur(String paramUtilisateur) {
    this.paramUtilisateur = paramUtilisateur;
  }

  /**
   * Obtenir l'identifiant du paramètre utilisateur.
   * @return l'identifiant du paramètre utilisateur
   */
  public String getParamCleUtilisateur() {
    return paramCleUtilisateur;
  }

  /**
   * Fixer l'identifiant du paramètre utilisateur
   * @param paramCleUtilisateur l'identifiant du paramètre utilisateur
   */
  public void setParamCleUtilisateur(String paramCleUtilisateur) {
    this.paramCleUtilisateur = paramCleUtilisateur;
  }

  /**
   * Obtenir la session en minutes.
   * @return la session en minutes
   */
  public Integer getSessionEnMinutes() {
    return sessionEnMinutes;
  }

  /**
   * Fixer la session en minutes.
   * @param sessionEnMinutes la session en minutes
   */
  public void setSessionEnMinutes(Integer sessionEnMinutes) {
    this.sessionEnMinutes = sessionEnMinutes;
  }

  /**
   * Retourne le path correspondant au site de surveillance.
   * @return le path correspondant au site de surveillance.
   */
  public String getPathSiteSurveillance() {
    return pathSiteSurveillance;
  }

  /**
   * Fixer le path correspondant au site de surveillance.
   * @param pathSiteSurveillance le path correspondant au site de surveillance.
   */
  public void setPathSiteSurveillance(String pathSiteSurveillance) {
    this.pathSiteSurveillance = pathSiteSurveillance;
  }

  public boolean isActif() {
    return actif;
  }

  public void setActif(boolean actif) {
    this.actif = actif;
  }

  static public void ajouterPageEnEchec(HttpServletRequest request,
      ActionMapping mapping, String messageErreur) {
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    UtilisateurSession utilisateurCourant = (UtilisateurSession) GestionSurveillance.getInstance()
        .getListeHMUtilisateur()
        .get(utilisateur.getCodeUtilisateur());

    String methodeParam = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.METHODE_PARAM);
    if (UtilitaireString.isVide(methodeParam)) {
      methodeParam = "methode";
    }
    String methode = request.getParameter(methodeParam);
    ErreurApplication erreurApplication = new ErreurApplication();
    erreurApplication.setMethode(methode);
    erreurApplication.setDateHeure(new Date(UtilitaireDate.getDateJourAvecHeure()
        .getTime()));
    erreurApplication.setMessage(messageErreur);

    if (mapping != null) {
      erreurApplication.setNomPageErreur(mapping.getPath());
    }

    if (utilisateur != null) {
      utilisateurCourant.setDateCreationSession(request.getSession()
          .getCreationTime());
      utilisateurCourant.setDateDerniereAcces(request.getSession()
          .getLastAccessedTime());

      UtilisateurSession utilisateurClone = (UtilisateurSession)utilisateurCourant.clone();
      utilisateurClone.setDerniereAction(utilisateurCourant.getDerniereAction());
      erreurApplication.setUtilisateur(utilisateurClone);
    } else {
      erreurApplication.setUtilisateur(null);
    }

    org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
    .ajouterPageEnEchec(erreurApplication);
  }

  /**
   * Ajouter une page en echec dans la console de Surveillance.
   * @param messageErreur le message d'erreur
   * @param request la requête en traitement.
   */
  static public void ajouterPageEnEchec(Utilisateur utilisateur,
      String messageErreur, HttpServletRequest request) {
    UtilisateurSession utilisateurCourant = (UtilisateurSession) GestionSurveillance.getInstance()
        .getListeHMUtilisateur()
        .get(utilisateur.getCodeUtilisateur());

    ErreurApplication erreurApplication = new ErreurApplication();
    erreurApplication.setDateHeure(new Date(UtilitaireDate.getDateJourAvecHeure()
        .getTime()));
    erreurApplication.setMessage(messageErreur);
    erreurApplication.setNomPageErreur(request.getRequestURI());

    if (utilisateur != null) {
      UtilisateurSession utilisateurClone = (UtilisateurSession)utilisateurCourant.clone();
      utilisateurClone.setDerniereAction(utilisateurCourant.getDerniereAction());
      erreurApplication.setUtilisateur(utilisateurClone);
    } else {
      erreurApplication.setUtilisateur(null);
    }

    org.sofiframework.application.surveillance.GestionSurveillance.getInstance()
    .ajouterPageEnEchec(erreurApplication);
  }

  static public void ajouterPageEnEchec(HttpServletRequest request,
      ActionMapping mapping, Exception exception) {
    // Traiter l'exception.
    StringWriter writer = new StringWriter();
    PrintWriter prn = new PrintWriter(writer);
    prn.flush();

    String messageErreur = writer.toString();
    ajouterPageEnEchec(request, mapping, messageErreur);
  }

  public ArrayList getListeUtilisateurEnLigneUnique() {
    ArrayList liste = getListeUtilisateur();
    HashMap listeUtilisateurUniqueMap = new HashMap();
    ArrayList listeUtilisateurUnique = new ArrayList();
    Iterator iterateur = liste.iterator();

    while (iterateur.hasNext()) {
      UtilisateurSession utilisateurSession = (UtilisateurSession) iterateur.next();
      Utilisateur utilisateur = (Utilisateur) utilisateurSession.getUtilisateur();
      UtilisateurSession utilisateurSessionAjoute = (UtilisateurSession) listeUtilisateurUniqueMap.get(utilisateur.getCodeUtilisateur());

      if ((utilisateurSessionAjoute == null) ||
          ((utilisateurSessionAjoute != null) &&
              (utilisateurSessionAjoute.getSession().getLastAccessedTime() < utilisateurSession.getSession()
                  .getLastAccessedTime()))) {
        if (utilisateurSessionAjoute != null) {
          listeUtilisateurUniqueMap.remove(utilisateur.getCodeUtilisateur());
        }

        try {
          if (utilisateurSession.getSession().getCreationTime() > 0) {
            listeUtilisateurUniqueMap.put(utilisateur.getCodeUtilisateur(),
                utilisateurSession);
          }
        } catch (Exception e) {
          // Ignorer car la session est invalidé.
        }
      }
    }

    Iterator iterateurMap = listeUtilisateurUniqueMap.values().iterator();

    while (iterateurMap.hasNext()) {
      listeUtilisateurUnique.add(iterateurMap.next());
    }

    return listeUtilisateurUnique;
  }

  /**
   * Permet d'ajouter un utilisateur dans la surveillance.
   * @param request la requête en traitement.
   * @param utilisateur l'utilisateur en traitement.
   */
  public static void ajouterUtilisateurDansSurveillance(
      Utilisateur utilisateur, HttpServletRequest request) {
    GestionSurveillance surveillance = GestionSurveillance.getInstance();

    // Indique si c'est une nouvelle session.
    if (GestionSurveillance.getInstance().isActif() &&
        UtilitaireControleur.isNouvelleAuthentification(request) &&
        (utilisateur != null)) {
      String attributCle = surveillance.getParamCleUtilisateur();
      Object valeurCle = org.sofiframework.utilitaire.UtilitaireObjet.getValeurAttribut(utilisateur,
          attributCle);

      request.getSession().setAttribute(Constantes.UTILISATEUR_IP,
          request.getRemoteAddr());

      try {
        if (valeurCle != null) {
          if (valeurCle.getClass().isAssignableFrom(Integer.class)) {
            surveillance.ajouterUtilisateur((Integer) valeurCle, utilisateur,
                request.getSession());
          } else {
            surveillance.ajouterUtilisateur((String) valeurCle, utilisateur,
                request.getSession());
          }
        }
      } catch (Exception e) {
        throw new SOFIException(
            "Vous devez configurer le paramètre <cleUtilisateur> dans struts-config pour le plugin SurveillancePlugIn afin de spécifier l'attribut qui spécifie la clé unique de l'utilisateur");
      }

      // Le nombre d'utilisateur en ligne
      request.getSession().setAttribute(Constantes.NB_UTILISATEUR_EN_LIGNE,
          String.valueOf(GestionSurveillance.getInstance()
              .getListeUtilisateurEnLigneUnique()
              .size()));
    }
  }
}
