/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.surveillance;


/**
 * Permet de bypasser le get dans la session de la derniere
 * action sans accéder à la session.
 *
 * @author Jean-Maxime Pelletier
 */
public class InfoUtilisateur extends UtilisateurSession {
  /**
   * 
   */
  private static final long serialVersionUID = 3794329541635451952L;
  private String derniereAction = null;

  @Override
  public String getDerniereAction() {
    return derniereAction;
  }

  @Override
  public void setDerniereAction(String derniereAction) {
    this.derniereAction = derniereAction;
  }
}
