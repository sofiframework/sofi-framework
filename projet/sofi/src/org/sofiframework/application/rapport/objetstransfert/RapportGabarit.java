/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Un gabarit de rapport est composé d'un ensemble d'entrées de rapport.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportGabarit extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = -7398405181942387275L;
  private String codeApplication;
  private String codeClient;
  private String codeGabarit;
  private String nom;
  private String codeStatut;
  private List<RapportMetadonnee> listeRapportMetadonnee;

  public List<RapportMetadonnee> getListeRapportMetadonnee() {
    if (listeRapportMetadonnee == null) {
      listeRapportMetadonnee = new ArrayList<RapportMetadonnee>();
    }
    return listeRapportMetadonnee;
  }

  public void setListeRapportMetadonnee(List<RapportMetadonnee> listeRapportMetadonnee) {
    this.listeRapportMetadonnee = listeRapportMetadonnee;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeGabarit() {
    return codeGabarit;
  }

  public void setCodeGabarit(String codeGabarit) {
    this.codeGabarit = codeGabarit;
  }

}
