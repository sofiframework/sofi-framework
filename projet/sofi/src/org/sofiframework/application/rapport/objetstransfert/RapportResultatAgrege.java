/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import java.math.BigDecimal;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeTypeMetadonnee;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * POJO représentant le résultat d'un rapport pour une métadonnée (généralement pour une période supérieure à une
 * journée). La valeur du résultat peut être une somme (pour les type {@link CodeTypeMetadonnee#COMPTE} ou
 * {@link CodeTypeMetadonnee#MONTANT}), une moyenne {@link CodeTypeMetadonnee#POURCENTAGE},...
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class RapportResultatAgrege extends ObjetTransfert {

  /**
   * 
   */
  private static final long serialVersionUID = -3071314264262551088L;

  private Long rapportMetadonneeId;
  private String codeClient;
  private BigDecimal valeur;

  public RapportResultatAgrege(Long rapportMetadonneeId, String codeClient, BigDecimal somme) {
    super();
    this.rapportMetadonneeId = rapportMetadonneeId;
    this.codeClient = codeClient;
    this.valeur = somme;
  }

  public RapportResultatAgrege(Long rapportMetadonneeId, String codeClient, Double moyenne) {
    super();
    this.rapportMetadonneeId = rapportMetadonneeId;
    this.codeClient = codeClient;
    this.valeur = new BigDecimal(moyenne);
  }

  public Long getRapportMetadonneeId() {
    return rapportMetadonneeId;
  }

  public void setRapportMetadonneeId(Long rapportMetadonneeId) {
    this.rapportMetadonneeId = rapportMetadonneeId;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public BigDecimal getValeur() {
    return valeur;
  }

  public void setValeur(BigDecimal valeur) {
    this.valeur = valeur;
  }

}
