/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import java.util.Date;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Entité représentant un résultat d'entrée de rapport.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportResultat extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = 1206424178446063663L;

  private String codeApplication;
  private String codeClient;
  private Long rapportMetadonneeId;
  private RapportMetadonnee rapportMetadonnee;
  private String valeur;
  private Date dateResultat;
  private Integer nombrePeriode;

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public Long getRapportMetadonneeId() {
    return rapportMetadonneeId;
  }

  public void setRapportMetadonneeId(Long rapportMetadonneeId) {
    this.rapportMetadonneeId = rapportMetadonneeId;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public Date getDateResultat() {
    return dateResultat;
  }

  public void setDateResultat(Date dateResultat) {
    this.dateResultat = dateResultat;
  }

  public Integer getNombrePeriode() {
    return nombrePeriode;
  }

  public void setNombrePeriode(Integer nombrePeriode) {
    this.nombrePeriode = nombrePeriode;
  }

  public RapportMetadonnee getRapportMetadonnee() {
    return rapportMetadonnee;
  }

  public void setRapportMetadonnee(RapportMetadonnee rapportMetadonnee) {
    this.rapportMetadonnee = rapportMetadonnee;
  }

}
