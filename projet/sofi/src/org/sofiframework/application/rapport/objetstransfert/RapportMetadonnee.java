/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Représente une entrée de rapport.
 * 
 * @author remi.mercier
 * @since 3.2
 */
public class RapportMetadonnee extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = -411202813867493872L;

  private String codeApplication;
  private String nom;
  private String nomClasseTraitement;
  private String codeStatut;
  private String codeTypeMetadonnee;
  private Integer ordreAffichage;
  private String codeModeResultat;
  private boolean totalisable = false;
  private Long parentId;
  private RapportMetadonnee rapportMetadonneeParent;
  /**
   * Niveau de la métadonnées dans l'arborescence du rapport. Par exemple, une métadonnée sans parent aura le niveau 0.
   */
  private Integer niveau;

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public RapportMetadonnee getRapportMetadonneeParent() {
    return rapportMetadonneeParent;
  }

  public void setRapportMetadonneeParent(RapportMetadonnee rapportMetadonneeParent) {
    this.rapportMetadonneeParent = rapportMetadonneeParent;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNomClasseTraitement() {
    return nomClasseTraitement;
  }

  public void setNomClasseTraitement(String nomClasseTraitement) {
    this.nomClasseTraitement = nomClasseTraitement;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeTypeMetadonnee() {
    return codeTypeMetadonnee;
  }

  public void setCodeTypeMetadonnee(String codeTypeMetadonnee) {
    this.codeTypeMetadonnee = codeTypeMetadonnee;
  }

  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }

  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  public Integer getNiveau() {
    return niveau;
  }

  public void setNiveau(Integer niveau) {
    this.niveau = niveau;
  }

  public String getCodeModeResultat() {
    return codeModeResultat;
  }

  public void setCodeModeResultat(String codeModeResultat) {
    this.codeModeResultat = codeModeResultat;
  }

  public boolean getTotalisable() {
    return totalisable;
  }

  public void setTotalisable(boolean totalisable) {
    this.totalisable = totalisable;
  }

}
