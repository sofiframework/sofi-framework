/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Représente dans l'infrastructure une entrée de rapport lors de son exécution.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportTraitement extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = -291963172896052168L;

  private String codeApplication;
  private String codeClient;
  private Long rapportMetadonneeId;
  private String codeEtatTraitement;
  private Date dateTraitement;
  private Integer nombrePeriode;

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public Long getRapportMetadonneeId() {
    return rapportMetadonneeId;
  }

  public void setRapportMetadonneeId(Long rapportMetadonneeId) {
    this.rapportMetadonneeId = rapportMetadonneeId;
  }

  public String getCodeEtatTraitement() {
    return codeEtatTraitement;
  }

  public void setCodeEtatTraitement(String codeEtatTraitement) {
    this.codeEtatTraitement = codeEtatTraitement;
  }

  public Date getDateTraitement() {
    return dateTraitement;
  }

  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }

  public Integer getNombrePeriode() {
    return nombrePeriode;
  }

  public void setNombrePeriode(Integer nombrePeriode) {
    this.nombrePeriode = nombrePeriode;
  }

  /**
   * Calcule la date de début de la période de traitement en fonction de <code>dateTraitement</code> et
   * <code>nombrePeriode</code> : <code>dateTraitement</code> - (<code>nombrePeriode</code> - 1) à minuit.
   * 
   * @return
   */
  public Date getDateDebutPeriode() {
    Date date = DateUtils.addDays(dateTraitement, (nombrePeriode - 1) * -1);
    return DateUtils.truncate(date, Calendar.DATE);
  }

  /**
   * Calcule la date de fin de la période de traitement : <code>dateTraitement</code> à 23h59m59s,999.
   * 
   * @return
   */
  public Date getDateFinPeriode() {
    Date date = DateUtils.addDays(dateTraitement, 1);
    date = DateUtils.truncate(date, Calendar.DATE);
    return DateUtils.addMilliseconds(date, -1);
  }
}
