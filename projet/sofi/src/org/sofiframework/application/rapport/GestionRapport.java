/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport;

import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.rapport.service.ServiceRapport;

/**
 * Singleton permettant l'accès au service d'exécution des rapports.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class GestionRapport extends BaseGestionService {
  /**
   * Référence à l'instance unique de l'objet de gestion des rapports.
   */
  private static GestionRapport singleton = new GestionRapport();

  /**
   * Service distant vers le service d'exécution des rapports.
   */
  private ServiceRapport serviceRapport;

  /**
   * Constructeur par défaut.
   */
  private GestionRapport() {
    super(ServiceRapport.class);
  }

  /**
   * Retourne l'instance unique de cette classe.
   * 
   * @return un objet {@link GestionRapport}.
   */
  public static GestionRapport getInstance() {
    return singleton;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.application.base.BaseGestionService#setServiceLocal(java.lang.Object)
   */
  @Override
  protected void setServiceLocal(Object service) {
    setServiceRapport((ServiceRapport) service);
  }

  public ServiceRapport getServiceRapport() {
    return serviceRapport;
  }

  public void setServiceRapport(ServiceRapport serviceRapport) {
    this.serviceRapport = serviceRapport;
  }

}
