/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.execution;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.sofiframework.application.rapport.GestionRapport;
import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.domaine.DomaineRapport.ParametreExecutionMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.application.rapport.service.ServiceRapport;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful;
import org.sofiframework.constante.Domaine.CodeStatut;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireListe;


/**
 * Tâche planifiée définissant le pattern de l'exécution des rapports dans SOFI. Cette tâche hérite de
 * {@link TacheStateful} ; de fait, son exécution est concurrentielle : il ne peut pas y avoir 2 tâches
 * {@link TacheRapport} qui s'exécutent au même instant.
 * <p>
 * Trois méthodes peuvent être surchargées pour ajouter des comportements spécifiques à l'application clients :
 * apres...().
 * <p>
 * Le comportement par défaut de cette tâche est de traiter les métadonnées de rapport actives pour la journée précédent
 * l'exécution de la tâche. Pour surcharger ce comportement, il faut redéfinir les paramètres de la tâche. Voir
 * {@link ParametreTacheRapport}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class TacheRapport extends TacheStateful {

  private static final Log log = LogFactory.getLog(TacheRapport.class);

  /**
   * Énumération des clés de paramètres possibles de la tâche {@link TacheRapport} ; le nom de l'élément de
   * l'énumération sera de clé du paramètre.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  protected static enum ParametreTacheRapport implements ParametreTache {
    /**
     * Nombre de journées sur lesquelles doivent être traitées les métadonnées des rapports.
     * <p>
     * Permet de surcharger {@link TacheRapport#nombrePeriode}.
     */
    NOMBRE_PERIODE,
    /**
     * Date du rapport ; les formats acceptés sont : yyyy-MM-dd, yyyyMMdd et ddMMyyyy.
     * <p>
     * Si le format n'est pas correct, une exception sera déclenchée lors de l'initialisation de la tâche et la tâche
     * s'arrêtera.
     * <p>
     * Permet de surcharger {@link TacheRapport#dateRapport}.
     */
    DATE_RAPPORT,
    /**
     * Lorsque actif, la tâche va tenter de ré-exécuter toutes les métadonnées en erreur. Le traitement normal n'est pas
     * effectué ; seul les métadonnées en erreur sont traitées.
     */
    MODE_ERREUR_ACTIF,
    /**
     * Permet de définir un client en particulier pour lequel on souhaite exécuter la tâche.
     */
    CODE_CLIENT;

    @Override
    public String getCle() {
      return name();
    }
  }

  /**
   * Représente la journée pour laquelle les rapports seront traités.
   * <p>
   * Peut être surchargé via les paramètres de la tâche.
   * 
   * @see ParametreTacheRapport#DATE_RAPPORT
   */
  private Date dateRapport;
  /**
   * Le nombre de journées pour lesquelles les métadonnées doivent être compilées (1 est la valeur par défaut).
   * <p>
   * Peut être surchargé via les paramètres de la tâche.
   * 
   * @see ParametreTacheRapport#NOMBRE_PERIODE
   */
  private int nombrePeriode = 1;

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.application.tacheplanifiee.job.Tache#executer(org.quartz.JobExecutionContext)
   */
  @Override
  public void executer(JobExecutionContext context) {

    initialiserTache(context);
    apresInitialiserTache(context);

    if (isModeErreurActif(context)) {
      executerMetadonneeEnErreur(context);
    } else {
      preparerRapport(context);

      avantExecuterRapport(context);
      executerRapport(context);
      apresExecuterRapport(context);
    }

    finaliserRapport(context);
  }

  /**
   * Checke dans les paramètres de la tâche si {@link ParametreTacheRapport#MODE_ERREUR_ACTIF} est présent et à true
   * 
   * @param context
   * @return
   */
  private Boolean isModeErreurActif(JobExecutionContext context) {
    String modeErreurActif = (String) getParametre(ParametreTacheRapport.MODE_ERREUR_ACTIF.getCle(), context);
    return BooleanUtils.toBoolean(modeErreurActif);
  }

  /**
   * Méthode pouvant être surchargée par la classe fille pour définir des actions à affectuer après l'initialisation de
   * la tâche.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  protected void apresInitialiserTache(JobExecutionContext context) {

  }

  /**
   * Méthode pouvant être surchargée par la classe fille pour définir des actions à affectuer avant l'exécution des
   * rapports.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  protected void avantExecuterRapport(JobExecutionContext context) {

  }

  /**
   * Méthode pouvant être surchargée par la classe fille pour définir des actions à affectuer à la fin de l'exécution
   * des rapports.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  protected void apresExecuterRapport(JobExecutionContext context) {

  }

  /**
   * Initialise la date des rapports et la période sur laquelle les entrées de rapport seront traitées.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   * @throws JobExecutionException
   * @throws ParseException
   */
  private void initialiserTache(JobExecutionContext context) {

    // nombre de journée
    String valeur = (String) getParametre(ParametreTacheRapport.NOMBRE_PERIODE.getCle(), context);
    if (valeur != null) {
      setNombrePeriode(Integer.parseInt(valeur));
    }

    dateRapport = new Date();
    // on regarde si le paramètre de surcharge DATE_RAPPORT est présent.
    String dateRapportString = (String) getParametre(ParametreTacheRapport.DATE_RAPPORT.getCle(), context);
    if (StringUtils.isNotEmpty(dateRapportString)) {
      try {
        dateRapport = DateUtils.parseDate(dateRapportString, new String[] { "yyyy-MM-dd", "yyyyMMdd", "ddMMyyyy" });
        // on ajoute un jour car la tâche est prévue pour réaliser l'exécution de la journée précédente ; or lorsque la
        // date de rapport est précisée dans les paramètres on veut que l'exécution soit faite pour la date spécifiée.
        dateRapport = DateUtils.addDays(dateRapport, 1);
      } catch (ParseException e) {
        log.error(
            dateRapportString
            + " n'est pas un format de date accepté par le système. L'exécution de la tâche des rapports est interrompue.",
            e);
        throw new SOFIException(e);
      }
    }

    // date rapport = date rapport - 1j à 23h59m59,999
    Date date = dateRapport;
    date = DateUtils.truncate(date, Calendar.DATE);
    dateRapport = DateUtils.addMilliseconds(date, -1);
  }

  /**
   * Méthode parcourant la liste des gabarits de rapport à exécuter (c'est-à-dire les gabarits ayant un statut actif) ;
   * pour chaque gabarit et pour chaque clients associés au gabarit, elle enregistre les métadonnées actives avec l'état
   * {@link CodeEtatTraitementMetadonnee#A_TRAITER}.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  private void preparerRapport(JobExecutionContext context) {
    String codeClient = (String) getParametre(ParametreTacheRapport.CODE_CLIENT.getCle(), context);
    List<RapportGabarit> listeRapportGabarit = getServiceRapport().getListeRapportGabaritActif(getCodeApplication(), codeClient);
    // pour chaque gabarit
    for (RapportGabarit gabarit : listeRapportGabarit) {
      List<String> listeCodeClientFinal = getListeCodeClientFinal(gabarit.getCodeClient());
      List<RapportTraitement> listeRapportTraitement = new ArrayList<RapportTraitement>();
      // pour chaque métadata associé au gabarit
      for (RapportMetadonnee metadonnee : gabarit.getListeRapportMetadonnee()) {
        // on exécute uniquement les métadonnées actives et qui ont un traitement spécifié.
        if (!metadonnee.getCodeStatut().equals(CodeStatut.INACTIF) && metadonnee.getNomClasseTraitement() != null) {
          listeRapportTraitement.addAll(getListeRapportTraitement(metadonnee, listeCodeClientFinal));
        }
      }
      getServiceRapport().enregistrerListeRapportTraitement(listeRapportTraitement);
    }
  }

  /**
   * Construit la liste d'objet {@link RapportTraitement} correspondant à <code>metadonnee</code> pour les codes client
   * passés en paramètre.
   * 
   * @param metadonnee
   *          une définition d'une enregistrement de rapport.
   * @param listeCodeClien
   *          une liste de code client.
   * @return une liste de {@link RapportTraitement}.
   */
  private List<RapportTraitement> getListeRapportTraitement(RapportMetadonnee metadonnee, List<String> listeCodeClient) {
    List<RapportTraitement> listeRapportTraitement = new ArrayList<RapportTraitement>();
    for (String codeClient : listeCodeClient) {
      RapportTraitement rapportTraitement = construireRapportTraitement(codeClient, metadonnee);
      listeRapportTraitement.add(rapportTraitement);
    }
    return listeRapportTraitement;
  }

  /**
   * Permet d'obtenir la liste des codes client final (c'est-à-dire n'ayant pas d'enfant) descendant du client de code
   * passé en paramètre. Si le client passé en paramètre n'a pas de descendant, la liste retournée contiendra un seul
   * élément : <code>codeClientGabarit</code>.
   * 
   * @param codeClientGabarit
   *          le code client dont on veut les descendants.
   * @return une liste de code client.
   * @see ServiceClient#getListeClientDescendant(String).
   */
  @SuppressWarnings("unchecked")
  private List<String> getListeCodeClientFinal(String codeClientGabarit) {
    // On détermine à quels clients finaux est associé le gabarit.
    List<String> listeCodeClient = new ArrayList<String>();
    List<Client> listeClientDescendant = getServiceClient().getListeClientDescendant(codeClientGabarit);
    listeClientDescendant = UtilitaireListe.getListeParentEnfantsEn1Niveau(listeClientDescendant, "listeClientEnfant");
    if (!listeClientDescendant.isEmpty()) {
      for (Client client : listeClientDescendant) {
        // S'il n'a pas d'enfant, c'est un client final ; on l'ajoute.
        if (client.getListeClientEnfant() == null || client.getListeClientEnfant().isEmpty()) {
          listeCodeClient.add(client.getNoClient());
        }
      }
    }
    // Il n'a pas de descendant, c'est un client final ; on l'ajoute.
    else {
      listeCodeClient.add(codeClientGabarit);
    }
    return listeCodeClient;
  }

  /**
   * Récupère la liste des entrées de rapport avec l'état {@link CodeEtatTraitementMetadonnee#A_TRAITER} et les exécute
   * en série.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  private void executerRapport(JobExecutionContext context) {
    List<RapportTraitement> listeRapportTraitement = getServiceRapport().getListeRapportTraitement(
        getCodeApplication(), CodeEtatTraitementMetadonnee.A_TRAITER);

    executerListeRapportTraitement(listeRapportTraitement, context);
  }

  /**
   * Récupère la liste des entrées de rapport avec l'état {@link CodeEtatTraitementMetadonnee#EN_ERREUR} et les exécute
   * en série.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  private void executerMetadonneeEnErreur(JobExecutionContext context) {
    List<RapportTraitement> listeRapportTraitementEnErreur = getServiceRapport().getListeRapportTraitement(
        getCodeApplication(), CodeEtatTraitementMetadonnee.EN_ERREUR);

    executerListeRapportTraitement(listeRapportTraitementEnErreur, context);
  }

  /**
   * Pour chaque élément de la liste, appelle la méthode {@link TacheRapport#executerRapport(RapportTraitement)},
   * enregistre le résultat (si il n'y a pas eu d'erreur) et met à jour le code de l'état de traitement correspondant.
   * 
   * @param listeRapportTraitement
   *          une liste de {@link RapportTraitement}.
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  @SuppressWarnings("unchecked")
  private void executerListeRapportTraitement(List<RapportTraitement> listeRapportTraitement,
      JobExecutionContext context) {
    // Pour chaque entrée de rapport à traiter
    for (RapportTraitement rapportTraitement : listeRapportTraitement) {
      Long metadonneeId = rapportTraitement.getRapportMetadonneeId();
      String codeClient = rapportTraitement.getCodeClient();
      CodeEtatTraitementMetadonnee codeEtatTraitement = null;
      String resultat = null;

      try {

        // Exécution de l'entrée de rapport
        if (log.isInfoEnabled()) {
          log.info("Exécution de " + metadonneeId + " pour " + codeClient + " démarrée...");
        }

        long start = System.currentTimeMillis();

        resultat = executerMetadonnee(context, rapportTraitement);
        resultat = formaterResultat(resultat);

        long end = System.currentTimeMillis();

        if (log.isInfoEnabled()) {
          log.info("Exécution de " + metadonneeId + " pour " + codeClient + " terminée après " + (end - start) + " ms.");
        }

        // Enregistrement du résultat
        RapportResultat rapportResultat = construireRapportResultat(rapportTraitement, resultat);
        getServiceRapport().enregistrerListeRapportResultat(Arrays.asList(new RapportResultat[] { rapportResultat }));
        codeEtatTraitement = CodeEtatTraitementMetadonnee.TRAITEE;

      } catch (Exception e) {
        log.error("Erreur lors de l'exécution de " + metadonneeId + " pour " + codeClient, e);
        codeEtatTraitement = CodeEtatTraitementMetadonnee.EN_ERREUR;

      } finally {
        rapportTraitement.setCodeEtatTraitement(codeEtatTraitement.getValeur());
        getServiceRapport().enregistrerListeRapportTraitement(
            Arrays.asList(new RapportTraitement[] { rapportTraitement }));
      }
    }
  }

  /**
   * Si le résultat est un nombre, remplace le point (format pour java) par une virgule (format pour les bases de
   * donnée).
   * 
   * @param resultat
   * @return
   */
  private String formaterResultat(String resultat) {
    String s = resultat;
    if (NumberUtils.isNumber(resultat)) {
      s = resultat.replace('.', ',');
    }
    return s;
  }

  /**
   * Supprime les métadonnées de rapport traitées sans erreur (c'est-à-dire avec l'état
   * {@link CodeEtatTraitementMetadonnee#TRAITEE}).
   * 
   * @param context
   *          le contexte d'exécution de la tache.
   */
  private void finaliserRapport(JobExecutionContext context) {
    int nombre = getServiceRapport().supprimerRapportTraitement(getCodeApplication(),
        CodeEtatTraitementMetadonnee.TRAITEE);

    if (log.isInfoEnabled()) {
      log.info(nombre + " métadonnées traitées par la tâche des rapports.");
    }
  }

  /**
   * Exécute une entrée de rapport en appelant sa classe de traitement.
   * 
   * @param context
   * 
   * @param rapportTraitement
   *          l'entrée de rapport à
   * @return la valeur résultat de l'exécution.
   * @throws ClassNotFoundException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  @SuppressWarnings("unchecked")
  private String executerMetadonnee(JobExecutionContext context, RapportTraitement rapportTraitement)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException {
    RapportMetadonnee rapport = getServiceRapport().getRapportMetadonnee(rapportTraitement.getRapportMetadonneeId());
    Class<ExecutionMetadonnee> classeTraitement = (Class<ExecutionMetadonnee>) Class.forName(rapport
        .getNomClasseTraitement());
    ExecutionMetadonnee executionRapport = (ExecutionMetadonnee) getBean(context, classeTraitement);

    EnumMap<ParametreExecutionMetadonnee, Object> parametres = new EnumMap<ParametreExecutionMetadonnee, Object>(
        ParametreExecutionMetadonnee.class);
    parametres.put(ParametreExecutionMetadonnee.CODE_CLIENT, rapportTraitement.getCodeClient());
    parametres.put(ParametreExecutionMetadonnee.DEBUT_PERIODE, rapportTraitement.getDateDebutPeriode());
    parametres.put(ParametreExecutionMetadonnee.FIN_PERIODE, rapportTraitement.getDateFinPeriode());
    return executionRapport.executer(parametres);
  }

  /**
   * Permet d'initialiser un objet {@link RapportResultat} à partir de l'objet {@link RapportTraitement} passé en
   * paramètre.
   * 
   * @param rapportTraitement
   *          un objet {@link RapportTraitement}.
   * @param resultat
   *          la valeur résultat de l'exécution de <code>rapportTraitement</code>.
   * @return un objet {@link RapportResultat}.
   */
  private RapportResultat construireRapportResultat(RapportTraitement rapportTraitement, String resultat) {
    RapportResultat rapportResultat = new RapportResultat();
    rapportResultat.setCodeApplication(rapportTraitement.getCodeApplication());
    rapportResultat.setCodeClient(rapportTraitement.getCodeClient());
    rapportResultat.setRapportMetadonneeId(rapportTraitement.getRapportMetadonneeId());
    rapportResultat.setValeur(resultat);
    rapportResultat.setDateResultat(rapportTraitement.getDateTraitement());
    rapportResultat.setNombrePeriode(getNombrePeriode());
    return rapportResultat;
  }

  /**
   * Permet d'initialiser un objet {@link RapportTraitement} à partir d'un objet {@link RapportMetadonnee}. L'objet
   * retourné aura son attribut <code>codeEtatTraitement</code> initialisé avec la valeur
   * {@link CodeEtatTraitementMetadonnee#A_TRAITER}.
   * 
   * @param codeClient
   *          le code client.
   * @param un
   *          objet {@link RapportMetadonnee}.
   * @return un objet {@link RapportTraitement}.
   */
  private RapportTraitement construireRapportTraitement(String codeClient, RapportMetadonnee metadonnee) {
    RapportTraitement rapportTraitement = new RapportTraitement();
    rapportTraitement.setCodeApplication(metadonnee.getCodeApplication());
    rapportTraitement.setCodeClient(codeClient);
    rapportTraitement.setRapportMetadonneeId(metadonnee.getId());
    rapportTraitement.setCodeEtatTraitement(CodeEtatTraitementMetadonnee.A_TRAITER.getValeur());
    // Il sera plus simple pour faire des requêtes SQL que la date de traitement (qui deviendra la date de résultat) ne
    // soit pas avec une heure à 23h59m59s,999. On la truncate au niveau du champ heure pour avoir 23h00 mais on aurait
    // pu la fixer aussi à 09h00 ou 14h53 ou ... ; cela importe peu tant que c'est inférieur à 23h59.
    rapportTraitement.setDateTraitement(DateUtils.truncate(getDateRapport(), Calendar.HOUR_OF_DAY));
    rapportTraitement.setNombrePeriode(getNombrePeriode());
    return rapportTraitement;
  }

  /**
   * Permet d'obtenir le code de l'application.
   * 
   * @return le code de l'application appelante.
   */
  private String getCodeApplication() {
    return GestionSecurite.getInstance().getCodeApplication();
  }

  /**
   * Permet d'obtenir le service des clients.
   * 
   * @return un objet de type {@link ServiceClient}.
   */
  private ServiceClient getServiceClient() {
    return GestionSecurite.getInstance().getServiceClient();
  }

  /**
   * Permet d'obtenir le service de rapport nécessaire à l'exécution des rapports.
   * 
   * @return un objet de type {@link ServiceRapport}.
   */
  private ServiceRapport getServiceRapport() {
    return GestionRapport.getInstance().getServiceRapport();
  }

  public Date getDateRapport() {
    return dateRapport;
  }

  public void setDateRapport(Date dateRapport) {
    this.dateRapport = dateRapport;
  }

  //
  // public Periode getPeriodeRapport() {
  // return periodeRapport;
  // }
  //
  // public void setPeriodeRapport(Periode periodeRapport) {
  // this.periodeRapport = periodeRapport;
  // }

  public int getNombrePeriode() {
    return nombrePeriode;
  }

  public void setNombrePeriode(int nombrePeriode) {
    this.nombrePeriode = nombrePeriode;
  }

}