/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.execution;

import java.util.Map;

import org.sofiframework.application.rapport.domaine.DomaineRapport.ParametreExecutionMetadonnee;

/**
 * Interface spécifiant le traitement nécessaire pour chaque entrée de rapport.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface ExecutionMetadonnee {

  /**
   * Définit les traitements nécessaires pour l'exécution une entrée de rapport. La tache de base de l'exécution des
   * rapports {@link TacheRapport} injecte le code client dans la table de paramètres.
   * 
   * @param parametres
   *          contient la liste des paramètres (sous la forme clé-valeur) nécessaire à l'exécution de l'entrée de
   *          rapport.
   * @return la valeur de l'exécution de l'entrée de rapport.
   */
  String executer(Map<ParametreExecutionMetadonnee, Object> parametres);
}
