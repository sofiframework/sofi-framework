/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.rapport.execution.TacheRapport;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.utilitaire.Periode;

/**
 * Interface de service fournissant les méthodes nécessaires à l'affichage des rapports.
 * <p>
 * Accès à la liste hiérarchisée et ordonnée des métadonnées de rapport.
 * <p>
 * Accès aux résultats des rapports.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface ServiceRapportResultat {

  /**
   * Retourne la liste des métadonnées de rapport ordonnées suivant leur hiérarchie parent-enfant et suivant la
   * propriété ordre affichage.
   * <p>
   * Exemple : <br>
   * Nom | Parent | Ordre affichage<br>
   * ------------------------------<br>
   * s1 | null | 1<br>
   * s2 | null | 2<br>
   * s3 | null | 3<br>
   * s31 | s3 | 1<br>
   * s11 | s1 | 1<br>
   * s12 | s1 | 2<br>
   * Dans ce cas, la liste résultat sera : s1, s2, s3, s11, s12, s3.
   * 
   * @param codeApplication
   *          le code de l'application.
   * @param codeClient
   *          le code du client.
   * @param codeGabarit
   *          le code du gabarit.
   * @return une liste de {@link RapportMetadonnee}.
   */
  List<RapportMetadonnee> getListeRapportMetadonneeOrdonnee(String codeApplication, String codeClient,
      String codeGabarit);

  /**
   * Permet d'obtenir la liste des résultats pour une liste de clients et une période donnée. Les résultats obtenus
   * correspondent à la somme des valeurs par métadonnée de rapport et par client.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeClient
   *          la liste des codes client dont on veut les résultats.
   * @param periode
   *          la période temporelle pour laquelle on veut les résultats.
   * @return une table dont les clés sont les identifiants de métadonnée de rapport et les valeurs la liste des sommes
   *         de résultat de rapport pour les clients demandés {@link RapportResultatAgrege}.
   */
  Map<Long, List<RapportResultatAgrege>> getListeRapportResultatAgrege(String codeApplication, List<String> codeClient,
      Periode periode);

  /**
   * Retourne la date des derniers résultats calculés par la tâche planifiée {@link TacheRapport} pour les paramètres
   * passés.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeClient
   *          la liste des codes client dont on veut la date des derniers résultats.
   * 
   * @return une {@link Date}.
   */
  Date getDateDernierResultat(String codeApplication, List<String> codeClient);
}
