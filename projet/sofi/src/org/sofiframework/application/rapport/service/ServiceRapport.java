/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.service;

import java.util.List;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;

/**
 * Service commun de la facette Rapport du framework SOFI. Cette interface
 * définit le contrat pour l'exécution des rapports.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface ServiceRapport {

  /**
   * Permet de récupérer la métadonnée de rapport d'identifiant passé en
   * paramètre.
   * 
   * @param id
   *          un identifiant
   * @return un objet de type {@link RapportMetadonnee}
   */
  RapportMetadonnee getRapportMetadonnee(Long id);

  /**
   * Retourne la liste des gabarits de rapport d'une application spécifique.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeClient
   *          code du client pour lequel on veut les gabarits actifs
   * @return la liste des gabarits de rapport.
   */
  List<RapportGabarit> getListeRapportGabaritActif(String codeApplication, String codeClient);

  /**
   * Permet d'enregistrer la liste d'objets en paramètre pour assurer le suivi
   * des entrées de rapport devant être calculées.
   * 
   * @param listeRapportTraitement
   *          une liste d'objet de type {@link RapportTraitement}.
   */
  void enregistrerListeRapportTraitement(List<RapportTraitement> listeRapportTraitement);

  /**
   * Permet d'obtenir les métadonnées en traitement pour l'application et avec
   * l'état spécifiés en paramètre.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeEtatTraitement
   *          voir {@link CodeEtatTraitementMetadonnee}.
   * 
   * @return une liste d'objet de type {@link RapportTraitement}.
   */
  List<RapportTraitement> getListeRapportTraitement(String codeApplication,
      CodeEtatTraitementMetadonnee codeEtatTraitement);

  /**
   * Supprime les métadonnées en traitement pour l'application et avec l'état
   * spécifiés en paramètre.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeEtatTraitement
   *          voir {@link CodeEtatTraitementMetadonnee}.
   * @return le nombre d'entités supprimées.
   */
  int supprimerRapportTraitement(String codeApplication, CodeEtatTraitementMetadonnee codeEtatTraitement);

  /**
   * Permet d'enregistrer les résultats de chaque entrée de rapport ayant été
   * calculées.
   * 
   * @param listeRapportResultat
   *          une liste d'objet de type {@link RapportResultat}.
   */
  void enregistrerListeRapportResultat(List<RapportResultat> listeRapportResultat);

}
