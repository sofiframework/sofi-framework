/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class FiltreRapportGabarit extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = 7083749080715177275L;

  private String codeApplication;
  private String codeClient;
  private String codeGabarit;
  private String codeStatut;

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getCodeGabarit() {
    return codeGabarit;
  }

  public void setCodeGabarit(String codeGabarit) {
    this.codeGabarit = codeGabarit;
  }

}
