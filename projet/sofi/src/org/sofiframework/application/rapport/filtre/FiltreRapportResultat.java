package org.sofiframework.application.rapport.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreRapportResultat extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = 179118157395281663L;

  private String codeApplication;
  private String[] codeClient;
  private Date dateResultat;
  private Date dateDebutResultat;
  private Date dateFinResultat;

  public FiltreRapportResultat() {
    ajouterIntervalle("dateDebutResultat", "dateFinResultat", "dateResultat");

  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public Date getDateResultat() {
    return dateResultat;
  }

  public void setDateResultat(Date dateResultat) {
    this.dateResultat = dateResultat;
  }

  public Date getDateDebutResultat() {
    return dateDebutResultat;
  }

  public void setDateDebutResultat(Date dateDebutResultat) {
    this.dateDebutResultat = dateDebutResultat;
  }

  public Date getDateFinResultat() {
    return dateFinResultat;
  }

  public void setDateFinResultat(Date dateFinResultat) {
    this.dateFinResultat = dateFinResultat;
  }

  public String[] getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

}
