/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.domaine;

import org.sofiframework.application.rapport.execution.TacheRapport;
import org.sofiframework.constante.Domaine;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class DomaineRapport extends Domaine {

  /**
   * Les différents états d'une métadonnée de rapport lors de l'exécution des rapports.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  public static enum CodeEtatTraitementMetadonnee implements ValeurDomaine {
    A_TRAITER, EN_COURS, TRAITEE, EN_ERREUR;

    @Override
    public String getValeur() {
      return name();
    }
  }

  /**
   * Les paramètres envoyés pour l'exécution de la métadonnée.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  public static enum ParametreExecutionMetadonnee {
    CODE_CLIENT, DEBUT_PERIODE, FIN_PERIODE;
  }

  /**
   * Les différents types d'une métadonnées de rapport.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  public static enum CodeTypeMetadonnee implements ValeurDomaine {
    /**
     * Type ne nécessitant pas de calcul ; une métadonnée de ce type n'aura pas de classe de traitement.
     */
    TITRE,
    /**
     * Ce type de métadonnée nécessite un calcul ; sa classe de traitement doit être renseignée. Elle sera appelée par
     * la tâche planifiée {@link TacheRapport} lors de son exécution.
     */
    COMPTE,
    /**
     * Ce type de métadonnée nécessite un calcul ; sa classe de traitement doit être renseignée. Elle sera appelée par
     * la tâche planifiée {@link TacheRapport} lors de son exécution.
     */
    MONTANT,
    /**
     * Ce type de métadonnée nécessite un calcul ; sa classe de traitement doit être renseignée. Elle sera appelée par
     * la tâche planifiée {@link TacheRapport} lors de son exécution.
     */
    POURCENTAGE;

    @Override
    public String getValeur() {
      return name();
    }
  }

  /**
   * Les différents modes de restitution des résultats d'une métadonnée.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  public static enum CodeModeResultat implements ValeurDomaine {
    /**
     * La valeur restituée correspondra au résultat obtenu à la date de fin de la période.
     */
    SIMPLE,
    /**
     * La valeur restituée correspondra à la somme des résultats de la métadonnée pour la période donnée.
     */
    SOMME,
    /**
     * La valeur restituée correspondra à la moyenne des résultats de la métadonnée pour la période donnée.
     */
    MOYENNE;

    @Override
    public String getValeur() {
      return name();
    }
  }
}
