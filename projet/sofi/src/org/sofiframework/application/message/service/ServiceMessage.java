/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.service;

import org.sofiframework.application.message.objetstransfert.Message;

/**
 * Interface spécifiant le service qui offre une service de message et qui implémente une méthode getMessage(...).
 * <p>
 * Votre service doit avoir une signature par exemple : public class ServiceMessageImpl implements ServiceMessage { ..
 * getMessage(..) { .. }
 * <p>
 * 
 * @author Steve Tremblay (Nurun inc.)
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public interface ServiceMessage {

  /**
   * 
   * @param identifiant
   * @param codeLangue
   * @param codeSexe
   * @return
   * @deprecated depuis 3.2, remplacée par {@link ServiceMessage#get(String, String, String)}
   */
  @Deprecated
  public Message getMessage(String identifiant, String codeLangue, String codeSexe);

  /**
   * Permet de retrouver un message à partir des paramètres spécifiés.
   * 
   * @param identifiant
   * @param codeLangue
   * @param codeSexe
   * @param codeClient
   *          le code client
   * @return un {@link Message}
   * @since 3.2
   */
  public Message get(String identifiant, String codeLangue, String codeClient);

}
