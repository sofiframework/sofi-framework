/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.service;

import org.sofiframework.application.message.objetstransfert.Libelle;

/**
 * Interface spécifiant le service qui offre une service de message et qui implémente une méthode getLibelle(...).
 * <p>
 * Votre service doit avoir une signature par exemple : public class ServiceLibelleImpl implements ServiceLibelle { ..
 * getLibelle(..) { .. }
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public interface ServiceLibelle {

  /**
   * 
   * @param identifiant
   * @param codeLangue
   * @param codeSexe
   * @return
   * @deprecated depuis 3.2, remplacée par {@link ServiceLibelle#get(String, String, String)}
   */
  @Deprecated
  public Libelle getLibelle(String identifiant, String codeLangue, String codeSexe);

  /**
   * Permet de retrouver un libellé à partir des paramètres spécifiés.
   * 
   * @param identifiant
   * @param codeLangue
   * @param codeClient
   *          le code du client
   * @return un objet {@link Libelle}
   * @since 3.2
   */
  public Libelle get(String identifiant, String codeLangue, String codeClient);
}
