/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.utilitaire.UtilitaireLocale;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe abstraite pour la gestion des messages et des libellés provenant de ressource tel qu'un SGBD, fichier plat,
 * etc..
 * 
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
abstract public class BaseGestionMessage extends BaseGestionService {
  /**
   * Liste des langues.
   */
  private HashSet<String> listeLocale = new HashSet<String>();

  /**
   * Langue qui doit êter utilisée par défault.
   */
  private Locale localeParDefaut = null;

  /**
   * Spécifie si la cache doit être vidé.
   */
  private boolean viderCache = false;

  /**
   * L'application à utiliser
   */
  private String application;

  /**
   * Le paramèter 1 à inclure dans le message.
   */
  private String parametre1;

  /**
   * Le paramèter 2 à inclure dans le message.
   */
  private String parametre2;

  /**
   * Le paramèter 3 à inclure dans le message.
   */
  private String parametre3;

  /**
   * Le paramèter 4 à inclure dans le message.
   */
  private String parametre4;

  /**
   * Le paramèter 5 à inclure dans le message.
   */
  private String parametre5;
  protected String aideParametre;

  protected BaseGestionMessage(@SuppressWarnings("rawtypes") Class classeServiceLocal) {
    super(classeServiceLocal);
  }

  /**
   * Obtenir si le cache est utilisé.
   * <p>
   * 
   * @return si le cache est utilisée
   */
  public boolean isViderCache() {
    return viderCache;
  }

  /**
   * Fixer si la cache doit être re-initialiser.
   * <p>
   * 
   * @param newIsViderCache
   *          si le cache est utilisé
   */
  public void setViderCache(boolean newIsViderCache) {
    this.viderCache = newIsViderCache;
  }

  /**
   * Retourne la liste de langues gérée par le gestionnaire de message.
   * 
   * @return la liste de langues.
   */
  public HashSet<String> getListeLocale() {
    return listeLocale;
  }

  /**
   * Fixer la liste de langue géré par le gestionnaire de messages.
   * 
   * @param newListeLangues
   */
  public void setListeLocale(HashSet<String> listeLocale) {
    this.listeLocale = listeLocale;
  }

  /**
   * Indique si la langue spécifié est géré
   * 
   * @param codeLangue
   * @return true si la langue spécifié est géré
   */
  public boolean isLocaleExiste(String codeLocale) {
    if (codeLocale == null) {
      return false;
    }

    return listeLocale.contains(codeLocale);
  }

  /**
   * Retourne la locale par défaut
   * 
   * @return la locale par défaut
   */
  public Locale getLocaleParDefaut() {
    return localeParDefaut;
  }

  /**
   * Fixer la locale par défaut si le locale de l'utilisateur n'est pas géré.
   * 
   * @param localeParDefaut
   */
  public void setLocaleParDefaut(Locale localeParDefaut) {
    this.localeParDefaut = localeParDefaut;
  }

  /**
   * Retourne le code de l'application
   * 
   * @return le code de l'application
   */
  public String getApplication() {
    return application;
  }

  /**
   * Fixer le code de l'application
   * 
   * @param application
   *          le code de l'application
   */
  public void setApplication(String application) {
    this.application = application;
  }

  /**
   * Obtenir le paramèter 1 à inclure dans le message.
   * 
   * @return Le paramèter 1 à inclure dans le message
   */
  public String getParametre1() {
    return parametre1;
  }

  /**
   * Fixer le paramèter 1 à inclure dans le message.
   * 
   * @param parametre1
   *          le paramèter 1 à inclure dans le message
   */
  public void setParametre1(String parametre1) {
    this.parametre1 = parametre1;
  }

  /**
   * Obtenir le paramèter 2 à inclure dans le message.
   * 
   * @return le paramèter 2 à inclure dans le message
   */
  public String getParametre2() {
    return parametre2;
  }

  /**
   * Fixer le paramèter 2 à inclure dans le message.
   * 
   * @param parametre2
   *          le paramèter 2 à inclure dans le message
   */
  public void setParametre2(String parametre2) {
    this.parametre2 = parametre2;
  }

  /**
   * Obtenir le paramèter 3 à inclure dans le message.
   * 
   * @return le paramèter 3 à inclure dans le message
   */
  public String getParametre3() {
    return parametre3;
  }

  /**
   * Fixer le paramèter 3 à inclure dans le message.
   * 
   * @param parametre3
   *          le paramèter 3 à inclure dans le message
   */
  public void setParametre3(String parametre3) {
    this.parametre3 = parametre3;
  }

  /**
   * Obtenir le paramèter 4 à inclure dans le message.
   * 
   * @return le paramèter 4 à inclure dans le message
   */
  public String getParametre4() {
    return parametre4;
  }

  /**
   * Fixer le paramèter 4 à inclure dans le message.
   * 
   * @param parametre4
   *          le paramèter 4 à inclure dans le message
   */
  public void setParametre4(String parametre4) {
    this.parametre4 = parametre4;
  }

  /**
   * Obtenir le paramèter 5 à inclure dans le message.
   * 
   * @return le paramèter 5 à inclure dans le message
   */
  public String getParametre5() {
    return parametre5;
  }

  /**
   * Fixer le paramèter 5 à inclure dans le message.
   * 
   * @param parametre5
   *          le paramèter 5 à inclure dans le message
   */
  public void setParametre5(String parametre5) {
    this.parametre5 = parametre5;
  }

  /**
   * Convertit un message pour qu'il soit standard avec la norme java de message dynamique.
   * 
   * @param message
   *          Message à qui l'on désire remplacer les paramètres
   * @return message après conversion des paramètres dynamiques.
   */
  public String convertirFormatMessage(String message) {
    message.replaceFirst(getParametre1(), "{0}");
    message.replaceFirst(getParametre2(), "{1}");
    message.replaceFirst(getParametre3(), "{2}");
    message.replaceFirst(getParametre4(), "{3}");
    message.replaceFirst(getParametre5(), "{4}");

    return message;
  }

  /**
   * Retourne la langue si elle est disponigble. Si non la méthode retourne la langue par défault
   * 
   * @param langueCourante
   *          Code de langue que l'on désire utiliser
   * @return le code de langue à utiliser
   */
  public Locale getLocaleDisponible(String codeLocale) {
    if (isLocaleExiste(codeLocale)) {
      return UtilitaireLocale.getLocale(codeLocale);
    } else {
      // Valider si présence de la même langue dans les locales dispo.
      String codeLangue = null;
      if (codeLocale != null) {
        if (codeLocale.indexOf("_") != -1) {
          codeLangue = codeLocale.substring(0, codeLocale.indexOf("_"));
        } else {
          codeLangue = codeLocale.toLowerCase();
        }
      }
      Iterator<String> iterateur = this.listeLocale.iterator();

      while (iterateur.hasNext()) {
        String locale = iterateur.next();
        if (codeLangue != null && locale.indexOf(codeLangue) != -1) {
          return UtilitaireLocale.getLocale(locale);
        }

      }
      return getLocaleParDefaut();
    }
  }

  /**
   * Permet de générer un message avec l'aide contextuelle incluse.
   * 
   * @param message
   *          le message original.
   * @param codeLangue
   *          le code de langue.
   * @param le
   *          code de sexe de l'utilisateur.
   * @return le message complet avec aide contextuelle.
   * @deprecated depuis 3.2, remplacée par
   *             {@link BaseGestionMessage#genererAideContextuelleComplete(String, String, String)}
   */
  @Deprecated
  public String genererAideContextuelle(String message, String codeLangue, String codeSexe) {
    return genererAideContextuelleComplete(message, codeLangue, null);
  }

  /**
   * 
   * @param message
   * @param codeLangue
   * @param codeClient
   * @return
   */
  public String genererAideContextuelleComplete(String message, String codeLangue, String codeClient) {
    StringBuffer messageComplet = new StringBuffer();

    boolean fin = false;
    int compteur = 1;
    int indexDebut = message.indexOf(getIndicateurAide(compteur));

    if (indexDebut != -1) {
      messageComplet.append(message.substring(0, indexDebut));
      message = message.substring(indexDebut);

      while (!fin) {
        compteur++;

        int indexFin = message.indexOf(getIndicateurAide(compteur));
        String cle = message.substring(2, indexFin);
        Message libelleAide = get(cle, codeLangue, (Object) null, codeClient);
        String messageAide = appliquerDecorateurAide(libelleAide);
        messageComplet.append(messageAide);
        compteur++;
        indexFin = indexFin + 2;
        message = message.substring(indexFin);
        indexDebut = message.indexOf(getIndicateurAide(compteur));

        if (indexDebut == -1) {
          fin = true;
          messageComplet.append(message);
        } else {
          messageComplet.append(message.substring(indexFin, indexDebut));
          message = message.substring(indexDebut);
        }
      }

      return messageComplet.toString();
    } else {
      return message;
    }
  }

  /**
   * Générer l'indicateur aide.
   * 
   * @param compteur
   *          le compteur
   * @return l'indicateur aide.
   */
  protected String getIndicateurAide(int compteur) {
    return getAideParametre() + compteur;
  }

  /**
   * Appliquer le décorateur d'aide contextuelle.
   * 
   * @param libelleAide
   *          le libellé aide.
   * @return le libellé décoré
   */
  private String appliquerDecorateurAide(Message libelleAide) {
    if (!UtilitaireString.isVide(libelleAide.getMessage())) {
      StringBuffer aideContextuelle = new StringBuffer();
      aideContextuelle.append("<label ");
      aideContextuelle.append("class=\"");
      aideContextuelle.append("motCle");

      if (UtilitaireString.isVide(libelleAide.getAideContextuelle())) {
        aideContextuelle.append("\"");
      } else {
        aideContextuelle.append("\" title=\"");
        aideContextuelle.append(libelleAide.getAideContextuelle());
      }

      aideContextuelle.append("\">");
      aideContextuelle.append(libelleAide.getMessage());
      aideContextuelle.append("</label>");

      return aideContextuelle.toString();
    }

    return "";
  }

  /**
   * Méthode abstraite qui retourne un libellé
   * 
   * @param identifiant
   *          l'identifiant du libellé.
   * @param codeLangue
   *          le code de langue.
   * @param codeSexe
   *          le code de sexe.
   * @param parametres
   *          les paramètres dynamique.
   * @return Message le libelle du message.
   * @deprecated remplacée par {@link BaseGestionMessage#get(String, String, Object, String)}.
   */
  @Deprecated
  abstract public Message getLibelle(String identifiant, String codeLangue, String codeSexe, Object parametres);

  /**
   * Méthode abstraite qui retourne un libellé
   * 
   * @param identifiant
   *          l'identifiant du libellé.
   * @param codeLangue
   *          le code de langue.
   * @param codeSexe
   *          le code de sexe.
   * @param parametres
   *          les paramètres dynamique.
   * @return Message le libelle du message.
   */
  abstract public Message get(String identifiant, String codeLangue, Object parametres, String codeClient);

  /**
   * Retourne le paramètre qui indique l'aide contextuelle dans une partie du libellé du message.
   * 
   * @return le paramètre qui indique l'aide contextuelle dans une partie du libellé du message.
   */
  public String getAideParametre() {
    return aideParametre;
  }

  /**
   * Fixer le paramètre qui indique l'aide contextuelle dans une partie du libellé du message.
   * 
   * @param aideParametre
   *          le paramètre qui indique l'aide contextuelle dans une partie du libellé du message.
   */
  public void setAideParametre(String aideParametre) {
    this.aideParametre = aideParametre;
  }

  /**
   * Permet de construire la clé utilisé pour identifier le message dans la cache.
   * 
   * @param cleMessage
   *          la clé du message ou du libellé.
   * @param locale
   *          la locale (fr_CA, en_US, ...)
   * @param codeClient
   *          le code du client ; peut être null : dans ce cas le <code>codeClient</code> ne sera pas utilisé pour
   *          construire la clé de la cache.
   * @return une clé unique au format &lt;cleMessage&gt;.&lt;codeClient&gt;.&lt;codeLocale&gt;
   */
  public String construireCleCache(String cleMessage, Locale locale, String codeClient) {
    StringBuilder cleCacle = new StringBuilder();
    cleCacle.append(cleMessage);
    if (StringUtils.isNotEmpty(codeClient)) {
      cleCacle.append(".");
      cleCacle.append(codeClient);
    }
    cleCacle.append(".");
    cleCacle.append(locale);
    return cleCacle.toString();
  }
}
