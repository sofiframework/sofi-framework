/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.objetstransfert;

import java.text.MessageFormat;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Objet de transfert permettant de spécifier un message d'erreur
 * d'information ou encore un libellé. Ce message peut être associé à une
 * aide contextuelle et/ou une aide externe, soit possiblement un appel
 * à une autre page.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.0.3 Il est possible d'extraire la référence associé (ex. un code de message normalisé)
 */
public class Message extends ObjetTransfert {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = 372722432626039768L;

  //La clé du message.
  private String cleMessage;

  //Le message prèt à l'affichage.
  private String message;

  // Le message avec des tokens dynamique.
  private MessageFormat messageFormat;

  //L'aide contextuelle
  private String aideContextuelle;

  //L'aide contextuelle en format HTML
  private String aideContextuelleHtml;

  //L'aide général
  private String aide;

  //La clé de l'aide général.
  private String cleAide;
  private String codeLangue;

  // L'aide contextuelle externe
  private String aideContextuelleExterne;

  // La référence associé.
  private String reference;

  // Un aide à la saisie.
  private String placeholder;

  /**
   * Constructeur par défaut
   */
  public Message() {
  }

  /**
   * Retourne la clé du message
   * @return la clé du message
   */
  public String getCleMessage() {
    return cleMessage;
  }

  /**
   * Fixer la clé du message
   * @param cle la clé du message
   */
  public void setCleMessage(String cleMessage) {
    this.cleMessage = cleMessage;
  }

  /**
   * Retourne le message prêt à l'affichage
   * @return le message prêt à l'affichage
   */
  public String getMessage() {
    return message;
  }

  /**
   * Fixer le message prêt à l'affichage
   * @param message le message prêt à l'affichagel
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Retourne l'aide contextuelle
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle
   * @param aideContextuelle l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne l'aide général sur un mot ou des mots du message ou du
   * libellé
   * @return l'aide général
   */
  public String getAide() {
    return aide;
  }

  /**
   * Fixer l'aide général sur un mot ou des mots du message ou du libellé
   * @param aide l'aide général
   */
  public void setAide(String aide) {
    this.aide = aide;
  }

  /**
   * Retourne le message avec les paramètres dynamique possible.
   * {0},{1},...
   * @return le message avec les paramètres dynamique possible.
   */
  public MessageFormat getMessageFormat() {
    return messageFormat;
  }

  /**
   * Fixer le message avec les paramètres dynamique possible.
   * {0},{1},...
   * @param messageFormat le message avec les paramètres dynamique possible.
   */
  public void setMessageFormat(MessageFormat messageFormat) {
    this.messageFormat = messageFormat;
  }

  /**
   * Retourne la référence de l'aide utilisateur.
   * @return a référence de l'aide utilisateur.
   */
  public String getCleAide() {
    return cleAide;
  }

  /**
   * Fixer la référence de l'aide utilisateur.
   * @param cleAide la référence de l'aide utilisateur.
   */
  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  /**
   * Fixer l'aide contextuelle en format Html.
   * @param aideContextuelleHtml l'aide contextuelle en format Html.
   */
  public void setAideContextuelleHtml(String aideContextuelleHtml) {
    this.aideContextuelleHtml = aideContextuelleHtml;
  }

  /**
   * Retourne l'aide contextuelle en format Html.
   * @return l'aide contextuelle en format Html.
   */
  public String getAideContextuelleHtml() {
    return aideContextuelleHtml;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Fixer l'aide contextuelle externe soit un url ou soit une page.
   * @param aideContextuelleExterne l'aide contextuelle externe soit un url ou soit une page.
   */
  public void setAideContextuelleExterne(String aideContextuelleExterne) {
    this.aideContextuelleExterne = aideContextuelleExterne;
  }

  /**
   * Retourne l'aide contextuelle externe soit un url ou soit une page.
   * @return l'aide contextuelle externe soit un url ou soit une page.
   */
  public String getAideContextuelleExterne() {
    return aideContextuelleExterne;
  }

  /**
   * Fixer la référence associé.
   * @param reference la référence associé.
   */
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Retourne la référence associé.
   * @return la référence associé.
   */
  public String getReference() {
    return reference;
  }

  /**
   * @param placeholder the placeholder to set
   */
  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  /**
   * @return the placeholder
   */
  public String getPlaceholder() {
    return placeholder;
  }
}
