/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.objetstransfert;


/**
 * Objet de transfert representant un erreur généré par l'application.
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version 1.0
 */
public class MessageErreur extends Message {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -4114599282375247704L;

  /**
   * Champs qui est associé au message.
   */
  private String focus;

  /**
   * Numéro de la ligne en erreur.
   */
  private Integer noLigne;

  /**
   * Nomde la liste en erreur.
   */
  private String nomListe;

  /**
   * Constructeur par défault.
   */
  public MessageErreur() {
  }

  /**
   * Contructeur d'un message d'erreur.
   * @param newMessage nouveau message
   * @param newFocus nouveau focus
   */
  public MessageErreur(String newMessage, String newFocus) {
    setMessage(newMessage);
    this.focus = newFocus;
  }

  /**
   * Contructeur d'un message d'erreur.
   * @param newMessage nouveau message
   * @param newFocus nouveau focus
   * @param newNomListe nouvelle liste
   * @param newNoLigne nouveau numéro de ligne
   */
  public MessageErreur(String newMessage, String newFocus, String newNomListe,
      Integer newNoLigne) {
    setMessage(newMessage);
    this.focus = newFocus;
    this.nomListe = newNomListe;
    this.noLigne = newNoLigne;
  }

  /**
   * Fixer l'attribut qui est en erreur afin d'avoir la possibilté
   * d'automatiser le focus sur le champ en erreur.
   * Correspond au nom d'attribut du champ en erreur.
   * @return le nom d'attribut en erreur.
   */
  public String getFocus() {
    return focus;
  }

  /**
   * Fixer l'attribut qui est en erreur afin d'avoir la possibilté
   * d'automatiser le focus sur le champ en erreur.
   * Correspond au nom d'attribut du champ en erreur.
   * @param newFocus le nom d'attribut en erreur.
   */
  public void setFocus(String newFocus) {
    focus = newFocus;
  }

  /**
   * Retourne le numéro de ligne de l'attribut en erreur.
   * Utilisé lors d'une erreur d'un attribut de type nested.
   * @return le numéro de ligne en erreur.
   */
  public Integer getNoLigne() {
    return noLigne;
  }

  /**
   * Fixer le numéro de ligne de l'attribut en erreur.
   * Utilisé lors d'une erreur d'un attribut de type nested.
   * @param newNoLigne
   */
  public void setNoLigne(Integer newNoLigne) {
    noLigne = newNoLigne;
  }

  /**
   * Retourne le nom de la liste d'ou l'attribut en erreur.
   * Utilisé lors d'une erreur d'un attribut de type nested.
   * @return le nom de liste de formulaire imbriqués (Nested).
   */
  public String getNomListe() {
    return nomListe;
  }

  /**
   * Fixer le nom de la liste d'ou l'attribut en erreur.
   * Utilisé lors d'une erreur d'un attribut de type nested.
   * @param newNomListe
   */
  public void setNomListe(String newNomListe) {
    nomListe = newNomListe;
  }
}
