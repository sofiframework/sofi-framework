/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.objetstransfert;

/**
 * Objet de transfert permettant de spécifier un libellé. Ce libellé peut être associé à une aide contextuelle et/ou une
 * aide externe, soit possiblement un appel à une autre page.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.application.message.objetstransfert.Message
 */
public class Libelle extends Message {
  // Compatiblité avec SOFI 2.0
  private static final long serialVersionUID = 3293474070906384227L;

  /** Constructeur par défaut */
  public Libelle() {
  }
}
