/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.objetstransfert;


/**
 * Objet de transfert representant un message informatif pouvant être
 * affiché à l'utilisateur
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class MessageInformatif extends Message {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = 1462627297260860102L;

  /** Constructeur par défaut */
  public MessageInformatif() {
  }
}
