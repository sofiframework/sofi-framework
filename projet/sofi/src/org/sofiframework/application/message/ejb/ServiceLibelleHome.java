/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message.ejb;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * Interface local permettant d'accéder à un service distant permettant
 * la gestion des libellés.
 * <p>
 * Le service implémenté doit donc être basé sur cette interface.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public interface ServiceLibelleHome extends EJBHome {
  RemoteServiceLibelle create()
      throws CreateException, java.rmi.RemoteException;
}
