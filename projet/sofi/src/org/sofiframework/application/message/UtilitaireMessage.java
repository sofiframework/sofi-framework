/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe permettant d'extraire un message provenant du gestionnaire des messages soit GestionMessage.
 * <p>
 * 
 * @author Jean-François Brasard (Nurun inc.)
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 * @see org.sofiframework.application.message.GestionMessage
 */
public class UtilitaireMessage {
  /**
   * Obtenir un message.
   * 
   * @param identifiant
   *          Identifiant unique du message
   * @param request
   *          requête http en cours de traitement.
   * @return message
   */
  public static Message get(String identifiant, HttpServletRequest request) {
    Message message = getMessagePourUtilisateur(identifiant, null, request.getSession(), null);

    return message;
  }

  /**
   * Retourne le message basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param parametres
   *          les parametres dynamique au message s'il y a lieu, sinon mettre null
   * @param contexte
   *          la page en traitement
   * @return le message basé sur la langue de l'utilisateur.
   */
  public static Message get(String identifiant, Object[] parametres, PageContext contexte) {
    return get(identifiant, parametres, contexte, null);
  }

  /**
   * Retourne le message basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param parametres
   *          les parametres dynamique au message s'il y a lieu, sinon mettre null
   * @param contexte
   *          la page en traitement
   * @param codeClient
   *          le code du client
   * @return le message basé sur la langue de l'utilisateur.
   */
  public static Message get(String identifiant, Object[] parametres, PageContext contexte, String codeClient) {
    Message message = getMessagePourUtilisateur(identifiant, parametres, contexte.getSession(), codeClient);

    return message;
  }

  /**
   * Obtenir un message selon la langue
   * 
   * @param identifiant
   *          l'identifiant du message d'erreur
   * @param parametres
   *          tableau de paramètres à intégrer au message
   * @param request
   *          La requête HTTP en traitement
   * @return un message.
   */
  public static Message get(String identifiant, Object[] parametres, HttpServletRequest request) {
    Message message = getMessagePourUtilisateur(identifiant, parametres, request.getSession(), null);

    return message;
  }

  /**
   * Obtenir un message.
   * 
   * @param identifiant
   *          Identifiant unique du message
   * @param codeLangue
   *          la langue du message demandé
   * @param codeSexe
   *          le sexe de l'utilisateur afin de chercher le genre s'il y lieu
   * @return le message
   * @deprecated depuis 3.2, remplacé par {@link UtilitaireMessage#getMessage(String, String, String)}
   */
  @Deprecated
  public static Message get(String identifiant, String codeLangue, String codeSexe) {
    Message message = GestionMessage.getInstance().get(identifiant, codeLangue, (Object) null, null);

    return message;
  }

  /**
   * Obtenir un message.
   * 
   * @param identifiant
   *          Identifiant unique du message
   * @param codeLangue
   *          la langue du message demandé
   * @param codeSexe
   *          le sexe de l'utilisateur afin de chercher le genre s'il y lieu
   * @return le message
   */
  public static Message getMessage(String identifiant, String codeLangue, String codeClient) {
    Message message = GestionMessage.getInstance().get(identifiant, codeLangue, (Object) null, codeClient);

    return message;
  }

  /**
   * Méthode privé qui retourne un message pour la langue et sexe de l'utilisateur s'il y a lieu
   * 
   * @param utilisateur
   *          l'utilisateur demandant un message
   * @param parametres
   *          les paramètres dynamique du message s'il y a lieu
   * @param la
   *          langue du Locale.
   * @param parametres
   *          les paramètres dynaniquement du message.
   * @param codeClient
   *          le code du client
   * @return le message
   */
  private static Message getMessagePourUtilisateur(String identifiant, Object[] parametres, HttpSession session,
      String codeClient) {
    String langueLocale = null;

    // Extraire l'utilisateur de la session.
    String langueUtilisateur = UtilitaireControleur.getLanguePourUtilisateur(session);

    // Si l'utilisateur n'a pas de langue utiliser la langue du Locale
    if (UtilitaireString.isVide(langueUtilisateur)) {
      langueUtilisateur = langueLocale;
    }

    Message message = GestionMessage.getInstance().get(identifiant, langueUtilisateur, parametres, codeClient);

    return message;
  }

  /**
   * Obtenir un message d'erreur selon la langue et le sauvegarder soit dans le request ou encore dans la session de
   * l'utilisateur.
   * 
   * @param messageErreur
   *          le message d'erreur
   * @param application
   *          le code d'application s'il y lieu sinon null
   * @param request
   * @return la collection d'erreurs.
   */
  public static MessageErreur getMessageErreur(MessageErreur messageErreur, HttpServletRequest request) {
    return getMessageErreur(messageErreur, null, request);
  }

  /**
   * Obtenir un message d'erreur selon la langue et le sauvegarder soit dans le request ou encore dans la session de
   * l'utilisateur
   * 
   * @param messageErreurInitial
   *          le message d'erreur initial
   * @param parametres
   *          tableau de paramêtres a intégrer au message
   * @param request
   *          La requête HTTP en traitement
   * @return le message d'erreur.
   */
  public static MessageErreur getMessageErreur(MessageErreur messageErreurInitial, Object[] parametres,
      HttpServletRequest request) {
    Message message = get(messageErreurInitial.getCleMessage(), parametres, request);
    MessageErreur messageErreur = null;

    if (MessageErreur.class.isInstance(message)) {
      messageErreur = (MessageErreur) message;
    } else {
      messageErreur = new MessageErreur();
      messageErreur.setMessage(message.getMessage());
    }

    if (messageErreurInitial.getFocus() != null) {
      messageErreur.setFocus(messageErreurInitial.getFocus());
    }

    if (messageErreurInitial.getNomListe() != null) {
      messageErreur.setNomListe(messageErreurInitial.getNomListe());
    }

    if (messageErreurInitial.getNoLigne() != null) {
      messageErreur.setNoLigne(messageErreurInitial.getNoLigne());
    }

    return messageErreur;
  }

  /**
   * Obtenir un message d'erreur selon la langue et le sauvegarder soit dans le request ou encore dans la session de
   * l'utilisateur
   * 
   * @param cleMessage
   *          la clé du message.
   * @param parametres
   *          tableau de paramêtres a intégrer au message
   * @param request
   *          La requête HTTP en traitement
   * @return le message d'erreur.
   */
  public static MessageErreur getMessageErreur(String cleMessage, Object[] parametres, HttpServletRequest request) {
    Message message = get(cleMessage, parametres, request);
    MessageErreur messageErreur = null;

    if (MessageErreur.class.isInstance(message)) {
      messageErreur = (MessageErreur) message;
    } else {
      messageErreur = new MessageErreur();
      messageErreur.setMessage(message.getMessage());
    }

    return messageErreur;
  }
}