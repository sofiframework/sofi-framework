/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import java.io.Serializable;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;
import org.sofiframework.application.message.objetstransfert.Message;


/**
 * Classe utilitaire permettant d'extraire un message provenant d'un fichier ressource.
 * <p>
 * Il permet aussi de spécifier un message d'erreur dans le request ou la session de l'utilisateur.
 * Un fichier ressource doit être spécifier dans le constructeur, il doit correspondre a ce format:
 * nomDuRepertoire.nomDuFichier
 * Par exemple:
 * message.Ressource
 * <p>
 * A noter que le multilingue n'est pas traité par cette classe utilitaire.
 * @author Jean-François Brasard
 * @version SOFI 1.0
 */
public class UtilitaireMessageFichier implements Serializable {

  private static final long serialVersionUID = -4156522157097155465L;

  /** Instance commune de journalisation */
  protected static Log log = LogFactory.getLog(UtilitaireMessageFichier.class);

  /**
   * Référence à l'instance de gestion des objets sécurisables.
   */
  private static final UtilitaireMessageFichier utilitaireMessageFichier =
      new UtilitaireMessageFichier();

  /** Le nom du fichier ressource */
  private String nomFichier;

  /** Le fichier ressource */
  private MessageResources mesRessources = null;

  /**
   * Obtenir l'instance unique.
   * @return l'instance unique.
   */
  public static UtilitaireMessageFichier getInstance() {
    return utilitaireMessageFichier;
  }

  /**
   * Le constructeur.
   * <p>
   * Le constructeur doit recevoir le nom du fichier ressource.
   * Par exemple:
   * message.Ressource
   * <p>
   */
  public UtilitaireMessageFichier() {
  }

  /**
   * Obtenir un message selon la langue
   * <p>
   * @param cle la clé du message
   * @return String le message
   */
  public String get(String cle, HttpServletRequest request) {
    mesRessources = setLanguage(request);

    String message = mesRessources.getMessage(cle);

    if (message == null) {
      message = cle;
    }

    return message;
  }

  /**
   * Retourne le message dans le fichier de base pour la clé spécifié.
   * @return le message dans le fichier
   * @param cle la clé demandé.
   */
  public String get(String cle) {
    mesRessources = MessageResourcesFactory.createFactory().createResources(nomFichier);

    String message = mesRessources.getMessage(cle);

    if (message == null) {
      message = cle;
    }

    return message;
  }

  /**
   * Obtenir un message d'erreur selon la langue
   * et le sauvegarder soit dans le request ou encore dans la session de l'utilisateur
   * <p>
   * @param messageErreur l'objet de transfert MessageErreur
   * @param request La requête HTTP en traitement
   * @param isPlacerDansLaSession true/false si on veut placer le message d'erreur dans la session
   */
  public void setMessageErreur(Message messageErreur,
      HttpServletRequest request, boolean isPlacerDansLaSession) {
    setMessageErreur(messageErreur, null, request, isPlacerDansLaSession);
  }

  /**
   * Obtenir un message d'erreur selon la langue
   * et le sauvegarder soit dans le request ou encore dans la session de l'utilisateur
   * <p>
   * @param messageErreur l'objet de transfert MessageErreur
   * @param parametres La liste des paramètres à formatter du message
   * @param request La requête HTTP en traitement
   * @param isPlacerDansLaSession true/false si on veut placer le message d'erreur dans la session
   */
  public void setMessageErreur(Message messageErreur, String[] parametres,
      HttpServletRequest request, boolean isPlacerDansLaSession) {
    // Extraire la clé correspondante à la clé de SOFI.
    String cleMessage = get(messageErreur.getCleMessage(), request);

    if (!cleMessage.equals(messageErreur.getCleMessage())) {
      // Extraire le message pour la clé.
      Message message = UtilitaireMessage.get(cleMessage, parametres, request);

      // Fixer la description du message selon la langue pour le message d'erreur courant.
      messageErreur.setMessage(message.getMessage());
    }
  }

  /**
   * Spécifier le bon fichier de ressource correspondant à la langue de l'utilisateur
   * <p>
   * @param request La requête HTTP en traitement
   * @return MessageResources Le fichier de ressource
   */
  private MessageResources setLanguage(HttpServletRequest request) {
    Locale locale = (Locale) request.getSession().getAttribute(Globals.LOCALE_KEY);

    if (locale.getLanguage().equals("fr")) {
      mesRessources = MessageResourcesFactory.createFactory().createResources(nomFichier);
    } else {
      mesRessources = MessageResourcesFactory.createFactory().createResources(nomFichier);
    }

    return mesRessources;
  }

  public String getNomFichier() {
    return nomFichier;
  }

  public void setNomFichier(String nomFichier) {
    this.nomFichier = nomFichier;
  }
}
