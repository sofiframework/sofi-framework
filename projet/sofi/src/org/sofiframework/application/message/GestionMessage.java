/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Objet permettant la gestion des messages provenant de table d'une base de données.
 * 
 * La configuration se fait en configurant MessagePlugin.
 * 
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * 
 * @author Steve Tremblay (Nurun inc.)
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @author Jean-François Brassard (Nurun inc.)
 * @see org.sofiframework.presentation.struts.plugin.MessagePlugIn
 * @version SOFI 1.0
 */
public class GestionMessage extends BaseGestionMessage {
  /**
   * Référence à l'objet de gestion des messages. 
   */
  private static GestionMessage gestionMessageRef = new GestionMessage();

  /**
   * L'instance de journalisation pour cette classe.
   */
  private static final Log log = LogFactory.getLog(GestionMessage.class);

  /**
   * Liste des Messages en cache.
   */
  private HashMap<String, Object> listeMessages = new HashMap<String, Object>();

  /**
   * Service distant à utiliser.
   */
  private ServiceMessage serviceMessage;

  private GestionMessage() {
    super(org.sofiframework.application.message.service.ServiceMessage.class);
    this.initialiser();
  }

  /**
   * Initialiser le fichier de conversion de messages à partir du paramètre système.
   */
  public void initialiser() {
    if (GestionParametreSysteme.getInstance().isListeParametresIntialisee()) {
      String nomFichier = GestionParametreSysteme.getInstance().getString("configurationMessage");
      if (StringUtils.isNotBlank(nomFichier)) {
        UtilitaireMessageFichier.getInstance().setNomFichier(nomFichier);
      }
    }
  }

  /**
   * Retourne l'instance unique de gestion des messages.
   * 
   * @return le gestionnaire des messages.
   */
  public static GestionMessage getInstance() {
    return gestionMessageRef;
  }

  /**
   * Retourne la référence du fichier de configuration des messages.
   * 
   * @return la référence du fichier de configuration des messages.
   */
  public UtilitaireMessageFichier getFichierConfiguration() {
    return UtilitaireMessageFichier.getInstance();
  }

  /**
   * Obtenir le libellé du message.
   * <p>
   * 
   * @param identifiant
   *          Identifiant unique du message
   * @param codeLangue
   *          le code de la langue
   * @param codeClient
   * @return une description de message
   */
  @Override
  public synchronized Message get(String identifiant, String codeLocale, Object parametres, String codeClient) {
    if (identifiant == null) {
      Message message = new Message();
      message.setMessageFormat(new MessageFormat(""));
      return message;
    }

    // Sélectionner le locale disponible si la locale demandé n'est pas disponible.
    Locale locale = getLocaleDisponible(codeLocale);
    if (locale == null) {
      locale = getLocaleParDefaut();
    }

    // À chacune des journées la cache des messages est vidé.
    if (!getDateDerniereMaj().equals(UtilitaireDate.getDateJourEn_AAAA_MM_JJ())) {
      setViderCache(true);
      setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
    }

    if (isViderCache()) {
      listeMessages.clear();
      setViderCache(false);
      if (log.isInfoEnabled()) {
        log.info("Le cache des messages est vidé!");
      }
    }

    String cleCache = construireCleCache(identifiant, locale, codeClient);
    Message message = (Message) listeMessages.get(cleCache.toString());

    if (message == null) {
      MessageFormat messageFormat = null;
      message = getMessage(identifiant, locale.toString(), codeClient);

      if ((message != null) && (getParametre1() != null) && !getParametre1().equals("{0}")) {
        // Si le message n'est pas dans le format {0}, {1}, etc.. Il faut le convertir dans ce format.
        messageFormat = new MessageFormat(convertirFormatMessage(message.getMessage()));
      }

      // Si le message m'existe pas, placé l'identifiant comme message.
      if ((message == null)) {
        message = new Message();
        messageFormat = new MessageFormat(UtilitaireString.traiterApostrophe(identifiant));
      } else {
        String messageOriginal = UtilitaireString.traiterApostrophe(message.getMessage());
        messageFormat = new MessageFormat(messageOriginal);
      }

      message.setMessageFormat(messageFormat);

      if (message.getAideContextuelle() != null) {
        Message aideContextuelle = getMessage(message.getAideContextuelle(), locale.toString(), codeClient);

        if (aideContextuelle != null) {
          message.setAideContextuelle(aideContextuelle.getMessage());
        }
      }

      listeMessages.put(cleCache, message);
    }

    Message messageTmp = null;

    if (parametres != null) {
      messageTmp = (Message) message.clone();
    } else {
      messageTmp = message;
    }

    // Affecter le message complet avec les paramètres dynamique s'il y a lieu
    messageTmp.setMessage(message.getMessageFormat().format(parametres));

    return messageTmp;

  }

  /**
   * Obtenir le libellé du message.
   * <p>
   * 
   * @param identifiant
   *          Identifiant unique du message
   * @param codeLangue
   *          le code de la langue
   * @param codeSexe
   *          le code du sexe
   * @param parametres
   * @return une description de message
   * @deprecated depuis 3.2, remplacée par {@link GestionMessage#get(String, String, Object, String)}.
   */
  @Override
  @Deprecated
  public Message getLibelle(String identifiant, String codeLocale, String codeSexe, Object parametres) {
    return get(identifiant, codeLocale, parametres, null);
  }

  /**
   * Permet de récupérer un message pour les paramètres spécifiés. Si le message n'est pas trouvé pour le code client
   * passé en paramètre, la méthode recherche récursivement dans l'ascendance du client un message correspondant.
   * 
   * @param identifiant
   *          la clé du message
   * @param codeLangue
   *          le code de langue (fr_CA, en_US,...))
   * @param codeClient
   *          le code du client
   * @return un objet {@link Message}
   */

  public Message getMessage(String identifiant, String codeLangue, String codeClient) {
    Message message = serviceMessage.get(identifiant, codeLangue, codeClient);
    if (message == null) {
      List<Client> listeClientAscendant = GestionSecurite.getInstance().getListeClientAscendant(codeClient);
      for (Client ascendant : listeClientAscendant) {
        message = serviceMessage.get(identifiant, codeLangue, ascendant.getNoClient());
        if (message != null) {
          return message;
        }
      }
      if(message == null) {
        message = serviceMessage.get(identifiant, codeLangue, null);
      }
    }
    return message;
  }

  /**
   * Supprimer une clé d'un message du gestionnaire des message.
   * 
   * @param cleMessage
   *          la clé du message à supprimer.
   */
  public void supprimer(String cleMessage) {
    Iterator<String> iterateur = getListeLocale().iterator();

    while (iterateur.hasNext()) {
      String locale = iterateur.next();
      StringBuffer cleComplete = new StringBuffer();
      cleComplete.append(cleMessage);
      cleComplete.append(".");
      cleComplete.append(locale);
      this.listeMessages.remove(cleComplete.toString());
    }
  }
  
  /**
   * Supprimer une clé d'un message du gestionnaire des message.
   * 
   * @param cleMessage
   *          la clé du message à supprimer.
   */
  public void supprimer(String cleMessage, String codeClient) {
    Iterator<String> iterateur = getListeLocale().iterator();

    while (iterateur.hasNext()) {
      String locale = iterateur.next();
      StringBuffer cleComplete = new StringBuffer();
      cleComplete.append(cleMessage);
      cleComplete.append(".");
      cleComplete.append(codeClient);
      cleComplete.append(".");    
      cleComplete.append(locale);
      this.listeMessages.remove(cleComplete.toString());
    }
  }  

  /**
   * Initialiser la cache gestionnaire des messages.
   */
  public void initialiserCache() {
    listeMessages.clear();
  }

  public ServiceMessage getServiceMessage() {
    return serviceMessage;
  }

  public void setServiceMessage(ServiceMessage serviceMessage) {
    this.serviceMessage = serviceMessage;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceMessage((ServiceMessage) service);
  }
}
