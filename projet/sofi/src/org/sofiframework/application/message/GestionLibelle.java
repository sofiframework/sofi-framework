/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Objet permettant la gestion des libellés provenant de table d'une base de
 * données ou de fichier ressource.
 * 
 * La configuration se fait en spécifiant dans WEB.XML le nom du service des
 * messages ainsi que le nom de configuration
 * 
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @see org.sofiframework.presentation.struts.plugin.LibellePlugIn
 * @version SOFI 1.0
 */
public class GestionLibelle extends BaseGestionMessage {
  /**
   * Référence à l'instance unique de l'objet de gestion des libellés.
   */
  private static GestionLibelle gestionLibelleRef = new GestionLibelle();

  /**
   * L'instance de journalisation pour cette classe.
   */
  private static final Log log = LogFactory.getLog(GestionLibelle.class);

  /**
   * Collectiondes libellés
   */
  private HashMap<String, Object> listeLibelles = new HashMap<String, Object>();

  /**
   * Service distant qui est fourni les libellés à l'objet de gestion.
   */
  private ServiceLibelle serviceLibelle;

  private GestionLibelle() {
    super(org.sofiframework.application.message.service.ServiceLibelle.class);
  }

  /**
   * Obtenir l'instance unique de gestion des libellés.
   * 
   * @return instance unique de gestion des libellés
   */
  public static GestionLibelle getInstance() {
    return gestionLibelleRef;
  }

  @Override
  public synchronized Message get(String cleLibelle, String codeLocale, Object parametres, String codeClient) {
    if (cleLibelle == null) {
      Libelle libelle = new Libelle();
      libelle.setMessageFormat(new MessageFormat(""));
      return libelle;
    }

    // Sélectionner le locale disponible si la locale demandé n'est pas
    // disponible.
    Locale locale = getLocaleDisponible(codeLocale);
    if (locale == null) {
      locale = getLocaleParDefaut();
    }

    // À chacune des journées la cache des messages est vidé.
    if (!getDateDerniereMaj().equals(UtilitaireDate.getDateJourEn_AAAA_MM_JJ())) {
      setViderCache(true);
      setDateDerniereMaj(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
    }

    if (isViderCache()) {
      listeLibelles.clear();
      setViderCache(false);
      if (log.isInfoEnabled()) {
        log.info("Le cache des libellés est vidé!");
      }
    }

    String cleCache = construireCleCache(cleLibelle, locale, codeClient);
    Libelle libelle = (Libelle) listeLibelles.get(cleCache.toString());

    if (libelle == null) {
      MessageFormat message = null;

      if (cleLibelle != null) {
        libelle = getLibelle(cleLibelle, locale, codeClient);
      }

      if (libelle == null) {
        // Extraire avec la locale par défaut, s'il y a lieu.
        libelle = getLibelle(cleLibelle, getLocaleParDefaut(), codeClient);
      }

      try {

        if ((libelle != null) && (getParametre1() != null) && !getParametre1().equals("{0}")) {
          // Si le message n'est pas dans le format {0}, {1}, etc.. Il faut le
          // convertir dans ce format.
          message = new MessageFormat(convertirFormatMessage(libelle.getMessage()));
        }

        // Si le message m'existe pas, placé l'identifiant comme message.
        if (((libelle == null) || (libelle.getMessage() == null))) {
          libelle = new Libelle();
          message = new MessageFormat(UtilitaireString.traiterApostrophe(cleLibelle));
        } else {
          String messageOriginal = UtilitaireString.traiterApostrophe(libelle.getMessage());
          message = new MessageFormat(messageOriginal);
        }
        libelle.setMessageFormat(message);
        if (!UtilitaireString.isVide(getAideParametre()) && (libelle.getMessage() != null)
            && (libelle.getMessage().indexOf(getIndicateurAide(1)) != -1)) {
          String messageCompletAvecAide = genererAideContextuelleComplete(libelle.getMessage(), locale.toString(),
              codeClient);
          libelle.setMessageFormat(new MessageFormat(messageCompletAvecAide));
        }
      } catch (Exception e) {
        // TODO: handle exception
      }
      listeLibelles.put(cleCache, libelle);
    }

    if (libelle.getMessageFormat() != null) {
      // Affecter le libellé complet avec les paramètres dynamique s'il y a lieu
      libelle.setMessage(libelle.getMessageFormat().format(parametres));
    }
    Libelle libelleTmp = null;

    if (parametres != null) {
      libelleTmp = (Libelle) libelle.clone();
      // Affecter le libellé complet avec les paramètres dynamique s'il y a lieu
      libelleTmp.setMessage(libelle.getMessageFormat().format(parametres));
    } else {
      libelleTmp = libelle;
    }

    return libelleTmp;
  }

  /**
   * Permet de récupérer un libellé pour les paramètres spécifiés. Si le libellé
   * n'est pas trouvé pour le code client passé en paramètre, la méthode
   * recherche récursivement dans l'ascendance du client un libellé
   * correspondant.
   * 
   * @param cleLibelle
   *          la qclé du libellé
   * @param locale
   *          la locale
   * @param codeClient
   *          le code du client
   * @return un objet {@link Libelle}
   */
  private Libelle getLibelle(String cleLibelle, Locale locale, String codeClient) {
    Libelle libelle = serviceLibelle.get(cleLibelle, locale.toString(), codeClient);
    if (libelle == null) {
      List<Client> listeClientAscendant = GestionSecurite.getInstance().getListeClientAscendant(codeClient);
      for (Client ascendant : listeClientAscendant) {
        libelle = serviceLibelle.get(cleLibelle, locale.toString(), ascendant.getNoClient());
        if (libelle != null) {
          return libelle;
        }
      }
      if (libelle == null) {
        libelle = serviceLibelle.get(cleLibelle, locale.toString(), null);
      }
    }
    return libelle;
  }

  /**
   * Obtenir une description de message.
   * <p>
   * 
   * @param identifiant
   *          Identifiant du libellé désiré
   * @param application
   *          application qui est propriétaire du libellé
   * @param module
   *          module propriétaire du libellé
   * @param codeLocale
   *          code de langue du libellé
   * @return le libelle désiré
   * @deprecated depuis 3.2, remplacée par
   *             {@link GestionLibelle#get(String, String, Object, String)}.
   */
  @Override
  @Deprecated
  public Message getLibelle(String identifiant, String codeLocale, String codeSexe, Object parametres) {
    return get(identifiant, codeLocale, parametres, null);
  }

  /**
   * Supprimer une clé de libellé du gestionnaire des libellés.
   * 
   * @param cleLibelle
   *          la clé du libellé à supprimer.
   */
  public void supprimer(String cleLibelle) {
    Iterator<String> iterateur = getListeLocale().iterator();

    while (iterateur.hasNext()) {
      String locale = iterateur.next();
      StringBuffer cleComplete = new StringBuffer();
      cleComplete.append(cleLibelle);
      cleComplete.append(".");
      cleComplete.append(locale);
      this.listeLibelles.remove(cleComplete.toString());
    }
  }

  /**
   * Supprimer une clé de libellé du gestionnaire des libellés.
   * 
   * @param cleLibelle
   *          la clé du libellé à supprimer.
   */
  public void supprimer(String cleLibelle, String codeClient) {
    Iterator<String> iterateur = getListeLocale().iterator();

    while (iterateur.hasNext()) {
      String locale = iterateur.next();
      StringBuffer cleComplete = new StringBuffer();
      cleComplete.append(cleLibelle);
      cleComplete.append(".");
      cleComplete.append(codeClient);
      cleComplete.append(".");
      cleComplete.append(locale);
      this.listeLibelles.remove(cleComplete.toString());
    }
  }

  /**
   * Initialiser la cache gestionnaire des libellés.
   */
  public void initialiserCache() {
    listeLibelles.clear();
  }

  public ServiceLibelle getServiceLibelle() {
    return serviceLibelle;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceLibelle((ServiceLibelle) service);
  }
}
