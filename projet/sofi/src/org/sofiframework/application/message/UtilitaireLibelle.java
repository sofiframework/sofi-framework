/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Classe utilitaire permettant d'extraire un libelle provenant du gestionnaire des libéllés (GestionLibelle).
 * <p>
 * 
 * @author Jean-François Brasard (Nurun inc)
 * @version 1.0
 * @see org.sofiframework.application.message.GestionLibelle
 */
public class UtilitaireLibelle {
  /**
   * Retourne le libellé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param request
   *          la requête en traitement
   * @param parametres
   *          les parametres dynamique au libellé s'il y a lieu, sinon mettre null
   * @return le libellé basé sur la langue de l'utilisateur.
   */
  public static Libelle getLibelle(String identifiant, HttpServletRequest request, Object[] parametres) {
    Libelle libelle = getLibellePourUtilisateur(identifiant, parametres, request.getSession(), null);

    return libelle;
  }

  /**
   * Retourne le libellé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param contexte
   *          la page en traitement
   * @param parametres
   *          les parametres dynamique au libellé s'il y a lieu, sinon mettre null
   * @return le libellé basé sur la langue de l'utilisateur.
   */
  public static Libelle getLibelle(String identifiant, PageContext contexte, Object[] parametres) {
    Libelle libelle = getLibellePourUtilisateur(identifiant, parametres, contexte.getSession(), null);

    return libelle;
  }

  /**
   * Retourne le libellé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param contexte
   *          la page en traitement
   * @param parametres
   *          les parametres dynamique au libellé s'il y a lieu, sinon mettre null
   * @return le libellé basé sur la langue de l'utilisateur.
   * @since 3.2
   */
  public static Libelle getLibelle(String identifiant, PageContext contexte, Object[] parametres, String codeClient) {
    Libelle libelle = getLibellePourUtilisateur(identifiant, parametres, contexte.getSession(), codeClient);

    return libelle;
  }

  /**
   * Retourne le libéllé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param contexte
   *          la page en traitement
   * @return le libellé basé sur la langue de l'utilisateur.
   */
  public static Libelle getLibelle(String identifiant, PageContext contexte) {
    Libelle libelle = getLibellePourUtilisateur(identifiant, null, contexte.getSession(), null);

    return libelle;
  }

  /**
   * Méthode privé qui retourne un libellé pour la langue de l'utilisateur s'il y a lieu
   * 
   * @param identifiant
   *          la clé du libellé
   * @param parametres
   *          la valeur des paramètres inclus dans le libellé.
   * @param session
   *          la session http pour obtenir la langue de l'utilisateur (voir
   *          {@link UtilitaireControleur#getLanguePourUtilisateur(HttpSession)}).
   * @param codeClient
   *          le code du client.
   * @return
   */
  private static Libelle getLibellePourUtilisateur(String identifiant, Object[] parametres, HttpSession session,
      String codeClient) {
    String langueUtilisateur = UtilitaireControleur.getLanguePourUtilisateur(session);

    // Extraire le libelle du gestion de libellé.
    Libelle libelle = (Libelle) GestionLibelle.getInstance().get(identifiant, langueUtilisateur, parametres,
        codeClient);

    return libelle;
  }

  /**
   * Retourne le libéllé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param utilisateur
   *          l'utilisateur en traitement
   * @param parametres
   *          les parametres dynamique au libellé s'il y a lieu, sinon mettre null
   * @return le libellé basé sur la langue de l'utilisateur.
   */
  public static Libelle getLibelle(String identifiant, Utilisateur utilisateur, Object[] parametres) {
    return getLibelle(identifiant, utilisateur, parametres, null);
  }

  /**
   * Retourne le libéllé basé sur la langue de l'utilisateur.
   * 
   * @param identifiant
   *          l'identifiant du libellé
   * @param utilisateur
   *          l'utilisateur en traitement
   * @param parametres
   *          les parametres dynamique au libellé s'il y a lieu, sinon mettre null
   * @param codeClient
   *          le code du client
   * @return le libellé basé sur la langue de l'utilisateur.
   * @since 3.2
   */
  public static Libelle getLibelle(String identifiant, Utilisateur utilisateur, Object[] parametres, String codeClient) {
    String langueUtilisateur = utilisateur.getCodeLocale();

    Libelle libelle = (Libelle) GestionLibelle.getInstance().get(identifiant, langueUtilisateur, parametres,
        codeClient);

    return libelle;
  }
}
