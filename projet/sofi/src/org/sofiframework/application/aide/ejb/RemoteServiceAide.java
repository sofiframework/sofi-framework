/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.aide.ejb;

import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.modele.ejb.exception.ServiceException;


/**
 * Interface à distance permettant d'accéder à un service distant permettant
 * la gestion des messages.
 * <p>
 * Le distance implémenté doit donc être basé sur cette interface.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.6
 */
public abstract interface RemoteServiceAide extends javax.ejb.EJBObject {

  /**
   * Retourne une aide pour un objet java et une langue
   * @throws org.sofiframework.modele.ejb.exception.ServiceException
   * @return le détail de l'aide.
   * @param codeLangue le code de langue
   * @param seqObjetReferentiel l'objet du référentiel
   */
  public Aide getAide(Long seqObjetReferentiel,
      String codeLangue) throws ServiceException,
      java.rmi.RemoteException;


  /**
   * Retourne une aide selon sa clé et la langue désirée.
   * @param cleAide la clé de l'aide
   * @param codeLangue le code de langue de l'aide.
   * @return le détail de l'aide.
   */
  Aide getAideSelonCle(String cleAide, String codeLangue) throws ServiceException,
  java.rmi.RemoteException;
}
