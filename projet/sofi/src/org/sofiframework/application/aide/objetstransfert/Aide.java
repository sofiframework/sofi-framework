/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.aide.objetstransfert;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Objet de transfert permettant de spécifier une aide en ligne.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.6
 * @since SOFI 2.0.3 Il est possible d'extraire la référence associé (ex. un code de message normalisé)
 */

public class Aide extends ObjetTransfert {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -6574319814603587866L;

  private static final String[] CLE = { "cleAide", "codeLangue" };
  private String cleAide;
  private String codeLangue;
  private String codeApplication;
  private String texteAide;
  private String titre;
  private String aideExterne;

  // La référence associé.
  private String reference;

  public Aide() {
    this.setCle(CLE);
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getTexteAide() {
    return texteAide;
  }

  public void setTexteAide(String texteAide) {
    this.texteAide = texteAide;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public void setAideExterne(String aideExterne) {
    this.aideExterne = aideExterne;
  }

  public String getAideExterne() {
    return aideExterne;
  }

  /**
   * Fixer la référence associé.
   * @param reference la référence associé.
   */
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Retourne la référence associé.
   * @return la référence associé.
   */
  public String getReference() {
    return reference;
  }
}
