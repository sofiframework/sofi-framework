/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.aide;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Onglet;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.base.ContenuDynamiqueTag;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet permettant la gestion de l'aide en ligne pour un composant d'interface.
 * <p>
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * @author Jean-Franïcois Brassard (Nurun inc.)
 * @version SOFI 1.6
 */
public class GestionAide extends org.sofiframework.application.base.BaseGestionService {
  /**
   * Identifiant unique pour fixer l'aide contextuelle dans le gestionnaire de cache.
   */
  private static final String IDENTIFIANT_CACHE = "aideEnLigne";

  /**
   * Référence à l'instance de gestion de l'aide
   */
  private static GestionAide gestionAideRef = new GestionAide();

  /**
   * L'instance de l'aide en ligne pour cette classe
   */
  private static final Log log = LogFactory.getLog(GestionAide.class);

  /**
   * L'url qui va spécifier d'appeler l'aide en ligne.
   */
  private String urlAide;

  /**
   * Le gabarit de l'aide en ligne.
   */
  private String gabaritAide;

  /**
   * Les infos du popup d'aide en ligne de l'application
   */
  private HashMap infoPopupAide = new HashMap();

  /**
   * Le service associé au gestionnaire d'aide.
   */
  private ServiceAide serviceAide;

  /**
   * Spécifie que l'aide en ligne est en Ajax.
   */
  private boolean ajax;

  public GestionAide() {
    super(ServiceAide.class);
  }

  /**
   * Obtenir l'instance unique de gestion de l'aide en ligne.
   * @return l'instance unique de gestion de l'aide en ligne.
   */
  public static GestionAide getInstance() {
    return gestionAideRef;
  }

  /**
   * Permet d'extraire l'aide en ligne pour un composant d'interface.
   * @throws java.lang.Exception
   * @return L'aide en ligne.
   * @param locale le l'utilisateur de l'utilisateur.
   * @param objetSecurisable l'objet java.
   * @throws java.lang.Exception une exception à l'accès au service.
   */
  public Aide getAide(ObjetSecurisable objetSecurisable,
      Locale locale) throws Exception {
    // Extraire l'aide en ligne pour l'objet java demandé s'il y a lieu.
    Aide aide =
        (Aide)GestionCache.getInstance().getCache(IDENTIFIANT_CACHE).get(objetSecurisable.getSeqObjetSecurisable());

    if (aide == null) {
      // Le composant d'interface demandé
      Long seqObjetJava = objetSecurisable.getSeqObjetSecurisable();

      try {
        // Extraire l'aide en ligne via le service EJB.
        String langueUtilisateur = locale.getLanguage().toUpperCase();

        try {
          aide =
              serviceAide.getAide(objetSecurisable.getSeqObjetSecurisable(), langueUtilisateur);
        } catch (Exception e) {
        }

        if ((aide == null) && objetSecurisable instanceof Onglet) {
          // Si l'aide n'existe pas pour l'onglet vérifier s'il en existe pour le service.
          while (!(objetSecurisable instanceof Service)) {
            objetSecurisable = objetSecurisable.getObjetSecurisableParent();
          }

          try {
            aide =
                serviceAide.getAide(objetSecurisable.getSeqObjetSecurisable(),
                    langueUtilisateur);
          } catch (Exception e) {
          }
        }

        //Si il n'y a pas d'aide d'associé au service et que l'aide globale est activé,
        //on va chercher l'aide de la page d'accueil

        boolean ignorerObjetEnfant =
            GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_IGNORER_AIDE_ENFANT).booleanValue();


        if ((aide == null) && objetSecurisable instanceof Service &&
            GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_APPLICATION_GLOBALE).booleanValue()) {
          try {
            aide =
                serviceAide.getAide(GestionSecurite.getInstance().getComposantMenuAccueil().getSeqObjetSecurisable(),
                    langueUtilisateur);
          } catch (Exception e) {
          }
        } else if (!ignorerObjetEnfant && (aide == null) &&
            objetSecurisable instanceof Service &&
            (objetSecurisable.getListeObjetSecurisableEnfants() !=
            null) &&
            (objetSecurisable.getListeObjetSecurisableEnfants().size() !=
            0) &&
            (objetSecurisable.getListeObjetSecurisableEnfants().get(0) !=
            null)) {
          // S'il n'existe pas d'aide pour le service demandé mais il en existe pour l'onglet associé, alors retourner
          // l'aide de cet onglet.
          ObjetSecurisable ongletEnfant =
              (ObjetSecurisable)objetSecurisable.getListeObjetSecurisableEnfants().get(0);

          try {
            aide =
                serviceAide.getAide(ongletEnfant.getSeqObjetSecurisable(), langueUtilisateur);
          } catch (Exception e) {
          }
        }

        // Fixer l'aide en ligne en cache.
        GestionCache.getInstance().getCache(IDENTIFIANT_CACHE).put(seqObjetJava,
            aide);
      } catch (Exception e) {
        log.error(e);
      }
    }

    return aide;
  }

  /**
   * Générer la réponse de l'aide en ligne demandé.
   * @param request la requête de l'utilisateur.
   * @return l'utilitaire velocity avec les paramètres nécessaire au gabarit.
   */
  public synchronized UtilitaireVelocity preparerAide(HttpServletRequest request) {
    // Extraire l'objet java dont l'aide est demandé.
    String seqObjetJava = request.getParameter("sofi_seq_objet_java_aide");

    // Extraire le locale de l'utilisateur.
    Locale locale = UtilitaireSession.getLocale(request.getSession());

    // Extraire l'objet Java demandé pour avoir de l'aide.
    ObjetSecurisable objetJava =
        GestionSecurite.getInstance().getObjetSecurisableParCle(new Long(seqObjetJava));
    Aide aide = null;

    if (objetJava != null) {
      // Si un objet java existe pour l'aide demandé, extraire l'aide en ligne correspondant.
      try {
        aide = GestionAide.getInstance().getAide(objetJava, locale);

        // Vérifier s'il y a de l'aide externe, si oui extraire son contenu.
        if (aide.getAideExterne() != null) {
          // Extraire le contenu dynamique basé sur un url.
          String contenuAide =
              getContenuAideExterne(aide.getAideExterne(), locale);

          aide.setTexteAide(contenuAide);
        }

        // Construire le contexte Velocity.
        UtilitaireVelocity velocity = new UtilitaireVelocity();

        velocity.ajouterAuContexte("aide", aide);

        return velocity;
      } catch (Exception e) {
        throw new SOFIException(e);
      }
    }

    return null;
  }

  /**
   * Écrire la réponse de l'aide en ligne demandé par l'utilisateur.
   * @param response la réponse à l'utilisateur.
   * @param utilitaireVelocity l'utilitaire sur Velocity.
   */
  public void ecrireReponseAide(UtilitaireVelocity utilitaireVelocity,

      HttpServletResponse response) {

    String resultat = utilitaireVelocity.genererHTML(getGabaritAide());
    Aide aide = (Aide)utilitaireVelocity.getContexte().get("aide");

    if (aide != null && aide.getAideExterne() != null) {
      String baseUrl =
          GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_BASE_URL);

      resultat =
          UtilitaireString.remplacerTous(resultat, "\"../", "\"" + baseUrl +
              "/");
    }

    try {
      response.setHeader("Pragma", "No-cache");
      response.setHeader("Cache-Control", "no-cache,no-store,max-age=0");
      response.setContentType("text/html; charset=UTF-8");
      response.setDateHeader("Expires", 1);

      PrintWriter pw = response.getWriter();
      pw.write(resultat);
      pw.close();
    } catch (IOException e) {
    }
  }

  /**
   * Est-ce que le service d'aide en ligne est configuré.
   * @return true si le service d'aide en ligne est configuré.
   */
  public boolean isServiceAideConfigure() {
    return serviceAide != null;
  }

  /**
   * Fixer l'url qui spécifie d'appeler l'aide en ligne.
   * @param urlAide l'url qui spécifie d'appeler l'aide en ligne.
   */
  public void setUrlAide(String urlAide) {
    this.urlAide = urlAide;
  }

  /**
   * Retourne l'url qui spécifie d'appeler l'aide en ligne.
   * @return l'url qui spécifie d'appeler l'aide en ligne.
   */
  public String getUrlAide() {
    return urlAide;
  }

  /**
   * Fixer le gabarit d'aide en ligne.
   * @param gabaritAide le gabarit d'aide en ligne.
   */
  public void setGabaritAide(String gabaritAide) {
    this.gabaritAide = gabaritAide;
  }

  /**
   * Retourne le gabarit d'aide en ligne.
   * @return le gabarit d'aide en ligne.
   */
  public String getGabaritAide() {
    return gabaritAide;
  }

  /**
   * Retourne le contenu d'une page d'aide externe.
   * @param page la page a extraire.
   * @param locale le locale de l'utilisateur.
   * @return le contenu de l'aide externe.
   */
  public String getContenuAideExterne(String page, Locale locale) {
    String baseUrl =
        GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_BASE_URL);
    String extensionPage =
        GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_PAGE_EXTENSION);
    Boolean multilingue =
        GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.AIDE_CONTEXTUELLE_MULTILINGUE);

    if (baseUrl.equals("") || extensionPage.equals("")) {
      throw new SOFIException("Vous devez spécifier les paramétres systémes suivants: aideContextuelleBaseUrl et aideContextuellePageExtension afin d'utiliser de l'aide basé sur un url.");
    }

    String structureUrlExterneAide =
        GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.AIDE_STRUCTURE__EXTERNE);

    if (UtilitaireString.isVide(structureUrlExterneAide)) {
      throw new SOFIException("Vous devez spécifier une valeur pour le paramétre systéme 'aideStructureUrlExterne'.");
    }

    // Extraire le contenu dynamique basé sur un url.
    String contenuAide =
        ContenuDynamiqueTag.getContenuDynamique(baseUrl, null, page, extensionPage,
            multilingue.booleanValue(),
            false, locale,
            structureUrlExterneAide);

    return contenuAide;
  }


  /**
   * Configure les propriétés du popup d'aide.
   * @param barreAdresse
   * @param barreMenu
   * @param barreOutil
   * @param scrollbars
   * @param hauteurFenetre
   * @param largeurFenetre
   * @param nomFenetre
   * @param adresseSurMemeDomaine
   * @param scope
   * @param property
   * @param name
   * @param paramScope
   * @param paramProperty
   * @param paramName
   * @param paramId
   * @param maximiser
   */
  public void setInfoPopupAide(boolean maximiser, String paramId,
      String paramName, String paramProperty,
      String paramScope, String name, String property,
      String scope, boolean adresseSurMemeDomaine,
      String nomFenetre, String largeurFenetre,
      String hauteurFenetre, String scrollbars,
      boolean barreOutil, boolean barreMenu,
      boolean barreAdresse) {
    infoPopupAide.put("maximiser", new Boolean(maximiser));
    infoPopupAide.put("paramId", paramId);
    infoPopupAide.put("paramName", paramName);
    infoPopupAide.put("paramProperty", paramProperty);
    infoPopupAide.put("paramScope", paramScope);
    infoPopupAide.put("property", property);
    infoPopupAide.put("adresseSurMemeDomaine",
        new Boolean(adresseSurMemeDomaine));
    infoPopupAide.put("nomFenetre", nomFenetre);
    infoPopupAide.put("largeurFenetre", largeurFenetre);
    infoPopupAide.put("hauteurFenetre", hauteurFenetre);
    infoPopupAide.put("scrollbars", scrollbars);
    infoPopupAide.put("barreOutil", new Boolean(barreOutil));
    infoPopupAide.put("barreMenu", new Boolean(barreMenu));
    infoPopupAide.put("barreAdresse", new Boolean(barreAdresse));
  }

  /**
   * Retourne les propriétés du popup d'aide
   * @return les propriétés du popup d'aide
   */
  public HashMap getInfoPopupAide() {
    return infoPopupAide;
  }

  public void setServiceAide(ServiceAide service) {
    this.serviceAide = service;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceAide((ServiceAide)service);
  }

  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  public boolean isAjax() {
    return ajax;
  }
}
