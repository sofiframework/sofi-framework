package org.sofiframework.application.locale.cache;

import java.util.List;

import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.application.locale.service.ServiceLocalisation;
import org.sofiframework.modele.distant.GestionServiceDistant;


public class ListeRegionEnfant extends ObjetCache {

  private static final long serialVersionUID = -8305222661195422982L;
  
  @Override
  public void chargerDonnees(Object sousCache) {
    ServiceLocalisation serviceLocalisation = (ServiceLocalisation) GestionServiceDistant.getServiceDistant("serviceLocalisation");
    String[] parameters = ((String[]) sousCache);
    List<Region> listeRegion = serviceLocalisation.getListeRegionEnfant(parameters[0], parameters[1]);
    for (Region region : listeRegion) {
      this.put(region.getCode(), region);
    }
    log.info("Fin de chargement de la liste des régions enfant");
  }

}
