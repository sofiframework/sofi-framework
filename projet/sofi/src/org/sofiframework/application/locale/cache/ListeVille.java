package org.sofiframework.application.locale.cache;

import java.util.List;

import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.locale.objetstransfert.Ville;
import org.sofiframework.application.locale.service.ServiceLocalisation;

public class ListeVille extends ObjetCache {

  private static final long serialVersionUID = -5776966607429094577L;

  @Override
  public void chargerDonnees( Object sousCache ) {
    ServiceLocalisation serviceLocalisation = (ServiceLocalisation) this.getServiceDistant("serviceLocalisation");
    List<Ville> listeVille = serviceLocalisation.getListeVille((String) sousCache);
    for  (Ville ville : listeVille){
      this.put(ville.getCodeVille(), ville);
    }
    log.info("Fin de chargement de la liste des villes ");
  }

  
}
