/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.locale.cache;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.application.locale.service.ServiceLocalisation;

public class ListePays extends ObjetCache {

  private static final long serialVersionUID = 2239552737155128550L;
  private static final Log log = LogFactory.getLog(ListePays.class);

  @Override
  public void chargerDonnees() {
    log.info("Début de chargement de la liste des pays ");
    ServiceLocalisation serviceLocalisation = (ServiceLocalisation) this.getServiceDistant("serviceLocalisation");
    List<Pays> listePays = serviceLocalisation.getListePays();
    for (Pays pays : listePays) {
      this.put(pays.getCode(), pays);
    }
    log.info("Fin de chargement de la liste des pays ");
  }
}
