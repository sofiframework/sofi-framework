/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.locale.cache;

import java.util.List;

import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.application.locale.service.ServiceLocalisation;
import org.sofiframework.modele.distant.GestionServiceDistant;

public class ListeRegion extends ObjetCache {

  private static final long serialVersionUID = -2136308830424121290L;

  @Override
  public void chargerDonnees(Object sousCache) {
    ServiceLocalisation serviceLocalisation = (ServiceLocalisation) GestionServiceDistant.getServiceDistant("serviceLocalisation");
    List<Region> listeRegion = serviceLocalisation.getListeRegion((String) sousCache);
    for (Region region : listeRegion) {
      this.put(region.getCode(), region);
    }
  }
}
