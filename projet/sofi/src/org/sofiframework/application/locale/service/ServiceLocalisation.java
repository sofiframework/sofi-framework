package org.sofiframework.application.locale.service;

import java.util.List;
import java.util.Locale;

import org.sofiframework.application.locale.objetstransfert.FuseauHoraire;
import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.application.locale.objetstransfert.Ville;
import org.sofiframework.application.locale.objetstransfert.VilleRegion;

public interface ServiceLocalisation {

  List<Pays> getListePays();
  
  Region getRegion(String codeRegion);

  List<Region> getListeRegion(String codePays);
  
  List<Region> getListeRegionEnfant(String codePays, String codeParent);
  
  Ville getVille(Long id);
  
  VilleRegion getVilleRegion(Long idVille);
  
  List<Ville> getListeVille(String codeRegionParent);
  
  List<Ville> getListeVille(String filtre, String locale, Long nbResult);
  
  List<Ville> getListeVilleParRegion(String codeRegion);

  List<FuseauHoraire> getListeFuseauHoraire();

  List<FuseauHoraire> getListeFuseauHoraire(String codePays, Locale locale);

  void chargerPaysEtRegions();
  
  public void modifierVille(Ville ville);
}
