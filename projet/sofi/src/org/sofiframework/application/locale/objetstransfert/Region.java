/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.locale.objetstransfert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.modele.entite.EntiteDescriptionLocale;

/**
 * Région. En fonction du pays peu-être considéré comme une province, un état ou
 * autre subdivision géographique.
 * 
 * @author Jean-Maxime Pelletier
 */
public class Region extends EntiteDescriptionLocale {

  private static final long serialVersionUID = 7411730486205315945L;

  private String code;

  private String codePays;

  private Integer ordre;
  
  private String codeParent;
  
  private List<RegionLocale> listeRegionLocale;

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }

  public String getCodePays() {
    return codePays;
  }

  public void setOrdre(Integer ordre) {
    this.ordre = ordre;
  }

  public Integer getOrdre() {
    return ordre;
  }
  
  public String getCodeParent() {
    return codeParent;
  }

  public void setCodeParent(String codeParent) {
    this.codeParent = codeParent;
  }

  /**
   * @return the listeRegionLocale
   */
  public List<RegionLocale> getListeRegionLocale() {
    return listeRegionLocale;
  }

  /**
   * @param listeRegionLocale the listeRegionLocale to set
   */
  public void setListeRegionLocale(List<RegionLocale> listeRegionLocale) {
    this.listeRegionLocale = listeRegionLocale;
    
    if (listeRegionLocale != null) {
      for (int i = 0; i < listeRegionLocale.size(); i++) {
        RegionLocale regionLocaleTmp = listeRegionLocale.get(i);

        // Set description
        getDescriptionLocale().put(regionLocaleTmp.getCode(),
            regionLocaleTmp.getDescription());
      }
    }
  }
  
  /**
   * Préparer la liste des descriptions par locale pour mise à jour.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void miseAJourListeDescriptionLocale() {

    HashSet<String> presentLocale = new HashSet<String>();

    if (getListeRegionLocale() != null) {

      for (int i = 0; i < getListeRegionLocale().size(); i++) {
        RegionLocale regionLocale = getListeRegionLocale().get(i);
        String valeur = getDescriptionLocale()
            .get(regionLocale.getCode());

        if (StringUtils.isBlank(valeur)) {
          getListeRegionLocale().remove(i);
        } else {
          regionLocale.setDescription(valeur);
        }
        presentLocale.add(regionLocale.getCode());
      }
    } else {
      setListeRegionLocale(new ArrayList());
    }

    Iterator iter = getDescriptionLocale().keySet().iterator();
    while (iter.hasNext()) {
      String code = (String) iter.next();
      if (!presentLocale.contains(code) || presentLocale.size() == 0) {
        RegionLocale regionLocale = new RegionLocale();
        regionLocale.setCode(code);
        regionLocale.setDescription(getDescriptionLocale().get(code));
        regionLocale.setCleEtrangere(this.getCode());
        getListeRegionLocale().add(regionLocale);
      }
    }
  }
}
