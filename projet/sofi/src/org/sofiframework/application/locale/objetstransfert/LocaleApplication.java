package org.sofiframework.application.locale.objetstransfert;


/**
 * Objet de transfert permettant de spécifier une locale disponible
 * pour une application.
 * @author jfbrassard
 * @since 3.0.2
 */

public class LocaleApplication extends org.sofiframework.application.locale.objetstransfert.Locale {

  private static final long serialVersionUID = -4263751731885979422L;

  private String codeApplication;
  private String codeLocale;

  private String cleLibelle;

  private Integer ordreAffichage;
  private boolean defaut;

  private org.sofiframework.application.locale.objetstransfert.Locale locale;

  /**
   * @param codeApplication the codeApplication to set
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }
  /**
   * @return the codeApplication
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * @param codeLocale the codeLocale to set
   */
  @Override
  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
  }
  /**
   * @return the codeLocale
   */
  @Override
  public String getCodeLocale() {
    return codeLocale;
  }

  /**
   * @param cleLibelle the cleLibelle to set
   */
  @Override
  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }
  /**
   * @return the cleLibelle
   */
  @Override
  public String getCleLibelle() {
    return cleLibelle;
  }

  /**
   * @param ordreAffichage the ordreAffichage to set
   */
  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }
  /**
   * @return the ordreAffichage
   */
  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }
  /**
   * @param defaut the defaut to set
   */
  public void setDefaut(boolean defaut) {
    this.defaut = defaut;
  }
  /**
   * @return the defaut
   */
  public boolean isDefaut() {
    return defaut;
  }

  /**
   * @param locale the locale to set
   */
  public void setLocale(org.sofiframework.application.locale.objetstransfert.Locale locale) {
    this.locale = locale;
  }
  /**
   * @return the locale
   */
  public org.sofiframework.application.locale.objetstransfert.Locale getLocale() {
    return locale;
  }

}
