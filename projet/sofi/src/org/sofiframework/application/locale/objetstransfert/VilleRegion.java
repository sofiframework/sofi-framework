package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.objetstransfert.Entite;

public class VilleRegion extends Entite {
  private static final long serialVersionUID = 875844071819918248L;
  
  private Ville ville;
  private Region region;
  
  public Ville getVille() {
    return ville;
  }
  public void setVille(Ville ville) {
    this.ville = ville;
  }
  public Region getRegion() {
    return region;
  }
  public void setRegion(Region region) {
    this.region = region;
  }
  
  public String getDescriptionVilleRegion(String locale) {
    StringBuffer sb = new StringBuffer();
    sb.append(this.ville != null ? this.ville.getDescriptionLocale()
        .get(locale) : "");
    sb.append(" (");
    sb.append(this.region != null ? this.region.getDescriptionLocale().get(
        locale) : "");
    sb.append(")");
    return sb.toString();
  }

}
