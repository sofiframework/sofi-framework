package org.sofiframework.application.locale.objetstransfert;

import java.util.List;

import org.sofiframework.modele.entite.EntiteDescriptionLocale;

public class Ville extends EntiteDescriptionLocale {

  private static final long serialVersionUID = -568167596094286001L;
  
  private Long id;
  
  private String codeVille;
  
  private Integer ordre;
  
  private String codeRegion;
  
  private Double latitude;
  
  private Double longitude;
  
  private List<VilleLocale> listeVilleLocale;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getCodeVille() {
    return codeVille;
  }
  
  public void setCodeVille(String codeVille) {
    this.codeVille = codeVille;
  }
  
  public Integer getOrdre() {
    return ordre;
  }
  
  public void setOrdre(Integer ordre) {
    this.ordre = ordre;
  }
  
  public String getCodeRegion() {
    return codeRegion;
  }
  
  public void setCodeRegion(String codeRegion) {
    this.codeRegion = codeRegion;
  }

  public List<VilleLocale> getListeVilleLocale() {
    return listeVilleLocale;
  }

  public void setListeVilleLocale(List<VilleLocale> listeVilleLocale) {
    this.listeVilleLocale = listeVilleLocale;
    if (listeVilleLocale != null){
      for (int i = 0;  i < listeVilleLocale.size(); i++){
        VilleLocale villeLocalTmp = listeVilleLocale.get(i);
        getDescriptionLocale().put(villeLocalTmp.getCode(),
            villeLocalTmp.getDescription());
      }
    }
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }
  
  
  

}
