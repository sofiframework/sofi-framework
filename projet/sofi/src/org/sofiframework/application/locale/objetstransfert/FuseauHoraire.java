package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.modele.entite.Entite;

public class FuseauHoraire extends Entite {

  private static final long serialVersionUID = -315766713459646511L;

  private String code;

  private String nom;

  public FuseauHoraire(String code, String nom) {
    super();
    this.code = code;
    this.nom = nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}
