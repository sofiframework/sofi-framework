/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.locale.objetstransfert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.modele.entite.EntiteDescriptionLocale;

/**
 * Pays. Les codes sont basés sur les codes officiels ISO 3166.
 * 
 * voir : http://www.iso.org/iso/country_codes/
 * 
 * @author Jean-Maxime Pelletier
 */
public class Pays extends EntiteDescriptionLocale {

  private static final long serialVersionUID = 4713872808041342992L;

  private String code;

  private Integer ordre;
  
  private List<PaysLocale> listePaysLocale;

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setOrdre(Integer ordre) {
    this.ordre = ordre;
  }

  public Integer getOrdre() {
    return ordre;
  }

  /**
   * @return the listePaysLocale
   */
  public List<PaysLocale> getListePaysLocale() {
    return listePaysLocale;
  }

  /**
   * @param listePaysLocale the listePaysLocale to set
   */
  public void setListePaysLocale(List<PaysLocale> listePaysLocale) {
    this.listePaysLocale = listePaysLocale;
    
    if (listePaysLocale != null) {
      for (int i = 0; i < listePaysLocale.size(); i++) {
        PaysLocale paysLocaleTmp = listePaysLocale.get(i);

        // Set description
        getDescriptionLocale().put(paysLocaleTmp.getCode(),
            paysLocaleTmp.getDescription());
      }
    }
  }
  
  /**
   * Préparer la liste des descriptions par locale pour mise à jour.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void miseAJourListeDescriptionLocale() {

    HashSet<String> presentLocale = new HashSet<String>();

    if (getListePaysLocale() != null) {

      for (int i = 0; i < getListePaysLocale().size(); i++) {
        PaysLocale paysLocale = getListePaysLocale().get(i);
        String valeur = getDescriptionLocale()
            .get(paysLocale.getCode());

        if (StringUtils.isBlank(valeur)) {
          getListePaysLocale().remove(i);
        } else {
          paysLocale.setDescription(valeur);
        }
        presentLocale.add(paysLocale.getCode());
      }
    } else {
      setListePaysLocale(new ArrayList());
    }

    Iterator iter = getDescriptionLocale().keySet().iterator();
    while (iter.hasNext()) {
      String code = (String) iter.next();
      if (!presentLocale.contains(code) || presentLocale.size() == 0) {
        PaysLocale paysLocale = new PaysLocale();
        paysLocale.setCode(code);
        paysLocale.setDescription(getDescriptionLocale().get(code));
        paysLocale.setCleEtrangere(this.getCode());
        getListePaysLocale().add(paysLocale);
      }
    }
  }
}
