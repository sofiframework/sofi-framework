package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.modele.entite.DescriptionLocale;

/**
 * VilleLocale. Classe pour identifier la description d'une ville selon la locale.
 * 
 */
public class VilleLocale extends DescriptionLocale {

  private static final long serialVersionUID = 5676024096870793931L;
  
  private Long villeId;


  /**
   * @return the villeId
   */
  public Long getVilleId() {
    return villeId;
  }

  /**
   * @param villeId the villeId to set
   */
  public void setVilleId(Long villeId) {
    this.villeId = villeId;
  }
  

}
