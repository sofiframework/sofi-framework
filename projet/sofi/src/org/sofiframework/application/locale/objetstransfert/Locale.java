package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Objet de transfert permettant de spécifier une locale.
 * @author jfbrassard
 * @since 3.0.2
 */

public class Locale extends ObjetTransfert {

  private static final long serialVersionUID = 9092197281250881273L;

  private String codeLocale;

  private String cleLibelle;

  private String codeLangue;

  private String codePays;

  /**
   * @param codeLocale the codeLocale to set
   */
  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
  }
  /**
   * @return the codeLocale
   */
  public String getCodeLocale() {
    return codeLocale;
  }
  /**
   * @param cleLibelle the cleLibelle to set
   */
  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }
  /**
   * @return the cleLibelle
   */
  public String getCleLibelle() {
    return cleLibelle;
  }
  /**
   * @param codeLangue the codeLangue to set
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }
  /**
   * @return the codeLangue
   */
  public String getCodeLangue() {
    return codeLangue;
  }
  /**
   * @param codePays the codePays to set
   */
  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }
  /**
   * @return the codePays
   */
  public String getCodePays() {
    return codePays;
  }


}
