package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.modele.entite.DescriptionLocale;

/**
 * PaysLocale. Classe pour identifier la description d'une région selon la locale.
 * 
 * @author Hacene Zidani
 */
public class RegionLocale extends DescriptionLocale {

  private static final long serialVersionUID = -395104614951225410L;
  
  private String codeRegion;

  /**
   * @return the codeRegion
   */
  public String getCodeRegion() {
    return codeRegion;
  }

  /**
   * @param codeRegion the codeRegion to set
   */
  public void setCodeRegion(String codeRegion) {
    this.codeRegion = codeRegion;
  }

}
