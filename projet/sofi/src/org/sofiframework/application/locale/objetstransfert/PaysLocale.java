package org.sofiframework.application.locale.objetstransfert;

import org.sofiframework.modele.entite.DescriptionLocale;

/**
 * PaysLocale. Classe pour identifier la description d'un pays selon la locale.
 * 
 * @author Hacene Zidani
 */
public class PaysLocale extends DescriptionLocale {

  private static final long serialVersionUID = 2095065090744669723L;
  
  private String codePays;

  /**
   * @return the codePays
   */
  public String getCodePays() {
    return codePays;
  }

  /**
   * @param codePays the codePays to set
   */
  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }

}
