/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.notification.objetstransferts;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class Notification extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = 6350103817705438302L;
  private Long noNotification;
  private String codeRole;
  private String codeApplication;
  private String codeUtilisateur;
  private String message;
  private String createur;
  private Date dateCreation;
  private Date dateDebut;
  private Date dateFin;

  private Date dateLecture;

  public Notification() {
  }

  public void setNoNotification(Long noNotification) {
    this.noNotification = noNotification;
  }

  public Long getNoNotification() {
    return noNotification;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  public String getCodeRole() {
    return codeRole;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setCreateur(String createur) {
    this.createur = createur;
  }

  public String getCreateur() {
    return createur;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  public Date getDateDebut() {
    return dateDebut;
  }

  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  public Date getDateFin() {
    return dateFin;
  }

  public void setDateLecture(Date dateLecture) {
    this.dateLecture = dateLecture;
  }

  public Date getDateLecture() {
    return dateLecture;
  }
}
