/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.notification.service;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.application.notification.objetstransfert.Notification;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Service de notification (message) pour les utilisateurs des différentes applications.
 * <p>
 * @author Mathieu Blanchet, Jean-François Brassard
 * @version SOFI 2.1
 */
public interface ServiceNotification {

  /**
   * Permet d'enregistrer une nouvelle notification.
   * @param notification la notification à enregistrer.
   * @return la notification enregistré.
   */
  Notification enregistrerNotification(Notification notification);

  /**
   * Inactiver une notification.
   * @param noNotification l'identifiant de la notification.
   */
  void inactiverNotification(Long noNotification);

  /**
   * Permet de réactiver une notification qui a été déjà lu et optionnellement
   * forcer son affichage à nouveau à l'utilisateur.
   * @param noNotification l'identifiant de la notification.
   * @param nouvelleLecture forcer une nouvelle lecture par l'utilisateur.
   */
  void reactiverNotification(Long noNotification, boolean nouvelleLecture);

  /**
   * Permet d'indiquer qu'un utilisateur a lu la notification.
   * @param noNotification l'identifiant de la notification.
   * @param codeUtilisateur le code utilisateur
   */
  void lireNotification(Long noNotification, String codeUtilisateur);

  /**
   * La liste des notifications
   * @param codeUtilisateur
   * @param codeApplication
   * @return la liste des notifications pour l'utilisateur.
   */
  List<Notification> getListeNotificationPourUtilisateur(String codeUtilisateur,
      String codeApplication);

  /**
   * Retourne la liste de navigation sur les notifications selon un
   * objet de filtrage.
   * @param liste la liste de navigation a compléter.
   * @return la liste de navigation résultante.
   */
  ListeNavigation getListeNavigationNotification(ListeNavigation liste);

  /**
   * Retourne une notification.
   * @param noNotification l'identification de la notification.
   * @return une notification.
   */
  Notification getNotification(Long noNotification);
}

