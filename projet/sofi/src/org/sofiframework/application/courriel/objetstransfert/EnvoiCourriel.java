/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.courriel.objetstransfert;

import java.util.Date;
import java.util.List;

import org.sofiframework.modele.entite.Entite;

/**
 * Représente l'envoi d'un courriel par un système. Permet de conserver les
 * tentatives d'envoi du courriel.
 * 
 * @author Jean-Maxime Pelletier
 * @since 3.2.4
 */
public class EnvoiCourriel extends Entite {

  private static final long serialVersionUID = 8568641538638093931L;

  private Integer version;

  private String smtpHost;

  private String smtpUsername;

  private String smtpPassword;

  private boolean smtpSecure;

  private Integer nombreTentative;

  private Date dateCreation;

  private Courriel courriel;

  private List<TentativeEnvoiCourriel> listeTentativeEnvoiCourriel;

  private Date derniereDateTentative;

  private String statut;

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getSmtpHost() {
    return smtpHost;
  }

  public void setSmtpHost(String smtpHost) {
    this.smtpHost = smtpHost;
  }

  public String getSmtpUsername() {
    return smtpUsername;
  }

  public void setSmtpUsername(String smtpUsername) {
    this.smtpUsername = smtpUsername;
  }

  public String getSmtpPassword() {
    return smtpPassword;
  }

  public void setSmtpPassword(String smtpPassword) {
    this.smtpPassword = smtpPassword;
  }

  public Integer getNombreTentative() {
    return nombreTentative;
  }

  public void setNombreTentative(Integer nombreTentative) {
    this.nombreTentative = nombreTentative;
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public Courriel getCourriel() {
    return courriel;
  }

  public void setCourriel(Courriel courriel) {
    this.courriel = courriel;
  }

  public List<TentativeEnvoiCourriel> getListeTentativeEnvoiCourriel() {
    return listeTentativeEnvoiCourriel;
  }

  public void setListeTentativeEnvoiCourriel(List<TentativeEnvoiCourriel> listeTentativeEnvoiCourriel) {
    this.listeTentativeEnvoiCourriel = listeTentativeEnvoiCourriel;
  }

  public boolean getSmtpSecure() {
    return smtpSecure;
  }

  public void setSmtpSecure(boolean smtpSecure) {
    this.smtpSecure = smtpSecure;
  }

  public String getStatut() {
    return statut;
  }

  public void setStatut(String statut) {
    this.statut = statut;
  }

  public Date getDerniereDateTentative() {
    return derniereDateTentative;
  }

  public void setDerniereDateTentative(Date derniereDateTentative) {
    this.derniereDateTentative = derniereDateTentative;
  }
}
