package org.sofiframework.application.courriel.objetstransfert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.entite.EntiteTrace;

public class Courriel extends EntiteTrace {
  
  /**
   * Représente une instance d'un courriel envoyé à un utilisateur.
   * 
   * @author jf.brassard
   * @since 3.2.4
   */
  private static final long serialVersionUID = -3442187683923827606L;

  private Long id; 
  private String sujet;
  private String corps;  
  private String corps2;  
  private String typeContenu;
  private String charset;
  private String prenomDestinataire;
  private String nomDestinataire;
  private String adresseDestinataire;
  private String nomExpediteur;
  private String adresseExpediteur;
  private String codeClient;
  private Long idUtilisateur;
  
  private List<EnteteCourriel> listeEnteteCourriel = new ArrayList<EnteteCourriel>();

  
  public Courriel() {
    super();
    this.setCle(new String[]{"id"});
  }
  
  public boolean isTypeHtml() {
    return this.getTypeContenu().equals("html");
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSujet() {
    return sujet;
  }

  public void setSujet(String sujet) {
    this.sujet = sujet;
  }

  public String getCorps() {
    return corps;
  }

  public void setCorps(String corps) {
    this.corps = corps;
  }

  public String getTypeContenu() {
    return typeContenu;
  }

  public void setTypeContenu(String typeContenu) {
    this.typeContenu = typeContenu;
  }

  public String getCharset() {
    return charset;
  }

  public void setCharset(String charset) {
    this.charset = charset;
  }

  public String getAdresseDestinataire() {
    return adresseDestinataire;
  }

  public void setAdresseDestinataire(String adresseDestinataire) {
    this.adresseDestinataire = adresseDestinataire;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public Long getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public String getNomDestinataire() {
    return nomDestinataire;
  }

  public void setNomDestinataire(String nomDestinataire) {
    this.nomDestinataire = nomDestinataire;
  }

  public String getNomExpediteur() {
    return nomExpediteur;
  }

  public void setNomExpediteur(String nomExpediteur) {
    this.nomExpediteur = nomExpediteur;
  }

  public String getAdresseExpediteur() {
    return adresseExpediteur;
  }

  public void setAdresseExpediteur(String adresseExpediteur) {
    this.adresseExpediteur = adresseExpediteur;
  }

  public List<EnteteCourriel> getListeEnteteCourriel() {
    return listeEnteteCourriel;
  }

  public void setListeEnteteCourriel(List<EnteteCourriel> listeEnteteCourriel) {
    this.listeEnteteCourriel = listeEnteteCourriel;
  }

  public String getCorps2() {
    return corps2;
  }

  public void setCorps2(String corps2) {
    this.corps2 = corps2;
  }

  public String getPrenomDestinataire() {
    return prenomDestinataire;
  }

  public void setPrenomDestinataire(String prenomDestinataire) {
    this.prenomDestinataire = prenomDestinataire;
  }
  
  public void ajouterEnteteCourriel(String nom, String valeur) {
    this.listeEnteteCourriel.add(new EnteteCourriel(nom, valeur));
  }

}
