package org.sofiframework.application.courriel.objetstransfert;

public class EnteteCourriel extends org.sofiframework.modele.entite.Entite {

  private static final long serialVersionUID = -7346944386506893521L;

  private String nom;

  private String valeur;

  public EnteteCourriel() {
    super();
    this.setCle(new String[] { "id" });
  }

  public EnteteCourriel(String nom, String valeur) {
    this();
    this.nom = nom;
    this.valeur = valeur;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

}
