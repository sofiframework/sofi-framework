/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.courriel.objetstransfert;

import java.util.Date;

import org.sofiframework.modele.entite.Entite;

/**
 * Information sur une tentative d'envoi d'un courriel.
 * 
 * @author jean-maxime.pelletier
 * @since 3.2.4
 */
public class TentativeEnvoiCourriel extends Entite {

  private static final long serialVersionUID = 5410964332579018096L;

  private Date dateEnvoi;

  private String statut;

  private String messageErreur;

  private EnvoiCourriel envoiCourriel;

  public TentativeEnvoiCourriel() {
    super();
    this.setCle(new String[]{"id"});
  }

  public TentativeEnvoiCourriel(Date dateEnvoi, String statut, String messageErreur) {
    super();
    this.dateEnvoi = dateEnvoi;
    this.statut = statut;
    this.messageErreur = messageErreur;
  }

  public Date getDateEnvoi() {
    return dateEnvoi;
  }

  public void setDateEnvoi(Date dateEnvoi) {
    this.dateEnvoi = dateEnvoi;
  }

  public String getStatut() {
    return statut;
  }

  public void setStatut(String statut) {
    this.statut = statut;
  }

  public String getMessageErreur() {
    return messageErreur;
  }

  public void setMessageErreur(String messageErreur) {
    this.messageErreur = messageErreur;
  }

  public EnvoiCourriel getEnvoiCourriel() {
    return envoiCourriel;
  }

  public void setEnvoiCourriel(EnvoiCourriel envoiCourriel) {
    this.envoiCourriel = envoiCourriel;
  }
}
