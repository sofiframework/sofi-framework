package org.sofiframework.application.courriel.service;

import java.util.HashMap;
import java.util.Map;

import org.sofiframework.application.courriel.objetstransfert.Courriel;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Interface sur le service de courriel de l'infrastructure
 * 
 * @author jfbrassard
 *
 */
public interface ServiceCourriel {

  void envoyer(String sujet, Object[] sujetParam, String corps,
      HashMap<String, Object> detail, String courrielTo, String prenomTo,
      String nomTo, String courrielFrom, String descriptionFrom,
      String codeLocale, String codeClient);

  void envoyer(String sujet, String message, Map<String, String> headers,
      String courrielTo, String prenomTo, String nomTo, String courrielFrom,
      String descriptionFrom, String codeLocale, String codeClient);

  void envoyer(Courriel courriel);
  
  ListeNavigation getListeCourriel(ListeNavigation liste);
  
  Courriel getCourriel(Long id);

}
