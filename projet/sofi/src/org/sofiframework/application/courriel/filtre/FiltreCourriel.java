package org.sofiframework.application.courriel.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreCourriel extends ObjetFiltre {

  /**
   * Filtre de recherche des courriels envoyé par l'infrastructure SOFI.
   * 
   * @author Jean-François Brassard
   * @since SOFI 3.2.4
   */
  private static final long serialVersionUID = 373785555229717336L;
  private String sujet;
  private String corps;
  private String prenomDestinataire;
  private String nomDestinataire;
  private String adresseDestinataire;
  private String nomExpediteur;
  private String adresseExpediteur;
  private String[] codeClient;
  private Long idUtilisateur;

  // La date et l'heure de début de période a extraire.
  private java.util.Date dateHeureDebutPeriode;
  
  // La date et l'heure de début de période a extraire.
  private java.util.Date dateHeureFinPeriode;
  
  public FiltreCourriel() {
    ajouterIntervalle("dateHeureDebutPeriode", "dateHeureFinPeriode",
        "dateCreation");
    ajouterRechercheAvantApresValeur(new String[] { "sujet", "corps",
        "prenomDestinataire", "nomDestinataire", "adresseDestinataire",
        "nomExpediteur", "adresseExpediteur" });
  }

  public String getSujet() {
    return sujet;
  }

  public void setSujet(String sujet) {
    this.sujet = sujet;
  }

  public String getCorps() {
    return corps;
  }

  public void setCorps(String corps) {
    this.corps = corps;
  }

  public String getNomDestinataire() {
    return nomDestinataire;
  }

  public void setNomDestinataire(String nomDestinataire) {
    this.nomDestinataire = nomDestinataire;
  }

  public String getAdresseDestinataire() {
    return adresseDestinataire;
  }

  public void setAdresseDestinataire(String adresseDestinataire) {
    this.adresseDestinataire = adresseDestinataire;
  }

  public String[] getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

  public Long getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public java.util.Date getDateHeureDebutPeriode() {
    return dateHeureDebutPeriode;
  }

  public void setDateHeureDebutPeriode(java.util.Date dateHeureDebutPeriode) {
    this.dateHeureDebutPeriode = dateHeureDebutPeriode;
  }

  public java.util.Date getDateHeureFinPeriode() {
    return dateHeureFinPeriode;
  }

  public void setDateHeureFinPeriode(java.util.Date dateHeureFinPeriode) {
    this.dateHeureFinPeriode = dateHeureFinPeriode;
  }

  public String getNomExpediteur() {
    return nomExpediteur;
  }

  public void setNomExpediteur(String nomExpediteur) {
    this.nomExpediteur = nomExpediteur;
  }

  public String getAdresseExpediteur() {
    return adresseExpediteur;
  }

  public void setAdresseExpediteur(String adresseExpediteur) {
    this.adresseExpediteur = adresseExpediteur;
  }

  public String getPrenomDestinataire() {
    return prenomDestinataire;
  }

  public void setPrenomDestinataire(String prenomDestinataire) {
    this.prenomDestinataire = prenomDestinataire;
  }

}
