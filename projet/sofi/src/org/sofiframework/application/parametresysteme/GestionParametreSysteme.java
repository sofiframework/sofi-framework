/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.parametresysteme;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Objet permettant de spécifier les paramètres systèmes commun à toutes les
 * applications. Correspond à un SINGLETON(Une seule dans la JVM).
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 * @since 3.2 Ajout du cloisonnement par client.
 */
public class GestionParametreSysteme extends BaseGestionService {
  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory
      .getLog(GestionParametreSysteme.class);

  /**
   * Référence à l'instance unique de l'objet de gestiond es paramèter systèmes.
   */
  private static GestionParametreSysteme singleton = new GestionParametreSysteme();

  /**
   * Structure qui concerve les nom et valeurs de paramètres.
   */
  private Map parametreSysteme = Collections.synchronizedMap(new HashMap());
  private String emplacementWebInf;
  private ServiceParametreSysteme service;
  private Set servicesConfigures = new HashSet();

  private GestionParametreSysteme() {
    super(ServiceParametreSysteme.class);
  }

  /**
   * Retourne l'instance unique de la gestion des paramètres systèmes.
   * 
   * @return l'instance unique de la gestion des paramètres systèmes.
   */
  public static GestionParametreSysteme getInstance() {
    return singleton;
  }

  /**
   * Ajouter un paramètres système au gestionnaire des paramètres systèmes
   * 
   * @param nomParametre
   *          le nom du paramètre
   * @param valeur
   *          la valeur du paramètre
   */
  public void ajouterParametreSysteme(String nomParametre, Object valeur) {
    String cle = getCleParametre(null, nomParametre, null);
    ajouterParametreSysteme(cle, valeur, null);
  }

  /**
   * Ajouter un paramètres système au gestionnaire des paramètres systèmes
   * 
   * @param nomParametre
   *          le nom du paramètre
   * @param valeur
   *          la valeur du paramètre
   * @since 3.2 Ajout du cloisonnement par client.
   */
  public void ajouterParametreSysteme(String nomParametre, Object valeur,
      String codeClient) {
    
    if (!StringUtils.isEmpty(codeClient)) {
      nomParametre = getCleParametre(null, nomParametre, codeClient);
    }

    if (log.isDebugEnabled()) {
      if (valeur != NULL) {
        log.debug("Ajout du paramètre système -> " + nomParametre + " : "
            + valeur);
      } else {
        log.debug("Ajout du paramètre système -> " + nomParametre + " : "
            + null);
      }
    }

    if (parametreSysteme.get(nomParametre) != null) {
      getInstance().supprimerParametreSysteme(nomParametre);
    }

    parametreSysteme.put(nomParametre, valeur);
  }

  /**
   * Retourne la valeur d'un paramètre système
   * 
   * @param nomParametre
   *          le nom d'un paramètre système
   * @return la valeur du paramètre système
   */
  public Object getParametreSysteme(String nomParametre) {

    return getParametreSystemeClient(nomParametre, null);
  }

  /**
   * Retourne la valeur d'un paramètre système
   * 
   * @param nomParametre
   *          le nom d'un paramètre système
   * @param codeClient
   *          le code du client
   * @return la valeur du paramètre système
   */
  public Object getParametreSystemeClient(String nomParametre, String codeClient) {

    String cle = getCleParametre(null, nomParametre, codeClient);

    Object valeur = null;

    String codeApplication = GestionSecurite.getInstance().getCodeApplication();

    if (nomParametre != null) {
      valeur = unmaskNull(parametreSysteme.get(cle));

      if ((parametreSysteme.get(nomParametre) == null)
          && !nomParametre.equals(ConstantesParametreSysteme.NOM_CLASSE_MODELE)
          && !nomParametre
              .equals(ConstantesParametreSysteme.FICHIER_CONFIG_SERVICE_EJB)) {
        valeur = getParametreSysteme(codeApplication, nomParametre, codeClient);
      }
    }

    return valeur;
  }

  /**
   * Retourne la valeur d'un paramètre système.
   * <p>
   * Par défaut, il prend le code application de l'application.
   * 
   * @param cle
   *          la clé du paramètre système
   * @return la valeur du paramètre système
   * @deprecated Utiliser getParametreSysteme()
   */
  @Deprecated
  public String getParametreSystemeEJB(String cle) {
    String codeApplication = GestionSecurite.getInstance().getCodeApplication();

    return getParametreSysteme(codeApplication, cle, false);
  }
  
  /**
   * Retourne la valeur d'un paramètre système.
   * <p>
   * Par défaut, il prend le code application de l'application.
   * 
   * @param codeApplication Le code de l'application.
   * @param cle
   *          la clé du paramètre système
   * @return la valeur du paramètre système
   * @deprecated Utiliser getParametreSysteme()
   */
  @Deprecated
  public String getParametreSystemeEJB(String codeApplication, String cle) {
    return getParametreSysteme(codeApplication, cle, false);
  }  
  
  
  public String getParametreSysteme(String codeApplication, String cle) {

    return getParametreSysteme(codeApplication, cle, null);
  }

  /**
   * Retourne la valeur d'un paramètre système
   * 
   * @param codeApplication
   *          le code application dont le paramètre système est disponible.
   * @param cle
   *          la clé du paramètre système
   * @param cacheParApplication
   * @return la valeur du paramètre système
   */
  public String getParametreSysteme(String codeApplication, String cle,
      boolean cacheParApplication) {
    return getParametreSysteme(codeApplication, cle, null);

  }

  /**
   * Retourne la valeur d'un paramètre système
   * 
   * @param codeApplication
   *          le code application dont le paramètre système est disponible.
   * @param cle
   *          la clé du paramètre système
   * @param codeClient
   * @return la valeur du paramètre système
   * @since 3.2
   */
  public String getParametreSysteme(String codeApplication, String cle,
      String codeClient) {

   return getValeurParamereSysteme(codeApplication, cle, codeClient);
  }

 
  /**
   * Supprimer un paramètre système
   * 
   * @param nomParametre
   *          le nom du paramètre à supprimer.
   */
  public void supprimerParametreSysteme(String nomParametre) {
    supprimerParametreSysteme(nomParametre, null);
  }

  /**
   * Supprimer un paramètre système
   * 
   * @param nomParametre
   *          le nom du paramètre à supprimer.
   * @param codeClient
   */
  public void supprimerParametreSysteme(String nomParametre, String codeClient) {
    String cle = getCleParametre(null, nomParametre, codeClient);
    parametreSysteme.remove(cle);
  }

  /**
   * Obtenir la valeur chaîne de caractère d'un paramètre.
   * <p>
   * 
   * @return la valeur chaîne de caractère d'un paramètre
   */
  public String getString(String nom) {
    return getString(nom, null);
  }

  public String getString(String nom, String codeClient) {
    Object valeur = this.getParametreSysteme(null, nom, codeClient);

    return (valeur == null) ? "" : valeur.toString();
  }

  /**
   * Obtenir une valeur de paramètre entier
   * 
   * @param nom
   *          le nom du paramètre dont l'on désire obtenir la valeur.
   * @return valeur du paramètre sous la forme d'un entier.
   */
  public Integer getInteger(String nom) {
    return getInteger(nom, null);
  }

  /**
   * Obtenir une valeur de paramètre entier
   * 
   * @param nom
   *          le nom du paramètre dont l'on désire obtenir la valeur.
   * @return valeur du paramètre sous la forme d'un entier.
   */
  public Integer getInteger(String nom, String codeClient) {
    Object valeur = this.getParametreSysteme(null, nom, codeClient);

    if ((valeur != null) && !(valeur instanceof Integer)) {
      if (valeur instanceof String) {
        try {
          valeur = new Integer((String) valeur);
        } catch (Exception e) {
          log.error(
              "Il s'est produit une erreur lors de la conversion du paramètre en Integer. Vérifier la valeur.",
              e);
        }
      } else {
        log.error("Conversion non suportée.");
      }
    }

    return (Integer) valeur;
  }

  /**
   * Obtenir une valeur de paramètre Boolean
   * 
   * @param nom
   *          le nom du paramètre dont l'on désire obtenir la valeur.
   * @return valeur du paramètre sous la forme d'un Boolean.
   */
  public Boolean getBoolean(String nom) {
    return getBoolean(nom, null);
  }

  /**
   * Obtenir une valeur de paramètre Boolean
   * 
   * @param nom
   *          le nom du paramètre dont l'on désire obtenir la valeur.
   * @param codeClient
   * @return valeur du paramètre sous la forme d'un Boolean.
   */
  public Boolean getBoolean(String nom, String codeClient) {
    Object valeur = this.getParametreSysteme(null, nom, codeClient);

    if (valeur == null) {
      return Boolean.FALSE;
    } else if (valeur instanceof Boolean) {
      return (Boolean) valeur;
    } else if (valeur instanceof String) {
      return Boolean.valueOf((String) valeur);
    } else {
      throw new SOFIException("Conversion non suportée.");
    }
  }

  /**
   * Retourne une valeur pour un paramètre système d'une application et
   * possiblement additionné de la valeur correspondant au système commun.
   * <p>
   * Dans le cas de valeur additionné, les valeurs seront séparé par une
   * virgule.
   * 
   * @param nomParametre
   *          le nom du paramêtre dont on désire extraire la valeur.
   * @return la valeur du paramètre.
   */
  public Object getListeValeursAvecSystemeCommun(String nomParametre) {
    String codeApplicationCommun = (String) GestionParametreSysteme
        .getInstance().getParametreSysteme(
            ConstantesParametreSysteme.CODE_APPLICATION_COMMUN);
    Object valeurs = null;

    if (!UtilitaireString.isVide(codeApplicationCommun)) {

      valeurs = parametreSysteme.get(getCleParametre(codeApplicationCommun,
          nomParametre, null));

      if (valeurs == null) {
        // Valeur provenant du système.
        String valeurCommun = service.getValeur(codeApplicationCommun,
            nomParametre);

        // Valeur du paramètre local au système
        Object valeurLocal = GestionParametreSysteme.getInstance()
            .getParametreSysteme(nomParametre);

        HashSet liste = null;

        if (valeurLocal != null) {
          if (valeurLocal instanceof HashSet) {
            liste = (HashSet) valeurLocal;
          } else {
            liste = getListeValeurAvecChaineMultiple(valeurLocal.toString());
          }
        }

        if (valeurCommun != null) {
          HashSet listeCommun = getListeValeurAvecChaineMultiple(valeurCommun
              .toString());

          Iterator iterateurCommun = listeCommun.iterator();

          while (iterateurCommun.hasNext()) {
            String valeur = (String) iterateurCommun.next();

            if (!liste.contains(valeur)) {
              liste.add(valeur);
            }
          }
        }

        if (log.isDebugEnabled() && (liste != null)) {
          log.debug("Ajout du paramètre système -> "
              + getCleParametre(codeApplicationCommun, nomParametre, null)
                  .toString() + " : " + liste.toString());
        }

        parametreSysteme.put(
            getCleParametre(codeApplicationCommun, nomParametre, null)
                .toString(), liste);

        valeurs = liste;
      }
    } else {
      valeurs = getParametreSysteme(nomParametre);
    }

    return valeurs;
  }

  /**
   * Retourne une valeur converti en chaine (chaque valeur séparé d'une virgule)
   * pour un paramètre système d'une application et possiblement additionné de
   * la valeur correspondant au système commun.
   * <p>
   * Dans le cas de valeur additionné, les valeurs seront séparé par une
   * virgule.
   * 
   * @param nomParametre
   *          le nom du paramêtre dont on désire extraire la valeur.
   * @return la valeur du paramètre en chaine de caractères.
   */
  public String getListeValeursEnChaineCaractereAvecSystemeCommun(
      String nomParametre) {
    Object listeValeur = getListeValeursAvecSystemeCommun(nomParametre);

    if ((listeValeur != null) && (listeValeur.toString().length() > 1)) {
      String listeValeurTmp = listeValeur.toString();

      return listeValeurTmp.substring(1, listeValeurTmp.length() - 1);
    } else {
      return "";
    }
  }

  /**
   * Chargement d'un fichier de paramètres.
   * 
   * @param fichierParametres
   *          fichier de paramètres.
   */
  public void chargerFichierParametres(String fichierParametres)
      throws IOException {
    List listeParametres = MetaDonneeParametre
        .getListeParametres(fichierParametres);
    traitementChargement(listeParametres);
  }

  /**
   * Chargement d'un node xml de paramètres.
   * 
   * @param noeudParent
   *          le node parent d'un contenu xml.
   */
  public void chargerFichierParametres(InputStream in) {
    List listeParametres = MetaDonneeParametre.getListeParametres(in);

    traitementChargement(listeParametres);
  }

  private void traitementChargement(List listeParametres) {
    for (Iterator i = listeParametres.iterator(); i.hasNext();) {
      MetaDonneeParametre p = (MetaDonneeParametre) i.next();

      if (!StringUtils.isEmpty(p.getClient())) {
        this.ajouterParametreSysteme(p.getNom(), p.getValeur(), p.getClient());
      }else {
        this.ajouterParametreSysteme(p.getNom(), p.getValeur());
      }
    }
  }

  public HashSet getListeValeurAvecChaineMultiple(String chaine) {
    // Plusieurs paramètres y sont associés.
    StringTokenizer liste = new StringTokenizer(chaine, ",");
    HashSet listeValeur = new HashSet();

    while (liste.hasMoreTokens()) {
      String valeur = liste.nextToken();
      listeValeur.add(valeur);
    }

    return listeValeur;
  }

  /**
   * Retourne true si le gestionnaire a initialise la liste de paramètres par
   * défaut.
   * 
   * @return true si le gestionnaire a initialise la liste de paramètres par
   *         défaut.
   */
  public boolean isListeParametresIntialisee() {
    if (this.parametreSysteme != null && this.parametreSysteme.size() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne true si le gestionnaire des paramètres systèmes est actif.
   * 
   * @param nomService
   *          le nom du service qui instancie des paramètres systèmes.
   * @return true si le gestionnaire des paramètres système a déjà traité le
   *         fichier de base.
   */
  public boolean isGestionParametreSystemeActif(String nomService) {
    return !servicesConfigures.add(nomService);
  }

  public void setEmplacementWebInf(String emplacementWebInf) {
    this.emplacementWebInf = emplacementWebInf;
  }

  public String getEmplacementWebInf() {
    return emplacementWebInf;
  }

  public ServiceParametreSysteme getService() {
    return service;
  }

  public void setService(ServiceParametreSysteme service) {
    this.service = service;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setService((ServiceParametreSysteme) service);
  }

  /**
   * Retourne la clé complète du paramètre système.
   * 
   * @param codeApplication
   *          le code application
   * @param cle
   *          la clé
   * @return la clé est composé de codeApplication.cle
   */
  public String getCleParametre(String codeApplication, String cle,
      String codeClient) {
    if (codeApplication == null) {
      codeApplication = GestionSecurite.getInstance().getCodeApplication();
    }
    StringBuffer cleComplet = new StringBuffer(codeApplication);
    cleComplet.append(".");
    cleComplet.append(cle);
    if (!StringUtils.isEmpty(codeClient)) {
      cleComplet.append(".");
      cleComplet.append(codeClient);
    }
    return cleComplet.toString();
  }

  private String getValeurParamereSysteme(String codeApplication, String cle,
      String codeClient) {
    if (cle == null) {
      return null;
    }

    String cleDansCache = getCleParametre(codeApplication, cle, codeClient);

    Object valeur = parametreSysteme.get(cleDansCache);

    try {
      if ((valeur == null) && (service != null)) {

        if (StringUtils.isEmpty(codeApplication)) {
          codeApplication = GestionSecurite.getInstance().getCodeApplication();
        }

        valeur = service.getValeur(codeApplication, cle, codeClient);
        
        if (codeClient != null) {
          valeur = service.getValeur(codeApplication, cle, codeClient);
          List<Client> listeClientAscendant = GestionSecurite.getInstance()
              .getListeClientAscendant(codeClient);
          for (Client ascendant : listeClientAscendant) {
            if (valeur == null) {
              valeur = service.getValeur(codeApplication, cle,
                  ascendant.getNoClient());
            }
          }
        }else {
          valeur = service.getValeur(codeApplication, cle, null);
        }
        
        
        if (valeur == null) {
          
          String cleDansCacheSansClient = getCleParametre(codeApplication, cle, null);
          // Extraire dans cache si dispo
          valeur = parametreSysteme.get(cleDansCacheSansClient);
          
          if (valeur == null) {
             // Appel du service
             valeur = service.getValeur(codeApplication, cle, null);  
          }
          
          // Valider s'il existe un système commun pour les paramètres systèmes
          // inter-application.
          if (valeur == null
              && !ConstantesParametreSysteme.CODE_APPLICATION_COMMUN
                  .equals(cle)) {
            String codeApplicationCommun = GestionParametreSysteme
                .getInstance().getString(
                    ConstantesParametreSysteme.CODE_APPLICATION_COMMUN);
            if (!UtilitaireString.isVide(codeApplicationCommun)) {
              valeur = service
                  .getValeur(codeApplicationCommun, cle, codeClient);
              
              if (valeur == null) {
                valeur = service
                    .getValeur(codeApplicationCommun, cle, null);
              }
            }
          }
        }
        // La valeur null est masqué afin que la connaissance que
        // le service retourne null pour un paramètre donnée
        // soit conservée dans la cache.
        ajouterParametreSysteme(cleDansCache, maskNull(valeur), null);

      } else {
        valeur = unmaskNull(valeur);
      }
    } catch (Exception e) {
      if (e instanceof org.springframework.remoting.RemoteAccessException) {
        log.error(e);
        throw new SOFIException(e);
      }
      
      if (log.isWarnEnabled()) {
        log.warn("Code système problématique : " + cle
            + " /nDétail de l'erreur :" + e.toString(), e);
      }
    }

    return (valeur == null) ? null : valeur.toString();
  }

}
