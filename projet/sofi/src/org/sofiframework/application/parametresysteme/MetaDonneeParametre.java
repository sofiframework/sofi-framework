/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.parametresysteme;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireXml;
import org.sofiframework.utilitaire.fichier.UtilitaireFichier;
import org.w3c.dom.Node;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


/**
 * Objet d'affaires qui décrit un paramèter.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class MetaDonneeParametre extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 2360710707759493248L;

  /**
   * Nom de la balise XML qui représente une liste de paramètres.
   */
  private static final String NOM_LISTE_NOEUD_XML = "parametres";

  /**
   * Nom d la balise XMl qui représente un paramètre.
   */
  public static final String NOM_NOEUD_XML = "parametre";

  /**
   * Nom du parametre.
   */
  private String nom;
  
  
  /**
   * Nom du client.
   */
  private String client;

  /**
   * Valeur du paramètre.
   */
  private String valeur;

  /** Constructeur par défaut */
  public MetaDonneeParametre() {
  }

  /**
   * La valeur du paramètre
   * @param valeur la nouvelle valeur du paramètre
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtnir la valeur du paramètre.
   * @return la val;eur du paramètre.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer le nom du parametre.
   * @param nom Nom du parametre.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le nom du parametre.
   * @return le nom du parametre
   */
  public String getNom() {
    return nom;
  }

  /**
   * Obtient une liste de parmètre contenu dans un fichier XML.
   * @param fichierParametres fichier XML de paramètres
   * @return Liste de paramètres.
   */
  public static List getListeParametres(String fichierParametres)
      throws IOException {
    byte[] bytes = UtilitaireFichier.convertirFichierEnByte(fichierParametres);
    String encoding = null;
    try {
      encoding = UtilitaireXml.getEncoding(bytes);
    } catch (Exception e) {
      // On a pas d'encoding de déclaré
    }
    return getListeParametres(new ByteArrayInputStream(bytes), encoding);
  }

  /**
   * Obtient une liste de parmètre contenu dans un node XML.
   * @param noeudParent le node parent du contenu XML
   * @return Liste de paramètres.
   */
  public static synchronized List getListeParametres(Node noeudParent) {
    ArrayList listeNoeuds = UtilitaireXml.getListeNodeDansUneNode(noeudParent,
        NOM_LISTE_NOEUD_XML);

    //liste de contextes
    ArrayList listeParametres = new ArrayList();

    for (Iterator i = listeNoeuds.iterator(); i.hasNext();) {
      Node noeud = (Node) i.next();
      MetaDonneeParametre parametre = new MetaDonneeParametre();

      // Le nom de classe est remplacé par une version plus literale
      parametre.ajouterAliasXml(NOM_NOEUD_XML,
          org.sofiframework.application.parametresysteme.MetaDonneeParametre.class);
      parametre.lireXml(noeud);
      listeParametres.add(parametre);
    }

    return listeParametres;
  }

  /**
   * Obtient une liste de paramètre d'un flux d'entrée XML.
   * @param in Flux d'entrée XML
   * @return Liste de paramètres.
   */
  public static synchronized List getListeParametres(InputStream in) {
    return getListeParametres(new InputStreamReader(in));
  }

  /**
   * Obtient une liste de paramètre d'un reader
   * @param reader Reader
   * @return Liste de paramètres.
   */
  public static synchronized List getListeParametres(Reader reader) {
    XStream xstream = new XStream(new DomDriver());
    xstream.alias(NOM_LISTE_NOEUD_XML, List.class);
    xstream.alias(NOM_NOEUD_XML, MetaDonneeParametre.class);

    return (List) xstream.fromXML(reader);
  }

  /**
   * Obtient une liste de paramètre d'un flux d'entrée et d'un encoding de fichier
   * @param xml Flux d'entrée
   * @param encoding Encoding du xml
   * @return Liste de paramètres.
   */
  public static synchronized List getListeParametres(InputStream xml, String encoding) {
    XStream xstream = new XStream(new DomDriver(encoding));
    xstream.alias(NOM_LISTE_NOEUD_XML, List.class);
    xstream.alias(NOM_NOEUD_XML, MetaDonneeParametre.class);
    return (List) xstream.fromXML(xml);
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }
}
