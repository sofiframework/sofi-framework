/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.parametresysteme;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe utilitaire pour accéder divers éléments du gestionnaire de paramètre système.
 */
public class UtilitaireParametreSysteme {
  /** Variable désignant l'instance commune de journalisation */
  protected static Log log = LogFactory.getLog(UtilitaireParametreSysteme.class);

  static public String getMethodeParam() {
    String methodeParam = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.METHODE_PARAM);

    if (UtilitaireString.isVide(methodeParam)) {
      methodeParam = "methode";
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(ConstantesParametreSysteme.METHODE_PARAM,
          methodeParam);
    }

    return methodeParam;
  }

  static public String getExtensionActionUrl() {
    String extension = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.EXTENSION_ACTION_URL);

    if (UtilitaireString.isVide(extension)) {
      extension = ".do";
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(ConstantesParametreSysteme.EXTENSION_ACTION_URL,
          extension);
    }

    return extension;
  }
}
