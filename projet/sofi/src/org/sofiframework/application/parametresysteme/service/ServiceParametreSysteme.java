/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.parametresysteme.service;

/**
 * Interface sur le service de paramètre système de l'infrastructure.
 * <p>
 * @author Jean-François Brassard
 * @since SOFI 2.0.3 Il est maintenant possible d'enregistrer un nouvelle
 * valeur de paramètre système lorsque celui-ci est définit en permettant
 * la modification à distance.
 */
public interface ServiceParametreSysteme {
  /**
   * Permet d'extraire un valeur de paramètre système de
   * l'infrastructure.
   * @param codeApplication le code de l'application.
   * @param codeParametre le code du paramètre.
   * @return la valeur du paramètre système.
   */
  String getValeur(String codeApplication, String codeParametre);


  /**
   * Permet d'extraire un valeur de paramètre système de
   * l'infrastructure.
   * @param codeApplication le code de l'application.
   * @param codeParametre le code du paramètre.
   * @param codeClient le code client
   * @return la valeur du paramètre système.
   * @since 3.2
   */
  String getValeur(String codeApplication, String codeParametre, String codeClient);

  /**
   * Permet d'enregistrer un valeur pour un paramètre système
   * de l'infrastructure.
   * @param codeApplication le code de l'application.
   * @param code le code du paramètre.
   */
  void enregistrerValeur(String codeApplication, String code, String valeur);

  /**
   * Permet d'enregistrer un valeur pour un paramètre système
   * de l'infrastructure.
   * @param codeApplication le code de l'application.
   * @param code le code du paramètre.
   * @param valeur la valeur du paramètre système.
   * @since 3.2
   */
  void enregistrerValeur(String codeApplication, String code, String valeur, String codeClient);
}
