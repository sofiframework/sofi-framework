/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.conversion.objettransfert;

import org.sofiframework.objetstransfert.Entite;

/**
 * Objet d'affaire représentant un objet de conversion de données.
 * 
 * À utiliser lorsque nous avons une anciennne et une nouvelle application qui
 * doivent communiquer entre eux et que le données doivent être syncronisées
 * dans les deux applications.
 * 
 * @author Sébastien Cayer, nurun inc
 */
public class Conversion extends Entite {

  private static final long serialVersionUID = -7659456590224641469L;

  private String nom;
  private String codeApplicationSource;
  private String codeApplicationDestination;
  private String sourceId;
  private String destinationId;

  public Conversion() {
    super();
    this.setCle(new String[] {"id"});
  }

  public Conversion(String nom, String codeApplicationSource,
      String codeApplicationDestination) {
    this();
    this.nom = nom;
    this.codeApplicationSource = codeApplicationSource;
    this.codeApplicationDestination = codeApplicationDestination;
  }

  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public String getDestinationId() {
    return destinationId;
  }

  public void setDestinationId(String destinationId) {
    this.destinationId = destinationId;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeApplicationSource() {
    return codeApplicationSource;
  }

  public void setCodeApplicationSource(String codeApplicationSource) {
    this.codeApplicationSource = codeApplicationSource;
  }

  public String getCodeApplicationDestination() {
    return codeApplicationDestination;
  }

  public void setCodeApplicationDestination(String codeApplicationDestination) {
    this.codeApplicationDestination = codeApplicationDestination;
  }
}