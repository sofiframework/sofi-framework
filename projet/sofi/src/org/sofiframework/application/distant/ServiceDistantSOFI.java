/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.distant;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.application.journalisation.service.ServiceJournalisation;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.modele.distant.GestionServiceDistant;

/**
 *  Composant central (singleton) d'accès aux services de l'infrastructure SOFI.
 *  <p>
 *  @author Jean-François Brassard
 *  @version SOFI 2.1
 */
public class ServiceDistantSOFI {

  /**
   * Instance du singleton
   */
  private static ServiceDistantSOFI instance = new ServiceDistantSOFI();

  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory.getLog(ServiceDistantSOFI.class);

  /**
   * Constructeur de l'utilitaire.
   */
  protected ServiceDistantSOFI() {
    super();
  }

  /**
   * Retourne l'instance du singleton.
   */
  public static ServiceDistantSOFI getInstance() {
    return instance;
  }

  /**
   * Accès au service des applications.
   * @return le service des applications.
   */
  public ServiceApplication getServiceApplication() {
    return (ServiceApplication)GestionServiceDistant.getServiceLocal("ServiceApplicationBean",
        ServiceApplication.class);
  }

  /**
   * Accès au service des paramètres systèmes.
   * @return le service des paramètres systèmes.
   */
  public ServiceParametreSysteme getServiceParametreSysteme() {
    return (ServiceParametreSysteme)GestionServiceDistant.getServiceLocal("ServiceParametreSystemeBean",
        ServiceParametreSysteme.class);
  }

  /**
   * Accès au service des domaines de valeurs.
   * @return le service des domaines de valeurs.
   */
  public ServiceDomaineValeur getServiceDomaineValeur() {
    return (ServiceDomaineValeur)GestionServiceDistant.getServiceLocal("ServiceDomaineValeurBean",
        ServiceDomaineValeur.class);
  }

  /**
   * Accès au service du référentiel des composant d'interface.
   * @return au service du référentiel des composants d'interface.
   */
  public ServiceObjetSecurisable getServiceReferentiel() {
    return (ServiceObjetSecurisable)GestionServiceDistant.getServiceLocal("ServiceReferentielObjetJavaBean",
        ServiceObjetSecurisable.class);
  }

  /**
   * Accès au service des messages.
   * @return le service des messages.
   */
  public ServiceMessage getServiceMessage() {
    return (ServiceMessage)GestionServiceDistant.getServiceLocal("ServiceMessageBean",
        ServiceMessage.class);
  }

  /**
   * Accès au service des libellés.
   * @return le service des libellés.
   */
  public ServiceLibelle getServiceLibelle() {
    return (ServiceLibelle)GestionServiceDistant.getServiceLocal("ServiceLibelleBean",
        ServiceLibelle.class);
  }

  /**
   * Accès au service de l'aide en ligne.
   * @return au service de l'aide en ligne.
   */
  public ServiceAide getServiceAide() {
    return (ServiceAide)GestionServiceDistant.getServiceLocal("ServiceAideBean",
        ServiceAide.class);
  }

  /**
   * Accès au service des utilisateurs.
   * @return le service des utilisateurs.
   */
  public ServiceUtilisateur getServiceUtilisateur() {
    return (ServiceUtilisateur)GestionServiceDistant.getServiceLocal("ServiceUtilisateurBean",
        ServiceUtilisateur.class);
  }

  /**
   * Accès au service des utilisateurs.
   * @return le service des utilisateurs.
   */
  public ServiceSecurite getServiceSecurite() {
    return (ServiceSecurite)GestionServiceDistant.getServiceLocal("ServiceSecuriteBean",
        ServiceSecurite.class);
  }

  /**
   * Accès au service d'authentification.
   * @return le service d'authentification.
   */
  public ServiceAuthentification getServiceAuthentification() {
    return (ServiceAuthentification)GestionServiceDistant.getServiceLocal("ServiceAuthentificationBean",
        ServiceAuthentification.class);
  }

  /**
   * Accès au service de journalisation.
   * @return le service de journalisation.
   */
  public ServiceJournalisation getServiceJournalisation() {
    return (ServiceJournalisation)GestionServiceDistant.getServiceLocal("ServiceJournalisationBean",
        ServiceJournalisation.class);
  }

}
