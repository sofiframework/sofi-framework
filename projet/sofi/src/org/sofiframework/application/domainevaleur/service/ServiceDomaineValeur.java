/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur.service;

import java.util.List;

import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Interface local permettant d'accéder à un service permettant la gestion des domaines de valeurs
 * <p>
 * Le distance implémenté doit donc être basé sur cette interface.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 2.0
 */
public interface ServiceDomaineValeur {

  /**
   * Méthode qui permet de rechercher des domaines de valeurs actifs.
   * 
   * @param ln
   *          l'objet <code>ListeNavigation</code> à retourner avec le résultat de la requête.
   * @param actifSeulement
   *          valeur qui indique si on doit retourner la liste des domaines de valeurs qui sont actifs seulement
   * @param valeurSelectionnee
   *          parametre utilisé seulement quand la valeur actifSeulement est à false. Cette valeur permet de spécifier
   *          une valeur qui doit apparaître dans la requête même si cette valeur est inactive.
   * @return le même objet <code>ListeNavigation</code> contenant le résultat de la recherche.
   */
  ListeNavigation getListeActif(ListeNavigation liste, String valeurSelectionnee);

  /**
   * Retourne une liste de navigation page par page qui représente les valeurs d'un domaine spécifique.
   * 
   * @return une liste de navigation page par page qui représente les valeurs d'un domaine spécifique.
   * @param liste
   *          la liste de navigation page par page incluant les paramètres de filtrage via le composant
   *          org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur.
   */

  ListeNavigation getListeValeur(ListeNavigation liste);

  /**
   * Méthode qui sert à obtenir la liste des domaines de valeur enfants d'une définition de domaine déjà existant dans
   * la base de données.
   * <p>
   * 
   * @param codeApplication
   *          l'identifiant de l'application du domaine de valeur.
   * @param nom
   *          le nom du domaine de valeur dont on désire la liste de valeur.
   * @param valeur
   *          le code de valeur dont on désire la liste de valeur. Spécifier null si vous désirez la racine du domaine
   *          de valeur.
   * @param codeLangue
   *          Le code de langue dont on désire la liste de valeur. Spécifier null si vous désirez que toutes les
   *          descriptions par langue soit inclut dans une seule occurence de domaine de valeur.
   * @return la liste des domaines de valeur correspondant au paramètres spécifiés.
   */
  List getListeValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent,
      boolean valeurInactive);

  List getListeValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent);

  /**
   * Retourne une valeur de domaine spécifique avec les descriptions par langue s'il y lieu.
   * 
   * @return une valeur de domaine spécifique avec les descriptions par langue s'il y lieu.
   * @param codeLangue
   *          le code de langue spécifique à la valeur désiré, spécifier null si vous désirez avoir toutes les
   *          descriptions par langue inclut dans la valeur du domaine.
   * @param valeur
   *          la valeur du domaine désiré.
   * @param nom
   *          le nom du domaine correspondant à la valeur désiré.
   * @param codeApplication
   *          le code d'application correspondant à la valeur du domaine désiré.
   */
  /**
   * Retourne une valeur de domaine spécifique avec les descriptions par langue s'il y lieu.
   * 
   * @return une valeur de domaine spécifique avec les descriptions par langue s'il y lieu.
   * @param codeApplication
   *          le code d'application correspondant à la valeur du domaine désiré.
   * @param nom
   *          le nom du domaine correspondant à la valeur désiré.
   * @param valeur
   *          la valeur du domaine désiré.
   * @param codeLangue
   *          le code de langue spécifique à la valeur désiré, spécifier null si vous désirez avoir toutes les
   *          descriptions par langue inclut dans la valeur du domaine.
   * @param codeClient
   *          le code du client dont on vuet le domaine de valeur (peut être null).
   */
  DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue, String codeClient);

  void activerDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  void desactiverDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  void enregistrerDomaineValeur(DomaineValeur domaine, DomaineValeur domaineOriginial);

  void supprimerDomaineValeur(DomaineValeur domaine);

  List getListeDefinitionDomaineValeur(FiltreDomaineValeur filtre);

  DomaineValeur getDefinitionDomaineValeur(String codeApplication, String nom, String codeLangue);

  List getListeDomaineValeur(FiltreDomaineValeur filtre);

  ListeNavigation getListeDefinitionDomaineValeur(ListeNavigation liste);

  void enregistrerDefinitionDomaineValeur(DomaineValeur definition);

  void enregistrerDomaineValeur(DomaineValeur domaineValeur);

  void ajouterDefinitionDomaineValeur(DomaineValeur definition);

  void ajouterDomaineValeur(DomaineValeur domaine);

  ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste);

}
