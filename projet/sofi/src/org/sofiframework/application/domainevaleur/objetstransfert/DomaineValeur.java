/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur.objetstransfert;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Objet de transfert traitant un domaine de valeur.
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 * @since SOFI 2.0 Le code de langue (codeLangue) a été ajouté. Le nom du
 *        domaine (nom) a été ajouté.
 * @since SOFI 2.0.1 le code du type de tri, l'ordre d'affichage ont été ajouté.
 */
public class DomaineValeur extends ObjetTransfert {
  private static final long serialVersionUID = 7612604564344138279L;
  private String nom;

  private String valeur;
  private String description;
  private String codeLangue;
  private HashMap listeDescriptionLangue;
  private ArrayList listeDomaineValeurEnfant;
  private DomaineValeur domaineValeurParent;
  private String codeTypeTri;
  private Integer ordreAffichage;

  private String codeApplication;
  private String nomParent;
  private String valeurParent;
  private String codeApplicationParent;
  private String codeLangueParent;
  private Date dateDebut;
  private Date dateFin;

  private ArrayList<DomaineValeur> associationInverse;

  private Boolean actif;

  private String codeClient;

  public DomaineValeur() {
  }

  /**
   * Fixer le nom du domaine.
   * 
   * @param nom
   *          le nom du domaine.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom du domaine.
   * 
   * @return le nom du domaine.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Retourne la valeur du domaine.
   * 
   * @return la valeur du domaine.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur du domaine.
   * 
   * @param valeur
   *          la valeur du domaine.
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Retourne la description.
   * 
   * @return la description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Retourne la description selon la langue spécifié.
   * 
   * @return la description selon la langue spécifié.
   * @param codeLangue
   *          le code de langue dont on désire la description.
   */
  public String getDescription(String codeLangue) {
    if ((getDescription() == null) && (getListeDescriptionLangue() != null)) {
      return (String) getListeDescriptionLangue().get(codeLangue);
    } else {
      return description;
    }
  }

  /**
   * Fixer la description du domaine.
   * 
   * @param description
   *          la description.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne les descriptions par langue.
   * 
   * @return la liste des descriptions par langue
   */
  public HashMap getListeDescriptionLangue() {
    return listeDescriptionLangue;
  }

  /**
   * Fixer la liste des descriptions par langue.
   * 
   * @param listeDescriptionLangue
   *          la liste des descriptions par langue.
   */
  public void setListeDescriptionLangue(HashMap listeDescriptionLangue) {
    this.listeDescriptionLangue = listeDescriptionLangue;
  }

  /**
   * Ajouter une description pour une langue spécifique.
   * 
   * @param description
   *          la description de la valeur.
   * @param codeLangue
   *          la langue spécifique.
   */
  public void ajouterDescriptionLangue(String codeLangue, String description) {
    if (this.listeDescriptionLangue == null) {
      this.listeDescriptionLangue = new HashMap();
    }

    this.listeDescriptionLangue.put(codeLangue, description);
  }

  /**
   * Retourne la liste des domaine de valeur enfant.
   * 
   * @return la liste des domaine de valeur enfant.
   */
  public ArrayList getListeDomaineValeurEnfant() {
    return listeDomaineValeurEnfant;
  }

  /**
   * Fixer la liste des domaines de valeurs enfant.
   * 
   * @param listeDomaineValeurEnfant
   *          la liste des domaines de valeurs enfant.
   */
  public void setListeDomaineValeurEnfant(ArrayList listeDomaineValeurEnfant) {
    this.listeDomaineValeurEnfant = listeDomaineValeurEnfant;
  }

  /**
   * Retourne le domaine de valeur parent.
   * 
   * @return le domaine de valeur parent.
   */
  public DomaineValeur getDomaineValeurParent() {
    return domaineValeurParent;
  }

  /**
   * Fixer le domaine de valeur parent.
   * 
   * @param domaineValeurParent
   *          le domaine de valeur parent.
   */
  public void setDomaineValeurParent(DomaineValeur domaineValeurParent) {
    this.domaineValeurParent = domaineValeurParent;
  }

  /**
   * Fixer le code de langue du domaine de valeur.
   * 
   * @param codeLangue
   *          code de langue du domaine de valeur.
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue du domaine de valeur.
   * 
   * @return le code de langue du domaine de valeur.
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixer le code de type de tri.
   * 
   * @param codeTypeTri
   *          le code de type de tri.
   */
  public void setCodeTypeTri(String codeTypeTri) {
    this.codeTypeTri = codeTypeTri;
  }

  /**
   * Retourne le code de type de tri.
   * 
   * @return le code de type de tri.
   */
  public String getCodeTypeTri() {
    return codeTypeTri;
  }

  /**
   * Fixer l'ordre d'affichage.
   * 
   * @param ordreAffichage
   *          l'ordre d'affichage.
   */
  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  /**
   * Retourne l'ordre d'affichage.
   * 
   * @return l'ordre d'affichage.
   */
  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de
   *          valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine parent du domaine de valeur.
   * 
   * @return le nom du domaine parent du domaine de valeur.
   */
  public String getNomParent() {
    return nomParent;
  }

  /**
   * Fixer le nom du domaine parent du domaine de valeur.
   * 
   * @param nomParent
   *          le nom du domaine parent du domaine de valeur.
   */
  public void setNomParent(String nomParent) {
    this.nomParent = nomParent;
  }

  /**
   * Obtenir la valeur du domaine parent du domaine de valeur.
   * 
   * @return la valeur du domaine parent du domaine de valeur.
   */
  public String getValeurParent() {
    return valeurParent;
  }

  /**
   * Fixer la valeur du domaine parent du domaine de valeur.
   * 
   * @param valeurParent
   *          la valeur du domaine parent du domaine de valeur.
   */
  public void setValeurParent(String valeurParent) {
    this.valeurParent = valeurParent;
  }

  /**
   * Obtenir l'identifiant unique du code d'application du domaine de valeur
   * parent.
   * 
   * @return l'identifiant unique du code d'application du domaine de valeur
   *         parent.
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  /**
   * Fixer l'identifiant unique du code d'application du domaine de valeur
   * parent.
   * 
   * @param codeApplicationParent
   *          l'identifiant unique du code d'application du domaine de valeur
   *          parent.
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  /**
   * Obtenir le code de langue parent.
   * 
   * @return Code de langue parent
   */
  public String getCodeLangueParent() {
    return codeLangueParent;
  }

  /**
   * Fixer le code de langue parent.
   * 
   * @param codeLangueParent
   *          Code de langue parent
   */
  public void setCodeLangueParent(String codeLangueParent) {
    this.codeLangueParent = codeLangueParent;
  }

  /**
   * Obtenir la date de début.
   * 
   * @return Date de début
   */
  public Date getDateDebut() {
    return dateDebut;
  }

  /**
   * Fixer la date de début.
   * 
   * @param dateDebut
   *          Date de début
   */
  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  /**
   * Obtenir la date de fin.
   * 
   * @return Date de fin
   */
  public Date getDateFin() {
    return dateFin;
  }

  /**
   * Fixer la date de fin.
   * 
   * @param dateFin
   *          Date de fin
   */
  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  /**
   * Obtenir l'association inverse du domaine valeur.
   * 
   * @return Association inverse
   */
  public ArrayList<DomaineValeur> getAssociationInverse() {
    return associationInverse;
  }

  /**
   * Fixer l'association inverse du domaine de valeur
   * 
   * @param associationInverse
   *          Association inverse
   */
  public void setAssociationInverse(ArrayList<DomaineValeur> associationInverse) {
    this.associationInverse = associationInverse;
  }

  /**
   * Obtenir si le domaine valeur est actif.
   * 
   * @return true = Actif, false = Inactif
   */
  public Boolean getActif() {
    return actif;
  }

  /**
   * Fixer si le domaine valeur est actif.
   * 
   * @param actif
   *          true = Actif, false = Inactif
   */
  public void setActif(Boolean actif) {
    this.actif = actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }


}
