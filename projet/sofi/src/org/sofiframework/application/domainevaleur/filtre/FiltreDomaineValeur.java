/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre permettant d'extraire une liste de navigation représentant une liste de valeur pour un domaine et ou une
 * valeur de domaine bien précise.
 * 
 * @version SOFI 2.0
 * @author Jean-François Brassard, Nurun inc.
 */
public class FiltreDomaineValeur extends ObjetFiltre {
  private static final long serialVersionUID = -7039139365105111425L;

  private String codeApplication;
  private String nom;
  private String valeur;
  private String description;
  private String codeLangue;

  private String nomParent;
  private String codeApplicationParent;
  private String valeurParent;
  private String codeLangueParent;

  private Date dateDebut;
  private Date dateFin;

  private Boolean actif;

  private String codeClient;

  public FiltreDomaineValeur() {
    this.ajouterRechercheAvantApresValeur(new String[] { "description" });

    this.ajouterExclureRechercheAvantApresValeur(new String[] { "codeApplication", "codeClient" });
  }

  /**
   * Fixer le code d'application dont on désire extraire une liste de domaine de valeur.
   * 
   * @param codeApplication
   *          le code d'application dont on désire extraire une liste de domaine de valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code d'application dont on désire extraire une liste de domaine de valeur.
   * 
   * @return le code d'application dont on désire extraire une liste de domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le nom du domaine dont on désire extraire une liste de domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine dont on désire extraire une liste de domaine de valeur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom du domaine dont on désire extraire une liste de domaine de valeur.
   * 
   * @return le nom du domaine dont on désire extraire une liste de domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer la valeur du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @param valeur
   *          la valeur du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Retourne la valeur du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @return la valeur du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la description la description du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @param description
   *          la description la description du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la description la description du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @return la description la description du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer le code de langue du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @param codeLangue
   *          le code de langue du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   * 
   * @return le code de langue du domaine de valeur dont on désire extraire une liste de domaine de valeur.
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Obtenir le nom du parent.
   * 
   * @return Nom du parent
   */
  public String getNomParent() {
    return nomParent;
  }

  /**
   * Fixer le critère de recherche du nom du parent.
   * 
   * @param nomParent
   *          Nom du parent
   */
  public void setNomParent(String nomParent) {
    this.nomParent = nomParent;
  }

  /**
   * Obtenir le critère de recherche du code de l'application parent.
   * 
   * @return Critère de recherche de code de l'application parent
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  /**
   * Fixer le critère de code de l'application parent.
   * 
   * @param codeApplicationParent
   *          Code de l'application parent
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  /**
   * Obtenir le critère de valeur parent.
   * 
   * @return Valeur parent
   */
  public String getValeurParent() {
    return valeurParent;
  }

  public void setValeurParent(String valeurParent) {
    this.valeurParent = valeurParent;
  }

  /**
   * Obtenir le critère de code de langue parent.
   * 
   * @return Le code de langue parent
   */
  public String getCodeLangueParent() {
    return codeLangueParent;
  }

  /**
   * Fixer le critère de code de langue parent.
   * 
   * @param codeLangueParent
   *          Code de langue parent
   */
  public void setCodeLangueParent(String codeLangueParent) {
    this.codeLangueParent = codeLangueParent;
  }

  /**
   * Obtenir le critère de date de début de la valeur de domaine.
   * 
   * @return Date de début
   */
  public Date getDateDebut() {
    return dateDebut;
  }

  /**
   * Fixer le critère de date de début de la valeur de domaine.
   * 
   * @param dateDebut
   *          Date de début de la valeur de domaine
   */
  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  /**
   * Obtenir le critère de date de fin de la valeur de domaine.
   * 
   * @return Date de fin de la valeur de domaine
   */
  public Date getDateFin() {
    return dateFin;
  }

  /**
   * Fixer le critère de date de fin de la valeur de domaine.
   * 
   * @param dateFin
   *          Date de fin de la valeur de domaine
   */
  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  /**
   * Obtenir le critère de recherche d'activité de la valeur de domaine.
   * 
   * @return true = Actif, false = Inactif
   */
  public Boolean getActif() {
    return actif;
  }

  /**
   * Fixer le critère de recherche d'activité des valeurs de domaine.
   * 
   * @param actif
   *          true = Actif, false = Inactif
   */
  public void setActif(Boolean actif) {
    this.actif = actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
