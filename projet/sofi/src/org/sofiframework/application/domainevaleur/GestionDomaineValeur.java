/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;

/**
 * Objet permettant la gestion des domaines de valeurs avec l'aide d'un service de l'infrastructure SOFI.
 * <p>
 * Correspond à un SINGLETON (Une seule dans la JVM par application).
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0
 */
public class GestionDomaineValeur extends BaseGestionService {

  /**
   * L'instance de journalisation pour cette classe
   */
  private static final Log log = LogFactory.getLog(GestionDomaineValeur.class);

  /**
   * Référence à l'instance de gestion des domaines de valeur.
   */
  private static GestionDomaineValeur gestionDomaineValeurRef = new GestionDomaineValeur();

  /**
   * Le service local assoicié au gestionnaire des domaines de valeurs.
   */
  private ServiceDomaineValeur serviceDomaineValeur = null;

  private GestionDomaineValeur() {
    super(org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur.class);
  }

  /**
   * Retourne une liste de navigation page par page pour une liste de valeurs d'un domaine.
   * 
   * @return une liste de navigation page par page pour une liste de valeurs d'un domaine.
   * @param liste
   *          la liste avec son filtre de recherche.
   */
  public ListeNavigation getListeValeur(ListeNavigation liste) {
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getObjetFiltre();

    if (filtre == null) {
      throw new SOFIException("Vous devez configurer l'objet filtre (FiltreDemandeValeur) dans la liste");
    } else {
      filtre.setCodeApplication(getCodeApplication());
    }

    if (serviceDomaineValeur != null) {
      return serviceDomaineValeur.getListeValeur(liste);
    } else {
      throw new SOFIException("Vous devez configurer le service de domaine de valeur de l'infrastructure SOFI");
    }
  }

  /**
   * Retourne une liste de valeurs pour un domaine pour une application spécifique.
   * 
   * @return une liste de valeurs pour un domaine pour une application spécifique.
   * @param codeLangue
   *          le code de langue correspondant au domaine demandé.
   * @param valeur
   *          la valeur du domaine demandé.
   * @param nom
   *          le nom de la définition du domaine demandé.
   * @param codeApplication
   *          le code application demandé.
   * @param codeClient
   *          le code du client.
   */
  private List getListeValeurParService(String codeApplication, String nom, String valeur, String codeLangue) {
    List liste = null;

    if (serviceDomaineValeur != null) {
      liste = serviceDomaineValeur.getListeValeur(codeApplication, nom, valeur, codeLangue);
    } else {
      throw new SOFIException("Vous devez configurer le service de domaine de valeur de l'infrastructure SOFI");
    }

    return liste;
  }

  /**
   * Retourne une liste de valeurs pour un domaine.
   * <p>
   * Extrait pour l'application en cours sinon extrait du code d'application commun pour l'organisation (paramètre
   * système : 'codeApplicationCommun').
   * 
   * @return une liste de valeurs pour un domaine.
   * @param codeLangue
   *          le code de langue correspondant au domaine demandé.
   * @param valeur
   *          la valeur du domaine demandé.
   * @param nom
   *          le nom de la définition du domaine demandé.
   */
  public List getListeValeur(String nom, String valeur, String codeLangue) {
    return getListeValeur(getCodeApplication(), nom, valeur, codeLangue, null, null);
  }

  /**
   * Retourne une liste de valeurs pour un domaine.
   * <p>
   * Extrait pour l'application en cours sinon extrait du code d'application commun pour l'organisation (paramètre
   * système : 'codeApplicationCommun').
   * 
   * @return une liste de valeurs pour un domaine.
   * @param codeLangue
   *          le code de langue correspondant au domaine demandé.
   * @param valeur
   *          la valeur du domaine demandé.
   * @param nom
   *          le nom de la définition du domaine demandé.
   */
  public List getListeValeur(String nom, String valeur, String codeLangue, String nomCache) {
    return getListeValeur(getCodeApplication(), nom, valeur, codeLangue, nomCache, null);
  }

  /**
   * Retourne une liste de valeurs pour un domaine.
   * <p>
   * Recherche pour le code d'application spécifié sinon on recherche pour le code d'application commun pour
   * l'organisation (paramètre système : 'codeApplicationCommun').
   * 
   * @return une liste de valeurs pour un domaine.
   * @nomCache le nom de la cache associé au domaine de valeur.
   * @param codeLangue
   *          le code de langue correspondant au domaine demandé.
   * @param valeur
   *          la valeur du domaine demandé.
   * @param nom
   *          le nom de la définition du domaine demandé.
   */
  public List getListeValeur(String codeApplication, String nom, String valeur, String codeLangue, String nomCache) {
    return getListeValeur(codeApplication, nom, valeur, codeLangue, nomCache, null);

  }

  public List getListeValeur(String codeApplication, String nom, String valeur, String codeLangue, String nomCache,
      String codeClient) {
    List listeValeur = null;

    // Initialiser le nom de la cache par le nom du domaine de valeurs, si nom spécifié.
    if (nomCache == null) {
      nomCache = nom;
    }

    String nomCacheComplet = getNomCache(codeApplication, nomCache, valeur, codeLangue);

    ObjetCache objetCache = GestionCache.getInstance().getCache(nomCacheComplet, false);

    if ((objetCache == null) || objetCache.isVide()) {
      try {
        listeValeur = getListeValeurParService(codeApplication, nom, valeur, codeLangue);
      } catch (Exception e) {
        log.error(e);
      }

      if (listeValeur == null) {
        // Valider s'il existe un système commun pour les paramètres systèmes qui sont partagé dans toutes les
        // applications.
        String codeApplicationCommun = GestionParametreSysteme.getInstance().getString(
            ConstantesParametreSysteme.CODE_APPLICATION_COMMUN);

        try {
          listeValeur = getListeValeurParService(codeApplicationCommun, nom, valeur, codeLangue);
        } catch (Exception e) {
          log.error(e);
        }
      }

      if (listeValeur != null) {
        GestionCache.getInstance().ajouterListeCachePourDomaineValeur(nomCacheComplet, listeValeur);
      }
    } else {
      listeValeur = objetCache.getListeValeur();
    }

    listeValeur = filtrerParCodeClient(new ArrayList(listeValeur), codeClient);
    return listeValeur;
  }

  /**
   * Permet d'enlever de la liste en paramètre les valeurs ayant un code client différent de null et n'appartenant pas à
   * l'ascendance du client passé en paramètre.
   * 
   * @param listeValeur
   * @param codeClient
   *          le code du client
   * @return la liste filtrée.
   */
  private List filtrerParCodeClient(List listeValeur, String codeClient) {
    if (codeClient != null && listeValeur != null) {
      for (Iterator i = listeValeur.iterator(); i.hasNext();) {
        String codeClientDomaineValeur = ((DomaineValeur) i.next()).getCodeClient();
        if (codeClientDomaineValeur != null
            && !GestionSecurite.getInstance().isClientAscendant(codeClientDomaineValeur, codeClient)) {
          i.remove();
        }
      }
    }
    return listeValeur;
  }

  /**
   * Retourne une instance de domaine de valeur pour une valeur et un code de langue.
   * 
   * @return une instance de domaine de valeur pour une valeur et un code de langue.
   * @param codeLangue
   *          le code de langue.
   * @param valeur
   *          la valeur du domaine.
   * @param nom
   *          le nom du domaine.
   */
  public DomaineValeur getDomaineValeur(String nom, String valeur, String codeLangue) {
    return getDomaineValeur(nom, valeur, codeLangue, null);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public DomaineValeur getDomaineValeur(String nom, String valeur, String codeLangue, String codeClient) {
    
    DomaineValeur domaineValeur = getDomaineValeur(getCodeApplication(), nom, valeur, codeLangue, codeClient);
    
    String codeApplicationCommun = GestionParametreSysteme
        .getInstance().getString(
            ConstantesParametreSysteme.CODE_APPLICATION_COMMUN);
    
    if (codeApplicationCommun != null) {
      HashSet listeCommun = GestionParametreSysteme.getInstance().getListeValeurAvecChaineMultiple(codeApplicationCommun
          .toString());

      Iterator iterateurApplicationCommun = listeCommun.iterator();

      while (iterateurApplicationCommun.hasNext() && domaineValeur == null) {
        String codeApplication = (String) iterateurApplicationCommun.next();
        domaineValeur = getDomaineValeur(codeApplication, nom, valeur, codeLangue, codeClient); 
      }
      
    }
    
    if (domaineValeur == null) {
      throw new SOFIException(
          "Avant d'accéder au domaine de valeur, vous devez tout d'abord charger la liste du domaine de valeur suivant : "
              + nom);
    }
    
    return domaineValeur;
  }
  
  public DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue, String codeClient) {
    if (serviceDomaineValeur != null) {
      // Valider tout d'abord que la liste de valeurs n'est pas en cache.
      DomaineValeur domaineValeur = (DomaineValeur) GestionCache.getInstance()
          .getCache(getNomCache(null, nom, valeur, codeLangue)).get(valeur);

      if (domaineValeur == null) {
        domaineValeur = serviceDomaineValeur
            .getDomaineValeur(codeApplication, nom, valeur, codeLangue, codeClient);
      }

      if (domaineValeur != null) {
        List listeDomaineValeurEnfant = filtrerParCodeClient(domaineValeur.getListeDomaineValeurEnfant(), codeClient);
        if (listeDomaineValeurEnfant != null) {
          domaineValeur.setListeDomaineValeurEnfant(new ArrayList(listeDomaineValeurEnfant));
        }
        return domaineValeur;
      } else {
        return null;
      }

    } else {
      throw new SOFIException("Vous devez configurer le service de domaine de valeur de l'infrastructure SOFI");
    }
  }

  private String getNomCache(String codeApplication, String nom, String valeur, String codeLangue) {
    StringBuilder nomCompletCache = new StringBuilder();

    if ((codeApplication != null) && (getCodeApplication() != null) && !codeApplication.equals(getCodeApplication())) {
      nomCompletCache.append(codeApplication);
      nomCompletCache.append(".");
    }

    nomCompletCache.append(nom);

    if (valeur != null) {
      nomCompletCache.append(".");
      nomCompletCache.append(valeur);
    }

    if (codeLangue != null) {
      nomCompletCache.append(".");
      nomCompletCache.append(codeLangue);
    }

    return nomCompletCache.toString();
  }

  /**
   * Retourne le code d'application en cours.
   * 
   * @return le code d'application en cours.
   */
  public String getCodeApplication() {
    return GestionSecurite.getInstance().getCodeApplication();
  }

  /**
   * Obtenir l'instance unique de gestion des domaines de valeurs.
   * 
   * @return l'instance unique de gestion des domaines de valeurs.
   */
  public static GestionDomaineValeur getInstance() {
    return gestionDomaineValeurRef;
  }

  /**
   * Est-ce que le service des domaines de valeurs est configuré dans le gestionnaire?
   * 
   * @return true si le service des domaines de valeurs est configuré dans le gestionnaire.
   */
  public boolean isServiceDomaineValeurConfigure() {
    return this.serviceDomaineValeur != null;
  }

  /**
   * Retourne le service de domaine de valeur associé au gestionnaire.
   * 
   * @return le service de domaine de valeur associé au gestionnaire.
   */
  public ServiceDomaineValeur getServiceDomaineValeur() {
    return serviceDomaineValeur;
  }

  /**
   * Fixer le service de domaine de valeur au gestionnaire.
   * 
   * @param serviceDomaineValeur
   *          le service de domaine de valeur au gestionnaire.
   */
  public void setServiceDomaineValeur(ServiceDomaineValeur serviceDomaineValeur) {
    this.serviceDomaineValeur = serviceDomaineValeur;
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceDomaineValeur((ServiceDomaineValeur) service);
  }

}
