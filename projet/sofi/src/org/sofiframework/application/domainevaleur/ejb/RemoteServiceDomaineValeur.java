/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur.ejb;

import org.sofiframework.modele.ejb.exception.ServiceException;


/**
 * Interface à distance permettant d'accéder à un service distant permettant
 * la gestion des domaines de valeurs
 * <p>
 * Le distance implémenté doit donc être basé sur cette interface.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 2.0
 */
public interface RemoteServiceDomaineValeur extends javax.ejb.EJBObject {
  public org.sofiframework.composantweb.liste.ListeNavigation getListeValeur(
      org.sofiframework.composantweb.liste.ListeNavigation liste)
          throws ServiceException, java.rmi.RemoteException;

  public java.util.List getListeValeur(String codeApplication, String nom,
      String valeur, String codeLangue)
          throws ServiceException, java.rmi.RemoteException;

  public org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur getDomaineValeur(
      String codeApplication, String nom, String valeur, String codeLangue)
          throws ServiceException, java.rmi.RemoteException;
}
