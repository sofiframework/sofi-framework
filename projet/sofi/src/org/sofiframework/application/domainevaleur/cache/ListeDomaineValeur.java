/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.domainevaleur.cache;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.exception.SOFIException;

/**
 * Objet cache qui est chargé avec l'aide du service de domaine de valeur.
 * <p>
 * 
 * @author Jean-François Brassard
 * @version SOFI 2.0.1
 */
public class ListeDomaineValeur extends ObjetCache {

  private static final long serialVersionUID = -5283363228053074254L;

  @SuppressWarnings("unchecked")
  @Override
  public void chargerDonnees() {
    if (getContexte().getNomDomaineValeur() != null) {

      String nomDomaine = getContexte().getNomDomaineValeur();
      String valeurDomaine = getContexte().getValeurDomaineValeur();
      String nom = getContexte().getNom();

      List<DomaineValeur> liste = GestionDomaineValeur.getInstance().getListeValeur(nomDomaine, valeurDomaine, null,
          nom);

      // On note les inactifs.
      Date maintenant = new Date();
      if (liste != null && !liste.isEmpty()) {
        for (DomaineValeur domaineValeur : liste) {

          Date dateDebut = domaineValeur.getDateDebut();
          Date dateFin = domaineValeur.getDateFin();
          Boolean apresDateFin = dateFin != null && maintenant.after(dateFin);
          Boolean avantDateDebut = dateDebut != null && maintenant.before(domaineValeur.getDateDebut());

          if (avantDateDebut || apresDateFin) {
            ajouterValeurInactive(domaineValeur.getValeur());
          } else {
            retirerValeurInactive(domaineValeur.getValeur());
          }
        }
      }
    } else {
      throw new SOFIException(
          "Le paramètre 'nomDomaineValeur' doit être associé à la définition de la cache suivante : "
              + getContexte().getNom());
    }
  }

  @Override
  public void chargerDonnees(Object sousCache) {
    String codeDomaineParent = (String) sousCache;
    if (getContexte().getNomDomaineValeur() != null) {
      GestionDomaineValeur.getInstance().getListeValeur(this.getContexte().getNomDomaineValeur(), codeDomaineParent,
          null, getContexte().getNom());
    } else {
      throw new SOFIException(
          "Le paramètre 'nomDomaineValeur' doit être associé à la définition de la cache suivante : "
              + getContexte().getNom());
    }
  }

}
