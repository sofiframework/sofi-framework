package org.sofiframework.application.tacheplanifiee.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.quartz.Calendar;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.DriverDelegate;
import org.sofiframework.application.tacheplanifiee.SemaphoreImpl;
import org.sofiframework.application.tacheplanifiee.utils.CleSerialisable;
import org.sofiframework.application.tacheplanifiee.utils.TriggerStatutSerialisable;
import org.sofiframework.modele.exception.OrdonnanceurNotFoundException;


/**
 * Interface reprenant les méthodes de l'interface {@link DriverDelegate} de
 * Quartz *
 */
public interface ServiceTachePlanifiee {

  /**
   * Permet de demander un verrou BD.
   * Méthode utilisée par {@link SemaphoreImpl}.
   * @param nomVerrou
   */
  void demanderVerrou(Long schedulerId, String nomVerrou);

  /**
   * Si aucune exception n'est générée, le verrou a été relâché correctement
   * Méthode utilisée par {@link SemaphoreImpl}.
   * @param codeApplication
   * @param codeScheduler
   * @param nomVerrou
   */
  void relacherVerrou(Long schedulerId, String nomVerrou);

  /**
   * Permet de vérifier si l'ordonnanceur (identifié par le codeApplication et le codeScheduler) n'est
   * pas déjà avec le statut "Démarré" en base de données. Si c'est le cas, cela peut venir de 2 situations :
   * <ul>
   *   <li>l'application où l'ordonnanceur s'exécute n'a pas été arrêtée correctement ; le statut de l'ordonnanceur
   *   n'a donc pas pu être mis à jour</li>
   *   <li>l'ordonnanceur s'exécute sur 2 machines différentes (mode clustering
   *   http://www.quartz-scheduler.org/docs/configuration/ConfigJDBCJobStoreClustering.html). Il est alors nécessaire
   *   de configurer correctement les propriétés Quartz dans le fichier tache_asynchrone.xml.
   * </ul>
   * Si le statut de l'ordonnanceur est à "Démarré" en BD, retourne true ; false sinon.
   * @param codeApplication
   * @param codeScheduler
   * @param nomHote
   * @return
   */
  Boolean verifierOrdonnanceurStatutDemarre(Long schedulerId, String nomHote);

  /**
   * @param codeApplication
   * @param codeScheduler
   * @param nomHote
   */
  void signalerOrdonnanceurDemarre(Long schedulerId, String nomHote);

  /**
   * @param codeApplication
   * @param codeScheduler
   * @param nomHote
   */
  void signalerOrdonnanceurArrete(Long schedulerId, String nomHote);

  /**
   * Permet d'obtenir l'identifiant interne (BD) d'un ordonnanceur à partir d'un code application
   * et d'un code scheduler
   * @param codeApplication
   * @param codeScheduler
   * @return un objet de type {@link Long}
   * @throws OrdonnanceurNotFoundException une exception est générée si l'ordonnanceur n'existe pas en BD
   */
  Long getSchedulerId(String codeApplication, String codeScheduler) throws OrdonnanceurNotFoundException;

  // Méthodes correspondant à l'interface DriverDelegate de Quartz
  ////////////////////////////////////////////////////////////////
  Integer updateTriggerStatesFromOtherStates(Long schedulerId, String nouvState, String ancState1,
      String ancState2);

  CleSerialisable[] selectMisfiredTriggers(Long schedulerId,
      Long date);

  CleSerialisable[] selectTriggersInState(Long schedulerId,
      String etat);

  CleSerialisable[] selectMisfiredTriggersInState(Long schedulerId, String etat,Long date);

  List selectMisfiredTriggersInStates(Long schedulerId, String etat1, String etat2, Long date);

  Integer countMisfiredTriggersInStates(Long schedulerId, String etat1, String etat2, Long date)
      throws SQLException;

  CleSerialisable[] selectMisfiredTriggersInGroupInState(Long schedulerId, String groupe, String etat, Long date);

  Trigger[] selectTriggersForRecoveringJobs(Long schedulerId, String instanceId, Boolean useProperties);

  Integer deleteFiredTriggers(Long schedulerId);

  Integer deleteFiredTriggers(Long schedulerId,
      String instanceId);

  Integer insertJobDetail(Long schedulerId,
      JobDetail job, Boolean useProperties);

  Integer updateJobDetail(Long schedulerId,
      JobDetail job, Boolean useProperties);

  Integer updateJobData(Long schedulerId,
      JobDetail job, Boolean useProperties);

  Integer insertJobListener(Long schedulerId,
      JobDetail job, String listener);

  CleSerialisable[] selectTriggerNamesForJob(Long schedulerId,
      String jobName, String groupName);

  Integer deleteJobListeners(Long schedulerId,
      String jobName, String groupName);

  Integer deleteJobDetail(Long schedulerId,
      String jobName, String groupName);

  Boolean isJobStateful(Long schedulerId,
      String jobName, String groupName);

  Boolean jobExists(Long schedulerId,
      String jobName, String groupName);

  String[] selectJobListeners(Long schedulerId,
      String jobName, String groupName);

  JobDetail selectJobDetail(Long schedulerId,
      String jobName, String groupName, Boolean useProperties);

  Integer selectNumJobs(Long schedulerId);

  String[] selectJobGroups(Long schedulerId);

  String[] selectJobsInGroup(Long schedulerId,
      String groupName);

  Integer insertTrigger(Long schedulerId,
      Trigger trigger, String state, JobDetail jobDetail);

  Integer insertTriggerListener(Long schedulerId,
      Trigger trigger, String listener);

  Integer insertSimpleTrigger(Long schedulerId,
      SimpleTrigger trigger);

  Integer insertCronTrigger(Long schedulerId,
      CronTrigger trigger);

  Integer insertBlobTrigger(Long schedulerId,
      Trigger trigger);

  Integer updateTrigger(Long schedulerId,
      Trigger trigger, String etat, JobDetail jobDetail);

  Integer updateSimpleTrigger(Long schedulerId,
      SimpleTrigger trigger);

  Boolean triggerExists(Long schedulerId,
      String triggerName, String groupName, Boolean useProperties);

  Integer updateTriggerState(Long schedulerId,
      String triggerName, String groupName, String state, Boolean useProperties);

  Integer updateTriggerStateFromOtherStates(Long schedulerId, String triggerName, String groupName,
      String newState, String oldState1, String oldState2, String oldState3);

  Integer updateTriggerStateFromOtherStatesBeforeTime(Long schedulerId, String newState, String oldState1,
      String oldState2, Long time);

  Integer updateTriggerStateFromOtherState(Long schedulerId, String triggerName, String groupName,
      String newState, String oldState);

  Integer updateTriggerStatesForJob(Long schedulerId,
      String jobName, String groupName, String state);

  Integer updateTriggerStatesForJobFromOtherState(Long schedulerId, String jobName, String groupName, String state,
      String oldState);

  Integer updateTriggerGroupStateFromOtherState(Long schedulerId, String groupName, String newState, String oldState);

  Integer deleteSimpleTrigger(Long schedulerId,
      String triggerName, String groupName);

  Integer deleteCronTrigger(Long schedulerId,
      String triggerName, String groupName);

  Integer deleteBlobTrigger(Long schedulerId,
      String triggerName, String groupName);

  Integer deleteTrigger(Long schedulerId,
      String triggerName, String groupName);

  JobDetail selectJobForTrigger(Long schedulerId,
      String triggerName, String groupName, Boolean useProperties);

  Trigger[] selectTriggersForJob(Long schedulerId,
      String jobName, String groupName, Boolean useProperties);

  Trigger[] selectTriggersForCalendar(Long schedulerId, String calName, Boolean useProperties);

  List selectStatefulJobsOfTriggerGroup(Long schedulerId, String groupName);

  String selectTriggerState(Long schedulerId,
      String triggerName, String groupName);

  Trigger selectTrigger(Long schedulerId,
      String triggerName, String groupName, Boolean useProperties);

  JobDataMap selectTriggerJobDataMap(Long schedulerId, String trigName, String trigGroup,
      Boolean isProperties);

  Integer selectNumTriggers(Long schedulerId);

  String[] selectTriggerGroups(Long schedulerId);

  TriggerStatutSerialisable selectTriggerStatus(Long schedulerId, String triggerName, String groupName);

  Integer insertPausedTriggerGroup(Long schedulerId,
      String groupName);

  Integer deletePausedTriggerGroup(Long schedulerId,
      String groupName);

  Integer deleteAllPausedTriggerGroups(Long schedulerId);

  Boolean isTriggerGroupPaused(Long schedulerId,
      String groupName);

  Boolean isExistingTriggerGroup(Long schedulerId,
      String groupName);

  // ---------------------------------------------------------------------------
  // calendars
  // ---------------------------------------------------------------------------

  Integer insertCalendar(Long schedulerId,
      String calendarName, Calendar calendar);

  Integer updateCalendar(Long schedulerId,
      String calendarName, Calendar calendar);

  Boolean calendarExists(Long schedulerId,
      String calendarName);

  Calendar selectCalendar(Long schedulerId,
      String calendarName);

  Boolean calendarIsReferenced(Long schedulerId,
      String calendarName);

  Integer deleteCalendar(Long schedulerId,
      String calendarName);

  Integer selectNumCalendars(Long schedulerId);

  String[] selectCalendars(Long schedulerId);

  // ---------------------------------------------------------------------------
  // trigger firing
  // ---------------------------------------------------------------------------
  Long selectNextFireTime(Long schedulerId);

  CleSerialisable selectTriggerForFireTime(Long schedulerId, Long fireTime);

  List selectTriggerToAcquire(Long schedulerId, Long noLaterThan, Long noEarlierThan);

  Integer insertFiredTrigger(Long schedulerId,String instanceId, Trigger trigger,String state, JobDetail job);

  List selectFiredTriggerRecords(Long schedulerId, String triggerName,String groupName);

  List selectFiredTriggerRecordsByJob(Long schedulerId, String jobName,String groupName);

  List selectInstancesFiredTriggerRecords(Long schedulerId,String instanceName) ;

  Set selectFiredTriggerInstanceNames(Long schedulerId);

  Integer deleteFiredTrigger(Long schedulerId, String entryId);

  Integer selectJobExecutionCount(Long schedulerId, String jobName,String jobGroup);

  Integer deleteVolatileFiredTriggers(Long schedulerId);

  Integer insertSchedulerState(Long schedulerId, String instanceId, Long checkIntegerime, Long Integererval);

  Integer deleteSchedulerState(Long schedulerId, String instanceId);

  Integer updateSchedulerState(Long schedulerId, String instanceId, Long checkIntegerime);

  List selectSchedulerStateRecords(Long schedulerId, String instanceId);

  CleSerialisable[] selectVolatileTriggers(Long schedulerId);

  CleSerialisable[] selectVolatileJobs(Long schedulerId);

  Set selectPausedTriggerGroups(Long schedulerId);

  Integer deleteTriggerListeners(Long schedulerId, String triggerName, String groupName);

  Integer selectNumTriggersForJob(Long schedulerId, String jobName, String groupName);

  String[] selectTriggerListeners(Long schedulerId, String triggerName,String groupName);

  String[] selectTriggersInGroup(Long schedulerId, String groupName);

  Integer updateBlobTrigger(Long schedulerId, Trigger trigger);

  Integer updateCronTrigger(Long schedulerId, CronTrigger trigger);

  Integer updateTriggerGroupStateFromOtherStates(Long schedulerId,
      String groupName, String newState, String oldState1,String oldState2, String oldState3);
}
