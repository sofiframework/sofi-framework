/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.tacheplanifiee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.quartz.Calendar;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.DriverDelegate;
import org.quartz.spi.ClassLoadHelper;
import org.quartz.utils.Key;
import org.quartz.utils.TriggerStatus;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.utils.CleSerialisable;
import org.sofiframework.application.tacheplanifiee.utils.TriggerStatutSerialisable;


/**
 * Implémentation de l'interface {@link DriverDelegate} de Quartz
 * permettant de faire le lien entre un scheduler Quartz et l'infrastructure SOFI.
 * 
 * 
 * @author rmercier
 */
public class DriverDelegateImpl implements DriverDelegate {

  private Long schedulerId;
  private String instanceId;
  private Boolean useProperties;

  private ServiceTachePlanifiee service;

  public DriverDelegateImpl(Long schedulerId, String instanceId, Boolean useProperties) {
    this.schedulerId = schedulerId;
    this.instanceId = instanceId;
    this.useProperties = useProperties;
    this.service = GestionTachePlanifiee.getInstance().getService();
  }

  @Override
  public boolean calendarExists(Connection conn, String calendarName)
      throws SQLException {
    return service.calendarExists(schedulerId, calendarName).booleanValue();
  }

  @Override
  public boolean calendarIsReferenced(Connection conn, String calendarName)
      throws SQLException {
    return service.calendarIsReferenced(schedulerId, calendarName).booleanValue();
  }

  @Override
  public int countMisfiredTriggersInStates(Connection conn, String state1,
      String state2, long ts) throws SQLException {
    return service.countMisfiredTriggersInStates(schedulerId, state1, state2, new Long(ts)).intValue();
  }

  @Override
  public int deleteAllPausedTriggerGroups(Connection conn) throws SQLException {
    return service.deleteAllPausedTriggerGroups(schedulerId).intValue();
  }

  @Override
  public int deleteBlobTrigger(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.deleteBlobTrigger(schedulerId, triggerName, groupName).intValue();
  }

  @Override
  public int deleteCalendar(Connection conn, String calendarName)
      throws SQLException {
    return service.deleteCalendar(schedulerId, calendarName).intValue();
  }

  @Override
  public int deleteCronTrigger(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.deleteCronTrigger(schedulerId, triggerName, groupName).intValue();
  }

  @Override
  public int deleteFiredTrigger(Connection conn, String entryId)
      throws SQLException {
    return service.deleteFiredTrigger(schedulerId, entryId).intValue();
  }

  @Override
  public int deleteFiredTriggers(Connection conn) throws SQLException {
    return service.deleteFiredTriggers(schedulerId).intValue();
  }

  @Override
  public int deleteFiredTriggers(Connection conn, String instanceId)
      throws SQLException {
    return service.deleteFiredTriggers(schedulerId,instanceId).intValue();
  }

  @Override
  public int deleteJobDetail(Connection conn, String jobName, String groupName)
      throws SQLException {
    return service.deleteJobDetail(schedulerId, jobName, groupName).intValue();
  }

  @Override
  public int deleteJobListeners(Connection conn, String jobName,
      String groupName) throws SQLException {
    return service.deleteJobListeners(schedulerId, jobName, groupName).intValue();
  }

  @Override
  public int deletePausedTriggerGroup(Connection conn, String groupName)
      throws SQLException {
    return service.deletePausedTriggerGroup(schedulerId, groupName).intValue();
  }

  @Override
  public int deleteSchedulerState(Connection conn, String instanceId)
      throws SQLException {
    return service.deleteSchedulerState(schedulerId, instanceId).intValue();
  }

  @Override
  public int deleteSimpleTrigger(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.deleteSimpleTrigger(schedulerId, triggerName, groupName).intValue();
  }

  @Override
  public int deleteTrigger(Connection conn, String triggerName, String groupName)
      throws SQLException {
    return service.deleteTrigger(schedulerId, triggerName, groupName).intValue();
  }

  @Override
  public int deleteTriggerListeners(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.deleteTriggerListeners(schedulerId, triggerName, groupName).intValue();
  }

  @Override
  public int deleteVolatileFiredTriggers(Connection conn) throws SQLException {
    return service.deleteVolatileFiredTriggers(schedulerId).intValue();
  }

  @Override
  public int insertBlobTrigger(Connection conn, Trigger trigger)
      throws SQLException, IOException {
    return service.insertBlobTrigger(schedulerId, trigger).intValue();
  }

  @Override
  public int insertCalendar(Connection conn, String calendarName,
      Calendar calendar) throws IOException, SQLException {
    return service.insertCalendar(schedulerId, calendarName, calendar).intValue();
  }

  @Override
  public int insertCronTrigger(Connection conn, CronTrigger trigger)
      throws SQLException {
    return service.insertCronTrigger(schedulerId, trigger).intValue();
  }

  @Override
  public int insertFiredTrigger(Connection conn, Trigger trigger, String state,
      JobDetail jobDetail) throws SQLException {
    return service.insertFiredTrigger(schedulerId, instanceId, trigger, state, jobDetail).intValue();
  }

  @Override
  public int insertJobDetail(Connection conn, JobDetail job)
      throws IOException, SQLException {
    return service.insertJobDetail(schedulerId, job, useProperties).intValue();
  }

  @Override
  public int insertJobListener(Connection conn, JobDetail job, String listener)
      throws SQLException {
    return service.insertJobListener(schedulerId, job, listener).intValue();
  }

  @Override
  public int insertPausedTriggerGroup(Connection conn, String groupName)
      throws SQLException {
    return service.insertPausedTriggerGroup(schedulerId, groupName).intValue();
  }

  @Override
  public int insertSchedulerState(Connection conn, String instanceId,
      long checkInTime, long interval) throws SQLException {
    return service.insertSchedulerState(schedulerId, instanceId, new Long(checkInTime), new Long(interval)).intValue();
  }

  @Override
  public int insertSimpleTrigger(Connection conn, SimpleTrigger trigger)
      throws SQLException {
    return service.insertSimpleTrigger(schedulerId, trigger).intValue();
  }

  @Override
  public int insertTrigger(Connection conn, Trigger trigger, String state,
      JobDetail jobDetail) throws SQLException, IOException {
    return service.insertTrigger(schedulerId, trigger, state, jobDetail).intValue();
  }

  @Override
  public int insertTriggerListener(Connection conn, Trigger trigger,
      String listener) throws SQLException {
    return service.insertTriggerListener(schedulerId, trigger, listener).intValue();
  }

  @Override
  public boolean isExistingTriggerGroup(Connection conn, String groupName)
      throws SQLException {
    return service.isExistingTriggerGroup(schedulerId, groupName).booleanValue();
  }

  @Override
  public boolean isJobStateful(Connection conn, String jobName, String groupName)
      throws SQLException {
    return service.isJobStateful(schedulerId, jobName, groupName).booleanValue();
  }

  @Override
  public boolean isTriggerGroupPaused(Connection conn, String groupName)
      throws SQLException {
    return service.isTriggerGroupPaused(schedulerId, groupName).booleanValue();
  }

  @Override
  public boolean jobExists(Connection conn, String jobName, String groupName)
      throws SQLException {
    return service.jobExists(schedulerId, jobName, groupName).booleanValue();
  }

  @Override
  public Calendar selectCalendar(Connection conn, String calendarName)
      throws ClassNotFoundException, IOException, SQLException {
    return service.selectCalendar(schedulerId, calendarName);
  }

  @Override
  public String[] selectCalendars(Connection conn) throws SQLException {
    return service.selectCalendars(schedulerId);
  }

  @Override
  public Set selectFiredTriggerInstanceNames(Connection conn)
      throws SQLException {
    return service.selectFiredTriggerInstanceNames(schedulerId);
  }

  @Override
  public List selectFiredTriggerRecords(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.selectFiredTriggerRecords(schedulerId, triggerName, groupName);
  }

  @Override
  public List selectFiredTriggerRecordsByJob(Connection conn, String jobName,
      String groupName) throws SQLException {
    return service.selectFiredTriggerRecordsByJob(schedulerId, jobName, groupName);
  }

  @Override
  public List selectInstancesFiredTriggerRecords(Connection conn,
      String instanceName) throws SQLException {
    return service.selectInstancesFiredTriggerRecords(schedulerId, instanceName);
  }

  @Override
  public JobDetail selectJobDetail(Connection conn, String jobName,
      String groupName, ClassLoadHelper loadHelper)
          throws ClassNotFoundException, IOException, SQLException {
    JobDetail jobDetail = service.selectJobDetail(schedulerId, jobName, groupName, useProperties);
    /* Correction bug (Mercier Rémi 20110720)
     * La classe qui sera lancée par
     * l'ordonnanceur doit être instanciée ici (au niveau de l'application
     * cliente)
     */
    if(jobDetail instanceof JobDetailImpl) {
      String nomClasse = ((JobDetailImpl) jobDetail).getNomClasse();
      ClassLoader classeLoader = getClass().getClassLoader();
      jobDetail.setJobClass(classeLoader.loadClass(nomClasse));
    }
    /* Fin correction bug (Mercier Rémi 20110720) */
    return jobDetail;
  }

  @Override
  public int selectJobExecutionCount(Connection conn, String jobName,
      String jobGroup) throws SQLException {
    return service.selectJobExecutionCount(schedulerId, jobName, jobGroup).intValue();
  }

  @Override
  public JobDetail selectJobForTrigger(Connection conn, String triggerName,
      String groupName, ClassLoadHelper loadHelper)
          throws ClassNotFoundException, SQLException {
    JobDetail jobDetail = service.selectJobForTrigger(schedulerId, triggerName, groupName, useProperties);
    /* Correction bug (Mercier Rémi 20110720)
     * La classe qui sera lancée par
     * l'ordonnanceur doit être instanciée ici (au niveau de l'application
     * cliente)
     */
    if(jobDetail instanceof JobDetailImpl) {
      String nomClasse = ((JobDetailImpl) jobDetail).getNomClasse();
      ClassLoader classeLoader = getClass().getClassLoader();
      jobDetail.setJobClass(classeLoader.loadClass(nomClasse));
    }
    /* Fin correction bug (Mercier Rémi 20110720) */
    return jobDetail;
  }

  @Override
  public String[] selectJobGroups(Connection conn) throws SQLException {
    return service.selectJobGroups(schedulerId);
  }

  @Override
  public String[] selectJobListeners(Connection conn, String jobName,
      String groupName) throws SQLException {
    return service.selectJobListeners(schedulerId, jobName, groupName);
  }

  @Override
  public String[] selectJobsInGroup(Connection conn, String groupName)
      throws SQLException {
    return service.selectJobsInGroup(schedulerId, groupName);
  }

  @Override
  public Key[] selectMisfiredTriggers(Connection conn, long ts)
      throws SQLException {
    return getKeyArray(service.selectMisfiredTriggers(schedulerId, new Long(ts)));
  }

  @Override
  public Key[] selectMisfiredTriggersInGroupInState(Connection conn,
      String groupName, String state, long ts) throws SQLException {
    return getKeyArray(service.selectMisfiredTriggersInGroupInState(schedulerId, groupName, state, new Long(ts)));
  }

  @Override
  public Key[] selectMisfiredTriggersInState(Connection conn, String state,
      long ts) throws SQLException {
    return getKeyArray(service.selectMisfiredTriggersInState(schedulerId, state, new Long(ts)));
  }


  @Override
  public boolean selectMisfiredTriggersInStates(Connection conn, String state1,
      String state2, long ts, int count, List resultList) throws SQLException {

    boolean result = false;
    List cleSerialisables = service.selectMisfiredTriggersInStates(schedulerId, state1, state2, new Long(ts));

    if(cleSerialisables.size() > 0){
      boolean isLimite = false;

      for(Iterator i = cleSerialisables.iterator(); i.hasNext();){
        CleSerialisable cleSerialisable = (CleSerialisable) i.next();
        if(!isLimite){
          if (resultList.size() == count) {
            isLimite = true;
          } else {
            resultList.add(new Key(cleSerialisable.getNom(), cleSerialisable.getGroupe()));
          }
        }
      }
      result = isLimite;
    }
    return result;
  }

  @Override
  public long selectNextFireTime(Connection conn) throws SQLException {
    return service.selectNextFireTime(schedulerId).longValue();
  }

  @Override
  public int selectNumCalendars(Connection conn) throws SQLException {
    return service.selectNumCalendars(schedulerId).intValue();
  }

  @Override
  public int selectNumJobs(Connection conn) throws SQLException {
    return service.selectNumJobs(schedulerId).intValue();
  }

  @Override
  public int selectNumTriggers(Connection conn) throws SQLException {
    return service.selectNumTriggers(schedulerId).intValue();
  }

  @Override
  public int selectNumTriggersForJob(Connection conn, String jobName,
      String groupName) throws SQLException {
    return service.selectNumTriggersForJob(schedulerId,  jobName, groupName).intValue();
  }

  @Override
  public Set selectPausedTriggerGroups(Connection conn) throws SQLException {
    return service.selectPausedTriggerGroups(schedulerId);
  }

  @Override
  public List selectSchedulerStateRecords(Connection conn, String instanceId)
      throws SQLException {
    return service.selectSchedulerStateRecords(schedulerId, instanceId);
  }

  @Override
  public List selectStatefulJobsOfTriggerGroup(Connection conn, String groupName)
      throws SQLException {
    return service.selectStatefulJobsOfTriggerGroup(schedulerId, groupName);
  }

  @Override
  public Trigger selectTrigger(Connection conn, String triggerName,
      String groupName) throws SQLException, ClassNotFoundException,
      IOException {
    return service.selectTrigger(schedulerId, triggerName, groupName, useProperties);
  }

  @Override
  public Key selectTriggerForFireTime(Connection conn, long fireTime)
      throws SQLException {
    return getKey(service.selectTriggerForFireTime(schedulerId,new Long(fireTime)));
  }

  @Override
  public String[] selectTriggerGroups(Connection conn) throws SQLException {
    return service.selectTriggerGroups(schedulerId);
  }

  @Override
  public JobDataMap selectTriggerJobDataMap(Connection conn,
      String triggerName, String groupName) throws SQLException,
      ClassNotFoundException, IOException {
    return service.selectTriggerJobDataMap(schedulerId, triggerName, groupName, useProperties);
  }

  @Override
  public String[] selectTriggerListeners(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.selectTriggerListeners(schedulerId,triggerName,groupName);
  }

  @Override
  public Key[] selectTriggerNamesForJob(Connection conn, String jobName,
      String groupName) throws SQLException {
    return getKeyArray(service.selectTriggerNamesForJob(schedulerId, jobName, groupName));
  }

  @Override
  public String selectTriggerState(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.selectTriggerState(schedulerId, triggerName, groupName);
  }

  @Override
  public TriggerStatus selectTriggerStatus(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return getTriggerStatut(service.selectTriggerStatus(schedulerId, triggerName, groupName));
  }

  @Override
  public List selectTriggerToAcquire(Connection conn, long noLaterThan,
      long noEarlierThan) throws SQLException {
    return getQuartzKeyList(service.selectTriggerToAcquire(schedulerId, new Long(noLaterThan), new Long(noEarlierThan)));
  }

  @Override
  public Trigger[] selectTriggersForCalendar(Connection conn, String calName)
      throws SQLException, ClassNotFoundException, IOException {
    return service.selectTriggersForCalendar(schedulerId, calName, useProperties);
  }

  @Override
  public Trigger[] selectTriggersForJob(Connection conn, String jobName,
      String groupName) throws SQLException, ClassNotFoundException,
      IOException {
    return service.selectTriggersForJob(schedulerId, jobName, groupName, useProperties);
  }

  @Override
  public Trigger[] selectTriggersForRecoveringJobs(Connection conn)
      throws SQLException, IOException, ClassNotFoundException {
    return service.selectTriggersForRecoveringJobs(schedulerId, instanceId, useProperties);
  }

  @Override
  public String[] selectTriggersInGroup(Connection conn, String groupName)
      throws SQLException {
    return service.selectTriggersInGroup(schedulerId,groupName);
  }

  @Override
  public Key[] selectTriggersInState(Connection conn, String state)
      throws SQLException {
    return getKeyArray(service.selectTriggersInState(schedulerId, state));
  }

  @Override
  public Key[] selectVolatileJobs(Connection conn) throws SQLException {
    return getKeyArray(service.selectVolatileJobs(schedulerId));
  }

  @Override
  public Key[] selectVolatileTriggers(Connection conn) throws SQLException {
    return getKeyArray(service.selectVolatileTriggers(schedulerId));
  }

  @Override
  public boolean triggerExists(Connection conn, String triggerName,
      String groupName) throws SQLException {
    return service.triggerExists(schedulerId, triggerName, groupName, useProperties).booleanValue();
  }

  @Override
  public int updateBlobTrigger(Connection conn, Trigger trigger)
      throws SQLException, IOException {
    return service.updateBlobTrigger(schedulerId,trigger).intValue();
  }

  @Override
  public int updateCalendar(Connection conn, String calendarName,
      Calendar calendar) throws IOException, SQLException {
    return service.updateCalendar(schedulerId, calendarName, calendar).intValue();
  }

  @Override
  public int updateCronTrigger(Connection conn, CronTrigger trigger)
      throws SQLException {
    return service.updateCronTrigger(schedulerId,trigger).intValue();
  }

  @Override
  public int updateJobData(Connection conn, JobDetail job) throws IOException,
  SQLException {
    return service.updateJobData(schedulerId, job, useProperties).intValue();
  }

  @Override
  public int updateJobDetail(Connection conn, JobDetail job)
      throws IOException, SQLException {
    return service.updateJobDetail(schedulerId, job, useProperties).intValue();
  }

  @Override
  public int updateSchedulerState(Connection conn, String instanceId,
      long checkInTime) throws SQLException {
    return service.updateSchedulerState(schedulerId, instanceId, new Long(checkInTime)).intValue();
  }

  @Override
  public int updateSimpleTrigger(Connection conn, SimpleTrigger trigger)
      throws SQLException {
    return service.updateSimpleTrigger(schedulerId, trigger).intValue();
  }

  @Override
  public int updateTrigger(Connection conn, Trigger trigger, String state,
      JobDetail jobDetail) throws SQLException, IOException {
    return service.updateTrigger(schedulerId, trigger, state, jobDetail).intValue();
  }

  @Override
  public int updateTriggerGroupStateFromOtherState(Connection conn,
      String groupName, String newState, String oldState) throws SQLException {
    return service.updateTriggerGroupStateFromOtherState(schedulerId, groupName, newState, oldState).intValue();
  }

  @Override
  public int updateTriggerGroupStateFromOtherStates(Connection conn,
      String groupName, String newState, String oldState1, String oldState2,
      String oldState3) throws SQLException {
    return service.updateTriggerGroupStateFromOtherStates(schedulerId, groupName, newState, oldState1, oldState2,oldState3).intValue();
  }

  @Override
  public int updateTriggerState(Connection conn, String triggerName,
      String groupName, String state) throws SQLException {
    return service.updateTriggerState(schedulerId, triggerName, groupName, state, useProperties).intValue();
  }

  @Override
  public int updateTriggerStateFromOtherState(Connection conn,
      String triggerName, String groupName, String newState, String oldState)
          throws SQLException {
    return service.updateTriggerStateFromOtherState(schedulerId, triggerName, groupName, newState, oldState).intValue();
  }

  @Override
  public int updateTriggerStateFromOtherStates(Connection conn,
      String triggerName, String groupName, String newState, String oldState1,
      String oldState2, String oldState3) throws SQLException {
    return service.updateTriggerStateFromOtherStates(schedulerId, triggerName, groupName
        , newState, oldState1, oldState2, oldState3).intValue();
  }

  @Override
  public int updateTriggerStateFromOtherStatesBeforeTime(Connection conn,
      String newState, String oldState1, String oldState2, long time)
          throws SQLException {
    return service.updateTriggerStateFromOtherStatesBeforeTime(schedulerId, newState, oldState1, oldState2, new Long(time)).intValue();
  }

  @Override
  public int updateTriggerStatesForJob(Connection conn, String jobName,
      String groupName, String state) throws SQLException {
    return service.updateTriggerStatesForJob(schedulerId, jobName, groupName, state).intValue();
  }

  @Override
  public int updateTriggerStatesForJobFromOtherState(Connection conn,
      String jobName, String groupName, String state, String oldState)
          throws SQLException {
    return service.updateTriggerStatesForJobFromOtherState(schedulerId, jobName, groupName, state, oldState).intValue();
  }

  @Override
  public int updateTriggerStatesFromOtherStates(Connection conn,
      String newState, String oldState1, String oldState2) throws SQLException {
    return service.updateTriggerStatesFromOtherStates(schedulerId, newState, oldState1, oldState2).intValue();
  }

  private Key[] getKeyArray(CleSerialisable[] cleSerialisables){
    Key[] result = null;

    if(cleSerialisables != null){
      result = new Key[cleSerialisables.length];
      for(int i=0;i< cleSerialisables.length;i++) {
        result[i] = getKey(cleSerialisables[i]);
      }
    }
    return result;
  }

  private List getQuartzKeyList(List cleSerialisables){
    List result = new ArrayList();
    for(Iterator i = cleSerialisables.iterator(); i.hasNext();) {
      CleSerialisable cle = (CleSerialisable) i.next();
      result.add(getKey(cle));
    }
    return result;
  }

  private Key getKey(CleSerialisable cleSerialisable){
    Key result = null;
    if(cleSerialisable != null) {
      result = new Key(cleSerialisable.getNom(),cleSerialisable.getGroupe());
    }
    return result;
  }

  private TriggerStatus getTriggerStatut(TriggerStatutSerialisable triggerStatutSerialisable){
    TriggerStatus result = new TriggerStatus(triggerStatutSerialisable.getFirst(),triggerStatutSerialisable.getSecond());
    result.setKey(getKey(triggerStatutSerialisable.getKey()));
    result.setJobKey(getKey(triggerStatutSerialisable.getJobKey()));
    return result;
  }

  public String getInstanceId() {
    return instanceId;
  }

  public void setInstanceId(String instanceId) {
    this.instanceId = instanceId;
  }

  public Boolean getUseProperties() {
    return useProperties;
  }

  public void setUseProperties(Boolean useProperties) {
    this.useProperties = useProperties;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

}
