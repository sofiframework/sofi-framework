/**
 * 
 */
package org.sofiframework.application.tacheplanifiee;

import java.sql.Connection;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.impl.jdbcjobstore.DBSemaphore;
import org.quartz.impl.jdbcjobstore.LockException;
import org.quartz.impl.jdbcjobstore.Semaphore;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Cette classe permet de faire un lock BD. Elle est utile au
 * {@link JobStoreImpl} pour éviter que plusieurs schéduler Quartz accédent au
 * même trigger au même moment. Cette classe s'inspire fortement de la classe
 * {@link DBSemaphore}.
 * 
 * @author rmercier
 */
public class SemaphoreImpl implements Semaphore {

  private static final Log log = LogFactory.getLog(SemaphoreImpl.class);

  ThreadLocal lockOwners = new ThreadLocal();

  /**
   * Délai d'attente du verrou en ms. Le thread va essayer d'obtenir le verrou durant ce délai. Si à la fin du délai
   * le verrou n'a toujours pas été obtenu, une exception est déclenchée.
   */
  private static final long DELAI_ATTENTE_VERROU = 5000;
  /**
   * Intervalle en ms entre 2 appel à la méthode {@link ServiceTachePlanifiee#obtenirVerrou(String, String, String)}
   */
  private static final long INTERVALLE_TENTATIVE_OBTENTION_VERROU = 200;

  /*
   * Ne doit pâs être de type Long car l'initialiseur de propriétés Quartz ne le gère pas
   */
  private long schedulerId;

  private ServiceTachePlanifiee service;

  public SemaphoreImpl() {
    this.service = GestionTachePlanifiee.getInstance().getService();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.quartz.impl.jdbcjobstore.Semaphore#isLockOwner(java.sql.Connection,
   * java.lang.String)
   */
  @Override
  public boolean isLockOwner(Connection conn, String lockName)
      throws LockException {
    lockName = lockName.intern();
    return getThreadLocks().contains(lockName);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.quartz.impl.jdbcjobstore.Semaphore#obtainLock(java.sql.Connection,
   * java.lang.String)
   */
  @Override
  public boolean obtainLock(Connection conn, String lockName)
      throws LockException {
    lockName = lockName.intern();

    if (log.isDebugEnabled()) {
      log.debug("Verrou '" + toStringVerrou(lockName) + "' demandé par : "
          + Thread.currentThread().getName());
    }

    if (!isLockOwner(null, lockName)) {
      boolean delaiDepasse = false;
      boolean verrouObtenu = false;
      long debutTraitement = System.currentTimeMillis();

      // On boucle DELAI_ATTENTE_VERROU millisecondes pour essayer d'obtenir un verrou
      while(!delaiDepasse && !verrouObtenu) {
        // Tentative d'obtention d'un verrou
        try {
          service.demanderVerrou(schedulerId, lockName);
          verrouObtenu = true;
        } catch(Exception ex) {
          if(log.isDebugEnabled()) {
            log.debug("Tentative d'obtention du verrou "
                + toStringVerrou(lockName) + " échouée : on réessaye! : " + ex.getMessage());
          }
        }

        delaiDepasse = ((System.currentTimeMillis() - debutTraitement) > DELAI_ATTENTE_VERROU);

        // Attente pour ne pas saturer l'application
        try {
          Thread.sleep(INTERVALLE_TENTATIVE_OBTENTION_VERROU);
        } catch (InterruptedException e) {
          log.error("Erreur WAIT!!!", e);
        }
      }

      if(!verrouObtenu) {
        throw new ModeleException("Le verrou "
            + toStringVerrou(lockName) + " n'a pas pu être obtenu dans le temps imparti.");
      }

      if (log.isDebugEnabled()) {
        log.debug("Verrou '" + toStringVerrou(lockName) + "' donné à : "
            + Thread.currentThread().getName());
      }

      getThreadLocks().add(lockName);
    } else if (log.isDebugEnabled()) {
      log.debug("Verrou '" + toStringVerrou(lockName) + "' déjà attribué à : "
          + Thread.currentThread().getName());
    }

    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.quartz.impl.jdbcjobstore.Semaphore#releaseLock(java.sql.Connection,
   * java.lang.String)
   */
  @Override
  public void releaseLock(Connection conn, String lockName)
      throws LockException {
    lockName = lockName.intern();

    if (isLockOwner(null, lockName)) {
      try {
        service.relacherVerrou(schedulerId, lockName);
      } catch (Exception ex) {
        if (log.isWarnEnabled()) {
          log.warn("Impossible de relâcher le verrou '"
              + toStringVerrou(lockName) + "' : " + ex.getCause());
        }
        throw new LockException("Impossible de relâcher le verrou '"
            + toStringVerrou(lockName), ex.getCause());
      }
      getThreadLocks().remove(lockName);

      if (log.isDebugEnabled()) {
        log.debug("Verrou '" + toStringVerrou(lockName) + "' relaché par : "
            + Thread.currentThread().getName());
      }

    } else if (log.isDebugEnabled()) {
      log.warn("Tentative de relâcher le verrou '" + toStringVerrou(lockName)
          + "' par : " + Thread.currentThread().getName()
          + " -- mais ce n'est pas le propriétaire!", new Exception());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.quartz.impl.jdbcjobstore.Semaphore#requiresConnection()
   */
  @Override
  public boolean requiresConnection() {
    return false;
  }

  private HashSet getThreadLocks() {
    HashSet threadLocks = (HashSet) lockOwners.get();
    if (threadLocks == null) {
      threadLocks = new HashSet();
      lockOwners.set(threadLocks);
    }
    return threadLocks;
  }

  /**
   * Utiliser pour les logs et les messages d'exception
   * @param lockName
   * @return un String au format "lockName [codeApplication, codeScheduler]"
   */
  private String toStringVerrou(String lockName) {
    return String.format("%s [%s]", new Object[]{lockName, schedulerId});
  }

  public long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(long schedulerId) {
    this.schedulerId = schedulerId;
  }

}
