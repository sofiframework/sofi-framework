package org.sofiframework.application.tacheplanifiee.job;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.sofiframework.modele.Modele;
import org.springframework.beans.factory.BeanFactory;

/**
 * Implémentation de l'interface {@link Job}. Permet de fournir aux classes filles un point d'entrée vers le modèle de
 * l'application.
 * 
 * @author rmercier
 */
public abstract class Tache implements Job {

  private static final Log log = LogFactory.getLog(Tache.class);

  /**
   * Les paramètres des tâches planifiées présents dans la map {@link JobDataMap} devrait implémenter cette interface.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  protected interface ParametreTache {
    /**
     * La clé du paramètre présent dans {@link JobDataMap}.
     * 
     * @return un String.
     */
    String getCle();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
   */
  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    long start = System.currentTimeMillis();

    if (log.isWarnEnabled()) {
      log.warn("Task " + this.getClass().getName() + " started");
    }

    try {
      this.executer(context);
    } catch (Exception ex) {
      log.error("Error during execution of " + this.getClass().getName(), ex);
      throw new JobExecutionException(ex);
    }

    long end = System.currentTimeMillis();

    if (log.isWarnEnabled()) {
      log.warn("Task " + this.getClass().getSimpleName() + " ended after " + (end - start) + " ms");
    }
  }

  /**
   * 
   * @param context
   */
  public abstract void executer(JobExecutionContext context) throws JobExecutionException;

  /**
   * Permet d'obtenir le modèle de l'application à partir du contexte du job passé en paramètre
   * 
   * @param context
   *          un objet de type {@link JobExecutionContext}
   * @param nomModele
   * @return un objet de type {@link Modele}
   */
  public Modele getModele(JobExecutionContext context, String nomModele) {
    Modele result = null;
    try {
      BeanFactory beanFactory = (BeanFactory) context.getScheduler().getContext().get("applicationContexte");
      result = (Modele) beanFactory.getBean(nomModele);
    } catch (SchedulerException e) {
      log.error("Erreur lors de la récupération du modèle " + nomModele + ".", e);
    }
    return result;
  }

  /**
   * Permet d'obtenir le bean de classe passée en paramètre à partir du contexte d'exécution du job.
   * 
   * @param context
   *          un objet de type {@link JobExecutionContext}
   * @param classe
   * @return un objet de type <code>classe</code>
   */
  protected Object getBean(JobExecutionContext context, Class<?> classe) {
    Object result = null;
    try {
      BeanFactory beanFactory = (BeanFactory) context.getScheduler().getContext().get("applicationContexte");
      result = beanFactory.getBean(classe);
    } catch (SchedulerException e) {
      log.error("Erreur lors de la récupération d'une instance de classe : " + classe.getName() + ".", e);
    }
    return result;
  }

  /**
   * Permet de récupérer la valeur du paramètre de clé <code>cle</code>.
   * 
   * @param cle
   *          la clé du paramètre dont on veut la valeur.
   * @param context
   *          le contexte d'exécution de la tâche.
   * @return la valeur du paramètre ou null si le paramètre n'Existe pas.
   * @see JobExecutionContext#getMergedJobDataMap().
   */
  protected Object getParametre(String cle, JobExecutionContext context) {
    JobDataMap data = context.getMergedJobDataMap();
    return data.get(cle);
  }

}
