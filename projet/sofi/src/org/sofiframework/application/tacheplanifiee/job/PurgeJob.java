/**
 * 
 */
package org.sofiframework.application.tacheplanifiee.job;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.StatefulJob;

/**
 * Une implémentation de l'interface {@link StatefulJob} permettant de purger récursivement le contenu
 * des répertoires listés dans les propriétés dynamiques du job.
 * Le nom de chaque propriété importe peu ; il doit juste être uniquement.
 * Si le chemin indiqué pointe vers un fichier, le fichier sera supprimé.
 * @author rmercier
 *
 */
public class PurgeJob extends TacheStateful {

  private static final Log log = LogFactory.getLog(PurgeJob.class);

  /*
   * (non-Javadoc)
   * @see org.sofiframework.application.tacheplanifiee.job.Tache#executer(org.quartz.JobExecutionContext)
   */
  @Override
  @SuppressWarnings("unchecked")
  public void executer(JobExecutionContext context) {
    if(log.isInfoEnabled()) {
      log.info("Lancement de la job de purge");
    }

    JobDataMap data = context.getMergedJobDataMap();
    Collection<String> liste = data.values();
    if(liste != null) {
      for(String chemin : liste) {
        File file = new File(chemin);
        if(file.exists()) {
          if(file.isDirectory()) {
            if(log.isDebugEnabled()) {
              log.debug("Purge du répertoire " + chemin);
            }
            try {
              FileUtils.deleteDirectory(file);
            } catch (IOException e) {
              if(log.isWarnEnabled()) {
                log.warn("Erreur dans la purge du répertoire " + chemin, e);
              }
            }
          } else {
            if(log.isDebugEnabled()) {
              log.debug("Suppression du fichier " + chemin);
            }
            file.delete();
          }
        }
      }
    } else {
      if(log.isDebugEnabled()) {
        log.debug("Aucun répertoire à purger");
      }
    }

    if(log.isInfoEnabled()) {
      log.info("Purge effectuée");
    }
  }

}
