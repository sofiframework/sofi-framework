package org.sofiframework.application.tacheplanifiee.job;

import java.util.Date;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 * Tache utilitaire permettant de "simuler" un long temps de traitement d'un job Quartz.
 * Le temps d'attente peut être spécifier via la propriété <code>tempsAttenteEnMs</code>.
 * Par defaut, il est de 20000 ms.
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class LongTraitementJob extends TacheStateful{

  public static final String PROP_TEMPS_ATTENTE = "tempsAttenteEnMs";

  private static final long TEMPS_PAR_DEFAULT = 20000;

  @Override
  public void executer(JobExecutionContext context) {
    long tempsAAttendre = TEMPS_PAR_DEFAULT;

    JobDataMap data = context.getMergedJobDataMap();
    Object tempsAttente = data.get(PROP_TEMPS_ATTENTE);
    if(tempsAttente != null) {
      tempsAAttendre = Long.valueOf(((String)tempsAttente)).longValue();
    }

    try {
      Thread.sleep(tempsAAttendre);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Date date = new Date();
    System.out.println(date + " Traitement d'une durée de " + tempsAAttendre + " ms terminé.");
  }
}
