/**
 * 
 */
package org.sofiframework.application.tacheplanifiee.job;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * Une implémentation de l'interface {@link Job} affichant dans la console sdtout le message indiqué dans le paramètre
 * PROP_MESSAGE. Si le paramètre n'est pas renseigné, affiche le message "Hello World!".
 * 
 * @author rmercier
 * 
 */
public class EchoJob extends Tache {

  public static final String PROP_MESSAGE = "message";

  private static final String MESSAGE_PAR_DEFAULT = "Hello World!";

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.application.tacheplanifiee.job.Tache#executer(org.quartz.JobExecutionContext)
   */
  @Override
  public void executer(JobExecutionContext context) {
    String messageAAfficher = MESSAGE_PAR_DEFAULT;

    Object message = getParametre(PROP_MESSAGE, context);
    if (message != null) {
      messageAAfficher = (String) message;
    }

    Date date = new Date();

    System.out.println(date + " " + messageAAfficher);
  }

}
