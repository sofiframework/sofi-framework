package org.sofiframework.application.tacheplanifiee.job;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.tacheplanifiee.utils.StatistiqueTache;

/**
 * Implémentation de l'interface {@link StatefulJob}. Stocke dans la data map du job les durées des dernières exécutions
 * de la tâche
 * 
 * @author rmercier
 * 
 */
public abstract class TacheStateful extends Tache implements StatefulJob {

  /**
   * Clé permettant d'obtenir dans la data map de la job la liste des durées d'exécution de la tache
   * 
   * @deprecated voir {@link ParametreTacheStateful}
   */
  @Deprecated
  public static final String PROP_LISTE_DUREE_EXECUTION = "_listeDureeExecutionEnMs";

  /**
   * Clé du paramètre système.
   */
  public static final String PARAMETRE_MAX_NBR_EXECUTION_ENREGISTRER = "tacheNbrMaxTempsExecEnregistrer";
  public static final Integer DEFAULT_MAX_NBR_EXECUTION_ENREGISTRER = new Integer(50);

  /**
   * Énumération des paramètres possibles de la tâche {@link TacheStateful}.
   * 
   * @author Rémi Mercier (Nurun Inc.)
   * @since 3.2
   */
  public static enum ParametreTacheStateful implements ParametreTache {
    LISTE_DUREE_EXECUTION("_listeDureeExecutionEnMs");

    private String cle;

    private ParametreTacheStateful(String cle) {
      this.cle = cle;
    }

    @Override
    public String getCle() {
      return cle;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
   */
  @SuppressWarnings("unchecked")
  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    long debutExecution = System.currentTimeMillis();

    // Afin de ne pas surenregistrer d information on fait une gestion de la liste.
    Integer tacheMaxNbrExecution = GestionParametreSysteme.getInstance().getInteger(
        PARAMETRE_MAX_NBR_EXECUTION_ENREGISTRER);
    if (tacheMaxNbrExecution == null) {
      tacheMaxNbrExecution = DEFAULT_MAX_NBR_EXECUTION_ENREGISTRER;
    }

    List<StatistiqueTache> liste = (List<StatistiqueTache>) context.getJobDetail().getJobDataMap()
        .get(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());
    if (liste == null) {
      liste = new ArrayList<StatistiqueTache>();
    } else if (liste.size() >= tacheMaxNbrExecution && tacheMaxNbrExecution > 0) {
      liste.remove(liste.size() - 1);
    }

    // on doit enlever les parametres propre au parent, pour ne pas influencer l enfant
    context.getMergedJobDataMap().remove(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());
    context.getJobDetail().getJobDataMap().remove(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());

    // Appel à l'exécution de la tâche
    // this.executer(context);
    super.execute(context);

    long finExecution = System.currentTimeMillis();

    if (!tacheMaxNbrExecution.equals(new Integer(0))) {
      liste.add(0, new StatistiqueTache(debutExecution, finExecution));
    }

    context.getJobDetail().getJobDataMap().put(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle(), liste);
  }

}
