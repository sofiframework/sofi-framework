/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.tacheplanifiee;

import org.quartz.impl.jdbcjobstore.FiredTriggerRecord;
import org.quartz.utils.Key;
import org.sofiframework.application.tacheplanifiee.utils.CleSerialisable;

/**
 * Surcharge de l'objet Quartz {@link FiredTriggerRecordImpl} pour l'encaplusation des {@link Key} qui sont non
 * sérialisables.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class FiredTriggerRecordImpl extends FiredTriggerRecord {

  /**
   * 
   */
  private static final long serialVersionUID = -6064289629531174533L;

  private CleSerialisable jobCle;
  private CleSerialisable triggerCle;

  @Override
  public Key getJobKey() {
    if (jobCle != null) {
      return new Key(jobCle.getNom(), jobCle.getGroupe());
    } else {
      return null;
    }
  }

  @Override
  public void setJobKey(Key key) {
    if (key != null) {
      jobCle = new CleSerialisable(key.getName(), key.getGroup());
    } else {
      jobCle = null;
    }
  }

  @Override
  public Key getTriggerKey() {
    if (triggerCle != null) {
      return new Key(triggerCle.getNom(), triggerCle.getGroupe());
    } else {
      return null;
    }
  }

  @Override
  public void setTriggerKey(Key key) {
    if (key != null) {
      triggerCle = new CleSerialisable(key.getName(), key.getGroup());
    } else {
      triggerCle = null;
    }
  }
}
