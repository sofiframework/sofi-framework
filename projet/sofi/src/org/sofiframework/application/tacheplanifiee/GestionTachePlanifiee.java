/**
 * 
 */
package org.sofiframework.application.tacheplanifiee;

import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;



/**
 * @author rmercier
 *
 */
public class GestionTachePlanifiee extends BaseGestionService {

  private static GestionTachePlanifiee singleton = new GestionTachePlanifiee();

  private ServiceTachePlanifiee service;

  private GestionTachePlanifiee() {
    super(ServiceTachePlanifiee.class);
  }

  public static GestionTachePlanifiee getInstance() {
    return singleton;
  }

  /* (non-Javadoc)
   * @see com.nurun.sofi.application.base.BaseGestionService#setServiceLocal(java.lang.Object)
   */
  @Override
  protected void setServiceLocal(Object service) {
    setService((ServiceTachePlanifiee) service);
  }

  public ServiceTachePlanifiee getService() {
    return service;
  }

  public void setService(ServiceTachePlanifiee service) {
    this.service = service;
  }

}
