/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.tacheplanifiee;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.StatefulJob;
import org.sofiframework.application.tacheplanifiee.job.Tache;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful;

/**
 * Classe permettant d'encapsuler un problème causé par le fonctionnement d'un ordonnanceur Quartz "en distant".
 * Généralement, la classe utilisée pour réaliser un traitement n'est pas connu de l'infrastructure SOFI ; la propriété
 * <code>jobClass</code> ne peut donc pas être sérialisée et désérialisée lors des échanges entre l'infrastructure SOFI
 * et une application cliente car l'infrastructure n'a pas connaissance de cette classe (exemple
 * mon.projet.tache.MaTache). Cette classe encapsule la propriété ; seul le nom de la classe sera transférée pour
 * permettre de loader la classe côté client.
 * 
 * @author remimercier
 * 
 */
public class JobDetailImpl extends JobDetail {

  /**
   * 
   */
  private static final long serialVersionUID = -6967121078615034753L;

  /**
   * Nom complet de la classe qui sera instanciée par l'ordonnanceur pour effectuer un traitement. Cette classe hérite
   * généralement de {@link Tache} ou {@link TacheStateful}. Elle doit obligatoirement implémenter l'interface
   * {@link Job} ou {@link StatefulJob}.
   */
  private String nomClasse;

  /**
   * Propriété transient car elle ne doit pas être sérialisée / désérialisée lors des échanges entre l'infrastructure
   * Sofi et l'application cliente.
   */
  @SuppressWarnings("rawtypes")
  private transient Class jobClass;

  public String getNomClasse() {
    return nomClasse;
  }

  public void setNomClasse(String nomClasse) {
    this.nomClasse = nomClasse;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public Class getJobClass() {
    return jobClass;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public void setJobClass(Class jobClass) {
    this.jobClass = jobClass;
  }

  /**
   * 
   */
  @Override
  public boolean isStateful() {
    if (nomClasse != null) {
      try {
        @SuppressWarnings("rawtypes")
        Class classe = Class.forName(nomClasse);
        return StatefulJob.class.isAssignableFrom(classe);
      } catch (ClassNotFoundException e) {
      }
    }
    return super.isStateful();
  }
}
