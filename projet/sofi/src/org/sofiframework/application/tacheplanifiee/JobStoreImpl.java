package org.sofiframework.application.tacheplanifiee;

import java.sql.Connection;

import org.quartz.JobPersistenceException;
import org.quartz.Scheduler;
import org.quartz.SchedulerConfigException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.jdbcjobstore.DriverDelegate;
import org.quartz.impl.jdbcjobstore.JobStoreCMT;
import org.quartz.impl.jdbcjobstore.JobStoreSupport;
import org.quartz.impl.jdbcjobstore.LockException;
import org.quartz.impl.jdbcjobstore.NoSuchDelegateException;
import org.quartz.spi.ClassLoadHelper;
import org.quartz.spi.SchedulerSignaler;
import org.sofiframework.modele.exception.ModeleException;




/**
 * Permet de redéfinir la méthode d'instanciation de l'objet
 * {@link DriverDelegate}. Cette méthode est appelé par l'objet
 * {@link SchedulerFactory} lors de la création d'un {@link Scheduler}.
 * 
 * @author rmercier
 */
public class JobStoreImpl extends JobStoreSupport {

  /*
   * Ne doit pâs être de type Long car l'initialiseur de propriétés Quartz ne le gère pas
   */
  private long schedulerId;

  /**
   * On garde la référence de l'implémentation de l'interface {@link DriverDelegate} qui ne
   * doit plus être au niveau de la classe parent car c'est notre objet {@link JobStoreImpl} qui contruit
   * le {@link DriverDelegate}
   */
  private DriverDelegate delegate;

  // Méthodes abstraites
  //////////////////////
  /**
   * Méthode inutile pour notre mode de fonctionnement avec Hibernate
   */
  @Override
  protected Connection getNonManagedTXConnection()
      throws JobPersistenceException {
    return null;
  }

  /**
   * Méthode grandement inspirée de la classe {@link JobStoreCMT} fournie par
   * Quartz
   */
  @Override
  protected Object executeInLock(String lockName, TransactionCallback txCallback)
      throws JobPersistenceException {

    boolean transOwner = false;
    Connection conn = null;
    try {
      if (lockName != null) {
        transOwner = getLockHandler().obtainLock(conn, lockName);
      }
      return txCallback.execute(null);
    } finally {
      releaseLock(conn, LOCK_TRIGGER_ACCESS, transOwner);
    }
  }

  /**
   * On fournit notre propre implémentation de l'interface
   * {@link DriverDelegate} à l'objet {@link JobStoreSupport}.
   */
  @Override
  protected DriverDelegate getDelegate() throws NoSuchDelegateException {
    if(delegate == null) {
      delegate = new DriverDelegateImpl(schedulerId,
          getInstanceId(), new Boolean(canUseProperties()));
    }
    return delegate;
  }

  // Méthodes surchargées
  ///////////////////////
  /*
   * (non-Javadoc)
   * @see org.quartz.impl.jdbcjobstore.JobStoreSupport#initialize(org.quartz.spi.ClassLoadHelper, org.quartz.spi.SchedulerSignaler)
   */
  @Override
  public void initialize(ClassLoadHelper loadHelper, SchedulerSignaler signaler)
      throws SchedulerConfigException {

    /*
     * Hack pour outre-passer un contrôle fait par le parent sur la connection
     */
    this.setDataSource("Inutile");
    super.initialize(loadHelper, signaler);
  }

  /**
   * On redéfinit la méthode car on n'a pas besoin de tester la connection jdbc
   * (délégée à Hibernate)
   */
  @Override
  protected void releaseLock(Connection conn, String lockName, boolean doIt) {
    try {
      if (doIt) {
        getLockHandler().releaseLock(conn, lockName);
      }
    } catch (LockException e) {
      throw new ModeleException("Impossible de relâcher le verrou " + lockName,
          e);
    }
  }

  public long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(long schedulerId) {
    this.schedulerId = schedulerId;
  }

}
