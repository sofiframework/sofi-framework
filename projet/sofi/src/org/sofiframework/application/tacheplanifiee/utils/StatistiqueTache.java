package org.sofiframework.application.tacheplanifiee.utils;

import java.io.Serializable;
import java.util.Date;

public class StatistiqueTache implements Serializable{

  private static final long serialVersionUID = 4069695996096572321L;

  private Long debut;
  private Long fin;

  public StatistiqueTache(Long debut,Long fin){
    this.debut = debut;
    this.fin = fin;
  }

  public Long getDebut() {
    return debut;
  }
  public void setDebut(Long debut) {
    this.debut = debut;
  }
  public Long getFin() {
    return fin;
  }
  public void setFin(Long fin) {
    this.fin = fin;
  }

  public Long getTempsExecution(){
    return fin - debut;
  }

  public Date getDateDebut(){
    return new Date(debut);
  }

  public Date getDateFin(){
    return new Date(fin);
  }
}
