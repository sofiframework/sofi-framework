/**
 * 
 */
package org.sofiframework.application.tacheplanifiee.utils;

import java.io.Serializable;

/**
 * 
 * @author rmercier
 *
 */
public class CleSerialisable implements Serializable {

  private String nom;
  private String groupe;
  /**
   * 
   */
  private static final long serialVersionUID = -372599798909482352L;

  public CleSerialisable(String nom, String groupe) {
    this.nom = nom;
    this.groupe = groupe;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

}
