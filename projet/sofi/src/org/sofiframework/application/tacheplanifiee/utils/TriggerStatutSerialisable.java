package org.sofiframework.application.tacheplanifiee.utils;

import java.io.Serializable;
import java.util.Date;

public class TriggerStatutSerialisable implements Serializable{


  private static final long serialVersionUID = 2378292812550336914L;

  public TriggerStatutSerialisable(String first,Date second,CleSerialisable key, CleSerialisable jobKey){
    this.first = first;
    this.second = second;
    this.key = key;
    this.jobKey = jobKey;
  }

  // ce qui constitue la paire
  private String first;
  private Date second;

  // ce qui constitue le triggerstatut
  private CleSerialisable key;
  private CleSerialisable jobKey;

  public String getFirst() {
    return first;
  }
  public void setFirst(String first) {
    this.first = first;
  }
  public Date getSecond() {
    return second;
  }
  public void setSecond(Date second) {
    this.second = second;
  }
  public CleSerialisable getKey() {
    return key;
  }
  public void setKey(CleSerialisable key) {
    this.key = key;
  }
  public CleSerialisable getJobKey() {
    return jobKey;
  }
  public void setJobKey(CleSerialisable jobKey) {
    this.jobKey = jobKey;
  }


}
