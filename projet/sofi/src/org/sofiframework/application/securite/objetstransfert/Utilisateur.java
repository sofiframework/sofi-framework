/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.sofiframework.application.locale.objetstransfert.VilleRegion;
import org.sofiframework.application.notification.objetstransfert.Notification;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.modele.entite.EntiteTrace;
import org.sofiframework.utilitaire.UtilitaireLocale;

/**
 * Classe qui sert d'objet de transfert du modele vers la couche de presentation
 * pour les information sur l'utilisateur avec sa sécurité associé.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun Inc.)
 * @version SOFI 1.0
 */
public class Utilisateur extends EntiteTrace {
  private static final long serialVersionUID = -1816062460352290638L;

  /**
   * Le code de l'utilisateur.
   */
  private String codeUtilisateur;
  
  /**
   * Le nom d'utilisateur.
   */
  private String nomUtilisateur;  

  /**
   * Le mot de passe de l'utilisateur.
   */
  private String motPasse;

  /**
   * Le nom de l'utilisateur.
   */
  private String nom;

  /**
   * Le prénom de l'utilisateur.
   */
  private String prenom;

  /**
   * Le couriel de l'utilisateur.
   */
  private String courriel;

  /**
   * La liste des applications de l'utilisateur.
   */
  private List<Application> listeApplications;

  /**
   * La liste des rôles de l'utilisateur.
   */
  private List<Role> listeRoles;

  /**
   * La liste des clients disponibles à l'utilisateur.
   * 
   * @since SOFI 2.0.2
   */
  @SuppressWarnings("rawtypes")
  private List listeClients;

  /**
   * Association 1 à plusieurs entre un utilisateur qui possède des objets
   * sécurisables
   * 
   * @label UtilisateurObjetSecurisable
   * @supplierCardinality 1..*
   * @associates
   *             <{org.sofiframework.application.securite.objetstransfert.ObjetSecurisable
   *             }>
   */
  protected List<ObjetSecurisable> listeObjetSecurisables;

  /**
   * Association 1 à plusieurs entre un utilisateur qui possède des objets
   * sécurisés soit des objets sécurisables qui ne lui sont pas accessibles.
   * 
   * @supplierCardinality 0..*
   * @associates
   *             <{org.sofiframework.application.securite.objetstransfert.ObjetSecurisable
   *             }>
   */
  @SuppressWarnings("rawtypes")
  protected HashMap listeObjetSecurises;

  /**
   * Liste des objets sécurisables navigable par une adresse web tel que les
   * sections, services, onglets, bouton et lien hypertexte.
   */
  @SuppressWarnings("rawtypes")
  transient private HashMap listeObjetSecurisablesParAdresse = null;

  /**
   * Liste des objets sécurisables qui peuvent être sécurié par son nom tel que
   * les champs de saisies, liens hypertexte ou boutons
   */
  @SuppressWarnings("rawtypes")
  transient private HashMap listeObjetSecurisablesParNom = null;

  /**
   * Spécifie le fil de navigation courant de l'utilisateur
   */
  @SuppressWarnings("rawtypes")
  transient private List filNavigation;

  /**
   * Spécifie la clé de l'objet sécurisable qui est utilisé pour sa page
   * d'accueil.
   */
  private Integer seqObjetSecurisableAccueil;

  /**
   * La langue de l'utilisateur
   */
  private String langue;

  /**
   * Le Sexe de l'utilisateur
   */
  private String sexe;

  /**
   * L'utilisateur spécifique à l'application
   */
  @SuppressWarnings("rawtypes")
  private HashMap detail;

  /**
   * Le pays de l'utilisateur
   */
  private String pays;

  /**
   * Le détail de l'utilisateur sur l'accès à l'application.
   */
  private AccesApplication accesApplication;

  /**
   * Spécifie le code utilisateur applicatif en référence au code utilisateur
   * qui est plus technique.
   */
  private String codeUtilisateurApplicatif;

  /**
   * Le code statut de l'utilisateur.
   */
  private String codeStatut;

  /**
   * Liste qui contient les notifications non lu par l'utilisateur
   */
  private List<Notification> listeNotification;

  /**
   * Date d'expiration du mot de passe
   */
  private Date dateMotPasse;

  /**
   * Identifier qu'il s'agit d'un nouveau mot de passe saisi par l'utilisateur.
   */
  private boolean nouveauMotPasse;

  /**
   * La liste des permissions disponibles à l'utilisateur.
   */
  @SuppressWarnings("rawtypes")
  private HashSet listePermission;

  /**
   * Date du dernier accès.
   */
  private Date dateDernierAcces;

  /**
   * Le nombre de tentative d'accès
   */
  private Integer nbTentativeAcces;

  /**
   * La locale de préférence de l'utilisateur.
   */
  private Locale locale;

  /**
   * Le code de la locale de préférence de l'utilisateur.
   */
  private String codeLocale;

  /**
   * Url de la photo de l'utilisateur.
   */
  private String urlPhoto;

  /**
   * Le code du fournisseur d'accès de l'authentification.
   */
  private String codeFournisseurAcces;

  /**
   * Description de l'utilisateur
   */
  private String description;

  /**
   * La date anniversaire de l'utilisateur.
   */
  private java.util.Date dateNaissance;

  /**
   * Groupe d'age de l'utilisateur.
   */
  private String codeGroupeAge;

  /**
   * Utilisateur supprimé?
   */
  private Boolean isDeleted;

  /**
   * Liste de propriétés
   * since 3.2
   */
  private List<Propriete> listePropriete;
  
  /**
   * Liste des accès externe
   * @since 3.2
   */
  private List<AccesExterne> listeAccesExterne;
  
  /**
   * Le code de pays de l'utilisateur.
   * @since 3.2
   */
  private String codePays;
  
  /**
   * Le code postal de l'utilisateur.
   * @since 3.2
   */
  private String codePostal;
  
  /**
   * La région de préférence de l'utilisateur.
   * @since 3.2
   */
  private String codeRegion;
  
  /**
   * La sous région de préférence de l'utilisateur.
   * @since 3.2
   */
  private String codeSousRegion;
  
  
  /**
   * La ville de préférence de l'utilisateur
   * @since 3.2
   */
  private Long idVille;
  
  private VilleRegion villeRegion;
  
  /**
   * Spécifie le code du fournisseur d'accès externe utilisé, s'il y a lieu.
   */
  private String codeDernierAccesExterne;
  
  /**
   * Spécifie le code client du dernier accès réalisé.
   */
  private String codeDernierAccesClient;
  
  /**
   * Spécifie le no de téléphone que l'utilisateur peut spécifier pour des raisons de sécurité ou de communication.
   */
  private String noTelephone;

  /**
   * Constructeur par défaut
   */
  public Utilisateur() {
    ajouterAttributAIgnorer("detail");
  }

  /**
   * Retourne le code de l'utilisateur.
   * <p>
   * 
   * @return String le code de l'utilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer le code de l'utilisateur.
   * <p>
   * 
   * @param newCodeUtilisateur
   *          le code de l'utilisateur
   */
  public void setCodeUtilisateur(String newCodeUtilisateur) {
    codeUtilisateur = newCodeUtilisateur;
  }

  /**
   * Retourne le mot de passe de l'utilisateur.
   * <p>
   * 
   * @return String le mot de passe de l'utilisateur
   */
  public String getMotPasse() {
    return motPasse;
  }

  /**
   * Fixer le prénom le mot de passe de l'utilisateur.
   * <p>
   * 
   * @param newMotPasse
   *          le mot de passe de l'utilisateur
   */
  public void setMotPasse(String newMotPasse) {
    motPasse = newMotPasse;
  }

  /**
   * Retourne le nom de l'utilisateur.
   * <p>
   * 
   * @return String le nom de l'utilisateur
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'utilisateur.
   * <p>
   * 
   * @param newNom
   *          le nom de l'utilisateur
   */
  public void setNom(String newNom) {
    nom = newNom;
  }

  /**
   * Retourne le prénom de l'utilisateur.
   * <p>
   * 
   * @return String le prénom de l'utilisateur
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prénom de l'utilisateur.
   * <p>
   * 
   * @param newPrenom
   *          le prénom de l'utilisateur
   */
  public void setPrenom(String newPrenom) {
    prenom = newPrenom;
  }

  /**
   * Retourne le courriel de l'utilisateur.
   * 
   * @return le courriel de l'utilisateur
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur
   * 
   * @param newCourriel
   *          le nouveau courriel
   */
  public void setCourriel(String newCourriel) {
    courriel = newCourriel;
  }

  /**
   * Retourne la liste des applications que l'utilisateur peut accéder ainsi que
   * la liste des services associés.
   * 
   * @return la liste des applications accessible par l'utilisateur
   */
  public List<Application> getListeApplications() {
    return listeApplications;
  }

  /**
   * Fixer la liste liste des applications que l'utilisateur peut accéder ainsi
   * que la liste des services associés.
   * 
   * @param newListeApplications
   *          la liste des applications accessibles par l'utilisateur
   */
  public void setListeApplications(List<Application> newListeApplications) {
    listeApplications = newListeApplications;
  }

  /**
   * Retourne la liste d'objets sécurisables qui ne sont pas accessible. Les
   * instances de la liste doit être du type Service, Section, ChampSaisie,
   * Onglet, Action ou bloc d'information.
   * 
   * @return la liste des services non accessible.
   * @see org.sofiframework.application.securite.objetstransfert.Action
   * @see org.sofiframework.application.securite.objetstransfert.ChampSaisie
   * @see org.sofiframework.application.securite.objetstransfert.Onglet
   * @see org.sofiframework.application.securite.objetstransfert.Section
   * @see org.sofiframework.application.securite.objetstransfert.Service
   * @see org.sofiframework.application.securite.objetstransfert.Bloc
   */
  @SuppressWarnings("rawtypes")
  public HashMap getListeObjetSecurises() {
    return listeObjetSecurises;
  }

  /**
   * Fixer la liste d'objets sécurisables qui ne sont pas accessible. Les
   * instances de la liste doit être du type Service, Section, ChampSaisie,
   * Onglet, Action ou bloc d'information.
   * 
   * @param listeServicesSecurise
   *          la liste des objets sécurisables non accessible.
   * @see org.sofiframework.application.securite.objetstransfert.Action
   * @see org.sofiframework.application.securite.objetstransfert.ChampSaisie
   * @see org.sofiframework.application.securite.objetstransfert.Onglet
   * @see org.sofiframework.application.securite.objetstransfert.Section
   * @see org.sofiframework.application.securite.objetstransfert.Service
   * @see org.sofiframework.application.securite.objetstransfert.Bloc
   */
  public void setListeObjetSecurises(@SuppressWarnings("rawtypes") HashMap listeObjetSecurises) {
    this.listeObjetSecurises = listeObjetSecurises;
  }

  /**
   * Retourne la liste des rôles de l'utilisateur
   * 
   * @return la liste des rôles
   */
  public List<Role> getListeRoles() {
    return listeRoles;
  }

  /**
   * Fixer la liste des rôles de l'utilisateur
   * 
   * @param listeRoles
   *          la liste des rôles de l'utilisateur
   */
  public void setListeRoles(List<Role> listeRoles) {
    this.listeRoles = listeRoles;
  }

  /**
   * Obtenir la liste des rôles de premier niveau (sans parent) pour une
   * application.
   * 
   * @return liste des rôles de premier niveau (sans parent)
   */
  public List<Role> getListeRolePremierNiveau(String codeApplication) {
    List<Role> listeRole = null;
    List<Role> listeRoleApplication = this.getListeRole(codeApplication);
    for (Role role : listeRoleApplication) {
      if (role.getCodeRoleParent() == null) {
        if (listeRole == null) {
          listeRole = new ArrayList<Role>();
        }
        listeRole.add(role);
      }
    }
    return listeRole;
  }

  /**
   * Obtenir la liste distincte des codes de client pour les rôles de premiers
   * niveaux d'une application.
   * 
   * @param codeApplication
   *          Code de l'application
   * @return liste des codes de client
   */
  public List<String> getListeClientPremierNiveau(String codeApplication) {
    List<Role> listeRolePremierNiveau = this.getListeRolePremierNiveau(codeApplication);
    Set<String> codeClient = null;
    for (Role role : listeRolePremierNiveau) {
      List<String> listeClient = role.getListeClient();
      if (listeClient != null) {
        if (codeClient == null) {
          codeClient = new HashSet<String>();
        }
        codeClient.addAll(listeClient);
      }
    }
    return codeClient == null ? null : new ArrayList<String>(codeClient);
  }

  /**
   * Est-ce que l'utilisateur possède le rôle demandé.
   * 
   * @return true l'utilisateur possède le rôle demandé.
   * @param codeRole
   *          le code de role demandé.
   * @param codeApplication
   *          le code d'application dont nous désirons le rôle.
   * @since SOFI 1.8.2
   */
  public boolean isPossedeRole(String codeApplication, String codeRole) {
    boolean possedeRole = false;

    if ((getListeRoles() != null) && (codeApplication != null) && (codeRole != null)) {
      Iterator iterateur = getListeRoles().iterator();

      while (iterateur.hasNext() && !possedeRole) {
        Role role = (Role) iterateur.next();

        possedeRole = (role.getCodeApplication().equals(codeApplication) && role.getCode().equals(codeRole));
      }
    }

    return possedeRole;
  }

  /**
   * Est-ce que l'utilisateur possède le rôle demandé.
   * 
   * @return true l'utilisateur possède le rôle demandé.
   * @param codeRole
   *          le code de role demandé.
   * @param codeApplication
   *          le code d'application dont nous désirons le rôle.
   * @since SOFI 1.8.2
   */
  public List getListeRole(String codeApplication) {
    List listeRole = new ArrayList();

    if ((getListeRoles() != null) && (codeApplication != null)) {
      Iterator iterateur = getListeRoles().iterator();

      while (iterateur.hasNext()) {
        Role role = (Role) iterateur.next();

        if (role.getCodeApplication().equals(codeApplication)) {
          listeRole.add(role);
        }
      }
    }

    return listeRole;
  }

  /**
   * Est-ce que l'utilisateur possède le rôle demandé.
   * 
   * @return true l'utilisateur possède le rôle demandé.
   * @param codeRole
   *          le code de role demandé.
   * @param codeApplication
   *          le code d'application dont nous désirons le rôle.
   * @since SOFI 1.9
   */
  public List getListeClient(String codeApplication) {

    return getListeClient(codeApplication, null, true);
  }

  /**
   * Est-ce que l'utilisateur possède le rôle demandé.
   * 
   * @return true l'utilisateur possède le rôle demandé.
   * @param cloisonnementFacette
   *          la liste de clients
   * @param listeRoleExclus
   *          la liste des rôles a exclure de la liste des clients.
   * @param codeApplication
   *          le code d'application dont nous désirons le rôle.
   * @since SOFI 2.0.6
   */
  public List getListeClient(String codeApplication, HashSet listeRoleExclus, boolean cloisonnementFacette) {

    /**
     * Vérification du paramètre système 'exclureCloisementClientParFacette'
     * pour cloisonnement par facette de la liste des clients.
     * 
     * @since SOFI 2.0.6
     */

    boolean exclureCloisementClientParFacette = GestionParametreSysteme.getInstance().getBoolean("exclureCloisementClientParFacette").booleanValue();

    if (exclureCloisementClientParFacette) {
      cloisonnementFacette = false;
    }

    List listeClient = new LinkedList();

    if ((getListeRoles() != null) && (codeApplication != null)) {
      Iterator iterateur = getListeRoles().iterator();

      while (iterateur.hasNext()) {
        Role role = (Role) iterateur.next();

        /**
         * @since SOFI 2.0.6 Ajout du cloisonnement par facette.
         */
        String codeFacette = GestionSecurite.getInstance().getCodeFacette();

        if ((codeFacette == null || (codeFacette != null && codeFacette.equals(role.getCodeFacette()))) || !cloisonnementFacette) {
          if (role.getCodeApplication().equals(codeApplication) && (role.getListeClient() != null)
              && ((listeRoleExclus == null || !listeRoleExclus.contains(role.getCode())))) {
            Iterator iterateurClient = role.getListeClient().iterator();

            while (iterateurClient.hasNext()) {
              listeClient.add(iterateurClient.next());
            }
          }
        }
      }
    }

    return listeClient;
  }

  /**
   * Retourne la liste des clients associé à l'utilisateur.
   * 
   * @return la liste des clients associé à l'utilisateur.
   * @param codeApplication
   *          le code d'application dont nous désirons la liste des clients
   * @param listeRoleExclus
   *          Les rôles qui doit être exclus de la liste des clients.
   * @since SOFI 2.0.1
   */
  public List getListeClient(String codeApplication, HashSet listeRoleExclus) {
    return getListeClient(codeApplication, listeRoleExclus, true);
  }

  /**
   * Retourne la liste d'objets sécurisables qui lui sont accessibles. Les
   * instances de la liste doit être du type Service, Section, ChampSaisie,
   * Onglet ou Action.
   * 
   * @return la liste des services non accessible.
   * @see org.sofiframework.application.securite.objetstransfert.Action
   * @see org.sofiframework.application.securite.objetstransfert.ChampSaisie
   * @see org.sofiframework.application.securite.objetstransfert.Onglet
   * @see org.sofiframework.application.securite.objetstransfert.Section
   * @see org.sofiframework.application.securite.objetstransfert.Service
   */
  public List getListeObjetSecurisables() {
    return listeObjetSecurisables;
  }

  /**
   * Fixer la liste d'objets sécurisables qui lui sont accessibles. Les
   * instances de la liste doit être du type Service, Section, ChampSaisie,
   * Onglet ou Action.
   * 
   * @param listeObjetSecurisables
   *          la liste des objets sécurisables accessibles.
   * @see org.sofiframework.application.securite.objetstransfert.Action
   * @see org.sofiframework.application.securite.objetstransfert.ChampSaisie
   * @see org.sofiframework.application.securite.objetstransfert.Onglet
   * @see org.sofiframework.application.securite.objetstransfert.Section
   * @see org.sofiframework.application.securite.objetstransfert.Service
   */
  public void setListeObjetSecurisables(List listeObjetSecurisables) {
    this.listeObjetSecurisables = listeObjetSecurisables;
  }

  /**
   * Retourne le fil de navigation (fil d'ariane) courant.
   * 
   * @return le fil de navigation (fil d'ariane) courant.
   */
  public List getFilNavigation() {
    return filNavigation;
  }

  /**
   * Fixer le fil de navigation (fil d'ariane) courant.
   * 
   * @param filNavigation
   *          le fil de navigation (fil d'ariane) courant.
   */
  public void setFilNavigation(List filNavigation) {
    this.filNavigation = filNavigation;
  }

  /**
   * Retourne la clé de l'objet sécurisable qui est utiliser comme par d'accueil
   * de l'utilisateur.
   * 
   * @return la clé de l'objet sécurisable qui est utiliser comme par d'accueil
   *         de l'utilisateur.
   */
  public Integer getSeqObjetSecurisableAccueil() {
    return seqObjetSecurisableAccueil;
  }

  /**
   * Fixer la clé de l'objet sécurisable qui est utiliser comme par d'accueil de
   * l'utilisateur.
   * 
   * @param seqObjetSecurisableAccueil
   *          la clé de l'objet sécurisable qui est utiliser comme par d'accueil
   *          de l'utilisateur.
   */
  public void setSeqObjetSecurisableAccueil(Integer seqObjetSecurisableAccueil) {
    this.seqObjetSecurisableAccueil = seqObjetSecurisableAccueil;
  }

  /**
   * Fixer la langue de l'utilisateur
   * 
   * @param langue
   *          la langue de l'utilisateur
   */
  public void setLangue(String langue) {
    this.langue = langue;
  }

  /**
   * Retourne la langue de l'utilisateur
   * 
   * @return la langue de l'utilisateur
   */
  public String getLangue() {
    if (langue == null) {
      langue = getLocale().getLanguage();
    }
    return langue;
  }

  /**
   * Fixer le sexe de l'utilisateur
   * 
   * @param sexe
   *          le sexe de l'utilisateur
   */
  public void setSexe(String sexe) {
    this.sexe = sexe;
  }

  /**
   * Retourne le sexe de l'utilisateur
   * 
   * @return le sexe de l'utilisateur
   */
  public String getSexe() {
    return sexe;
  }

  /**
   * Retourne le détail de l'utilisateur spécifique à l'application
   * 
   * @return le détail de l'utilisateur spécifique à l'application
   */
  public HashMap getDetail() {
    if (detail == null) {
      detail = new HashMap();
    }

    return detail;
  }

  /**
   * Fixer le détail de l'utilisateur spécifique à l'application
   * 
   * @param le
   *          détail de l'utilisateur spécifique à l'application
   */
  public void setDetail(HashMap detail) {
    this.detail = detail;
  }

  /**
   * Retourne la liste des objets Sécurisables par adresse.
   * 
   * @return la liste des objets Sécurisables par adresse.
   */
  public HashMap getListeObjetSecurisablesParAdresse() {
    return listeObjetSecurisablesParAdresse;
  }

  /**
   * Fixer la liste des objets Sécurisables par adresse.
   * 
   * @param listeObjetSecurisablesParAdresse
   *          la liste des objets Sécurisables par adresse.
   */
  public void setListeObjetSecurisablesParAdresse(HashMap listeObjetSecurisablesParAdresse) {
    this.listeObjetSecurisablesParAdresse = listeObjetSecurisablesParAdresse;
  }

  /**
   * Retourne la liste des objets sécurisables par nom. On y retrouve les objets
   * sécurisables de type Action (lien hypertexte ou bouton) ou encore les
   * champs de saisies.
   * 
   * @return la liste des objets sécurisables par nom.
   */
  public HashMap getListeObjetSecurisablesParNom() {
    return listeObjetSecurisablesParNom;
  }

  /**
   * Fixer la liste des objets sécurisables par nom.
   * 
   * @param listeObjetSecurisablesParNom
   *          la liste des objets sécurisables par nom.
   */
  public void setListeObjetSecurisablesParNom(HashMap listeObjetSecurisablesParNom) {
    this.listeObjetSecurisablesParNom = listeObjetSecurisablesParNom;
  }

  /**
   * Fixer le pays de l'utilisateur.
   * 
   * @param pays
   *          le pays de l'utilisateur.
   */
  public void setPays(String pays) {
    this.pays = pays;
  }

  /**
   * Retourne le pays de l'utilisateur.
   * 
   * @return le pays de l'utilisateur.
   */
  public String getPays() {
    if (pays == null && getCodeLocale() != null) {
      return getCodeLocale().substring(getCodeLocale().indexOf("_") + 1);
    }
    return pays;
  }

  /**
   * Les collections autres que ArrayList sont remplacés par des ArrayList
   * 
   * @return
   */
  private Object writeReplace() {
    if ((this.listeApplications != null) && !(this.listeApplications instanceof ArrayList)) {
      this.listeApplications = new ArrayList(this.listeApplications);
    }

    if ((this.listeRoles != null) && !(this.listeRoles instanceof ArrayList)) {
      this.listeRoles = new ArrayList(this.listeRoles);
    }

    return this;
  }

  /**
   * Fixer le détail de l'accès à l'application.
   * 
   * @param accesApplication
   *          le détail de l'accès à l'application.
   */
  public void setAccesApplication(AccesApplication accesApplication) {
    this.accesApplication = accesApplication;
  }

  /**
   * Retourne le détail de l'accès à l'application.
   * 
   * @return le détail de l'accès à l'application.
   */
  public AccesApplication getAccesApplication() {
    return accesApplication;
  }

  /**
   * Fixer le code utilisateur applicatif.
   * 
   * @param codeUtilisateurApplicatif
   *          le code utilisateur applicatif.
   */
  public void setCodeUtilisateurApplicatif(String codeUtilisateurApplicatif) {
    this.codeUtilisateurApplicatif = codeUtilisateurApplicatif;
  }

  /**
   * Retourne le code utilisateur applicatif.
   * 
   * @return le code utilisateur applicatif.
   */
  public String getCodeUtilisateurApplicatif() {
    return codeUtilisateurApplicatif;
  }

  /**
   * Retourne le code statut de l'utilisateur.
   * 
   * @param codeStatut
   *          le code statut de l'utilisateur.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Fixer le code statut de l'utilisateur.
   * 
   * @return le code statut de l'utilisateur.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer la liste des clients disponible à l'utilisateur.
   * 
   * @param listeClients
   *          la liste des clients disponible à l'utilisateur.
   */
  public void setListeClients(List listeClients) {
    this.listeClients = listeClients;
  }

  /**
   * Retourne la liste des clients disponible à l'utilisateur.
   * 
   * @return la liste des clients disponible à l'utilisateur.
   */
  public List getListeClients() {
    return listeClients;
  }

  /**
   * Fixer la liste des notifications à l'utilisateur.
   * 
   * @param listeNotification
   *          la liste des notifications à l'utilisateur.
   */
  public void setListeNotification(List<Notification> listeNotification) {
    this.listeNotification = listeNotification;
  }

  /**
   * Retourne la liste des notifications à l'utilisateur.
   * 
   * @return la liste des notifications à l'utilisateur.
   */
  public List<Notification> getListeNotification() {
    return listeNotification;
  }

  /**
   * Est-ce que l'utilisateur a un nouveau mot de passe?
   * 
   * @return true si l'utilisateur a un nouveau mot de passe.
   */
  public boolean isNouveauMotPasse() {
    return nouveauMotPasse;
  }

  /**
   * Fixer true si nouveau mot de passe a l'utilisateur.
   * 
   * @param nouveauMotPasse
   *          true si nouveau mot de passe a l'utilisateur.
   */
  public void setNouveauMotPasse(boolean nouveauMotPasse) {
    this.nouveauMotPasse = nouveauMotPasse;
  }

  /**
   * Retourne la date de fixation du mot de passe de l'utilisateur.
   * 
   * @return la date de fixation du mot de passe de l'utilisateur.
   */
  public Date getDateMotPasse() {
    return dateMotPasse;
  }

  /**
   * Fixer la date du mot de passe de l'utilisateur.
   * 
   * @param dateMotPasse
   *          la date du mot de passe de l'utilisateur.
   */
  public void setDateMotPasse(Date dateMotPasse) {
    this.dateMotPasse = dateMotPasse;
  }

  /**
   * La liste des permissions autorisés à l'utilisateur
   * 
   * @return la liste des permissions autorisés à l'utilisateur
   */
  public HashSet getListePermission() {
    return listePermission;
  }

  /**
   * Fixer la liste des permissions autorisés à l'utilisateur
   * 
   * @param listePermission
   *          la liste des permissions autorisés à l'utilisateur
   */
  public void setListePermission(HashSet listePermission) {
    this.listePermission = listePermission;
  }

  /**
   * Est-ce que l'utilisateur possède le code de permission demandé.
   * 
   * @param codePermission
   *          le code de permission a valider.
   * @return true si l'utilisateur possède le code de permission demandé.
   */
  public boolean isPossedePermission(String codePermission) {
    if (this.getListePermission().contains(codePermission)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne la date et heure du dernier accès de l'utilisateur.
   * 
   * @return la date et heure du dernier accès de l'utilisateur.
   */
  public Date getDateDernierAcces() {
    return dateDernierAcces;
  }

  /**
   * Fixer la date et heure du dernier accès de l'utilisateur.
   * 
   * @param dateDernierAcces
   *          la date et heure du dernier accès de l'utilisateur.
   */
  public void setDateDernierAcces(Date dateDernierAcces) {
    this.dateDernierAcces = dateDernierAcces;
  }

  /**
   * Retourne le nombre de tentative d'accès avant succès.
   * 
   * @return le nombre de tentative d'accès avant succès.
   */
  public Integer getNbTentativeAcces() {
    return nbTentativeAcces;
  }

  /**
   * Fixer le nombre de tentative d'accès avant succès.
   * 
   * @param nbTentativeAcces
   *          le nombre de tentative d'accès avant succès.
   */
  public void setNbTentativeAcces(Integer nbTentativeAcces) {
    this.nbTentativeAcces = nbTentativeAcces;
  }

  /**
   * Retourne la locale de l'utilisateur
   * 
   * @return la locale de l'utilisateur.
   */
  public Locale getLocale() {
    if (locale == null) {
      locale = UtilitaireLocale.getLocale(getCodeLocale());
    }
    return locale;
  }

  /**
   * Fixer la locale de l'utilisateur.
   * 
   * @param locale
   *          la locale de l'utilisateur.
   */
  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  /**
   * Le code de la locale de l'utilisateur.
   * 
   * @return
   */
  public String getCodeLocale() {
    return codeLocale;
  }

  /**
   * Fixer le code de locale de l'utilisateur.
   * 
   * @param codeLocale
   *          le code de locale de l'utilisateur.
   */
  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
    if (codeLocale != null && codeLocale.indexOf("_") != -1) {
      setLocale(UtilitaireLocale.getLocale(codeLocale));
    }
  }

  /**
   * L'url de la photo qui personnalise l'utilisateur.
   * <p>
   * Si facebook on doit prendre la dimension spécifique pour être carré, si on désire l'original, il faut prendre <code>getUrlPhotoOriginal()</code>.
   * @return l'url de la photo qui personnalise l'utilisateur.
   * @since 3.0.2
   */
  public String getUrlPhoto() {
    return urlPhoto;
  }
  
  /**
   * L'url de la photo original de l'utilisateur.
   * 
   * @return l'url de la photo original de l'utilisateur.
   * @since 3.2
   */
  public String getUrlPhotoOriginal() {
    return urlPhoto;
  }  
  
  /**
   * L'url de la photo personnalisée de l'utilisateur. Si la photo provient d'un
   * fournisseur d'accès alors on retourne l'url avec ses paramètres supplémentaires
   * si applicable.
   * 
   * @return l'url de la photo qui personnalise l'utilisateur.
   * @since 3.0.2
   */
  public String getUrlPhotoComplete() {
    if (!StringUtils.isEmpty(this.getUrlPhoto()) && this.getListeAccesExterne() != null) {
      for (AccesExterne accesExterne : this.getListeAccesExterne()) {
        if (accesExterne.getUrlPhoto() != null
            && accesExterne.getUrlPhoto().equals(this.getUrlPhoto())) {
          return accesExterne.getUrlPhotoComplete();
        }
      }
    }
    return urlPhoto;
  }

  /**
   * Fixer l'url de la photo qui personnalise l'utilisateur.
   * 
   * @param urlPhoto
   *          l'url de la photo qui personnalise l'utilisateur.
   * @since 3.0.2
   */
  public void setUrlPhoto(String urlPhoto) {
    this.urlPhoto = urlPhoto;
  }

  /**
   * Le code de fournisseur d'accès de l'authentification de l'utilisateur.
   * 
   * @return le code de fournisseur d'accès de l'authentification de
   *         l'utilisateur.
   * @since 3.0.2
   */
  public String getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  /**
   * Fixer le code de fournisseur d'accès de l'authentification de
   * l'utilisateur.
   * 
   * @param codeFournisseurAcces
   *          le code de fournisseur d'accès de l'authentification de
   *          l'utilisateur.
   * @since 3.0.2
   */
  public void setCodeFournisseurAcces(String codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  /**
   * Retourne la description de l'utilisateur.
   * 
   * @return la description de l'utilisateur.
   * @since 3.0.2
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description de l'utilisateur.
   * 
   * @param description
   *          la description de l'utilisateur.
   * @since 3.0.2
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la date naissance.
   * 
   * @return la date naissance.
   * @since 3.0.2
   */
  public java.util.Date getDateNaissance() {
    return dateNaissance;
  }

  /**
   * Fixer la date anniversaire de l'utilisateur.
   * 
   * @param dateNaissance
   */
  public void setDateNaissance(java.util.Date dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  /**
   * @param isDeleted
   *          the isDeleted to set
   */
  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  /**
   * @return the isDeleted
   */
  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public String getCodeGroupeAge() {
    return codeGroupeAge;
  }

  public void setCodeGroupeAge(String codeGroupeAge) {
    this.codeGroupeAge = codeGroupeAge;
  }

  public List<Propriete> getListePropriete() {
    return listePropriete;
  }

  public void setListePropriete(List<Propriete> listePropriete) {
    this.listePropriete = listePropriete;
  }

  /**
   * Obtenir la liste des propriétés qui sont pour le client spécifié. Les
   * propriétés qui sont pour tous les clients seront aussi retournées.
   */
  public List<Propriete> getListePropriete(String codeClient) {
    List<Propriete> propsClient = new ArrayList<Propriete>();

    for (Propriete propriete : propsClient) {
      String cc = propriete.getCodeClient();

      if (cc == null || cc.equals(codeClient)) {
        propsClient.add(propriete);
      }
    }

    return propsClient;
  }

  public List<AccesExterne> getListeAccesExterne() {
    return listeAccesExterne;
  }

  public void setListeAccesExterne(List<AccesExterne> listeAccesExterne) {
    this.listeAccesExterne = listeAccesExterne;
  }

  public String getCodePays() {
    return codePays;
  }

  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }

  public String getCodeSousRegion() {
    return codeSousRegion;
  }

  public void setCodeSousRegion(String codeSousRegion) {
    this.codeSousRegion = codeSousRegion;
  }

  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public String getNomUtilisateur() {
    return nomUtilisateur;
  }

  public void setNomUtilisateur(String nomUtilisateur) {
    this.nomUtilisateur = nomUtilisateur;
  }

  public String getCodeRegion() {
    return codeRegion;
  }

  public void setCodeRegion(String codeRegion) {
    this.codeRegion = codeRegion;
  }

  public Long getIdVille() {
    return idVille;
  }

  public void setIdVille(Long idVille) {
    this.idVille = idVille;
  }

  public VilleRegion getVilleRegion() {
    return villeRegion;
  }

  public void setVilleRegion(VilleRegion villeRegion) {
    this.villeRegion = villeRegion;
  }

  public String getCodeDernierAccesExterne() {
    return codeDernierAccesExterne;
  }

  public void setCodeDernierAccesExterne(String codeDernierAccesExterne) {
    this.codeDernierAccesExterne = codeDernierAccesExterne;
  }

  public String getNoTelephone() {
    return noTelephone;
  }

  public void setNoTelephone(String noTelephone) {
    this.noTelephone = noTelephone;
  }

  public String getCodeDernierAccesClient() {
    return codeDernierAccesClient;
  }

  public void setCodeDernierAccesClient(String codeDernierAccesClient) {
    this.codeDernierAccesClient = codeDernierAccesClient;
  }
}
