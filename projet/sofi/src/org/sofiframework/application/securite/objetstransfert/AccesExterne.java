/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.Date;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe qui sert d'objet de transfert du modele vers la couche de presentation
 * pour les informations concernant les accès externes de l'utilisateur.
 * <p>
 * @author Jean-François Brassard (Sun Media Corporation)
 * @version 3.2
 */
public class AccesExterne extends ObjetTransfert {

  private static final long serialVersionUID = -4214559420571865661L;

  private Long id;
  private Long idUtilisateur;
  private String codeFournisseurAcces;
  private String codeUtilisateur;
  private String reference;
  private String codeFournisseurType;
  private String urlPhoto;
  private String urlPhotoComplete;
  private String urlSite;
  private Boolean isPrefere;
  private String creePar;
  private java.util.Date dateCreation;
  private String modifiePar;
  private java.util.Date dateModification;
  private Integer version;
  private String accessToken;
  private Date accessTokenExpiration;

  public AccesExterne() {
    this.setCle(new String[]{"id"});
  }


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  

  public Long getIdUtilisateur() {
    return idUtilisateur;
  }


  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public String getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  public void setCodeFournisseurAcces(String codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }


  /**
   * @param creePar the creePar to set
   */
  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  /**
   * @return the creePar
   */
  public String getCreePar() {
    return creePar;
  }

  /**
   * @param dateCreation the dateCreation to set
   */
  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  /**
   * @return the dateCreation
   */
  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  /**
   * @param modifiePar the modifiePar to set
   */
  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  /**
   * @return the modifiePar
   */
  public String getModifiePar() {
    return modifiePar;
  }

  /**
   * @param dateModification the dateModification to set
   */
  public void setDateModification(java.util.Date dateModification) {
    this.dateModification = dateModification;
  }

  /**
   * @return the dateModification
   */
  public java.util.Date getDateModification() {
    return dateModification;
  }


  public String getCodeFournisseurType() {
    return codeFournisseurType;
  }


  public void setCodeFournisseurType(String codeFournisseurType) {
    this.codeFournisseurType = codeFournisseurType;
  }


  public Integer getVersion() {
    return version;
  }


  public void setVersion(Integer version) {
    this.version = version;
  }


  public String getUrlPhoto() {
    return urlPhoto;
  }


  public void setUrlPhoto(String urlPhoto) {
    this.urlPhoto = urlPhoto;
  }
  
  public String getUrlSite() {
    return urlSite;
  }
  
  public void setUrlSite(String urlSite) {
    this.urlSite = urlSite;
  }


  public Boolean isPrefere() {
    return isPrefere;
  }


  public void setPrefere(Boolean isPrefere) {
    this.isPrefere = isPrefere;
  }


  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }


  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }


  public String getAccessToken() {
    return accessToken;
  }


  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }


  public Date getAccessTokenExpiration() {
    return accessTokenExpiration;
  }


  public void setAccessTokenExpiration(Date accessTokenExpiration) {
    this.accessTokenExpiration = accessTokenExpiration;
  }


  public String getUrlPhotoComplete() {
    return urlPhotoComplete;
  }


  public void setUrlPhotoComplete(String urlPhotoComplete) {
    this.urlPhotoComplete = urlPhotoComplete;
  }
  
}