/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Objet de transfert décrivant un client disponible à l'utilisateur.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.2
 * @since 3.2 La notion de client devient une entité.
 */
public class Client extends EntiteTrace {
  /**
   * 
   */
  private static final long serialVersionUID = -5228828759507241147L;

  // Le numéro de référence du client.
  private String noClient;
  // Le nom court du client.
  private String nomCourt;
  // Le nom officiel du client.
  private String nom;
  // Est-ce reservé pour de la consultation seulement.
  private boolean consultationSeulement;

  // Le code client de son parent s'il en a un.
  private String codeClientParent;
  private List<Client> listeClientEnfant;
  private Client clientParent;

  public Client() {
  }

  /**
   * Fixer le numéro du client.
   * 
   * @param noClient
   *          le numéro du client.
   */
  public void setNoClient(String noClient) {
    this.noClient = noClient;
  }

  /**
   * Retourne le numéro de client.
   * 
   * @return le numéro de client.
   */
  public String getNoClient() {
    return noClient;
  }

  /**
   * Retourne le nom court du client.
   * 
   * @param nomCourt
   *          le nom court du client.
   */
  public void setNomCourt(String nomCourt) {
    this.nomCourt = nomCourt;
  }

  /**
   * Retourne le nom court du client.
   * 
   * @return le nom court du client.
   */
  public String getNomCourt() {
    return nomCourt;
  }

  /**
   * Fixer le nom du client.
   * 
   * @param nom
   *          le nom du client.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom du client.
   * 
   * @return le nom du client.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer true si le rôle est à la consultation seulement.
   * 
   * @param consultationSeulement
   *          true si le rôle est spécifique à la consultation seulement.
   */
  public void setConsultationSeulement(boolean consultationSeulement) {
    this.consultationSeulement = consultationSeulement;
  }

  /**
   * Est-ce un rôle spécifique à la consultation seulement.
   * 
   * @return true si un rôle spécifique à la consultation seulement.
   */
  public boolean isConsultationSeulement() {
    return consultationSeulement;
  }

  public String getCodeClientParent() {
    return codeClientParent;
  }

  public void setCodeClientParent(String codeClientParent) {
    this.codeClientParent = codeClientParent;
  }

  public List<Client> getListeClientEnfant() {
    return listeClientEnfant;
  }

  public void setListeClientEnfant(List<Client> listeClientEnfant) {
    this.listeClientEnfant = listeClientEnfant;
  }

  public Client getClientParent() {
    return clientParent;
  }

  public void setClientParent(Client clientParent) {
    this.clientParent = clientParent;
  }

}
