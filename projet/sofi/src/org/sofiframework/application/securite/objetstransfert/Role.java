/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;


/**
 * Objet de transfert décrivant un role d'un utilisateur
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 3.0
 * @since 2.0.6
 * Ajout du concept de facette.
 * @since 3.0
 * Ajout du concept de permission.
 */
public class Role extends EntiteTrace {
  // Compatiblité avec SOFI 1.8.2 et inférieure
  private static final long serialVersionUID = 1169594329994578184L;
  // Le code de rôle
  private String code;
  // Le nom du rôle
  private String nom;
  // La description du röle
  private String description;
  // Le code d'application correspondant du rôle.
  private String codeApplication;

  /**
   * Le code de la facette correspondant au role.
   * @since 2.0.6
   */
  private String codeFacette;

  // Le code d'application parent.
  private String codeApplicationParent;
  // Le code du rôle parent.
  private String codeRoleParent;

  /**
   * La liste des clients disponibles pour un role.
   */
  private List listeClient;

  /**
   * La liste des rôles dépendante au rôle.
   */
  private List listeRoles;

  /**
   * Le rôle parent.
   */
  private Role roleParent;

  /**
   * Le type utilisateur qui spécifie le rôle.
   */
  private String codeTypeUtilisateur;

  /**
   * La liste détaillé des clients associés au rôle.
   * @since 2.0.2
   */
  private List listeClientDetaille;

  /**
   * Est-ce un rôle spécifique en consultation seulement.
   * @since 2.0.2
   */
  private boolean consultationSeulement;

  /**
   * La liste des rôles qui sont liés au rôle.
   */
  private List listeRoleLie;

  /**
   * La liste permissions associés au role.
   * @since 3.0
   */
  private List listePermission;

  /**
   * Le statut du role.
   */
  private String codeStatut;

  /** Construteur par défaut */
  public Role() {
  }

  /**
   * Retourne le code du role
   * @return  le code du role
   */
  public String getCode() {
    return code;
  }

  /**
   * Fixer le code du role
   * @param code le code du role
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Retourne le nom du role
   * @return le nom du role
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du role
   * @param nom le nom du role
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Fixer la description du role.
   * @param description la description du role.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la description du rôle.
   * @return la description du rôle.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Retourne le code d'application dont appartient le role
   * @return le code d'application dont appartient le role
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code d'application dont appartient le role
   * @param codeApplication le code d'application dont appartient le role
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Fixer la liste des clients associé au role.
   * @param listeClient la liste des clients associé au role.
   */
  public void setListeClient(List listeClient) {
    this.listeClient = listeClient;
  }

  /**
   * Retourne la liste des clients associé au role.
   * @return la liste des clients associé au role.
   */
  public List getListeClient() {
    return listeClient;
  }

  /**
   * Fixer le code application parent.
   * @param codeApplicationParent le code application parent.
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  /**
   * Retourne le code application parent.
   * @return le code application parent.
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  /**
   * Fixer le code de rôle parent.
   * @param codeRoleParent le code de rôle parent.
   */
  public void setCodeRoleParent(String codeRoleParent) {
    this.codeRoleParent = codeRoleParent;
  }

  /**
   * Retourne le code de rôle parent.
   * @return le code de rôle parent.
   */
  public String getCodeRoleParent() {
    return codeRoleParent;
  }

  /**
   * La liste des rôles de l'utilisateur.
   * @param listeRoles la liste des rôles de l'utilisateur.
   */
  public void setListeRoles(List listeRoles) {
    this.listeRoles = listeRoles;
  }

  /**
   * Retourne la liste des rôles enfants.
   * @return la liste des rôles enfants.
   */
  public List getListeRoles() {
    return listeRoles;
  }

  /**
   * Fixer le rôle parent.
   * @param roleParent le rôle parent.
   */
  public void setRoleParent(Role roleParent) {
    this.roleParent = roleParent;
  }

  /**
   * Retourne le rôle parent.
   * @return le rôle parent.
   */
  public Role getRoleParent() {
    return roleParent;
  }

  /**
   * Fixer le code type utilisateur qui spécifie le rôle.
   * @param codeTypeUtilisateur le code type utilisateur qui spécifie le rôle.
   */
  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  /**
   * Retourne le code type utilisateur qui spécifie le rôle.
   * @return  le code type utilisateur qui spécifie le rôle.
   */
  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  /**
   * Fixer la liste des clients détaillés.
   * @param listeClientDetaille la liste des clients détaillés.
   */
  public void setListeClientDetaille(List listeClientDetaille) {
    this.listeClientDetaille = listeClientDetaille;
  }

  /**
   * Retourne la liste des clients détaillés.
   * @return la liste des clients détaillés.
   */
  public List getListeClientDetaille() {
    return listeClientDetaille;
  }

  /**
   * Fixer true si le rôle est  à la consultation seulement.
   * @param consultationSeulement true si le rôle est spécifique  à la consultation seulement.
   */
  public void setConsultationSeulement(boolean consultationSeulement) {
    this.consultationSeulement = consultationSeulement;
  }

  /**
   * Est-ce un rôle spécifique  à la consultation seulement.
   * @return true si un rôle spécifique à la consultation seulement.
   */
  public boolean isConsultationSeulement() {
    return consultationSeulement;
  }

  /**
   * Fixer la liste des rôles qui sont lié par un autre
   * rôle de l'application ou d'une autre application.
   * @param listeRoleLie la liste des rôles qui sont lié par un autre
   * rôle de l'application ou d'une autre application.
   */
  public void setListeRoleLie(List listeRoleLie) {
    this.listeRoleLie = listeRoleLie;
  }

  /**
   * Retourne la liste des rôles qui sont lié par un autre
   * rôle de l'application ou d'une autre application.
   * @return la liste des rôles qui sont lié par un autre
   * rôle de l'application ou d'une autre application.
   */
  public List getListeRoleLie() {
    return listeRoleLie;
  }

  /**
   * Est-ce que le rôle est lié avec un ou des autres rôle du système ou
   * d'un autre système.
   * @return true si le rôle est lié avec un ou des autres rôle du système ou
   * d'un autre système.
   */
  public boolean isRoleLie() {
    if (listeRoleLie != null && listeRoleLie.size() > 0) {
      return true;
    }else {
      return false;
    }
  }

  /**
   * Fixer le code la facette specifique au role.
   * @param codeFacette le code la facette specifique au role.
   * @since 2.0.6
   */
  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  /**
   * Retourne le code la facette specfique au role.
   * @return le code la facette specfique au role.
   * @since 2.0.6
   */
  public String getCodeFacette() {
    return codeFacette;
  }

  /**
   * Retourne la liste des permissions associé au role.
   * @return la liste des permissions associé au role.
   * @since 3.0
   */
  public List getListePermission() {
    return listePermission;
  }

  /**
   * 
   * @param listePermission
   */
  public void setListePermission(List listePermission) {
    this.listePermission = listePermission;
  }

  /**
   * @param codeStatut the codeStatut to set
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * @return the codeStatut
   */
  public String getCodeStatut() {
    return codeStatut;
  }
}
