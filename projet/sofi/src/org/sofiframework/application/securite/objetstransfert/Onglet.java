/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;


/**
 * Objet de transfert décrivant un objet sécurisable de type onglet.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class Onglet extends ObjetSecurisable {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = 6785506202288182744L;

  /** Le niveau d'onglet **/
  private Integer niveauOnglet = null;

  /** Construteur par défaut */
  public Onglet() {
  }

  /**
   * Fixer le niveau d'onglet
   * @param niveauOnglet le niveau d'onglet.
   */
  public void setNiveauOnglet(Integer niveauOnglet) {
    this.niveauOnglet = niveauOnglet;
  }

  /**
   * Retourne le niveau d'onglet
   * @return le niveau d'onglet.
   */
  public Integer getNiveauOnglet() {
    return niveauOnglet;
  }
}
