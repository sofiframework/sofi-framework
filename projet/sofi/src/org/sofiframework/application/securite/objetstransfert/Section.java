/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;


/**
 * Objet de transfert décrivant un objet sécurisable de type section de menu.
 * Donc il permet de regrouper différent menu dans une section de menu.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class Section extends ObjetSecurisable {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -8021113472284135308L;

  /** Construteur par défaut */
  public Section() {
  }

  /**
   * Retourne si la section n'inclus pas de service.
   * @return true si la section n'inclus pas de service.
   */
  public boolean isSectionVide() {
    if ((getListeObjetSecurisableEnfants() != null) &&
        (getListeObjetSecurisableEnfants().size() > 0)) {
      if (Service.class.isInstance(getListeObjetSecurisableEnfants().get(0))) {
        return false;
      } else if (Section.class.isInstance(getListeObjetSecurisableEnfants().get(0))) {
        ObjetSecurisable objetSecurisable = (ObjetSecurisable) getListeObjetSecurisableEnfants()
            .get(0);

        return ((Section) objetSecurisable).isSectionVide();
      } else {
        return true;
      }
    }

    return false;
  }
}
