package org.sofiframework.application.securite.objetstransfert;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Entite pour definir un somaire des accès d'un utilisateur.
 * @author jfbrassard
 * @version 3.1
 */

public class CertificatAccesSommaire extends ObjetTransfert {

  private static final long serialVersionUID = -1799330007003550968L;

  private Long idUtilisateur;
  private String codeUtilisateur;
  private String courriel;
  private String nom;
  private String prenom;
  private Integer nbConnexion;
  private Integer nbConnexionDifferente;
  private java.util.Date premiereConnexion;
  private java.util.Date derniereConnexion;
  /**
   * @param codeUtilisateur the codeUtilisateur to set
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }
  /**
   * @return the codeUtilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  /**
   * @return the nom
   */
  public String getNom() {
    return nom;
  }
  /**
   * @param prenom the prenom to set
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }
  /**
   * @return the prenom
   */
  public String getPrenom() {
    return prenom;
  }
  /**
   * @param nbConnexionDifferente the nbConnexionDifferente to set
   */
  public void setNbConnexionDifferente(Integer nbConnexionDifferente) {
    this.nbConnexionDifferente = nbConnexionDifferente;
  }
  /**
   * @return the nbConnexionDifferente
   */
  public Integer getNbConnexionDifferente() {
    return nbConnexionDifferente;
  }
  /**
   * @param premiereConnexion the premiereConnexion to set
   */
  public void setPremiereConnexion(java.util.Date premiereConnexion) {
    this.premiereConnexion = premiereConnexion;
  }
  /**
   * @return the premiereConnexion
   */
  public java.util.Date getPremiereConnexion() {
    return premiereConnexion;
  }
  /**
   * @param derniereConnexion the derniereConnexion to set
   */
  public void setDerniereConnexion(java.util.Date derniereConnexion) {
    this.derniereConnexion = derniereConnexion;
  }
  /**
   * @return the derniereConnexion
   */
  public java.util.Date getDerniereConnexion() {
    return derniereConnexion;
  }
  /**
   * @param nbConnexion the nbConnexion to set
   */
  public void setNbConnexion(Integer nbConnexion) {
    this.nbConnexion = nbConnexion;
  }
  /**
   * @return the nbConnexion
   */
  public Integer getNbConnexion() {
    return nbConnexion;
  }
  /**
   * @param idUtilisateur the idUtilisateur to set
   */
  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }
  /**
   * @return the idUtilisateur
   */
  public Long getIdUtilisateur() {
    return idUtilisateur;
  }
  /**
   * @return the courriel
   */
  public String getCourriel() {
    return courriel;
  }
  /**
   * @param courriel the courriel to set
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }



}
