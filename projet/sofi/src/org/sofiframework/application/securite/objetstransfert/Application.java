/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.sofiframework.application.locale.objetstransfert.LocaleApplication;
import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Objet de transfert spécifiant une application.
 * <p>
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class Application extends ObjetTransfert {
  private static final long serialVersionUID = -3322768520275726476L;

  /**
   * Code de l'application.
   */
  private String codeApplication;

  /**
   * L'acronyme désignant l'application.
   * @since SOFI 2.0.2
   */
  private String acronyme;

  /**
   * Nom de l'application.
   */
  private String nom;

  /**
   * Adresse du site de l'application.
   */
  private String adresseSite;
  
  /**
   * Adresse des noeuds pour rafrachissement des caches de l'application.
   */
  private String adresseCache;

  /**
   * Description de l'application.
   */
  private String description;

  /**
   * Aide contextuelle associée à l'application.
   */
  private String aideContextuelle;

  /**
   * Le chemin correspondant à l'icône de l'application.
   */
  private String icone;

  /**
   * Version de la livraison courante de l'application.
   */
  private String versionLivraison;

  /**
   * Comment here
   * @label ListeObjetSecurisable
   * @supplierCardinality 0..*
   * @associates <{org.sofiframework.application.securite.objetstransfert.ObjetSecurisable}>
   */
  protected List listeObjetSecurisables;

  /**
   * La liste des rôles disponibles pour l'application.
   */
  private List listeRoles;

  /**
   * Est-ce une application commun.
   * @since SOFI 2.0.2
   */
  private Boolean commun;

  /**
   * Est-ce que l'application est grand public et
   * offert au utilisateur externe.
   * @since 3.0.2
   */
  private Boolean externe;

  /**
   * Le locale pas défaut a utiliser par l'application.
   */
  private Locale localeParDefaut;


  /**
   * La liste des locales disponibles pour l'application<code>LocaleApplication</code>.
   */
  private List<LocaleApplication> listeLocales;

  private String codeUtilisateurAdmin;
  private String motPasseAdmin;
  private String statut;

  /**
   * Construteur par défaut.
   */
  public Application() {
  }

  /**
   * Obtenir le code, l'identifiant de l'application
   * @return le code, l'identifiant de l'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code de l'application.
   * @param newCodeApplication
   */
  public void setCodeApplication(String newCodeApplication) {
    codeApplication = newCodeApplication;
  }

  /**
   * Retourne le nom de l'application.
   * @return le nom de l'application
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'application.
   * @param newNom le nouveau nom de l'application
   */
  public void setNom(String newNom) {
    nom = newNom;
  }

  /**
   * Obtenir l'adresse qui permet d'appeller l'application.
   * @return l'adresse qui permet d'appeller l'application
   */
  public String getAdresseSite() {
    return adresseSite;
  }

  /**
   * Fixer l'adresse qui permet d'appeller l'application.
   * @param newAdresseSite nouvelle adresse de l'application
   */
  public void setAdresseSite(String newAdresseSite) {
    adresseSite = newAdresseSite;
  }
  
  /**
   * Obtenir l'adresse qui permet d'appeller les noeuds de l'application.
   * @return l'adresse qui permet d'appeller les noeuds de l'application
   */
  public String getAdresseCache() {
    return adresseCache;
  }

  /**
   * Fixer l'adresse qui permet d'appeller les noeuds de l'application.
   * @param newAdresseSite nouvelle adresse les noeuds de l'application.
   */
  public void setAdresseCache(String newAdresseCache) {
    adresseCache = newAdresseCache;
  }

  /**
   * Retourne la description de l'application.
   * @return la description de l'application
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description de l'application.
   * @param newDescription la description de l'application
   */
  public void setDescription(String newDescription) {
    description = newDescription;
  }

  /**
   * Retourne l'aide contextuelle pour l'application.
   * @return l'aide contextuelle pour l'application
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle pour l'application.
   * @param aideContextuelle l'aide contextuelle pour l'application
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne le chemin correspondant à l'icône de l'application.
   * @return le chemin correspondant à l'icône de l'application
   */
  public String getIcone() {
    return icone;
  }

  /**
   * Fixer le chemin correspondant à l'icône de l'application.
   * @param icone le chemin correspondant à l'icône de l'application.
   */
  public void setIcone(String icone) {
    this.icone = icone;
  }

  /**
   * Obtenir la liste des objets sécurisables pour l'application.
   * <p>
   * C'est objets sécurisables peut être des sections de menu, des services,
   * des liens hypertextes, des boutons ou encore des champs de saisie.
   * @return la liste des objets sécurisables pour l'application
   */
  public List getListeObjetSecurisables() {
    return listeObjetSecurisables;
  }

  /**
   * Fixer la liste des objets sécurisables pour l'application.
   * <p>
   * C'est objets sécurisables peut être des sections de menu, des services,
   * des liens hypertextes, des boutons ou encore des champs de saisie.
   * @param listeObjetSecurisables
   */
  public void setListeObjetSecurisables(List listeObjetSecurisables) {
    this.listeObjetSecurisables = listeObjetSecurisables;
  }

  /**
   * Obtenir le numéro de la version de la livraison.
   * @return version
   */
  private String getVersionLivraison() {
    return versionLivraison;
  }

  /**
   * Modifier le numéro de la version de la livraison.
   * @param versionLivraison nouvelle version
   */
  private void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  /**
   * Fixer la liste des rôles associés à l'application.
   * @param listeRoles la liste des rôles associés à l'application.
   */
  public void setListeRoles(List listeRoles) {
    this.listeRoles = listeRoles;
  }

  /**
   * Retourne la liste des rôles associés à l'application.
   * @return  la liste des rôles associés à l'application.
   */
  public List getListeRoles() {
    return listeRoles;
  }

  /**
   * Fixer l'acronyme de l'application.
   * @param acronyme l'acronyme de l'application.
   */
  public void setAcronyme(String acronyme) {
    this.acronyme = acronyme;
  }

  /**
   * Retourne l'acronyme de l'application.
   * @return l'acronyme de l'application.
   */
  public String getAcronyme() {
    return acronyme;
  }

  /**
   * Fixer true si application commun.
   * @param commun true si application commun.
   */
  public void setCommun(Boolean commun) {
    this.commun = commun;
  }

  /**
   * Est-ce un application commun.
   * @return true si application commun.
   */
  public Boolean getCommun() {
    return commun;
  }

  /**
   * Retourne la liste de tous les rôles peut importe son niveau hiérarchique.
   * <p>
   * @since SOFI 2.0.2
   * @return la liste de tous les rôles peut importe son niveau hiérarchique.
   */
  public List getListeTousRolesSansNiveauHierarchique() {
    List listeRolesSansNiveauHierarchique = new ArrayList();
    if (getListeRoles() != null) {
      Iterator iterateur = getListeRoles().iterator();
      while (iterateur.hasNext()) {
        Role role = (Role) iterateur.next();
        listeRolesSansNiveauHierarchique.add(role);

        if (role.getListeRoles() != null && role.getListeRoles().size() > 0) {
          remplirRoleEnfants(listeRolesSansNiveauHierarchique, role);
        }

      }
    }
    return listeRolesSansNiveauHierarchique;
  }

  /**
   * Méthode récursif qui permet de naviguer dans tous les rôles et de remplire une liste
   * incluant tous les rôles peut importe son niveau hiérarchique.
   * @param listeRolesSansNiveauHierarchique la liste des rôles peut importe son niveau hiérarchique.
   * @param roleEnfant role enfant a traiter.
   */
  private void remplirRoleEnfants(List listeRolesSansNiveauHierarchique, Role roleEnfant) {
    Iterator iterateur = roleEnfant.getListeRoles().iterator();
    while (iterateur.hasNext()) {
      Role role = (Role) iterateur.next();
      listeRolesSansNiveauHierarchique.add(role);

      if (role.getListeRoles() != null && role.getListeRoles().size() > 0) {
        remplirRoleEnfants(listeRolesSansNiveauHierarchique, role);
      }
    }
  }

  /**
   * Fixer la liste des locales disponibles pour l'application.
   * @param listeLocales listeLocales a fixer.
   * @since 3.0.2
   */
  public void setListeLocales(List<LocaleApplication> listeLocales) {
    this.listeLocales = listeLocales;
  }

  /**
   * Retourne la liste des locales disponibile
   * @return listeLocales
   * @since 3.0.2
   */
  public List<LocaleApplication> getListeLocales() {
    return listeLocales;
  }

  /**
   * Fixer le locale par défaut a utiliser pour l'application.
   * @param localeParDefaut localeParDefaut a fixer.
   * @since 3.0.2
   */
  public void setLocaleParDefaut(Locale localeParDefaut) {
    this.localeParDefaut = localeParDefaut;
  }

  /**
   * Retourne le locale par défaut a utiliser pour l'application.
   * @return localeParDefaut
   * @since 3.0.2
   */
  public Locale getLocaleParDefaut() {
    return localeParDefaut;
  }

  /**
   * @param codeUtilisateurAdmin the codeUtilisateurAdmin to set
   */
  public void setCodeUtilisateurAdmin(String codeUtilisateurAdmin) {
    this.codeUtilisateurAdmin = codeUtilisateurAdmin;
  }

  /**
   * @return the codeUtilisateurAdmin
   */
  public String getCodeUtilisateurAdmin() {
    return codeUtilisateurAdmin;
  }

  /**
   * @param motPasseAdmin the motPasseAdmin to set
   */
  public void setMotPasseAdmin(String motPasseAdmin) {
    this.motPasseAdmin = motPasseAdmin;
  }

  /**
   * @return the motPasseAdmin
   */
  public String getMotPasseAdmin() {
    return motPasseAdmin;
  }

  /**
   * @param statut the statut to set
   */
  public void setStatut(String statut) {
    this.statut = statut;
  }

  /**
   * @return the statut
   */
  public String getStatut() {
    return statut;
  }

  /**
   * @param externe the externe to set
   */
  public void setExterne(Boolean externe) {
    this.externe = externe;
  }

  /**
   * @return the externe
   */
  public Boolean getExterne() {
    return externe;
  }
}
