/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.parametresysteme.UtilitaireParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet de transfert décrivant un objet sécurisable. Cet objet peut être une
 * section, un service, un onglet, un champ de saisie ou encore une action (peut
 * être représenté par un bouton ou un lien hypertexte).
 * <p>
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class ObjetSecurisable extends ObjetTransfert {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -6365444602272570279L;

  /**
   * La sequence de l'objet sécurisable. Clé unique.
   */
  private Long seqObjetSecurisable;

  /**
   * Le nom du service.
   */
  private String nom;

  /**
   * L'adresse Web de l'objet sécurisé.
   */
  private String adresseWeb;

  /**
   * Le nom de l'action qui est basé l'objet sécurisable s'il y a lieu.
   */
  private String nomAction;

  /**
   * Le nom de paramètre que le nom d'action utilisable.
   */
  private String nomParametre;

  /**
   * Ordre de présentation de l'objet sécurisable.
   */
  private Integer ordrePresentation;

  /**
   * Le type d'objet sécurisable.
   */
  private String type;

  /**
   * Liste des objets sécurisables enfants.
   */
  private List listeObjetSecurisableEnfants;

  /**
   * Aide contextuelle
   */
  private String aideContextuelle;

  /**
   * cle de l'aide contextuelle associé
   */
  private String cleAideContextuelle;

  /**
   * Récurisivité sur l'objet sécurisable 1 à 1.
   * Un objet sécurisable peut alors avoir un parent.
   * @label ObjetSecurisableobjetSecurisable
   */
  protected ObjetSecurisable objetSecurisableParent;

  /**
   * Est-ce que l'objet sécurisable est en lecture seulement
   */
  private boolean lectureSeulement = false;

  /**
   *  Le type de mode d'affichage (Dans la même fenêtre, dans un nouveau fenêtre, en mode ajax, etc.)
   */
  private String modeAffichage = null;

  /**
   * Le div de retour Ajax lorsque le mode d'affichage est Ajax.
   */
  private String divAjax = null;

  /**
   * Le style CSS à appliquer à l'objet
   */
  private String styleCss = null;

  private String codeApplication;

  /** Le code de la facette de l'application */
  private String codeFacette;

  private Long seqObjetSecurisableParent;

  private Date dateDebutActivite;

  private Date dateFinActivite;

  private String indSecurite;

  private String typeModeAffichage;

  /**
   * Version de la livraison ou doit être livré l'objet sécurisable.
   */
  private String versionLivraison;

  /**
   * Retourne la séquence du service.
   * @return Integer La séquence du service.
   */
  public Long getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  /**
   * Fixer la séquence du service.
   *
   * @param <seqService> la séquence du service.
   */
  public void setSeqObjetSecurisable(Long seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  /**
   * Retourne la clé du service parent.
   * @return String La clé du service parent.
   */
  public ObjetSecurisable getObjetSecurisableParent() {
    return objetSecurisableParent;
  }

  /**
   * Fixer la clé du service parent s'il y  lieu.
   * Si égale à un parent, null est assigné.
   * @param <seqServiceParent> la clé du service parent
   */
  public void setObjetSecurisableParent(ObjetSecurisable objetSecurisableParent) {
    this.objetSecurisableParent = objetSecurisableParent;
  }

  /**
   * Retourne le nom de l'objet sécurisé.
   * @return String Le nom de l'objet sécurisé.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'objet sécurisé.
   *
   * @param <nom> le nom de l'objet sécurisé.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne l'adresse web de l'objet sécurisé.
   * @return String L'adresse web de l'objet sécurisé.
   */
  public String getAdresseWeb() {
    if ((UtilitaireString.isVide(adresseWeb)) && (getNomAction() != null)) {
      StringBuffer adresseComplete = new StringBuffer();
      adresseComplete.append(getNomAction());
      adresseComplete.append(UtilitaireParametreSysteme.getExtensionActionUrl());

      if (getNomParametre() != null) {
        adresseComplete.append("?");
        String methodeParam = UtilitaireParametreSysteme.getMethodeParam();
        String separateur =
            GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.SEPARATEUR_METHODE_ACTION);
        String parametre = getNomParametre();
        if (!UtilitaireString.isVide(separateur)) {
          int indexPosition = getNomParametre().indexOf(separateur);
          if (indexPosition != -1) {
            parametre = parametre.substring(0, indexPosition);
          }
        }
        adresseComplete.append(methodeParam);
        adresseComplete.append("=");
        adresseComplete.append(parametre);
      }

      return adresseComplete.toString();
    }

    return adresseWeb;
  }

  /**
   * Fixer l'adresse web de l'objet sécurisé.
   *
   * @param adresseWeb l'adresse web.
   */
  public void setAdresseWeb(String adresseWeb) {
    this.adresseWeb = adresseWeb;
  }

  /**
   * Retourne le nom de l'action dont l'objet sécurisable est basé.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @return le nom de l'action dont l'objet sécurisable est basé
   */
  public String getNomAction() {
    return nomAction;
  }

  /**
   * Fixer le nom d'action dont l'objet sécurisable est basé.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @param nomAction le nom d'action dont l'objet sécurisable est basé
   */
  public void setNomAction(String nomAction) {
    this.nomAction = nomAction;
  }

  /**
   * Retourne le nom paramètre utilisé pour sécurisé l'objet sécurisable.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @return le nom paramètre
   */
  public String getNomParametre() {
    return nomParametre;
  }

  /**
   * Fixer le nom paramètre utilisé pour sécurisé l'objet sécurisable.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @param nomParametre le nom paramètre
   */
  public void setNomParametre(String nomParametre) {
    this.nomParametre = nomParametre;
  }

  /**
   * Retourne l'ordre d'affichage de l'objet sécurisable dans le cas qu'il
   * corresponde à une section, service ou onglet.
   * @return String L'ordre d'affichage
   */
  public Integer getOrdrePresentation() {
    return ordrePresentation;
  }

  /**
   * Fixer l'ordre d'affichage de l'objet sécurisable dans le cas qu'il
   * corresponde à une section, service ou onglet.
   * @param ordrePresentation L'ordre d'affichage.
   */
  public void setOrdreAffichage(Integer ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  /**
   * Retourne la liste des services enfants
   * @return ArrayList la liste des services enfants
   */
  public List getListeObjetSecurisableEnfants() {
    return listeObjetSecurisableEnfants;
  }

  /**
   * Fixer la liste des services enfants
   * @param listeObjetSecurisableEnfants la liste des services enfants
   */
  public void setListeObjetSecurisableEnfants(List listeObjetSecurisableEnfants) {
    this.listeObjetSecurisableEnfants = listeObjetSecurisableEnfants;
  }

  /**
   * Retourne l'aide contextuelle pour l'objet sécurisable.
   * @return l'aide contextuelle pour l'objet sécurisable
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle pour l'objet sécurisable.
   * @param aideContextuelle l'aide contextuelle pour l'objet sécurisable.
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne si l'objet sécurisable est actif.
   * @return si l'objet sécurisable est actif.
   */
  public boolean isActif() {
    if (getDateFinActivite() == null ||
        (!UtilitaireDate.isDateAvantDateDuJour(getDateFinActivite()))) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fixer si l'objet sécurisable est actif.
   * @param actif si l'objet sécurisable est actif
   */
  public void setActif(boolean actif) {
    if (actif) {
      setDateFinActivite(null);
    } else {
      // Forcer l'inactivation avec la journée d'hier.
      Date hier =
          UtilitaireDate.getDatePlusOuMoinsXHeuresMoins1Minute(UtilitaireDate.getDateJourSansHeure(),
              -24, false);
      setDateFinActivite(hier);
    }
  }

  /**
   * Retourne le nom de l'onglet pour la génération du menu du côté
   * de la couche de présentation.
   * @return le nom de l'onglet
   */
  public String getNomOnglet() {
    return "Onglet" + getSeqObjetSecurisable().toString();
  }

  /**
   * Est-ce que l'objet sécurisable est en lecture seulement
   * @return true si l'objet sécurisable est en lecture seulement.
   */
  public boolean isLectureSeulement() {
    return lectureSeulement;
  }

  /**
   * Fixer true si l'objet sécurisable est en lecture seulement
   * @param lectureSeulement true si l'objet sécurisable est en lecture seulement
   */
  public void setLectureSeulement(boolean lectureSeulement) {
    this.lectureSeulement = lectureSeulement;
  }

  /**
   * Retourne le type de l'objet Java
   * @return le type de l'objet Java
   */
  public String getType() {
    return type;
  }

  /**
   * Fixer le type de l'objet Java
   * @param type le type de l'objet Java
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Fixer le mode d'affichage
   * @param modeAffichage le mode d'affichage
   */
  public void setModeAffichage(String modeAffichage) {
    this.modeAffichage = modeAffichage;
  }

  /**
   * Retourne le mode d'affichage.
   * @return le mode d'affichage.
   */
  public String getModeAffichage() {
    return modeAffichage;
  }

  /**
   * Fixer le div qui va traiter la fonctionnalité ajax
   * @param divAjax le div qui va traiter la fonctionnalité ajax
   */
  public void setDivAjax(String divAjax) {
    this.divAjax = divAjax;
  }

  /**
   * Retourne le div qui va tratier la fonctionnalité ajax.
   * @return le div qui va tratier la fonctionnalité ajax.
   */
  public String getDivAjax() {
    return divAjax;
  }

  /**
   * Fixer le style css que l'objet doit appliquer.
   * @param styleCss le style css que l'objet doit appliquer.
   */
  public void setStyleCss(String styleCss) {
    this.styleCss = styleCss;
  }

  /**
   * Retourne le style CSS que l'objet doit appliquer.
   * @return le style CSS que l'objet doit appliquer.
   */
  public String getStyleCss() {
    return styleCss;
  }

  /**
   * Retourne le code application du composant.
   * @return le code application du composant.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code application du composant.
   * @param codeApplication le code application du composant.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne l'identifiant du composant parent.
   * @return l'identifiant du composant parent.
   */
  public Long getSeqObjetSecurisableParent() {
    return seqObjetSecurisableParent;
  }

  /**
   * Fixer l'identifiant du composant.
   * @param seqObjetSecurisableParent l'identifiant du composant.
   */
  public void setSeqObjetSecurisableParent(Long seqObjetSecurisableParent) {
    this.seqObjetSecurisableParent = seqObjetSecurisableParent;
  }

  /**
   * Fixer l'odre de présentation du composant.
   * @param ordrePresentation l'odre de présentation du composant.
   */
  public void setOrdrePresentation(Integer ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  /**
   * Fixer la clé de l'aide contextuelle associé au composant.
   * @param cleAideContextuelle la clé de l'aide contextuelle associé au composant.
   */
  public void setCleAideContextuelle(String cleAideContextuelle) {
    this.cleAideContextuelle = cleAideContextuelle;
  }

  /**
   * Retourne la clé de l'aide contextuelle associé au composant.
   * @return la clé de l'aide contextuelle associé au composant.
   */
  public String getCleAideContextuelle() {
    return cleAideContextuelle;
  }

  /**
   * Fixer la date de fin d'activité du composant.
   * @param dateFinActivite la date de fin d'activité du composant.
   */
  private void setDateFinActivite(Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  /**
   * Retourne la date de fin d'activité du composant.
   * @return la date de fin d'activité du composant.
   */
  private Date getDateFinActivite() {
    return dateFinActivite;
  }

  /**
   * Fixer  l'indicateur qui spécifie que le composant est sécurité et
   * que la sécurité doit être appliqué.
   * @param indSecurite l'indicateur qui spécifie que le composant est sécurité et
   * que la sécurité doit être appliqué.
   */
  public void setIndSecurite(String indSecurite) {
    this.indSecurite = indSecurite;
  }

  /**
   * Retourne l'indicateur qui spécifie que le composant est sécurité et
   * que la sécurité doit être appliqué.
   * @return l'indicateur qui spécifie que le composant est sécurité et
   * que la sécurité doit être appliqué.
   */
  public String getIndSecurite() {
    return indSecurite;
  }

  /**
   * Fixer le type de mode d'affichage du composant.
   * @param typeModeAffichage le type de mode d'affichage du composant.
   */
  public void setTypeModeAffichage(String typeModeAffichage) {
    this.typeModeAffichage = typeModeAffichage;
  }

  /**
   * Le type de mode d'affichage du composant.
   * @return Le type de mode d'affichage du composant.
   */
  public String getTypeModeAffichage() {
    return typeModeAffichage;
  }

  /**
   * Obtenir la version de la livraison ou doit être livré l'objet sécurisable.
   * @return la version de la livraison ou doit être livré l'objet sécurisable.
   */
  private String getVersionLivraison() {
    return versionLivraison;
  }

  /**
   * Modifier la version de la livraison ou doit être livré l'objet sécurisable.
   * @param versionLivraison Nouvelle version à laquelle doit être activé le composant.
   */
  private void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  /**
   * Fixer le code de la facette de l'application.
   * <p>
   * @param codeFacette le code de la facette de l'application
   * @since SOFI 2.0.6
   */
  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  /**
   * Retourne le code de la facette de l'application.
   * <p>
   * @since SOFI 2.0.6
   */
  public String getCodeFacette() {
    return codeFacette;
  }

  public void setDateDebutActivite(Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public Date getDateDebutActivite() {
    return dateDebutActivite;
  }
}
