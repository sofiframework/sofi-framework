/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import org.sofiframework.objetstransfert.Entite;

/**
 * Entite definissant une permission pouvant etre associe a un role et un objet securisable.
 * @author Jean-Francois Brassard, Nurun inc.
 * @version 3.0
 *
 */
public class Permission extends Entite {

  private static final long serialVersionUID = 3026368781721187085L;

  /**
   * Le code de la permission.
   */
  private String codePermission;

  /**
   * La description de la permission.
   */
  private String description;

  /**
   * Le code de regroupement de la permission.
   */
  private String codeRegroupement;

  /**
   * Fixer le code de permission.
   * @param codePermission le code de permission
   */
  public void setCodePermission(String codePermission) {
    this.codePermission = codePermission;
  }

  /**
   * Retourne le code de permission.
   * @return le code de permission.
   */
  public String getCodePermission() {
    return codePermission;
  }


  /**
   * Fixer la description de la permission.
   * @param description la description de la permission.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la description de la permission.
   * @return la description de la permission.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer le code de regroupement
   * @param codeRegroupement le code de regroupement.
   */
  public void setCodeRegroupement(String codeRegroupement) {
    this.codeRegroupement = codeRegroupement;
  }

  /**
   * Retourne le code de regroupement.
   * @return le code de regroupement.
   */
  public String getCodeRegroupement() {
    return codeRegroupement;
  }

}
