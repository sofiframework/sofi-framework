/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;


import org.sofiframework.modele.entite.EntiteTrace;


/**
 * Objet de transfert décrivant un objet sécurisable de type bloc d'information.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.9
 */
public class AccesApplication extends EntiteTrace {

  private static final long serialVersionUID = 38169666040799232L;

  /**
   * Détail de l'application dont l'utilisateur accède.
   */
  private Application application;

  /**
   * Le type d'utilisateur qui accède
   * à l'application.
   */
  private String codeTypeUtilisateur;

  /**
   * Le statut de l'utilisateur qui
   * accède à l'application.
   */
  private String codeStatut;

  /**
   * Le code d'organisation associé à l'utilisateur qui
   * accède à l'application.
   */
  private String codeOrganisation;

  /**
   * Le type d'organisation associé à l'utilisateur qui
   * accède à l'application.
   */
  private String typeOrganisation;

  /**
   * Le code application que l'utilise à accès.
   */
  private String codeApplication;

  /**
   * Le code utilisateur.
   */
  private String codeUtilisateur;


  /** Construteur par défaut */
  public AccesApplication() {
  }

  /**
   * Constructeur avec les paramètres minimum a remplir pour recherche.
   * @param codeUtilisateur
   * @param codeApplication
   * @param codeTypeUtilisateur
   * @param codeStatut
   */
  public AccesApplication(String codeUtilisateur, String codeApplication) {
    this.codeUtilisateur = codeUtilisateur;
    this.codeApplication = codeApplication;
  }

  /**
   * Constructeur avec les paramètres minimum a remplir pour sauvegarde.
   * @param codeUtilisateur
   * @param codeApplication
   * @param codeTypeUtilisateur
   * @param codeStatut
   */
  public AccesApplication(String codeUtilisateur, String codeApplication, String codeTypeUtilisateur, String codeStatut) {
    this.codeUtilisateur = codeUtilisateur;
    this.codeApplication = codeApplication;
    this.codeTypeUtilisateur = codeTypeUtilisateur;
    this.codeStatut = codeStatut;
  }

  /**
   * Fixer le type d'utilisateur qui accède à l'application.
   * @param typeUtilisateur le type d'utilisateur qui accède à l'application.
   */
  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  /**
   * Retourne le type d'utilisateur qui accède à l'application.
   * @return le type d'utilisateur qui accède à l'application.
   */
  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  /**
   * Fixer le statut de l'utilisateur qui accède à l'application.
   * @param statut
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Retourne le statut de l'utilisateur qui accède à l'application.
   * @return le statut de l'utilisateur qui accède à l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code d'organisation associé à l'utilisateur qui accède à l'application.
   * @param codeOrganisation
   */
  public void setCodeOrganisation(String codeOrganisation) {
    this.codeOrganisation = codeOrganisation;
  }

  /**
   * Retourne le code de l'organisation qui est strictement
   * lié à l'utilisateur pour l'application.
   * @return le code de l'organisation qui est strictement lié à l'utilisateur pour l'application.
   */
  public String getCodeOrganisation() {
    return codeOrganisation;
  }

  /**
   * Fixer le type de l'organisation qui est strictement
   * lié à l'utilisateur pour l'application.
   * @param typeOrganisation le code de l'organisation qui est strictement lié à l'utilisateur pour l'application.
   */
  public void setTypeOrganisation(String typeOrganisation) {
    this.typeOrganisation = typeOrganisation;
  }

  /**
   * Retourne le type de l'organisation qui est strictement
   * lié à l'utilisateur pour l'application.
   * @return le type de l'organisation qui est strictement lié à l'utilisateur pour l'application.
   */
  public String getTypeOrganisation() {
    return typeOrganisation;
  }

  /**
   * Fixer l'application dont l'utilisateur à un détail d'accès.
   * @param application  l'application dont l'utilisateur à un détail d'accès.
   */
  public void setApplication(Application application) {
    this.application = application;
  }

  /**
   * Retourne l'application  dont l'utilisateur à un détail d'accès.
   * @return  l'application dont l'utilisateur à un détail d'accès.
   */
  public Application getApplication() {
    return application;
  }

  /**
   * @param codeApplication the codeApplication to set
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * @return the codeApplication
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * @param codeUtilisateur the codeUtilisateur to set
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * @return the codeUtilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }
}
