package org.sofiframework.application.securite.objetstransfert;

import java.util.Date;

import org.sofiframework.objetstransfert.ObjetTransfert;

public class CertificatAcces extends ObjetTransfert {

  private static final long serialVersionUID = 7025741355926067815L;

  private String certificat = null;

  private String codeUtilisateur = null;

  private Date dateConnexion = null;

  private Date dateDeconnexion = null;

  private String codeApplication = null;

  private String ip = null;

  private String userAgent = null;
  
  private String urlReferer = null;
  
  private String codeClient = null;
  
  private String codeFournisseurAcces = null;
  
  private java.util.Date dateDernierAcces = null;
  
  private Integer nbAcces = null;
  
  private String etiquette = null;

  /**
   * @param certificat the certificat to set
   */
  public void setCertificat(String certificat) {
    this.certificat = certificat;
  }

  /**
   * @return the certificat
   */
  public String getCertificat() {
    return certificat;
  }

  /**
   * @param codeUtilisateur the codeUtilisateur to set
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * @return the codeUtilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * @param dateConnexion the dateConnexion to set
   */
  public void setDateConnexion(Date dateConnexion) {
    this.dateConnexion = dateConnexion;
  }

  /**
   * @return the dateConnexion
   */
  public Date getDateConnexion() {
    return dateConnexion;
  }

  /**
   * @param dateDeconnexion the dateDeconnexion to set
   */
  public void setDateDeconnexion(Date dateDeconnexion) {
    this.dateDeconnexion = dateDeconnexion;
  }

  /**
   * @return the dateDeconnexion
   */
  public Date getDateDeconnexion() {
    return dateDeconnexion;
  }

  /**
   * @param codeApplication the codeApplication to set
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * @return the codeApplication
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * @param ip the ip to set
   */
  public void setIp(String ip) {
    this.ip = ip;
  }

  /**
   * @return the ip
   */
  public String getIp() {
    return ip;
  }

  /**
   * @param userAgent the userAgent to set
   */
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  /**
   * @return the userAgent
   */
  public String getUserAgent() {
    return userAgent;
  }

  /**
   * Retourne l'url du referer.
   * @return l'url du referer.
   */
  public String getUrlReferer() {
    return urlReferer;
  }

  /**
   * Fixer l'url du referer.
   * @param urlReferer l'url du referer.
   */
  public void setUrlReferer(String urlReferer) {
    this.urlReferer = urlReferer;
  }

  /**
   * Retourne le code client associé à l'authentification.
   * @return le code client associé à l'authentification.
   */
  public String getCodeClient() {
    return codeClient;
  }

  /**
   * Fixer le code client associé à l'authentification.
   * @param codeClient le code client associé à l'authentification.
   */
  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * Retourne le fournisseur d'accès.
   * @return le founisseur d'accès.
   */
  public String getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  /**
   * Fixer le fournisseur d'accès.
   * @param codeFournisseurAcces le fournisseur d'accès.
   */
  public void setCodeFournisseurAcces(String codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  public java.util.Date getDateDernierAcces() {
    return dateDernierAcces;
  }

  public void setDateDernierAcces(java.util.Date dateDernierAcces) {
    this.dateDernierAcces = dateDernierAcces;
  }

  public String getEtiquette() {
    return etiquette;
  }

  public void setEtiquette(String etiquette) {
    this.etiquette = etiquette;
  }

  public Integer getNbAcces() {
    return nbAcces;
  }

  public void setNbAcces(Integer nbAcces) {
    this.nbAcces = nbAcces;
  }

}
