/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe qui sert d'objet de transfert du modele vers la couche de presentation
 * pour les informations concernant un role d'un utilisateur.
 * <p>
 * @author Jean-François Brassard (Nurun Inc.)
 * @version 2.0
 * @since 3.0.2 Ajout des dates d'activités et de la journalisation.
 */
public class UtilisateurRole extends ObjetTransfert {

  private static final long serialVersionUID = -4214559420571865661L;

  private Long id;
  private String codeRole;
  private String codeApplication;
  private String codeUtilisateur;
  private String codeClient;
  private java.util.Date dateDebutActivite;
  private java.util.Date dateFinActivite;
  private String descriptionPeriodeActivite;
  private String creePar;
  private java.util.Date dateCreation;
  private String modifiePar;
  private java.util.Date dateModification;

  private Role role;



  public UtilisateurRole() {
    this.setCle(new String[]{"id"});
  }

  public UtilisateurRole(String codeApplication, String codeUtilisateur, String codeRole, String codeClient) {
    this.setCodeApplication(codeApplication);
    this.setCodeUtilisateur(codeUtilisateur);
    this.setCodeRole(codeRole);
    this.setCodeClient(codeClient);
    this.setCle(new String[]{"id"});
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodeRole() {
    return codeRole;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getCodeClient() {
    return codeClient;
  }

  /**
   * @param dateDebutActivite the dateDebutActivite to set
   */
  public void setDateDebutActivite(java.util.Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  /**
   * @return the dateDebutActivite
   */
  public java.util.Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  /**
   * @param dateFinActivite the dateFinActivite to set
   */
  public void setDateFinActivite(java.util.Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  /**
   * @return the dateFinActivite
   */
  public java.util.Date getDateFinActivite() {
    return dateFinActivite;
  }

  /**
   * @param descriptionPeriodeActivite the descriptionPeriodeActivite to set
   */
  public void setDescriptionPeriodeActivite(String descriptionPeriodeActivite) {
    this.descriptionPeriodeActivite = descriptionPeriodeActivite;
  }

  /**
   * @return the descriptionPeriodeActivite
   */
  public String getDescriptionPeriodeActivite() {
    return descriptionPeriodeActivite;
  }

  /**
   * @param creePar the creePar to set
   */
  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  /**
   * @return the creePar
   */
  public String getCreePar() {
    return creePar;
  }

  /**
   * @param dateCreation the dateCreation to set
   */
  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  /**
   * @return the dateCreation
   */
  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  /**
   * @param modifiePar the modifiePar to set
   */
  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  /**
   * @return the modifiePar
   */
  public String getModifiePar() {
    return modifiePar;
  }

  /**
   * @param dateModification the dateModification to set
   */
  public void setDateModification(java.util.Date dateModification) {
    this.dateModification = dateModification;
  }

  /**
   * @return the dateModification
   */
  public java.util.Date getDateModification() {
    return dateModification;
  }

  /**
   * @param role the role to set
   */
  public void setRole(Role role) {
    this.role = role;
  }

  /**
   * @return the role
   */
  public Role getRole() {
    return role;
  }
}