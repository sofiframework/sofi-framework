/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.objetstransfert;

import java.util.Date;

import org.sofiframework.objetstransfert.Entite;

/**
 * @author Jérôme Fiolleau, Nurun inc.
 * @date 09-10-07
 *
 */
public class HistoriqueMotPasse extends Entite {

  /**
   * 
   */
  private static final long serialVersionUID = 7928655296750584603L;

  private String codeUtilisateur;

  private String motPasse;

  private Date dateCreation;

  /**
   * @return the codeUtilisateur
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * @param codeUtilisateur the codeUtilisateur to set
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * @return the motPasse
   */
  public String getMotPasse() {
    return motPasse;
  }

  /**
   * @param motPasse the motPasse to set
   */
  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  /**
   * @return the dateCreation
   */
  @Override
  public Date getDateCreation() {
    return dateCreation;
  }

  /**
   * @param dateCreation the dateCreation to set
   */
  @Override
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

}
