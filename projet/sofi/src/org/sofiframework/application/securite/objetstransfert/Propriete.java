package org.sofiframework.application.securite.objetstransfert;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Propriété qui est associée à l'utilisateur. Une propriété peut
 * être défini pour un client particulier ou tous les clients. Dans
 * le cas ou la propriété est pour tous les clients, le valeurs
 * codeClient est null.
 * 
 * @author Jean-Maxime Pelletier
 */
public class Propriete extends ObjetTransfert {

  private static final long serialVersionUID = 7181527928713984191L;

  public static String TYPE_DOMAINE = "domaine:";

  private Long id;

  private Integer version;

  private String valeur;

  private Long utilisateurId;

  private String creePar;

  private Date dateCreation;

  private String modifiePar;

  private Date dateModification;

  private Long metadonneeProprieteId;

  private String typeDonnee;

  private String nom;

  private String nom2;

  public String getNom2() {
    return nom2;
  }

  public void setNom2(String nom2) {
    this.nom2 = nom2;
  }

  private String codeClient;

  private Long ordrePresentation;
  
  private Boolean obligatoire;
  
  private String codeDomaine;
  
  private Long ordrePresentationCategorie;
  
  private String refUtilisateurExterne;
  
  private String refProperieteExterne;
  
  private Boolean prioritaire;

  public Propriete() {
    this.setCle(new String[] { "id" });
  }

  public boolean isDomaineValeur() {
    return this.typeDonnee != null && this.typeDonnee.startsWith(TYPE_DOMAINE);
  }

  public String getNomDomaineValeur() {
    String nom = null;

    if (this.isDomaineValeur()) {
      nom = StringUtils.substringAfter(this.typeDonnee, TYPE_DOMAINE);
    }

    return nom;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public Long getUtilisateurId() {
    return utilisateurId;
  }

  public void setUtilisateurId(Long utilisateurId) {
    this.utilisateurId = utilisateurId;
  }

  public String getCreePar() {
    return creePar;
  }

  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getModifiePar() {
    return modifiePar;
  }

  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  public Date getDateModification() {
    return dateModification;
  }

  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }

  public String getTypeDonnee() {
    return typeDonnee;
  }

  public void setTypeDonnee(String typeDonnee) {
    this.typeDonnee = typeDonnee;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public Long getMetadonneeProprieteId() {
    return metadonneeProprieteId;
  }

  public void setMetadonneeProprieteId(Long metadonneeProprieteId) {
    this.metadonneeProprieteId = metadonneeProprieteId;
  }

  public Long getOrdrePresentation() {
    return ordrePresentation;
  }

  public void setOrdrePresentation(Long ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  public String getCodeDomaine() {
    return codeDomaine;
  }

  public void setCodeDomaine(String codeDomaine) {
    this.codeDomaine = codeDomaine;
  }

  public Boolean getObligatoire() {
    return obligatoire;
  }

  public void setObligatoire(Boolean obligatoire) {
    this.obligatoire = obligatoire;
  }

  public Long getOrdrePresentationCategorie() {
    return ordrePresentationCategorie;
  }

  public void setOrdrePresentationCategorie(Long ordrePresentationCategorie) {
    this.ordrePresentationCategorie = ordrePresentationCategorie;
  }

  public String getRefUtilisateurExterne() {
    return refUtilisateurExterne;
  }

  public void setRefUtilisateurExterne(String refUtilisateurExterne) {
    this.refUtilisateurExterne = refUtilisateurExterne;
  }

  public String getRefProperieteExterne() {
    return refProperieteExterne;
  }

  public void setRefProperieteExterne(String refProperieteExterne) {
    this.refProperieteExterne = refProperieteExterne;
  }

  public Boolean getPrioritaire() {
    return prioritaire;
  }

  public void setPrioritaire(Boolean prioritaire) {
    this.prioritaire = prioritaire;
  }

}
