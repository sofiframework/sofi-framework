package org.sofiframework.application.securite.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre sur l'extraction des certificats.
 * @author jfbrassard
 * @version 3.0.2
 */

public class FiltreCertificat extends ObjetFiltre {
  private static final long serialVersionUID = 182716739812766698L;
  
  private String certificat = null;

  // Liste de code utilisateur a extraire.
  private String[] codeUtilisateur;

  // La date et l'heure de début de connexion a extraire.
  private java.util.Date dateConnexionDebut;
  // La date et l'heure de fin de connexion a extraire.
  private java.util.Date dateConnexionFin;
  
  // La date et l'heure de début du dernier accès a extraire.
  private java.util.Date dateDernierAccesDebut;
  // La date et l'heure de fin du dernier accès a extraire.
  private java.util.Date dateDernierAccesFin;

  private java.util.Date dateConnexion;

  // Le code du rôle de l'utilisateur.
  private String[] codeRole;

  // Le ou les codes clients dont on désire extraire des utilisateurs.
  private String[] codeClient;
  
  // Le ou les codes clients dont on désire extraire des utilisateurs.
  private String[] codeFournisseurAcces;

  //
  private String userAgent;

  //
  private String ip;
  
  private String urlReferer;
  
  private java.util.Date dateDernierAcces;
  
  private Integer nbAcces; 
  
  private Integer nbAccesMin;
  
  private Integer nbAccesMax;
  
  private String etiquette;


  public FiltreCertificat() {
    super();
    ajouterIntervalle("dateDernierAccesDebut", "dateDernierAccesFin",
        "dateDernierAcces");
    
    ajouterIntervalle("dateConnexionDebut", "dateConnexionFin",
        "dateConnexion");
   
    ajouterIntervalle("nbAccesMin", "nbAccesMax", "nbAcces");    
    
    ajouterExclureRechercheAvantApresValeur(new String[]{"certificat"});
    
  }

  /**
   * @param codeUtilisateur the codeUtilisateur to set
   */
  public void setCodeUtilisateur(String[] codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * @return the codeUtilisateur
   */
  public String[] getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * @param dateConnexionDebut the dateConnexionDebut to set
   */
  public void setDateConnexionDebut(java.util.Date dateConnexionDebut) {
    this.dateConnexionDebut= dateConnexionDebut;
  }

  /**
   * @return the dateConnexionDebut
   */
  public java.util.Date getDateConnexionDebut() {
    return dateConnexionDebut;
  }

  /**
   * @param dateConnexionFin the dateHeureConnexionFin to set
   */
  public void setDateConnexionFin(java.util.Date dateConnexionFin) {
    this.dateConnexionFin = dateConnexionFin;
  }

  /**
   * @return the dateHeureConnexionFin
   */
  public java.util.Date getDateConnexionFin() {
    return dateConnexionFin;
  }

  /**
   * @param dateHeureConnexion the dateHeure to set
   */
  public void setDateConnexion(java.util.Date dateConnexion) {
    this.dateConnexion = dateConnexion;
  }

  /**
   * @return the dateHeureConnexion
   */
  public java.util.Date getDateConnexion() {
    return dateConnexion;
  }

  /**
   * @return the codeRole
   */
  public String[] getCodeRole() {
    return codeRole;
  }

  /**
   * @param codeRole the codeRole to set
   */
  public void setCodeRole(String[] codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * @return the codeClient
   */
  public String[] getCodeClient() {
    return codeClient;
  }

  /**
   * @param codeClient the codeClient to set
   */
  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * @return the userAgent
   */
  public String getUserAgent() {
    return userAgent;
  }

  /**
   * @param userAgent the userAgent to set
   */
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  /**
   * @return the ip
   */
  public String getIp() {
    return ip;
  }

  /**
   * @param ip the ip to set
   */
  public void setIp(String ip) {
    this.ip = ip;
  }

  /**
   * Retourn l'url du referer
   * @return l'url du referer
   */
  public String getUrlReferer() {
    return urlReferer;
  }

  /**
   * Fixer l'url du referer
   * @param urlReferer url du referer
   */
  public void setUrlReferer(String urlReferer) {
    this.urlReferer = urlReferer;
  }

  public String getCertificat() {
    return certificat;
  }

  public void setCertificat(String certificat) {
    this.certificat = certificat;
  }

  public String[] getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  public void setCodeFournisseurAcces(String[] codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  public java.util.Date getDateDernierAcces() {
    return dateDernierAcces;
  }

  public void setDateDernierAcces(java.util.Date dateDernierAcces) {
    this.dateDernierAcces = dateDernierAcces;
  }

  public String getEtiquette() {
    return etiquette;
  }

  public void setEtiquette(String etiquette) {
    this.etiquette = etiquette;
  }

  public java.util.Date getDateDernierAccesDebut() {
    return dateDernierAccesDebut;
  }

  public void setDateDernierAccesDebut(java.util.Date dateDernierAccesDebut) {
    this.dateDernierAccesDebut = dateDernierAccesDebut;
  }

  public java.util.Date getDateDernierAccesFin() {
    return dateDernierAccesFin;
  }

  public void setDateDernierAccesFin(java.util.Date dateDernierAccesFin) {
    this.dateDernierAccesFin = dateDernierAccesFin;
  }

  public Integer getNbAcces() {
    return nbAcces;
  }

  public void setNbAcces(Integer nbAcces) {
    this.nbAcces = nbAcces;
  }

  public Integer getNbAccesMin() {
    return nbAccesMin;
  }

  public void setNbAccesMin(Integer nbAccesMin) {
    this.nbAccesMin = nbAccesMin;
  }

  public Integer getNbAccesMax() {
    return nbAccesMax;
  }

  public void setNbAccesMax(Integer nbAccesMax) {
    this.nbAccesMax = nbAccesMax;
  }

}
