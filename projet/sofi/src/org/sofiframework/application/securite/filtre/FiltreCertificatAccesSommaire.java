package org.sofiframework.application.securite.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;


public class FiltreCertificatAccesSommaire extends ObjetFiltre {

  private static final long serialVersionUID = 8239157034538243416L;

  private java.util.Date dateDebut;

  private java.util.Date dateFin;
  
  private String userAgent;
  
  private String urlReferer;
  
  private String courriel;
  
  private String etiquette;
  
  private Boolean applicationSeulement; 
  
  private Integer nbConnexionMin;
  
  // Le ou les codes clients dont on désire extraire des utilisateurs.
  private String[] codeClient;

  public FiltreCertificatAccesSommaire() {
    ajouterAttributAExclure("dateDebut");
    ajouterAttributAExclure("dateFin");
  }

  /**
   * @param dateDebut the dateDebut to set
   */
  public void setDateDebut(java.util.Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  /**
   * @return the dateDebut
   */
  public java.util.Date getDateDebut() {
    return dateDebut;
  }

  /**
   * @param dateFin the dateFin to set
   */
  public void setDateFin(java.util.Date dateFin) {
    this.dateFin = dateFin;
  }

  /**
   * @return the dateFin
   */
  public java.util.Date getDateFin() {
    return dateFin;
  }

  public String[] getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public String getUrlReferer() {
    return urlReferer;
  }

  public void setUrlReferer(String urlReferer) {
    this.urlReferer = urlReferer;
  }

  public String getCourriel() {
    return courriel;
  }

  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  public String getEtiquette() {
    return etiquette;
  }

  public void setEtiquette(String etiquette) {
    this.etiquette = etiquette;
  }

  public Boolean getApplicationSeulement() {
    return applicationSeulement;
  }

  public void setApplicationSeulement(Boolean applicationSeulement) {
    this.applicationSeulement = applicationSeulement;
  }

  public Integer getNbConnexionMin() {
    return nbConnexionMin;
  }

  public void setNbConnexionMin(Integer nbConnexionMin) {
    this.nbConnexionMin = nbConnexionMin;
  }

}
