package org.sofiframework.application.securite.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre pour l'extraction des rôles selon des
 * critère de recherche.
 * @author jfbrassard
 * @since 3.0.2
 */
public class FiltreRole extends ObjetFiltre {

  private static final long serialVersionUID = 1097880596750915863L;

  // Le code de rôle
  private String code;

  // Le nom du rôle
  private String nom;

  private String codeFacette;

  // Le code d'application correspondant du rôle.
  private String codeApplication;

  // Le code d'application parent.
  private String codeApplicationParent;

  // Le code du rôle parent.
  private String codeRoleParent;

  /**
   * Le type utilisateur qui spécifie le rôle.
   */
  private String[] codeTypeUtilisateur;


  private boolean consultationSeulement;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeFacette() {
    return codeFacette;
  }

  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  public String getCodeRoleParent() {
    return codeRoleParent;
  }

  public void setCodeRoleParent(String codeRoleParent) {
    this.codeRoleParent = codeRoleParent;
  }

  public String[] getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  public void setCodeTypeUtilisateur(String[] codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  public boolean isConsultationSeulement() {
    return consultationSeulement;
  }

  public void setConsultationSeulement(boolean consultationSeulement) {
    this.consultationSeulement = consultationSeulement;
  }

}
