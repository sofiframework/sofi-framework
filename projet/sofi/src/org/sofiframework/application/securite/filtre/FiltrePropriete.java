package org.sofiframework.application.securite.filtre;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltrePropriete extends ObjetFiltre {

  private static final long serialVersionUID = -8843118161468143929L;

  private String nom;

  private String valeur;

  private String typeDonnee;

  private String codeClient;
  
  private String codeDomaine;

  private Date dateCreationDebut;
  private Date dateCreationFin;
  private Date dateCreation;

  public FiltrePropriete() {
    this.ajouterAttributAExclure("domaineValeur");
    this.ajouterAttributAExclure("nomDomaineValeur");
    
    ajouterIntervalle("dateCreationDebut", "dateCreationFin", "dateCreation");
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getTypeDonnee() {
    return typeDonnee;
  }

  public void setTypeDonnee(String typeDonnee) {
    this.typeDonnee = typeDonnee;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public boolean isDomaineValeur() {
    return this.typeDonnee != null && this.typeDonnee.startsWith(Propriete.TYPE_DOMAINE);
  }

  public String getNomDomaineValeur() {
    String nom = null;

    if (this.isDomaineValeur()) {
      nom = StringUtils.substringAfter(this.typeDonnee, Propriete.TYPE_DOMAINE);
    }

    return nom;
  }

  public String getCodeDomaine() {
    return codeDomaine;
  }

  public void setCodeDomaine(String codeDomaine) {
    this.codeDomaine = codeDomaine;
  }

  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }

  public void setDateCreationDebut(Date dateCreationDebut) {
    this.dateCreationDebut = dateCreationDebut;
  }

  public Date getDateCreationFin() {
    return dateCreationFin;
  }

  public void setDateCreationFin(Date dateCreationFin) {
    this.dateCreationFin = dateCreationFin;
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
}
