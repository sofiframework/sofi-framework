/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.filtre;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.securite.objetstransfert.AccesExterne;
import org.sofiframework.modele.filtre.FiltreTrace;

/**
 * Filtre sur les utilisateurs de l'infrastructure SOFI.
 * <p>
 * Il est possible de filtrer sur les utilisateurs ainsi que sur les
 * autorisations (rôles) qui lui ont été alloués.
 * 
 * @author Jean-François Brassard
 * @version SOFI 2.0.3
 */
public class FiltreUtilisateur extends FiltreTrace {

  private static final long serialVersionUID = 2586638879165754318L;

  // Le nom de l'utilisateur.
  private String nom;

  // Le prénom de l'utilisteur.
  private String prenom;

  // Le code de l'utilisateur.
  private String codeUtilisateur;

  /**
   * Le nom d'utilisateur.
   */
  private String nomUtilisateur;

  // Le code applicatif de l'utilisteur.
  private String codeUtilisateurApplicatif;

  // Le courriel de l'utilisateur
  private String courriel;

  // Le statut de l'utilisateur.
  private String[] codeStatut;

  // Le code application de l'utilisateur.
  private String[] codeApplication;

  // Le code du rôle de l'utilisateur.
  private String[] codeRole;

  // Est-ce un rôle en consultation seulement.
  private Boolean roleConsultationSeulement;

  // Est-ce un rôle actif selon la période d'activité pour l'utilisateur.
  private Boolean roleActifSelonPeriode = Boolean.TRUE;

  // Le statut du rôle.
  private String[] statutRole;

  // La locale de l'utilisateur
  private String[] codeLocale;

  /**
   * Permet de rechercher les utilisateurs qui ont une propriété qui correspond
   * aux critère du filtre propriété.
   */
  private List<FiltrePropriete> listePropriete;
  
  /**
   * Permet de rechercher les utilisateurs en fonction de leurs accès externes
   */
  private List<AccesExterne> listeAccesExterne;

  // Le ou les codes clients dont on désire extraire des utilisateurs.
  private String[] codeClient;

  // Extraire les utilisateurs pour un ou des rôles avec code client seulement.
  private Boolean roleAvecCodeClientSeulement = Boolean.FALSE;

  // Extraire les utilisateurs sans roles mais cree par l'utilisateur en cours.
  private Boolean sansRoleEtCreeParUtilisateurEnTraitement = Boolean.FALSE;

  // Le ou les codes d'organisations dont on désire extraire des utilisateurs.
  private String[] codeOrganisation;

  /**
   * Le code du fournisseur d'accès de l'authentification.
   */
  private String[] codeFournisseurAcces;
  
  /**
   * Le ou les codes d'accès externe.
   */
  private String[] codeAccesExterne;  
  
  /**
   * Le type d'accès externe 
   */
  private String typeAccesExterne;
  
  /**
   * Spécifie true pour extraire aucun accès externe.
   */
  private Boolean aucunAccesExterne = Boolean.FALSE;

  /**
   * Le ou les identifiants qui peut être utilisé comme référence sur un
   * utilisateur dans d'autres systèmes.
   */
  private Long[] id;

  /**
   * Critères d'intervalle de date de naissance. Bornes début et fin.
   */
  private Date dateNaissanceDebut;

  private Date dateNaissanceFin;

  /**
   * Code postal de l'utilisateur.
   */
  private String codePostal;

  /*
   * Code de pays de l'utilisateur.
   */
  private String codePays;
  
  /*
   * Code de region de l'utilisateur.
   */
  private String[] codeRegion; 
  
  /*
   * Code sous region de l'utilisateur.
   */
  private String codeSousRegion;
  
  /*
   * ID de la ville de l'utilisateur.
   */
  private Long[] idVille;    
  
  /**
   * Si l'utilisateur est associé à une ville
   */
  private Boolean isVilleExiste = false;

  /**
   * Utilisateur supprimé inclut dans la recherche?
   */
  private Boolean isDeleted = false;
  /**
   * Le Sexe de l'utilisateur
   */
  private String sexe;

  /**
   * Le code du groupe d'age.
   */
  private String codeGroupeAge;

  // La date et l'heure de début du denier accès.
  private java.util.Date dateDernierAccesDebut;
  // La date et l'heure de fin du dernier accès.
  private java.util.Date dateDernierAccesFin;
  // La date et l'heure du dernier accès.
  private java.util.Date dateDernierAcces;

  // La date et l'heure de début du denier accès.
  private java.util.Date dateCreationDebut;
  // La date et l'heure de fin du dernier accès.
  private java.util.Date dateCreationFin;
  // La date et l'heure de la création de compte.
  private java.util.Date dateCreation;
  
  private String codeDernierAccesExterne;
  
  private String codeDernierAccesClient;

  public FiltreUtilisateur() {
    super();
    ajouterAttributAExclure("roleActifSelonPeriode");
    ajouterAttributAExclure("aucunAccesExterne");
    ajouterAttributAExclure("modeRechercheUtilisateurSansFiltreRoleApplication");
    ajouterAttributAExclure("isVilleExiste");
    ajouterRechercheAvantApresValeur(new String[] { 
        "nom", "prenom","courriel", "codeUtilisateur", "codeUtilisateurApplicatif", "codePostal", "nomUtilisateur"
    });
    ajouterIntervalle("dateDernierAccesDebut", "dateDernierAccesFin", "dateDernierAcces");
    ajouterIntervalle("dateCreationDebut", "dateCreationFin", "dateCreation");
    ajouterIntervalle("dateNaissanceDebut", "dateNaissanceFin", "dateNaissance");

  }

  /**
   * Fixer le nom (ou une partie) de l'utilisateur.
   * 
   * @param nom
   *          le nom (ou une partie) de l'utilisateur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom (ou une partie) de l'utilisateur.
   * 
   * @return le nom (ou une partie) de l'utilisateur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le prénom (ou une partie) de l'utilisateur.
   * 
   * @param prenom
   *          le prénom (ou une partie) de l'utilisateur.
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Retourne le prénom (ou une partie) de l'utilisateur.
   * 
   * @return le prénom (ou une partie) de l'utilisateur.
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le code utilisateur.
   * 
   * @param codeUtilisateur
   *          le code utilisateur.
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Retourne le code utilisateur.
   * 
   * @return le code utilisateur.
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer le code utilisateur applicatif.
   * <p>
   * A utiliser lorsque le code utilisateur est technique et non fonctionnel.
   * 
   * @param codeUtilisateurApplicatif
   *          le code utilisateur applicatif.
   */
  public void setCodeUtilisateurApplicatif(String codeUtilisateurApplicatif) {
    this.codeUtilisateurApplicatif = codeUtilisateurApplicatif;
  }

  /**
   * Retourne le code utilisateur applicatif.
   * <p>
   * A utiliser lorsque le code utilisateur est technique et non fonctionnel.
   * 
   * @return le code utilisateur applicatif.
   */
  public String getCodeUtilisateurApplicatif() {
    return codeUtilisateurApplicatif;
  }

  /**
   * Fixer un ou des statuts pour les utilisateurs dont on désire extraire.
   * 
   * @param statut
   *          liste des statuts a filtrer.
   */
  public void setCodeStatut(String[] codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Retourne un ou des statuts pour les utilisateurs dont on désire extraire.
   * 
   * @returnt liste des statuts a filtrer.
   */
  public String[] getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer un ou des roles pour les utilisateurs dont on désire extraire.
   * 
   * @param codeRole
   *          liste des code de roles.
   */
  public void setCodeRole(String[] codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Retourne un ou des roles pour les utilisateurs dont on désire extraire.
   * 
   * @return liste des roles a filtrer.
   */
  public String[] getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer true si vous désirez extraire des utilisateurs ayant des rôles en
   * consultation seulement.
   * 
   * @param roleConsultationSeulement
   *          true si vous désirez extraire des utilisateurs ayant des rôles en
   *          consultation seulement.
   */
  public void setRoleConsultationSeulement(Boolean roleConsultationSeulement) {
    this.roleConsultationSeulement = roleConsultationSeulement;
  }

  /**
   * Indique si vous désirez extraire des utilisateurs ayant des rôles en
   * consultation seulement.
   * 
   * @return true si vous désirez extraire des utilisateurs ayant des rôles en
   *         consultation seulement.
   */
  public Boolean getRoleConsultationSeulement() {
    return roleConsultationSeulement;
  }

  /**
   * Fixer si vous désirez extraire des utilisateurs ayant des roles actifs
   * selon la période d'activation.
   * <p>
   * Par défaut la valeur est true.
   * 
   * @param roleActifSelonPeriode
   *          true
   */
  public void setRoleActifSelonPeriode(Boolean roleActifSelonPeriode) {
    this.roleActifSelonPeriode = roleActifSelonPeriode;
  }

  /**
   * Indique si vous désirez extraire des utilisateurs ayant des roles actifs
   * selon la période d'activation.
   * 
   * @return true si vous désirez extraire des utilisateurs ayant des roles
   *         actifs selon la période d'activation.
   */
  public Boolean getRoleActifSelonPeriode() {
    return roleActifSelonPeriode;
  }

  /**
   * Fixer un ou des applications pour les utilisateurs dont on désire extraire.
   * 
   * @param codeApplication
   *          liste des applications a filtrer.
   */
  public void setCodeApplication(String[] codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le ou les applications pour les utilisateurs dont on désire
   * extraire.
   * 
   * @return liste des applications a filtrer.
   */
  public String[] getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer un ou des statuts des roles pour les utilisateurs dont on désire
   * extraire.
   * 
   * @param statutRole
   *          liste des statuts de roles a filtrer.
   */
  public void setStatutRole(String[] statutRole) {
    this.statutRole = statutRole;
  }

  /**
   * Retourne un ou des statuts des roles pour les utilisateurs dont on désire
   * extraire.
   * 
   * @return liste des statuts de roles a filtrer.
   */
  public String[] getStatutRole() {
    return statutRole;
  }

  /**
   * Indique si le filtrage est fait seulement sur les utilisateurs sans
   * condition sur les rôles et applications.
   * 
   * @return true si le filtrage est fait seulement sur les utilisateurs sans
   *         condition sur les rôles et applications.
   */
  public boolean isModeRechercheUtilisateurSansFiltreRoleApplication() {
    return getCodeApplication() == null && getCodeRole() == null
        && getStatutRole() == null && getRoleConsultationSeulement() == null;

  }

  /**
   * Fixer la liste des code clients dont on désire extraire des utilisateurs.
   * 
   * @param codeClient
   *          la liste des code clients dont on désire extraire des
   *          utilisateurs.
   */
  public void setCodeClient(String[] codeClient) {
    this.codeClient = codeClient;
  }

  /**
   * Retourne la liste des code clients dont on désire extraire des
   * utilisateurs.
   * 
   * @return la liste des code clients dont on désire extraire des utilisateurs.
   */
  public String[] getCodeClient() {
    return codeClient;
  }

  /**
   * Fixer la liste des code d'organisation dont l'utilisateur appartient.
   * 
   * @param codeOrganisation
   */
  public void setCodeOrganisation(String[] codeOrganisation) {
    this.codeOrganisation = codeOrganisation;
  }

  /**
   * Retourne la liste des code d'organisation dont l'utilisateur appartient.
   * 
   * @return la liste des code d'organisation dont l'utilisateur appartient.
   */
  public String[] getCodeOrganisation() {
    return codeOrganisation;
  }

  /**
   * Retourne le ou les code de fournisseurs à rechercher.
   * 
   * @return le ou les code de fournisseur a rechercher.
   * @since 3.0.2
   */
  public String[] getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  /**
   * Fixer le ou les code de fournisseur a rechercher.
   * 
   * @param codeFournisseurAcces
   *          le ou les code de fournisseur a rechercher.
   * @since 3.0.2
   */
  public void setCodeFournisseurAcces(String[] codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  /**
   * Retourne la liste des identifiants unique d'utilisateur.
   * 
   * @return la liste des identifiants unique d'utilisateur.
   * @since 3.0.2
   */
  public Long[] getId() {
    return id;
  }

  /**
   * Fixer la liste des identifiants unique d'utilisateur.
   * 
   * @param id
   *          la liste des identifiants unique d'utilisateur.
   * @since 3.0.2
   */
  public void setId(Long[] id) {
    this.id = id;
  }

  /**
   * Retourne le ou les courriels à rechercher.
   * 
   * @return le ou les courriels de fournisseur a rechercher.
   * @since 3.0.2
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le ou les courriels a rechercher.
   * 
   * @param courriel
   *          le ou les courriels a rechercher.
   * @since 3.0.2
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  /**
   * @param codeLocale
   *          the codeLocale to set
   * @since 3.0.2
   */
  public void setCodeLocale(String[] codeLocale) {
    this.codeLocale = codeLocale;
  }

  /**
   * @return the codeLocale
   * @since 3.0.2
   */
  public String[] getCodeLocale() {
    return codeLocale;
  }

  /**
   * Utilisateur supprimé inclut dans la recherche?
   * 
   * @param isDeleted
   *          the isDeleted to set
   * @since 3.0.2
   */
  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  /**
   * Utilisateur supprimé inclut dans la recherche?
   * 
   * @return the isDeleted
   * @since 3.0.2
   */
  public Boolean getIsDeleted() {
    return isDeleted;
  }

  /**
   * Extraire les utilisateurs pour un ou des rôles avec code client seulement.
   * 
   * @param roleAvecCodeClientSeulement
   *          the roleAvecCodeClientSeulement to set
   * @since 3.0.2
   */
  public void setRoleAvecCodeClientSeulement(Boolean roleAvecCodeClientSeulement) {
    this.roleAvecCodeClientSeulement = roleAvecCodeClientSeulement;
  }

  /**
   * Extraire les utilisateurs pour un ou des rôles avec code client seulement.
   * 
   * @return the roleAvecCodeClientSeulement
   * @Since 3.0.2
   */
  public Boolean getRoleAvecCodeClientSeulement() {
    return roleAvecCodeClientSeulement;
  }

  /**
   * @return the sansRoleEtCreeParUtilisateurEnTraitement
   */
  public Boolean getSansRoleEtCreeParUtilisateurEnTraitement() {
    return sansRoleEtCreeParUtilisateurEnTraitement;
  }

  /**
   * @param sansRoleEtCreeParUtilisateurEnTraitement
   *          the sansRoleEtCreeParUtilisateurEnTraitement to set
   */
  public void setSansRoleEtCreeParUtilisateurEnTraitement(
      Boolean sansRoleEtCreeParUtilisateurEnTraitement) {
    this.sansRoleEtCreeParUtilisateurEnTraitement = sansRoleEtCreeParUtilisateurEnTraitement;
  }

  public List<FiltrePropriete> getListePropriete() {
    return listePropriete;
  }

  public void setListePropriete(List<FiltrePropriete> listePropriete) {
    this.listePropriete = listePropriete;
  }

  /**
   * @return the codeGroupeAge
   */
  public String getCodeGroupeAge() {
    return codeGroupeAge;
  }

  /**
   * @param codeGroupeAge
   *          the codeGroupeAge to set
   */
  public void setCodeGroupeAge(String codeGroupeAge) {
    this.codeGroupeAge = codeGroupeAge;
  }

  /**
   * @return the sexe
   */
  public String getSexe() {
    return sexe;
  }

  /**
   * @param sexe
   *          the sexe to set
   */
  public void setSexe(String sexe) {
    this.sexe = sexe;
  }

  /**
   * @return the dateDernierAccesDebut
   */
  public java.util.Date getDateDernierAccesDebut() {
    return dateDernierAccesDebut;
  }

  /**
   * @param dateDernierAccesDebut
   *          the dateDernierAccesDebut to set
   */
  public void setDateDernierAccesDebut(java.util.Date dateDernierAccesDebut) {
    this.dateDernierAccesDebut = dateDernierAccesDebut;
  }

  /**
   * @return the dateDernierAccesFin
   */
  public java.util.Date getDateDernierAccesFin() {
    return dateDernierAccesFin;
  }

  /**
   * @param dateDernierAccesFin
   *          the dateDernierAccesFin to set
   */
  public void setDateDernierAccesFin(java.util.Date dateDernierAccesFin) {
    this.dateDernierAccesFin = dateDernierAccesFin;
  }

  /**
   * @return the dateDernierAcces
   */
  public java.util.Date getDateDernierAcces() {
    return dateDernierAcces;
  }

  /**
   * @param dateDernierAcces
   *          the dateDernierAcces to set
   */
  public void setDateDernierAcces(java.util.Date dateDernierAcces) {
    this.dateDernierAcces = dateDernierAcces;
  }

  @Override
  public java.util.Date getDateCreationDebut() {
    return dateCreationDebut;
  }

  @Override
  public void setDateCreationDebut(java.util.Date dateCreationDebut) {
    this.dateCreationDebut = dateCreationDebut;
  }

  @Override
  public java.util.Date getDateCreationFin() {
    return dateCreationFin;
  }

  @Override
  public void setDateCreationFin(java.util.Date dateCreationFin) {
    this.dateCreationFin = dateCreationFin;
  }

  @Override
  public java.util.Date getDateCreation() {
    return dateCreation;
  }

  @Override
  public void setDateCreation(java.util.Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getNomUtilisateur() {
    return nomUtilisateur;
  }

  public void setNomUtilisateur(String nomUtilisateur) {
    this.nomUtilisateur = nomUtilisateur;
  }

  public Date getDateNaissanceDebut() {
    return dateNaissanceDebut;
  }

  public void setDateNaissanceDebut(Date dateNaissanceDebut) {
    this.dateNaissanceDebut = dateNaissanceDebut;
  }

  public Date getDateNaissanceFin() {
    return dateNaissanceFin;
  }

  public void setDateNaissanceFin(Date dateNaissanceFin) {
    this.dateNaissanceFin = dateNaissanceFin;
  }

  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public String getCodePays() {
    return codePays;
  }

  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }

  public List<AccesExterne> getListeAccesExterne() {
    return listeAccesExterne;
  }

  public void setListeAccesExterne(List<AccesExterne> listeAccesExterne) {
    this.listeAccesExterne = listeAccesExterne;
  }

  public String[] getCodeAccesExterne() {
    return codeAccesExterne;
  }

  public void setCodeAccesExterne(String[] codeAccesExterne) {
    this.codeAccesExterne = codeAccesExterne;
  }

  public String getTypeAccesExterne() {
    return typeAccesExterne;
  }

  public void setTypeAccesExterne(String typeAccesExterne) {
    this.typeAccesExterne = typeAccesExterne;
  }

  public Boolean getAucunAccesExterne() {
    return aucunAccesExterne;
  }

  public void setAucunAccesExterne(Boolean aucunAccesExterne) {
    this.aucunAccesExterne = aucunAccesExterne;
  }

  public String[] getCodeRegion() {
    return codeRegion;
  }

  public void setCodeRegion(String[] codeRegion) {
    this.codeRegion = codeRegion;
  }

  public String getCodeSousRegion() {
    return codeSousRegion;
  }

  public void setCodeSousRegion(String codeSousRegion) {
    this.codeSousRegion = codeSousRegion;
  }

  public Long[] getIdVille() {
    return idVille;
  }

  public void setIdVille(Long[] idVille) {
    this.idVille = idVille;
  }

  public Boolean getIsVilleExiste() {
    return isVilleExiste;
  }

  public void setIsVilleExiste(Boolean villeExiste) {
    this.isVilleExiste = villeExiste;
  }

  public String getCodeDernierAccesExterne() {
    return codeDernierAccesExterne;
  }

  public void setCodeDernierAccesExterne(String codeDernierAccesExterne) {
    this.codeDernierAccesExterne = codeDernierAccesExterne;
  }

  public String getCodeDernierAccesClient() {
    return codeDernierAccesClient;
  }

  public void setCodeDernierAccesClient(String codeDernierAccesClient) {
    this.codeDernierAccesClient = codeDernierAccesClient;
  }

}
