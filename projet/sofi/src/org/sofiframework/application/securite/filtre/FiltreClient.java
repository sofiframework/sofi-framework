/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class FiltreClient extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = -3339722040804069331L;

  private String codeClientParent;

  public String getCodeClientParent() {
    return codeClientParent;
  }

  public void setCodeClientParent(String codeClientParent) {
    this.codeClientParent = codeClientParent;
  }

}
