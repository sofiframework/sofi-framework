/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.parametresysteme.MetaDonneeParametre;
import org.sofiframework.application.securite.filtre.FiltrePropriete;
import org.sofiframework.application.securite.objetstransfert.AccesApplication;
import org.sofiframework.application.securite.objetstransfert.AccesExterne;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Service commun de gestion des utilisateurs d'applications SOFI.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface ServiceUtilisateur {

  /**
   * Recherche une liste de navigation d'objets du référentiel à l'aide d'un
   * filtre de type <code>FiltreObjetReferentielRole</code>.
   * <p>
   * 
   * @param liste
   *          la liste de navigation possédant toutes les caractéristiques pour
   *          la recherche
   * @return la liste de navigation d'objets du référentiel qui répond à la
   *         recherche
   */
  ListeNavigation getListeUtilisateur(ListeNavigation liste);

  /**
   * Recherche une liste de navigation d'objets du référentiel à l'aide d'un
   * filtre de type <code>FiltreObjetReferentielRole</code>.
   * <p>
   * 
   * @param liste
   *          la liste de navigation possédant toutes les caractéristiques pour
   *          la recherche
   * @param codeUtilisateur
   * @return la liste de navigation d'objets du référentiel qui répond à la
   *         recherche
   * @since 3.1 La liste d'utilisateurs retourner incluent aussi ceux qui ont
   *        été créer par l'utilisateur lorsque pas de rôle d'associer.
   */
  ListeNavigation getListeUtilisateur(ListeNavigation liste, String codeUtilisateur);

  /**
   * Retourne un utilisateur selon son code utilisateur ou code utilisateur
   * applicatif.
   * 
   * @param codeUtilisateur
   *          code utilisateur ou code utilisateur applicatif.
   * @return l'utilisateur
   */
  Utilisateur getUtilisateur(String codeUtilisateur);

  /**
   * Retourne un utilisateur selon son code utilisateur ou code utilisateur
   * applicatif.
   * 
   * @param codeUtilisateur
   *          code utilisateur ou code utilisateur applicatif.
   * @param codeApplication
   *          le code application pour avoir le détail de l'accès de
   *          l'utilisateur.
   * @return l'utilisateur
   */
  Utilisateur getUtilisateur(String codeUtilisateur, String codeApplication);

  /**
   * Retourne un utilisateur selon son courriel pour un système.
   * 
   * @param courriel
   * @param codeApplication
   *          le code application pour avoir le détail de l'accès de
   *          l'utilisateur.
   * @return l'utilisateur
   * @since 3.1
   */
  Utilisateur getUtilisateurPourCourriel(String courriel, String codeApplication);
  
  /**
   * Retourne un utilisateur selon son accès externe.
   * 
   * @param identifiant de son accès externe
   * @return l'utilisateur
   * @since 3.2
   */
  Utilisateur getUtilisateurPourAccesExterne(String identifiant);

  /**
   * Retourne un utilisateur selon son ID (Clé primaire).
   * <p>
   * 
   * @param id
   *          identifiant unique de l'utilisateur à obtenir
   * @return l'utilisateur
   */
  Utilisateur getUtilisateurPourId(Long id);

  /**
   * Retourne un utilisateur selon son ID (Clé primaire).
   * <p>
   * 
   * @param id
   *          identifiant unique de l'utilisateur à obtenir
   * @param codeApplication
   *          le code application pour avoir le détail de l'accès de
   *          l'utilisateur.
   * @return l'utilisateur
   */
  Utilisateur getUtilisateurPourId(Long id, String codeApplication);

  /**
   * Ajouter un objet de type <code>Utilisateur</code> dans la base de données.
   * <p>
   * 
   * @param utilisateur
   *          l'utilisateur à ajouter
   * @return l'utilisateur ajouté
   * @since 3.0.2 l'utilisateur ajouté est retourné.
   */
  Utilisateur ajouterUtilisateur(Utilisateur utilisateur);

  /**
   * Permet d'enregistrer un rôle de l'utilisateur.
   * 
   * @param utilisateurRole
   */
  UtilisateurRole enregistrerRole(UtilisateurRole utilisateurRole);

  /**
   * Permet d'associer un rôle à l'utilisateur.
   * 
   * @param utilisateurRole
   */
  UtilisateurRole associerRole(UtilisateurRole utilisateurRole);

  /**
   * Permet de dissocier un rôle à l'utilisateur.
   * 
   * @param utilisateurRole
   */
  void dissocierRole(UtilisateurRole utilisateurRole);

  /**
   * Permet de dissocier un rôle à l'utilisateur.
   * 
   * @param l
   *          'identifiant unique pour dissocier un role.
   */
  void dissocierRole(Long idUtilisateurRole);

  /**
   * Retourne le détail d'un rôle pour un utilisateur.
   * 
   * @param idUtilisateurRole
   *          l'identifiant unique du détail d'un role pour un utilisateur.
   * @return le détail du rôle.
   */
  UtilisateurRole getUtilisateurRole(Long idUtilisateurRole);

  /**
   * Retourne la listes des rôles d'un utilisateur <code>UtilisateurRole</code>
   * 
   * @param codeUtilisateur
   *          le code utilisateur
   * @param codeApplication
   *          le code application.
   * @return la liste des rôles de l'utilisateur.
   */
  List<UtilisateurRole> getListeUtilisateurRole(String codeUtilisateur, String codeApplication);

  /**
   * Permet d'enregistrer les modifications sur un utilisateur.
   * 
   * @param utilisateur
   *          l'utilisateur à enregistrer.
   * @return l'utilisateur enregistrer
   * @since 3.0.2 l'utilisateur enregistrer est retourné.
   */
  Utilisateur enregistrerUtilisateur(Utilisateur utilisateur);

  /**
   * Supprimer un utilisateur.
   * 
   * @param id
   *          l'identifiant de l'utilisateur.
   */
  void supprimerUtilisateur(Long id);

  /**
   * Retourne la liste des utilisateurs pour une application et un rôle.
   * 
   * @param codeApplication
   *          le code application.
   * @param codeRole
   *          le code de rôle.
   * @return La liste des utilisateur.
   */
  List<Utilisateur> getListeUtilisateurPourApplicationEtRole(String codeApplication, String codeRole);

  /**
   * Permet de modifier le mot de passe d'un utilisateur.
   * 
   * @param changementMotPasse
   *          information pour changer le mot de passe
   */
  void changerMotPasse(ChangementMotPasse changementMotPasse);

  /**
   * Permet de modifier le mot de passe expiré d'un utilisateur.
   * 
   * @param changementMotPasse
   *          information pour changer le mot de passe
   */
  void changerMotPasseExpirer(ChangementMotPasse changementMotPasse);

  /**
   * Force le changement du mot de passe de l'utilisateur lors de sa prochaine
   * authentification.
   * 
   * @param codeUtilisateur
   *          Code de l'utilisateur
   */
  void forcerChangementMotPasse(String codeUtilisateur);

  /**
   * Retourne la liste des applications dont un utilisateur peut piloter le
   * système.
   * 
   * @param codeUtilisateur
   *          le code application.
   * @return la liste
   */
  List<Application> getListeApplicationPourPilotage(String codeUtilisateur);

  /**
   * Permet d'ajouter un accès à une application.
   * 
   * @param accesApplication
   * @return
   * @since 3.1
   */
  AccesApplication ajouterAccesApplication(AccesApplication accesApplication);

  /**
   * Permet d'enregistrer le détail d'un accès à une application.
   * 
   * @param accesApplication
   * @return
   * @since 3.1
   */
  AccesApplication enregistrerAccesApplication(AccesApplication accesApplication);

  /**
   * Permet de supprimer un accès à une application.
   * 
   * @param codeUtilisateur
   * @param codeApplication
   * @since 3.1
   */
  void supprimerAccesApplication(String codeUtilisateur, String codeApplication);

  /**
   * Obtenir la liste des critères de recherche de propriété utilisateur
   * possible. Permet de bâtir dynamiquement un formulaire de recherche avec
   * tous les sous-formulaires possibles.
   */
  List<FiltrePropriete> getListeFiltrePropriete();

  /**
   * Obtenir la liste des propriétés possibles pour un client. Retourne les
   * propriétés globales qui ne sont pas spécifiques a un client.
   */
  List<Propriete> getListePropriete(String codeClient);

  /**
   * Obtenir la liste des propriétés possibles pour un client. Retourne les
   * propriétés globales qui ne sont pas spécifiques a un client.
   * 
   * @param actifSeulement
   *          Si TRUE, retourne seulement les actifs, si FALSE retourne
   *          seulement les inactifs, si null retourne tous les enregistrements.
   */
  List<Propriete> getListePropriete(String codeClient, Boolean actifSeulement);
  
  /**
   * Permet de modifier la valeur d'un propriété utilisateur.
   * @param idUtilisateur
   * @param metadonneProprieteId
   * @param valeur
   */
  void modifieProprieteUtilisateur(Long idUtilisateur, Long metadonneProprieteId, String valeur);
  
  /**
   * Permet d'ajouter une valeur de propriété utilisateur.
   * 
   * @param idUtilisateur
   * @param metadonneProprieteId
   * @param valeur
   */
  void ajouterProprieteUtilisateur(Long idUtilisateur, Long metadonneProprieteId, String valeur);
  
  /**
   * Permet de mettre à jour les proprietes de l'utilisateur.
   * 
   * @param utilisateur
   */
  void sauvegarderProprietesUtilisateur(Utilisateur utilisateur);

  /**
   * Permet de récupérer une liste d'utiliasteur qui possède une propriété
   * répondant aux critères du filtre, et dont la date de création (si la date
   * de modification est null) ou la date de modification est dans l'intervalle
   * de date passée en paramètre.
   * 
   * @param filtrePropriete
   * @param dateDebutCreationMAJ
   * @param dateFinCreationMAJ
   * @return
   */
  List<Utilisateur> getListeUtilisateurPourFiltrePropriete(FiltrePropriete filtrePropriete, Date dateDebutCreationMAJ,
      Date dateFinCreationMAJ);

  /**
   * Permet de récupérer tous les utilisateurs(sans les collections pour un
   * traitement plus rapide) qui sont abonné à une metadonnée propriété.
   * 
   * @param listeId
   * @return
   */
  List<Utilisateur> getListeUtilisateurPourPropriete(String listeId, Integer indexFirstResult, Integer nbResult);
  
  /**
   * Permet d'ajouter un accès à un utilisateur.
   * 
   * @param accesApplication
   * @return
   * @since 3.2
   */
  AccesExterne ajouterAccesExterne(AccesExterne accesExterne);

  /**
   * Permet d'enregistrer le détail d'un accès externe.
   * 
   * @param accesExterne
   * @return
   * @since 3.2
   */
  AccesExterne enregistrerAccesExterne(AccesExterne accesExterne);

  /**
   * Permet de supprimer un accès externe d'un utilisateur.
   * 
   * @param id
   * @since 3.2
   */
  void supprimerAccesExterne(Long id);
  
  /**
   * Permet de supprimer tous les accès externes d'un utilisateur.
   * 
   * @param id utilisateur
   * @since 3.2
   */
  void supprimerAccesExterneUtilisateur(Long utilisateurId);
  
  /**
   * Permet d'obtenir l'URL complet de la photo concernant le fournisseur d'accès passé en paramètre avec tout ses paramètres si applicable.
   * Les paramètres d'url sont définis en tant que paramètres système. 
   * @param url l'URL original persistée.
   * @param connectionProvider Le fournisseur d'accès concerné.
   * @return
   */
  String getAccesExterneImageUrl(final String url, final String connectionProvider);

  boolean isUtilisateurSupprime(Long utilisateurId);
  
  Utilisateur getUtilisateurEpure(String codeUtilisateur);
  
}
