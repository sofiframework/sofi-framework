/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service;

import java.util.List;

import org.sofiframework.application.securite.filtre.FiltreRole;
import org.sofiframework.application.securite.objetstransfert.Permission;
import org.sofiframework.application.securite.objetstransfert.Role;

/**
 * @author Jean-Francois Brassard, Nurun inc.
 * @version 3.0
 *
 */
public interface ServiceRole {

  /**
   * Associer une permission pour un role.
   * @param codeRole le code de role
   * @param codePermission le code de la permission.
   */
  public void associerPermission(String codeRole, String codePermission);

  /**
   * Dissocier une permission pour un role
   * @param codeRole le code de role
   * @param codePermission le code de permission.
   */
  public void dissocierPermission(String codeRole, String codePermission);

  /**
   * Retourne la liste des permissions pour un role.
   * @param codeRole le code de role
   * @return la liste des permission pour un role.
   */
  public List<Permission> getListePermission(String codeRole);

  /**
   * Retourne la liste des rôles filtrer avec <code>FiltreRole</code>
   * @param filtre objet de filtrage des rôles.
   * @param codeUtilisateur
   * @return la liste des rôles que utilisateur peut faire le pilotage.
   */
  public List<Role> getListeRolePilotage(FiltreRole filtre, String codeUtilisateur);

  /**
   * Retourne la liste des rôles dont un utilisateur peu faire le pilotage.
   * @param codeApplication
   * @param codeUtilisateur
   * @param codeStatut
   * @return la liste des rôles que utilisateur peut faire le pilotage.
   */
  public List<Role> getListeRolePilotage(String codeApplication, String codeUtilisateur, String codeStatut);

}
