/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service;

import org.sofiframework.application.securite.objetstransfert.Application;


/**
 * Interface spécifiant le service qui offre qui permet d'extraire l'information
 * du référentiel pour une ou des applications.
 * <p>
 * Votre service doit avoir une signature par exemple :
 * public class ServiceReferentielImpl
 *            extends BaseServiceImpl
 *            implements ServiceReferentiel {
 *  ..
 *     getListeApplicationsAvecObjetSecurisable(..) {
 *       ..
 *     }
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.3 Nouvel interface afin de renommer ServiceObjetSecurisable
 */
public interface ServiceReferentiel {
  /**
   * Retourne une application ainsi que tous les composants du référentiel associés.
   * @param codeApplication le code d'application a traiter.
   * @param applicationActiveSeulement
   * @return
   */
  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      boolean applicationActiveSeulement);

  /**
   * Retourne une facette d'une application incluant tous les composants du référentiel
   * associés.
   * @param codeApplication
   * @param codeFacette
   * @param applicationActiveSeulement
   * @return
   * @since 2.0.6
   */
  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      String codeFacette,
      boolean applicationActiveSeulement);
}
