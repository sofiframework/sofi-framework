/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;


/**
 * Interface spécifiant le service qui offre qui permet d'extraire utilisateur
 * avec les objets sécurisés associés.
 * <p>
 * Votre service doit avoir une signature par exemple :
 * public class ServiceSecuriteImpl
 *            extends BaseServiceImpl
 *            implements ServiceSecurite {
 *  ..
 *     getUtilisateurAvecObjetSecurise(..) {
 *       ..
 *     }
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.1 Ajout d'une méthode pour cloisonné la sécurité applicative selon un client.
 */
public interface ServiceSecurite {
  /**
   * Retourne un utilisateur avec sa liste d'objet sécurisables accessibles.
   * @param codeApplication le code de l'application correspondant à l'utilisateur authentifié.
   * @param identifiant l'identifiant de l'utilisateur.
   * @return l'utilisateur avec sa liste d'objet du référentiel.
   */
  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String identifiant);

  /**
   * Retourne un utilisateur avec sa liste d'objet sécurisables accessibles.
   * @param codeApplication le code de l'application correspondant à l'utilisateur authentifié.
   * @param codeFacette le code de la facette a utiliser pour extraire la sécurité de l'utilisateur.
   * @param identifiant l'identifiant de l'utilisateur.
   * @return l'utilisateur avec sa liste d'objet du référentiel.
   */
  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String codeFacette,
      String identifiant);

  /**
   * Retourne l'utilisateur sécurisé selon un client spécifique.
   * <p>
   * Seulement les composants du référentiel accesssible selon les rôles alloué pour un client X seront
   * disponible à l'utilisateur.
   * @param codeApplication le code de l'application correspondant à l'utilisateur authentifié.
   * @param identifiant l'identifiant de l'utilisateur.
   * @param codeClient le code client dont on désire restreindre la sécurité de l'utilisateur.
   * @return l'utilisateur avec sa liste d'objet du référentiel.
   */
  public org.sofiframework.application.securite.objetstransfert.Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication,
      String identifiant,
      String codeClient);


  /**
   * Retourne l'utilisateur sécurisé selon un client spécifique.
   * <p>
   * Seulement les composants du référentiel accesssible selon les rôles alloué pour un client X seront
   * disponible à l'utilisateur.
   * @param codeApplication le code de l'application correspondant à l'utilisateur authentifié.
   * @param codeFacette le code de la facette a utiliser pour extraire la sécurité de l'utilisateur.
   * @param identifiant l'identifiant de l'utilisateur.
   * @param codeClient le code client dont on désire restreindre la sécurité de l'utilisateur.
   * @return l'utilisateur avec sa liste d'objet du référentiel.
   */
  public org.sofiframework.application.securite.objetstransfert.Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication,
      String codeFacette,
      String identifiant,
      String codeClient);

}
