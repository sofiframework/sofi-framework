/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service;

import org.sofiframework.application.securite.objetstransfert.CertificatAcces;
import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Service d'authentification de l'infrastructure SOFI.
 * @author Jean-François Brassard
 */

public interface ServiceAuthentification {
  /**
   * Retourne le code utilisateur associé au certificat.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @return le code utilisateur associé au certficat.
   */
  String getCodeUtilisateur(String certificat);
  
  /**
   * Retourne le code utilisateur associé au certificat.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @param ip L'ip utilisé pour extraire un code utilisateur pour un certificat.
   * @param majAcces Est-ce qu'on désire indiquer une mise à jour d'accès, par défaut c'est true.
   * @return le code utilisateur associé au certficat.
   */
  String getCodeUtilisateur(String certificat, String ip, boolean majAcces);  
    
  
  /**
   * Retourne le code utilisateur associé au certificat.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @param majAcces Est-ce qu'on désire indiquer une mise à jour d'accès, par défaut c'est true.
   * @return le code utilisateur associé au certficat.
   */
  String getCodeUtilisateur(String certificat, boolean majAcces);  
  
 
  /**
   * Retourne le détail de l'accès.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @return le détail de l'accès.
   * @since 3.2
   */
  CertificatAcces getCertificatAcces(String certificat);
  
  /**
   * Retourne le détail de l'accès.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @param majAcces Est-ce qu'on désire indiquer une mise à jour d'accès, par défaut c'est true.
   * @return le détail de l'accès.
   * @since 3.2
   */
  CertificatAcces getCertificatAcces(String certificat,  boolean majAcces);    
  
  
  /**
   * Retourne le détail de l'accès.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @param ip L'ip utilisé pour extraire un code utilisateur pour un certificat.
   * @param actifSeulement extaire seulement si le certificat est actif.
   * @param majAcces Est-ce qu'on désire indiquer une mise à jour d'accès, par défaut c'est true.
   * @return le détail de l'accès.
   * @since 3.2
   */
  CertificatAcces getCertificatAcces(String certificat, String ip,  boolean actifSeulement, boolean majAcces);  
  
  
  /**
   * Retourne le détail de l'accès valide.
   * @param certificat le certificat d'authentification de l'utilisateur
   * @return le détail de l'accès.
   * @since 3.2
   */
  CertificatAcces getCertificatAccesValide(String codeUtilisateur);

  
  /**
   * Permet d'invalider un certificat.
   * @param certificat le certificat a invalider.
   */
  void terminerCertificat(String certificat);

  /**
   * Permet d'invalider tous les certificats valide d'un utilisateur.
   * @param codeUtilisateur Utilisateur a invalider les certificats.
   */
  void terminerCertificatPourCode(String codeUtilisateur);

  /**
   * Permet de générer un certificat pour un code utilisateur.
   * @param codeUtilisateur le code utilisateur dont on désire un certificat.
   * @return le certificat valide pour l'utilisateur en traitement.
   */
  String genererCertificat(String codeUtilisateur);
  
  /**
   * Permet de modifier un certificat pour un certificat courant.
   * @param codeUtilisateur le code utilisateur dont on désire un certificat.
   * @return le nouveau certificat valide pour l'utilisateur en traitement.
   */
  String modifierCertificat(String certificat, String codeUtilisateur);  

  /**
   * Permet de générer un certificat pour un code utilisateur pour un nombre de jours fixe.
   * @param codeUtilisateur le code utilisateur dont on désire un certificat.
   * @param nbJour le nombre de jour avant expiration.
   * @return le certificat valide pour l'utilisateur en traitement.
   */
  String genererCertificat(String codeUtilisateur, Integer nbJourExpiration);

  /**
   *  Permet de générer un certificat pour un code utilisateur pour un nombre de jours fixe.
   * @param codeUtilisateur
   * @param nbJourExpiration
   * @param ip
   * @param userAgent
   * @return le certificat valide pour l'utilisateur en traitement.
   * @since 3.2 
   */
  String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent, String urlReferer);
  
  /**
   *  Permet de générer un certificat pour un code utilisateur pour un nombre de jours fixe.
   * @param codeUtilisateur
   * @param nbJourExpiration
   * @param ip
   * @param userAgent
   * @param codeClient
   * @return le certificat valide pour l'utilisateur en traitement.
   * @since 3.2 
   */
  String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent, String urlReferer, String codeClient);

  /**
   *  Permet de générer un certificat pour un code utilisateur pour un nombre de jours fixe.
   * @param codeUtilisateur
   * @param nbJourExpiration
   * @param ip
   * @param userAgent
   * @param codeClient
   * @param codeFournisseurAcces
   * @return le certificat valide pour l'utilisateur en traitement.
   * @since 3.2 
   */
  String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent, String urlReferer, String codeClient, String codeFournisseurAcces);
  
  /**
   * Extraire un résultat de recherche sur les certificats d'authentification de l'infrastructure.
   * @param liste la définition de la recherche.
   * @return le résultat de la recherche.
   */
  public ListeNavigation getListeCertificat(ListeNavigation liste);

  /**
   * Extraire une liste du sommaire des certificats d'accès.
   * @param liste la définition de la recherche.
   * @return  le résultat de la recherche.
   */

  public ListeNavigation getListeSommaireCertificat(ListeNavigation liste);
}
