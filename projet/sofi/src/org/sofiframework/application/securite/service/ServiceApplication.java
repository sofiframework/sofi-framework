package org.sofiframework.application.securite.service;

import org.sofiframework.application.securite.objetstransfert.Application;

public interface ServiceApplication {

  Application getApplication(String codeApplication);

}
