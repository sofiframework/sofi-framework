/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.base.BaseGestionService;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.parametresysteme.UtilitaireParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Action;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.Bloc;
import org.sofiframework.application.securite.objetstransfert.ChampSaisie;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Onglet;
import org.sofiframework.application.securite.objetstransfert.Section;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Objet permettant la gestion des objets sécurisé et sécurisables. Un objet sécurisable peut être une section, un
 * service, un onglet, un action (lien hypertexte ou bouton) ou encore un champ de saisie.
 * <p>
 * Correspond à un SINGLETON (Une seule dans la JVM par application).listeObjetSecurisablesParAdresse
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class GestionSecurite extends BaseGestionService {
  /**
   * L'instance de journalisation pour cette classe
   */
  private static final Log log = LogFactory.getLog(GestionSecurite.class);

  /**
   * Référence à l'instance de gestion des objets sécurisables.
   */
  private static final GestionSecurite gestionObjetSecurisableRef = new GestionSecurite();

  /**
   * Liste des objets sécurisables navigable par une adresse web tel que les sections, services, onglets, bouton et lien
   * hypertexte.
   */
  private HashMap listeObjetSecurisablesParAdresse = new HashMap();

  /**
   * Liste des objets sécurisables qui peuvent être sécurié par son nom tel que les champs de saisies, liens hypertexte
   * ou boutons
   */
  private HashMap listeObjetSecurisablesParNom = new HashMap();

  /**
   * Liste des objets sécurisables qui peuvent être sécurié par sa clé séquentiel.
   */
  private HashMap listeObjetSecurisableParCle = new HashMap();

  /**
   * Liste des adresses des objets avec leur paramètres associés
   */
  private HashMap listeAdresseObjetAvecParametre = new HashMap();

  /**
   * Est-ce que la gestion des objet sécurisable est actif?
   */
  private boolean objetSecurisableActif = false;

  /**
   * Est-ce que la gestion de la sécurité est actif?
   */
  private boolean securiteActif = false;

  /**
   * Code de l'application propriétaire des objets décurisables
   */
  private String codeApplication;

  /**
   * Code de la facette de l'application
   */
  private String codeFacette;

  /**
   * Code de la couche de présentation
   */
  private String couchePresentation = null;

  /**
   * L'application en cours d'utilisation.
   */
  private Application application;

  /**
   * La page d'accueil par défaut
   */
  private Integer pageAccueilDefaut;

  /**
   * Composant qui représente la page d'accueil
   */
  private ObjetSecurisable composantAcceuil;

  /**
   * Est-ce que la page d'accueil est inclus dans le menu principal.
   */
  private boolean pageAccueilInclusDansMenu;

  /**
   * Spécifie si l'autorisation est positive.
   */
  private boolean autorisationPositive;

  /**
   * Spécifie la procédure BD qui fixe l'utilisateur dans le contexte BD.
   */
  private String procedureContexteBD;

  /**
   * Nom de cookie qui est logé le certificat d'authentification
   */
  private String nomCookieCertificat;

  /**
   * Le nom du domaine du cookie.
   */
  private String nomDomaineCookie;

  /**
   * La liste des clients racines de chaque arborescence clientèle.
   */
  private List<Client> listeClientRacine;

  /**
   * Nom du parametre de retour de l'url
   */
  private String nomParametreUrlRetour;
  private ServiceObjetSecurisable serviceReferentielLocal;
  private ServiceSecurite serviceSecuriteLocal;
  private ServiceApplication serviceApplicationLocal;
  private ServiceAuthentification serviceAuthentification;
  private ServiceClient serviceClient;
  private ServiceUtilisateur serviceUtilisateur = null;
  private String nomServiceAuthentification = "ServiceAuthentificationBean";
  private boolean donneesChargees = false;

  private GestionSecurite() {
    super(org.sofiframework.application.securite.service.ServiceSecurite.class);
  }

  /**
   * Obtenir l'instance unique de gestion des objets sécurisés.
   * 
   * @return l'instance unique de gestion des objets sécurisés
   */
  public static GestionSecurite getInstance() {
    return gestionObjetSecurisableRef;
  }

  /**
   * Retourne la liste des objets sécurisalbe pour l'application en cours.
   * 
   * @return la liste des objets sécurisalbe pour l'application en cours.
   */
  public List getListeObjetSecurisablePourApplication() {
    if (application != null) {
      return application.getListeObjetSecurisables();
    } else {
      throw new SOFIException("Aucune application avec des composants de défini!");
    }
  }

  /**
   * Permet d'ajouter un objet sécurisable spécifier par son adresse avec la liste des objets sécurisables sécurisé.
   */
  private void ajouterObjetSecursisableAvecAdresse(ObjetSecurisable objetSecurisable, List listeObjetSecurisables,
      Utilisateur utilisateur) {
    HashMap listeObjetSecurisablesParAdresse = null;

    if (utilisateur != null) {
      // Prendre la liste de l'utilisateur.
      listeObjetSecurisablesParAdresse = utilisateur.getListeObjetSecurisablesParAdresse();

      if (listeObjetSecurisablesParAdresse == null) {
        listeObjetSecurisablesParAdresse = new HashMap();
        utilisateur.setListeObjetSecurisablesParAdresse(listeObjetSecurisablesParAdresse);
      }
    } else {
      // Prendre la liste de l'application.
      listeObjetSecurisablesParAdresse = this.listeObjetSecurisablesParAdresse;
    }

    if ((listeObjetSecurisables != null) && (listeObjetSecurisables.size() > 0)) {
      // Ajouter l'objet sécurisable Service ou Section
      if (isObjetSecurisableParAdresse(objetSecurisable)
          && isObjetSecurisableParAdresse((ObjetSecurisable) listeObjetSecurisables.get(0))) {
        String separateurParametre = GestionParametreSysteme.getInstance().getString(
            ConstantesParametreSysteme.SEPARATEUR_METHODE_ACTION);
        String nomParametre = objetSecurisable.getNomParametre();

        /**
         * Si un separateur de methodes est defini pour l'application on doit transformer le nom de parametre en une
         * liste de methodes. Donc afficher;acceder devient - afficher - acceder
         */
        if (!UtilitaireString.isVide(separateurParametre) && nomParametre != null
            && nomParametre.indexOf(separateurParametre) != -1) {
          for (StringTokenizer parametres = new StringTokenizer(nomParametre, separateurParametre); parametres
              .hasMoreTokens();) {
            String parametre = parametres.nextToken();
            String adresseWeb = this.getAdresseWeb(objetSecurisable.getNomAction(), parametre);
            listeObjetSecurisablesParAdresse.put(adresseWeb, listeObjetSecurisables);
          }
        } else {
          listeObjetSecurisablesParAdresse.put(getAdresseWebPourObjetSecurisable(objetSecurisable),
              listeObjetSecurisables);
        }
      } else {
        listeObjetSecurisables = new ArrayList();
        listeObjetSecurisables.add(objetSecurisable);
        listeObjetSecurisablesParAdresse.put(getAdresseWebPourObjetSecurisable(objetSecurisable),
            listeObjetSecurisables);
      }

      // Ajouter la référence de l'objet sécurisable pour sa clé unique
      this.listeObjetSecurisableParCle.put(objetSecurisable.getSeqObjetSecurisable(), objetSecurisable);
    } else {
      listeObjetSecurisables = new ArrayList();
      listeObjetSecurisables.add(objetSecurisable);

      // Ajouter l'objet sécurisable
      if (isObjetSecurisableParAdresse(objetSecurisable)) {
        listeObjetSecurisablesParAdresse.put(getAdresseWebPourObjetSecurisable(objetSecurisable),
            listeObjetSecurisables);
      }

      // Ajouter la référence de l'objet sécurisable pour sa clé unique
      this.listeObjetSecurisableParCle.put(objetSecurisable.getSeqObjetSecurisable(), objetSecurisable);
    }
  }

  public String getAdresseWebPourObjetSecurisable(ObjetSecurisable objetSecurisable) {
    String adresseWeb = objetSecurisable.getAdresseWeb();

    if (adresseWeb != null && adresseWeb.indexOf("[") != -1) {
      int positionFin = adresseWeb.indexOf("]");
      if (adresseWeb.substring(positionFin + 1, positionFin + 2).indexOf("/") != -1) {
        adresseWeb = adresseWeb.substring(positionFin + 2);
      } else {
        adresseWeb = adresseWeb.substring(positionFin + 1);
      }
    }

    return adresseWeb;
  }

  /**
   * Obtenir une adresse web composee de l'action et du parametre.
   */
  public String getAdresseWeb(String action, String parametre) {
    String adresseWeb = null;
    if (action != null) {
      StringBuffer adresseComplete = new StringBuffer();
      adresseComplete.append(action);
      adresseComplete.append(".do");

      if (parametre != null) {
        adresseComplete.append("?");
        String methodeParam = UtilitaireParametreSysteme.getMethodeParam();
        adresseComplete.append(methodeParam);
        adresseComplete.append("=");
        adresseComplete.append(parametre);
      }
      adresseWeb = adresseComplete.toString();
    }
    return adresseWeb;
  }

  /**
   * Est-ce que l'objet sécurisable doit être sécurisé par adresse et il fait partie de la navigation par un menu.
   * 
   * @param objetSecurisable
   *          l'objet sécurisable en traitement.
   * @return true si l'objet sécurisable doit être sécurisé par adresse.
   */
  private boolean isObjetSecurisableParAdresse(ObjetSecurisable objetSecurisable) {
    if ((!Action.class.isInstance(objetSecurisable) || (Action.class.isInstance(objetSecurisable) && (getAdresseWebPourObjetSecurisable(objetSecurisable) != null)))
        && !ChampSaisie.class.isInstance(objetSecurisable) && !Bloc.class.isInstance(objetSecurisable)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Ajouter un détail au service, correspondant normalement à un onglet.
   * 
   * @param objetSecurisable
   *          objet sécurisable.
   * @param utilisateur
   *          l'utilisateur en traitement.
   */
  public void ajouterObjetSecurisableParNom(ObjetSecurisable objetSecurisable, Utilisateur utilisateur) {
    HashMap listeObjetSecurisablesParNom = null;

    if (utilisateur != null) {
      // Prendre la liste de l'utilisateur.
      listeObjetSecurisablesParNom = utilisateur.getListeObjetSecurisablesParNom();
    } else {
      // Prendre la liste de l'application.
      listeObjetSecurisablesParNom = this.listeObjetSecurisablesParNom;

      // Ajouter la référence de l'objet sécurisable pour sa clé unique
      this.listeObjetSecurisableParCle.put(objetSecurisable.getSeqObjetSecurisable(), objetSecurisable);
    }

    listeObjetSecurisablesParNom.put(objetSecurisable.getNom(), objetSecurisable);
  }

  /**
   * Retourne l'objet sécurisable par son nom.
   * 
   * @param nom
   *          le nom de l'objet sécurisable
   * @return l'objet sécurisable.
   */
  public ObjetSecurisable getObjetSecurisable(String nom, HashMap listeObjetSecurisableParNom) {
    return (ObjetSecurisable) listeObjetSecurisablesParNom.get(nom);
  }

  /**
   * Retourne l'objet sécurisable par son nom.
   * 
   * @param nom
   *          le nom de l'objet sécurisable
   * @return l'objet sécurisable.
   */
  public ObjetSecurisable getObjetSecurisable(String nom) {
    return (ObjetSecurisable) this.listeObjetSecurisablesParNom.get(nom);
  }

  /**
   * Retourne l'objet sécurisable par sa clé unique.
   * 
   * @param cle
   *          la séquence de l'objet sécurisable
   * @return l'objet sécurisable.
   */
  public ObjetSecurisable getObjetSecurisableParCle(Long cle) {
    return (ObjetSecurisable) this.listeObjetSecurisableParCle.get(cle);
  }

  /**
   * Retourne l'objet sécurisable par son adresse Web.
   * 
   * @return un objet sécurisable
   * @param adresseWeb
   *          l'adresse Web correspondant à un objet sécurisable.
   */
  public ObjetSecurisable getObjetSecurisableParAdresse(String adresseWeb) {
    ObjetSecurisable objetSecurisableRetour = null;

    if ((adresseWeb != null) && adresseWeb.startsWith("/")) {
      adresseWeb = adresseWeb.substring(1);
    }

    List listeObjetSecurisable = (List) this.listeObjetSecurisablesParAdresse.get(adresseWeb);

    if (listeObjetSecurisable != null) {
      Iterator iterateur = listeObjetSecurisable.iterator();

      while (iterateur.hasNext() && (objetSecurisableRetour == null)) {
        ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateur.next();
        String adresseWebObjet = getAdresseWebPourObjetSecurisable(objetSecurisable);

        if (adresseWebObjet.equals(adresseWeb)) {
          objetSecurisableRetour = objetSecurisable;
        }

        // Vérifier dans les parents
        ObjetSecurisable objetSecurisableParent = objetSecurisable.getObjetSecurisableParent();

        while ((objetSecurisableParent != null) && (objetSecurisableParent.getAdresseWeb() != null)
            && (objetSecurisableRetour == null)) {
          if (objetSecurisableParent.getAdresseWeb().equals(adresseWeb)) {
            objetSecurisableRetour = objetSecurisableParent;
          } else {
            objetSecurisableParent = objetSecurisableParent.getObjetSecurisableParent();
          }
        }
      }
    }

    return objetSecurisableRetour;
  }

  /**
   * Retourne l'objet sécurisable pour son adresse web.
   * 
   * @param adresseWeb
   *          la séquence de l'objet sécurisable
   * @return l'objet sécurisable.
   */
  public ObjetSecurisable getObjetSecurisableParAdresse(String adresseWeb, List listeObjetSecurisable) {
    if ((listeObjetSecurisable != null) && (listeObjetSecurisable.size() > 0)) {
      Iterator iterateur = listeObjetSecurisable.iterator();

      while (iterateur.hasNext()) {
        ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateur.next();

        if ((getAdresseWebPourObjetSecurisable(objetSecurisable) != null)
            && getAdresseWebPourObjetSecurisable(objetSecurisable).equals(adresseWeb)) {
          return objetSecurisable;
        }
      }

      return (ObjetSecurisable) listeObjetSecurisable.get(0);
    }

    return null;
  }

  /**
   * Retourne le code de l'application.
   * 
   * @return le code de l'application qui est propriétaire des objets sécurisables.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code de l'application.
   * 
   * @param codeApplication
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code de la couche de présentation utilisée.
   * 
   * @return le code de la couche de présentation de l'application qui est propriétaire des objets sécurisables.
   */
  public String getCouchePresentation() {
    return couchePresentation;
  }

  /**
   * Fixer le code de la couche de présentation de l'application.
   * 
   * @param couchePresentation
   */
  public void setCouchePresentation(String couchePresentation) {
    this.couchePresentation = couchePresentation;
  }

  /**
   * Retourne l'application en cours
   * 
   * @return l'application en cours
   */
  public Application getApplication() {
    return application;
  }

  /**
   * Fixer l'application en cours
   * 
   * @param application
   *          l'application en cours
   */
  public void setApplication(Application application) {
    this.application = application;
  }

  /**
   * Charger les données initial pour l'application.
   */
  public synchronized void chargeDonnees() throws Exception {
    if (isDonneesChargees()) {
      return;
    }

    // Accéder au service local via une interface.
    if (getCodeFacette() == null) {
      this.application = serviceReferentielLocal.getApplicationAvecObjetSecurisable(getCodeApplication(), true);
    } else {
      try {
        this.application = serviceReferentielLocal.getApplicationAvecObjetSecurisable(getCodeApplication(),
            getCodeFacette(), true);
      } catch (Exception e) {
        // Si non disponible alors utiliser le service du référentiel sans facette.
        this.application = serviceReferentielLocal.getApplicationAvecObjetSecurisable(getCodeApplication(), true);
      }
    }
    setDonneesChargees(true);

    if (getApplication() != null) {
      if (log.isInfoEnabled()) {
        log.info(getApplication());
      }

      // Accéder aux objets sécurisables.
      Iterator iterateurServices = application.getListeObjetSecurisables().iterator();

      while (iterateurServices.hasNext()) {
        populerLesObjetSecurisableEnfants(iterateurServices, null);
      }

      setObjetSecurisableActif(true);
    }

    if (getServiceClient() != null) {
      listeClientRacine = getServiceClient().getListeClientRacine();
    }

  }

  /**
   * Permet de générer le référentiel des objets sécurisables pour un utilisateur
   * 
   * @param utilisateur
   *          l'utilisateur a traiter.
   */
  public synchronized void genererReferentielObjetSecurisables(Utilisateur utilisateur) {
    utilisateur.setListeObjetSecurisablesParNom(new HashMap());

    // Accéder aux objets sécurisables.
    Iterator iterateurServices = utilisateur.getListeObjetSecurisables().iterator();

    while (iterateurServices.hasNext()) {
      populerLesObjetSecurisableEnfants(iterateurServices, utilisateur);
    }
  }

  /**
   * Populer les objets sécursiables enfants
   * 
   * @param iterateurEnfants
   *          iterateurs des objets sécurisables enfants.
   */
  private void populerLesObjetSecurisableEnfants(Iterator iterateurEnfants, Utilisateur utilisateur) {
    while (iterateurEnfants.hasNext()) {
      ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateurEnfants.next();

      if (getAdresseWebPourObjetSecurisable(objetSecurisable) != null) {
        Href lienComplet = new Href(getAdresseWebPourObjetSecurisable(objetSecurisable));

        if ((objetSecurisable.getNomParametre() != null) && (objetSecurisable.getNomParametre().indexOf("&") != -1)) {
          Map listeParametres = lienComplet.getListeParametres();

          StringBuffer adresseReference = new StringBuffer(objetSecurisable.getNomAction());
          adresseReference.append(".do");
          adresseReference.append("?");
          adresseReference.append(UtilitaireParametreSysteme.getMethodeParam());
          adresseReference.append("=");
          adresseReference.append(objetSecurisable.getNomParametre().substring(0,
              objetSecurisable.getNomParametre().indexOf("&")));

          listeParametres.remove(UtilitaireParametreSysteme.getMethodeParam());
          ajouterAdresseAvecParametres(adresseReference.toString(), listeParametres);
        }
      }

      // Ajouter l'objet du référentiel dans la liste par nom.
      ajouterObjetSecurisableParNom(objetSecurisable, utilisateur);

      // Traiter si l'objet sécurisable est basé sur une adresse Web.
      if ((getAdresseWebPourObjetSecurisable(objetSecurisable) != null)
          && !getAdresseWebPourObjetSecurisable(objetSecurisable).trim().equals("")) {
        if (Onglet.class.isInstance(objetSecurisable)) {
          ArrayList listeObjetSecursisables = new ArrayList();

          // Ajouter l'objet sécurisable Onglet avec parent et enfants associés
          ajouterParentOngletEtService(listeObjetSecursisables, objetSecurisable, utilisateur);

          ajouterObjetSecursisableAvecAdresse(objetSecurisable, listeObjetSecursisables, utilisateur);
        } else {
          // Ajouter l'objet sécurisable Service ou Section
          if (isObjetSecurisableParAdresse(objetSecurisable)) {
            if (Service.class.isInstance(objetSecurisable)
                && (objetSecurisable.getListeObjetSecurisableEnfants() != null)
                && (objetSecurisable.getListeObjetSecurisableEnfants().size() > 0)
                && Service.class.isInstance(objetSecurisable.getListeObjetSecurisableEnfants().get(0))) {
              ArrayList liste = new ArrayList();
              liste.add(objetSecurisable);
              ajouterObjetSecursisableAvecAdresse(objetSecurisable, liste, utilisateur);
            } else {
              ajouterObjetSecursisableAvecAdresse(objetSecurisable, objetSecurisable.getListeObjetSecurisableEnfants(),
                  utilisateur);
            }
          }
        }
      }

      // Populer les objets sécurisables enfants.
      if ((objetSecurisable.getListeObjetSecurisableEnfants() != null)
          && (objetSecurisable.getListeObjetSecurisableEnfants().size() > 0)) {
        populerLesObjetSecurisableEnfants(objetSecurisable.getListeObjetSecurisableEnfants().iterator(), utilisateur);
      }
    }
  }

  /**
   * Retourner la liste des onglets disponibles pour la sélectionner d'un service ou d'un onglet selon la sécurité de
   * l'utilisateur.
   * 
   * @param utilisateur
   * @return la liste des onglets disponibles.
   */
  public synchronized ArrayList getListeOngletDisponibles(Utilisateur utilisateur) {
    return null;
  }

  /**
   * Permet d'ajouter, dans une liste, les onglets et services parent à l'objet sécurisable courant.
   * 
   * @param listeObjetSecurisableParent
   *          la liste des objets sécurisables parent.
   * @param objetSecurisable
   *          objet sécurisable à traiter.
   */
  private void ajouterParentOngletEtService(ArrayList listeObjetSecurisableParent, ObjetSecurisable objetSecurisable,
      Utilisateur utilisateur) {
    ObjetSecurisable parent = objetSecurisable.getObjetSecurisableParent();

    if (Onglet.class.isInstance(parent)) {
      ajouterParentOngletEtService(listeObjetSecurisableParent, parent, utilisateur);

      // Ajouter les enfants du parents s'il n'existe pas déjà
      Iterator iterateur = parent.getListeObjetSecurisableEnfants().iterator();

      while (iterateur.hasNext()) {
        ObjetSecurisable objetSecurisableEnfant = (ObjetSecurisable) iterateur.next();

        if (!isExisteListeObjetSecurisableParent(listeObjetSecurisableParent, objetSecurisableEnfant)
            && isObjetSecurisableParAdresse(objetSecurisableEnfant)) {
          listeObjetSecurisableParent.add(objetSecurisableEnfant);
        }
      }
    } else {
      if (Service.class.isInstance(parent)) {
        // Ajouter les enfants du parents s'il n'existe pas déjà
        Iterator iterateur = parent.getListeObjetSecurisableEnfants().iterator();

        while (iterateur.hasNext()) {
          ObjetSecurisable objetSecurisableEnfant = (ObjetSecurisable) iterateur.next();

          if (!isExisteListeObjetSecurisableParent(listeObjetSecurisableParent, objetSecurisableEnfant)
              && isObjetSecurisableParAdresse(objetSecurisableEnfant)) {
            listeObjetSecurisableParent.add(objetSecurisableEnfant);
          }
        }
      }
    }
  }

  /**
   * Vérifier si l'objet sécurisable existe déja dans la liste.
   * 
   * @param listeObjetSecurisableParent
   *          liste d'objet sécurisable parent ou frere.
   * @param objetSecurisableCourant
   *          l'objet sécurisable a traiter
   * @return true si l'objet sécurisable existe
   */
  private boolean isExisteListeObjetSecurisableParent(ArrayList listeObjetSecurisableParent,
      ObjetSecurisable objetSecurisableCourant) {
    Iterator iterateur = listeObjetSecurisableParent.iterator();

    while (iterateur.hasNext()) {
      ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateur.next();

      if (objetSecurisableCourant.getSeqObjetSecurisable().intValue() == objetSecurisable.getSeqObjetSecurisable()
          .intValue()) {
        return true;
      }
    }

    return false;
  }

  /**
   * Retourne la liste des objets Sécurisables par adresse.
   * 
   * @return la liste des objets Sécurisables par adresse.
   */
  public HashMap getListeObjetSecurisablesParAdresse() {
    return listeObjetSecurisablesParAdresse;
  }

  /**
   * Fixer la liste des objets Sécurisables par adresse.
   * 
   * @param listeObjetSecurisablesParAdresse
   *          la liste des objets Sécurisables par adresse.
   */
  public void setListeObjetSecurisablesParAdresse(HashMap listeObjetSecurisablesParAdresse) {
    this.listeObjetSecurisablesParAdresse = listeObjetSecurisablesParAdresse;
  }

  /**
   * Retourne la liste des objets sécurisables par nom. On y retrouve les objets sécurisables de type Action (lien
   * hypertexte ou bouton) ou encore les champs de saisies.
   * 
   * @return la liste des objets sécurisables par nom.
   */
  public HashMap getListeObjetSecurisablesParNom() {
    return listeObjetSecurisablesParNom;
  }

  /**
   * Fixer la liste des objets sécurisables par nom.
   * 
   * @param listeObjetSecurisablesParNom
   *          la liste des objets sécurisables par nom.
   */
  public void setListeObjetSecurisablesParNom(HashMap listeObjetSecurisablesParNom) {
    this.listeObjetSecurisablesParNom = listeObjetSecurisablesParNom;
  }

  /**
   * Retourne l'identifiant de la page d'accueil
   * 
   * @return l'identifiant de la page d'accueil
   */
  public Integer getPageAccueilDefaut() {
    return pageAccueilDefaut;
  }

  /**
   * Fixer l'identifiant de la page d'accueil
   * 
   * @param pageAccueilDefaut
   *          l'identifiant de la page d'accueil
   */
  public void setPageAccueilDefaut(Integer pageAccueilDefaut) {
    this.pageAccueilDefaut = pageAccueilDefaut;
  }

  public ObjetSecurisable getComposantMenuAccueil() {
    return composantAcceuil;
  }

  public void setComposantMenuAccueil(ObjetSecurisable accueil) {
    this.composantAcceuil = accueil;
  }

  /**
   * Est-ce que la page d'accueil est inclus dans la menu?
   * 
   * @return true si la page d'accueil est dans le menu.
   */
  public boolean isPageAccueilInclusDansMenu() {
    return pageAccueilInclusDansMenu;
  }

  /**
   * Fixer si la page d'accueil est inclus dans le menu.
   * 
   * @param pageAccueilInclusDansMenu
   *          true si la page d'accueil est dans le menu.
   */
  public void setPageAccueilInclusDansMenu(boolean pageAccueilInclusDansMenu) {
    this.pageAccueilInclusDansMenu = pageAccueilInclusDansMenu;
  }

  /**
   * Est-ce que le gestionnaire des objet sécurisable est actif?
   * 
   * @return true si le gestionnaire des objet sécurisable est actif
   */
  public boolean isObjetSecurisableActif() {
    return objetSecurisableActif;
  }

  /**
   * Fixer si le gestionnaire des objet sécurisable est actif
   * 
   * @param objetSecurisableActif
   *          true si le gestionnaire des objet sécurisable est actif
   */
  public void setObjetSecurisableActif(boolean objetSecurisableActif) {
    this.objetSecurisableActif = objetSecurisableActif;
  }

  /**
   * Est-ce que l'utilisateur peut accéder à un objet sécurisable?
   * 
   * @param utilisateur
   *          l'utilisateur en traitement.
   * @param identifiant
   *          l'identifiant de l'objet sécurisable.
   * @return true si l'utilisateur peut accéder à un objet sécurisable en lecture seulement.
   */
  public synchronized boolean isUtilisateurAccesObjetSecurisable(Utilisateur utilisateur, String identifiant) {
    boolean acces = false;
    // Si l'application possède l'objet et l'utilisateur aussi
    // Objet sécurisable accessible par l'utilisateur
    boolean securise = getListeObjetSecurisablesParNom().get(identifiant) != null;
    // Si l'application ne possède pas l'objet
    // Objet non sécurisable
    if (securise) {
      if (utilisateur != null && utilisateur.getListeObjetSecurisablesParNom() != null) {
        ObjetSecurisable objetSecurisableUtilisateur = (ObjetSecurisable) utilisateur.getListeObjetSecurisablesParNom()
            .get(identifiant);

        if ((getListeObjetSecurisablesParNom().get(identifiant) != null)
            && (objetSecurisableUtilisateur != null && objetSecurisableUtilisateur.isActif())) {
          acces = true;
        }
      }
    } else {
      acces = true;
    }
    return acces;
  }

  /**
   * Est-ce que l'utilisateur peut accéder à un objet sécurisable en lecture seulement?
   * 
   * @param utilisateur
   *          l'utilisateur en traitement.
   * @param identifiant
   *          l'identifiant de l'objet sécurisable.
   * @return true si l'utilisateur peut accéder à un objet sécurisable en lecture seulement.
   */
  public synchronized boolean isUtilisateurAccesObjetSecurisableLectureSeulement(Utilisateur utilisateur,
      String identifiant) {
    if ((utilisateur != null) && (utilisateur.getListeObjetSecurisablesParNom() != null)) {
      ObjetSecurisable objetSecurisable = (ObjetSecurisable) utilisateur.getListeObjetSecurisablesParNom().get(
          identifiant);

      if ((getListeObjetSecurisablesParNom().get(identifiant) != null) && (objetSecurisable != null)) {
        return objetSecurisable.isLectureSeulement();
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * Est-ce que l'autorisation est positive?
   * <p>
   * Soit que l'utilisateur reçu contient les objets sécurisables qui a accès. Sinon ce sont les objets sécurisé qui
   * sont reçu avec l'utilisateur.
   * 
   * @return true si l'autorisation est positive
   */
  public boolean isAutorisationPositive() {
    return autorisationPositive;
  }

  /**
   * Fixer true si autorisisation positive.
   * <p>
   * 
   * @param autorisationPositive
   *          true si autorisation positive.
   */
  public void setAutorisationPositive(boolean autorisationPositive) {
    setSecuriteActif(true);
    this.autorisationPositive = autorisationPositive;
  }

  /**
   * Est-ce que le gestionnaire de sécurité est actif?
   * 
   * @return true si le gestionnaire de sécurité est actif
   */
  public boolean isSecuriteActif() {
    return securiteActif;
  }

  /**
   * Fixer si le gestionnaire de sécurité est actif
   * 
   * @param securiteActif
   *          true si le gestionnaire de sécurité est actif
   */
  public void setSecuriteActif(boolean securiteActif) {
    this.securiteActif = securiteActif;
  }

  /**
   * Retourne un utilisateur avec ces droits d'autorisation.
   * 
   * @param
   * @return l'utilisateur authentifié.
   */
  public synchronized Utilisateur getUtilisateur(String identifiant) throws Exception {
    Utilisateur utilisateur = null;

    if (getServiceSecuriteLocal() == null) {
      throw new Exception("Aucun service de configuré dans SecuritePlugIn: " + identifiant);
    }

    if (getCodeFacette() == null) {
      // Accéder au service local via une interface.
      utilisateur = serviceSecuriteLocal.getUtilisateurAvecObjetsSecurises(getCodeApplication(), identifiant);
    } else {
      // Accéder au service local via une interface.
      utilisateur = serviceSecuriteLocal.getUtilisateurAvecObjetsSecurises(getCodeApplication(), getCodeFacette(),
          identifiant);
    }

    setSecuriteActif(true);

    if (log.isInfoEnabled()) {
      // Logger les informations de l'utilisateur.
      if (utilisateur != null) {
        log.info(utilisateur.toString());
      } else {
        log.info("L'utilisateur est null");
      }
    }

    return utilisateur;
  }

  /**
   * Retourne un utilisateur avec ces droits d'autorisation selon un client spécifique.
   * <p>
   * Les autorisations de l'utilisateur sont automatiquement généré pour le client spécifique.
   * 
   * @param identifiant
   *          l'identifiant de l'utilisateur a extraire.
   * @param codeClient
   *          le code client que l'ont désiré extraire la sécurité de l'utilisateur.
   * @return l'utilisateur avec la sécurité associé à un client.
   */
  public synchronized Utilisateur getUtilisateur(String identifiant, String codeClient) throws Exception {
    Utilisateur utilisateur = null;

    if (getServiceSecuriteLocal() == null) {
      throw new Exception("Aucun service de configuré dans SecuritePlugIn: " + identifiant);
    }

    if (getCodeFacette() != null) {
      // Accéder au service local via une interface.
      utilisateur = serviceSecuriteLocal.getUtilisateurSecuriseSelonClient(getCodeApplication(), codeFacette,
          identifiant, codeClient);
    } else {
      // Accéder au service local via une interface.
      utilisateur = serviceSecuriteLocal.getUtilisateurSecuriseSelonClient(getCodeApplication(), identifiant,
          codeClient);
    }

    // Générer les autorisations de l'utilisateur selon un client spécifique.
    genererAutorisationUtilisateur(utilisateur);

    return utilisateur;
  }

  /**
   * Générer le référentiel des objets java sécurisable.
   */
  public static void genererReferentiel() {
    GestionSecurite.getInstance().genererReferentiel(false);
  }

  /**
   * Générer le référentiel des objets java sécurisable.
   */
  public void genererReferentiel(boolean forcerInitialiser) {
    GestionSecurite gestionSecurite = GestionSecurite.getInstance();
    if (forcerInitialiser) {
      gestionSecurite.setObjetSecurisableActif(false);
      gestionSecurite.setListeObjetSecurisablesParAdresse(new HashMap());
      gestionSecurite.setListeObjetSecurisablesParNom(new HashMap());
      gestionSecurite.setDonneesChargees(false);
    }
    if ((gestionSecurite.getCodeApplication() != null) && !gestionSecurite.isObjetSecurisableActif()) {
      try {
        GestionSecurite.getInstance().chargeDonnees();
      } catch (Exception e) {
        log.error("Erreur lors du chargement du référentiel", e);
      }
    }
  }

  /**
   * Générer le profil d'authorisation de l'utilisateur en comparant le référentiel des objets java sécurisable de
   * l'application et les objets sécurisé ou sécurisable accessible par l'utilisateur.
   * 
   * @param utilisateur
   *          l'utilisateur en traitement
   */
  public static void genererAutorisationUtilisateur(Utilisateur utilisateur) {
    genererReferentiel();

    if (GestionSecurite.getInstance().getCodeApplication() != null) {
      if (GestionSecurite.getInstance().isAutorisationPositive()
          && !GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.APPLICATION_GRAND_PUBLIC)
          .booleanValue()) {
        // Traiter l'autorisation positive
        GestionSecurite.getInstance().genererReferentielObjetSecurisables(utilisateur);
      } else {
        // Traiter l'autorisation négative
        // Affecter une copie des objets sécurisable par adresse.
        utilisateur.setListeObjetSecurisablesParAdresse((HashMap) UtilitaireObjet.clonerObjet(GestionSecurite
            .getInstance().getListeObjetSecurisablesParAdresse().clone()));

        // Affecter une copie des objets sécurisable de navigation par adresse.
        utilisateur.setListeObjetSecurisablesParNom((HashMap) UtilitaireObjet.clonerObjet(GestionSecurite.getInstance()
            .getListeObjetSecurisablesParNom()));

        List listeObjetSecurisableClone = (List) UtilitaireObjet.clonerObjet(GestionSecurite.getInstance()
            .getListeObjetSecurisablePourApplication());

        // Fixer la liste des objets sécurisables accessible à l'utilisateur.
        utilisateur.setListeObjetSecurisables(listeObjetSecurisableClone);

        if (utilisateur.getListeObjetSecurises() != null) {
          Iterator iterateurObjetSecurise = utilisateur.getListeObjetSecurises().keySet().iterator();

          while (iterateurObjetSecurise.hasNext()) {
            Long seqObjetSecurisable = (Long) iterateurObjetSecurise.next();
            ObjetSecurisable objetSecurisableSecure = (ObjetSecurisable) utilisateur.getListeObjetSecurises().get(
                seqObjetSecurisable);

            // Spécifier si l'objet sécurisé est en lecture seulement
            boolean lectureSeulement = objetSecurisableSecure.isLectureSeulement();

            objetSecurisableSecure = GestionSecurite.getInstance().getObjetSecurisableParCle(seqObjetSecurisable);

            try {
              if (lectureSeulement) {
                // Si l'objet sécurisé est en lecture seulement, spécifier l'objet sécurisable accessible à
                // l'utilisateur
                // en lecture seulement.
                ObjetSecurisable objetSecurisableNom = (ObjetSecurisable) utilisateur.getListeObjetSecurisablesParNom()
                    .get(objetSecurisableSecure.getNom());

                if (objetSecurisableNom != null) {
                  objetSecurisableNom.setLectureSeulement(true);
                }

                List listeObjetSecurisable = (List) utilisateur.getListeObjetSecurisablesParAdresse().get(
                    objetSecurisableSecure.getAdresseWeb());
                ObjetSecurisable objetSecurisableAdresse = GestionSecurite.getInstance().getObjetSecurisableParAdresse(
                    objetSecurisableSecure.getAdresseWeb(), listeObjetSecurisable);

                if ((objetSecurisableAdresse != null) && (listeObjetSecurisable != null)
                    && (listeObjetSecurisable.size() > 0)) {
                  ObjetSecurisable objetSecurisableParent = ((ObjetSecurisable) listeObjetSecurisable.get(0))
                      .getObjetSecurisableParent();

                  // Si correspond à un service, alors spécifier tous ces enfants en lecture seulement.
                  if (Service.class.isInstance(objetSecurisableParent)) {
                    objetSecurisableParent.setLectureSeulement(true);

                    Iterator iterateurEnfants = objetSecurisableParent.getListeObjetSecurisableEnfants().iterator();

                    while (iterateurEnfants.hasNext()) {
                      ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateurEnfants.next();
                      objetSecurisable.setLectureSeulement(true);

                      if (!iterateurEnfants.hasNext()) {
                        if (objetSecurisable.getListeObjetSecurisableEnfants() != null) {
                          iterateurEnfants = objetSecurisable.getListeObjetSecurisableEnfants().iterator();
                        }
                      }
                    }
                  }
                }

                if (objetSecurisableAdresse != null) {
                  objetSecurisableAdresse.setLectureSeulement(true);
                }
              } else {
                // Supprimer l'objet sécurisé de la liste des objets sécurisables accessible à l'utilisateur.
                utilisateur.getListeObjetSecurisablesParNom().remove(objetSecurisableSecure.getNom());
                utilisateur.getListeObjetSecurisablesParAdresse().remove(objetSecurisableSecure.getAdresseWeb());
              }

              // Accéder aux objets sécurisables de l'utilisateur.
              Iterator iterateurObjetSecurisable = utilisateur.getListeObjetSecurisables().iterator();

              while (iterateurObjetSecurisable.hasNext()) {
                appliquerSecuritePourObjetSecurisable(iterateurObjetSecurisable,
                    objetSecurisableSecure.getSeqObjetSecurisable(), lectureSeulement);
              }

              // Supprimer les sections qui n'ont pas de services.
              iterateurObjetSecurisable = utilisateur.getListeObjetSecurisables().iterator();

              while (iterateurObjetSecurisable.hasNext()) {
                ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateurObjetSecurisable.next();

                if (Section.class.isInstance(objetSecurisable)
                    && ((objetSecurisable.getListeObjetSecurisableEnfants() == null) || (objetSecurisable
                        .getListeObjetSecurisableEnfants().size() == 0))) {
                  iterateurObjetSecurise.remove();
                }
              }
            } catch (Exception e) {
              // objet sécurisable inexistant, donc rien faire...
            }
          }
        }
      }
    }
  }

  /**
   * Appliquer la sécurisable sur l'objet sécurisable recherché.
   * 
   * @param iterateurObjetSecurisable
   *          l'itérateur sur la liste des objets sécurisable
   * @param seqObjetSecurisableRecherche
   *          la clé de l'objet sécurisable à trouver
   * @param lectureSeulement
   *          est-ce une objet sécurisable en lecture seulement
   */
  private static void appliquerSecuritePourObjetSecurisable(Iterator iterateurObjetSecurisable,
      Long seqObjetSecurisableRecherche, boolean lectureSeulement) {
    while (iterateurObjetSecurisable.hasNext()) {
      ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateurObjetSecurisable.next();

      if (objetSecurisable.getSeqObjetSecurisable().intValue() == seqObjetSecurisableRecherche.intValue()) {
        if (lectureSeulement) {
          objetSecurisable.setLectureSeulement(lectureSeulement);
        } else {
          iterateurObjetSecurisable.remove();
        }

        return;
      }

      // Chercher dans les objets sécurisables enfants.
      if (objetSecurisable.getListeObjetSecurisableEnfants() != null) {
        appliquerSecuritePourObjetSecurisable(objetSecurisable.getListeObjetSecurisableEnfants().iterator(),
            seqObjetSecurisableRecherche, lectureSeulement);
      }
    }
  }

  /**
   * Est-ce qu'il existe une procédure qui fixe le contexte dans la bd avec le code utilisateur
   * 
   * @return true s'il existe une procédure qui fixe le contexte dans la bd avec le code utilisateur
   */
  public boolean isProcedureContexteBDExiste() {
    if (!UtilitaireString.isVide(getProcedureContexteBD())) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne le nom du cookie où loge le certificat d'authentification.
   * 
   * @return le nom du cookie où loge le certificat d'authentification.
   */
  public String getNomCookieCertificat() {
    return nomCookieCertificat;
  }

  /**
   * Fixer le nom du cookie où loge le certificat d'authentification.
   * 
   * @param nomCookieCertificat
   *          le nom du cookie où loge le certificat d'authentification.
   */
  public void setNomCookieCertificat(String nomCookieCertificat) {
    this.nomCookieCertificat = nomCookieCertificat;
  }

  /**
   * Retourne le nom de parametre de l'url de retour.
   * 
   * @return le nom de parametre de l'url de retour.
   */
  public String getNomParametreUrlRetour() {
    return nomParametreUrlRetour;
  }

  /**
   * Fixer le nom de parametre de l'url de retour.
   * 
   * @param nomParametreUrlRetour
   *          le nom de parametre de l'url de retour.
   */
  public void setNomParametreUrlRetour(String nomParametreUrlRetour) {
    this.nomParametreUrlRetour = nomParametreUrlRetour;
  }

  /**
   * Retourne le nom du domaine du cookie.
   * 
   * @return le nom du domaine du cookie.
   */
  public String getNomDomaineCookie() {
    return nomDomaineCookie;
  }

  /**
   * Fixer le nom du domaine du cookie.
   * 
   * @param nomDomaineCookie
   *          le nom du domaine du cookie.
   */
  public void setNomDomaineCookie(String nomDomaineCookie) {
    this.nomDomaineCookie = nomDomaineCookie;
  }

  /**
   * Retourne la procédure de la base de données qui fixe au contexte BD le code utilisateur
   * 
   * @return la procédure de la base de données qui fixe au contexte BD le code utilisateur
   */
  public String getProcedureContexteBD() {
    return procedureContexteBD;
  }

  /**
   * Fixer la procédure de la base de données qui fixe au contexte BD le code utilisateur
   * 
   * @param procedureContexteBD
   *          la procédure de la base de données qui fixe au contexte BD le code utilisateur
   */
  public void setProcedureContexteBD(String procedureContexteBD) {
    this.procedureContexteBD = procedureContexteBD;
  }

  public void setServiceSecuriteLocal(ServiceSecurite serviceSecuriteLocal) {
    this.serviceSecuriteLocal = serviceSecuriteLocal;
  }

  public ServiceSecurite getServiceSecuriteLocal() {
    return serviceSecuriteLocal;
  }

  public void setServiceReferentielLocal(ServiceObjetSecurisable serviceReferentielLocal) {
    this.serviceReferentielLocal = serviceReferentielLocal;
  }

  public void setServiceApplicationLocal(ServiceApplication serviceApplicationLocal) {
    this.serviceApplicationLocal = serviceApplicationLocal;
  }

  public ServiceObjetSecurisable getServiceReferentielLocal() {
    return serviceReferentielLocal;
  }

  public ServiceApplication getServiceApplicationLocal() {
    return serviceApplicationLocal;
  }

  /**
   * Obtenir le service authentification.
   * 
   * @return Service authentification
   */
  public ServiceAuthentification getServiceAuthentification() {
    if (this.serviceAuthentification == null) {
      this.serviceAuthentification = (ServiceAuthentification) GestionServiceDistant
          .getServiceLocal(this.nomServiceAuthentification,
              org.sofiframework.application.securite.service.ServiceAuthentification.class);
    }

    return this.serviceAuthentification;
  }

  /**
   * Vérifie si le service authentification est configuré.
   * 
   * @return vrai si il est configuré, faux si il ne l'est pas
   */
  public boolean isServiceAuthentificationExiste() {
    return this.getServiceAuthentification() != null;
  }

  public void setServiceAuthentification(ServiceAuthentification service) {
    this.serviceAuthentification = service;
  }

  /**
   * Est-ce que l'application est grand public
   * 
   * @return true si l'application est grand public, donc aucune sécurité.
   */
  public static boolean isApplicationGrandPublic() {
    return GestionParametreSysteme.getInstance().getBoolean(ConstantesParametreSysteme.APPLICATION_GRAND_PUBLIC)
        .booleanValue();
  }

  public void setDonneesChargees(boolean donneesChargees) {
    this.donneesChargees = donneesChargees;
  }

  public boolean isDonneesChargees() {
    return donneesChargees;
  }

  private void ajouterAdresseAvecParametres(String adresse, Map listeParametres) {
    this.listeAdresseObjetAvecParametre.put(adresse, listeParametres);
  }

  /**
   * Retourne la liste des paramètres associé à une adresse d'un objet sécurisable.
   * 
   * @return la liste des paramètres associé à une adresse d'un objet sécurisable.
   * @param adresse
   *          l'adresse que l'on désire avoir les paramètres qui y sont associés.
   */
  public Map getListeParametresPourAdresse(String adresse) {
    return (Map) this.listeAdresseObjetAvecParametre.get(adresse);
  }

  @Override
  protected void setServiceLocal(Object service) {
    setServiceSecuriteLocal((ServiceSecurite) service);
  }

  /**
   * Fixer le code de la facette de l'application.
   * <p>
   * 
   * @param codeFacette
   *          le code de la facette de l'application
   * @since SOFI 2.0.6
   */
  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  /**
   * Retourne le code de la facette de l'application.
   * <p>
   * 
   * @since SOFI 2.0.6
   */
  public String getCodeFacette() {
    return codeFacette;
  }

  /**
   * Retourne true lorsque le code client <code>codeClientAscendant</code> est un client ascendant du client de code
   * <code>codeClient</code>.
   * 
   * @param codeClientAscendant
   * @param codeClient
   * @return un boolean
   */
  public boolean isClientAscendant(String codeClientAscendant, String codeClient) {
    if (codeClientAscendant != null && codeClient != null && codeClientAscendant.equals(codeClient)) {
      return true;
    }

    @SuppressWarnings("unchecked")
    List<Client> listePlate = UtilitaireListe.getListeParentEnfantsEn1Niveau(listeClientRacine, "listeClientEnfant");
    Client client = (Client) UtilitaireListe.getValeurDansListe(listePlate, "noClient", codeClient);

    if (client != null) {
      Client parent = null;
      // On remonte l'ascendance du client pour voir si le code client en paramètre en fait parti.
      while ((parent = (Client) UtilitaireListe
          .getValeurDansListe(listePlate, "noClient", client.getCodeClientParent())) != null) {
        if (parent.getNoClient().equals(codeClientAscendant)) {
          return true;
        }
        client = parent;
      }
    }
    return false;
  }

  /**
   * Retourne la liste des clients ascendants du client passé en paramètre
   * 
   * @param codeClient
   *          le code du client dont on veut les descendants.
   * @return si codeClient est null, une liste vide ; sinon la liste des descendants.
   */
  public List<Client> getListeClientAscendant(String codeClient) {
    List<Client> listeClient = new ArrayList<Client>();
    @SuppressWarnings("unchecked")
    List<Client> listePlate = UtilitaireListe.getListeParentEnfantsEn1Niveau(listeClientRacine, "listeClientEnfant");
    Client client = (Client) UtilitaireListe.getValeurDansListe(listePlate, "noClient", codeClient);

    if (client != null) {
      Client parent = null;
      // On remonte l'ascendance du client pour voir si le code client en paramètre en fait parti.
      while ((parent = (Client) UtilitaireListe
          .getValeurDansListe(listePlate, "noClient", client.getCodeClientParent())) != null) {
        listeClient.add(parent);
        client = parent;
      }
    }
    return listeClient;
  }

  public ServiceUtilisateur getServiceUtilisateur() {
    return serviceUtilisateur;
  }

  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }

  public ServiceClient getServiceClient() {
    return serviceClient;
  }

  public void setServiceClient(ServiceClient serviceClient) {
    this.serviceClient = serviceClient;
  }

}
