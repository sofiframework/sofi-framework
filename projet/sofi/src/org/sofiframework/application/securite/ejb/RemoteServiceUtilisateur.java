/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.ejb;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.ejb.exception.ServiceException;


/**
 * Interface à distance permettant d'accéder à un service distant permettant
 * la gestion des utilisateurs.
 * <p>
 * Le distance implémenté doit donc être basé sur cette interface.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0
 * @since SOFI 2.0.3 Il est maintenant possible de faire de la recherche des
 * utilisateurs dans l'infrastructure.
 */
public interface RemoteServiceUtilisateur extends javax.ejb.EJBObject {

  void ajouterUtilisateur(Utilisateur utilisateur)
      throws ServiceException, java.rmi.RemoteException;

  void associerRole(UtilisateurRole utilisateurRole)
      throws ServiceException, java.rmi.RemoteException;

  void dissocierRole(UtilisateurRole utilisateurRole)
      throws ServiceException, java.rmi.RemoteException;

  void enregistrerUtilisateur(Utilisateur utilisateur)
      throws ServiceException, java.rmi.RemoteException;

  Utilisateur getUtilisateur(String identifiant)
      throws ServiceException, java.rmi.RemoteException;

  List getListeUtilisateurPourApplicationEtRole(String codeApplication, String codeRole)
      throws ServiceException, java.rmi.RemoteException;

  ListeNavigation getListeUtilisateur(ListeNavigation liste)
      throws ServiceException, java.rmi.RemoteException;
}
