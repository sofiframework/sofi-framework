/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.ejb;

import javax.ejb.CreateException;


/**
 * Interface local EJB permettant d'accéder à un service distant permettant
 * d'extraire un utilisateur avec ces objets sécurisé.
 * <p>
 * Le service implémenté doit donc être basé sur cette interface.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public interface ServiceSecuriteHome extends javax.ejb.EJBHome {
  /**
   * Comment here
   * @label ServiceSecuriteHomeremoteServiceSecurite0
   */
  public static final RemoteServiceSecurite remoteServiceSecurite = null;

  RemoteServiceSecurite create()
      throws CreateException, java.rmi.RemoteException;
}
