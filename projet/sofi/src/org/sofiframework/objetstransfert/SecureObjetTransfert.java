/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;

import java.util.HashMap;
import java.util.Map;


/**
 * Classe abstraite permettant à un objet de transfert d'appliquer de la
 * sécurité sur ces attributs.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.1
 */
public abstract class SecureObjetTransfert extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 8546999224773309867L;
  /** Les attributs du formulaire à sécuriser */
  private Map attributsASecuriser = new HashMap();

  /**
   * Permet d'ajouter un attribut à sécuriser.
   * <p>
   * @param nomAttribut le nom de l'attribut à sécuriser
   */
  public void ajouterAttributASecurier(String nomAttribut) {
    attributsASecuriser.put(nomAttribut, Boolean.TRUE);
  }

  /**
   * Indique si l'attribut spécifié est sécurisé
   * <p>
   * @param nomAttribut le nom de l'attribut à vérifier
   * @return true si l'attribut spécifié est sécurisé.
   */
  public boolean isAttributSecurise(String nomAttribut) {
    if (attributsASecuriser.get(nomAttribut) != null) {
      return true;
    } else {
      return false;
    }
  }
}
