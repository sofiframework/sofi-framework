/*
 * $URL: svn://qcdoas04/sofi/trunk/sources/SOFI/src/org/sofiframework/objetstransfert/Identifiant.java $
 * $LastChangedBy: brasje $
 * $LastChangedDate: 2006-05-16 11:46:42 -0400 (mar., 16 mai 2006) $
 * $LastChangedRevision: 360 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;


/**
 * Objet qui reprensente les valeurs des propriétés qui permettent d'identifier uniquement un objet de tranfert.
 */
public class Identifiant {
  private final Object[] items;

  /**
   * Contruit un objet Identifiant à partir d'un tableau de valeurs d'une clé primaire.
   * @param items Valeurs de la clé primaire
   */
  public Identifiant(Object[] items) {
    this.items = items;

    if (items == null) {
      throw new IllegalArgumentException("Le tableau ne doit pas etre null");
    } else if (items.length == 0) {
      throw new IllegalArgumentException("Le tableau ne doit pas etre vide");
    }
  }

  /**
   * Contruit un objet Identifiant à partir d'un objet de tranfert. Les valeurs de la clé
   * sont obtenu de l'objet de tranfert et utilisés pour contruire l'Identifiant. Les
   * propriétés de la clé sont connus par la méthode getCle() de l'objet de tranfert
   * qui donne la liste des noms de propriétés de la clé.
   *
   * @param objet Objet de transfert dont on veut obtenir la valeur de l'identifiant
   */
  public Identifiant(ObjetTransfert objet) {
    String[] definitionCle = objet.getCle();

    if ((definitionCle == null) || (definitionCle.length == 0)) {
      throw new IllegalStateException(
          "L'objet de transfert ne definit pas de cle primaire : " +
              objet.getClass().getName());
    }

    int taille = definitionCle.length;
    items = new Object[taille];

    for (int i = 0; i < taille; i++) {
      items[i] = objet.getPropriete(definitionCle[i]);
    }
  }

  /**
   * Permet de construire un identifiant avec une valeur Long.
   * @param identifiant identifiant unique de Classe Long
   */
  public Identifiant(Long identifiant) {
    this(new Object[] { identifiant });
  }

  /**
   * Permet de svoir si deux identifiant sont identiques. Un test
   * d'égalité est effectués sur chaque partir des identifiants.
   * Toutes les parties doivent être égales pour que les identifiants
   * soient considérés égaux.
   *
   * @return Si les identifiants sont égaux
   * @param o Objet dont on désire obtenir le test d'égalité avec l'objet courant
   */
  @Override
  public boolean equals(Object o) {
    if (o instanceof Identifiant) {
      Identifiant id = (Identifiant) o;

      for (int i = 0, n = items.length; i < n; i++) {
        if (items[i] == null) {
          if (id.items[i] != null) {
            return false;
          }
        } else if (!items[i].equals(id.items[i])) {
          return false;
        }
      }

      return true;
    } else {
      return false;
    }
  }

  /**
   * Est utilisé par les Structures de données Hash (ex. : HashMap).
   *
   * @return Valeur int
   */
  @Override
  public int hashCode() {
    int hash = 0;

    for (int i = 0, n = items.length; i < n; i++) {
      Object item = items[i];

      if (item != null) {
        hash += item.hashCode();
      }
    }

    return hash;
  }

  /**
   * Retourne vrai si tous les champs de la clé primaire sont nul.
   */
  public boolean isNul() {
    for (int i = 0, n = items.length; i < n; i++) {
      if (items[i] != null) {
        return false;
      }
    }

    return true;
  }

  /**
   * Obtenir les valeurs de l'identifiant.
   * @return Tableau d'objets des valeurs identifiant uniquement un objet
   */
  public Object[] getItems() {
    return items;
  }

  /**
   * Obtenir la valeur Long à la première position de la clé.
   * @return
   */
  public Long getLong() {
    return this.getLong(0);
  }

  /**
   * Obenir une valeur Long à la position spécifiée.  Si la valeur n'est
   * pas un Long la méthode retourne null.
   *
   * @param positionCle Position de la clé dont on veux obtenir
   * la valeur null
   */
  public Long getLong(int positionCle) {
    Long id = null;

    if ((items != null) && (items.length > positionCle) &&
        Long.class.isAssignableFrom(items[positionCle].getClass())) {
      id = (Long) this.getItems()[positionCle];
    }

    return id;
  }
}
