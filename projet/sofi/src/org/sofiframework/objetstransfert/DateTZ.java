/*
 * $URL: https://svn.nurunquebec.com/sofi/dev/v1.6.0/sources/SOFI/src/org/sofiframework/objetstransfert/DateTZ.java $
 * $LastChangedBy: j-m.pelletier $
 * $LastChangedDate: 2006-02-09 16:20:19 -0500 (jeu., 09 fÃ©vr. 2006) $
 * $LastChangedRevision: 617 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;

import java.util.Date;

/**
 * Classe de date qui indique que la date doit
 * être ajustée au fuseau horaire de l'utilisateur.
 */
public class DateTZ extends Date {

  /**
   * 
   */
  private static final long serialVersionUID = 7719020488845317393L;
  public static final int SERVEUR_A_CLIENT = -1;
  public static final int CLIENT_A_SERVEUR = 1;

  private int polarite = SERVEUR_A_CLIENT;

  /**
   * Constructeur par défaut
   */
  public DateTZ() {
  }

  /**
   * Construit une date avec un temsp en millisecondes
   * @param temps Temps en millisecondes
   */
  public DateTZ(long temps) {
    super(temps);
  }

  /**
   * Construit une date avec une date de type java.util.Date
   * @param date Date utilisée pour initialisé la DateTZ
   */
  public DateTZ(java.util.Date date) {
    super(date != null ? date.getTime() : 0L);
  }

  //  /**
  //   * Obtenir une date ajusté à un fuseau horaire spécifique.
  //   * @return Date ajustée au fuseau horaire
  //   * @param fuseauHoraire
  //   */
  //  public DateTZ getDate(TimeZone fuseauHoraireClient) {
  //    DateTZ ajustee = null;
  //
  //    if (fuseauHoraireClient != null) {
  //      TimeZone fuseauDepart = polarite == SERVEUR_A_CLIENT ? TimeZone.getDefault() : fuseauHoraireClient;
  //      TimeZone fuseauArrive = polarite == SERVEUR_A_CLIENT ? fuseauHoraireClient : TimeZone.getDefault();
  //
  //      int decalageDepart = fuseauDepart.getRawOffset();
  //      int decalageArrive = fuseauArrive.getRawOffset();
  //
  //      decalageTotal = decalageArrive - decalageDepart;
  //
  //
  //    } else {
  //      ajustee = new DateTZ(this.getTime());
  //    }
  //
  //    return ajustee;
  //  }

  /**
   * Dans quel sens doit s'effectuer l'ajustement
   * @param polarite Sens de l'ajustement de fuseau horaire à effectuer
   */
  public void setPolarite(int polarite) {
    this.polarite = polarite;
  }

  /**
   * Obtenir le sens de l'ajustement à effectuer.
   * @return Polarité de l'ajustement des fuseaux horaires
   */
  public int getPolarite() {
    return polarite;
  }
}