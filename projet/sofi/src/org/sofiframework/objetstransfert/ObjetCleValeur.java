/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;


/**
 * Classe permettant d'emmagasiner un clé associer à un valeur.
 * Cet objet peut être un type de base tel que String ou un objet de transfert.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class ObjetCleValeur extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 1308196769471673762L;
  private Object objetCle;
  private Object objetValeur;

  public ObjetCleValeur() {
  }

  public ObjetCleValeur(Object objetCle, Object objetValeur) {
    this.objetCle = objetCle;
    this.objetValeur = objetValeur;
  }

  /**
   * Retourne l'objet de la clé
   * @return l'objet de la clé
   */
  public Object getObjetCle() {
    return objetCle;
  }

  /**
   * Fixer l'objet de la clé
   * @param objetCle l'objet de la clé
   */
  public void setObjetCle(Object objetCle) {
    this.objetCle = objetCle;
  }

  /**
   * Retourne l'objet valeur
   * @return l'objet valeur
   */
  public Object getObjetValeur() {
    return objetValeur;
  }

  /**
   * Fixer l'objet valeur
   * @param objetValeur l'objet valeur
   */
  public void setObjetValeur(Object objetValeur) {
    this.objetValeur = objetValeur;
  }
}
