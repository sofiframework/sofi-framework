/*
 * $URL: https://svn.nurunquebec.com/sofi/dev/v1.6.0/sources/SOFI/src/org/sofiframework/objetstransfert/DateTZ.java $
 * $LastChangedBy: j-m.pelletier $
 * $LastChangedDate: 2006-02-09 16:20:19 -0500 (jeu., 09 fÃ©vr. 2006) $
 * $LastChangedRevision: 617 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;

import java.util.Date;

/**
 * Classe entité hérité par les entités.
 * 
 * Gère la concurrence à l'aide du numéro de version.
 * 
 * @author Jean-Maxime Pelletier
 * @author Sergio Couture
 */
public class Entite extends ObjetTransfert {

  private static final long serialVersionUID = -5017774415963140937L;

  private Long id;
  private Integer version;
  private String codeUtilisateurCreation;
  private String codeUtilisateurModification;
  private Date dateCreation;
  private Date dateModification;

  public Entite() {
    this.setCle(new String[] { "id" });
  }

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public Date getDateModification() {
    return dateModification;
  }

  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }

  public String getCodeUtilisateurCreation() {
    return codeUtilisateurCreation;
  }

  public void setCodeUtilisateurCreation(String codeUtilisateurCreation) {
    this.codeUtilisateurCreation = codeUtilisateurCreation;
  }

  public String getCodeUtilisateurModification() {
    return codeUtilisateurModification;
  }

  public void setCodeUtilisateurModification(String codeUtilisateurModification) {
    this.codeUtilisateurModification = codeUtilisateurModification;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }
}