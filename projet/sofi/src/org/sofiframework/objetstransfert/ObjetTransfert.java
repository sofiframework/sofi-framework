/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.objetstransfert;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireXml;
import org.w3c.dom.Node;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.EncodedByteArrayConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * Objet décrivant un objet transférable entre les diverses couches d'une application.
 * <p>
 * IMPORTANT, si vous faites des tests unitaires avec les objets de transfert,
 * veuillez mettre appliquez la méthode setModifie(true); afin que la mise à jouer ou
 * l'insertion puisse se faire.
 * <p>
 * Cette classe sert de dépot de données permettant rapidement le transfert des données
 * de la couche modèle de l'application vers la couche vue. Cette classe offre des
 * fonctionnalités de gestion des erreurs ainsi que de conversion vers et de XML.
 * <p>
 * <i><b>Note :</b></i> Il est important pour que la lecture d'un XML que l'élément
 * Node passé à la méthode lireXml(Node) soit de type XmlElement. Il est possible
 * que dépendamment des classes trouvées dans le classPath ce soit un type
 * DeferedDocumentImpl ou DomElement. Dans le cas ou le type des DeferedDocumentImpl,
 * il s'agit d'une erreur causée par le jar de xerces. Lors que l'erreur est causée
 * par un DomElement, il faut placer ces trois librairies en haut de liste et le problème
 * devrait se régler :<br>
 * <li>BC4J Generic Domains
 * <li>BC4J Oracle Domains
 * <li>BC4J Runtime
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public abstract class ObjetTransfert implements Serializable, Cloneable {
  private static final long serialVersionUID = 2872303130739578965L;

  /**
   * Nom des propriétés de l'objet de transfert qui composent sa clé unique.
   */
  private String[] cle;

  /**
   * Si il y a erreur.
   */
  private transient boolean erreur;

  /**
   * Si il y a eu modification.
   */
  private boolean modifie = true;

  /**
   * Alias pouvant être substitué dans un document XML pour le nom de
   * la classe (en balise).
   */
  private transient HashMap aliasXml;

  /**
   * Les attributs du formulaire qui ont été modifié.
   * <p>
   * À noter que si le Map est vide, tout les attributs seront considérés
   * lors d'une mise à jour éventuelle avec la BD.
   */
  private Map attributsModifies = new HashMap();

  /**
   * Liste des attributs à ignorer lors de la population
   * automatique.
   */
  private HashSet listeAttributAIgnorer = new HashSet();

  /**
   * Permet d'ajouter un attribut modifie.
   * <p>
   * @param nomAttribut le nom de l'attribut qui a été modifié
   */
  public void ajouterAttributModifie(String nomAttribut) {
    attributsModifies.put(nomAttribut, Boolean.TRUE);
  }

  /**
   * Indique si l'attribut spécifié a été modifié
   * <p>
   * @param nomAttribut le nom de l'attribut à vérifier
   * @return true si l'attribut spécifié a été modifié.
   */
  public boolean isAttributModifie(String nomAttribut) {
    if (attributsModifies.get(nomAttribut) != null) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Réinitialise la liste des attributs qui ont été modifiés
   */
  public void reinitialiserLesAttributsModifies() {
    if (!attributsModifies.isEmpty()) {
      attributsModifies.clear();
    }
  }

  /**
   * Indique si la liste d'attributs modifiés est vide
   * <p>
   * @return true si le Map des attributs modifiés est vide.
   */
  public boolean isAttributsModifiesVide() {
    if (attributsModifies != null) {
      if (attributsModifies.isEmpty()) {
        return true;
      } else {
        return false;
      }
    }

    return true;
  }

  public Map getAttributsModifies() {
    return this.attributsModifies;
  }

  /**
   * Assigne un Map d'attributs modifies
   * <p>
   * @param attributsModifiesACopier le Map d'attributs modifes à copier
   */
  public void setAttributsModifies(Map attributsModifiesACopier) {
    attributsModifies.putAll(attributsModifiesACopier);
  }

  /**
   * Obtenir les nom des propriétés de l'objet de transfert qui composent sa clé unique.
   * @return les nom des propriétés de l'objet de transfert qui composent sa clé unique
   */
  public String[] getCle() {
    return cle;
  }

  /**
   * Fixer les nom des propriétés de l'objet de transfert qui composent sa clé unique
   * @param newCle les nom des propriétés de l'objet de transfert qui composent sa clé unique
   */
  public void setCle(String[] newCle) {
    cle = newCle;
  }

  /**
   * Obtenir si il y a erreur.
   * @return si il y a erreur
   */
  public boolean isErreur() {
    return erreur;
  }

  /**
   * Fixer la valeur indiquant si l'objet possède une erreur
   * <p>
   * @param erreur la valeur indiquant si l'objet possède une erreur
   */
  public void setErreur(boolean erreur) {
    this.erreur = erreur;
  }

  /**
   * Comparaison de l'objet avec une autre instance d'un objet.
   * @param o Objet à comparer
   * @return Si vrai les deux instances on les mêmes valeurs de propriétés
   */
  @Override
  public boolean equals(Object o) {
    return EqualsBuilder.reflectionEquals(this, o);
  }

  /**
   * Retourne si un attribut de l'objet de transfert a été modifié.
   * @return true/false si un attribut de l'objet de transfert a été modifié.
   */
  public boolean isModifie() {
    return modifie;
  }

  /**
   * Fixer si un attribut de l'objet de transfert a été modifié.
   * @param modifie true/false si un attribut de l'objet de transfert a été modifié.
   */
  public void setModifie(boolean modifie) {
    this.modifie = modifie;
  }

  /**
   * Méthode qui transforme l'objet courant en un fichier XML.
   * <p>
   * Cette méthode converti tous les attributs de l'objet courrant en une structure XML
   * <p>
   * @return Document le XML représentant l'objet
   */
  public Node ecrireXml() {
    return UtilitaireXml.ecrireXml(this);
  }

  /**
   * Méthode qui lit une Node XML et qui rempli un objet de transfert avec.
   * <p>
   * Afin d'éviter de voir des noms complets de classes dans le XML (où à l'inverse, afin
   * d'éviter de devoir mettre des noms complets de classes dans le Xml.<BR>
   * Ex : modele.objetstranfert.Utilisateur<BR>
   * Vous pouvez utiliser la méthode ajouterAliasXml(String, Class) avant d'exécuter le traitement.<BR>
   * utilisateur.ajouterAliasXml("adresse", modele.objetstransfert.Adresse.class);
   * @param node la Node XML contenant toutes les valeurs à copier dans l'objet de transfert
   */
  public void lireXml(Node node) {
    UtilitaireXml.lireXml(this, node);
  }

  public void lireXml(String xml) {
    creerXStream().fromXML(xml, this);
  }

  public void lireXml(Reader reader) {
    creerXStream().fromXML(reader, this);
  }

  public void lireXml(InputStream in) {
    lireXml(new InputStreamReader(in));
  }

  private XStream creerXStream() {
	XStream xstream = new XStream(new StaxDriver());
    //xstream.registerConverter(new EncodedByteArrayConverter());

    if (this.aliasXml != null) {
      Iterator iterateur = this.aliasXml.keySet().iterator();

      while (iterateur.hasNext()) {
        String nomAttribut = (String)iterateur.next();
        Class classe = (Class)this.aliasXml.get(nomAttribut);
        xstream.alias(nomAttribut, classe);
      }
    }

    return xstream;
  }

  /**
   * Méthode qui transforme en String le contenu de l'objet courrant sous forme de XML.
   * <p>
   * Cette méthode sert à afficher le contenu d'un fichier sous forme de XML
   * <p>
   * @return String le document XML sous forme de String
   */
  @Override
  public String toString() {
    return UtilitaireXml.toXML(this);
  }

  /**
   * Méthode qui ajoute un alias Xml dans le HashMap Alias.
   * <p>
   * Cette méthode sert à ajouter un alias dans le HashMap des alias, un alias sert
   * à faire le lien entre un nom d'élément Xml et un classe Java
   * <p>
   * @param nomAttribut le nom du paramètre affiché dans le XML
   * @param classe la classe que le nom d'attribut remplace
   */
  public void ajouterAliasXml(String nomAttribut, Class classe) {
    if (this.aliasXml == null) {
      this.aliasXml = new HashMap();
    }

    this.aliasXml.put(nomAttribut, classe);
  }

  /**
   * Retourne la valeur de la propriété de l'objet de transfert correspondant
   * a nom attribut spécifié.
   * @param nomAttribut
   * @return la valeur du nom attribut spécifié
   */
  public Object getPropriete(String nomAttribut) {
    return UtilitaireObjet.getValeurAttribut(this, nomAttribut);
  }

  /**
   * Permet de faire une copie de l'instance dans une autre instance de
   * l'objet de transfert
   * @return une instance d'objet de transfert
   */
  @Override
  public Object clone() {
    return UtilitaireObjet.clonerObjet(this);
  }

  /**
   * Obtenir le map des alias de propriétés de l'objet utilisés lors de
   * la conversion.
   *
   * @return Map des alias XML qui doivent être utilis!s lors de la
   * conversion XML de l'objet.
   */
  public HashMap getAliasXml() {
    return this.aliasXml;
  }

  /**
   * Est-ce que l'attribut est un attribut a ignorer de la population automatique.
   * <p>
   * @param nomAttribut l'attribut à tester si existant.
   * @return true si l'attribut est à ignorer de la population automatique.
   */
  public boolean isAttributAIgnorer(String nomAttribut) {
    return listeAttributAIgnorer.contains(nomAttribut);
  }

  /**
   * Permet d'ajouter un attribut à ignorer de la population automatique.
   * @param attributAExclure attribut à ignorer de la population automatique.
   */
  public void ajouterAttributAIgnorer(String attributAIgnorer) {
    this.listeAttributAIgnorer.add(attributAIgnorer);
  }

  /**
   * Retourne la liste des attribut a ignorer de la population automatique.
   * @return la liste des attribut a ignorer de la population automatique.
   * @since SOFI 2.0.3
   */
  public HashSet getAttributsAIgnorer() {
    if (listeAttributAIgnorer == null) {
      listeAttributAIgnorer = new HashSet();
    }

    if (!listeAttributAIgnorer.contains("cle")) {
      // Initialisation des attributs a exclure par défaut.
      listeAttributAIgnorer.add("cle");
      listeAttributAIgnorer.add("modifie");
      listeAttributAIgnorer.add("aliasXml");
      listeAttributAIgnorer.add("attributsModifies");
      listeAttributAIgnorer.add("listeAttributAExclure");
    }

    return this.listeAttributAIgnorer;
  }

  /**
   * valeur Hash de l'objet.
   * @return Valeur hash
   */
  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
