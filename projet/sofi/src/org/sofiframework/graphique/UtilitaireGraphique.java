/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.graphique;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.encoders.ImageEncoderFactory;
import org.jfree.chart.encoders.ImageFormat;
import org.jfree.data.general.DefaultPieDataset;


public class UtilitaireGraphique {
  /**
   * Générer une diagramme à tarte à partir d'un titre, une liste de libellés et une liste de données.
   * @param titre le titre du tableau
   * @param libelles les libellés des valeurs
   * @param donnees les données du tableau
   * @return une image en format 250x250 avec le contenu du graphique
   * @throws java.io.IOException
   */
  public static byte[] genererDiagrammeTarte(String titre, String[] libelles,
      double[] donnees) throws java.io.IOException {
    return genererDiagrammeTarte(titre, libelles, donnees, 250, 250);
  }

  /**
   * Générer une diagramme à tarte à partir d'un titre, une liste de libellés et une liste de données.
   * @param titre le titre du tableau
   * @param libelles les libellés des valeurs
   * @param donnees les données du tableau
   * @param largeur la largeur du graphique
   * @param hauteur la hauteur du graphique
   * @return une image avec le contenu du graphique
   * @throws java.io.IOException
   */
  public static byte[] genererDiagrammeTarte(String titre, String[] libelles,
      double[] donnees, int largeur, int hauteur) throws java.io.IOException {
    DefaultPieDataset dataset = new DefaultPieDataset();

    for (int i = 0; i < libelles.length; i++) {
      dataset.setValue(libelles[i], donnees[i]);
    }

    JFreeChart diagramme = ChartFactory.createPieChart(titre, dataset, true,
        true, false);

    return ImageEncoderFactory.newInstance(ImageFormat.JPEG).encode(diagramme.createBufferedImage(
        largeur, hauteur));
  }
}
