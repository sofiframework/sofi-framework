/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.calendrier;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Objet permettant d'offrir le détail d'une journée d'un instance Calendrier.
 * <p>
 * Notons que cette objet utilise la notion du polymorphisme afin de pouvoir
 * stocker un objet de tout type correspondant au détail d'une date d'un calendrier.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.composantweb.calendrier.Calendrier
 */
public class DateCalendrier {
  /**
   * Objet de détail de la date du calendrier.
   */
  private Object objetDetail;

  /**
   * Le calendrier ayant le date courante.
   */
  private Calendar calendar;

  /**
   * Référence au calendrier contenant le mois affiché.
   * Utile pour savoir si la date courante est dans le mois du calendrier affiché
   * ou dans les derniers jours du mois précédent ou les premiers jours du
   * mois suivant.
   */
  private Date calendrierAffiche;

  /**
   * Contructeur par défault.
   */
  public DateCalendrier() {
  }

  /**
   * Constructeur de DateCalendrier avec l'aide d'une date (java.util.Date).
   * @param newDate Nouvelle date
   */
  public DateCalendrier(Date newDate) {
    calendar = Calendar.getInstance();
    calendar.setTime(newDate);
    calendar.set(Calendar.AM_PM, Calendar.AM);
    calendar.set(Calendar.HOUR, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  }

  /**
   * Retourne un objet correspondant au détail de la journée
   * @return l'objet détail du jour courant.
   */
  public Object getObjetDetail() {
    return objetDetail;
  }

  /**
   * Fixer un objet correspondant au détail de la journée
   * @param newObjetDetail
   */
  public void setObjetDetail(Object newObjetDetail) {
    objetDetail = newObjetDetail;
  }

  public void setCalendrierAffiche(Date calendrierAffiche) {
    this.calendrierAffiche = calendrierAffiche;
  }

  public Date getCalendrierAffiche() {
    return calendrierAffiche;
  }

  /**
   * Retourne la date de l'instance DateCalendrier
   * @return java.util.Date l'objet date
   */
  public Date getDate() {
    return this.calendar.getTime();
  }

  /**
   * Permet de changer la date de l'instance DateCalendrier
   * L'objetDetail n'est pas modifié.
   * @param date
   */
  public void setDate(Date date) {
    calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.AM_PM, Calendar.AM);
    calendar.set(Calendar.HOUR, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  }

  /**
   * @return Le numéro du jour courant. Ex: 15.
   */
  public String getNoJourTexte() {
    return "" + calendar.get(Calendar.DATE);
  }

  /**
   * @return Le numéro du mois courant. Ex: 9.
   */
  public String getNoMoisTexte() {
    return "" + calendar.get(Calendar.MONTH);
  }

  /**
   * @return L'année courante.
   */
  public String getAnnee() {
    return "" + this.calendar.get(Calendar.YEAR);
  }

  /**
   * Retourne le jour de l'instance en texte.
   * Par exemple:
   * Mercredi le 11 mars 2004
   * @return le jour courant en texte.
   */
  public String getJourTextePourDetailJournee() {
    return this.getJourTexte() + " le " + this.getNoJourTexte() + " " +
        this.getMoisTexte().toLowerCase() + " " + this.getAnnee();
  }

  /**
   * Retourne la date courante sous format AAAA-MM-JJ
   * @return la date courante sous format AAAA-MM-JJ
   */
  public String getDateTexte() {
    return UtilitaireDate.convertirEn_AAAA_MM_JJ(this.calendar.getTime());
  }

  /**
   * Retourne le mois sous format texte (Janvier, Février, etc..)
   * @return le mois courante sous forme texte.
   */
  public String getMoisTexte() {
    return Calendrier.getMoisTexte(this.calendar.get(Calendar.MONTH));
  }

  /**
   * Retourne la première lettre de la journée de la semaine.
   * Par Exemple D pour Dimanche.
   * @return la première lette de la journée de la semaine.
   */
  public String getJourPremiereLettre() {
    return new DateFormatSymbols().getShortWeekdays()[this.calendar.get(Calendar.DAY_OF_WEEK)].substring(0,
        1);
  }

  /**
   * Retourne la journée de la semaine sous format texte:
   * Par exemple : Dimanche, Lundi, etc..
   * @return la journée de la semaine
   */
  public final String getJourTexte() {
    return (new DateFormatSymbols().getWeekdays()[this.calendar.get(Calendar.DAY_OF_WEEK)]);
  }

  public boolean isJourPasse() {
    Calendar aujourdhuiHeureZero = Calendar.getInstance();
    aujourdhuiHeureZero.set(Calendar.AM_PM, Calendar.AM);
    aujourdhuiHeureZero.set(Calendar.HOUR, 0);
    aujourdhuiHeureZero.set(Calendar.MINUTE, 0);
    aujourdhuiHeureZero.set(Calendar.SECOND, 0);
    aujourdhuiHeureZero.set(Calendar.MILLISECOND, 0);

    return this.calendar.getTime().before(aujourdhuiHeureZero.getTime());
  }

  public boolean isJourCourant() {
    Calendar aujourdhuiHeureZero = Calendar.getInstance();
    aujourdhuiHeureZero.set(Calendar.AM_PM, Calendar.AM);
    aujourdhuiHeureZero.set(Calendar.HOUR, 0);
    aujourdhuiHeureZero.set(Calendar.MINUTE, 0);
    aujourdhuiHeureZero.set(Calendar.SECOND, 0);
    aujourdhuiHeureZero.set(Calendar.MILLISECOND, 0);

    return this.calendar.getTime().equals(aujourdhuiHeureZero.getTime());
  }

  public boolean isJourFutur() {
    Calendar demainHeureZeroMoinsUn = Calendar.getInstance();
    demainHeureZeroMoinsUn.set(Calendar.AM_PM, Calendar.AM);
    demainHeureZeroMoinsUn.set(Calendar.HOUR, 0);
    demainHeureZeroMoinsUn.set(Calendar.MINUTE, 0);
    demainHeureZeroMoinsUn.set(Calendar.SECOND, 0);
    demainHeureZeroMoinsUn.set(Calendar.MILLISECOND, 0);
    demainHeureZeroMoinsUn.add(Calendar.DATE, 1);
    demainHeureZeroMoinsUn.add(Calendar.MILLISECOND, -1);

    return this.calendar.getTime().after(demainHeureZeroMoinsUn.getTime());
  }

  public boolean isMoisPasse() {
    if (getCalendrierAffiche() == null) {
      throw new NullPointerException(
          "Le mois du calendrier affiché n'est pas initialisé." +
          "\n\t--> Fixer à l'aide de setMoisCalendrierAffiche(Date moisCalendrierAffiche)");
    }

    Calendar premierJourDuMoisCalendrierHeureZero = Calendar.getInstance();
    premierJourDuMoisCalendrierHeureZero.setTime(getCalendrierAffiche());
    premierJourDuMoisCalendrierHeureZero.set(Calendar.DATE, 1);
    premierJourDuMoisCalendrierHeureZero.set(Calendar.AM_PM, Calendar.AM);
    premierJourDuMoisCalendrierHeureZero.set(Calendar.HOUR, 0);
    premierJourDuMoisCalendrierHeureZero.set(Calendar.MINUTE, 0);
    premierJourDuMoisCalendrierHeureZero.set(Calendar.SECOND, 0);
    premierJourDuMoisCalendrierHeureZero.set(Calendar.MILLISECOND, 0);

    premierJourDuMoisCalendrierHeureZero.add(Calendar.MILLISECOND, -1);

    return this.calendar.getTime().before(premierJourDuMoisCalendrierHeureZero.getTime());
  }

  public boolean isMoisCourant() {
    if (getCalendrierAffiche() == null) {
      throw new NullPointerException(
          "Le mois du calendrier affiché n'est pas initialisé." +
          "\n\t--> Fixer à l'aide de setMoisCalendrierAffiche(Date moisCalendrierAffiche)");
    }

    Calendar calendrierAffiche = Calendar.getInstance();
    calendrierAffiche.setTime(getCalendrierAffiche());

    return (calendrierAffiche.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) &&
        (calendrierAffiche.get(Calendar.MONTH) == calendar.get(Calendar.MONTH));
  }

  public boolean isMoisFutur() {
    if (getCalendrierAffiche() == null) {
      throw new NullPointerException(
          "Le mois du calendrier affiché n'est pas initialisé." +
          "\n\t--> Fixer à l'aide de setMoisCalendrierAffiche(Date moisCalendrierAffiche)");
    }

    Calendar moisProchainHeureZeroMoinsUn = Calendar.getInstance();
    moisProchainHeureZeroMoinsUn.setTime(getCalendrierAffiche());
    moisProchainHeureZeroMoinsUn.set(Calendar.DATE, 1);
    moisProchainHeureZeroMoinsUn.set(Calendar.AM_PM, Calendar.AM);
    moisProchainHeureZeroMoinsUn.set(Calendar.HOUR, 0);
    moisProchainHeureZeroMoinsUn.set(Calendar.MINUTE, 0);
    moisProchainHeureZeroMoinsUn.set(Calendar.SECOND, 0);
    moisProchainHeureZeroMoinsUn.set(Calendar.MILLISECOND, 0);
    moisProchainHeureZeroMoinsUn.add(Calendar.MONTH, 1);
    moisProchainHeureZeroMoinsUn.add(Calendar.MILLISECOND, -1);

    return this.calendar.getTime().after(moisProchainHeureZeroMoinsUn.getTime());
  }
}
