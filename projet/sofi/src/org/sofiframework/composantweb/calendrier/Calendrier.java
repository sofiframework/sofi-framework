/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.calendrier;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Objet permettant la gestion d'un calendrier. Un calendrier représente
 * un mois en particulier. Il peut aussi contenir les jours précédents le
 * premier jour du mois pour compléter la première semaine et les jours
 * suivants pour compléter la dernière semaine.
 * <p>
 * Le calendrier peut avoir différentes caractéristiques.
 * Par exemple:
 * <ul>
 *   <li>La première journée de la semaine (par défaut Dimanche);</li>
 *   <li>Le nombre de journée dans une semaine (par défaut 7);</li>
 *   <li>Le calendrier débute la première journée de la semaine suivante
 *   si le premier jour du mois n'est pas la première journée de la semaine
 *   (par défaut faux);</li>
 *   <li>Le calendrier débute la première journée de la semaine précédente
 *   si le premier jour du mois n'est pas la première journée de la semaine
 *   (par défaut faux);</li>
 *   <li>Le calendrier se termine la dernière journée de la semaine même si
 *   cette journée n'est pas dans le mois courant (par défaut faux).</li>
 * </ul>
 * Le détail d'une journée correspond à une instance de DateCalendrier.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 2.0
 * @see org.sofiframework.composantweb.calendrier.DateCalendrier
 */
public class Calendrier {

  private static Log log = LogFactory.getLog(Calendrier.class);

  /**
   * Index du premier jour d'une semaine dans le calendrier.
   * La semaine commence normalement un dimanche et se termine normalement un samedi.
   * Il est possible de raccourcir la semaine. Ex: Du lundi au vendredi.
   * Il n'est pas possible d'avoir une semaine débutant le lundi et se terminant le dimanche.
   */
  private int premierJourSemaine = Calendar.SUNDAY;

  /**
   * Nombre de jour dans la semaine dans le calendrier.
   */
  private int nbJourSemaine = 7;

  /**
   * Si on doit débuter le calendrier le premier jour de la semaine.
   */
  private boolean debuterCalendrierPremierJourSemaine = false;

  /**
   * Si la dernière semaine doit être affichée complètement.
   */
  private boolean derniereSemaineComplete = false;

  /**
   * Si la première semaine doit être affichée complètement.
   */
  private boolean premiereSemaineComplete = false;

  /**
   * Liste des semaines
   */
  List semaines = new ArrayList(0);

  /**
   * Calendrier en cours
   */
  Calendar calendrierCourant = Calendar.getInstance();

  /**
   * Date de calendrier en cours.
   */
  DateCalendrier dateCalendrierCourante;

  /**
   * Constructeur par défault.
   */
  public Calendrier() {
  }

  /**
   * @return La date de début du calendrier.
   */
  public Date getPremiereDateDuCalendrier() {
    if ((semaines != null) && !semaines.isEmpty()) {
      return ((DateCalendrier) ((List) semaines.get(0)).get(0)).getDate();
    }

    return null;
  }

  /**
   * @return La dernière date du calendrier.
   */
  public Date getDerniereDateDuCalendrier() {
    if ((semaines != null) && !semaines.isEmpty()) {
      List journees = (List) semaines.get(this.semaines.size() - 1);

      return ((DateCalendrier) journees.get(journees.size() - 1)).getDate();
    }

    return null;
  }

  /**
   * @return Le nombre de jours dans le calendrier.
   */
  public int getNbJourDansLeCalendrier() {
    int compteur = 0;
    Iterator semaines = this.semaines.iterator();

    while (semaines.hasNext()) {
      List journees = (List) semaines.next();
      compteur += journees.size();
    }

    return compteur;
  }

  /**
   * @return Retourne le jour courant sous forme d'instance de DateCalendrier.
   */
  public DateCalendrier getDateCalendrierCourante() {
    return this.dateCalendrierCourante;
  }

  /**
   * @return Le calendrier du mois courant sous forme de liste de semaines.
   */
  public List getSemaines() {
    return this.semaines;
  }

  /**
   * @return Le nombre de semaine dans le calendrier.
   */
  public int getNbSemaineCalendrier() {
    return this.semaines.size();
  }

  /**
   * Retourne les DateCalendrier d'une semaine pour un numéro de semaine spécifique.
   * @param noSemaine numéro de semaine. Indexé 0.
   * @return liste de DateCalendrier pour une semaine.
   */
  public List getDatesCalendrierPourSemaine(int noSemaine) {
    return (List) this.semaines.get(noSemaine);
  }

  /**
   * @return L'année du calendrier courant.
   */
  public int getAnnee() {
    return this.calendrierCourant.get(Calendar.YEAR);
  }

  /**
   * @return Le mois du calendrier courant sous form texte.
   */
  public String getMoisTexte() {
    return getMoisTexte(this.calendrierCourant.get(Calendar.MONTH));
  }

  /**
   * Permet la création d'un mois à partir d'un date spécifique.
   * @param date date spécifique
   * @return la liste des semaines du calendrier créé.
   */
  public List creerMois(Date date) {
    ArrayList uneSemaine;
    this.semaines = new ArrayList();

    calendrierCourant = Calendar.getInstance();
    calendrierCourant.setTime(date);
    calendrierCourant.set(Calendar.AM_PM, Calendar.AM);
    calendrierCourant.set(Calendar.HOUR, 0);
    calendrierCourant.set(Calendar.MINUTE, 0);
    calendrierCourant.set(Calendar.SECOND, 0);
    calendrierCourant.set(Calendar.MILLISECOND, 0);

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(calendrierCourant.getTime());

    Calendar calendarDernierJourMois = Calendar.getInstance();
    calendarDernierJourMois.setTime(calendar.getTime());
    calendarDernierJourMois.set(Calendar.DATE, 1);
    calendarDernierJourMois.add(Calendar.MONTH, 1);
    calendarDernierJourMois.add(Calendar.DATE, -1);

    Calendar prochainDimanche = Calendar.getInstance();

    Calendar calendarMilieuDuMois = Calendar.getInstance();
    calendarMilieuDuMois.setTime(calendar.getTime());
    calendarMilieuDuMois.set(Calendar.DATE, 15);

    Calendar tempCalendar = Calendar.getInstance();

    calendar.set(Calendar.DATE, 1);

    // Trouver le premier jour de la semaine suivant?
    if (isDebuterMoisPremierJourSemaine()) {
      while (calendar.get(Calendar.DAY_OF_WEEK) != getPremierJourSemaine()) {
        calendar.add(Calendar.DATE, 1);
      }
    } else if (isPremiereSemaineComplete()) {
      // Reculer si la première semaine doit être complète et si elle ne commence pas par le premier jour de la semaine
      while (calendar.get(Calendar.DAY_OF_WEEK) != getPremierJourSemaine()) {
        calendar.add(Calendar.DATE, -1);
      }
    }

    DateCalendrier dateCalendrier;

    while ((calendar.get(Calendar.MONTH) == this.calendrierCourant.get(
        Calendar.MONTH)) || this.semaines.isEmpty()) {
      // Création d'une nouvelle semaine.
      uneSemaine = new ArrayList();

      prochainDimanche.setTime(calendar.getTime());

      prochainDimanche.add(Calendar.DATE,
          8 - prochainDimanche.get(Calendar.DAY_OF_WEEK));

      // On commence toujours un dimanche
      while (calendar.before(prochainDimanche)) {
        // On ajoute la journée seulement si elle est plus grande ou égale à la première journée de la semaine
        // Si elle est plus petite ou égale à la dernière journée de la semaine
        // Si elle est dans le mois courant
        if ((calendar.get(Calendar.DAY_OF_WEEK) >= getPremierJourSemaine()) &&
            (calendar.get(Calendar.DAY_OF_WEEK) <= ((getNbJourSemaine() +
                getPremierJourSemaine()) - 1))) {
          if (calendar.equals(calendarMilieuDuMois) ||
              (calendar.after(calendarMilieuDuMois) &&
                  ((isDerniereSemaineComplete() &&
                      (!uneSemaine.isEmpty() ||
                          (uneSemaine.isEmpty() &&
                              (calendar.get(Calendar.MONTH) == this.calendrierCourant.get(
                                  Calendar.MONTH))))) ||
                                  (!isDerniereSemaineComplete() &&
                                      (calendar.get(Calendar.MONTH) == this.calendrierCourant.get(
                                          Calendar.MONTH))))) ||
                                          (calendar.before(calendarMilieuDuMois) &&
                                              (isPremiereSemaineComplete() ||
                                                  (!isPremiereSemaineComplete() &&
                                                      (calendar.get(Calendar.MONTH) == this.calendrierCourant.get(
                                                          Calendar.MONTH)))))) {
            dateCalendrier = new DateCalendrier(calendar.getTime());
            dateCalendrier.setCalendrierAffiche(this.calendrierCourant.getTime());
            uneSemaine.add(dateCalendrier);
            if (log.isDebugEnabled()) {
              log.debug("Ajoute le: " + calendar.getTime());
            }
          } else {
            if (log.isDebugEnabled()) {
              log.debug("Passe le: " + calendar.getTime());
            }
          }
        } else {
          if (log.isDebugEnabled()) {
            log.debug("Passe le: " + calendar.getTime() +
                " - Pas dans la semaine en cours affichable");
          }
        }

        // Ajouter une journee au calendrier
        calendar.add(Calendar.DATE, 1);

        if (log.isDebugEnabled()) {
          log.debug(calendar.getTime() + "DAY_OF_WEEK: " +
              calendar.get(Calendar.DAY_OF_WEEK));
        }
      }

      if (log.isDebugEnabled()) {
        log.debug("Ajoute la semaine");
      }

      if (!uneSemaine.isEmpty()) {
        // On s'assure que la semaine contient au moins une journée du mois courant.
        Iterator iter = uneSemaine.iterator();
        boolean isTrouve = false;

        while (iter.hasNext() && !isTrouve) {
          DateCalendrier dateCalen = (DateCalendrier) iter.next();
          tempCalendar.setTime(dateCalen.getDate());

          if (tempCalendar.get(Calendar.MONTH) == this.calendrierCourant.get(
              Calendar.MONTH)) {
            isTrouve = true;
            this.semaines.add(uneSemaine);
          }
        }
      }
    }

    return this.semaines;
  }

  /**
   * Retourne une instance de DateCalendrier (Détail pour une journée) pour une
   * date du format 9999-99-99.
   * @param dateString une date formattée
   * @return une instance de DateCalendrier (Détail pour une journée)
   * @see org.sofiframework.composantweb.calendrier.DateCalendrier
   */
  public DateCalendrier getUneDateCalendrier(String dateString) {
    return getUneDateCalendrier(UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate(
        dateString));
  }

  /**
   * Retourne une instance de DateCalendrier (Détail pour une journée) pour une
   * date de format java.util.Date.
   * @param date une date sous format java.util.Date
   * @return une instance de DateCalendrier (Détail pour une journée)
   * @see org.sofiframework.composantweb.calendrier.DateCalendrier
   */
  public DateCalendrier getUneDateCalendrier(Date date) {
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(date);
    calendrier.set(Calendar.AM_PM, Calendar.AM);
    calendrier.set(Calendar.HOUR, 0);
    calendrier.set(Calendar.MINUTE, 0);
    calendrier.set(Calendar.SECOND, 0);
    calendrier.set(Calendar.MILLISECOND, 0);

    boolean trouve = false;
    Iterator semaines = this.semaines.iterator();

    while ((semaines.hasNext()) && !trouve) {
      Iterator journees = ((List) semaines.next()).iterator();

      while ((journees.hasNext()) && !trouve) {
        this.dateCalendrierCourante = (DateCalendrier) journees.next();

        if (this.dateCalendrierCourante.getDate().equals(calendrier.getTime())) {
          trouve = true;
        }
      }
    }

    if (!trouve) {
      this.dateCalendrierCourante = null;
    }

    return this.dateCalendrierCourante;
  }

  /**
   * Retourne le 15ieme jour du mois courant.
   * Peut servir de clé pour une liste déroulante qui correspond
   * à un mois d'une année.
   * @return le 15ieme jour du mois courant Format: AAAA-MM-JJ
   */
  public String getClePourListeAnneeMois() {
    return UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour("" +
        this.calendrierCourant.get(Calendar.YEAR),
        "" + this.calendrierCourant.get(Calendar.MONTH), "15");
  }

  /**
   * Retourne une liste servant à une liste déroulante (ObjetCleValeur) donnant
   * les 11 mois précédent au mois courant et les 12 mois suivants au mois
   * courant.
   * @param date date de départ (mois courant)
   * @return liste de mois précédents et mois suivants au mois courant.
   */
  static public List getListeMoisSuivantPrecedent(Date date) {
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(date);
    calendrier.set(Calendar.DATE, 1);
    calendrier.add(Calendar.MONTH, -11);

    List mois = new ArrayList(24);
    String descriptionMois;
    String valeurMois;

    for (int i = 0; i < 24; i++) {
      descriptionMois = getMoisTexte(calendrier.get(Calendar.MONTH)) + " " +
          calendrier.get(Calendar.YEAR);
      valeurMois = UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour("" +
          calendrier.get(Calendar.YEAR),
          "" + (calendrier.get(Calendar.MONTH) + 1), "15");

      mois.add(new ObjetCleValeur(descriptionMois, valeurMois));
      calendrier.add(Calendar.MONTH, 1);
    }

    return mois;
  }

  public int getPositionPremierJourDuCalendrier() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(getPremiereDateDuCalendrier());

    return calendar.get(Calendar.DAY_OF_WEEK);
  }

  public int getPositionDernierJourDuCalendrier() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(getDerniereDateDuCalendrier());

    return calendar.get(Calendar.DAY_OF_WEEK);
  }

  public int getPositionPremierJourDuMois() {
    Calendar calendar = (Calendar) this.calendrierCourant.clone();
    calendar.set(Calendar.DATE, 1);

    return calendar.get(Calendar.DAY_OF_WEEK);
  }

  public int getPositionDernierJourDuMois() {
    Calendar calendar = (Calendar) this.calendrierCourant.clone();
    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
    calendar.set(Calendar.DATE, 1);
    calendar.add(Calendar.DATE, -1);

    return calendar.get(Calendar.DAY_OF_WEEK);
  }

  /**
   * @return Le nom du mois au format court.
   * @param moisInt L'index du mois désiré. Indexé 0. Utiliser les constante de Calendar.
   */
  public static String getMoisTexte(int moisInt) {
    return new DateFormatSymbols().getMonths()[moisInt];
  }

  public void setDerniereSemaineComplete(boolean derniereSemaineComplete) {
    this.derniereSemaineComplete = derniereSemaineComplete;
  }

  public boolean isDerniereSemaineComplete() {
    return derniereSemaineComplete;
  }

  public void setDebuterMoisPremierJourSemaine(
      boolean debuterCalendrierPremierJourSemaine) {
    if (debuterCalendrierPremierJourSemaine && isPremiereSemaineComplete()) {
      throw new IllegalArgumentException(
          "On ne peut pas faire débuter le mois au premier jour de la semaine et " +
              "\navoir la première semaine du calendrier complète." +
          "\n\tIl faut avoir un des 2 indicateurs à faux. (setDebuterMoisPremierJourSemaine ou setPremiereSemaineComplete).");
    }

    this.debuterCalendrierPremierJourSemaine = debuterCalendrierPremierJourSemaine;
  }

  public boolean isDebuterMoisPremierJourSemaine() {
    return debuterCalendrierPremierJourSemaine;
  }

  public void setPremierJourSemaine(int premierJourSemaine) {
    if (((premierJourSemaine + getNbJourSemaine()) - 1) > 7) {
      throw new IllegalArgumentException(
          "Le premier jour de la semaine doit concorder avec le nombre de jour par semaine." +
              "\n\tEx: Doit être dimanche si le nombre de jour est 7." +
              "\n\tIl peut être lundi si le nombre de jours par semaine est 6 ou moins." +
          "\n\t--> *** Ajustez le nombre de jours par semaine en premier. ***");
    }

    this.premierJourSemaine = premierJourSemaine;
  }

  public int getPremierJourSemaine() {
    return premierJourSemaine;
  }

  public void setNbJourSemaine(int nbJourSemaine) {
    if (((getPremierJourSemaine() + nbJourSemaine) - 1) > 7) {
      throw new IllegalArgumentException(
          "Le nombre de jour par semaine doit concorder avec le premier jour de la semaine." +
              "\n\tEx: Doit être 6 ou moins si le premier jour de la semaine est lundi." +
          "\n\t--> Ajustez le nombre de jours par semaine en premier");
    }

    this.nbJourSemaine = nbJourSemaine;
  }

  public int getNbJourSemaine() {
    return nbJourSemaine;
  }

  public List getNomsJoursSemaine() {
    List nomJours = Arrays.asList(new DateFormatSymbols().getWeekdays());

    return nomJours.subList(getPremierJourSemaine(),
        (getNbJourSemaine() + getPremierJourSemaine()));
  }

  public void setPremiereSemaineComplete(boolean premiereSemaineComplete) {
    if (premiereSemaineComplete && isDebuterMoisPremierJourSemaine()) {
      throw new IllegalArgumentException(
          "On ne peut pas avoir la première semaine du calendrier complète et " +
              "\nfaire débuter le mois au premier jour de la semaine." +
          "\n\tIl faut avoir un des 2 indicateurs à faux. (setDebuterMoisPremierJourSemaine ou setPremiereSemaineComplete).");
    }

    this.premiereSemaineComplete = premiereSemaineComplete;
  }

  public boolean isPremiereSemaineComplete() {
    return premiereSemaineComplete;
  }

  /* Pour fin de test seulement*/
  public static void main(String[] args) throws Exception {
    Calendrier calendrier = new Calendrier();
    calendrier.setDerniereSemaineComplete(false);
    calendrier.setPremiereSemaineComplete(false);
    calendrier.setDebuterMoisPremierJourSemaine(false);
    calendrier.setNbJourSemaine(7);
    calendrier.setPremierJourSemaine(Calendar.SUNDAY);
    calendrier.creerMois(new Date());

    DateCalendrier dateCalendrier = calendrier.getUneDateCalendrier(new Date());
    if (log.isDebugEnabled()) {
      log.debug("Date Calendrier: " +
          dateCalendrier.getJourTextePourDetailJournee());
    }

    if (log.isDebugEnabled()) {
      log.debug("Toutes les journée du mois de: " +
          calendrier.getMoisTexte());
    }

    Iterator semaines = calendrier.getSemaines().iterator();

    while (semaines.hasNext()) {
      Iterator journees = ((List) semaines.next()).iterator();

      while (journees.hasNext()) {
        dateCalendrier = (DateCalendrier) journees.next();
        if (log.isDebugEnabled()) {
          log.debug(dateCalendrier.getJourTextePourDetailJournee());
        }
      }
    }
  }
}
