package org.sofiframework.composantweb.liste;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Filtre permettant d'appliquer un opérateur spécifique sur un attribut
 * du filtre.
 * @author jfbrassard
 *
 */

public class FiltreOperateurAttribut extends ObjetTransfert {

  private static final long serialVersionUID = -4148207832656714878L;

  private String nomAttributFiltre;
  private String nomAttributEntite;
  private Operateur operateur;

  public FiltreOperateurAttribut(String nomAttributFiltre, String nomAttributEntite, Operateur operateur) {
    this.nomAttributFiltre = nomAttributFiltre;
    this.nomAttributEntite = nomAttributEntite;
    this.operateur = operateur;

  }
  /**
   * @return the nomAttributFiltre
   */
  public String getNomAttributFiltre() {
    return nomAttributFiltre;
  }
  /**
   * @param nomAttributFiltre the nomAttributFiltre to set
   */
  public void setNomAttributFiltre(String nomAttributFiltre) {
    this.nomAttributFiltre = nomAttributFiltre;
  }
  /**
   * @return the nomAttributEntite
   */
  public String getNomAttributEntite() {
    return nomAttributEntite;
  }
  /**
   * @param nomAttributEntite the nomAttributEntite to set
   */
  public void setNomAttributEntite(String nomAttributEntite) {
    this.nomAttributEntite = nomAttributEntite;
  }
  /**
   * @return the operateur
   */
  public Operateur getOperateur() {
    return operateur;
  }
  /**
   * @param operateur the operateur to set
   */
  public void setOperateur(Operateur operateur) {
    this.operateur = operateur;
  }

}
