/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe technique permettant d'emmagasiner les informations
 * d'un intervalle entre 2 attributs d'un filtre.
 * @author Jean-François Brassard (Nurun inc.)
 * @version Cours SOFI 1.0
 */
public class FiltreIntervalleAttribut extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -5637660818220664754L;
  private String attributDebut;
  private String attributFin;
  private String attributDestination;

  /**
   * Constructeur par défaut.
   * @param attributDebut l'attribut du début d'intervalle
   * @param attributFin l'attribut de fin de l'intervalle.
   * @param attributDestination le nom d'attribut de destination.
   * @throws org.sofiframework.exception.SOFIException exception runtime de SOFI
   */
  protected FiltreIntervalleAttribut(String attributDebut, String attributFin,
      String attributDestination) throws SOFIException {
    this.attributDebut = attributDebut;
    this.attributFin = attributFin;
    this.attributDestination = attributDestination;

    if (!((this.attributDebut != null) && (this.attributFin != null) &&
        (this.attributDestination != null))) {
      throw new SOFIException(
          "FiltreIntervalleAttribut: Vous devez spécifier les attributs de début, fin et de destination!");
    }
  }

  /**
   * Retourne le nom d'attribut du début de l'intervalle.
   * @return le nom d'attribut du début de l'intervalle.
   */
  public String getAttributDebut() {
    return attributDebut;
  }

  /**
   * Fixer le nom d'attribut du début de l'intervalle.
   * @param attributDebut le nom d'attribut du début de l'intervalle.
   */
  public void setAttributDebut(String attributDebut) {
    this.attributDebut = attributDebut;
  }

  /**
   * Retourne le nom d'attribut de fin de l'intervalle.
   * @return le nom d'attribut de fin de l'intervalle.
   */
  public String getAttributFin() {
    return attributFin;
  }

  /**
   * Fixer le nom d'attribut de fin de l'intervalle.
   * @param attributFin le nom d'attribut de fin de l'intervalle.
   */
  public void setAttributFin(String attributFin) {
    this.attributFin = attributFin;
  }

  /**
   * Retourne le nom d'attribut de destination pour tester l'intervalle.
   * @return le nom d'attribut de destination pour tester l'intervalle.
   */
  public String getAttributDestination() {
    return attributDestination;
  }

  /**
   * Fixer le nom d'attribut de destination pour tester l'intervalle.
   * @param attributDestination le nom d'attribut pour tester l'intervalle.
   */
  public void setAttributDestination(String attributDestination) {
    this.attributDestination = attributDestination;
  }
}
