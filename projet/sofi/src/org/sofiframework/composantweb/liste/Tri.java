/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet qui décrit le tri d'une liste.
 */
public class Tri implements Serializable {

  private static final long serialVersionUID = -1017835932963361301L;
  /**
   * Mot clé SQL pour un trie ascendant.
   */
  public static final String TRI_ASCENDANT = "ASC";

  /**
   * Mot clé SQL pour un trie descendant.
   */
  public static final String TRI_DESCENDANT = "DESC";

  /** L'attribut ayant le tri **/
  private String triAttribut;

  /** L'ordre de tri de la liste **/
  private boolean ascendant;

  /** Répertoire des attributs logique à trier pour un attribut spécifié */
  private HashMap repertoireAttributLogiqueATrier = new HashMap();

  /** Un tri fixe **/
  private String triFixe;

  /**
   * Répertoire des tri personnalisé (valeurs sql qui doivent être utilisées
   * au lieu de seulement le nom de colonne).
   */
  private HashMap repertoireTriComplexe = new HashMap();

  public Tri() {
  }

  public Tri(String nomAttribut, boolean asc) {
    setTriAttribut(nomAttribut);
    this.ascendant = asc;
  }

  public final void setAscendant(boolean asc) {
    this.ascendant = asc;
  }

  public boolean isAscendant() {
    return ascendant;
  }

  public boolean isDescendant() {
    return !ascendant;
  }

  public final void setTriAttribut(String attribut) {
    if ((attribut != null) && !"".equals(attribut)) {
      triAttribut = UtilitaireString.convertirPremiereLettreEnMajuscule(attribut);
    }
  }

  /**
   * Retourne la liste des attributs à trier
   */
  public Collection attributs() {
    if (triAttribut == null) {
      return Collections.EMPTY_LIST;
    } else {
      ArrayList attributs = new ArrayList();
      ArrayList liste = getListeAttributTriLogique(triAttribut);

      if ((liste == null) || liste.isEmpty()) {
        attributs.add(new AttributTri(triAttribut, isAscendant()));
      } else {
        for (int i = 0, n = liste.size(); i < n; i++) {
          ListeTriAttribut attr = (ListeTriAttribut) liste.get(i);
          boolean asc = TRI_ASCENDANT.equals(attr.getTriAttributOrdre());
          if (!this.isAscendant()) {
            asc = !asc;
          }
          attributs.add(new AttributTri(attr.getTriAttributSupplementaire(), asc));
        }
      }

      return attributs;
    }
  }

  /**
   * Permet d'ajouter des attributs logiques avec son ordre de tri pour
   * un attribut sélectionné pour tri.
   * @param nomAttributSelectionne le nom de l'attribut dans la page JSP
   * @param nomAttributLogiqueATrier le nom de la proriété de l'objet d'affaire qui doit être trié
   * @param isAscendant l'ordre de trie
   */
  public void ajouterAttributLogiqueAtrier(String nomAttributSelectionne,
      String nomAttributLogiqueATrier, boolean isAscendant) {
    if (nomAttributSelectionne == null) {
      throw new NullPointerException("nomAttributSelectionne");
    }

    if (nomAttributLogiqueATrier == null) {
      throw new NullPointerException("nomAttributLogiqueATrier");
    }

    nomAttributSelectionne = UtilitaireString.convertirPremiereLettreEnMajuscule(nomAttributSelectionne);
    nomAttributLogiqueATrier = UtilitaireString.convertirPremiereLettreEnMajuscule(nomAttributLogiqueATrier);

    ArrayList listeAttributLogiqueATrier = (ArrayList) repertoireAttributLogiqueATrier.get(nomAttributSelectionne);

    if (listeAttributLogiqueATrier == null) {
      listeAttributLogiqueATrier = new ArrayList();
      repertoireAttributLogiqueATrier.put(nomAttributSelectionne,
          listeAttributLogiqueATrier);
    }

    ListeTriAttribut listeTriAttribut = new ListeTriAttribut(nomAttributSelectionne,
        nomAttributLogiqueATrier, isAscendant);

    listeAttributLogiqueATrier.add(listeTriAttribut);
  }

  /**
   * Ajouter un tri complexe (par exemple, un appel de fonction, un decode,etc.)
   * pour remplacer un nom d'attribut comme tri. Cette méthode peut être
   * utilisée en combinaison avec les attributs logique pour combiner
   * plusieurs fonctions de tri.
   * @param nomAttribut Nom de l'attribut qui est utilisé pour identifier le
   * tri (correspond à une propriété de l'objet de transfert).
   * @param sql fragment de code SQL qui est utilisé comme valeur du tri
   * lorsque le tri en cours est celui de l'attribut spécifié
   */
  public void ajouterTriComplexe(String nomAttribut, String sql) {
    this.repertoireTriComplexe.put(
        UtilitaireString.convertirPremiereLettreEnMajuscule(nomAttribut), sql);
  }


  public String getTriComplexe(String nomAttribut) {
    if (this.repertoireTriComplexe != null && this.repertoireTriComplexe.size() > 0) {
      return (String) this.repertoireTriComplexe.get(nomAttribut);
    }else {
      return null;
    }
  }

  /**
   * Spécifier le tri initial pour une liste. Vous devez spécifier ce tri
   * initial dans le service d'affaires avant d'appliquer la liste navigation.
   * @param nomAttribut le nom de l'attribut qui doit être trié par défault.
   * @param isAscendant l'ordre de trie
   */
  public void ajouterTriInitial(String nomAttribut, boolean isAscendant) {
    if (getTriAttribut() == null) {
      setTriAttribut(nomAttribut);
      setAscendant(isAscendant);
    }
  }

  public String getTriAttribut() {
    return triAttribut;
  }

  public void setOrdreTri(String ordreTri) {
    this.ascendant = !TRI_DESCENDANT.equals(ordreTri);
  }

  public String getOrdreTri() {
    return ascendant ? TRI_ASCENDANT : TRI_DESCENDANT;
  }

  // --------------------------------------------
  // Méthodes utilisés par la liste de navigation
  // pour backward compatibility.
  // --------------------------------------------
  void setTriFixe(String triFixe) {
    this.triFixe = triFixe;
  }

  String getTriFixe() {
    return triFixe;
  }

  void initialiserTriAttribut() {
    this.triAttribut = null;
  }

  /**
   * Retourne la liste des attributs qui corresponde au tri logique lors de la
   * sélection d'un attribut.
   * @param attributSelectionne le nom de l'attribut dans la page JSP
   * @return la liste des attributs correspondant à un tri logique.
   */
  ArrayList getListeAttributTriLogique(String attributSelectionne) {
    return (ArrayList) repertoireAttributLogiqueATrier.get(attributSelectionne);
  }

  /**
   * Retourne le nombre d'attributs logique à trier pour l'attribut spécifié
   * pour tri.
   * @return le nombre d'attributs logique à trier pour l'attribut spécifié pour tri.
   */
  int getNbAttributLogiqueATrier() {
    int nb = 0;

    if ((getTriAttribut() != null) &&
        (repertoireAttributLogiqueATrier.get(getTriAttribut()) != null)) {
      ArrayList listeAttributLogiqueATrier = (ArrayList) repertoireAttributLogiqueATrier.get(getTriAttribut());

      nb = listeAttributLogiqueATrier.size();
    }

    return nb;
  }
}
