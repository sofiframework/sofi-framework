/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.exception.SOFIException;


/**
 * Objet de transfert représentant une liste de navigation.
 * <p>
 * La liste de cette objet ne correspond qu'à une seule page de la liste complète
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class ListeNavigation
extends org.sofiframework.objetstransfert.ObjetTransfert {
  // Compatiblité avec SOFI 1.7 et inférieure
  private static final long serialVersionUID = -1410824060138679627L;

  /**
   * Mot clé SQL pour un trie ascendant.
   */
  public static final String TRI_ASCENDANT = Tri.TRI_ASCENDANT;

  /**
   * Mot clé SQL pour un trie descendant.
   */
  public static final String TRI_DESCENDANT = Tri.TRI_DESCENDANT;

  /** Le filtre correspondant à la liste de navigation **/
  private Filtre filtre;

  /**
   * Le tri de la liste navigation.
   */
  private Tri tri = new Tri();

  /**
   * La liste de navigation
   */
  private List liste;

  /** Le nombre d'items dans la liste complète */
  private long nbListe;

  /** La page courante */
  private int pageCourante;

  /** Le maximum d'items par page */
  private Integer maxParPage;

  /** Le début de la page en cours */
  private int debutListe;

  /** Le no de page a traiter **/
  private int noPage;
  
  /** Spécifier le maximum de résultat total possible **/
  private int maxResultatTotal;

  /**
   * Le constructeur de la liste qui permet la navigation page par page.
   * <p>
   * Initialisation de la liste par défaut
   */
  public ListeNavigation() {
    liste = new ArrayList();

    if (maxParPage == null) {
      GestionParametreSysteme parametreSysteme = GestionParametreSysteme.getInstance();
      maxParPage = parametreSysteme.getInteger("nbListeParPage");
    }
  }

  /**
   * Constructeur pour utilisation en mode seul.
   * @param noPage le numéro de page demandé, 1, 2, 3, etc...
   * @param maxParPage le maximum par page
   */
  public ListeNavigation(int noPage, int maxParPage) {
    this.noPage = noPage;
    this.maxParPage = new Integer(maxParPage);
    this.liste = new ArrayList();
  }

  /**
   * Obtenir la liste.
   * <p>
   * @return ArrayList la liste
   */
  public List getListe() {
    return liste;
  }

  /**
   * Fixer la liste.
   * <p>
   * @param newListe la liste
   */
  public void setListe(List newListe) {
    liste = newListe;
  }

  /**
   * Ajouter un objet à la liste.
   * <p>
   * @param item l'objet à ajouter
   */
  public void ajouterListe(Object item) {
    liste.add(item);
  }

  /**
   * Obtenir le nombre d'objets dans la liste complète.
   * <p>
   * @return le nombre d'objets dans la liste complète
   */
  public long getNbListe() {
    return nbListe;
  }

  /**
   * Fixer le nombre d'objets dans la liste complète.
   * <p>
   * @param newNbListe le nombre d'objets dans la liste complète
   */
  public void setNbListe(long newNbListe) {
    nbListe = newNbListe;
  }

  /**
   * Obtenir le numéro de la page courante.
   * <p>
   * @return int le numéro de la page courante
   */
  public int getPageCourante() {
    this.pageCourante = (this.debutListe / this.maxParPage.intValue()) + 1;

    if (getNbPage() == 0) {
      this.pageCourante = 0;
    }

    return pageCourante;
  }

  /**
   * Obtenir le nombre de pages dans la liste.
   * <p>
   * @return int le nombre de pages
   */
  public int getNbPage() {
    long nbListe = getNbListe();
    long totalPages = (long) Math.ceil((double) nbListe / (double) this.maxParPage.intValue());

    return (int) totalPages;
  }

  /**
   * Obtenir la position du début de la liste.
   * <p>
   * @return int le début de la liste
   */
  public int getDebutListe() {
    return debutListe;
  }

  /**
   * Fixer la position du début de la liste.
   * <p>
   * @param newDebutListe la position du début de la liste
   */
  public void setDebutListe(int newDebutListe) {
    debutListe = newDebutListe;
  }

  /**
   * Obtenir la position de fin de la liste.
   * <p>
   * @return int la position de la fin de la liste
   */
  public int getFinListe() {
    int finListe = (debutListe + getMaxPourPageCourante()) - 1;

    if (finListe > getNbListe()) {
      return (int) getNbListe();
    }

    return finListe;
  }

  /**
   * Obtenir le maximum d'occurence pour la page courante.
   * <p>
   * @return int le maximum d'occurence pour la page courante.
   */
  public int getMaxPourPageCourante() {
    if (((int) nbListe - (getDebutListe() + getMaxParPage().intValue())) >= 0) {
      return getMaxParPage().intValue();
    } else {
      return (int) nbListe - getDebutListe();
    }
  }

  /**
   * Obtenir le maximum d'items possibles dans une page.
   * <p>
   * @return int le maximum d'items possibles dans une page
   */
  public Integer getMaxParPage() {
    return this.maxParPage;
  }

  /**
   * Fixer le maximum d'items possibles dans une page.
   * @param newMaxParPage le maximum d'items possibles dans une page
   */
  public void setMaxParPage(Integer newMaxParPage) {
    this.maxParPage = newMaxParPage;
  }

  /**
   * Spécifier s'il existe une page suivante à la page courante.
   * <p>
   * @return boolean vrai si il y a une page suivante sinon faux
   */
  public boolean getIsPageSuivante() {
    return getPageCourante() < getNbPage();
  }

  /**
   * Spécifier s'il existe une page précédente à la page courante.
   * <p>
   * @return boolean vrai si il y a une page précédente sinon faux
   */
  public boolean getIsPagePrecedente() {
    return getPageCourante() > 1;
  }

  /**
   * Retourne la position de la dernière page
   * <p>
   * @return int position de la dernière page
   */
  public int getDernierePage() {
    int nbPage = (int) nbListe / maxParPage.intValue();

    if ((nbListe % maxParPage.intValue()) > 0) {
      return ((nbPage - 1) * maxParPage.intValue()) + 1;
    } else {
      return ((nbPage - 2) * maxParPage.intValue()) + 1;
    }
  }

  /**
   * Retourne la position de la permiere page
   * <p>
   * @return int position de la premiere page
   */
  public int getPremierePage() {
    return 1 + maxParPage.intValue();
  }

  /**
   * Obtenir la position de la navigation courante en format texte.
   * <p>
   * @return String la position de la navigation courante
   */
  public String getPositionTexte() {
    StringBuffer positionTexte = new StringBuffer();
    positionTexte.append("page ");
    positionTexte.append(getPageCourante());
    positionTexte.append(" de ");
    positionTexte.append(getNbPage());

    return positionTexte.toString();
  }

  /**
   * Spécifie l'ordre de tri
   * @param newOrdreTri
   */
  public void setOrdreTri(String newOrdreTri) {
    this.tri.setOrdreTri(newOrdreTri);
  }

  /**
   * Retourne l'ordre de tri
   * @return l'ordre de tri.
   */
  public String getOrdreTri() {
    try {
      return this.tri.getOrdreTri();
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * Retourne le nom d'attribut à trier.
   * @return le nom d'attribut à trier.
   */
  public String getTriAttribut() {
    try {
      return this.tri.getTriAttribut();
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * Fixer le nom attribut à trier.
   * @param newTriAttribut
   */
  public void setTriAttribut(String newTriAttribut) {
    this.tri.setTriAttribut(newTriAttribut);
  }

  /**
   * Annuler le tri par défaut.
   */
  public void initialiserTriAttribut() {
    this.tri.initialiserTriAttribut();
  }

  /**
   * Retourne le numéro de page courante.
   * @return le numéro de page courante.
   */
  public int getNoPage() {
    return noPage;
  }

  /**
   * Fixer le numéro de page courante.
   * @param newNoPage le nouveau numéro de la page en cours
   */
  public void setNoPage(int newNoPage) {
    noPage = newNoPage;
  }

  /**
   * Initialise la liste de navigation à vide.
   */
  public void initialiserListe() {
    this.liste = new ArrayList();
  }

  /**
   * Retourne la position de début de la liste.
   * La première page correspond à 0, la deuxième au maximum d'occurence
   * dans une page, etc...
   * @return la position de début de la liste.
   */
  public int getPositionListe() {
    if (noPage == 1) {
      debutListe = 0;
    } else {
      debutListe = (noPage - 1) * getMaxParPage().intValue();
    }

    return debutListe;
  }

  /**
   * Permet d'ajouter des attributs logiques avec son ordre de tri pour
   * un attribut sélectionné pour tri.
   * @param nomAttributSelectionne le nom de l'attribut dans la page JSP
   * @param nomAttributLogiqueATrier le nom de la proriété de l'objet d'affaire qui doit être trié
   * @param isAscendant l'ordre de trie
   */
  public void ajouterAttributLogiqueAtrier(String nomAttributSelectionne,
      String nomAttributLogiqueATrier, boolean isAscendant) {
    this.tri.ajouterAttributLogiqueAtrier(nomAttributSelectionne,
        nomAttributLogiqueATrier, isAscendant);
  }

  /**
   * Retourne la liste des attributs qui corresponde au tri logique lors de la
   * sélection d'un attribut.
   * @param attributSelectionne le nom de l'attribut dans la page JSP
   * @return la liste des attributs correspondant à un tri logique.
   */
  public ArrayList getListeAttributTriLogique(String attributSelectionne) {
    return this.tri.getListeAttributTriLogique(attributSelectionne);
  }

  /**
   * Retourne le nombre d'attributs logique à trier pour l'attribut spécifié
   * pour tri.
   * @return le nombre d'attributs logique à trier pour l'attribut spécifié pour tri.
   */
  public int getNbAttributLogiqueATrier() {
    return this.tri.getNbAttributLogiqueATrier();
  }

  /**
   * Spécifier le tri initial pour une liste. Vous devez spécifier ce tri
   * initial dans le service d'affaires avant d'appliquer la liste navigation.
   * @param nomAttribut le nom de l'attribut qui doit être trié par défault.
   * @param isAscendant l'ordre de trie
   */
  public void ajouterTriInitial(String nomAttribut, boolean isAscendant) {
    this.tri.ajouterTriInitial(nomAttribut, isAscendant);
  }

  /**
   * Retourne l'objet filtre associé a la liste de navigation.
   * @return  l'objet filtre associé a la liste de navigation.
   */
  public ObjetFiltre getObjetFiltre() {
    return (ObjetFiltre) filtre;
  }

  /**
   * Fixer l'objet filtre associé a la liste de navigation.
   * @param objetFiltre
   * @exception org.sofiframework.composantweb.liste.ListeNavigationException
   */
  public void setObjetFiltre(ObjetFiltre objetFiltre)
      throws ListeNavigationException {
    if (objetFiltre == null) {
      throw new ListeNavigationException(
          "ObjetFiltre ne doit pas être null, veuillez fixer un objet filtre non null. Si vous utiliser un formulaire pour filtrage, veuillez utiliser setObjetFiltre(BaseForm formulaire, Class typeObjetFiltre)");
    }

    this.filtre = objetFiltre;
  }

  /**
   * Fixer l'objet filtre associé a la liste de navigation.
   * <p>
   * Si l'objet filtre est à null, alors il sera instancié automatiquement
   * avec l'aide du type spécifié.
   * @param objetFiltre l'objet de transfert de type ObjetFiltre
   * @param typeObjetFiltre le type de l'objet filtre.
   * @since SOFI 1.8.2
   */
  public void setObjetFiltre(Object objetFiltre, Class typeObjetFiltre)
      throws ListeNavigationException {
    if (objetFiltre == null) {
      try {
        objetFiltre = typeObjetFiltre.newInstance();
      } catch (InstantiationException e) {
        throw new SOFIException(
            "ObjetFiltre ne doit pas être null, veuillez initialiser d'abord votre formulaire");
      } catch (IllegalAccessException e) {
        throw new SOFIException(
            "ObjetFiltre ne doit pas être null, veuillez initialiser d'abord votre formulaire");
      }
    }

    this.filtre = (ObjetFiltre) objetFiltre;
  }

  /**
   * Retourne le tri fixe de la liste navigation.
   * <p>
   * Exemple : TRI_1 DESC, TRI_2 ASC
   * @return le tri fixe de la liste navigation.
   */
  public String getTriFixe() {
    return this.tri.getTriFixe();
  }

  /**
   * Fixer le tri fixe de la liste navigation.
   * <p>
   * Exemple : TRI_1 DESC, TRI_2 ASC
   * @param triFixe le tri fixe de la liste navigation.
   */
  public void setTriFixe(String triFixe) {
    this.tri.setTriFixe(triFixe);
  }

  /**
   * Fixer le tri de la liste navigation
   * @param tri
   */
  public void setTri(Tri tri) {
    this.tri = tri;
  }

  /**
   * Obtenir le tri de la liste navigation.
   * @return tri
   */
  public Tri getTri() {
    return tri;
  }

  /**
   * Retourne le filtre associé a la liste de navigation.
   * @return  le filtre associé a la liste de navigation.
   */
  public Filtre getFiltre() {
    return filtre;
  }

  /**
   * Fixer le filtre associé a la liste de navigation.
   */
  public void setFiltre(Filtre filtre) {
    this.filtre = filtre;
  }

  /**
   * Détermine si la liste de navigation est vide (est null
   * ou contient aucun élément).
   * @return true liste vide et false liste contient un élément
   */
  public boolean isVide() {
    return ListeNavigation.isVide(this);
  }

  /**
   * Détermine si la liste de navigation est vide (est null ou
   * contient aucun élément).
   * @param liste
   * @return true liste vide et false liste contient un élément
   */
  public static boolean isVide(ListeNavigation liste) {
    return (liste == null)
        || (liste.getListe() == null)
        || (liste.getListe().size() == 0);
  }

  /**
   * Ajouter un tri complexe (par exemple, un appel de fonction, un decode,etc.)
   * pour remplacer un nom d'attribut comme tri. Cette méthode peut être
   * utilisée en combinaison avec les attributs logique pour combiner
   * plusieurs fonctions de tri.
   * @param attributATrier Nom de l'attribut qui est utilisé pour identifier le
   * tri (correspond à une propriété de l'objet de transfert).
   * @param sqlTriComplexe fragment de code SQL qui est utilisé comme valeur du tri
   * lorsque le tri en cours est celui de l'attribut spécifié
   */
  public void ajouterTriComplexe(String attributATrier, String sqlTriComplexe) {
    this.tri.ajouterTriComplexe(attributATrier, sqlTriComplexe);
  }

  public int getMaxResultatTotal() {
    return maxResultatTotal;
  }

  public void setMaxResultatTotal(int maxResultatTotal) {
    this.maxResultatTotal = maxResultatTotal;
  }
}
