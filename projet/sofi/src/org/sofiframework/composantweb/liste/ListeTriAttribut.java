/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.io.Serializable;


/**
 * Classe utilitaire permettant de spécifier des tris supplémentaires.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class ListeTriAttribut implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -1474729656131716755L;

  /**
   * L'attribut à trier.
   */
  private String triAttribut;

  /**
   * Le trie suplémentaire qui doit être appliqué à un requête SQL.
   */
  private String triAttributSupplementaire;

  /**
   * L'ordre de trie de la requête.
   */
  private boolean ascendant;

  /**
   * Contructeur d'une liste d'attributs à trier.
   * @param triAttribut l'attribut
   * @param triAttributSupplementaire l'attribut supplémentaire à trier
   * @param isAscendant l'ordre de trie
   */
  public ListeTriAttribut(String triAttribut, String triAttributSupplementaire,
      boolean isAscendant) {
    this.triAttribut = triAttribut;
    this.triAttributSupplementaire = triAttributSupplementaire;
    setAscendant(isAscendant);
  }

  /**
   * Retourne le nom d'attribut correspondant au tri sélectionné
   * @return le nom d'attribut correspondant au tri sélectionné.
   */
  public String getTriAttribut() {
    return triAttribut;
  }

  /**
   * Spécifie le nom d'attribut correspondant au tri sélectionné.
   * @param triAttribut le nom d'attribut correspondant au tri sélectionné.
   */
  public void setTriAttribut(String triAttribut) {
    this.triAttribut = triAttribut;
  }

  /**
   * Retourne le nom d'attribut correspondant à un tri supplémentaire.
   * @return le nom d'attribut correspondant à un tri supplémentaire.
   */
  public String getTriAttributSupplementaire() {
    return triAttributSupplementaire;
  }

  /**
   * Fixer le nom attribut correspondant à un tri supplémentaire.
   * @param triAttributSupplementaire le nom attribut correspondant à un tri supplémentaire
   */
  public void setTriAttributSupplementaire(String triAttributSupplementaire) {
    this.triAttributSupplementaire = triAttributSupplementaire;
  }

  /**
   * Retourne l'ordre du tri de l'attribut supplémentaire spécifié.
   * @return l'ordre du tri de l'attribut supplémentaire spécifié.
   */
  public String getTriAttributOrdre() {
    if (isAscendant()) {
      return Tri.TRI_ASCENDANT;
    } else {
      return Tri.TRI_DESCENDANT;
    }
  }

  /**
   * Retourne si le tri est ascendant.
   * @return true si le tri est ascendant.
   */
  public boolean isAscendant() {
    return ascendant;
  }

  /**
   * Spécifie si le tri est ascendant.
   * @param ascendant si le tri est ascendant
   */
  public void setAscendant(boolean ascendant) {
    this.ascendant = ascendant;
  }
}
