/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

/**
 * Contient l'information sur un attribut à filter lors d'une recherche.
 * @author Guillaume Poirier
 */
public class AttributFiltre {
  /** le nom de l'attribut */
  private String nom;

  /** la valeur de l'attribut */
  private Object valeur;

  /** le mode de comparaison pour les chaines de caractères */
  private MatchMode matchMode = MatchMode.EXACT;

  /** l'opérateur de comparaison */
  private Operateur operateur = Operateur.EGALE;

  public AttributFiltre(String nom, Object valeur) {
    this.nom = nom;
    this.valeur = valeur;
  }

  public AttributFiltre(String nom, Object valeur, MatchMode matchMode) {
    this.nom = nom;
    this.valeur = valeur;
    this.matchMode = matchMode;
  }

  public AttributFiltre(String nom, Object valeur, Operateur operateur) {
    this.nom = nom;
    this.valeur = valeur;
    this.operateur = operateur;
  }

  /**
   * Retourne le nom de l'attribut.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Retourne la valeur de l'attribut.
   */
  public Object getValeur() {
    return valeur;
  }

  /**
   * Retourne le mode de comparaison pour les chaines de caractères.
   */
  public MatchMode getMatchMode() {
    return matchMode;
  }

  /**
   * Retourne l'opérateur de comparaison
   */
  public Operateur getOperateur() {
    return operateur;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(nom).append(" ");
    if (operateur == Operateur.EGALE && valeur instanceof String) {
      sb.append("ILIKE ").append(
          toString(matchMode.toMatchString((String) valeur)));
    } else {
      sb.append(operateur.toString()).append(" ").append(toString(valeur));
    }
    return sb.toString();
  }

  /**
   * Retourne une valeur affichable pour l'objet
   */
  private String toString(Object valeur) {
    if (valeur instanceof String) {
      return "'" + valeur + "'";
    } else if (valeur == null) {
      return "NULL";
    } else {
      return valeur.toString();
    }
  }
}
