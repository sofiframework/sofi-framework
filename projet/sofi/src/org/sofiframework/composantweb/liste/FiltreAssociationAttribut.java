/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe technique permettant d'emmagasiner les informations
 * d'une association entre 2 attributs d'un filtre.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FiltreAssociationAttribut extends ObjetTransfert {

  private static final long serialVersionUID = 35553287094160151L;

  private String attributSource1;
  private String attributSource2;
  private String attributDestination;

  /**
   * Constructeur par défaut.
   * @param attributSource1 le premier attribut à associer.
   * @param attributSource2 le second attribut à associer.
   * @param attributDestination le nom d'attribut de destination.
   * @throws org.sofiframework.exception.SOFIException exception runtime de SOFI
   */
  protected FiltreAssociationAttribut(String attributSource1,
      String attributSource2, String attributDestination)
          throws SOFIException {
    this.attributSource1 = attributSource1;
    this.attributSource2 = attributSource2;
    this.attributDestination = attributDestination;

    if (!((this.attributSource1 != null) && (this.attributSource2 != null) &&
        (this.attributDestination != null))) {
      throw new SOFIException(
          "FiltreAssociationAttribut: Vous devez spécifier les attributs de source 1, source 2 et de destination!");
    }
  }

  /**
   * Retourne le premier attribut source d'association.
   * @return le premier attribut source d'association.
   */
  public String getAttributSource1() {
    return attributSource1;
  }

  /**
   * Fixer le premier attribut source d'association.
   * @param attributSource1 le premier attribut source d'associtation.
   */
  public void setAttributSource1(String attributSource1) {
    this.attributSource1 = attributSource1;
  }

  /**
   * Retourne le second attribut source d'association.
   * @return le second attribut source d'association.
   */
  public String getAttributSource2() {
    return attributSource2;
  }

  /**
   * Fixer le second attribut source d'association.
   * @param attributSource1 le second attribut source d'association.
   */
  public void setAttributSource2(String attributSource2) {
    this.attributSource2 = attributSource2;
  }

  /**
   * Retourne le nom d'attribut de destination à associer.
   * @return le nom d'attribut de destination à associer.
   */
  public String getAttributDestination() {
    return attributDestination;
  }

  /**
   * Fixer le nom d'attribut de destination à associer.
   * @param attributDestination le nom d'attribut de destination à associer.
   */
  public void setAttributDestination(String attributDestination) {
    this.attributDestination = attributDestination;
  }
}
