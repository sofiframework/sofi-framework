/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.io.Serializable;

/**
 * Contient l'information sur un attribut à trier.
 * 
 * @author Guillaume Poirier
 */
public class AttributTri implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 3346559933359419639L;

  /** le nom de l'attribut */
  private String nom;

  /** indique si le tri doit être ascendant ou descendant */
  private boolean ascendant;

  public AttributTri(String nom, boolean asc) {
    this.nom = nom;
    this.ascendant = asc;
  }

  /**
   * Retourne le nom de l'attribut.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Retourne vrai si le tri doit être ascendant, faux s'il est descendant.
   */
  public boolean isAscendant() {
    return ascendant;
  }
}
