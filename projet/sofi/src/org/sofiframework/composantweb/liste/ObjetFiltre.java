/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Objet de transfert utilisé afin de représenté un filtre.
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class ObjetFiltre extends ObjetTransfert implements Filtre {
  // Compatiblité avec SOFI 1.7 et 1.8
  private static final long serialVersionUID = -425649972564162205L;
  private HashMap listeAssociationAttribut = new HashMap();
  private HashMap listeIntervalleAttribut = new HashMap();
  private HashMap listeOperateurAttribut = new HashMap();
  private HashMap listeTousPourBooleanNonSelectionne = new HashMap();
  private HashMap listeAttributAExclure = new HashMap();
  private HashSet<String> listeExclureAttributRechercheApres = new HashSet<String>();
  private HashSet<String> listeExclureAttributRechercheAvantApres = new HashSet<String>();
  private HashSet<String> listeAttributRechercheAvantApres = new HashSet<String>();
  private HashSet<String> listeAttributRechercheApres = new HashSet<String>();
  private HashSet<String> listeAttributRechercheValeurNull = new HashSet<String>();
  private boolean rechercheAvantEtApresAttribut = false;
  private boolean rechercheApresAttribut = false;
  private boolean dejaGenere = false;
  private Map listeValeurs = null;

  /**
   * Permet d'ajouter une association de deux attribut qui sont fusionné pour
   * appliquer un filtre.
   * 
   * @param attributSource1
   *          l'attribut source 1
   * @param attributSource2
   *          l'attribut source 2
   * @param attributDestination
   *          l'attribut de destination pour filtrage.
   */
  public void ajouterAssociation(String attributSource1,
      String attributSource2, String attributDestination) {
    FiltreAssociationAttribut associationAttribut = new FiltreAssociationAttribut(
        attributSource1, attributSource2, attributDestination);
    listeAssociationAttribut.put(attributDestination, associationAttribut);
  }

  /**
   * Permet d'ajouter un filtre ayant un intervalle ayant un début et une fin.
   * 
   * @param attributDebut
   *          l'attribut correspondant au début de l'intervalle.
   * @param attributFin
   *          l'attribut correspondant à la fin de l'intervalle.
   * @param attributDestination
   *          l'attribut de destination pour filtrage.
   */
  public void ajouterIntervalle(String attributDebut, String attributFin,
      String attributDestination) {
    FiltreIntervalleAttribut intervalleAttribut = new FiltreIntervalleAttribut(
        attributDebut, attributFin, attributDestination);
    listeIntervalleAttribut.put(attributDestination, intervalleAttribut);
  }

  /**
   * Permet d'ajouter un opérateur sur un attribut
   * 
   * @param nomAttributFiltre
   *          le nom de l'attribut du filtre
   * @param nomAttributEntite
   *          le nom de l'attribut de l'entite
   * @param operateur
   *          <code>org.sofiframework.composantweb.liste.Operateur</code>
   *          l'operateur a appliquer
   * @since 3.1
   */
  public void ajouterOperateur(String nomAttributFiltre,
      String nomAttributEntite, Operateur operateur) {
    FiltreOperateurAttribut operateurAttribut = new FiltreOperateurAttribut(
        nomAttributFiltre, nomAttributEntite, operateur);
    listeOperateurAttribut.put(nomAttributEntite, operateurAttribut);
  }

  /**
   * Permet d'ajouter un attribut du filtre qui doit géré les boolean non
   * sélectionné comme un non critère de sélection du filtre.
   * 
   * @param nomAttribut
   *          le nom d'attribut du filtre
   */
  public void ajouterAttributTousPourNonSelectionne(String nomAttribut) {
    listeTousPourBooleanNonSelectionne.put(nomAttribut, Boolean.TRUE);
  }

  /**
   * Permet d'ajouter un attribut dont on désire que la recherche s'applique
   * seulement pour les valeurs à null
   * 
   * @param nomAttribut
   * @since 3.1
   */
  public void ajouterAttributRechercheValeurNull(String nomAttribut) {
    listeAttributRechercheValeurNull.add(nomAttribut);
  }


  /**
   * Permet de supprimer un attribut dont on ne désire plus que la recherche s'applique
   * seulement pour les valeurs à null
   * 
   * @param nomAttribut
   * @since 3.1
   */
  public void supprimerAttributRechercheValeurNull(String nomAttribut) {
    listeAttributRechercheValeurNull.remove(nomAttribut);
  }

  /**
   * Est-ce que l'attribut à une valeur à null.
   * 
   * @param nomAttribut
   *          l'attribut à une valeur à null.
   * @return true si l'attribut à une valeur à null.
   * @since 3.1
   */
  public boolean isAttributRechercheValeurNull(String nomAttribut) {
    return listeAttributRechercheValeurNull.contains(nomAttribut);
  }

  /**
   * Initialiser la liste des attributs dont on désire faire une rechercher à null.
   * @since 3.1
   */
  public void initialiserListeAttributRechercheValeurNull() {
    this.listeAttributRechercheValeurNull = new HashSet<String>();
  }

  /**
   * Est-ce que l'attribut exclue la non sélectionné lors du traitement du
   * filtre?
   * 
   * @param nomAttribut
   * @return true si l'attribut exclue la non sélectionné lors du traitement du
   *         filtre.
   */
  public boolean isAttributBooleanExcluantNonSelectionne(String nomAttribut) {
    return listeTousPourBooleanNonSelectionne.containsKey(nomAttribut);
  }

  /**
   * Retourne la liste des intervalles entre attributs du filtre.
   * 
   * @return la liste des intervalles entre attributs du filtre.
   */
  public HashMap getListeIntervalles() {
    return listeIntervalleAttribut;
  }

  /**
   * Est-ce que l'attribut correspond à un association d'attributs?
   * <p>
   * Par exemple un attribut >= associer avec un champ
   * 
   * @param nomAttribut
   *          l'attribut à tester si existant.
   * @return true si l'attribut est associé avec d'autre(s) attribut(s).
   */
  public boolean isAttributsAssocie(String nomAttribut) {
    return listeAssociationAttribut.containsKey(nomAttribut);
  }

  /**
   * Est-ce que l'attribut est un attribut à exclure de la population du filtre?
   * <p>
   * 
   * @param nomAttribut
   *          l'attribut à tester si existant.
   * @return true si l'attribut est associé avec d'autre(s) attribut(s).
   */
  public boolean isAttributAExclure(String nomAttribut) {
    return listeAttributAExclure.containsKey(nomAttribut);
  }

  /**
   * Est-ce que la recherche doit être appliqué avant et après la valeur de
   * l'attribut?
   * <p>
   * Exemple: %valeur1% sera appliqué.
   * 
   * @return true si la recherche doit être appliqué avant et après la valeur de
   *         l'attribut
   */
  public boolean isRechercheAvantEtApresAttribut() {
    return rechercheAvantEtApresAttribut;
  }

  /**
   * Fixer true si la recherche doit être appliqué avant et après la valeur de
   * l'attribut.
   * <p>
   * Un %valeur1% sera appliqué.
   * 
   * @param recherche
   *          true si la recherche doit être appliqué avant et après la valeur
   *          de l'attribut.
   */
  public void setRechercheAvantEtApresAttribut(boolean recherche) {
    rechercheAvantEtApresAttribut = recherche;
  }

  /**
   * Est-ce que la recherche doit être appliqué seulement après la valeur de
   * l'attribut?
   * <p>
   * Exemple: valeur1% sera appliqué.
   * 
   * @return true si a recherche doit être appliqué seulement après la valeur de
   *         l'attribut
   */
  public boolean isRechercheApresAttribut() {
    return rechercheApresAttribut;
  }

  /**
   * Fixer true si la recherche doit être appliqué seulement après la valeur de
   * l'attribut.
   * <p>
   * Un valeur1% sera appliqué.
   * 
   * @param recherche
   *          true si la recherche doit être appliqué après la valeur de
   *          l'attribut.
   */
  public void setRechercheApresAttribut(boolean recherche) {
    rechercheApresAttribut = recherche;
  }

  /**
   * Retourne la liste des attribut à exclure d'un possible filtrage
   * 
   * @return la liste des attribut à exclure d'un possible filtrage
   */
  public HashMap getAttributsAExclure() {
    if (!listeAttributAExclure.containsKey("rechercheAvantEtApresAttribut")) {
      listeAttributAExclure.put("rechercheAvantEtApresAttribut", Boolean.TRUE);
      listeAttributAExclure.put("rechercheApresAttribut", Boolean.TRUE);
      listeAttributAExclure.put("attributsAExclure", Boolean.TRUE);
      listeAttributAExclure.put("listeAssocierAttribut", Boolean.TRUE);
      listeAttributAExclure.put("erreur", Boolean.TRUE);
      listeAttributAExclure.put("cle", Boolean.TRUE);
      listeAttributAExclure.put("class", Boolean.TRUE);
      listeAttributAExclure.put("dejaGenere", Boolean.TRUE);
      listeAttributAExclure.put("modifie", Boolean.TRUE);
      listeAttributAExclure.put("conditionDejaTraite", Boolean.TRUE);
    }

    listeAttributAExclure.put("attributsModifiesVide", Boolean.TRUE);
    listeAttributAExclure.put("attributsModifies", Boolean.TRUE);

    return this.listeAttributAExclure;
  }

  /**
   * Permet d'ajouter une attribut à exclure d'un possible filtrage
   * 
   * @param attributAExclure
   *          attribut à exclure
   */
  public void ajouterAttributAExclure(String attributAExclure) {
    this.listeAttributAExclure.put(attributAExclure, Boolean.TRUE);
  }

  /**
   * Permet de supprimer un attribut qui est exclus de la génération de la
   * condition SQL avec l'objet filtre.
   * 
   * @param nomAttribut
   *          l'attribut à inclure dans le traitement du filtre.
   */
  public void supprimerAttributAExclure(String nomAttribut) {
    if (isAttributAExclure(nomAttribut)) {
      this.listeAttributAExclure.remove(nomAttribut);
    }
  }

  /**
   * Est-ce que la liste de valeur disponible pour filtrage à déjà été généré.
   * 
   * @return true si la liste de valeur disponible pour filtrage à déjà été
   *         généré.
   */
  public boolean isDejaGenere() {
    return dejaGenere;
  }

  /**
   * Fixer true si la liste de valeur disponible pour filtrage à déjà été
   * généré.
   * 
   * @param dejaGenere
   *          true si la liste de valeur disponible pour filtrage à déjà été
   *          généré.
   */
  public void setDejaGenere(boolean dejaGenere) {
    this.dejaGenere = dejaGenere;
  }

  /**
   * Retourne la liste des attributs associés
   * 
   * @return la liste des attributs associés
   */
  public HashMap getListeAssociationAttribut() {
    return this.listeAssociationAttribut;
  }

  /**
   * Retourne la liste des valeurs de l'objet filtre
   * 
   * @return la liste des valeurs de l'objet filtre
   */
  public Map getListeValeurs() {
    return this.listeValeurs;
  }

  /**
   * Fixer la liste des valeurs de l'objet filtre.
   * 
   * @param listeValeurs
   *          la liste des valeurs de l'objet filtre.
   */
  public void setListeValeurs(Map listeValeurs) {
    this.listeValeurs = listeValeurs;
  }

  /**
   * Ajouter un ou des attributs que la recherche automatique après la valeur ne
   * doit être faite.
   * 
   * @param listeNomAttribut
   *          une liste d'attribut que la recherche après la valeur ne doit être
   *          faite.
   */
  public void ajouterExclureRechercheApresValeur(String[] listeNomAttribut) {
    this.setRechercheApresAttribut(true);

    if (listeNomAttribut != null) {
      for (int i = 0; i < listeNomAttribut.length; i++) {
        this.listeExclureAttributRechercheApres.add(listeNomAttribut[i]);
      }
    }
  }

  /**
   * Ajouter un ou des attributs que la recherche automatique avant etaprès la
   * valeur ne doit être faite.
   * 
   * @param listeNomAttribut
   *          une liste d'attribut que la recherche avant et après la valeur ne
   *          doit être faite.
   */
  public void ajouterExclureRechercheAvantApresValeur(String[] listeNomAttribut) {
    this.setRechercheAvantEtApresAttribut(true);

    if (listeNomAttribut != null) {
      for (int i = 0; i < listeNomAttribut.length; i++) {
        this.listeExclureAttributRechercheAvantApres.add(listeNomAttribut[i]);
      }
    }
  }

  /**
   * Retourne la liste des attributs à exclure de la recherche après valeur.
   * 
   * @return la liste des attributs à exclure de la recherche après valeur.
   */
  public HashSet getListeExclureAttributRechercheApres() {
    return listeExclureAttributRechercheApres;
  }

  /**
   * Retourne la liste des attributs à exclure de la recherche après et avant
   * valeur.
   * 
   * @return la liste des attributs à exclure de la recherche après et avant
   *         valeur.
   */
  public HashSet getListeExclureAttributRechercheAvantApres() {
    return listeExclureAttributRechercheAvantApres;
  }

  /**
   * Ajouter un ou des attributs que la recherche automatique après la valeur
   * soit traité.
   * 
   * @param listeNomAttribut
   *          une liste d'attribut que la recherche après la valeur soit traité.
   * @since SOFI 2.0.3
   */
  public void ajouterRechercheApresValeur(String[] listeNomAttribut) {
    setRechercheApresAttribut(false);

    if (listeNomAttribut != null) {
      for (int i = 0; i < listeNomAttribut.length; i++) {
        this.listeAttributRechercheApres.add(listeNomAttribut[i]);
      }
    }
  }

  /**
   * Ajouter un ou des attributs que la recherche automatique avant et après la
   * valeur soit traité.
   * 
   * @param listeNomAttribut
   *          une liste d'attribut que la recherche avant et après la valeur
   *          soit traité.
   * @since SOFI 2.0.3
   */
  public void ajouterRechercheAvantApresValeur(String[] listeNomAttribut) {
    setRechercheAvantEtApresAttribut(false);

    if (listeNomAttribut != null) {
      for (int i = 0; i < listeNomAttribut.length; i++) {
        this.listeAttributRechercheAvantApres.add(listeNomAttribut[i]);
      }
    }
  }

  /**
   * Retourne la liste des attributs à exclure de la recherche après valeur.
   * 
   * @return la liste des attributs à exclure de la recherche après valeur.
   * @since SOFI 2.0.3
   */
  public HashSet getListeAttributRechercheApres() {
    return listeAttributRechercheApres;
  }

  /**
   * Retourne la liste des attributs à exclure de la recherche après et avant
   * valeur.
   * 
   * @return la liste des attributs à exclure de la recherche après et avant
   *         valeur.
   * @since SOFI 2.0.3
   */
  public HashSet getListeAttributRechercheAvantApres() {
    return listeAttributRechercheAvantApres;
  }

  /**
   * Est-ce que l'atttribut est à exclure du traitement de recherche automatique
   * après valeur.
   * 
   * @return true si l'atttribut est à exclure du traitement de recherche
   *         automatique après valeur.
   * @param nomAttribut
   *          le nom d'attribut à verifier.
   */
  public boolean isAttributAExclureRechercheApresValeur(String nomAttribut) {
    return getListeExclureAttributRechercheApres().contains(nomAttribut);
  }

  /**
   * Est-ce que l'atttribut est à exclure du traitement de recherche automatique
   * avant et après valeur.
   * 
   * @return true si l'atttribut est à exclure du traitement de recherche
   *         automatique avant et après valeur.
   * @param nomAttribut
   *          le nom d'attribut à verifier.
   */
  public boolean isAttributAExclureRechercheAvantApresValeur(String nomAttribut) {
    return getListeExclureAttributRechercheAvantApres().contains(nomAttribut);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.composantweb.liste.Filtre#getValeurs()
   */
  @Override
  public Collection produireValeurs() {
    // La configuration est refait à chaque appel de getValeurs()
    // afin de bien supporter les changements faits directement
    // aux collections privées de configuration.
    ConfigFiltre config = new ConfigFiltre(ObjetFiltre.class);

    config.exclureAttributs(listeAttributAExclure.keySet());

    // Traitement lorsque objet filtre a mode par défaut spécifique de
    // recherche.
    if (rechercheAvantEtApresAttribut) {
      config.setDefautMatchMode(MatchMode.ANYWHERE);
      config.setMatchMode(listeExclureAttributRechercheAvantApres,
          MatchMode.EXACT);
    } else if (rechercheApresAttribut) {
      config.setDefautMatchMode(MatchMode.START);
      config.setMatchMode(listeExclureAttributRechercheApres, MatchMode.EXACT);
    }

    // Traitement lorsque attribut avec recherche après spécifique.
    if (getListeAttributRechercheApres() != null
        && getListeAttributRechercheApres().size() > 0) {
      config.setDefautMatchMode(MatchMode.EXACT);
      config.setMatchMode(listeAttributRechercheApres, MatchMode.START);
    }

    // Traitement lorsque attribut avec recherche avant et après spécifique.
    if (getListeAttributRechercheAvantApres() != null
        && getListeAttributRechercheAvantApres().size() > 0) {
      config.setDefautMatchMode(MatchMode.EXACT);
      config.setMatchMode(listeAttributRechercheAvantApres, MatchMode.ANYWHERE);
    }

    for (Iterator it = listeIntervalleAttribut.values().iterator(); it
        .hasNext();) {
      FiltreIntervalleAttribut attr = (FiltreIntervalleAttribut) it.next();
      config.ajouterIntervalle(attr.getAttributDebut(), attr.getAttributFin(),
          attr.getAttributDestination());
    }

    for (Iterator it = listeAssociationAttribut.values().iterator(); it
        .hasNext();) {
      FiltreAssociationAttribut attr = (FiltreAssociationAttribut) it.next();
      config.ajouterAlias(attr.getAttributSource2(),
          attr.getAttributDestination(), attr.getAttributSource1());
    }

    for (Iterator it = listeOperateurAttribut.values().iterator(); it.hasNext();) {
      FiltreOperateurAttribut attr = (FiltreOperateurAttribut) it.next();
      config.ajouterAlias(attr.getNomAttributFiltre(),
          attr.getNomAttributEntite(), attr.getOperateur());
    }

    for (Iterator it = listeAttributRechercheValeurNull.iterator(); it
        .hasNext();) {
      String attribut = (String) it.next();
      config.ajouterAlias(attribut, attribut, Operateur.EGALE);
    }

    return config.getValeurs(this);
  }

  @Override
  public String toString() {
    return "ObjectFiltre" + this.getClass().getName();
  }
}
