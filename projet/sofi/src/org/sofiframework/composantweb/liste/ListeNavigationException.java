/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import org.sofiframework.exception.SOFIException;


/**
 * Exception provenant de la liste navigation.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class ListeNavigationException extends SOFIException {
  /**
   * 
   */
  private static final long serialVersionUID = 5969276183801007691L;

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * @param message le message de base.
   */
  public ListeNavigationException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur du modèle
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public ListeNavigationException(String message, Throwable cause) {
    super(message, cause);
  }
}
