/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Contient la configuration d'un filtre. Les critères de recherches sont
 * ensuite obtenu à partir d'un JavaBean.
 * 
 * @author Guillaume Poirier
 */
public class ConfigFiltre implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 3807925558648345484L;

  private static final ConfigFiltre CONFIG_DEFAUT = new ConfigFiltre();

  /** Contient les attributs à exclure */
  private Set exclures = new HashSet();

  /** La classe parent à ignorer pour les propriétés JavaBeans */
  private Class stopClass;

  /**
   * Les aliases pour les champs, avec redéfinition optionel de l'opération de
   * comparaison.
   */
  private Map aliases = new HashMap();

  /** Mapping pour redéfinir le mode de comparaison pour les Strings */
  private Map matchModes = new HashMap();

  /** Le mode de comparaison par défaut pour les Strings */
  private MatchMode defautMatchMode = MatchMode.EXACT;

  /**
   * Définit si les primitives ayant la valeur par défaut doivent être inclues
   * dans les critères de recherches.
   */
  private boolean accepteDefautPrimitives = false;

  /**
   * Permet de modifier l'élément de configuration précédent pour chaque champ.
   */
  private Map defautsPrimitives = new HashMap();

  /**
   * La configuration des JavaBeans nested pour en faire des filtres.
   */
  private Map nestedConfigs = new HashMap();

  public ConfigFiltre() {
  }

  /**
   * Constructeur permettant de définir <code>stopClass</code>.
   * 
   * @param stopClass
   *          la classe parent à ignorer pour les propriétés JavaBeans
   */
  public ConfigFiltre(Class stopClass) {
    this.stopClass = stopClass;
  }

  /**
   * Retourne les critères à utiliser pour la recherche
   * 
   * @param cible
   *          le JavaBean contenant les valeurs des critères
   * @return les critères de recherche
   * @see Filtre
   */
  public Collection getValeurs(Object cible) {
    Class stopClass = this.stopClass;

    if (stopClass == null) {
      stopClass = (cible instanceof ObjetTransfert) ? ObjetTransfert.class
          : Object.class;
    }

    BeanInfo bi;

    try {
      bi = Introspector.getBeanInfo(cible.getClass(), stopClass);
    } catch (IntrospectionException e) {
      throw new SOFIException(e);
    }

    ArrayList valeurs = new ArrayList();
    PropertyDescriptor[] props = bi.getPropertyDescriptors();

    for (int i = 0, n = props.length; i < n; i++) {
      PropertyDescriptor p = props[i];
      String nom = p.getName();
      Object valeur;

      if ((p.getReadMethod() != null) && !exclures.contains(nom)
          && ((valeur = getValeur(cible, p)) != null)) {
        valeurs.add(getAttribut(nom, valeur));
      } else {
        valeur = UtilitaireObjet.getValeurAttribut(cible, nom);

        if (cible instanceof ObjetFiltre) {
          ObjetFiltre filtre = (ObjetFiltre) cible;

          // Si association alors ne pas ajouté dans les valeurs du filtre ici.
          if (filtre.getListeAssociationAttribut().get(nom) != null) {
            valeur = null;
          }

          // Si recherche des valeurs nulls seulement.
          if (filtre.isAttributRechercheValeurNull(nom)) {
            valeurs.add(getAttribut(nom, "NULL"));
          }
        }

        if ((valeur != null) && valeur.getClass().isArray()
            && !exclures.contains(nom)) {
          valeurs.add(getAttribut(nom, valeur));
        }
      }
    }

    for (Iterator it = nestedConfigs.entrySet().iterator(); it.hasNext();) {
      Map.Entry entry = (Map.Entry) it.next();
      String nom = (String) entry.getKey();
      ConfigFiltre nested = (ConfigFiltre) entry.getValue();
      Object valeur = UtilitaireObjet.getValeurAttribut(cible, nom);
      valeur = annuler(valeur);

      if (valeur != null) {
        FiltreWrapper filtre = new FiltreWrapper(valeur, nested);
        String nomAttribut;

        if (aliases.containsKey(nom)) {
          Alias alias = (Alias) aliases.get(nom);
          nomAttribut = alias.attributEntite;
        } else {
          nomAttribut = nom;
        }

        valeurs.add(new AttributFiltre(nomAttribut, filtre));
      }
    }

    for (Iterator it = aliases.entrySet().iterator(); it.hasNext();) {
      Map.Entry entry = (Map.Entry) it.next();
      String nom = (String) entry.getKey();
      Alias alias = (Alias) entry.getValue();

      if (!nestedConfigs.containsKey(nom)) {
        Object valeur = getValeur(cible, nom);

        if (valeur != null) {
          Operateur op;

          if (alias.operateur != null) {
            op = alias.operateur;
          } else {
            String opStr = (String) UtilitaireObjet.getValeurAttribut(cible,
                alias.attributOperateur);

            if (UtilitaireString.isVide(opStr)) {
              op = null;
            } else {
              op = Operateur.valueOf(opStr);
            }
          }

          if (op != null) {
            valeurs.add(verifierCritere(alias, valeur, op));
          }
        }
      }
    }

    return valeurs;
  }

  /**
   * Gestion des critères de recherche.
   */
  protected AttributFiltre verifierCritere(Alias alias, Object valeur,
      Operateur op) {
    // Si on effectue une recherche sur une date, avec un critère de fin
    // inférieur ou égal à une date donnée,
    // prendre en compte les enregistrements dont la date est la même que le
    // critère,
    // pour cela, mettre strictement inférieur au critère et positionner la date
    // à 23h59m59s.999
    if (Operateur.PLUS_PETIT_EGALE.equals(op) && valeur instanceof Date) {

      Calendar calendar = Calendar.getInstance();
      calendar.setTime((Date) valeur);
      int hour = calendar.get(Calendar.HOUR);
      int minute = calendar.get(Calendar.MINUTE);

      if (hour == 0 && minute == 0) {

        // On ajoute un jour
        Date nouvelleDate = DateUtils.addDays((Date) valeur, 1);
        // On l'initialise à minuit
        nouvelleDate = DateUtils.truncate(nouvelleDate, Calendar.DATE);
        // On enlève 1 ms
        valeur = DateUtils.addMilliseconds(nouvelleDate, -1);
      }
      // Change l'opérateur
      op = Operateur.PLUS_PETIT;
    }
    return (new AttributFiltre(alias.attributEntite, valeur, op));
  }

  /**
   * Si l'objet est une <code>String</code>, les espaces au début et à la fin
   * sont enlevés et la valeur <code>null</code> est retourné si la chaine est
   * de longueur zéro.
   */
  private static Object annuler(Object valeur) {
    if (valeur instanceof String) {
      String str = ((String) valeur).trim();

      return (str.length() == 0) ? null : str;
    } else {
      return valeur;
    }
  }

  /**
   * Retourne un <code>AttributFiltre</code> pour le nom et la valeur spécifiée.
   * 
   * @param nom
   *          le nom de l'attribut
   * @param valeur
   *          la valeur de l'attribut
   */
  private AttributFiltre getAttribut(String nom, Object valeur) {
    if (valeur instanceof String) {
      MatchMode mode = (MatchMode) matchModes.get(nom);

      if (mode == null) {
        mode = defautMatchMode;
      }

      return new AttributFiltre(nom, valeur, mode);
    } else {
      return new AttributFiltre(nom, valeur);
    }
  }

  /**
   * Retourne la valeur de l'attribut <code>nom</code> de l'objet
   * <code>cible</code>, traitée pour les valeurs primitives et chaines vides.
   */
  private Object getValeur(Object cible, String nom) {
    Object valeur = UtilitaireObjet.getValeurAttribut(cible, nom);
    Class type = UtilitaireObjet.getClasse(cible, nom);

    return getValeur(nom, valeur, type);
  }

  /**
   * Retourne la valeur de l'attribut décrit par le
   * <code>PropertyDescriptor</code> de l'objet <code>cible</code>, traitée pour
   * les valeurs primitives et chaines vides.
   */
  private Object getValeur(Object bean, PropertyDescriptor pd) {
    Object valeur;

    try {
      valeur = pd.getReadMethod().invoke(bean, (Object[]) null);
    } catch (IllegalAccessException e) {
      throw new IllegalArgumentException("Le getter pour la propriété "
          + pd.getName() + " dans la classe " + bean.getClass().getName()
          + " doit être publique");
    } catch (InvocationTargetException e) {
      Throwable cause = e.getCause();

      if (cause instanceof Error) {
        throw (Error) cause;
      } else if (cause instanceof RuntimeException) {
        throw (RuntimeException) cause;
      } else {
        throw new SOFIException(cause);
      }
    }

    return getValeur(pd.getName(), valeur, pd.getPropertyType());
  }

  private Object getValeur(String nom, Object valeur, Class type) {
    if ((valeur != null) && type.isPrimitive() && !acceptPrimitive(nom, valeur)) {
      return null;
    } else {
      return annuler(valeur);
    }
  }

  /**
   * Définit un attribut comme étant imbriqué, utilisant une configuration de
   * filtre par défaut.
   * 
   * @param nomAttribut
   *          le nom de l'attribut imbriqué
   */
  public void ajouterNested(String nomAttribut) {
    ajouterNested(nomAttribut, CONFIG_DEFAUT);
  }

  /**
   * /** Définit un attribut comme étant imbriqué, utilisant la configuration de
   * filtre spécifiée.
   * 
   * @param nomAttribut
   *          le nom de l'attribut imbriqué
   * @param config
   *          la configuration du filtre imbriqué
   */
  public void ajouterNested(String nomAttribut, ConfigFiltre config) {
    nestedConfigs.put(nomAttribut, config);

    // Les attributs nested sont exclus pour
    // éviter qu'ils soient traiter deux fois
    exclureAttribut(nomAttribut);
  }

  /**
   * Permet de déterminer si la valeur correspondant à une primitive passé en
   * paramètre doit être inclues dans les résultats retournés pour le filtre.
   * 
   * @param nom
   *          le nom de l'attribut pour la valeur
   * @param valeur
   *          la valeur de la primitive
   * @return <code>true</code> si la valeur doit être traitée,
   *         <code>false</code> sinon.
   */
  private boolean acceptPrimitive(String nom, Object valeur) {
    Boolean accepte = (Boolean) defautsPrimitives.get(nom);

    if (accepte == null) {
      accepte = Boolean.valueOf(accepteDefautPrimitives);
    }

    if (!accepte.booleanValue()) {
      if (valeur instanceof Boolean) {
        return Boolean.TRUE.equals(valeur);
      } else if (valeur instanceof Character) {
        return !(new Character((char) 0)).equals(valeur);
      } else {
        Number n = (Number) valeur;

        return (n.longValue() != 0) || (n.doubleValue() != 0);
      }
    } else {
      return true;
    }
  }

  /**
   * Exclue un attribut du filtre.
   * 
   * @param nom
   *          le nom de l'attribut à exclure
   */
  public void exclureAttribut(String nom) {
    exclures.add(nom);
  }

  /**
   * Exclue des attributs du filtre.
   * 
   * @param noms
   *          les noms des l'attributs à exclure
   */
  public void exclureAttributs(String[] noms) {
    exclures.addAll(Arrays.asList(noms));
  }

  /**
   * Exclue des attributs du filtre.
   * 
   * @param noms
   *          les noms des l'attributs à exclure
   */
  public void exclureAttributs(Collection noms) {
    for (Iterator it = noms.iterator(); it.hasNext();) {
      // Cast en String pour s'assurer du type des
      // éléments de la liste
      exclures.add(it.next());
    }
  }

  /**
   * Définit un intervalle comme critère de recherche.
   * 
   * @param attributDebut
   *          le nom de l'attribut de début de l'intervalle
   * @param attributFin
   *          le nom de l'attribut de début de fin de l'intervalle
   * @param attributEntite
   *          le nom de l'attribut de l'entité avec lequel faire la comparaison
   */
  public void ajouterIntervalle(String attributDebut, String attributFin,
      String attributEntite) {
    ajouterAlias(attributDebut, attributEntite, Operateur.PLUS_GRAND_EGALE);
    ajouterAlias(attributFin, attributEntite, Operateur.PLUS_PETIT_EGALE);
  }

  /**
   * La valeur de l'attribut <code>nom</code> sera utilisée pour comparaison
   * avec <code>attributEntite</code>.
   * 
   * @param nom
   *          le nom de l'attribut sur l'objet filtre
   * @param attributEntite
   *          le nom de l'attribut sur l'entité
   */
  public void ajouterAlias(String nom, String attributEntite) {
    ajouterAlias(nom, attributEntite, Operateur.EGALE);
  }

  /**
   * La valeur de l'attribut <code>nom</code> sera utilisée pour comparaison
   * avec <code>attributEntite</code>.
   * 
   * @param nom
   *          le nom de l'attribut sur l'objet filtre
   * @param attributEntite
   *          le nom de l'attribut sur l'entité
   * @param op
   *          l'opérateur à utiliser pour la comparaison
   */
  public void ajouterAlias(String nom, String attributEntite, Operateur op) {
    aliases.put(nom, new Alias(attributEntite, op));

    // Les attributs alias sont exclus pour
    // éviter qu'ils soient traiter deux fois
    exclureAttribut(nom);
  }

  /**
   * La valeur de l'attribut <code>nom</code> sera utilisée pour comparaison
   * avec <code>attributEntite</code>.
   * 
   * @param nom
   *          le nom de l'attribut sur l'objet filtre
   * @param attributEntite
   *          le nom de l'attribut sur l'entité
   * @param attrOp
   *          le nom de l'attribut où obtenir la valeur de l'opérateur à
   *          utiliser pour la comparaison
   */
  public void ajouterAlias(String nom, String attributEntite, String attrOp) {
    aliases.put(nom, new Alias(attributEntite, attrOp));

    // Les attributs alias sont exclus pour
    // éviter qu'ils soient traiter deux fois
    exclureAttribut(nom);
    exclureAttribut(attrOp);
  }

  /**
   * Définit le mode de comparaison des chaines de charactère pour l'attribut
   * <code>nomAttribut</code>.
   * 
   * @param nomAttribut
   *          le nom de l'attribut
   * @param mode
   *          le mode de comparaison à utiliser
   */
  public void setMatchMode(String nomAttribut, MatchMode mode) {
    if (nomAttribut == null) {
      throw new NullPointerException("nomAttribut");
    } else if (mode == null) {
      throw new NullPointerException("MatchMode");
    }

    matchModes.put(nomAttribut, mode);
  }

  /**
   * Définit le mode de comparaison des chaines de charactère pour les attributs
   * identifiés par <code>nomAttributs</code>.
   * 
   * @param nomAttributs
   *          le nom des attributs
   * @param mode
   *          le mode de comparaison à utiliser
   */
  public void setMatchMode(String[] nomAttributs, MatchMode mode) {
    for (int i = 0, n = nomAttributs.length; i < n; i++) {
      setMatchMode(nomAttributs[i], mode);
    }
  }

  /**
   * Définit le mode de comparaison des chaines de charactère pour les attributs
   * identifiés par <code>nomAttributs</code>.
   * 
   * @param nomAttributs
   *          le nom des attributs
   * @param mode
   *          le mode de comparaison à utiliser
   */
  public void setMatchMode(Collection attrs, MatchMode mode) {
    for (Iterator it = attrs.iterator(); it.hasNext();) {
      setMatchMode((String) it.next(), mode);
    }
  }

  /**
   * Définit le mode de comparaison par défaut pour les chaines de charactères.
   */
  public void setDefautMatchMode(MatchMode mode) {
    if (mode == null) {
      throw new NullPointerException("MatchMode");
    }

    this.defautMatchMode = mode;
  }

  /**
   * Définit si les primitives ayant la valeur par défaut (0) doivent être
   * exclue ou non. Par défaut, elle sont exclues.
   * 
   * @param accepteDefautPrimitives
   *          vrai pour les inclure, faux sinon.
   */
  public void setAccepteDefautPrimitives(boolean accepteDefautPrimitives) {
    this.accepteDefautPrimitives = accepteDefautPrimitives;
  }

  /**
   * Définit s'il faut inclure les valeurs primives à zéro pour le nom
   * d'attribut spécifié.
   * 
   * @param nomAttribut
   *          le nom de l'attribut
   * @param inclure
   *          s'il faut inclure les valeurs primives à zéro
   */
  public void setDefautPrimitive(String nomAttribut, boolean inclure) {
    if (nomAttribut == null) {
      throw new NullPointerException("nomAttribut");
    }

    defautsPrimitives.put(nomAttribut, Boolean.valueOf(inclure));
  }

  /**
   * Définit s'il faut inclure les valeurs primives à zéro pour les noms
   * d'attributs spécifiés.
   * 
   * @param nomAttributs
   *          les noms des attributs
   * @param inclure
   *          s'il faut inclure les valeurs primives à zéro
   */
  public void setDefautPrimitives(String[] nomAttributs, boolean inclure) {
    for (int i = 0, n = nomAttributs.length; i < n; i++) {
      setDefautPrimitive(nomAttributs[i], inclure);
    }
  }

  /**
   * Définit s'il faut inclure les valeurs primives à zéro pour les noms
   * d'attributs spécifiés.
   * 
   * @param nomAttributs
   *          les noms des attributs
   * @param inclure
   *          s'il faut inclure les valeurs primives à zéro
   */
  public void setDefautPrimitives(Collection attrs, boolean inclure) {
    for (Iterator it = attrs.iterator(); it.hasNext();) {
      setDefautPrimitive((String) it.next(), inclure);
    }
  }

  /**
   * Classe contenant les paramètres d'un alias
   */
  private static class Alias implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -6047317677646812606L;
    private String attributEntite;
    private Operateur operateur;
    private String attributOperateur;

    public Alias(String attributEntite, Operateur op) {
      this.attributEntite = attributEntite;
      this.operateur = op;
    }

    public Alias(String attributEntite, String attributOperateur) {
      this.attributEntite = attributEntite;
      this.attributOperateur = attributOperateur;
    }
  }
}
