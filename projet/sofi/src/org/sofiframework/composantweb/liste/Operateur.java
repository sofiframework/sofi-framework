/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Énumération d'opérateur de comparaison pour le filtre.
 * @author Guillaume Poirier
 */
public final class Operateur implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5282862337496769642L;
  private String nom;
  private static final Map INSTANCES = new HashMap();

  protected Operateur(String nom) {
    this.nom = nom;
  }

  @Override
  public String toString() {
    return nom;
  }

  public static Operateur valueOf(String nom) {
    Operateur op = (Operateur) INSTANCES.get(nom);
    if (op == null) {
      throw new IllegalArgumentException("Aucun opérateur '" + nom + "'");
    }
    return op;
  }

  public static final Operateur EGALE = new Operateur("=");
  public static final Operateur PLUS_GRAND = new Operateur(">");
  public static final Operateur PLUS_GRAND_EGALE = new Operateur(">=");
  public static final Operateur PLUS_PETIT = new Operateur("<");
  public static final Operateur PLUS_PETIT_EGALE = new Operateur("<=");

  static {
    ajouter(EGALE);
    ajouter(PLUS_GRAND);
    ajouter(PLUS_GRAND_EGALE);
    ajouter(PLUS_PETIT);
    ajouter(PLUS_PETIT_EGALE);
  }
  private static void ajouter(Operateur operateur) {
    INSTANCES.put(operateur.nom, operateur);
  }

  private Object readResolve() {
    return INSTANCES.get(nom);
  }

}
