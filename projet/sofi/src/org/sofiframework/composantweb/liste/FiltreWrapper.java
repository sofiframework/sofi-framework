/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.io.Serializable;
import java.util.Collection;


/**
 * Permet de réunir un Java Bean qui contient les données pour générer les
 * critères de recherche avec un objet de configuration du filtre.  Le filtre
 * retourne les attributs générés par l'objet <code>ConfigFiltre</code>.
 *
 * @author Guillaume Poirier
 */
public class FiltreWrapper implements Filtre, Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2446754656365352866L;
  private Object cible;
  private ConfigFiltre config;

  public FiltreWrapper(Object cible) {
    this(cible, new ConfigFiltre());
  }

  public FiltreWrapper(Object cible, ConfigFiltre config) {
    this.cible = cible;
    this.config = config;

    if (cible == null) {
      throw new NullPointerException("cible");
    }

    if (config == null) {
      throw new NullPointerException("config");
    }
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.composantweb.liste.Filtre#getValeurs()
   */
  @Override
  public Collection produireValeurs() {
    return config.getValeurs(cible);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "FiltreWrapper" + this.getClass().getName();
  }
}
