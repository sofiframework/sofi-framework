/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


/**
 * Cette classe abstraite de base
 * contient les methode pour conserver,
 * reinitialiser les onglets et rechercher
 * un onglet.
 *
 *
 *
 * @author Sebastien Cayer, nurun inc.
 */
public abstract class AbstractComposantMenuModifiable extends ComposantMenu
implements ComposantMenuModifiable {

  /**
   * 
   */
  private static final long serialVersionUID = 2431544950029471895L;

  protected void reinitialiserOnglet(String nomOngletEnCache,
      List listeOnglets, HttpServletRequest request) {
    List listeOngletsOriginal = (List) request.getSession().getAttribute(nomOngletEnCache);

    if (listeOngletsOriginal != null) {
      for (Iterator iterOnglet = listeOngletsOriginal.iterator();
          iterOnglet.hasNext();) {
        ComposantMenu ongletTemporaire = (ComposantMenu) iterOnglet.next();
        ComposantMenu onglet = getComposantMenuSelonNom(listeOnglets,
            ongletTemporaire.getLibelle());
        onglet.setLibelle(ongletTemporaire.getLibelle());
        onglet.setStyleCSS(ongletTemporaire.getStyleCSS());
        onglet.setOrdreAffichage(ongletTemporaire.getOrdreAffichage());
      }

      listeOnglets = listeOngletsOriginal;
    }
  }

  protected void conserverProprieteOnglet(String nomOngletEnCache,
      List listeOnglets, HttpServletRequest request) {
    List ongletInitiale = (List) request.getSession().getAttribute(nomOngletEnCache);

    if (ongletInitiale == null) {
      List nouvelleList = new ArrayList();

      for (Iterator iterOnglet = listeOnglets.iterator(); iterOnglet.hasNext();) {
        ComposantMenu onglet = (ComposantMenu) iterOnglet.next();
        ComposantMenu ongletTemporaire = new ComposantMenu();
        ongletTemporaire.setLibelle(onglet.getLibelle());
        ongletTemporaire.setStyleCSS(onglet.getStyleCSS());
        ongletTemporaire.setOrdreAffichage(onglet.getOrdreAffichage());
        nouvelleList.add(ongletTemporaire);
      }

      request.getSession().setAttribute(nomOngletEnCache, nouvelleList);
    }
  }

  protected ComposantMenu getComposantMenuSelonNom(List listeOnglets, String nom) {
    ComposantMenu ongletRecherche = null;

    for (Iterator iterOnglet = listeOnglets.iterator();
        iterOnglet.hasNext() && (ongletRecherche == null);) {
      ComposantMenu onglet = (ComposantMenu) iterOnglet.next();

      if (onglet.getLibelle().equals(nom)) {
        ongletRecherche = onglet;
      }
    }

    return ongletRecherche;
  }
}
