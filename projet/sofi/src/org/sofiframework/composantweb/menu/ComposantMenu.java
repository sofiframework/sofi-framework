/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Onglet;
import org.sofiframework.application.securite.objetstransfert.Section;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe permettant de créer des menus (horizontal, vertical, arbre, etc)
 * dynamiquement. Il est basé sur le canevas Struts-Menu.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @see net.sf.navigator.menu.MenuBase
 */
public class ComposantMenu implements Cloneable, Serializable {

  private static final long serialVersionUID = 6095957438107526172L;

  /** Tableau des composants de menu enfants **/
  protected static ComposantMenu[] composantMenu = new ComposantMenu[0];

  /** L'identifiant du menu. */
  protected String identifiant;

  /** Image associé avec le menu. */
  protected String image;

  /** la cible du menu. */
  protected String cible;

  /** Action Struts. */
  protected String action;

  /** Adresse web externe. */
  protected String adresseWeb;

  /** Url du menu. */
  protected String url;

  /** Forward s'il y lieu. */
  protected String forward;

  /** Page s'il y lieu. */
  protected String page;

  /** Libellé du menu. */
  protected String libelle;

  /** Aide contextuelle du menu. */
  protected String aideContextuelle;

  /** Adresse Web de base, action + methode seulement **/
  private String locationDeBase = null;

  /** Si section, est-ce la dernière section **/
  private boolean derniereSection = false;

  /** Est-ce que le menu est sélectionné **/
  private boolean selectionne = false;

  /** Est-ce qu'un url existe pour le menu **/
  private boolean urlExiste = false;

  /** Compostant de menu parent **/
  private ComposantMenu composantMenuParent;

  /** Liste des composants de menu associé au menu courant **/
  protected ArrayList listeComposantMenus = new ArrayList();

  /** Le numéro de menu, correspond à l'offre d'affichage **/
  private Integer noMenu;

  /** Le nombre de menu enfants **/
  private Integer nbMenuEnfants;

  /** Est-ce le dernier menu enfants **/
  private boolean dernierMenuEnfant = false;

  /** L'identifiant parent au menu **/
  private String identifiantParent;

  /** L'objet sécurisable associé au menu **/
  private ObjetSecurisable objetSecurisable;
  private boolean composantMenuAccueil;

  /** Le style CSS a appliquer sur le composant de menu **/
  private String styleCSS;

  /** Le mode d'affichage du composant de menu **/
  private String modeAffichage;

  /** Y as-t-il des enfants pour le fil de navigation **/
  private boolean enfantsPourFilNavigation = false;
  private boolean actif = true;
  private boolean selectionDesative;

  /**
   * Afficher le libelle avec seulement la première lettre
   * en majuscule.
   */
  private boolean libellePremiereLettreEnMajuscule;
  private Integer ordreAffichage;

  public ComposantMenu() {
  }

  /**
   * Afficher un résumé du composant de menu.
   */
  @Override
  public String toString() {
    return getLibelle() + " " + getNbMenuEnfants() + " Enfants";
  }

  /**
   * Retourne l'identifiant du composant de menu.
   * @return l'identifiant de la composante de menu.
   */
  public String getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant de la composante de menu.
   * @param identifiant l'identifiant de la composante de menu.
   */
  public void setIdentifiant(String identifiant) {
    this.identifiant = "menu" + identifiant;
  }

  /**
   * Retourne le libellé du menu qui est affiché dans le menu.
   * @return le libellé du menu
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Fixer le libelle qui doit être affiché dans le menu.
   * <p>
   * Si vous utilisez un engin de message, vous devez spécifiez la clé
   * du message correspondant au nom du menu.
   * @param libelle le libellé du menu
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Retourne l'action (Struts Action) que la composante du menu doit appellé.
   * @return l'action de la composante de menu.
   */
  public String getAction() {
    return action;
  }

  /**
   * Fixer l'action (Struts Action) que la composante du menu doit appellé.
   * <p>
   * Utiliser lorsque le composant du menu est associé à
   * un action Struts.
   * @param action l'action de la composante de memu.
   */
  public void setAction(String action) {
    this.action = action;
  }

  /**
   * Retourne une adresse complète d'un site web.
   * @since 1.8 Vous pouvez spécifiez la base du url via une paramètre système
   * en encapsulant avec les caractères [ et ]. Exemple [urlApplicationA]/accueil.do?methode=acceder
   * @return une adresse complète.
   */
  public String getAdresseWeb() {
    if ((adresseWeb != null) && (adresseWeb.indexOf("[") != -1)) {
      int positionDebutParametre = adresseWeb.indexOf("[");
      int positionFinParametre = adresseWeb.indexOf("]");

      if ((positionDebutParametre != -1) && (positionFinParametre != -1)) {
        String parametre = adresseWeb.substring(positionDebutParametre + 1,
            positionFinParametre);
        String valeurParametre = GestionParametreSysteme.getInstance()
            .getString(parametre);

        return valeurParametre +
            adresseWeb.substring(positionFinParametre + 1);
      }
    }

    return adresseWeb;
  }

  /**
   * Fixer une adresse complète.
   * <p>
   * Doit débuter par http://
   * @param newAdresse une adresse complète.
   */
  public void setAdresseWeb(String adresseWeb) {
    this.adresseWeb = adresseWeb;
  }

  /**
   * Retourne s'il a un url qui est spécifié pour le menu.
   * @return true s'il a un url qui est spécifié pour le menu.
   */
  public boolean isMenuAvecUrl() {
    if (!UtilitaireString.isVide(getUrl()) &&
        !getUrl().equals("javascript:void(0);")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne l'adresse Web complète du composant de menu.
   * @return adresse Web complète.
   */
  public String getUrl() {
    if (getModeAffichage() == null) {
      setModeAffichage("M");
    }

    if ((getAction() == null) && (getAdresseWeb() == null)) {
      url = "javascript:void(0);";
    }

    String cleMessageAvertissement = GestionParametreSysteme.getInstance()
        .getString(ConstantesParametreSysteme.CLE_MESSAGE_AVERTISSEMENT_FORMULAIRE_MODIFIE);

    if (!isSelectionDesative() && !getModeAffichage().equals("N") &&
        !getModeAffichage().equals("P")) {
      if ((url != null) && (url.indexOf("javascript") == -1) &&
          !cleMessageAvertissement.equals("")) {
        StringBuffer lienAppel = new StringBuffer();
        lienAppel.append("javascript:go('");
        lienAppel.append(url);
        lienAppel.append("');");
        url = lienAppel.toString();
      }
    }

    return url;
  }

  /**
   * Fixer l'url du menu
   * @param url l'url du menu
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Retourne l'image du menu s'il y a lieu
   * @return l'image
   */
  public String getImage() {
    return image;
  }

  /**
   * Fixer l'image du menu
   * @param image l'image du menu
   */
  public void setImage(String image) {
    this.image = image;
  }

  /**
   * Fixer le forward s'il y a lieu
   * @return String le forward
   */
  public String getForward() {
    return forward;
  }

  /**
   * Fixer le forward.
   * @param forward le forward a utiliser
   */
  public void setForward(String forward) {
    this.forward = forward;
  }

  /**
   * Retourne la valeur de la page.
   * @return la valeur de la page.
   */
  public String getPage() {
    return this.page;
  }

  /**
   * Fixer la valeur de la page
   * @param page la valeur de page.
   */
  public void setPage(String page) {
    this.page = page;
  }

  /**
   * Retourne la cible.
   * @return la cible.
   */
  public String getCible() {
    return this.cible;
  }

  /**
   * Fixer la cible
   * @param cible la cible.
   */
  public void setCible(String cible) {
    this.cible = cible;
  }

  /**
   * Retourne l'aide contextuelle
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle du composant de menu.
   * <p>
   * Si vous utilisez un engin de message, vous devez spécifiez la clé
   * du message correspondant au nom du menu.
   * @param aideContextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Permet d'ajouter une composante du menu courant.
   * @param composanteMenu
   */
  public void ajouterComposantMenu(ComposantMenu composantMenu,
      ObjetSecurisable objetSecurisable) {
    composantMenu.setObjetSecurisable(objetSecurisable);

    if ((this.getObjetSecurisable() != null) &&
        (this.getObjetSecurisable().getSeqObjetSecurisable().longValue() != objetSecurisable.getSeqObjetSecurisable()
        .longValue())) {
      listeComposantMenus.add(composantMenu);

      if (objetSecurisable.getObjetSecurisableParent() != null) {
        composantMenu.setParent(this);
      }

      if ((composantMenu.getIdentifiant() == null) ||
          (composantMenu.getIdentifiant().equals(""))) {
        composantMenu.setIdentifiant(identifiant + listeComposantMenus.size());
      }
    }
  }

  /**
   * Permet d'ajouter une composante du menu courant.
   * @param composanteMenu
   */
  public void ajouterComposantMenu(ComposantMenu composantMenu) {
    listeComposantMenus.add(composantMenu);
    composantMenu.setParent(this);

    if ((composantMenu.getIdentifiant() == null) ||
        (composantMenu.getIdentifiant().equals(""))) {
      composantMenu.setIdentifiant(identifiant + listeComposantMenus.size());
    }
  }

  public ComposantMenu[] getComposantMenus() {
    ComposantMenu[] menus = (ComposantMenu[]) listeComposantMenus.toArray(composantMenu);

    return menus;
  }

  /**
   * Retourne tous les composants du menu associé au menu courant.
   * @return
   */
  public ArrayList getComposantsMenu() {
    Iterator iterateur = listeComposantMenus.iterator();
    boolean isTrouve = false;
    int noMenu = 1;

    while (iterateur.hasNext() && !isTrouve) {
      ComposantMenu composantMenu = (ComposantMenu) iterateur.next();

      if (composantMenu.getNoMenu() != null) {
        isTrouve = true;
      } else {
        composantMenu.setNoMenu(new Integer(noMenu));
        noMenu++;
      }
    }

    setNbMenuEnfants(new Integer(getNbMenuEnfants(listeComposantMenus)));

    return listeComposantMenus;
  }

  public int getNbMenuEnfants(ArrayList listeComposantMenus) {
    int nbSousMenu = 0;

    if (listeComposantMenus != null) {
      Iterator iterateur = listeComposantMenus.iterator();

      while (iterateur.hasNext()) {
        ComposantMenu composantMenu = (ComposantMenu) iterateur.next();

        if ((composantMenu.getOrdreAffichage() != null) &&
            (composantMenu.getOrdreAffichage().intValue() > 0)) {
          nbSousMenu++;
        }
      }
    }

    return nbSousMenu;
  }

  /**
   * Retourne si le composant de menu est sélectionné
   * @return true si le composant de menu est sélectioné
   */
  public boolean isSelectionne() {
    return selectionne;
  }

  /**
   * Fixer si le compostant de menu est sélectionné
   * @param selectionne true si menu sélectionné
   */
  public void setSelectionne(boolean selectionne) {
    this.selectionne = selectionne;
  }

  /**
   * Retourne la location de base.
   * Utiliser par la gestion des onglets.
   * @return la location de base.
   */
  public String getLocationDeBase() {
    return locationDeBase;
  }

  /**
   * Fixer la location de base.
   * Utiliser par la gestion des onglets.
   * @param newLocationDeBase la location de base
   */
  public void setLocationDeBase(String newLocationDeBase) {
    this.locationDeBase = newLocationDeBase;
  }

  /**
   * Retourne si un url existe.
   * @return true si un url existe.
   */
  public boolean isUrlExiste() {
    return urlExiste;
  }

  /**
   * Fixer si un url existe pour le composant de menu.
   * @param urlExiste true si un url existe pour le composant de menu.
   */
  public void setUrlExiste(boolean urlExiste) {
    this.urlExiste = urlExiste;
  }

  /**
   * Retourne le composant de menu parent
   * @return le composant de menu parent.
   */
  public ComposantMenu getParent() {
    return composantMenuParent;
  }

  /**
   * Fixer le composant menu Parent
   * @param parentMenu le composant menu Parent
   */
  public void setParent(ComposantMenu composantMenuParent) {
    this.composantMenuParent = composantMenuParent;
  }

  /**
   * Spécifie l'ordre d'affichage du composant de menu
   * @return l'ordre d'affichage du composant de menu
   */
  public Integer getNoMenu() {
    return noMenu;
  }

  /**
   * Fixer l'ordre d'affichage du composant de menu
   * @param ordreAffichage l'ordre d'affichage du composant de menu
   */
  public void setNoMenu(Integer noMenu) {
    this.noMenu = noMenu;
  }

  /**
   * Retourne le nombre de menu enfants si le menu est une section.
   * @return le nombre de menu enfants si le menu est une section.
   */
  public Integer getNbMenuEnfants() {
    return nbMenuEnfants;
  }

  /**
   * Fixer le nombre de menu enfants si le menu est une section.
   * @param nbMenuEnfants le nombre de menu enfants si le menu est une section.
   */
  public void setNbMenuEnfants(Integer nbMenuEnfants) {
    this.nbMenuEnfants = nbMenuEnfants;
  }

  /**
   * Spécifie que le menu est enfants d'un parent et il est le dernier
   * menu affiché.
   * @return true si le menu est enfants d'un parent et il est le dernier menu affiché.
   */
  public boolean isDernierMenuEnfant() {
    if (getParent() == null) {
      return dernierMenuEnfant;
    }

    if (getNoMenu().intValue() == getParent().getComposantsMenu().size()) {
      return true;
    }

    return false;
  }

  /**
   * Fixer si le le menu est enfants d'un parent et il est le dernier
   * menu affiché.
   * @param dernierMenuEnfant true  si le menu est enfants d'un parent et il est le dernier menu affiché.
   */
  public void setDernierMenuEnfant(boolean dernierMenuEnfant) {
    this.dernierMenuEnfant = dernierMenuEnfant;
  }

  /**
   * Retourne si le composant de menu est la dernière section du menu.
   * @return true si le composant de menu est la dernière section du menu.
   */
  public boolean isDerniereSection() {
    return derniereSection;
  }

  /**
   * Fixer si c'est le composant de menu est la dernière section du menu.
   * @param derniereSection true si c'est le composant de menu est la dernière section du menu.
   */
  public void setDerniereSection(boolean derniereSection) {
    this.derniereSection = derniereSection;
  }

  /**
   * Retourne l'identifiant du parent.
   * @return l'identifiant du parent
   */
  public String getIdentifiantParent() {
    return identifiantParent;
  }

  /**
   * Fixer l'identifiant du parent.
   * @param identifiantParent l'identifiant du parent
   */
  public void setIdentifiantParent(String identifiantParent) {
    this.identifiantParent = "menu" + identifiantParent;
  }

  /**
   * Retourne si le composant de menu est une section.
   * @return true si le composant de menu est une section.
   */
  public boolean isSection() {
    if (getObjetSecurisable() != null) {
      if (Section.class.isInstance(getObjetSecurisable())) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * Retourne si le menu est un onglet
   * @return true si le composant de menu est un onglet
   */
  public boolean isService() {
    if (getObjetSecurisable() != null) {
      if (Service.class.isInstance(getObjetSecurisable()) &&
          !isComposantMenuAccueil()) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * Retourne si le menu est un onglet
   * @return true si le composant de menu est un onglet
   */
  public boolean isOnglet() {
    if (getObjetSecurisable() != null) {
      if (Onglet.class.isInstance(getObjetSecurisable())) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * Retourne l'objet sécurisable correspondant au composant de menu.
   * @return l'objet sécurisable correspondant au composant de menu.
   */
  public ObjetSecurisable getObjetSecurisable() {
    return objetSecurisable;
  }

  /**
   * Fixer l'objet sécurisable correspondant au composant de menu.
   * @param objetSecurisable l'objet sécurisable correspondant au composant de menu.
   */
  public void setObjetSecurisable(ObjetSecurisable objetSecurisable) {
    this.objetSecurisable = objetSecurisable;
  }

  /**
   * Permet de faire une copie de l'instance dans une autre instance du
   * composant de menu
   * @return une instance du composant de menu
   */
  @Override
  public Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }

  /**
   * Retourne true si correspond au composant qui est la page
   * d'accueil de l'application.
   * @return true si correspond au composant qui est la page d'accueil de l'application.
   */
  public boolean isComposantMenuAccueil() {
    return composantMenuAccueil;
  }

  /**
   * Fixer true si correspond au composant qui est la page
   * d'accueil de l'application.
   * @param composantMenuAccueil true si correspond au composant qui est la page d'accueil de l'application.
   */
  public void setComposantMenuAccueil(boolean composantMenuAccueil) {
    this.composantMenuAccueil = composantMenuAccueil;
  }

  /**
   * Retourne le style CSS pour le composant de menu.
   * @return  le style CSS pour le composant de menu.
   */
  public String getStyleCSS() {
    return styleCSS;
  }

  /**
   * Fixer le style CSS pour le composant de menu.
   * @param styleCSS  le style CSS pour le composant de menu.
   */
  public void setStyleCSS(String styleCSS) {
    this.styleCSS = styleCSS;
  }

  /**
   * Y as-t-il des enfants pour l'affichage du fil de navigation.
   * @return true s'il y a des enfants pour l'affichage du fil de navigation.
   */
  public boolean isEnfantsPourFilNavigation() {
    return enfantsPourFilNavigation;
  }

  /**
   * Fixer true s'il y a des enfants pour l'affichage du fil de navigation.
   * @param enfantsPourFilNavigation true s'il y a des enfants pour l'affichage du fil de navigation.
   */
  public void setEnfantsPourFilNavigation(boolean enfantsPourFilNavigation) {
    this.enfantsPourFilNavigation = enfantsPourFilNavigation;
  }

  /**
   * Fixer si le composant de menu est actif.
   * @param actif true si le composant de menu est actif.
   */
  public void setActif(boolean actif) {
    this.actif = actif;
  }

  /**
   * Est-ce que le composant de menu est actif?
   * @return true si le composant de menu est actif.
   */
  public boolean isActif() {
    return actif;
  }

  public void setSelectionDesative(boolean selectionDesative) {
    this.selectionDesative = selectionDesative;
  }

  public boolean isSelectionDesative() {
    return selectionDesative;
  }

  /**
   * Fixer true si le libellé avec la première lettre en majuscule seulement.
   * @param libellePremiereLettreEnMajuscule le libellé avec la première lettre en majuscule seulement.
   */
  public void setLibellePremiereLettreEnMajuscule(
      boolean libellePremiereLettreEnMajuscule) {
    this.libellePremiereLettreEnMajuscule = libellePremiereLettreEnMajuscule;
  }

  /**
   * Est-ce que le libelle doit débuter par la première lettre en majuscule seulement.
   * @return true si le libelle doit débuter par la première lettre en majuscule seulement.
   */
  public boolean isLibellePremiereLettreEnMajuscule() {
    return libellePremiereLettreEnMajuscule;
  }

  public void setModeAffichage(String modeAffichage) {
    this.modeAffichage = modeAffichage;
  }

  public String getModeAffichage() {
    if ((getAdresseWeb() != null) &&
        (getAdresseWeb().indexOf("popup('") != -1)) {
      return "P";
    }

    return modeAffichage;
  }

  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }
}
