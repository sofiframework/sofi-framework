/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.menu;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.digester.Digester;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Section;
import org.sofiframework.application.securite.objetstransfert.Service;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.balisesjsp.menu.AfficheurMenuVelocity;


/**
 * Répertoire des menus d'un utilisateur.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class RepertoireMenu implements Serializable {

  private static final long serialVersionUID = 7414122443487405907L;

  /** Variable désignant l'instance commune de journalisation */
  protected static Log log = LogFactory.getLog(RepertoireMenu.class);

  /** Clé spécifiant le répertoire de menu de l'utilisateur */
  public static final String CLE_REPERTOIRE_MENU = "menuUtilisateur";

  /** Clé spécifiant la désactivation de la sélection de menu */
  public static final String INACTIVER_SELECTION = "inactiverSelection";

  /** Clé spécifiant le message lorsque la sélection n'est pas possible */
  public static final String CLE_MESSAGE_SELECTION_INACTIF =
      "cleMessageSelectionInactif";

  /** Liste de tous les menus **/
  protected HashMap tousLesMenus = new HashMap();
  protected String config = null;
  protected String name = null;

  /** Liste de tous les menus avec hiérarchie **/
  protected LinkedHashMap menus = new LinkedHashMap();

  /** Composant de menu utilisé la page d'accueil **/
  private ComposantMenu composantMenuAccueil;

  /** Le servlet context **/
  protected ServletContext servletContext = null;

  public RepertoireMenu() {
  }

  /**
   * Ajouter le composant de menu dans la liste de tous les menus
   * peut importe leur hiérarchie.
   * @param composantMenu un composant de menu.
   */
  public void ajouterDansTousLesMenus(ComposantMenu composantMenu) {
    tousLesMenus.put(composantMenu.getIdentifiant(), composantMenu);
  }

  /**
   * Retourne le composant de menu dans la liste de tous les menus
   * peut importe leur hiérarchie.
   * @param identifiant l'identifiant du menu.
   * @return un composant de menu.
   */
  public ComposantMenu getComposantMenuDeTouslesMenus(String identifiant) {
    return (ComposantMenu)tousLesMenus.get(identifiant);
  }

  /**
   * Ajouter un nouveau menu.
   * @param menu Le composant de menu à ajouter.
   */
  public void ajouterMenu(ComposantMenu menu) {
    List enfants = null;

    ajouterDansTousLesMenus(menu);

    if (menus.containsKey(menu.getIdentifiant())) {
      enfants = getMenu(menu.getIdentifiant()).getComposantsMenu();
    } else {
      enfants = menu.getComposantsMenu();
    }

    if ((enfants != null) && (menu.getComposantsMenu() != null)) {
      for (Iterator it = enfants.iterator(); it.hasNext(); ) {
        ComposantMenu enfant = (ComposantMenu)it.next();

        if (enfant.getIdentifiantParent() != null) {
          enfant.setParent(getComposantMenuDeTouslesMenus(enfant.getIdentifiantParent()));
        }

        ajouterDansTousLesMenus(enfant);
      }
    }

    if (menu.getIdentifiantParent() != null) {
      menu.setParent(getComposantMenuDeTouslesMenus(menu.getIdentifiantParent()));
    }

    menus.put(menu.getIdentifiant(), menu);
  }

  /**
   * Permet d'ajouter un composant de menu associé avec un objet sécurisable
   * @param menu le composant de menu
   * @param objetSecurisable l'objet sécurisable associé.
   */
  public ComposantMenu ajouterMenu(ComposantMenu menu,
      ObjetSecurisable objetSecurisable) {
    if (getComposantMenuDeTouslesMenus(menu.getIdentifiant()) == null) {
      menu.setObjetSecurisable(objetSecurisable);
      ajouterMenu(menu);
    } else {
      menu = getComposantMenuDeTouslesMenus(menu.getIdentifiant());
    }

    return menu;
  }

  protected Digester initDigester() {
    Digester digester = new Digester();
    digester.setClassLoader(Thread.currentThread().getContextClassLoader());
    digester.push(this);

    //digester.setDebug(getDebug());
    // 1
    digester.addObjectCreate("MenuConfig/Menus/Menu",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu");
    digester.addSetNext("MenuConfig/Menus/Menu", "ajouterMenu");

    // 2
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item", "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    // 3
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item/Item",
        "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    // 4
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item/Item/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item/Item/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item/Item/Item",
        "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    // 5
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item/Item/Item/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    // 6
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item/Item/Item/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    // 7
    digester.addObjectCreate("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "org.sofiframework.composantweb.menu.ComposantMenu",
        "type");
    digester.addSetProperties("MenuConfig/Menus/Menu/Item/Item/Item/Item");
    digester.addSetNext("MenuConfig/Menus/Menu/Item/Item/Item/Item",
        "ajouterComposantMenu",
        "org.sofiframework.composantweb.menu.ComposantMenu");

    return digester;
  }

  /**
   * Charger le fichier de configuration application-menu.xml
   */
  public void load() {
    if (getServletContext() == null) {
      log.error("Servlet Context introuvable");
    }

    InputStream input = null;
    Digester digester = initDigester();

    try {
      input = getServletContext().getResourceAsStream(config);
      digester.parse(input);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    } finally {
      try {
        input.close();
      } catch (Exception e) {
      }
    }
  }

  /**
   * Retourne les nom de menu
   * @return les noms de menu
   */
  public Set getNomsMenu() {
    return menus.keySet();
  }

  /**
   * Retourne les menus du premier niveau Seulement.
   * @return les menus de premier niveau seulement.
   */
  public ArrayList getMenuPremierNiveau() {
    ArrayList topMenus = new ArrayList();

    if (menus == null) {
      log.warn("Aucun répertoire de menu de trouvé menus!");

      return topMenus;
    }

    for (Iterator it = menus.keySet().iterator(); it.hasNext(); ) {
      String name = (String)it.next();
      ComposantMenu menu = getMenu(name);

      if (menu.getParent() == null) {
        topMenus.add(menu);
      }
    }

    return topMenus;
  }

  /**
   * Retourne un composant de menu.
   * @param menuName le nom du menu
   * @return un composant de menu
   */
  public ComposantMenu getMenu(String menuName) {
    return (ComposantMenu)menus.get(menuName);
  }

  /**
   * Retourne un type d'affichage de menu
   * @param displayerName le type d'affichage de menu.
   * @return le type d'affichage de menu
   */
  public AfficheurMenuVelocity getAfficheurMenu() {
    AfficheurMenuVelocity afficheur = null;

    try {
      afficheur =
          (AfficheurMenuVelocity)Class.forName("org.sofiframework.presentation.balisesjsp.menu.AfficheurMenuVelocity").newInstance();
    } catch (InstantiationException e) {
      log.error(e.getMessage(), e);
    } catch (IllegalAccessException e) {
      log.error(e.getMessage(), e);
    } catch (ClassNotFoundException e) {
      log.error(e.getMessage(), e);
    }

    return afficheur;
  }

  /**
   * Supprimer un menu par son nom.
   * @param nom le nom du menu
   */
  public void supprimerMenu(String nom) {
    if (menus.containsKey(nom)) {
      menus.remove(getMenu(nom));
    }
  }

  /**
   * Initialiser le repertoire de menu
   */
  public void reload() {
    menus.clear();
    load();
  }

  public void setLoadParam(String loadParam) {
    config = loadParam;
  }

  public String getLoadParam() {
    return config;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public ServletContext getServletContext() {
    return servletContext;
  }

  public void setServletContext(ServletContext context) {
    this.servletContext = context;
  }

  /**
   * Retourne le composant de menu qui est utilisé pour l'accueil
   * @return le composant de menu qui est utilisé pour l'accueil
   */
  public ComposantMenu getComposantMenuAccueil() {
    return composantMenuAccueil;
  }

  /**
   * Fixer le composant de menu qui est utilisé pour l'accueil
   * @param composantMenuAccueil le composant de menu qui est utilisé pour l'accueil
   */
  public void setComposantMenuAccueil(ComposantMenu composantMenuAccueil) {
    if (GestionSecurite.getInstance().isPageAccueilInclusDansMenu()) {
      ComposantMenu composantMenu =
          (ComposantMenu)composantMenuAccueil.clone();
      ObjetSecurisable copieObjetSecurisable =
          (ObjetSecurisable)composantMenu.getObjetSecurisable().clone();
      composantMenu.setObjetSecurisable(copieObjetSecurisable);
      composantMenu.setIdentifiant("-1");
      composantMenu.getObjetSecurisable().setSeqObjetSecurisable(new Long(-1));
      composantMenu.setLibelle("Accueil");
      composantMenu.setComposantMenuAccueil(true);
      ajouterDansTousLesMenus(composantMenu);
      this.composantMenuAccueil = composantMenu;
    } else {
      composantMenuAccueil.setComposantMenuAccueil(true);
      ajouterDansTousLesMenus(composantMenuAccueil);
    }

    this.composantMenuAccueil = composantMenuAccueil;
  }

  /**
   * 
   */
  public ComposantMenu getComposantMenuFacette(String nomFacette) {
    ComposantMenu composantMenuFacette = getComposantMenuDeTouslesMenus(nomFacette);
    if (composantMenuFacette == null) {
      composantMenuFacette = new ComposantMenu();
      composantMenuFacette.setAdresseWeb("index.html");
      composantMenuFacette.setLibelle(nomFacette);
      composantMenuFacette.setIdentifiant(nomFacette);
      composantMenuFacette.setEnfantsPourFilNavigation(true);
      composantMenuFacette.setSelectionne(true);
      composantMenuFacette.setSelectionDesative(false);
      composantMenuFacette.setOrdreAffichage(new Integer(1));
      ajouterDansTousLesMenus(composantMenuFacette);
    }
    return composantMenuFacette;
  }


  /**
   * Créer un répertoire de menu avec l'aide d'un profil utilisateur.
   * @return le répertoire de menu de l'utilisateur.
   * @param request la requête HTTP en traitement.
   * @param utilisateur l'utilisateur en traitement.
   */
  public static void creerRepertoireMenu(Utilisateur utilisateur,
      HttpServletRequest request) {
    ServletContext servletContext = request.getSession().getServletContext();


    if ((servletContext.getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU) !=
        null) ||
        ((utilisateur != null) && (utilisateur.getListeObjetSecurisables() !=
        null))) {
      // Prendre le répertoire de menu original de la porté application.
      RepertoireMenu repertoireMenuOriginal =
          (RepertoireMenu)servletContext.getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);

      if (request.getSession().getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU) ==
          null) {
        int compteurNiveau1 = 1;

        RepertoireMenu repertoireMenu = new RepertoireMenu();
        // Créer un répertoire contenu le menu personnalisé d'un utilisateur
        if (repertoireMenuOriginal != null) {
          // Ajouter les menus déjà existant par défaut (dans fichier menu-config.xml).
          Iterator iterateurMenuExistant =
              repertoireMenuOriginal.getNomsMenu().iterator();

          while (iterateurMenuExistant.hasNext()) {
            String nomMenu = (String)iterateurMenuExistant.next();
            ComposantMenu composantMenu =
                repertoireMenuOriginal.getMenu(nomMenu);
            composantMenu.setNoMenu(new Integer(compteurNiveau1));
            repertoireMenu.ajouterMenu(composantMenu, null);

            if ((composantMenu.getObjetSecurisable() == null) ||
                (composantMenu.getObjetSecurisable().getOrdrePresentation() ==
                null) ||
                (composantMenu.getObjetSecurisable().getOrdrePresentation().intValue() !=
                0)) {
              compteurNiveau1++;
            }
          }
        }
        request.getSession().setAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU,
            repertoireMenu);
      }
    }

    // Créer le répertoire des composants de menus accessibles à l'utilisateur.
    request.getSession().setAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU,
        creerRepertoireMenu(utilisateur));
  }


  /**
   * Méthode permettant de créer un répertoire de composant de menu qui sont
   * accessible à l'utilisateur.
   * @param utilisateur l'utilisateur en traitement.
   * @return le repertoire du menu correponsant à l'utilisateur.
   */
  public static RepertoireMenu creerRepertoireMenu(Utilisateur utilisateur) {
    // Initialisation du répertoire de menu.
    RepertoireMenu repertoireMenu = new RepertoireMenu();
    int compteurNiveau1 = 1;
    boolean pageAccueilTrouve = false;


    int nombreSection = utilisateur.getListeObjetSecurisables().size();

    if ((GestionSecurite.getInstance().getPageAccueilDefaut() != null) &&
        !GestionSecurite.getInstance().isPageAccueilInclusDansMenu()) {
      nombreSection = nombreSection - 1;
    }

    if (utilisateur != null) {
      if ((utilisateur.getListeObjetSecurisables() != null) &&
          (utilisateur.getListeObjetSecurisables().size() > 0)) {
        Iterator iterateurListeObjetSecurisables =
            utilisateur.getListeObjetSecurisables().iterator();
        ComposantMenu composantMenuParent = null;
        ComposantMenu composantMenu = null;
        ObjetSecurisable ancienObjetSecurisable = null;

        while (iterateurListeObjetSecurisables.hasNext()) {
          composantMenuParent = new ComposantMenu();
          composantMenu = new ComposantMenu();

          ObjetSecurisable objetSecurisable =
              (ObjetSecurisable)iterateurListeObjetSecurisables.next();

          if ((ancienObjetSecurisable != null) &&
              ((objetSecurisable != null) &&
                  !objetSecurisable.getClass().getName().equals(ancienObjetSecurisable.getClass().getName())) &&
                  (objetSecurisable.getObjetSecurisableParent() != null) &&
                  (ancienObjetSecurisable.getObjetSecurisableParent() != null)) {
            compteurNiveau1 = 1;
          }

          if (objetSecurisable.getOrdrePresentation() != null && objetSecurisable.getOrdrePresentation().intValue() > 0) {
            ajouterMenuEnfant(repertoireMenu, composantMenuParent,
                composantMenu, objetSecurisable, compteurNiveau1,
                pageAccueilTrouve);
          }else {
            compteurNiveau1 =  compteurNiveau1 -1 ;
          }
          // Fixer le composant comme étant le dernière section du menu de premier niveau.
          if ((compteurNiveau1) > nombreSection) {
            composantMenu.setDerniereSection(true);
          }

          compteurNiveau1++;

          ancienObjetSecurisable = objetSecurisable;
        }
      }
    }

    return repertoireMenu;
  }

  public static void ajouterMenuEnfant(RepertoireMenu repertoireMenu,
      ComposantMenu composantMenuParent,
      ComposantMenu composantMenu,
      ObjetSecurisable objetSecurisable,
      int compteurNiveau,
      boolean pageAccueilTrouve) {
    composantMenu.setNoMenu(new Integer(compteurNiveau));

    if (Section.class.isInstance(objetSecurisable) &&
        !((Section)objetSecurisable).isSectionVide()) {
      composantMenu.setObjetSecurisable(objetSecurisable);
      composantMenu.setIdentifiant(objetSecurisable.getSeqObjetSecurisable().toString());
      composantMenu.setLibelle(objetSecurisable.getNom());
      composantMenu.setObjetSecurisable(objetSecurisable);
      composantMenu.setNbMenuEnfants(new Integer(objetSecurisable.getListeObjetSecurisableEnfants().size()));

      composantMenu.setOrdreAffichage(objetSecurisable.getOrdrePresentation());
      composantMenu.setStyleCSS(objetSecurisable.getStyleCss());

      // YT 2006-02-01 - Véfifier si la section a des enfants
      if (objetSecurisable.getObjetSecurisableParent() != null) {
        composantMenu.setIdentifiantParent(objetSecurisable.getObjetSecurisableParent().getSeqObjetSecurisable().toString());
        composantMenuParent.ajouterComposantMenu(composantMenu,
            objetSecurisable);
      }

      Iterator iterateurListeServiceEnfants =
          objetSecurisable.getListeObjetSecurisableEnfants().iterator();
      int index = 1;

      while (iterateurListeServiceEnfants.hasNext()) {
        ObjetSecurisable serviceEnfant =
            (ObjetSecurisable)iterateurListeServiceEnfants.next();
        ajouterMenuEnfant(repertoireMenu, composantMenu, new ComposantMenu(),
            serviceEnfant, index, pageAccueilTrouve);

        if (compteurNiveau ==
            objetSecurisable.getListeObjetSecurisableEnfants().size()) {
          composantMenu.setDernierMenuEnfant(true);
        } else {
          composantMenu.setDernierMenuEnfant(false);
        }

        index++;
      }
    } else {
      if ((objetSecurisable.getSeqObjetSecurisable() != null) &&
          (Service.class.isInstance(objetSecurisable) ||
              Section.class.isInstance(objetSecurisable))) {
        composantMenu.setIdentifiant(objetSecurisable.getSeqObjetSecurisable().toString());
        composantMenu.setAideContextuelle(objetSecurisable.getAideContextuelle());
        composantMenu.setLibelle(objetSecurisable.getNom());
        composantMenu.setModeAffichage(objetSecurisable.getTypeModeAffichage());

        if (objetSecurisable.getNomAction() == null) {
          composantMenu.setAdresseWeb(objetSecurisable.getAdresseWeb());
        } else {
          composantMenu.setAction(objetSecurisable.getAdresseWeb());
        }

        composantMenu.setModeAffichage(objetSecurisable.getModeAffichage());
        composantMenu.setStyleCSS(objetSecurisable.getStyleCss());

        composantMenu.setOrdreAffichage(objetSecurisable.getOrdrePresentation());

        if ((objetSecurisable.getObjetSecurisableParent() != null)) {
          composantMenu.setIdentifiantParent(objetSecurisable.getObjetSecurisableParent().getSeqObjetSecurisable().toString());
        }

        Integer pageAccueilDefaut =
            GestionSecurite.getInstance().getPageAccueilDefaut();

        if (pageAccueilDefaut != null) {
          if (pageAccueilDefaut.intValue() ==
              objetSecurisable.getSeqObjetSecurisable().intValue()) {
            if (GestionSecurite.getInstance().isPageAccueilInclusDansMenu()) {
              if (objetSecurisable.getObjetSecurisableParent() != null) {
                composantMenuParent.ajouterComposantMenu(composantMenu,
                    objetSecurisable);
              } else {
                repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);
              }

              repertoireMenu.setComposantMenuAccueil(composantMenu);
            } else {
              if (repertoireMenu.getComposantMenuAccueil() == null) {
                composantMenu.setObjetSecurisable(objetSecurisable);
                pageAccueilTrouve = true;
              }
            }

            repertoireMenu.setComposantMenuAccueil(composantMenu);
            GestionSecurite.getInstance().setComposantMenuAccueil(objetSecurisable);
            repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);
          } else {
            if (objetSecurisable.getObjetSecurisableParent() != null) {
              composantMenuParent.ajouterComposantMenu(composantMenu,
                  objetSecurisable);
            }

            repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);
          }
        }

        if ((objetSecurisable.getListeObjetSecurisableEnfants() != null) &&
            (objetSecurisable.getListeObjetSecurisableEnfants().size() >= 1)) {
          Iterator iterateur =
              objetSecurisable.getListeObjetSecurisableEnfants().iterator();
          int index = 1;

          while (iterateur.hasNext()) {
            ObjetSecurisable serviceEnfant =
                (ObjetSecurisable)iterateur.next();

            if (Service.class.isInstance(serviceEnfant)) {
              // Si service enfant, l'ajouter comme item de menu.
              ajouterMenuEnfant(repertoireMenu, composantMenu,
                  new ComposantMenu(), serviceEnfant, index,
                  pageAccueilTrouve);
            }

            index++;
          }
        }
      }
    }

    if (!pageAccueilTrouve &&
        !GestionSecurite.getInstance().isPageAccueilInclusDansMenu() &&
        (repertoireMenu.getComposantMenuAccueil() == null)) {
      repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);
    } else {
      if ((objetSecurisable.getListeObjetSecurisableEnfants() != null) &&
          (objetSecurisable.getListeObjetSecurisableEnfants().size() > 0)) {
        repertoireMenu.ajouterMenu(composantMenu, objetSecurisable);
      }

      pageAccueilTrouve = false;
    }
  }

  /**
   * Retourne le répertoire de menu disponible à l'utilisateur.
   * @return le répertoire de menu disponible à l'utilisateur.
   * @param request la requête HTTP en traitement.
   */
  public static RepertoireMenu getRepertoireMenu(HttpServletRequest request) {
    RepertoireMenu repertoireMenu =
        (RepertoireMenu)request.getSession().getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);

    if (repertoireMenu == null) {
      repertoireMenu =
          (RepertoireMenu)request.getSession().getServletContext().getAttribute(RepertoireMenu.CLE_REPERTOIRE_MENU);
    }

    return repertoireMenu;
  }

  /**
   * Permet de modifier le libellé d'un composant de menu dans le répertoire.
   * @param libelle Nouveau libellé du composant
   * @param identifiant Identifiant unique du composant (Exemple : "menu16").
   */
  public void modifierLibelleMenu(String identifiant, String libelle) {
    // Pour tous les menus. On essaie de modifier le composant pour chaque élément de menu.
    for (Iterator i = this.getNomsMenu().iterator(); i.hasNext(); ) {
      this.modifierLibelleComposantMenu(identifiant, libelle,
          this.getComposantMenuDeTouslesMenus((String)i.next()));
    }
  }

  /**
   * Modifie le libellé d'un composant si il correspond à l'identifiant
   * en paramètre. La méthode appelle récursivement les enfants de ce
   * composnat de menu.
   * @param itemMenu L'item qui doit être vérifié (avec tous ces enfants)
   * @param libelle Nouveau libellé donné à l'élément de menu.
   * @param identifiant Identifiant unique correspondant à l'item de
   * menu à modifier.
   */
  private void modifierLibelleComposantMenu(String identifiant, String libelle,
      ComposantMenu itemMenu) {
    // Si le composant est celui que l'on recherche. On modifie son libellé
    if (itemMenu.getIdentifiant().equals(identifiant)) {
      itemMenu.setLibelle(libelle);
    }

    // Si il possède des enfants
    if (itemMenu.getComposantsMenu() != null) {
      for (Iterator i = itemMenu.getComposantsMenu().iterator(); i.hasNext();
          ) {
        this.modifierLibelleComposantMenu(identifiant, libelle,
            (ComposantMenu)i.next());
      }
    }
  }

  public HashMap getTousLesMenus() {
    return tousLesMenus;
  }
}
