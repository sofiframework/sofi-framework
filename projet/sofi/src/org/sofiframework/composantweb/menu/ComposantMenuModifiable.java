/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;


/**
 * Interface qui permet la modification de composant
 * de menu.
 *
 * @author Sebastien Cayer,nurun inc.
 */
public interface ComposantMenuModifiable {
  /**
   * Obtenir le libelle du composant de
   * menu.
   *
   * @return libelle
   */
  String getLibelle();

  /**
   * Cette methode applique la modification
   * au composant de menu specifique.
   *
   * @param request
   * @param niveauATraiter
   * @param listeOnglets
   * @param identifiantObjetSecurisableParent
   * @param utilisateur
   */
  void appliquerModification(Utilisateur utilisateur,
      String identifiantObjetSecurisableParent, List listeOnglets,
      int niveauATraiter, HttpServletRequest request);
}
