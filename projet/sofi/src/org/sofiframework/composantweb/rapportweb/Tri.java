/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.rapportweb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.struts.util.LabelValueBean;


/**
 * Classes permettant de trier des listes.
 * <p>
 * Cet ensemble d'objet utilise les classes utilitaires de comparaison afin d'effectuer
 * le trier d'une liste.
 * <p>
 * Notons qu'il faut définir des objets "inline" afin d'indiquer la façon dont
 * se fait le tri.
 * <p>
 * @author Jean-François Brasard
 * @version 1.0
 * @see java.util.Comparator
 */
public class Tri {
  /** Classe interne servant à comparer deux lignes de rapport de facon alphabétique. */
  static final Comparator VALEUR_ALPHABETIQUE = new Comparator() {
    /**
     * Comparer deux lignes de rapport de facon alphabétique.
     * <p>
     * @param o1 le premier objet à comparer
     * @param o2 le deuxième objet a comparer
     * @return int un entier indiquant le résultat de la comparaison entre les deux objets
     * @see commun.RapportLigne
     */
    @Override
    public int compare(Object o1, Object o2) {
      RapportLigne ligne1 = (RapportLigne) o1;
      RapportLigne ligne2 = (RapportLigne) o2;
      String valeur1 = (String) ligne1.getValeurTri();
      String valeur2 = (String) ligne2.getValeurTri();

      return valeur1.compareTo(valeur2);
    }
  };

  /** Classe interne servant à comparer deux entrées de listes déroulantes de facon alphabétique. */
  static final Comparator TEXTE_LISTE = new Comparator() {
    /**
     * Comparer deux entrées de listes déroulantes de facon alphabétique.
     * <p>
     * @param o1 le premier objet à comparer
     * @param o2 le deuxième objet a comparer
     * @return int un entier indiquant le résultat de la comparaison entre les deux objets
     * @see org.apache.struts.util.LabelValueBean
     */
    @Override
    public int compare(Object o1, Object o2) {
      LabelValueBean ligne1 = (LabelValueBean) o1;
      LabelValueBean ligne2 = (LabelValueBean) o2;
      String valeur1 = ligne1.getLabel();
      String valeur2 = ligne2.getLabel();

      return valeur1.compareTo(valeur2);
    }
  };

  /** Classe interne servant à comparer deux entiers. */
  static final Comparator INTEGER = new Comparator() {
    @Override
    public int compare(Object o1, Object o2) {
      /**
       * Comparer deux entiers.
       * <p>
       * @param o1 le premier objet à comparer
       * @param o2 le deuxième objet a comparer
       * @return int un entier indiquant le résultat de la comparaison entre les deux objets
       */
      Integer int1 = (Integer) o1;
      Integer int2 = (Integer) o2;

      return int1.compareTo(int2);
    }
  };

  /** Constructeur par défaut de la classe. */
  public Tri() {
  }

  /**
   * Trier une liste de lignes de rapports selon le tri par valeur alphabétique.
   * <p>
   * @param listeLignes la liste a triée
   * @return Collection la liste triée
   */
  public Collection triParValeur(Collection listeLignes) {
    List liste = new ArrayList(listeLignes);
    Collections.sort(liste, VALEUR_ALPHABETIQUE);

    return liste;
  }

  /**
   * Trier une liste d'entrées de listes déroulantes selon le tri alphabétique.
   * <p>
   * @param listeLabel la liste a triée
   * @return ArrayList la liste triée
   */
  public ArrayList triLabelParTexte(ArrayList listeLabel) {
    ArrayList liste = new ArrayList(listeLabel);
    Collections.sort(liste, TEXTE_LISTE);

    return liste;
  }
}
