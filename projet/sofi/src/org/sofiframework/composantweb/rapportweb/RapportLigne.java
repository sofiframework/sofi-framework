/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.rapportweb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Objet représentant une ligne d'un rapport multi-niveaux.
 * <p>
 * Cet objet est utilisé dans la gestion des rapports à plusieurs niveaux afin
 * de représenter une ligne
 * <p>
 * Notons que ce rapport utilise la récursivité permettant ainsi la multitude des niveaux.
 * <p>
 * Notons de plus que ce rapport utilise la notion du polymorphisme afin de pouvoir
 * stocker un objet de tout type correspondant au détail d'une ligne.
 * <p>
 * @author Steve Tremblay
 * @version 1.0
 * @see org.sofiframework.composantweb.rapportweb.Rapport
 */
public class RapportLigne implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -7224801634920108299L;

  /** Constante pour une clé menant vers une autre page */
  public static final String CLE_AUTRE_PAGE = "AUTRE_PAGE";

  /** Constante pour une clé menant vers le détail d'une ligne */
  public static final String CLE_DETAIL = "DETAIL";

  /** Constante pour une clé qui n'offre pas de lien */
  public static final String CLE_AUCUNE = "AUCUNE";

  /** Un objet polymorphe contenant le détail de la ligne */
  Object objetDetail;

  /** Un objet rapport contenant le sous-rapport relié à cette ligne */
  Rapport rapportDetail;

  /** L'entête de la ligne */
  String entete;

  /** Le type de clé de la ligne */
  String typeCle;

  /** La clé de la ligne */
  int cle;

  /** Un booléen indiquant si on doit afficher le rapport de détail de la ligne */
  boolean afficherDetail;

  /** Une liste contenant les cellules de la ligne */
  ArrayList cellules;

  /** Obtenir le mapping pour les liens faits avec Struts */
  HashMap mapping;

  /** Le numéro de la colonne selon laquelle le tri est effectué */
  int noColonneTri;

  /** Le constructeur par défaut de la ligne. */
  public RapportLigne() {
    this.cle = -1;
    this.objetDetail = null;
    this.rapportDetail = null;
    this.entete = null;
    this.afficherDetail = false;
    this.typeCle = CLE_AUCUNE;
    this.cellules = new ArrayList();
  }

  /** Permet d'invers la valeur booléenne de l'affichage du détail de la ligne. */
  public void changerAfficherDetail() {
    afficherDetail = !afficherDetail;
  }

  /**
   * Obtenir l'indicateur d'affichage du détail de la ligne.
   * <p>
   * @return boolean l'indicateur d'affichage du détail de la ligne
   */
  public boolean getAfficherDetail() {
    return afficherDetail;
  }

  /**
   * Ajouter une valeur à la ligne.
   * <p>
   * @param valeur la valeur à ajouter
   */
  public void addValeur(Object valeur) {
    cellules.add(valeur);
  }

  /**
   * Fixer une valeur dans la ligne selon le numéro de la colonne.
   * <p>
   * @param valeur la valeur a fixer
   * @param noColonne le numéro de la colonne à fixer
   */
  public void setValeur(Object valeur, int noColonne) {
    cellules.set(noColonne, valeur);
  }

  /**
   * Obtenir une valeur dans la ligne selon le numéro de la colonne.
   * <p>
   * @param noColonne le numéro de la colonne désirée
   * @return Object la valeur demandée
   */
  public Object getValeur(int noColonne) {
    try {
      return cellules.get(noColonne);
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * Obtenir la liste des cellules de la ligne.
   * <p>
   * @return ArrayList la liste des cellules de la ligne
   */
  public ArrayList getCellules() {
    return cellules;
  }

  /**
   * Fixer la liste des cellules de la ligne.
   * <p>
   * @param newCellules la liste des cellules de la ligne
   */
  public void setCellules(ArrayList newCellules) {
    cellules = newCellules;
  }

  /**
   * Obtenir l'objet contenant le détail de la ligne.
   * <p>
   * @return Object l'objet contenant le détail de la ligne
   */
  public Object getObjetDetail() {
    return objetDetail;
  }

  /**
   * Fixer l'objet contenant le détail de la ligne.
   * <p>
   * @param newObjetDetail l'objet contenant le détail de la ligne
   */
  public void setObjetDetail(Object newObjetDetail) {
    objetDetail = newObjetDetail;
  }

  /**
   * Obtenir le rapport du sous-niveau de cette ligne.
   * <p>
   * @return Rapport le rapport du sous-niveau de cette ligne
   */
  public Rapport getRapportDetail() {
    return rapportDetail;
  }

  /**
   * Fixer le rapport du sous-niveau de cette ligne.
   * <p>
   * @param newRapportDetail le rapport du sous-niveau de cette ligne
   */
  public void setRapportDetail(Rapport newRapportDetail) {
    rapportDetail = newRapportDetail;
  }

  /**
   * Obtenir l'entête de la ligne.
   * <p>
   * @return String l'entête de la ligne
   */
  public String getEntete() {
    return entete;
  }

  /**
   * Fixer l'entête de la ligne.
   * <p>
   * @param newEntete l'entête de la ligne
   */
  public void setEntete(String newEntete) {
    entete = newEntete;
  }

  /**
   * Obtenir la clé de la ligne.
   * <p>
   * @return int la clé de la ligne
   */
  public int getCle() {
    return cle;
  }

  /**
   * Fixer la clé de la ligne.
   * <p>
   * @param newCle la nouvelle clé de la ligne
   */
  public void setCle(int newCle) {
    cle = newCle;
  }

  /** Fixer le type de clé de la ligne comme une clé menant vers une page de détail. */
  public void setAvecDetailAutrePage() {
    typeCle = CLE_AUTRE_PAGE;
  }

  /** Fixer le type de clé de la ligne comme une clé servant à éclater un niveau supplémentaire. */
  public void setAvecDetailMemePage() {
    typeCle = CLE_DETAIL;
  }

  /**
   * Obtenir le mapping pour les liens faits avec Struts.
   * <p>
   * @return HashMap le mapping pour les liens faits avec Struts
   */
  public HashMap getMapping() {
    mapping = new HashMap();
    mapping.put("cle", String.valueOf(getCle()));

    return mapping;
  }

  /**
   * Obtenir le type de clé.
   * <p>
   * @return String le type de clé
   */
  public String getTypeCle() {
    return typeCle;
  }

  /**
   * Fixer le type de clé.
   * <p>
   * @param newTypeCle le type de clé
   */
  public void setTypeCle(String newTypeCle) {
    typeCle = newTypeCle;
  }

  /**
   * Obtenir le numéro de colonne selon laquelle le tri est effectué.
   * <p>
   * @return int le numéro de colonne selon laquelle le tri est effectué
   */
  public int getNoColonneTri() {
    return noColonneTri;
  }

  /**
   * Fixer le numéro de colonne selon laquelle le tri est effectué.
   * <p>
   * @param newNoColonneTri le numéro de colonne selon laquelle le tri est effectué
   */
  public void setNoColonneTri(int newNoColonneTri) {
    noColonneTri = newNoColonneTri;
  }

  /**
   * Obtenir la valeur selon laquelle le tri est effectué.
   * <p>
   * @return Object la valeur selon laquelle le tri est effectué
   */
  public Object getValeurTri() {
    return this.getValeur(this.getNoColonneTri());
  }
}
