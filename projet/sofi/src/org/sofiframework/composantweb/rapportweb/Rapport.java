/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.rapportweb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


/**
 * Objet permettant la gestion de rapports multi-niveaux.
 * <p>
 * Cet objet permet de gérer des rapports à plusieurs niveaux et de pouvoir
 * gérer le niveau d'éclatement de chacun des niveaux hiérarchique.
 * <p>
 * Notons que ce rapport utilise la récursivité permettant ainsi la multitude des niveaux.
 * <p>
 * Notons de plus que ce rapport utilise la notion du polymorphisme afin de pouvoir
 * stocker un objet de tout type dans chacune des cellules
 * <p>
 * @author Steve Tremblay
 * @version 1.1
 * @see org.sofiframework.composantweb.rapportweb.RapportLigne
 */
public class Rapport implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -7966677108423055406L;

  /** Constante représentant le rapport pour la mise en mémoire dans la session */
  public static final String RAPPORT = "RAPPORT";

  /** Le nombre de lignes dans ce niveau du rapport */
  int no_ligne;

  /** Le contenu de ce niveau rapport */
  HashMap rapport;

  /** Les entêtes de chacune des colonnes du rapport */
  ArrayList enteteColonnes;

  /** Une ligne contenant les totaux du niveau */
  RapportLigne total;

  /** Un objet servant à stocker les paramètres de création du rapport */
  Object objetParametres;

  /** Permet d'associer un titre général au rapport */
  String titreRapport;

  /** Le numéro de la conlone selon laquelle le rapport doit être trié */
  int noColonneTri;

  /** Le constructeur par défaut du rapport */
  public Rapport() {
    this.no_ligne = 0;
    this.enteteColonnes = new ArrayList();
    this.rapport = new HashMap();
    this.total = null;
    this.noColonneTri = 0;
  }

  /**
   * Obtenir la ligne des totaux de ce niveau du rapport.
   * <p>
   * @return RapportLigne la ligne des totaux de ce niveau du rapport
   */
  public RapportLigne getTotal() {
    return total;
  }

  /**
   * Fixer la ligne des totaux de ce niveau du rapport.
   * <p>
   * @param newTotal la ligne des totaux de ce niveau du rapport
   */
  public void setTotal(RapportLigne newTotal) {
    total = newTotal;
  }

  /**
   * Calculer le total d'une colonne.
   * <p>
   * Cette méthode calcule le total d'une colonne à condition que chacune
   * des cellules contienne une valeur qui peut être convertie en double.
   * <p>
   * Si une des cellules ne répond pas a ce prérequis, une chaine de caractères
   * vide est retournée
   * <p>
   * @return String le total de la colonne en format texte
   */
  public String calculerTotalColonne(int colonne) {
    try {
      Iterator iterateurLignes = this.rapport.values().iterator();
      int compteurLignes = 0;
      double valeurTotal = 0;

      while (iterateurLignes.hasNext()) {
        RapportLigne ligne = (RapportLigne) iterateurLignes.next();
        Double valeurLigne = Double.valueOf(ligne.getValeur(colonne).toString());
        valeurTotal = valeurTotal + valeurLigne.doubleValue();
        compteurLignes++;
      }

      String stringTotal = String.valueOf(valeurTotal);

      if (stringTotal.endsWith(".0")) {
        stringTotal = stringTotal.substring(0, stringTotal.indexOf("."));
      }

      return stringTotal;
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * Calculer le total de toutes les colonnes.
   * <p>
   * Cette méthode calcule grâce à la méthode calculerTotalColonne(). Les résultats
   * obtenus sont insérés dans la variable total qui contient la ligne des totaux
   * de ce niveau du rapport
   * <p>
   * @param titreTotaux le titre que la ligne des totaux doit prendre (inséré automatiquement dans la première colonne)
   * @see #calculerTotalColonne(int colonne)
   */
  public void calculerTotaux(String titreTotaux) {
    this.total = new RapportLigne();

    Iterator iterateurColonnes = this.enteteColonnes.iterator();
    int compteurColonnes = 0;

    while (iterateurColonnes.hasNext()) {
      iterateurColonnes.next();

      if (compteurColonnes == 0) {
        total.addValeur(titreTotaux);
      } else {
        total.addValeur(this.calculerTotalColonne(compteurColonnes));
      }

      compteurColonnes++;
    }
  }

  /**
   * Ajouter une entête de colonne.
   * <p>
   * Cette méthode ajoute à la liste des entetes une nouvelle entête. Il est important
   * d'ajouter une entête pour chacune des colonnes du rapport. En effet, ce sont
   * ces entêtes qui déterminent le nombre de colonnes du rapport pour le calcul
   * des totaux.
   * <p>
   * @param entete l'entête à ajouter
   */
  public void ajouterEnteteColonne(String entete) {
    enteteColonnes.add(entete);
  }

  /**
   * Ajouter une ligne au rapport.
   * <p>
   * @param ligne la ligne à ajouter
   */
  public void ajouterLigne(RapportLigne ligne) {
    ligne.setCle(this.no_ligne);
    ligne.setNoColonneTri(this.getNoColonneTri());
    rapport.put(new Integer(this.no_ligne), ligne);
    this.no_ligne++;
  }

  /**
   * Obtenir l'index d'une colonne par son nom.
   * <p>
   * @param colonne le nom de la colonne dont on désire l'index
   * @return int l'index de la colonne ou -1 si elle n'existe pas
   */
  public int getColonneByName(String colonne) {
    return enteteColonnes.indexOf(colonne);
  }

  /**
   * Fixer une valeur dans une cellule selon le numéro de ligne et le numéro de colonne.
   * <p>
   * @param noLigne   le numéro de la ligne
   * @param noColonne le numéro de la colonne
   * @param valeur    un objet polymorphe contenant la valeur à fixer
   */
  public void setValeur(int noLigne, int noColonne, Object valeur) {
    RapportLigne ligne = (RapportLigne) rapport.get(new Integer(noLigne));
    ligne.setValeur(valeur, noColonne);
  }

  /**
   * Fixer une valeur dans une cellule selon le numéro de ligne et le nom de la colonne.
   * <p>
   * @param noLigne le numéro de la ligne
   * @param colonne le nom de la colonne
   * @param valeur  un objet polymorphe contenant la valeur à fixer
   */
  public void setValeur(int noLigne, String colonne, Object valeur) {
    RapportLigne ligne = (RapportLigne) rapport.get(new Integer(noLigne));
    ligne.setValeur(valeur, this.getColonneByName(colonne));
  }

  /**
   * Obtenir une valeur dans une cellule selon le numéro de ligne et le numéro de la colonne.
   * <p>
   * @param noLigne   le numéro de la ligne
   * @param noColonne le numéro de la colonne
   * @return Object   un objet polymorphe contenant la valeur de la cellule
   */
  public Object getValeur(int noLigne, int noColonne) {
    RapportLigne ligne = (RapportLigne) rapport.get(new Integer(noLigne));

    return ligne.getValeur(noColonne);
  }

  /**
   * Obtenir une valeur dans une cellule selon le numéro de ligne et le nom de la colonne.
   * <p>
   * @param noLigne   le numéro de la ligne
   * @param colonne   le nom de la colonne
   * @return Object   un objet polymorphe contenant la valeur de la cellule
   */
  public Object getValeur(int noLigne, String colonne) {
    RapportLigne ligne = (RapportLigne) rapport.get(new Integer(noLigne));

    return ligne.getValeur(this.getColonneByName(colonne));
  }

  /**
   * Obtenir le nombre de lignes dans ce niveau du rapport.
   * <p>
   * @return int le nombre de lignes dans ce niveau du rapport
   */
  public int getNombreDeLigne() {
    int nb_ligne = no_ligne;

    return nb_ligne;
  }

  /**
   * Obtenir une collection contenant les données du rapport.
   * <p>
   * @return Collection la collection contenant les données du rapport
   */
  public Collection getRapport() {
    ArrayList rapportDansLeBonOrdre = new ArrayList();

    for (int i = 0; i < this.getNombreDeLigne(); i++) {
      rapportDansLeBonOrdre.add(this.getLigne(new Integer(i)));
    }

    return rapportDansLeBonOrdre;
  }

  /**
   * Obtenir la liste des entêtes des colonnes.
   * <p>
   * @return ArrayList la liste des entêtes de colonnes
   */
  public ArrayList getEnteteColonnes() {
    return enteteColonnes;
  }

  /**
   * Obtenir le nombre de colonnes de ce niveau du rapport.
   * <p>
   * @return int le nombre de colonnes de ce niveau du rapport
   */
  public int getNombreDeColonnes() {
    return enteteColonnes.size();
  }

  /**
   * Fixer la liste des entêtes des colonnes.
   * <p>
   * @param newEnteteColonnes la liste des entêtes de colonnes
   */
  public void setEnteteColonnes(ArrayList newEnteteColonnes) {
    enteteColonnes = newEnteteColonnes;
  }

  /**
   * Obtenir une ligne selon son numéro.
   * <p>
   * @return RapportLigne la ligne correspondant au numéro demandé
   */
  public RapportLigne getLigne(Integer index) {
    return (RapportLigne) rapport.get(index);
  }

  /**
   * Obtenir l'objet contenant les paramètres de création du rapport.
   * <p>
   * @return Object l'objet contenant les paramètres de création du rapport
   */
  public Object getObjetParametres() {
    return objetParametres;
  }

  /**
   * Fixer l'objet contenant les paramètres de création du rapport.
   * <p>
   * @param newObjetParametres l'objet contenant les paramètres de création du rapport
   */
  public void setObjetParametres(Object newObjetParametres) {
    objetParametres = newObjetParametres;
  }

  /**
   * Obtenir le titre du rapport.
   * <p>
   * @return le titre du rapport
   */
  public String getTitreRapport() {
    return titreRapport;
  }

  /**
   * Fixer le titre du rapport.
   * <p>
   * @param newTitreRapport le titre du rapport
   */
  public void setTitreRapport(String newTitreRapport) {
    titreRapport = newTitreRapport;
  }

  /**
   * Obtenir le rapport trié de façon alphabétique.
   * <p>
   * @return La collection contenant les lignes triées de façon alphabétique
   */
  public Collection getRapportTrieAlphabetique() {
    Collection listeRapport = this.getRapport();
    Tri tri = new Tri();
    Collection listeRapportRetour = tri.triParValeur(listeRapport);

    return listeRapportRetour;
  }

  /**
   * Obtenir le numéro de la colonne selon laquelle le tri s'effectue.
   * <p>
   * @return Object le numéro de la colonne selon laquelle le tri s'effectue
   */
  public int getNoColonneTri() {
    return noColonneTri;
  }

  /**
   * Fixer le numéro de la colonne selon laquelle le tri s'effectue.
   * <p>
   * @param newNoColonneTri le numéro de la colonne selon laquelle le tri s'effectue
   */
  public void setNoColonneTri(int newNoColonneTri) {
    noColonneTri = newNoColonneTri;
  }
}
