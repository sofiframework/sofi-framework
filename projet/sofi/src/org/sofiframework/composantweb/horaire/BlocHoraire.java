/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.horaire;


/**
 * Objet servant à représenter un bloc dans la gestion d'un horaire.
 * <p>
 * Un bloc peut contenir de 1 à 60 minutes selon la façon dont l'objet
 * horaire a été créé intialement.
 * <p>
 * De façon interne, l'heure de début et de fin du bloc sont représentées
 * par un entier à quatre chiffres (ex : 855 pour 8h55, 2100 pour 21h00).
 * <p>
 * Notons que cette classe est utilisée automatiquement par la classe
 * Horaire et n'a donc pas à être gérée par l'utilisateur outre pour
 * l'affichage.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.composantweb.horaire.Horaire
 * @see org.sofiframework.composantweb.horaire.LigneHoraire
 */
public class BlocHoraire {
  /** L'heure de début du bloc */
  private int heureDebut;

  /** L'heure de fin du bloc */
  private int heureFin;

  /** Indique si le bloc est en conflit (deux éléments ont tenté d'être fixés) */
  private boolean enConflit;

  /** L'élément de détail polymorphe */
  private Object element;

  /**
   * Constructeur servant à créer un bloc selon son heure de début et de fin.
   * <p>
   * @param heureDebut l'heure de début du bloc
   * @param heureFin l'heure de fin du bloc
   */
  public BlocHoraire(int heureDebut, int heureFin) {
    this.setHeureDebut(heureDebut);
    this.setHeureFin(heureFin);
  }

  /**
   * Obtenir l'heure de début de la ligne.
   * <p>
   * @return l'heure de début de la ligne
   */
  public int getHeureDebut() {
    return heureDebut;
  }

  /**
   * Fixer l'heure de début de la ligne.
   * <p>
   * @param newHeureDebut l'heure de début de la ligne
   */
  public void setHeureDebut(int newHeureDebut) {
    heureDebut = newHeureDebut;
  }

  /**
   * Obtenir l'heure de fin de la ligne.
   * <p>
   * @return l'heure de fin de la ligne
   */
  public int getHeureFin() {
    return heureFin;
  }

  /**
   * Fixer l'heure de fin de la ligne.
   * <p>
   * @param newHeureFin l'heure de fin de la ligne
   */
  public void setHeureFin(int newHeureFin) {
    heureFin = newHeureFin;
  }

  /**
   * Obtenir l'indicateur de bloc en conflit.
   * <p>
   * @return l'indicateur de bloc en conflit
   */
  public boolean isEnConflit() {
    return enConflit;
  }

  /**
   * Fixer l'indicateur de bloc en conflit.
   * <p>
   * @param newEnConflit l'indicateur de bloc en conflit
   */
  public void setEnConflit(boolean newEnConflit) {
    enConflit = newEnConflit;
  }

  /**
   * Obtenir l'élément de détail polymorphe.
   * <p>
   * @return l'élément de détail polymorphe
   */
  public Object getElement() {
    return element;
  }

  /**
   * Fixer l'élément de détail polymorphe.
   * <p>
   * Si un élément est déjà présent, le bloc sera indiqué en conflit.
   * <p>
   * @param newElement l'élément de détail polymorphe
   */
  public void setElement(Object newElement) {
    if (element != null) {
      this.setEnConflit(true);
    } else {
      element = newElement;
    }
  }
}
