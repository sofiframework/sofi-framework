/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.horaire;

import java.util.ArrayList;
import java.util.Iterator;

import org.sofiframework.utilitaire.UtilitaireDate;


/**
 * Objet permettant la gestion d'une horaire dans une journée.
 * <p>
 * Cet objet permet de gérer l'horaire d'une journée en permettant de diviser
 * chaque heure en un nombre de bloc variable (de 1 à 60 minutes).
 * <p>
 * L'horaire se charge de positionner lui-même un objet de détail polymorphe
 * appelé "element" qui contient des informations sur ce qui est placé à l'horaire.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.composantweb.horaire.BlocHoraire
 * @see org.sofiframework.composantweb.horaire.LigneHoraire
 */
public class Horaire {
  /** La liste des différentes lignes de l'horaire (une ligne équivaut à une heure) */
  private ArrayList listeLignesHoraire;

  /** La date de la journée représentée par l'horaire */
  private String dateJournee;

  /**
   * Construit un nouveau calendrier selon certain paramètres.
   * <p>
   * Exemple : Pour construire un horaire de réservation de 8h à 23h avec des
   * blocs de quinze minutes, on créera l'horaire de la façon suivante :
   * <p>
   * <code>Horaire horaire = new Horaire(8, 23, 15);</code>
   * <p>
   * @param heureDebut l'heure de début de la journée
   * @param heureFin l'heure de fin de la journée
   * @param nbMinutesBloc le nombre de minutes dans un bloc de l'horaire
   */
  public Horaire(int heureDebut, int heureFin, int nbMinutesBloc) {
    ArrayList listeLignes = new ArrayList();

    for (int i = heureDebut; i < heureFin; i++) {
      LigneHoraire ligne = new LigneHoraire(i, i + 1);
      ArrayList listeBlocs = new ArrayList();

      for (int j = i * 100; j < ((i * 100) + 60); j = j + nbMinutesBloc) {
        BlocHoraire bloc = new BlocHoraire(j, j + nbMinutesBloc);
        listeBlocs.add(bloc);
      }

      ligne.setListeBlocs(listeBlocs);
      listeLignes.add(ligne);
    }

    this.setListeLignesHoraire(listeLignes);
  }

  /**
   * Obtenir la liste des lignes de l'horaire.
   * <p>
   * @return la liste des lignes de l'horaire
   */
  public ArrayList getListeLignesHoraire() {
    return listeLignesHoraire;
  }

  /**
   * Fixer la liste des lignes de l'horaire.
   * <p>
   * @param newListeLignesHoraire la liste des lignes de l'horaire
   */
  public void setListeLignesHoraire(ArrayList newListeLignesHoraire) {
    listeLignesHoraire = newListeLignesHoraire;
  }

  /**
   * Obtenir le nombre de blocs dans une ligne.
   * <p>
   * @return le nombre de blocs dans une ligne
   */
  public int getNombreBlocsParLigne() {
    LigneHoraire ligne = (LigneHoraire) listeLignesHoraire.get(0);

    return ligne.getListeBlocs().size();
  }

  /**
   * Placer un élément de détail dans l'horaire.
   * <p>
   * L'élément est polymorphe et peut donc être de tout type objet.
   * <p>
   * Exemple : Pour placer une chaîne de caractère contenant le détail textuel
   * d'une réservation de 8:00 à 9:15, on fera l'appel suivant :
   * <p>
   * <code>horaire.placerElement("8:00", "9:15", "Réservation de Steve Tremblay");</code>
   * @param heureDebut l'heure de début de l'élément à placer à l'horaire
   * @param heureFin l'heure de fin de l'élément à placer à l'horaire
   * @param element l'élément polymorphe qui contient le détail
   */
  public void placerElement(String heureDebut, String heureFin, Object element) {
    Iterator iterateurLignes = this.getListeLignesHoraire().iterator();

    while (iterateurLignes.hasNext()) {
      LigneHoraire ligne = (LigneHoraire) iterateurLignes.next();

      int heureDebutNum = new Integer(heureDebut.substring(0, 2) +
          heureDebut.substring(3, 5)).intValue();
      int heureFinNum = new Integer(heureFin.substring(0, 2) +
          heureFin.substring(3, 5)).intValue();

      if ((((ligne.getHeureDebut() * 100) <= heureDebutNum) &&
          ((ligne.getHeureFin() * 100) > heureDebutNum)) ||
          (((ligne.getHeureDebut() * 100) < heureFinNum) &&
              ((ligne.getHeureFin() * 100) > heureFinNum)) ||
              (((ligne.getHeureDebut() * 100) > heureDebutNum) &&
                  ((ligne.getHeureFin() * 100) <= heureFinNum))) {
        Iterator iterateurBlocs = ligne.getListeBlocs().iterator();

        while (iterateurBlocs.hasNext()) {
          BlocHoraire bloc = (BlocHoraire) iterateurBlocs.next();

          if (((bloc.getHeureDebut() <= heureDebutNum) &&
              (bloc.getHeureFin() > heureDebutNum)) ||
              ((bloc.getHeureDebut() < heureFinNum) &&
                  (bloc.getHeureFin() > heureFinNum)) ||
                  ((bloc.getHeureDebut() > heureDebutNum) &&
                      (bloc.getHeureFin() <= heureFinNum))) {
            bloc.setElement(element);
          }
        }
      }
    }
  }

  /**
   * Obtenir la date de la journée représentée par l'horaire.
   * <p>
   * Le format préconisé est aaaa-mm-jj (ex: 2004-05-01)
   * @return la date de la journée représentée par l'horaire.
   */
  public String getDateJournee() {
    return dateJournee;
  }

  /**
   * Fixer la date de la journée représentée par l'horaire.
   * <p>
   * Le format préconisé est aaaa-mm-jj (ex: 2004-05-01)
   * @param newDateJournee la date de la journée représentée par l'horaire.
   */
  public void setDateJournee(String newDateJournee) {
    dateJournee = newDateJournee;
  }

  /**
   * Obtenir la date de la journée représentée par l'horaire en format JJ Mois au long (ex: 4 mai).
   * <p>
   * @return la date en format JJ Mois
   */
  public String getDateJourneeLongueSansAnnee() {
    String dateCourte = dateJournee.substring(8, 10) + " " +
        UtilitaireDate.convertir_MM_En_MONTH(new Integer(dateJournee.substring(
            5, 7)).intValue());

    if (dateCourte.startsWith("0")) {
      dateCourte = dateCourte.substring(1);
    }

    return dateCourte;
  }
}
