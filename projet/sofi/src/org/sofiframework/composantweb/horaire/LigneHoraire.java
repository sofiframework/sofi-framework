/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.horaire;

import java.util.ArrayList;


/**
 * Objet servant à représenter une ligne dans la gestion d'un horaire.
 * <p>
 * Une ligne correspond à une heure de la journée et est donc caractérisée
 * par une heure de début (entière) et une heure de fin (entier).
 * <p>
 * Notons que cette classe est utilisée automatiquement par la classe
 * Horaire et n'a donc pas à être gérée par l'utilisateur outre pour
 * l'affichage.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.composantweb.horaire.Horaire
 * @see org.sofiframework.composantweb.horaire.BlocHoraire
 */
public class LigneHoraire {
  /** La liste des blocs contenus par l'heure */
  private ArrayList listeBlocs;

  /** L'heure de début de la ligne **/
  private int heureDebut;

  /** L'heure de fin de la ligne **/
  private int heureFin;

  /**
   * Constructeur servant à créer une ligne selon son heure de début et de fin.
   * <p>
   * @param heureDebut l'heure de début de la ligne
   * @param heureFin l'heure de fin de la ligne
   */
  public LigneHoraire(int heureDebut, int heureFin) {
    this.setHeureDebut(heureDebut);
    this.setHeureFin(heureFin);
  }

  /**
   * Obtenir la liste des blocs de la ligne.
   * <p>
   * @return la liste des blocs de la ligne
   */
  public ArrayList getListeBlocs() {
    return listeBlocs;
  }

  /**
   * Fixer la liste des blocs de la ligne.
   * <p>
   * @param newListeBlocs la liste des blocs de la ligne
   */
  public void setListeBlocs(ArrayList newListeBlocs) {
    listeBlocs = newListeBlocs;
  }

  /**
   * Obtenir l'heure de début de la ligne.
   * <p>
   * @return l'heure de début de la ligne
   */
  public int getHeureDebut() {
    return heureDebut;
  }

  /**
   * Fixer l'heure de début de la ligne.
   * <p>
   * @param newHeureDebut l'heure de début de la ligne
   */
  public void setHeureDebut(int newHeureDebut) {
    heureDebut = newHeureDebut;
  }

  /**
   * Obtenir l'heure de fin de la ligne.
   * <p>
   * @return l'heure de fin de la ligne
   */
  public int getHeureFin() {
    return heureFin;
  }

  /**
   * Fixer l'heure de fin de la ligne.
   * <p>
   * @param newHeureFin l'heure de fin de la ligne
   */
  public void setHeureFin(int newHeureFin) {
    heureFin = newHeureFin;
  }

  /**
   * Obtenir l'heure de début de la ligne en format 99h99 (ex: 08h00).
   * <p>
   * @return l'heure de début de la ligne en format 99h99
   */
  public String getHeureDebutAffichage() {
    String heureAffichage = String.valueOf(heureDebut) + "h00";

    if (heureAffichage.length() == 4) {
      heureAffichage = "0" + heureAffichage;
    }

    return heureAffichage;
  }
}
