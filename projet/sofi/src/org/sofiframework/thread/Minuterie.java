/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.thread;

import org.sofiframework.exception.SOFIException;


/**
 * Classe qui ermet d'exécuter un traitemet (implémente Runnable) par
 * l'intermédiaie d'un tread.
 * <p>
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version
 */
public class Minuterie {
  public Minuterie() {
  }

  /**
   * Exécut le traitement spécifié dans l'objet traitement.
   * @param traitement Objet qui implémente l'interface Runnable.
   * @param cl ClassLoader qui est requis pour l'exécution du traitement.
   * @param tempsMaximal Temps maximal qui doit durer le traitement.
   * Si le traitement dépasse le temps maximal une excetion sera lancée.
   * @throws org.sofiframework.exception.SOFIException Exception lors de l'xécution
   * du traitement.
   */
  public void executer(Runnable traitement, ClassLoader cl, long tempsMaximal)
      throws SOFIException {
    Thread t = new Thread(traitement);
    t.setContextClassLoader(cl);

    // démarrer le traitement
    /*    try {
          t.start();
        } catch (Exception ex) {
          t.interrupt();
          throw new SOFIException("Erreur lors de l'exécution de l'action minutée " +
            traitement.getClass().getName(), ex);
        } finally {
        }

        // attendre un certain temps pour que le traitement ce termine
        try {
          for (long tempsEcoule = 0L; t.isAlive() && (tempsEcoule <= tempsMaximal);) {
            Thread.currentThread().sleep(100L);
            tempsEcoule += 100L;
          }

          // Suite au temps maximal si le thread est encore en vie
          if (t.isAlive()) {
            t.interrupt();
            throw new SOFIException("l'action minutée prend trop de temps." +
              traitement.getClass().getName());
          } else {
          }
        } catch (InterruptedException ex) {
          t.interrupt();
          throw new SOFIException("Erreur lors de l'attente de l'action minutée " +
            traitement.getClass().getName(), ex);
        }*/
    t.start();

    try {
      t.join(tempsMaximal);
    } catch (InterruptedException e) {
      throw new SOFIException("Erreur lors de l'attente de l'action minutée " +
          traitement.getClass().getName(), e);
    } finally {
      if (t.isAlive()) {
        t.interrupt();
      }
    }
  }
}
