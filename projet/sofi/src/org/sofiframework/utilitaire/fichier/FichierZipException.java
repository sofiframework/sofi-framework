/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.fichier;


/**
 * Exception spécifiant qu'il y a eu une erreur lors d'un fichier zip.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class FichierZipException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 5352586992522096303L;

  /**
   * Constructeur de l'exception
   */
  public FichierZipException() {
    super();
  }

  /**
   * Constructeur à l'aide d'un message spécifique.
   * <p>
   * @param msg Message d'exception spécifique
   */
  public FichierZipException(String msg) {
    super(msg);
  }

  /**
   * Constructeur à l'aide d'un message ainsi que de la cause du l'exception.
   * <p>
   * @param msg Message d'exception spécifique
   * @param cause La cause de l'exception
   */
  public FichierZipException(String msg, Throwable cause) {
    super(msg, cause);
  }

  /**
   * Constructeur à l'aide d'une cause de l'exception.
   * <p>
   * @param cause La cause de l'exception
   */
  public FichierZipException(Throwable cause) {
    super(cause);
  }
}
