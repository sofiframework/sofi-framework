/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.fichier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.metadata.FormatDescription;
import org.sofiframework.utilitaire.metadata.FormatIdentification;


/**
 * Classe utilitaire pour traiter tous ce qui touche la manipulation
 * de fichier ou de répertoire.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireFichier {
  /**
   * Copier un répertoire dans un autre répertoire.
   * <p>
   * @param nomRepertoireSource Le répertoire source
   * @param nomRepertoireDestination Le répertoire de destination
   */
  public static void copierRepertoire(String nomRepertoireSource,
      String nomRepertoireDestination) {
    copierRepertoire(new File(nomRepertoireSource),
        new File(nomRepertoireDestination));
  }

  /**
   * Copier un répertoire dans un autre répertoire.
   * <p>
   * @param source Le répertoire source
   * @param destination Le répertoire de destination
   */
  public static void copierRepertoire(File source, File destination) {
    if (source.exists() && source.isDirectory()) {
      if (!destination.exists()) {
        destination.mkdirs();
      }

      File[] fileArray = source.listFiles();

      for (int i = 0; i < fileArray.length; i++) {
        if (fileArray[i].isDirectory()) {
          copierRepertoire(fileArray[i],
              new File(destination.getPath() + File.separator +
                  fileArray[i].getName()));
        } else {
          copierFichier(fileArray[i],
              new File(destination.getPath() + File.separator +
                  fileArray[i].getName()));
        }
      }
    }
  }

  /**
   * Copier un fichier dans un autre fichier.
   * <p>
   * @param nomFichierSource Le nom du fichier source
   * @param nomFichierDestination Le nom du fichier de destination
   */
  public static void copierFichier(String nomFichierSource,
      String nomFichierDestination) {
    copierFichier(new File(nomFichierSource), new File(nomFichierDestination));
  }

  /**
   * Copier un fichier dans un autre fichier.
   * <p>
   * @param source Le nom du fichier source
   * @param destination Le nom du fichier de destination
   */
  public static void copierFichier(File source, File destination) {
    if (!source.exists()) {
      return;
    }

    if ((destination.getParentFile() != null) &&
        (!destination.getParentFile().exists())) {
      destination.getParentFile().mkdirs();
    }

    try {
      FileInputStream fis = new FileInputStream(source);
      FileOutputStream fos = new FileOutputStream(destination);

      byte[] buffer = new byte[1024 * 4];
      int n = 0;

      while ((n = fis.read(buffer)) != -1) {
        fos.write(buffer, 0, n);
      }

      fis.close();
      fos.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Supprimer le contenu d'un répertoire.
   * <p>
   * @param repertoire Le nom du répertoire à supprimer
   */
  public static void supprimerRepertoire(String repertoire) {
    supprimerRepertoire(new File(repertoire));
  }

  /**
   * Supprimer le contenu d'un répertoire.
   * <p>
   * @param repertoire Le nom du répertoire à supprimer
   */
  public static void supprimerRepertoire(File repertoire) {
    if (repertoire.exists() && repertoire.isDirectory()) {
      File[] tableauFichier = repertoire.listFiles();

      for (int i = 0; i < tableauFichier.length; i++) {
        if (tableauFichier[i].isDirectory()) {
          supprimerRepertoire(tableauFichier[i]);
        } else {
          tableauFichier[i].delete();
        }
      }

      repertoire.delete();
    }
  }

  /**
   * Spécifie si le fichier ou le répertoire existe.
   * <p>
   * @param nomFichier Le nom du fihier ou du répertoire à vérifier
   * @return true si le fichier ou répertoire existe, false sinon
   */
  public static boolean exists(String nomFichier) {
    File fichier = new File(nomFichier);

    return fichier.exists();
  }

  /**
   * Retourne la liste des répertoires pour un répertoire donné.
   * <p>
   * @param nomRepertoire Le nom du répertoire où l'on veut la liste des répertoires
   * @return Une liste de répertoires contenue dans le répertoire spécifié
   * @throws java.io.IOException Si le répertoire spécifié n'est pas trouvé
   */
  public static String[] listeRepertoires(String nomRepertoire)
      throws IOException {
    return listeRepertoires(new File(nomRepertoire));
  }

  /**
   * Retourne la liste des répertoires pour un fichier de type répertoire.
   * <p>
   * @param fichier Le nom du répertoire où l'on veut la liste des répertoires
   * @return Une liste de répertoires contenue dans le répertoire spécifié
   * @throws java.io.IOException Si le répertoire spécifié n'est pas trouvé
   */
  public static String[] listeRepertoires(File fichier)
      throws IOException {
    List dirs = new ArrayList();

    File[] tableauFichier = fichier.listFiles();

    for (int i = 0; i < tableauFichier.length; i++) {
      if (tableauFichier[i].isDirectory()) {
        dirs.add(tableauFichier[i].getName());
      }
    }

    return (String[]) dirs.toArray(new String[0]);
  }

  /**
   * Retourne la liste des fichiers dans un répertoire.
   * <p>
   * @param nomRepertoire Le nom du répertoire où l'on veut la liste des fichiers
   * @return Une liste de fichiers contenu dans le répertoire spécifié
   * @throws java.io.IOException Si le répertoire spécifié n'est pas trouvé
   */
  public static String[] listerFichiers(String nomRepertoire)
      throws IOException {
    return listerFichiers(new File(nomRepertoire));
  }

  /**
   * Retourne la liste des fichiers contenu dans un fichier de type
   * répertoire.
   * <p>
   * @param fichier Le nom du répertoire où l'on veut la liste des fichiers
   * @return Une liste de fichiers contenu dans le répertoire spécifié
   * @throws java.io.IOException Si le répertoire spécifié n'est pas trouvé
   */
  public static String[] listerFichiers(File fichier) throws IOException {
    List fichiers = new ArrayList();

    File[] tableauFichier = fichier.listFiles();

    for (int i = 0; i < tableauFichier.length; i++) {
      if (tableauFichier[i].isFile()) {
        fichiers.add(tableauFichier[i].getName());
      }
    }

    return (String[]) fichiers.toArray(new String[0]);
  }

  /**
   * Permet de créer un répertoire.
   * <p>
   * @param nomRepertoire le nom du répertoire à créer
   */
  public static void creerRepertoire(String nomRepertoire) {
    File file = new File(nomRepertoire);
    file.mkdirs();
  }

  /**
   * Lire un fichier.
   * <p>
   * @param fichier le fichier que l'on veut lire
   * @return le contenu du fichier lu
   * @throws java.io.IOException Si le fichier n'existe pas ou n'est pas accessible
   */
  public static String lire(File fichier) throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(fichier));

    StringBuffer sb = new StringBuffer();
    String line = null;

    while ((line = br.readLine()) != null) {
      sb.append(line).append('\n');
    }

    br.close();

    return sb.toString().trim();
  }

  /**
   * Permet d'écrire un fichier avec un contenu provenant d'une chaine de
   * caractère.
   * <p>
   * Le format du fichier est UTF-8.
   * <p>
   * @param nomFichier Le nom du fichier où l'on veut écrire le contenu
   * @param contenuFichier Le contenu que l'on veut écrire
   * @throws java.lang.Exception Si une erreur est survenue durant l'écriture
   */
  public static void ecrireFichier(String nomFichier, String contenuFichier)
      throws Exception {
    try {
      BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
          new FileOutputStream(nomFichier), "UTF-8"));
      out.write(contenuFichier);
      out.close();
    } catch (IOException ie) {
      throw new IOException(ie.getMessage());
    }
  }

  /**
   * Renommer un fichier.
   * <p>
   * @param ancienNom Le nom de l'ancien fichier
   * @param nouveauNom Le nom du nouveau fichier
   * @return true si complété, false sinon
   */
  public static boolean renommerFichier(String ancienNom, String nouveauNom) {
    File ancienFichier = new File(ancienNom);
    File nouveauFichier = new File(nouveauNom);

    return ancienFichier.renameTo(nouveauFichier);
  }

  /**
   * Méthode qui sert convertir un fichier en tableau de byte.
   * <p>
   * Cette méthode se sert d'une <code>String</code> pointant vers le fichier afin de récupérer les
   * données à convertir en bytes. Il est très important de donner le chemin complet du fichier afin
   * de ne pas avoir une <code>FileNotFoundException</code>.
   * <p>
   * @param lienFichier le chemin d'accès complet vers le fichier à convertir
   * @return la suite de bytes représentant le contenu du fichier
   * @throws FileNotFoundException Exception lancée lorsque la méthode est incapable de trouver le fichier identifié par le paramètre d'entrée.
   * @throws IOException Exception lancée lorsque que la méthode est incapable de lire le fichier à convertir.
   */
  public static byte[] convertirFichierEnByte(String lienFichier)
      throws FileNotFoundException, IOException {
    byte[] tableauByte = null;

    // Lire le fichier pour remplir le tableau de bytes
    File fichier = new File(lienFichier);
    FileInputStream fileInputStream = null;

    try {
      fileInputStream = new FileInputStream(fichier);
      tableauByte = new byte[new Long(fichier.length()).intValue()];
      fileInputStream.read(tableauByte);
      fileInputStream.close();
    } catch (FileNotFoundException e) {
      throw e;
    } catch (IOException e) {
      fileInputStream.close();
      throw e;
    }

    return tableauByte;
  }

  /**
   * Permet de supprimer un fichier.
   * <p>
   * @param nomFichier le nom du fichier avec répertoire spécifique à supprimer.
   */
  public static void supprimerFichier(String nomFichier) {
    File file = new File(nomFichier);
    file.delete();
  }

  /**
   * Créer un fichier avec des données en byte.
   * @param donnees les donnes en byte.
   * @param nomFichier le nom du fichier.
   */
  public static void creerFichierAvecByte(String nomFichier, byte[] donnees) {
    FileOutputStream nouveauFichier = null;

    try {
      nouveauFichier = new FileOutputStream(nomFichier);
      nouveauFichier.write(donnees);
      nouveauFichier.flush();
    } catch (FileNotFoundException e) {
      throw new SOFIException(e);
    } catch (IOException e) {
      throw new SOFIException(e);
    } finally {
      if (nouveauFichier != null) {
        try {
          nouveauFichier.close();
        } catch (Exception e) {
          throw new SOFIException(e);
        }
      }
    }
  }

  /**
   * Valide de type d'un fichier
   * @param type Le type de fichier à valider
   * @param data Le fichier en bytes array
   * @return true si le format du fichier est identique à celui passé en paramètre
   */
  public static boolean isFormatValide(String type, byte[] data) {
    FormatDescription desc = FormatIdentification.identify(data);

    if ((desc != null) &&
        desc.getShortName().toLowerCase().equals(type.toLowerCase())) {
      return true;
    } else {
      return false;
    }
  }
}
