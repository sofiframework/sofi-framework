/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.fichier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet représentant un fichier séquentiel de texte.
 * <p>
 * Cet objet offre les outils nécessaire à l'écriture rapide d'un fichier séquentiel
 * contenant du texte simple. Il est important pour que le fichier se génère correctement
 * d'invoquer la méthode genererFichierTexte(), sinon le fichier ne s'écriera jamais sur
 * le disque dur.<br><br>
 * <b>Exemple :</b><br><code>
 * FichierTexte fichier = new FichierTexte("fichier_test", "C:/repertoire/sous_repertoire");<br>
 * fichier.ajouterLigne("Ligne1");<br>
 * fichier.ajouterLigne("Ligne2");<br>
 * fichier.genererFichierTexte();<br></code><br>
 * Il est très important lors de la déclaration du fichier d'utiliser le charactère "/" au lieu
 * du charactère "\" dans la destination du fichier, sinon le fichier ne se créera pas. Il n'est
 * pas obligatoire d'inscrite une extension au nom du fichier, dans le cas ou l'on ne spécifie
 * pas d'extension, le fichier prendra automatiquement l'extension ".txt".
 * <p>
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.0
 */
public class FichierTexte {
  /** Variable globale identifiant la valeur d'un retour de chariot dans le fichier séquentiel */
  private static final String RETOUR_CHARIOT = "\r\n";

  /** Le nom du fichier */
  private String nomFichier;

  /** Le <I>path</I> complet ou le fichier sera copié */
  private String destination;

  /** Le contenu du fichier */
  private StringBuffer contenu;

  /**
   * Constructeur.
   * <p>
   * Constructeur du fichier de texte, il est important d'y mentionner le nom du fichier
   * ainsi que la destination du fichier.<BR>
   * <B>Note :</B> Il ne faut pas utiliser le charactère "\" dans la destination, utilisez plutot
   * le charactère "/". Il ne faut pas mettre de "/" à la fin de la destination non plus.
   * <p>
   * @param nomFichier le nom du fichier
   * @param pathFichier le <I>path</I> complet où le fichier sera copié
   */
  public FichierTexte(String nomFichier, String pathFichier) {
    if (nomFichier.indexOf(".") == -1) {
      nomFichier = nomFichier + ".txt";
    }

    this.nomFichier = nomFichier;
    this.destination = pathFichier;

    if (this.isExiste()) {
      contenu = this.lireContenuFichierActuel();
    } else {
      contenu = new StringBuffer();
    }
  }

  /**
   * Crée un nouveau fichier Texte à l'aide du path absolu du fichier.
   * Dans ce cas la propriété destination est à "".
   * Exemple : c:\test\mon_fichier.xml
   * 
   * @param nomFichierPathAbsolu
   */
  public FichierTexte(String nomFichierPathAbsolu) {
    destination = "";

    this.nomFichier = nomFichierPathAbsolu;

    File fichier = new File(nomFichierPathAbsolu);

    if (fichier.exists()) {
      contenu = this.lireContenuFichierActuel();
    } else {
      contenu = new StringBuffer();
    }
  }

  /**
   * Obtenir le nom du fichier de texte
   * <p>
   * @return le nom du fichier de texte
   */
  public String getNomFichier() {
    return nomFichier;
  }

  /**
   * Fixer le nom du fichier de texte
   * <p>
   * @param newNomFichier le nom du fichier de texte
   */
  public void setNomFichier(String newNomFichier) {
    if (newNomFichier.indexOf(".") == -1) {
      newNomFichier = newNomFichier + ".txt";
    }

    nomFichier = newNomFichier;
  }

  /**
   * Obtenir la destination du fichier texte
   * <p>
   * @return la destination du fichier texte
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Fixer la destination du fichier texte
   * <p>
   * @param newDestination la destination du fichier texte
   */
  public void setDestination(String newDestination) {
    destination = newDestination;
  }

  /**
   * Obtenir le contenu du fichier texte
   * <p>
   * @return le contenu du fichier texte
   */
  public StringBuffer getContenu() {
    return contenu;
  }

  /**
   * Fixer le contenu du fichier texte
   * <p>
   * @param newContenu le contenu du fichier texte
   */
  public void setContenu(StringBuffer newContenu) {
    contenu = newContenu;
  }

  /**
   * Méthode qui indique si le fichier existe déjà.
   * <p>
   * Méthode qui vérifie si le fichier existe déjà dans le répertoire de destination.
   * <p>
   * @return valeur indiquant si le fichier existe
   */
  public boolean isExiste() {
    File fichier = new File(destination, nomFichier);

    return fichier.exists();
  }

  /**
   * Méthode qui ajoute une ligne au contenu du fichier.
   * <p>
   * Méthode qui ajoute une ligne au contenu du fichier. Cette méthode ajoute aussi un retour de
   * chariot après la ligne en question. Il n'est donc pas nécessaire d'ajouter un retour de
   * chariot manuellement.
   * <p>
   * @param ligne la ligne à ajouter au fichier
   */
  public void ajouterLigne(String ligne) {
    contenu.append(ligne);
    contenu.append(RETOUR_CHARIOT);
  }

  /**
   * Méthode qui écrit le fichier à sa destination.
   * <p>
   * Cette méthode est utilisée pour créer le fichier physique. En effet, avant d'invoquer cette méthode,
   * le fichier n'existe que logiquement et non pas physiquement. Ainsi lorsque l'on désire créer le
   * fichier, il suffit d'appeler cette méthode. Dans le cas où le fichier logique spécifié existe déjà,
   * cette méthode ajoutera à la fin du fichier les lignes du fichier logique.
   * <p>
   * @return Valeur indiquant si l'opération a réussie
   */
  public boolean genererFichierTexte() {
    boolean operationReussie = true;

    try {
      File fichier = new File(destination, nomFichier);
      File path = new File(destination);
      path.mkdirs();

      RandomAccessFile curseur = new RandomAccessFile(fichier, "rw");
      curseur.seek(curseur.length());
      curseur.writeBytes(contenu.toString());
      curseur.close();
    } catch (IOException e) {
      System.out.println(e.getMessage());
      operationReussie = false;
    }

    return operationReussie;
  }

  /**
   * Méthode qui dit le nombre de ligne dans le fichier.
   * <p>
   * Cette méthode retourne le nombre de ligne que le fichier séquentiel possède. Dans le cas
   * d'un fichier déjà existant, le nombre de lignes retourné correspond au nombre de ligne du
   * fichier existant additionné du nombre de lignes que possède le fichier logique. Dans le cas
   * d'un fichier inexistant, le nombre de lignes retourné correspond au nombre de ligne dans le
   * fichier physique
   * <p>
   * @return Le nombre de ligne du fichier séquentiel
   */
  public int getNombreLignes() {
    int nombreLignes = 1;

    if (this.isExiste()) {
      try {
        File fichier = new File(destination, nomFichier);
        RandomAccessFile curseur = new RandomAccessFile(fichier, "rw");

        while (curseur.readLine() != null) {
          nombreLignes++;
        }
      } catch (IOException e) {
        nombreLignes = 0;
      }

      String tampon = contenu.toString();
      while (tampon.indexOf(RETOUR_CHARIOT) != -1) {
        nombreLignes++;
        tampon = tampon.substring(tampon.indexOf(RETOUR_CHARIOT) +
            RETOUR_CHARIOT.length());
      }
    } else {
      String tampon = contenu.toString();

      while (tampon.indexOf(RETOUR_CHARIOT) != -1) {
        nombreLignes++;
        tampon = tampon.substring(tampon.indexOf(RETOUR_CHARIOT) +
            RETOUR_CHARIOT.length());
      }
    }

    return nombreLignes;
  }

  /**
   * Méthode qui dit le nombre de ligne dans le fichier physique.
   * <p>
   * Cette méthode retourne le nombre de ligne que le fichier séquentiel possède. Dans le cas
   * d'un fichier déjà existant, le nombre de lignes retourné correspond au nombre de ligne du
   * fichier existant. Dans le cas d'un fichier inexistant, le nombre de lignes retourné est 0
   * <p>
   * @return Le nombre de ligne du fichier
   */
  public int getNombreLignesFichierPhysique() {
    int nombreLignes = 0;

    if (this.isExiste()) {
      try {
        File fichier = new File(destination, nomFichier);
        RandomAccessFile curseur = new RandomAccessFile(fichier, "rw");

        while (curseur.readLine() != null) {
          nombreLignes++;
        }
      } catch (IOException e) {
        nombreLignes = 0;
      }
    } else {
      nombreLignes = 0;
    }

    return nombreLignes;
  }

  /**
   * Méthode qui lit le contenu du fichier actuel s'il existe déjà.
   * <p>
   * Cette méthode est appelée lors de la construction du fichier. Cette méthode
   * lit le contenu actuel du fichier existant et le retourne sous forme de StringBuffer
   * <p>
   * @return le contendu du fichier existant
   */
  private StringBuffer lireContenuFichierActuel() {
    StringBuffer contenuFichier = new StringBuffer();

    File fichier = null;

    if (!UtilitaireString.isVide(destination)) {
      fichier = new File(destination, nomFichier);
    } else {
      fichier = new File(nomFichier);
    }

    try {
      FileReader lecteurFichier = new FileReader(fichier);
      BufferedReader lecteur = new BufferedReader(lecteurFichier);
      String ligne = null;

      while ((ligne = lecteur.readLine()) != null) {
        contenuFichier.append(ligne);
      }

      lecteurFichier.close();
    } catch (FileNotFoundException ex) {
      System.out.println("Impossible de trouver le fichier " + destination +
          "\\" + nomFichier);
      ex.printStackTrace();
    } catch (IOException ex) {
      System.out.println("Erreur de lecture du fichier");
      ex.printStackTrace();
    }

    return contenuFichier;
  }
}
