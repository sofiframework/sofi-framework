/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.fichier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Un utilitaire pour faire des manipulations sur des fichiers jar, zip
 * war ou ear.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireFichierZip {
  /** Variable désignant le buffer maximum à utiliser */
  public static final int BUFFER = 2048;

  /** Variable désignant l'extension du fichier ZIP */
  public static final String ZIP_EXTENSION = ".zip";

  /** L'instance de journalisation pour cette classe */
  static private Log log = LogFactory.getLog(UtilitaireFichierZip.class);

  /** le chemin complet du fichier **/
  private String m_FilePath;

  /** le fichier compressé **/
  private JarFile m_JarFile;

  /**
   * Constructeur.
   * <p>
   * @param path Le chemin complet du fichier
   * @exception IOException Si une exception survient lors de la manipulation
   */
  public UtilitaireFichierZip(String path) throws IOException {
    m_FilePath = path;

    File f = new File(path);

    try {
      m_JarFile = new JarFile(f);
    } catch (IOException ioe) {
      log.fatal("IOException occurred " + f.getAbsolutePath());
      throw new IOException(ioe.getMessage());
    } catch (java.lang.NullPointerException e) {
      log.fatal("NullPointerException " + f.getAbsolutePath());
      throw new IOException(e.getMessage());
    }

    if (m_JarFile == null) {
      throw new IOException(" Le fichier source est null");
    }
  }

  /**
   * Permet de compresser un répertoire dans un fichier de type ZIP.
   * <p>
   * @param repertoirePourSauver Le nom du répertoire des fichiers que l'on veut compressé
   * @param fichierPrefixe Le nom frd fichier compressé que l'on veut filtrer
   * @param extensionStr L'extention des fichiers à filtrer lors de la compression
   * @param nomFichierZip Le nom du fichier compressé
   * @return La destination du fichier compressé
   * @throws java.lang.Exception Si une erreur survient lors de la compression
   */
  public String compresserRepertoire(String repertoirePourSauver,
      String fichierPrefixe, String extensionStr, String nomFichierZip)
          throws Exception {
    // Verifier si on a des fichiers autrement on quitte.
    File[] files = null;

    try {
      files = getListeFichier(repertoirePourSauver, fichierPrefixe, extensionStr);
    } catch (Exception e) {
      throw e;
    }

    if (files.length <= 0) {
      return null;
    }

    String destStr = repertoirePourSauver + fichierPrefixe + ZIP_EXTENSION;

    try {
      BufferedInputStream origin = null;
      FileOutputStream dest = new FileOutputStream(destStr);
      ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
      byte[] data = new byte[BUFFER];

      //
      for (int i = 0; i < files.length; i++) {
        log.info("Ajout: " + files[i]);

        FileInputStream fi = new FileInputStream(files[i]);
        origin = new BufferedInputStream(fi, BUFFER);

        ZipEntry entry = new ZipEntry(files[i].getAbsolutePath());
        out.putNextEntry(entry);

        int count;

        while ((count = origin.read(data, 0, BUFFER)) != -1) {
          out.write(data, 0, count);
        }

        origin.close();
      }

      out.close();
    } catch (Exception e) {
      throw new FichierZipException("Impossible de compresser le répertoire" +
          e);
    }

    return destStr;
  }

  /**
   * Décompresser à l'endroit spécifier.
   * <p>
   * @throws java.io.IOException Incapable de décompresser le fichier
   */
  public void unzip() throws IOException {
    try {
      File f = new File(m_FilePath);

      String parentPath = f.getParent() + File.separator;
      String path = null;

      FileInputStream fis = new FileInputStream(m_FilePath);
      BufferedInputStream bis = new BufferedInputStream(fis);
      ZipInputStream zis = new ZipInputStream(bis);
      ZipFile zf = new ZipFile(m_FilePath);
      ZipEntry ze = null;
      String zeName = null;

      try {
        while ((ze = zis.getNextEntry()) != null) {
          zeName = ze.getName();
          path = parentPath + genPathFile(zeName);

          File fo = new File(path);

          if (ze.isDirectory()) {
            fo.mkdirs();
          } else {
            copyStream(zis, new FileOutputStream(fo));
          }

          zis.closeEntry();
        }
      } finally {
        // Important !!!
        zf.close();
        fis.close();
        zis.close();
        bis.close();
      }
    } catch (IOException ioe) {
      log.fatal(" fail unzipping " + ioe.getMessage());

      throw new IOException("Echec de decompression:" + ioe.getMessage());
    }
  }

  /**
   * Décompresser pour un répertoire donné.
   * <p>
   * @param path Le nom du répertoire à décompresser
   * @throws java.io.IOException Si une erreur se produit lors de la décompression
   */
  public void unzip(String path) throws IOException {
    try {
      String destPath = null;

      FileInputStream fis = new FileInputStream(m_FilePath);
      BufferedInputStream bis = new BufferedInputStream(fis);
      ZipInputStream zis = new ZipInputStream(bis);
      ZipFile zf = new ZipFile(m_FilePath);
      ZipEntry ze = null;
      String zeName = null;

      try {
        while ((ze = zis.getNextEntry()) != null) {
          zeName = ze.getName();

          destPath = path + File.separator + genPathFile(zeName);

          File fo = new File(destPath);

          if (ze.isDirectory()) {
            fo.mkdirs();
          } else {
            File parent = new File(fo.getParent());
            parent.mkdirs();

            FileOutputStream fos = new FileOutputStream(fo);
            copyStream(zis, fos);
          }

          zis.closeEntry();
        }
      } finally {
        // Important de tout fermer
        zf.close();
        fis.close();
        zis.close();
        bis.close();
      }
    } catch (IOException ioe) {
      log.fatal(" fail unzipping " + ioe.getMessage());
      throw new IOException("Échec de traitement de décompressement" +
          ioe.getMessage());
    }
  }

  /**
   * Extraire un fichier qui est dans un fichier jar.
   * <p>
   * Retourne une référence sur fichier non compréssé.
   * <p>
   * @param nomFichier Le nom du fichier que l'on veut extraire du jar
   * @return un fichier temporaire décompressé
   * @throws java.io.IOException Incapable de décompresser le fichier ciblé
   * @throws org.sofiframework.exceptions.FichierZipException Exception générique
   */
  public File extraireFichier(String nomFichier)
      throws IOException, FichierZipException {
    File fichierTemporaire = null;

    // Créer un fichier temporaire et écrire dans ce fichier.
    ZipEntry fichier = m_JarFile.getEntry(nomFichier);

    if ((fichier != null) && !fichier.isDirectory()) {
      InputStream ins = m_JarFile.getInputStream(fichier);

      if (ins != null) {
        fichierTemporaire = File.createTempFile("nurun_temp", "", null);

        if ((fichierTemporaire == null) || !fichierTemporaire.canWrite()) {
          throw new IOException("impossible de créer un fichier temporaire");
        }

        FileOutputStream outs = new FileOutputStream(fichierTemporaire);
        copyStream(ins, outs);
        outs.flush();
        outs.close();
      }
    } else {
      log.fatal("extraireFichier(nomFichier) est null ou est un répertoire ");
      throw new FichierZipException(
          "extraireFichier(nomFichier) est null ou est un répertoire ");
    }

    return fichierTemporaire;
  }

  /**
   * Extraire un fichier pour un repertoire. Si ce fichier est un repertoire,
   * tout le contenu du repertoire est décompressé.
   * <p>
   * @param nomFichier Le nom du fichier ou répertoire à décompresser
   * @param repertoire La destination du fichier ou répertoire décompressé
   * @throws org.sofiframework.exceptions.FichierZipException Exception générique
   */
  public void extraireFichierPourRepertoire(String nomFichier, String repertoire)
      throws FichierZipException {
    try {
      if (nomFichier == null) {
        StringBuffer strBuf = new StringBuffer(1024);
        strBuf.append(
            " extraireFichierPourRepertoire(), ne trouve pas le fichier ou repertoire ");
        strBuf.append(nomFichier);
        strBuf.append(" dans le fichier zip ");

        throw new FichierZipException(strBuf.toString());
      }

      File destRepertoire = new File(repertoire);

      if ((destRepertoire == null) || !destRepertoire.isDirectory() ||
          !destRepertoire.canWrite()) {
        log.fatal("Incapable de trouvé la destination du repertoire.");

        throw new FichierZipException(
            " Incapable de trouvé la destination du repertoire.");
      }

      String path = null;

      FileInputStream fis = new FileInputStream(m_FilePath);
      BufferedInputStream bis = new BufferedInputStream(fis);
      ZipInputStream zis = new ZipInputStream(bis);
      ZipFile zf = new ZipFile(m_FilePath);
      ZipEntry ze = null;
      String zeName = null;

      while (((ze = zis.getNextEntry()) != null) &&
          !ze.getName().equalsIgnoreCase(nomFichier)) {
        zis.closeEntry();
      }

      try {
        if (ze.isDirectory()) {
          while (ze != null) {
            zeName = ze.getName();
            path = repertoire + File.separator + genPathFile(zeName);

            File fo = new File(path);

            if (ze.isDirectory()) {
              fo.mkdirs();
            } else {
              FileOutputStream outs = new FileOutputStream(fo);
              copyStream(zis, outs);
            }

            zis.closeEntry();
            ze = zis.getNextEntry();
          }
        } else {
          zeName = ze.getName();
          path = repertoire + File.separator + genPathFile(zeName);

          File fo = new File(path);
          FileOutputStream outs = new FileOutputStream(fo);
          copyStream(zis, outs);
        }
      } finally {
        // Important !!!
        zf.close();
        fis.close();
        zis.close();
        bis.close();
      }
    } catch (IOException ioe) {
      log.fatal(" incapable de décompresser" + ioe.getMessage());
      throw new FichierZipException(" incapable de décompresser.");
    }
  }

  /**
   * Retourne le Fichier (sous forme de ZipEntry) du fichier Zip, null si non trouvé.
   * <p>
   * @param nomFichier Le nom du fichier que l'on veut extraire
   * @return Le fichier ZipEntry, null si non trouvé
   */
  public ZipEntry getFichier(String nomFichier) {
    return m_JarFile.getEntry(nomFichier);
  }

  /**
   * Spécifie si le fichier est un repertoire ou non.
   * <p>
   * @param nomFichier Le fichier ou répertoire que l'on veut vérifier
   * @return true si c'est un fichier ou répertoire, false sinon
   */
  public boolean isRepertoire(String nomFichier) {
    return ((m_JarFile.getEntry(nomFichier) != null) &&
        m_JarFile.getEntry(nomFichier).isDirectory());
  }

  /**
   * Fermer le fichier zip.
   * <p>
   * Très important de fermer l'objet JarFile afin d'être capable de le supprimer du disque.
   */
  public void closeArchiveFile() {
    try {
      m_JarFile.close();
    } catch (IOException e) {
      log.fatal("incapable de fermer le fichier zip");
    }
  }

  /**
   * Générer un chemin pour le nom de fichier ou répertoire.
   * <p>
   * @param nomFichier Le nom du fichier ou répertoire à générer un chemin
   * @return Le chemin généré
   */
  protected String genPathFile(String nomFichier) {
    StringBuffer sb = new StringBuffer(nomFichier.length());

    for (int i = 0; i < nomFichier.length(); i++) {
      if (nomFichier.charAt(i) == '/') {
        sb.append(File.separator);
      } else {
        sb.append(nomFichier.charAt(i));
      }
    }

    return (sb.toString());
  }

  /**
   * Copier le InputStream dans le OutputStream.
   * <p>
   * @param ins L'inputstream du fichier
   * @param outs L'outputstream du fichier
   * @throws java.io.IOException Une erreur est survenue lors de la manipulation
   */
  protected void copyStream(InputStream ins, OutputStream outs)
      throws IOException {
    int bufferSize = 1024;
    byte[] writeBuffer = new byte[bufferSize];

    BufferedOutputStream bos = new BufferedOutputStream(outs, bufferSize);
    int bufferRead;

    while ((bufferRead = ins.read(writeBuffer)) != -1) {
      bos.write(writeBuffer, 0, bufferRead);
    }

    bos.flush();
    bos.close();
    outs.flush();
    outs.close();
  }

  /**
   * Obtenir liste de fichier pour zipper.
   * <p>
   * (Il est obligatoire d'en avoir au moins un car autrement erreur).
   * <p>
   * @param repertoirePourSauver le nom de repertoire de la generation
   * @param fichierPrefixe le prefixe des noms de fichier généré (.CSV, .WORD, etc.)
   * @param extensionStr l'extension des fichiers a prendre
   * @return File[] la liste de fichier desire
   * @throws Exception si une erreur se produit
   */
  private File[] getListeFichier(String repertoirePourSauver,
      String fichierPrefixe, String extensionStr) throws Exception {
    try {
      String filtrePropre = repertoirePourSauver + fichierPrefixe;
      FichierZipFiltre filtre = new FichierZipFiltre(filtrePropre, extensionStr);
      File f = new File(repertoirePourSauver);

      return f.listFiles(filtre);
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Classe interne de UtilitaireFichierZip qui permet de filtrer un certain
   * type de fichier débutant par un prefixe spécifique ou/et un extension
   * spécifique.
   * <p>
   * @author André Marcotte (Nurun inc.)
   * @version SOFI 1.0
   */
  public class FichierZipFiltre extends Object implements java.io.FileFilter {
    /** prefixe utilise pour filtrer */
    private String prefixeStr = null;

    /** extension utilise pour filtrer */
    private String extensionStr = null;

    /** Constructeur par defaut. */
    public FichierZipFiltre() {
    }

    /**
     * Creation d'instance.
     * <p>
     * @param prefixeStr prefixe au nom de fichier
     * @param extensionStr extension au nom de fichier
     */
    public FichierZipFiltre(String prefixeStr, String extensionStr) {
      this.prefixeStr = prefixeStr;
      this.extensionStr = extensionStr;
    }

    /**
     * Spécifie si le fichier est inclus dans un repertoire.
     * <p>
     * Les fichiers débutant par un point (.) sont ignorés.
     * <p>
     * @param f Le fichier à vérifier
     * @return true si le fichier est accepté, false sinon
     */
    @Override
    public boolean accept(File f) {
      if (f == null) {
        return false;
      }

      if (f.isDirectory() == true) {
        return false;
      }

      if (f.isFile() == false) {
        return false;
      }

      if (extensionStr == null) {
        if (prefixeStr == null) {
          return true;
        }

        if (f.getAbsolutePath().startsWith(prefixeStr) == true) {
          return true;
        }

        return false;
      }

      String fileExtension = getExtension(f);

      if (fileExtension != null) {
        if (fileExtension.equals(extensionStr) == true) {
          if (prefixeStr == null) {
            return true;
          }

          if (f.getAbsolutePath().startsWith(prefixeStr) == true) {
            return true;
          }

          return false;
        }
      }

      return false;
    }

    /**
     * Retourne l'extension d'un fichier spécifique.
     * <p>
     * @param f fichier que l'on veut extraire l'extension
     * @return l'extension
     */
    public String getExtension(File f) {
      if (f != null) {
        String filename = f.getName();
        int i = filename.lastIndexOf('.');

        if ((i > 0) && (i < (filename.length() - 1))) {
          return "." + filename.substring(i + 1).toLowerCase();
        }
      }

      return null;
    }
  }
}
