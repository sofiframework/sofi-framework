/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.StringTokenizer;


/**
 * Utilitaire de des clés des messages de tout type.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireCleMessage {
  /**
   * Retourne l'attribut en focus avec l'aide de l'identifiant du
   * message qui l'a plupart de temps est une message d'erreur.
   * @param identifiant
   * @return l'attribut en focus.
   */
  public static String getAttributEnFocus(String cleMessage) {
    if (cleMessage == null) {
      return null;
    }

    StringTokenizer motCleErreur = new StringTokenizer(cleMessage, ".");
    int i = 1;
    boolean trouve = false;

    while (motCleErreur.hasMoreTokens() && !trouve) {
      String motCle = motCleErreur.nextToken();

      if (i == 4) {
        return motCle;
      }

      i++;
    }

    return null;
  }
}
