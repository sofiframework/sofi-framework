/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Décorateur pour une instance de <code>java.util.Map</code> qui transforme
 * toutes les clés en majuscule. Les clés doivent donc être de
 * type <code>java.lang.String</code>.
 */
public class CaseInsensitiveMap implements Map {
  private Map delegue;

  /**
   * Décore l'instance de <code>Map</code> passé en paramètre.
   */
  public CaseInsensitiveMap(Map delegue) {
    this.delegue = delegue;
  }

  /**
   * Construit une <code>Map</code> qui délègue à une nouvelle instance
   * de <code>java.util.HashMap</code>.
   */
  public CaseInsensitiveMap() {
    this(new HashMap());
  }
  /**
   * Convertie la clé à une chaine en majuscule
   * @param key la clé à convertir
   * @return la clé en majuscule
   * @throws ClassCastException si key n'est pas de type <code>String</code>
   */
  private String upper(Object key) throws ClassCastException {
    return ((String) key).toUpperCase();
  }

  // -------------------------------------------
  // Méthodes modifiés avec une clé en majuscule
  // -------------------------------------------

  @Override
  public Object get(Object key) {
    return delegue.get(upper(key));
  }
  @Override
  public Object put(Object key, Object value) {
    return delegue.put(upper(key), value);
  }
  @Override
  public Object remove(Object key) {
    return delegue.remove(upper(key));
  }
  @Override
  public void putAll(Map t) {
    for (Iterator it = t.entrySet().iterator(); it.hasNext();) {
      Entry e = (Entry) it.next();
      put(upper(e.getKey()), e.getValue());
    }
  }
  @Override
  public boolean containsKey(Object key) {
    return delegue.containsKey(upper(key));
  }

  // ------------------------------
  // Méthodes de déléguation simple
  // ------------------------------

  @Override
  public int size() {
    return delegue.size();
  }
  @Override
  public boolean isEmpty() {
    return delegue.isEmpty();
  }
  @Override
  public boolean containsValue(Object value) {
    return delegue.containsValue(value);
  }
  @Override
  public void clear() {
    delegue.clear();
  }
  @Override
  public Set keySet() {
    return delegue.keySet();
  }
  @Override
  public Collection values() {
    return delegue.values();
  }
  @Override
  public Set entrySet() {
    return delegue.entrySet();
  }
  @Override
  public int hashCode() {
    return delegue.hashCode();
  }
  @Override
  public String toString() {
    return delegue.toString();
  }
}
