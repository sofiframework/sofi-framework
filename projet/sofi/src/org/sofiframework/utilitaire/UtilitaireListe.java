/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.AttributTri;

/**
 * Classe assurant la gestion des listes.
 * <p>
 * Cet objet fournit des utilitaires permettant la gestion des listes
 * <p>
 * 
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireListe {

  /**
   * Cette méthode permet de déterminer si une liste de String est vide.
   * 
   * Une liste de String est vide si elle est null, si elle est de longeur 0 ou si la totalité des Strings contenu est
   * vide.
   * 
   * @return
   * @param listeATester
   *          La liste à tester.
   */
  public static boolean isVide(String[] listeATester) {
    if (listeATester == null) {
      // Si la liste est null alors elle est vide.
      return true;
    } else {
      for (int i = 0; i < listeATester.length; i++) {
        // S'il existe une String dans la liste qui n'est pas vide, alors la
        // liste n'est pas vide
        if (!UtilitaireString.isVide(listeATester[i])) {
          return false;
        }
      }
    }

    // Si la liste est égale à zéro ou la totalité des String contenu est vide,
    // alors la liste de String est vide.
    return true;
  }

  /**
   * Insérer un item dans une collection.
   * 
   * @param liste
   *          Collection ou ajouter un item
   * @param index
   *          Index ou sera ajouté l'item
   * @param item
   *          Item à ajouter
   */
  @SuppressWarnings("unchecked")
  public static void insererItem(@SuppressWarnings("rawtypes") List liste, int index, Object item) {
    if (liste != null) {
      @SuppressWarnings("rawtypes")
      List listeInsertion = new ArrayList();

      for (int i = 0; i < liste.size(); i++) {
        if (index == i) {
          listeInsertion.add(item);
        }

        int indexAncienItem = i;

        if (i >= index) {
          indexAncienItem = i + 1;
        }

        listeInsertion.add(indexAncienItem, liste.get(i));
      }

      liste.clear();
      liste.addAll(listeInsertion);
    }
  }

  /**
   * Retourne l'instance de la liste qui correspond à une valeur comparateur et optionnellement une propriété
   * comparateur si objet complexe comme instance de liste.
   * 
   * @return l'instance de la liste qui correspond à une valeur comparateur et optionnellement une propriété comparateur
   *         si objet complexe comme instance de liste.
   * @param valeurComparateur
   *          la valeur servant de comparateur pour extraire une instance dans une liste.
   * @param proprieteComparateur
   *          la propriété comparateur pour extraire une valeur d'une liste.
   * @param liste
   *          la liste a traiter afin d'extraire une instance correspondant à une valeur comparateur.
   * @since SOFI 2.0.2
   */
  @SuppressWarnings("rawtypes")
  public static Object getValeurDansListe(List liste, String proprieteComparateur, String valeurComparateur) {
    if ((valeurComparateur != null) && (liste != null)) {
      Iterator iterateur = liste.iterator();
      boolean existe = false;

      while (iterateur.hasNext() && !existe) {
        String valeurATester = "";
        Object valeur = iterateur.next();

        if (proprieteComparateur != null) {
          try {
            valeurATester = UtilitaireObjet.getValeurAttribut(valeur, proprieteComparateur).toString();
          } catch (Exception e) {
          }
        } else {
          valeurATester = valeur.toString();
        }

        if (valeurATester.equals(valeurComparateur)) {
          return valeur;
        }
      }
    }

    return null;
  }

  /**
   * Ajuster la liste selon des valeurs à conserver ou exclure.
   * 
   * @param listeOriginal
   *          la liste a traiter (sera clone durant le traitement).
   * @param proprieteComparateur
   *          la propriete dont on désire faire la comparaison
   * @param valeurAAjuster
   *          les valeurs a être ajuster dans la liste.
   * @return la liste ajuste.
   * @since 3.1
   */
  @SuppressWarnings("rawtypes")
  public static List ajusterListeAvecValeurs(List listeOriginal, String proprieteComparateur, String[] valeurAAjuster,
      boolean conserver) {
    List liste = UtilitaireObjet.clonerListe(listeOriginal);
    if ((valeurAAjuster != null) && (liste != null)) {
      Iterator iterateur = liste.iterator();
      boolean existe = false;

      while (iterateur.hasNext() && !existe) {
        String valeurATester = "";
        Object valeur = iterateur.next();

        if (proprieteComparateur != null) {
          try {
            valeurATester = UtilitaireObjet.getValeurAttribut(valeur, proprieteComparateur).toString();
          } catch (Exception e) {
          }
        } else {
          valeurATester = valeur.toString();
        }

        if (conserver) {
          if (StringUtils.indexOfAny(valeurATester, valeurAAjuster) == -1) {
            iterateur.remove();
          }
        } else {
          if (StringUtils.indexOfAny(valeurATester, valeurAAjuster) != -1) {
            iterateur.remove();
          }
        }
      }
      return liste;
    }

    return null;
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   * 
   * @param c
   *          Comparator qui doit être utilisé pour le tri
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static List trierListe(List liste, final Comparator comparateur) {
    Collections.sort(liste, comparateur);

    return liste;
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   */
  @SuppressWarnings("rawtypes")
  public static List trierListe(List liste) {
    return trierListe(liste, (Comparator) null);
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel avec l'aide d'un collator. Le collator permet de
   * configurer la précision du tri pour l'ordonnement des String.
   */
  @SuppressWarnings("rawtypes")
  public static List trierListe(List liste, Collator collator) {
    return trierListe(liste, new ComparateurPaire(collator));
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel.
   */
  @SuppressWarnings("rawtypes")
  public static List trierListe(List liste, final AttributTri[] tri) {
    return trierListe(liste, tri, null);
  }

  /**
   * Tri les valeurs en les comparant dans leur ordre naturel. Le collator permet de configurer la précision du tri pour
   * l'ordonnement des String.
   */
  @SuppressWarnings("rawtypes")
  static public List trierListe(List liste, final AttributTri[] tri, final Collator collator) {
    /*
     * On crée un comparateur qui évalue chaque propriété jusqu'a ce qu'on obtienne une différence.
     */
    Comparator c = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        /*
         * On compare chaque propriété jusqu'a ce qu'une soit différente de l'autre.
         */
        int resultat = 0;

        for (int i = 0; (i < tri.length) && (resultat == 0); i++) {
          AttributTri attribut = tri[i];
          String nom = UtilitaireString.convertirPremiereLettreEnMinuscule(attribut.getNom());
          boolean descendant = !attribut.isAscendant();
          Object val1 = UtilitaireObjet.getValeurAttribut(o1, nom);
          Object val2 = UtilitaireObjet.getValeurAttribut(o2, nom);
          resultat = compareValeur(val1, val2, collator);
          resultat = (descendant ? (-resultat) : resultat);
        }

        return resultat;
      }
    };

    /*
     * Trier avec ce comparateur
     */
    return trierListe(liste, c);
  }

  /*
   * Tri des éléments de la collection du tri.
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static List trierListe(List liste, ComparateurPaire comparateur) {
    if ((liste != null) && (liste.size() > 1)) {
      /*
       * Composer les paires clé valeur
       */
      List listeValeur = new ArrayList();

      Iterator iterateur = liste.iterator();

      while (iterateur.hasNext()) {
        Object object = iterateur.next();
        listeValeur.add(object);
      }

      Collections.sort(listeValeur, comparateur);

      return listeValeur;
    }

    return liste;
  }

  /*
   * Compare les valeurs val1 et val2
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static int compareValeur(Object val1, Object val2, Collator collator) {
    int resultat = 0;

    if ((val1 != null) || (val2 != null)) {
      if (val1 == null) {
        resultat = -1;
      } else if (val2 == null) {
        resultat = +1;
      } else {
        /*
         * Si les valeurs sont comparables ont les compare
         */
        if (val1 instanceof Comparable && val2 instanceof Comparable) {
          if (collator == null) {
            resultat = ((Comparable) val1).compareTo(val2);
          } else {
            if (!(val1 instanceof String)) {
              throw new UnsupportedOperationException(
                  "Les valeurs doivent être des String pour les trier avec un Collator. La valeur " + val1 + " est "
                      + val1.getClass().getName());
            }

            resultat = collator.compare(val1, val2);
          }
        } else {
          /*
           * On doit lancer une exception si les valeurs ne sont pas comparables.
           */
          throw new UnsupportedOperationException("Les deux valeurs comparées ne sont pas Comparables.");
        }
      }
    }

    return resultat;
  }

  /**
   * Permet de setter listeSource avec nouvelleListe mais sans écraser la référence de listeSource. La mise à jour de
   * listeSource est effectuée par ajout / suppression au lieu d'utiliser une simple affectation. Si listeSource est
   * null alors nouvelleListe est affectée à listeSource
   * 
   * @param listeSource
   *          la liste à mettre à jour
   * @param nouvelleListe
   *          la liste avec laquelle listeSource doit être mise à jour
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static List synchroniserListe(List listeSource, List nouvelleListe) {
    List liste = null;
    if (nouvelleListe != null) {
      liste = listeSource;
      if (listeSource != null) {
        List listeSupprimer = ListUtils.subtract(listeSource, nouvelleListe);
        listeSource.removeAll(listeSupprimer);
        List listeAjout = ListUtils.subtract(nouvelleListe, listeSource);
        listeSource.addAll(listeAjout);
      } else {
        liste = nouvelleListe;
      }
    }
    return liste;
  }

  /**
   * Permet d'extraire une liste en tableau <String> pour une liste et une valeur d'un attribut spécifique.
   * 
   * @param liste
   *          la liste en traitement.
   * @param attribut
   *          le nom d'attribut dont on désire extraire la valeur.
   * @return un tableau de <String>
   */
  public static String[] getTableau(@SuppressWarnings("rawtypes") List liste, String attribut) {
    String[] tableau = new String[liste.size()];

    for (int i = 0; i < liste.size(); i++) {
      Object objetCourant = liste.get(i);
      objetCourant = UtilitaireObjet.getValeurAttribut(objetCourant, attribut);
      if (objetCourant != null) {
        tableau[i] = objetCourant.toString();
      }
    }

    return tableau;
  }

  /**
   * Permet de prendre une liste parents-enfants et de la mettre en 1 seul niveau.
   * 
   * @param liste
   * @param proprieteListComparateur
   * @return la liste complète hiérarchique en 1 seul niveau.
   * @since 3.2
   */
  public static List getListeParentEnfantsEn1Niveau(List liste, String proprieteListComparateur) {
    List liste1Niveau = new ArrayList();

    if (liste != null) {
      for (int i = 0; i < liste.size(); i++) {
        Object valeur = liste.get(i);

        liste1Niveau.add(valeur);

        List listeValeurATester = null;

        try {
          listeValeurATester = (List) UtilitaireObjet.getValeurAttribut(valeur, proprieteListComparateur);
          remplirListe1Niveau(liste1Niveau, listeValeurATester, proprieteListComparateur);
        } catch (Exception e) {
        }
      }
    }

    return liste1Niveau;
  }

  private static void remplirListe1Niveau(List liste1Niveau, List listeTemporaire, String proprieteListComparateur) {
    for (int i = 0; i < listeTemporaire.size(); i++) {
      Object valeur = listeTemporaire.get(i);

      liste1Niveau.add(valeur);

      List listeValeurATester = null;

      try {
        listeValeurATester = (List) UtilitaireObjet.getValeurAttribut(valeur, proprieteListComparateur);
        remplirListe1Niveau(liste1Niveau, listeValeurATester, proprieteListComparateur);
      } catch (Exception e) {
      }

    }

  }

}
