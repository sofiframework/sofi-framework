/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class UtilitaireSingleton {
  /**
   * L'instance de journalisation pour cette classe
   */
  static private Log log = LogFactory.getLog(org.sofiframework.utilitaire.UtilitaireSingleton.class);

  public static Object creerClasseUnique(String nomClasse) {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    if (log.isInfoEnabled()) {
      log.info("Création du singleton :" + nomClasse);
    }

    Class classe = null;

    try {
      classe = classLoader.loadClass(nomClasse);
    } catch (ClassNotFoundException e) {
    }

    Object instance = null;

    try {
      instance = classe.newInstance();
    } catch (InstantiationException e) {
    } catch (IllegalAccessException e) {
    }

    return instance;
  }
}
