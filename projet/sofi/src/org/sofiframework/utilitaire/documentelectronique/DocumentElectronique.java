/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.documentelectronique;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Classe qui sert d'objet de transfert du modele vers la couche de presentation
 * pour les documents électroniques.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class DocumentElectronique extends ObjetTransfert {

  private static final long serialVersionUID = -6495498107736923918L;

  /**
   * Constante qui loge un document PDF.
   */
  public static final String DOCUMENT_PDF = "documentPdf";

  /**
   * Constante qui loge un document CSV.
   */
  public static final String DOCUMENT_CSV = "documentCsv";
  public static final String TYPE_MIME_PDF = "pdf";
  public static final String TYPE_MIME_CSV = "application/vnd.ms-excel";

  /** l'identifiant du document électronique */
  private Integer identifiant;

  /** le nom du fichier */
  private String nomFichier;

  /** les données contenues dans le fichier */
  private byte[] valeurDocument;

  /** le titre du document */
  private String nomTitreDocument;

  /** le type MIME du contenu du document */
  private String codeTypeMime;

  /** le code du type de document */
  private String codeTypeDocument;

  /** La description du fichier **/
  private String descriptionFichier;

  /** Le volume du fichier **/
  private Integer volumeFichier;

  /**
   * Constructeur par défaut
   */
  public DocumentElectronique() {
  }

  /**
   * Obtenir l'identifiant du document électronique.
   * <p>
   * @return Integer l'identifiant du document électronique
   */
  public Integer getIdentifiant() {
    return identifiant;
  }

  /**
   * Fixer l'identifiant du document électronique.
   * <p>
   * @param newIdentifiant l'identifiant du document électronique.
   */
  public void setIdentifiant(Integer newIdentifiant) {
    identifiant = newIdentifiant;
  }

  /**
   * Obtenir le nom du fichier.
   * <p>
   * @return le nom du fichier
   */
  public String getNomFichier() {
    return nomFichier;
  }

  /**
   * Fixer le nom du fichier.
   * <p>
   * @param newNomFichier le nom du fichier
   */
  public void setNomFichier(String newNomFichier) {
    nomFichier = newNomFichier;
  }

  /**
   * Obtenir la description du fichier.
   * <p>
   * @return la description du fichier
   */
  public String getDescriptionFichier() {
    return this.descriptionFichier;
  }

  /**
   * Fixer la description du fichier fichier.
   * <p>
   * @param newDescriptionFichier la description du fichier
   */
  public void setDescriptionFichier(String newDescriptionFichier) {
    this.descriptionFichier = newDescriptionFichier;
  }

  /**
   * Obtenir les données contenues dans le fichier.
   * <p>
   * @return les données contenues dans le fichier
   */
  public byte[] getValeurDocument() {
    return valeurDocument;
  }

  /**
   * Fixer les données contenues dans le fichier.
   * <p>
   * @param newValeurDocument les données contenues dans le fichier
   */
  public void setValeurDocument(byte[] newValeurDocument) {
    valeurDocument = newValeurDocument;
  }

  /**
   * Obtenir le titre du document.
   * <p>
   * @return String les données contenues dans le fichier
   */
  public String getNomTitreDocument() {
    return nomTitreDocument;
  }

  /**
   * Fixer le titre du document.
   * <p>
   * @param newNomTitreDocument le titre du document
   */
  public void setNomTitreDocument(String newNomTitreDocument) {
    nomTitreDocument = newNomTitreDocument;
  }

  /**
   * Obtenir le type MIME du contenu du document.
   * <p>
   * @return String le type MIME du contenu du document
   */
  public String getCodeTypeMime() {
    if (codeTypeMime == null) {
      codeTypeMime = "application/octet-stream";
    }

    return codeTypeMime;
  }

  /**
   * Fixer le type MIME du contenu du document.
   * <p>
   * @param newCodeTypeMime le type MIME du contenu du document
   */
  public void setCodeTypeMime(String newCodeTypeMime) {
    codeTypeMime = newCodeTypeMime;
  }

  /**
   * Obtenir le code du type de document.
   * <p>
   * @return String le code du type de document.
   */
  public String getCodeTypeDocument() {
    return codeTypeDocument;
  }

  /**
   * Fixer le code du type de document.
   * <p>
   * @param newCodeTypeDocument le code du type de document.
   */
  public void setCodeTypeDocument(String newCodeTypeDocument) {
    codeTypeDocument = newCodeTypeDocument;
  }

  /**
   * Retourne si le type de document est une image.
   * <p>
   * @return la valeur indiquant si le document est une image ou pas
   */
  public boolean isImage() {
    if ((codeTypeMime != null) && (codeTypeMime.indexOf("image") != -1)) {
      return true;
    } else {
      if (UtilitaireDocumentElectronique.getTypeMimeFichier(nomFichier).indexOf("image") !=
          -1) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Obtenir le volume du fichier en octet.
   * <p>
   * @return le volume du fichier en octet
   */
  public int getVolumerFichierEnBytes() {
    if (getValeurDocument() != null) {
      return getValeurDocument().length;
    } else {
      return getVolumeFichier().intValue();
    }
  }

  /**
   * Obtenir le volume du fichier en Kilo-octet pour affichage.
   * <p>
   * @return le volume du fichier en Kilo-octet pour affichage
   */
  public String getVolumeFichierEnKoAffichage() {
    return getVolume() + "k";
  }

  /**
   * Fixer le volume du fichier, si la valeur du document n'est pas comprise.
   * @param volumeFichier le volume du fichier.
   */
  public void setVolumeFichier(Integer volumeFichier) {
    this.volumeFichier = volumeFichier;
  }

  /**
   * Retourne le volume du fichier, si la valeur du document n'est pas comprise.
   * @return  le volume du fichier
   */
  public Integer getVolumeFichier() {
    if (getValeurDocument() != null) {
      this.volumeFichier = new Integer(getValeurDocument().length);
    }

    return volumeFichier;
  }

  /**
   * Retourne le nom du fichier avec son volume.
   * @return le nom du fichier avec son volume.
   */
  public String getNomAvecVolume() {
    StringBuffer nomAvecVolume = new StringBuffer();
    nomAvecVolume.append(getNomFichier());
    nomAvecVolume.append(" (");
    nomAvecVolume.append(getVolumeFichierEnKoAffichage());
    nomAvecVolume.append(")");

    return nomAvecVolume.toString();
  }

  /**
   * Retourne le volume du fichier en format texte.
   * @return le volume du fichier en format texte.
   */
  public String getVolume() {
    double volumeEnBytes = getVolumerFichierEnBytes();
    double volume = volumeEnBytes / 1024d;
    String volumeAffichage = null;

    if (volume < 1) {
      volumeAffichage = "1";
    } else {
      volumeAffichage = String.valueOf(volume);
    }
    try {
      return volumeAffichage.substring(0, volumeAffichage.indexOf("."));
    } catch (Exception e) {
      return volumeAffichage;
    }
  }
}
