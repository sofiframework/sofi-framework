/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.documentelectronique;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Classe utilitaire qui permet la gestion des documents électroniques qui peuvent
 * être envoyé à un client. Ces types de document électronique peut être un
 * document Word, Excel, PDF, fichier zip, etc.. Il peut aussi être utilisé
 * afin d'afficher une image dynamiquement à un client lorsque celle-ci provient
 * d'une base de données par exemple.
 * <p>
 * @author Jean-François Brassard (Nurun, inc.)
 * @version SOFI 1.0
 */
public class UtilitaireDocumentElectronique {
  /** Le type de contenu par défaut */
  public static final String TYPE_CONTENU_DEFAUT = "application/octet-stream";

  /** Le type de contenu pour MICROSOFT WORD */
  public static final String TYPE_CONTENU_WORD = "application/msword";

  /** Le type de contenu pour MICROSOFT EXCEL */
  public static final String TYPE_CONTENU_EXCEL = "application/vnd.ms-excel";

  /** Le type de contenu pour MICROSOFT POWERPOINT */
  public static final String TYPE_CONTENU_POWERPOINT = "application/vnd.ms-powerpoint";

  /** Le type de contenu pour PDF */
  public static final String TYPE_CONTENU_PDF = "application/pdf";

  /** Le type de contenu pour ZIP */
  public static final String TYPE_CONTENU_ZIP = "application/zip";

  /** Le type de contenu pour COMPRESS */
  public static final String TYPE_CONTENU_COMPRESS = "application/x-compress";

  /** Le type de contenu pour HTML */
  public static final String TYPE_CONTENU_HTML = "text/html";

  /** Le type de contenu pour XML */
  public static final String TYPE_CONTENU_XML = "text/xml";

  /** Le type de contenu pour HTML */
  public static final String TYPE_CONTENU_TEXTE = "text/plain";

  /** Le type de contenu pour image GIF*/
  public static final String TYPE_CONTENU_IMAGE_GIF = "image/gif";

  /** Le type de contenu pour image JPEG*/
  public static final String TYPE_CONTENU_IMAGE_JPEG = "image/jpeg";

  /** Le type de contenu pour image TIFF*/
  public static final String TYPE_CONTENU_IMAGE_TIFF = "image/tiff";

  /** Le type de contenu pour image PNG*/
  public static final String TYPE_CONTENU_IMAGE_PNG = "image/png";

  // L'instance de journalisation pour cette classe.
  private Log log = LogFactory.getLog(UtilitaireDocumentElectronique.class);

  /** La réponse à renvoyer à la session */
  private HttpServletResponse response;

  /** Le nom du fichier complet incluant le path */
  private String nomFichierAvecCheminComplet = "";

  /** Le nom du fichier */
  private String nomFichier = "";

  /** Le contenu d'un fichier à afficher */
  private byte[] contenuFichier;

  /** Le mime type du fichier */
  private String typeContenuFichier;

  /** La grosseur du buffer d'écriture */
  private int m_BufferSize = 65536;

  /** Le buffer d'écriture */
  private byte[] m_WriteBuffer = new byte[m_BufferSize];

  /** La longueur du fichier en bytes **/
  private int volumeFichier;

  /** La description du fichier **/
  private String descriptionFichier;

  /**
   * Constructeur.
   * <p>
   * @param newRequest l'objet HttpServletRequest correspondant à la requête.
   * @param newResponse l'objet HttpServletResponse correspondant à la réponse.
   * @param newNomFichierAvecCheminComplet le nom complet du fichier avec son emplacement
   * @param newNomFichier le nom de fichier.
   */
  public UtilitaireDocumentElectronique(HttpServletRequest newRequest,
      HttpServletResponse newResponse, String newNomFichierAvecCheminComplet,
      String newNomFichier) {
    response = newResponse;
    nomFichierAvecCheminComplet = newNomFichierAvecCheminComplet;
    nomFichier = newNomFichier;

    // Spécifier le le type de fichier selon son extension.
    this.setTypeFichierAvecNomFichier(newNomFichier.substring(newNomFichier.lastIndexOf(
        ".") + 1));
  }

  /**
   * Constructeur.
   * <p>
   * @param newRequest l'objet HttpServletRequest correspondant à la requête.
   * @param newResponse l'objet HttpServletResponse correspondant à la réponse.
   * @param newContenuFichier le contenu du fichier.
   * @param newNomFichier le type de contenu du fichier.
   */
  public UtilitaireDocumentElectronique(HttpServletRequest newRequest,
      HttpServletResponse newResponse, byte[] newContenuFichier,
      String newNomFichier) {
    response = newResponse;
    contenuFichier = newContenuFichier;
    nomFichier = newNomFichier;

    this.volumeFichier = contenuFichier.length;

    // Spécifier le le type de fichier selon son extension.
    this.setTypeFichierAvecNomFichier(newNomFichier.substring(newNomFichier.lastIndexOf(
        ".") + 1));
  }

  /**
   * Méthode qui assigne automatiquement un type de fichier en fonction de l'extension donnée.
   * <p>
   * Cette méthode prend une extension et attribut un type de fichier en fonction de l'extension donnée.
   * Toutes les extensions sont testées, si ce n'est pas une extension connue, alors l'extension par défaut
   * est utilisée
   * <p>
   * @param extensionFichier l'extension à utiliser
   */
  public void setTypeFichierAvecNomFichier(String ext) {
    String extensionFichier = ext.toLowerCase();

    if (extensionFichier.equals("xls") || extensionFichier.equals("csv")) {
      typeContenuFichier = TYPE_CONTENU_EXCEL;
    } else if (extensionFichier.equals("doc")) {
      typeContenuFichier = TYPE_CONTENU_WORD;
    } else if (extensionFichier.equals("pdf")) {
      typeContenuFichier = TYPE_CONTENU_PDF;
    } else if (extensionFichier.equals("ppt")) {
      typeContenuFichier = TYPE_CONTENU_POWERPOINT;
    } else if (extensionFichier.equals("zip")) {
      typeContenuFichier = TYPE_CONTENU_ZIP;
    } else if (extensionFichier.equals("html")) {
      typeContenuFichier = TYPE_CONTENU_HTML;
    } else if (extensionFichier.equals("txt")) {
      typeContenuFichier = TYPE_CONTENU_TEXTE;
    } else if (extensionFichier.equals("gif")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_GIF;
    } else if (extensionFichier.equals("pgn")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_PNG;
    } else if (extensionFichier.equals("tiff")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_TIFF;
    } else if (extensionFichier.equals("jpeg") ||
        extensionFichier.equals("jpg")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_JPEG;
    } else {
      typeContenuFichier = TYPE_CONTENU_DEFAUT;
    }
  }

  /**
   * Méthode utilitaire qui retourne le type mine correspondant à
   * l'extension du fichier.
   * @return le type mime correspondant au type mime.
   * @param nomFichier le nom du fichier incluant l'extension du fichier.
   */
  static public String getTypeMimeFichier(String nomFichier) {
    String typeContenuFichier = null;
    String extensionFichier = nomFichier.substring(nomFichier.lastIndexOf(".") +
        1).toLowerCase();

    if (extensionFichier.equals("xls") || extensionFichier.equals("csv")) {
      typeContenuFichier = TYPE_CONTENU_EXCEL;
    } else if (extensionFichier.equals("doc")) {
      typeContenuFichier = TYPE_CONTENU_WORD;
    } else if (extensionFichier.equals("pdf")) {
      typeContenuFichier = TYPE_CONTENU_PDF;
    } else if (extensionFichier.equals("ppt")) {
      typeContenuFichier = TYPE_CONTENU_POWERPOINT;
    } else if (extensionFichier.equals("zip")) {
      typeContenuFichier = TYPE_CONTENU_ZIP;
    } else if (extensionFichier.equals("html")) {
      typeContenuFichier = TYPE_CONTENU_HTML;
    } else if (extensionFichier.equals("txt")) {
      typeContenuFichier = TYPE_CONTENU_TEXTE;
    } else if (extensionFichier.equals("gif")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_GIF;
    } else if (extensionFichier.equals("pgn")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_PNG;
    } else if (extensionFichier.equals("tiff")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_TIFF;
    } else if (extensionFichier.equals("jpeg") ||
        extensionFichier.equals("jpg")) {
      typeContenuFichier = TYPE_CONTENU_IMAGE_JPEG;
    } else {
      typeContenuFichier = TYPE_CONTENU_DEFAUT;
    }

    return typeContenuFichier;
  }

  /**
   * Methode qui écrit le contenu au client.
   * <p>
   * @throws IOException Exception générique
   */
  public void ecrireContenuFichierAuClient() throws IOException {
    response.reset();

    // Quand c'est le nom du fichier qui est spécifié
    if ((nomFichierAvecCheminComplet != null) &&
        (nomFichierAvecCheminComplet.length() > 0)) {
      File leFichier = new File(nomFichierAvecCheminComplet);

      if (leFichier.exists() && leFichier.isFile() && leFichier.canRead()) {
        response.setContentLength((int) leFichier.length());
        response.setContentType(typeContenuFichier);

        if (!isImage()) {
          response.addHeader("Content-Disposition",
              "attachment; filename=\"" + nomFichier + "\"");
        }

        try {
          copyStream(new FileInputStream(nomFichierAvecCheminComplet),
              response.getOutputStream());

          /**
           A Essayer pour régler le problème des gros fichiers
                    InputStream in = new FileInputStream(leFichier);
                    ServletOutputStream outs = response.getOutputStream();

                    int bit = 256;
                    int i = 0;

                    try {
                      while ((bit) >= 0) {
                        bit = in.read();
                        outs.write(bit);
                      }
                    } catch (IOException ioe) {
                      ioe.printStackTrace(System.out);
                    }

                    outs.flush();
                    outs.close();
                    in.close();
           **/
        } catch (IOException ioe) {
          log.error("FichierTelechargement :: Erreur ecrire fichier au client" +
              ioe.getMessage());
          throw new IOException(
              "FichierTelechargement::Erreur ecrire fichier au client");
        }
      } else {
        log.error("FichierTelechargement: le fichier " +
            nomFichierAvecCheminComplet + "n'existe pas");
        throw new IOException(
            "FichierTelechargement::Incapable d'ouvrir le fichier");
      }
    }

    //Quand c'est le contenu du fichier qui est spécifié
    if ((contenuFichier != null) && (contenuFichier.length > 0)) {
      response.setContentLength(contenuFichier.length);
      response.setContentType(typeContenuFichier);
      response.addHeader("Accept-Ranges", "none");

      if (!isImage()) {
        response.addHeader("Content-Disposition",
            "attachment; filename=\"" + nomFichier + "\"");
      }

      if (contenuFichier != null) {
        try {
          copyStream(new ByteArrayInputStream(contenuFichier),
              response.getOutputStream());
        } catch (IOException ioe) {
          log.error("FichierTelechargement :: Erreur ecrire fichier au client" +
              ioe.getMessage());
          throw new IOException(
              "FichierTelechargement::Erreur ecrire fichier au client");
        }
      }
    }
  }

  /**
   * Methode qui prépare l'envoie du fichier.
   * <p>
   * @param ins correspond au InputStream.
   * @param outs correspond au OutputStream.
   * @throws IOException Exception générique
   */
  private void copyStream(InputStream ins, OutputStream outs)
      throws IOException {
    BufferedInputStream bis = new BufferedInputStream(ins, m_BufferSize);
    BufferedOutputStream bos = new BufferedOutputStream(outs, m_BufferSize);
    int bufread;

    while ((bufread = bis.read(m_WriteBuffer)) != -1) {
      bos.write(m_WriteBuffer, 0, bufread);
    }

    bos.flush();
    bos.close();
    bis.close();
  }

  /**
   * Obtenir le volume du fichier en octet.
   * <p>
   * @return le volume de fichier en octet
   */
  public int getVolumerFichierEnBytes() {
    return volumeFichier;
  }

  /**
   * Obtenir le volume du fichier en Kilo-octet pour affichage.
   * <p>
   * @return le volume du fichier en Kilo-octect dans un format pour affichage
   */
  public String getVolumeFichierEnKoAffichage() {
    float volume = (new Float(volumeFichier / 1024)).floatValue();
    int intVolume = (new Integer(Float.toString(volume))).intValue();

    return intVolume + "k";
  }

  /**
   * Specifie si le type de fichier est une image.
   * <p>
   * @return true/false si le type de fichier est une image
   */
  public boolean isImage() {
    String typeMime = this.typeContenuFichier.substring(0,
        typeContenuFichier.indexOf("/"));

    if (typeMime.equals("image")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Obtenir le nom du fichier.
   * <p>
   * @return le nom du fichier
   */
  public String getNomFichier() {
    return nomFichier;
  }

  /**
   * Specifie si le fichier est de type microsoft (Ex: Word, excel, powerpoint).
   * <p>
   * @return true/false si le type de fichier est un logiciel microsoft
   */
  public boolean isFichierMicrosoft() {
    try {
      String tmp = typeContenuFichier.substring(typeContenuFichier.indexOf("m"),
          typeContenuFichier.indexOf("m") + 2);

      if (tmp.trim().equals("ms")) {
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Obtenir le type de fichier (MIME Type).
   * <p>
   * @return le type de fichier (MIME Type)
   */
  public String getTypeFichier() {
    return this.typeContenuFichier;
  }

  /**
   * Specifier le type de fichier.
   * <p>
   * @param typeContenuFichier le type de contenu du fichier
   */
  public void setTypeFichier(String typeContenuFichier) {
    this.typeContenuFichier = typeContenuFichier;
  }

  /**
   * Obtenir la description du fichier.
   * <p>
   * @return la description du fichier
   */
  public String getDescriptionFichier() {
    return this.descriptionFichier;
  }

  /**
   * Specifier la description du fichier.
   * <p>
   * @param descriptionFichier la description du fichier
   */
  public void setDescriptionFichier(String descriptionFichier) {
    this.descriptionFichier = descriptionFichier;
  }

  /**
   * Prépare le contenu du document électronique pour affichage, si le
   * document électronique correspond à une image.
   * <p>
   * UtilitaireDocumentElectronique est est accessible via la session
   * avec le nom d'attribut "utilitaireDocumentElectronique".
   * @param request l'objet HttpServletRequest correspondant à la requête.
   * @param response l'objet HttpServletResponse correspondant à la réponse.
   */
  public static void preparerAffichageImage(HttpServletRequest request,
      HttpServletResponse response, DocumentElectronique documentElectronique) {
    //Préparer l'affichage si image
    if (documentElectronique.isImage()) {
      UtilitaireDocumentElectronique utilitaireDocumentElectronique = new UtilitaireDocumentElectronique(request,
          response, documentElectronique.getValeurDocument(),
          documentElectronique.getNomFichier());
      utilitaireDocumentElectronique.setTypeFichier(documentElectronique.getCodeTypeMime());
      request.getSession().setAttribute("utilitaireDocumentElectronique",
          utilitaireDocumentElectronique);
    }
  }

  /**
   * Retourne un document électronique depuis un nom de fichier
   * et une valeur de document (déjà générer par JasperReports).
   * @param valeurDocument une valeur du document (déjà générer par JasperReports).
   * @param nomFichier le nom du fichier associé au document.
   * @return un document électronique de type PDF depuis un nom de fichier
   * et une valeur du document (déjà générer par JasperReports).
   */
  public static DocumentElectronique getDocumentEletronique(byte[] valeurDocument,
      String nomFichier) {

    DocumentElectronique documentPdf = new DocumentElectronique();

    String typeMime =
        UtilitaireDocumentElectronique.getTypeMimeFichier(nomFichier);
    documentPdf.setCodeTypeMime(typeMime);
    documentPdf.setNomFichier(nomFichier);
    documentPdf.setValeurDocument(valeurDocument);
    documentPdf.setVolumeFichier(new Integer(valeurDocument.length));

    return documentPdf;
  }
}
