/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


/**
 * Classe permettant d'envoyer des courriels
 * <p>
 * Cette classe permet d'envoyer des courriels en format texte ou html,
 * avec ou sans fichiers attaché
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @author Steve Tremblay (Nurun inc.)
 * @author Pierre-Frederick Duret (Nurun inc.)
 * @version SOFI 1.0
 * @deprecated utiliser la classe com.nurun.utilitaire.couriel.CourrielHtml ou com.nurun.utilitaire.couriel.CourrielTexte.
 */
@Deprecated
public class Courriel {
  /** L'adresse du serveur d'envoi de courrier SMTP */
  private String smtpServer;

  /** L'adresse de courriel de la personne qui envoie le message */
  private String emailDestinateur;

  /** Le nom complet de la personne qui envoie le message */
  private String nomDestinateur;

  /** la liste des destinataires ï¿½ ajouter en CC du courriel */
  private ArrayList listeDestinatairesCC;

  /** la liste des destinataires ï¿½ ajouter ne BCC du courriel */
  private ArrayList listeDestinatairesBCC;

  /** la liste des destinataires ï¿½ ajouter au courriel */
  private ArrayList listeDestinataires;

  /** la liste des piï¿½ces jointes attachï¿½ au courriel **/
  private ArrayList listePiecesJointes;

  /**
   * Le constructeur par dï¿½faut.
   * <p>
   * Le constructeur fixe par dï¿½faut le nom du serveur SMTP et le nom et l'adresse
   * de courrier ï¿½lectronique du destinateur. Il faut donc chagner ces valeurs
   * par la suite selon le projet.
   * <p>
   * @param smptServer L'adresse ip du serveur SMTP
   * @param emailDestinateur L'adresse courriel du destinataire
   * @param nomDestinateur Le nom du destinataire
   */
  public Courriel(String smptServer, String emailDestinateur,
      String nomDestinateur) {
    this.smtpServer = smptServer;
    this.emailDestinateur = emailDestinateur;
    this.nomDestinateur = nomDestinateur;
  }

  /**
   * Obtenir l'adresse du serveur d'envoi de courrier SMTP.
   * <p>
   * @return String l'adresse du serveur d'envoi de courrier SMTP
   */
  public String getSmtpServer() {
    return smtpServer;
  }

  /**
   * Fixer l'adresse du serveur d'envoi de courrier SMTP.
   * <p>
   * @param newSmtpServer l'adresse du serveur d'envoi de courrier SMTP
   */
  public void setSmtpServer(String newSmtpServer) {
    smtpServer = newSmtpServer;
  }

  /**
   * Obtenir l'adresse de courrier ï¿½lectronique du rï¿½cipiendaire du message.
   * <p>
   * @return String l'adresse de courrier ï¿½lectronique du rï¿½cipiendaire du message
   */
  public String getEmailDestinateur() {
    return emailDestinateur;
  }

  /**
   * Fixer l'adresse de courrier ï¿½lectronique du rï¿½cipiendaire du message.
   * <p>
   * @param newEmailDestinateur l'adresse de courrier ï¿½lectronique du rï¿½cipiendaire du message
   */
  public void setEmailDestinateur(String newEmailDestinateur) {
    emailDestinateur = newEmailDestinateur;
  }

  /**
   * Obtenir le nom complet du rï¿½cipiendaire du message.
   * <p>
   * @return String le nom complet du rï¿½cipiendaire du message
   */
  public String getNomDestinateur() {
    return nomDestinateur;
  }

  /**
   * Fixer le nom complet du rï¿½cipiendaire du message.
   * <p>
   * @param newNomDestinateur le nom complet du rï¿½cipiendaire du message
   */
  public void setNomDestinateur(String newNomDestinateur) {
    nomDestinateur = newNomDestinateur;
  }

  /**
   * Envoyer un message en format texte sans fichier attachï¿½.
   * <p>
   * Notons que la valeur de retour ne garantit en aucun cas la livraison du message
   * au destinataire. La mï¿½thode retounera faux seulement si le serveur SMTP n'a pas
   * acceptï¿½ de relayer le message.
   * <p>
   * @param sujet le sujet du message
   * @param message le contenu du message
   * @throws java.lang.Exception exception si le courriel n'a pas ï¿½tï¿½ envoyï¿½.
   */
  public void envoyerCourrielTexte(String sujet, String message)
      throws Exception {
    Properties props = new Properties();
    props.put("mail.smtp.host", smtpServer);

    try {
      Session session = Session.getDefaultInstance(props, null);
      MimeMessage msg = new MimeMessage(session);
      session.setDebug(false);
      msg.setFrom(new InternetAddress(this.emailDestinateur, this.nomDestinateur));

      if ((this.listeDestinataires != null) &&
          (this.listeDestinataires.size() > 0)) {
        msg.addRecipients(Message.RecipientType.TO,
            this.convertirEnTableau(this.listeDestinataires));
      }

      if ((this.listeDestinatairesCC != null) &&
          (this.listeDestinatairesCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.CC,
            this.convertirEnTableau(this.listeDestinatairesCC));
      }

      if ((this.listeDestinatairesBCC != null) &&
          (this.listeDestinatairesBCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.BCC,
            this.convertirEnTableau(this.listeDestinatairesBCC));
      }

      msg.setSubject(sujet);
      msg.setText(message, "UTF-8");
      Transport.send(msg);
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Envoyer un message en format texte avec un attachement en format texte HTML.
   * <p>
   * Notons que la valeur de retour ne garantit en aucun cas la livraison du message
   * au destinataire. La mï¿½thode retounera faux seulement si le serveur SMTP n'a pas
   * acceptï¿½ de relayer le message.
   * <p>
   * @param emailDestinataire l'adresse de courriel du rï¿½cipiendaire du message
   * @param nomDestinataire le nom complet du rï¿½cipiendaire du message
   * @param sujet le sujet du message
   * @param message le contenu du message
   * @param nomDuFichier le nom du fichier attachï¿½
   * @param attachment le fichier attachï¿½ en format texte HTML
   * @throws java.lang.Exception exception si le courriel n'a pas ï¿½tï¿½ envoyï¿½.
   */
  public void envoyerCourrielTexteAvecAttachment(String emailDestinataire,
      String nomDestinataire, String sujet, String message, String nomDuFichier,
      String attachment) throws Exception {
    if ((emailDestinataire != null) && (nomDestinataire != null)) {
      ajouterDestinataire(emailDestinataire, nomDestinataire);
    }

    Properties props = new Properties();
    props.put("mail.smtp.host", smtpServer);

    try {
      MimeMultipart mp = new MimeMultipart();

      MimeBodyPart mbp1 = new MimeBodyPart();
      MimeBodyPart mbp2 = new MimeBodyPart();

      mbp1.setText(message, "UTF-8");
      mbp2.setText(attachment, "UTF-8");
      mbp2.setFileName(nomDuFichier);

      Session session = Session.getDefaultInstance(props, null);
      MimeMessage msg = new MimeMessage(session);
      session.setDebug(false);
      msg.setFrom(new InternetAddress(emailDestinateur, nomDestinateur));

      if ((this.listeDestinataires != null) &&
          (this.listeDestinataires.size() > 0)) {
        msg.addRecipients(Message.RecipientType.TO,
            this.convertirEnTableau(this.listeDestinataires));
      }

      if ((this.listeDestinatairesCC != null) &&
          (this.listeDestinatairesCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.CC,
            this.convertirEnTableau(this.listeDestinatairesCC));
      }

      if ((this.listeDestinatairesBCC != null) &&
          (this.listeDestinatairesBCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.BCC,
            this.convertirEnTableau(this.listeDestinatairesBCC));
      }

      msg.setSubject(sujet, "UTF-8");
      mp.addBodyPart(mbp1);
      mp.addBodyPart(mbp2);
      msg.setContent(mp);
      Transport.send(msg);
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Envoyer un message en format texte avec un attachement en format texte HTML.
   * <p>
   * Notons que la valeur de retour ne garantit en aucun cas la livraison du message
   * au destinataire. La mï¿½thode retounera faux seulement si le serveur SMTP n'a pas
   * acceptï¿½ de relayer le message.
   * <p>
   * @param sujet le sujet du message
   * @param message le contenu du message
   * @param nomDuFichier le nom du fichier attachï¿½
   * @param attachment le fichier attachï¿½ en format texte HTML
   * @throws java.lang.Exception exception si le courriel n'a pas ï¿½tï¿½ envoyï¿½.
   */
  public void envoyerCourrielTexteAvecAttachment(String sujet, String message,
      String nomDuFichier, String attachment) throws Exception {
    envoyerCourrielTexteAvecAttachment(null, null, sujet, message,
        nomDuFichier, attachment);
  }

  /**
   * Envoyer un message en format texte avec un fichier attachï¿½.
   * <p>
   * Notons que la valeur de retour ne garantit en aucun cas la livraison du message
   * au destinataire. La mï¿½thode retounera faux seulement si le serveur SMTP n'a pas
   * acceptï¿½ de relayer le message.
   * <p>
   * @param emailDestinataire l'adresse de courriel du rï¿½cipiendaire du message
   * @param nomDestinataire le nom complet du rï¿½cipiendaire du message
   * @param sujet le sujet du message
   * @param message le contenu du message
   * @param nomDuFichier le nom du fichier attachï¿½
   * @param fichier un vecteur d'octets contenant les donnï¿½es du fichier
   * @throws java.io.UnsupportedEncodingException Format de l'attachement invalide
   * @throws javax.mail.MessagingException erreur d'envoie du message.
   */
  public void envoyerCourrielTexteAvecAttachment(String emailDestinataire,
      String nomDestinataire, String sujet, String message, String nomDuFichier,
      byte[] fichier) throws UnsupportedEncodingException, MessagingException {
    if ((emailDestinataire != null) && (nomDestinataire != null)) {
      ajouterDestinataire(emailDestinataire, nomDestinataire);
    }

    Properties props = new Properties();
    props.put("mail.smtp.host", smtpServer);

    try {
      MimeMultipart mp = new MimeMultipart();

      MimeBodyPart mbp1 = new MimeBodyPart();

      Session session = Session.getDefaultInstance(props, null);
      MimeMessage msg = new MimeMessage(session);
      session.setDebug(false);
      msg.setFrom(new InternetAddress(emailDestinateur, nomDestinateur));

      if ((this.listeDestinataires != null) &&
          (this.listeDestinataires.size() > 0)) {
        msg.addRecipients(Message.RecipientType.TO,
            this.convertirEnTableau(this.listeDestinataires));
      }

      if ((this.listeDestinatairesCC != null) &&
          (this.listeDestinatairesCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.CC,
            this.convertirEnTableau(this.listeDestinatairesCC));
      }

      if ((this.listeDestinatairesBCC != null) &&
          (this.listeDestinatairesBCC.size() > 0)) {
        msg.addRecipients(Message.RecipientType.BCC,
            this.convertirEnTableau(this.listeDestinatairesBCC));
      }

      msg.setSubject(sujet, "UTF-8");
      mp.addBodyPart(mbp1);

      if (fichier != null) {
        ajouterPieceJointe(nomDuFichier, fichier);
      }

      if (listePiecesJointes != null) {
        Iterator iterateur = listePiecesJointes.iterator();

        while (iterateur.hasNext()) {
          HashMap detailPieceJointe = (HashMap) iterateur.next();

          // Extraire le dï¿½tail de la piï¿½ce jointe.
          String nomFichier = (String) detailPieceJointe.get("nomFichier");
          byte[] detailFichier = (byte[]) detailPieceJointe.get("detail");

          MimeBodyPart partiePieceJointe = new MimeBodyPart();

          mbp1.setText(message, "UTF-8");

          ByteArrayDataSource source = null;

          if (nomFichier.endsWith(".zip")) {
            source = new ByteArrayDataSource(detailFichier, "application/zip",
                nomFichier);
          } else {
            source = new ByteArrayDataSource(detailFichier,
                "application/download", nomFichier);
          }

          partiePieceJointe.setDataHandler(new DataHandler(source));
          partiePieceJointe.setFileName(nomFichier);

          mp.addBodyPart(partiePieceJointe);
        }
      }

      msg.setContent(mp);
      Transport.send(msg);
    } catch (UnsupportedEncodingException e) {
      throw e;
    } catch (MessagingException e) {
      throw e;
    }
  }

  /**
   * Envoyer un message en format texte avec un fichier attachï¿½.
   * <p>
   * Notons que la valeur de retour ne garantit en aucun cas la livraison du message
   * au destinataire. La mï¿½thode retounera faux seulement si le serveur SMTP n'a pas
   * acceptï¿½ de relayer le message.
   * <p>
   * @param sujet le sujet du message
   * @param message le contenu du message
   * @param nomDuFichier le nom du fichier attachï¿½
   * @param fichier un vecteur d'octets contenant les donnï¿½es du fichier
   * @throws java.io.UnsupportedEncodingException Format de l'attachement invalide
   * @throws javax.mail.MessagingException erreur d'envoie du message.
   */
  public void envoyerCourrielTexteAvecAttachment(String sujet, String message,
      String nomDuFichier)
          throws UnsupportedEncodingException, MessagingException {
    envoyerCourrielTexteAvecAttachment(null, null, sujet, message,
        nomDuFichier, (byte[]) null);
  }

  /**
   * Mï¿½thode qui sert ï¿½ ajouter un destinataire dans la liste des CC.
   * <p>
   * @param adresseDestinataire l'adresse courriel du destinataire ï¿½ ajouter
   * @param nomDestinataire le nom du destinataire ï¿½ ajouter
   */
  public void ajouterDestinataireCC(String adresseDestinataire,
      String nomDestinataire) {
    try {
      InternetAddress adresse = new InternetAddress(adresseDestinataire,
          nomDestinataire);

      if (this.listeDestinatairesCC == null) {
        this.listeDestinatairesCC = new ArrayList();
      }

      this.listeDestinatairesCC.add(adresse);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  /**
   * Mï¿½thode qui sert ï¿½ ajouter un destinataire dans la liste des BCC.
   * <p>
   * @param adresseDestinataire l'adresse courriel du destinataire ï¿½ ajouter
   * @param nomDestinataire le nom du destinataire ï¿½ ajouter
   */
  public void ajouterDestinataireBCC(String adresseDestinataire,
      String nomDestinataire) {
    try {
      InternetAddress adresse = new InternetAddress(adresseDestinataire,
          nomDestinataire);

      if (this.listeDestinatairesBCC == null) {
        this.listeDestinatairesBCC = new ArrayList();
      }

      this.listeDestinatairesBCC.add(adresse);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  /**
   * Mï¿½thode qui sert ï¿½ ajouter un destinataire au courriel.
   * <p>
   * @param adresseDestinataire l'adresse courriel du destinataire ï¿½ ajouter
   * @param nomDestinataire le nom du destinataire ï¿½ ajouter
   */
  public void ajouterDestinataire(String adresseDestinataire,
      String nomDestinataire) {
    try {
      InternetAddress adresse = new InternetAddress(adresseDestinataire,
          nomDestinataire);

      if (this.listeDestinataires == null) {
        this.listeDestinataires = new ArrayList();
      }

      this.listeDestinataires.add(adresse);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  /**
   * Mï¿½thode qui sert ï¿½ ajouter une piï¿½ce jointe au courriel.
   * <p>
   * @param adresseDestinataire l'adresse courriel du destinataire ï¿½ ajouter
   * @param nomDestinataire le nom du destinataire ï¿½ ajouter
   */
  public void ajouterPieceJointe(String nomFichier, byte[] pieceJointe) {
    try {
      if (this.listePiecesJointes == null) {
        this.listePiecesJointes = new ArrayList();
      }

      HashMap detailPieceJointe = new HashMap();
      detailPieceJointe.put("nomFichier", nomFichier);
      detailPieceJointe.put("detail", pieceJointe);
      this.listePiecesJointes.add(detailPieceJointe);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Mï¿½thode qui sert ï¿½ transformer le contenu d'une liste en tableau d'objets
   * InternetAddress.
   * <p>
   * @param liste la liste ï¿½ convertir
   * @return un tableau d'objets InternetAddress
   */
  private InternetAddress[] convertirEnTableau(ArrayList liste) {
    if ((liste != null) && (liste.size() > 0)) {
      InternetAddress[] tableau = new InternetAddress[liste.size()];

      for (int i = 0; i < liste.size(); i++) {
        tableau[i] = (InternetAddress) liste.get(i);
      }

      return tableau;
    }

    return null;
  }

  /**
   * Classe de test
   * @param args
   */
  /**
  public static void main(String[] args) {
   Courriel courriel = new Courriel("relais.videotron.ca",
       "pierre-frederick.duret@nurun.com", "P-F");
   courriel.ajouterDestinataire("jean-francois.brassard@nurun.com", "Jeff");
   courriel.ajouterDestinataireCC("pierre-frederick.duret@nurun.com",
     "P-Frederick");
   courriel.ajouterDestinataireBCC("jean-francois.brassard@nurun.com", "Jeff");
   courriel.ajouterDestinataireBCC("jfbrassard@yahoo.com", "Jeff");
   courriel.ajouterDestinataireCC("jfbrassard@videotron.ca", "Jeff");

   try {
     courriel.envoyerCourrielTexte("Test de mail envoyï¿½ par SOFI",
       "Ceci est un courriel envoyï¿½ ï¿½ 100% par SOFI!!! Yï¿½");
   } catch (Exception e) {
     System.out.println(e.getMessage());
   }

   int toto = 2;
  }
   **/
  /**
   * Classe interne dï¿½signant le contenu du courriel.
   * <p>
   * @author Steve Tremblay (Nurun)
   * @version 1.0
   */
  class ByteArrayDataSource implements DataSource {
    /** le contenu du fichier attachï¿½ */
    byte[] bytes;

    /** le MIME type du fichiï¿½ attachï¿½ */
    String contentType = "application/octet-stream";

    /** le nom du fichiï¿½ attachï¿½ */
    String name;

    /** constructeur */
    ByteArrayDataSource(byte[] bytes, String contentType, String name) {
      this.bytes = bytes;
      this.name = name;

      if (contentType != null) {
        this.contentType = contentType;
      }
    }

    /**
     * Obtenir le MIME type du fichiï¿½ attachï¿½.
     * <p>
     * @return String le MIME type du fichiï¿½ attachï¿½
     */
    @Override
    public String getContentType() {
      return contentType;
    }

    /**
     * Obtenir le InputStream correspondant ï¿½ cet objet.
     * <p>
     * @return InputStream le InputStream correspondant ï¿½ cet objet
     */
    @Override
    public InputStream getInputStream() {
      // remove the final CR/LF
      return new ByteArrayInputStream(bytes, 0, bytes.length - 2);
    }

    /**
     * Obtenir le nom du fichiï¿½ attachï¿½.
     * <p>
     * @return String le nom du fichiï¿½ attachï¿½
     */
    @Override
    public String getName() {
      return name;
    }

    /**
     * Lance une exception en tout temps.
     * <p>
     * @exception FileNotFoundException exception lancï¿½e en tout temps
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
      throw new FileNotFoundException();
    }
  }
}
