/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.fichier.FichierTexte;
import org.sofiframework.xml.sax.xerces.XercesSaxParserImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.EncodedByteArrayConverter;
import com.thoughtworks.xstream.io.xml.JDomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;


/**
 * Classe offrant des outils de gestion pour tout ce qui est XML.
 * <p>
 * @author Pierre-Frédérick Duret (Nurun inc.), Jean-Maxime Pelletier
 * @version SOFI 2.0
 */
public class UtilitaireXml {

  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory.getLog(UtilitaireXml.class);

  /**
   * Méthode qui enleve un attribut portant un certain nom.
   * <p>
   * Cette méthode sert à enlever les attributs d'une node de type Element.
   * Si la node n'est pas de type Element, alors rien n'est enlevé. Cette méthode peut
   * s'appeler de manière récursive afin d'enlever tous les attributs portant le nom
   * désigné dans les enfants de la node passée en paramètre.
   * <p>
   * @param node la node principale dans laquelle il faut enlever l'attribut
   * @param nomAttribut le nom de l'attribut à enlever
   * @param enleverDansEnfants valeur indiquant si l'on doit enlever l'attribut dans les enfants de la node principale
   */
  public static void enleverAttribut(Node node, String nomAttribut,
      boolean enleverDansEnfants) {
    // Vérifier si la node est bien de type Element et possède des attributs
    if ((node.getNodeType() == Node.ELEMENT_NODE) && node.hasAttributes()) {
      Element element = (Element) node;
      int nombreAttributs = (node.getAttributes() != null)
          ? node.getAttributes().getLength() : 0;
          Attr[] arrayAttributs = new Attr[nombreAttributs];

          for (int i = 0; i < nombreAttributs; i++) {
            arrayAttributs[i] = (Attr) node.getAttributes().item(i);
          }

          for (int i = 0; i < arrayAttributs.length; i++) {
            Attr attribut = arrayAttributs[i];

            if (attribut.getName().equals(nomAttribut)) {
              element.removeAttributeNode(attribut);
            }
          }
    }

    // Enlever tous les enfants si la valeur "enleverDansEnfants" est à true
    if (enleverDansEnfants && node.hasChildNodes()) {
      NodeList listeNode = node.getChildNodes();

      for (int i = 0; i < listeNode.getLength(); i++) {
        UtilitaireXml.enleverAttribut(listeNode.item(i), nomAttribut, true);
      }
    }
  }

  /**
   * Méthode qui enleve un element portant un certain nom.
   * <p>
   * Cette méthode sert à enlever les Element porant un certain nom d'une node Document.
   * <p>
   * @param node la node principale dans laquelle il faut enlever l'élément
   * @param nomElement le nom de l'élément à enlever
   * @param enleverDansEnfants valeur indiquant si l'on doit enlever l'élément dans les enfants de la node principale
   */
  public static void enleverElement(Node node, String nomElement,
      boolean enleverDansEnfants) {
    // Vérifier si la node est bien de type Element ou Document
    if ((node.getNodeType() == Node.ELEMENT_NODE) ||
        (node.getNodeType() == Node.DOCUMENT_NODE)) {
      NodeList listeNode = node.getChildNodes();

      if (listeNode != null) {
        for (int i = 0; i < listeNode.getLength(); i++) {
          Node nodeATester = listeNode.item(i);

          if ((nodeATester.getNodeType() == Node.ELEMENT_NODE) &&
              nodeATester.getNodeName().equals(nomElement)) {
            node.removeChild(nodeATester);
          }
        }
      }

      // Enlever tous les enfants si la valeur "enleverDansEnfants" est à true
      if (enleverDansEnfants && node.hasChildNodes()) {
        listeNode = node.getChildNodes();

        for (int i = 0; i < listeNode.getLength(); i++) {
          UtilitaireXml.enleverElement(listeNode.item(i), nomElement, true);
        }
      }
    }
  }

  /**
   * Méthode qui converti le contenu d'un FichierTexte en Node XML.
   * <p>
   * Cette méthode transforme le contenu d'un FichierTexte en Node XML
   * afin d'etre "parsée" par d'autres méthodes
   * <p>
   * @param fichier le fichier XML
   * @return Node le document en format XML
   */
  public static Node convertirFichierEnDocument(FichierTexte fichierTexte) {
    Node document = null;

    try {
      File fichier = new File(fichierTexte.getDestination() + "/" +
          fichierTexte.getNomFichier());

      document = convertirFichierEnDocument(new FileInputStream(fichier));
    } catch (Exception ex) {
      throw new SOFIException(
          "Erreur pour convertir un fichier en Document XML.");
    }

    return document;
  }

  /**
   * Méthode qui converti le contenu d'un InputStream en Node XML.
   * <p>
   * Cette méthode transforme le contenu d'un InputStream en Node XML
   * afin d'etre "parsée" par d'autres méthodes
   * <p>
   * @param inputstream un fichier XML en InputStream
   * @return Node le document en format XML
   */
  public static Node convertirFichierEnDocument(InputStream inXML)
      throws Exception {
    DOMParser parser = new DOMParser();
    parser.parse(new InputSource(inXML));

    return parser.getDocument();
  }

  /**
   * Méthode qui retourne une node située dans une Node XML.
   * <p>
   * Cette méthode recherche pour un nom de Node dans une Node XML. La méthode cherche seulement
   * parmis les enfants de la Node passée en paramètres.<BR><BR>
   * <B>Note : </B>Il est important de savoir que le root d'un Document XML est une Node à
   * l'intérieur d'une Node de type DOCUMENT.
   * <p>
   * @param node la node dans lequel cherche pour un Node en particulier
   * @param nomElement le nom de l'élément (Node) à retourner
   * @return la Node qui correspond à l'élément voulu
   */
  public static Node getNodeDansUneNode(Node node, String nomElement) {
    Node nodeARetourner = null;

    if (node.hasChildNodes()) {
      NodeList listeNode = node.getChildNodes();
      boolean trouve = false;

      for (int i = 0; i < listeNode.getLength(); i++) {
        Node nodeATester = listeNode.item(i);

        if ((nodeATester.getNodeType() == Node.ELEMENT_NODE) &&
            nodeATester.getNodeName().equals(nomElement) && !trouve) {
          nodeARetourner = nodeATester;
          trouve = true;
        }
      }
    }

    return nodeARetourner;
  }

  /**
   * Méthode qui recherche pour une liste de Node et qui les retourne dans un ArrayList.
   * <p>
   * Cette méthode regarde tous les enfants de la Node passée en paramètres pour trouver
   * l'élément identifié en paramètre. Lorsque l'élément est trouvé, toutes les Nodes
   * de type ELEMENT sont retournées dans une ArrayList. Afin de trouver la liste représentant une
   * liste d'objet, cette méthode cherche seulement parmis les enfants de la Node mentionnée
   * en paramètres.
   * <p>
   * @param node la Node dans laquelle il faut trouver une liste d'autre Node
   * @param nomElementListe le nom de la node de type ELEMENT qui contient les éléments à mettre dans une liste
   * @return une liste contenant une série d'objets de type Node qui sont en fait tous des Node de type ELEMENT
   */
  public static ArrayList getListeNodeDansUneNode(Node node,
      String nomElementListe) {
    ArrayList listeNodes = new ArrayList();

    // Trouver la node de liste à l'intérieur de la Node passée en paramètres
    Node nodePrincipale = UtilitaireXml.getNodeDansUneNode(node, nomElementListe);

    if (nodePrincipale != null) {
      if (nodePrincipale.hasChildNodes()) {
        NodeList listeDeNodes = nodePrincipale.getChildNodes();

        for (int i = 0; i < listeDeNodes.getLength(); i++) {
          Node nodeATester = listeDeNodes.item(i);

          // Ajouter toutes les Nodes de type ELEMENT dans la liste
          if (nodeATester.getNodeType() == Node.ELEMENT_NODE) {
            listeNodes.add(nodeATester);
          }
        }
      }
    }

    return listeNodes;
  }

  /**
   * Méthode qui valide un Node avec un fichier XSD.
   * <p>
   * Cette méthode sert à valider le contenu d'une Node XML à l'aide d'un fichier XSD.
   * Cette méthode ne fait que vérifier la validité des données, elle ne lance pas les
   * exceptions à la méthode appelante.
   * <p>
   * @param node la Node contenant le XML à valider
   * @param fichierXsd le fichier XSD qui fourni les règles de validation de structure
   * @return boolean valeur indiquant si la Node XML est valide ou non
   */
  public static boolean isValideAvecXsd(Node node, FichierTexte fichierXsd) {
    boolean valide = false;

    try {
      validerAvecXsd(node, fichierXsd);
      valide = true;
    } catch (Exception e) {
    }

    return valide;
  }

  /**
   * Méthode qui valide un Node avec un fichier XSD.
   * <p>
   * Cette méthode sert à valider le contenu d'une Node XML à l'aide d'un fichier XSD.
   * Elle ne retourne aucune valeur, à la place, elle vérifie la validité de l'objet
   * <Node> avec le fichier Xsd et si ce dernier n'est pas conforme, alors une exception
   * est lancée et doit être captée par l'appelant.
   * <p>
   * @param node la Node contenant le XML à valider
   * @param fichierXsd le fichier XSD qui fourni les règles de validation de structure
   * @throws Exception exception lancée quand la validation avec le Xsd rencontre une erreur
   */
  public static void validerAvecXsd(Node node, FichierTexte fichierXsd) throws Exception {
    // Faire un InputStream avec la Node
    ByteArrayInputStream inputStreamXml = new ByteArrayInputStream(UtilitaireXml.convertirEnString(node).getBytes());
    XercesSaxParserImpl parser = new XercesSaxParserImpl();
    parser.setPathXsd(fichierXsd.getDestination() + "/" + fichierXsd.getNomFichier());
    parser.setXml(new InputSource(inputStreamXml));
    parser.valider();
  }

  /**
   * Méthode qui converti une String en Node XML.
   * <p>
   * Il est important pour bien fonctionner que la String possède une ligne ressemblant à celle-ci :<BR>
   * <I>&lt;?xml version = '1.0' encoding = 'UTF-8'?&gt;</B><BR>
   * Cette ligne doit être la première de la String
   * <p>
   * @param stringXml l'objet String représentant un XML
   * @return Node le document en format XML
   */
  public static Node convertirStringEnDocument(String stringXml) {
    Node document = null;

    try {
      ByteArrayInputStream inXml = new ByteArrayInputStream(stringXml.getBytes());
      document = convertirFichierEnDocument(inXml);
    } catch (Exception ex) {
      throw new SOFIException("Erreur XML: " + ex.getMessage());
    }

    return document;
  }

  /**
   * Convertir un objet en une string xml
   * @return un document xml en string.
   * @param objet l'objet en traduire en xml.
   */
  public static String ecrireObjetEnXML(Object objet) {
	XStream xstream = new XStream(new StaxDriver());
    //xstream.registerConverter(new EncodedByteArrayConverter());

    return xstream.toXML(objet);
  }

  /**
   * Convertir un document xml en un objet.
   * @return un document xml en string.
   * @param objet un nouvelle instance d'un objet.
   * @param xml le document xml.
   */
  public static Object genererObjetAvecXML(String xml, Object objet) {
	XStream xstream = new XStream(new StaxDriver());
    //xstream.registerConverter(new EncodedByteArrayConverter());

    return xstream.fromXML(xml, objet);
  }

  /**
   * Permet de remplir une collection de <code>LinkedHashMap</code> avec
   * une liste de node Xml.
   * @return une collection de <code>LinkedHashMap</code>
   * @param listeRapportFinancier la liste à remplir.
   * @param listeValeur la liste de node Xml.
   */
  public static LinkedHashMap construireDocumentXmlEnHashMap(
      NodeList listeNode, LinkedHashMap listeRapportFinancier) {
    for (int i = 0; i < listeNode.getLength(); i++) {
      if (!listeNode.item(i).getNodeName().startsWith("#")) {
        Node noeudCourant = listeNode.item(i);
        String nomNoeud = noeudCourant.getNodeName();
        String valeurNoeud = noeudCourant.getFirstChild().getNodeValue();

        if (valeurNoeud.trim().length() == 0) {
          listeRapportFinancier.put(nomNoeud,
              construireDocumentXmlEnHashMap(noeudCourant.getChildNodes(),
                  new LinkedHashMap()));
        } else {
          listeRapportFinancier.put(nomNoeud, valeurNoeud);
        }
      }
    }

    return listeRapportFinancier;
  }

  /**
   * Permet de remplir une collection de <code>LinkedHashMap</code> avec
   * une liste de node Xml.
   * @return une collection de <code>LinkedHashMap</code>
   * @param listeRapportFinancier la liste à remplir.
   * @param listeValeur la liste de node Xml.
   */
  public static ArrayList construireListeDocumentXmlEnHashMap(
      NodeList listeNode) {
    ArrayList listeValeurXml = new ArrayList();

    for (int i = 0; i < listeNode.getLength(); i++) {
      if (!listeNode.item(i).getNodeName().startsWith("#")) {
        Node noeudCourant = listeNode.item(i);
        String valeurNoeud = noeudCourant.getFirstChild().getNodeValue();

        if (valeurNoeud.trim().length() == 0) {
          listeValeurXml.add(construireDocumentXmlEnHashMap(
              noeudCourant.getChildNodes(), new LinkedHashMap()));
        }
      }
    }

    return listeValeurXml;
  }

  /**
   * Permet de lire un fichier XML qui se trouve dans le classpath a l'endroit
   * spécifier.
   * 
   * L'emplacement doit avoir la syntaxe suivante :
   *    org.sofiframework.exemple.MaClasseQuiSeTrouveALaBonnePlace
   * 
   * Le nom du fichier XML doit commencer par un slash. Exemple : /services.xml
   * 
   * @param nomClasseAvecFichier Nom d'une classe qui se trouve au même niveau
   * que le fichier désiré
   * @param nomFichier Nom du fichier XML
   * @return un document xml
   */
  static public synchronized Node getDocumentXmlDansClassPath(
      String nomClasseAvecFichier, String nomFichier) throws Exception {
    int indiceNomService = nomClasseAvecFichier.lastIndexOf(".");
    StringBuffer nomFichierConfiguration = new StringBuffer();
    //nomFichierConfiguration.append("/");
    nomFichierConfiguration.append(nomClasseAvecFichier.substring(nomClasseAvecFichier.indexOf(
        " ") + 1, indiceNomService).replaceAll("[.]", "/"));
    nomFichierConfiguration.append(nomFichier);

    /* j-m.pelletier : avant on utilisait JBOCLass ...
     */
    InputStream inputstream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream(nomFichierConfiguration.toString());

    Node premierNode = null;

    if (inputstream == null) {
      inputstream = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("/" + nomFichierConfiguration);
    }

    try {
      if (inputstream != null) {
        premierNode = UtilitaireXml.convertirFichierEnDocument(inputstream);
        inputstream.close();
        inputstream = null;
      }
    } catch (Exception exception) {
      throw exception;
    }

    return premierNode;
  }

  /**
   * Méthode qui transforme l'objet courant en un fichier XML.
   * <p>
   * Cette méthode converti tous les attributs de l'objet courrant en une structure XML
   * <p>
   * @return Document le XML représentant l'objet
   */
  public static Node ecrireXml(ObjetTransfert objetTransfert) {
    Node document = null;

    try {
      StringBuffer fichierXml = new StringBuffer("<?xml version = '1.0' encoding = '");
      fichierXml.append(getFormatEncodage());
      fichierXml.append("'?>");
      fichierXml.append(objetTransfert.toString());

      document = convertirFichierEnDocument(new ByteArrayInputStream(
          fichierXml.toString().getBytes()));
    } catch (Exception e) {
      throw new SOFIException("Erreur pour ecrir un objet de transfert en XML");
    }

    return document;
  }

  public static void lireXml(ObjetTransfert objetDestination, Node node) {
    XStream xstream = new XStream(new StaxDriver());
    //xstream.registerConverter(new EncodedByteArrayConverter());

    try {
      String xml = UtilitaireXml.convertirEnString(node);
      HashMap alias = objetDestination.getAliasXml();

      if (alias != null) {
        Iterator iterateur = alias.keySet().iterator();

        while (iterateur.hasNext()) {
          String nomAttribut = (String) iterateur.next();
          Class classe = (Class) alias.get(nomAttribut);
          xstream.alias(nomAttribut, classe);
        }
      }

      // On a pas besoin de creer un nouvel objet on peut juste copier
      xstream.fromXML(xml, objetDestination);

      //UtilitaireObjet.copyProperties(objetConvertiDuXML, objetDestination);

      /**
       * j-m.pelletier :
       * Pourquoi on changerait des types si a lorigine ils etaient type Oracle...pourquoi on devrait mettre autre chose dans l'objet.
       */

      //this.populerAvecObjetTransfert(objetDestination, objetConvertiDuXML);
    } catch (Exception ex) {
      /*System.out.println(
        "Impossible de convertir l'objet Document en ObjetTransfert.");
      ex.printStackTrace();*/
      throw new SOFIException("Impossible de convertir le XML en objet de transfert.",
          ex);
    }
  }

  /**
   * Méthode qui transforme une Node XML en une string.
   * <p>
   * @param node la node XML à convertir en String
   * @return l'objet String correspondant au Document passé en paramètre
   */
  public static String convertirEnString(Node node) {
    String resultat = null;

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      OutputFormat format = new OutputFormat();
      format.setEncoding(getFormatEncodage());
      format.setIndenting(true);

      XMLSerializer serializer = new XMLSerializer(out, format);

      if (node instanceof Document) {
        serializer.serialize((Document) node);
      } else if (node instanceof Element) {
        serializer.serialize((Element) node);
      }

      resultat = out.toString();
    } catch (Exception e) {
      throw new SOFIException("Erreur pour convertir un document en String", e);
    }

    return resultat;
  }

  /**
   * Méthode qui transforme en XML le contenu de l'objet.
   * <p>
   * @return String le XML
   */
  public static String toXML(ObjetTransfert objet) {
	XStream xstream = new XStream(new StaxDriver());
    //xstream.registerConverter(new EncodedByteArrayConverter());

    Map alias = objet.getAliasXml();

    // Appliquer les différents alias s'il y en a
    if (alias != null) {
      Iterator iterateur = alias.keySet().iterator();

      while (iterateur.hasNext()) {
        String nomAttribut = (String) iterateur.next();
        Class classe = (Class) alias.get(nomAttribut);
        xstream.alias(nomAttribut, classe);
      }
    }

    return xstream.toXML(objet);
  }

  /**
   * Obtenir l'encoding d'un XML. Retourne l'encoding déclaré
   * dans le document. Non pas le charset de la
   * chaîne de caractère.
   * 
   * Exemple : <?xml version="1.0" encoding="UTF-8" ?>.
   * 
   * @param xml chaîne de caractère xml.
   * @return encoding
   */
  public static String getEncoding(String xml) {
    String paramEncoding = "encoding=\"";
    int  debut = xml.indexOf(paramEncoding) + paramEncoding.length();
    int fin = xml.indexOf("\"", debut);
    return xml.substring(debut, fin);
  }

  /**
   * Obtenir l'encoding d'un XML. Retourne l'encoding déclaré
   * dans le document.
   * La châine doit au moin contenir la section de fichier
   * ci-dessous.
   * 
   * Exemple : <?xml version="1.0" encoding="UTF-8" ?>.
   * 
   * @param xml chaîne de caractère xml.
   * @return encoding
   */
  public static String getEncoding(byte[] bytes) {
    return UtilitaireXml.getEncoding(new String(ArrayUtils.subarray(bytes, 0, 40)));
  }

  /**
   * Retourne le format d'encodage à utiliser par défaut.
   * <p>
   * Il est possible de configurer ce format avec le paramètre 'formatEncodageXml'.
   * Spécifier 'ISO-8859-1' ou 'UTF-8', par défaut 'UTF-8'.
   * @return le format
   */
  public static String getFormatEncodage() {
    String formatEncodage = GestionParametreSysteme.getInstance().getString(ConstantesParametreSysteme.FORMAT_ENCODAGE_XML);
    if (UtilitaireString.isVide(formatEncodage)) {
      log.info("Aucun parametre pour 'formatEncodageXml', le format utiliser est :" + formatEncodage);
    }
    return formatEncodage;
  }
}
