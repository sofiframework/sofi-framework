/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.text.Collator;
import java.util.Comparator;


/**
 * Comme rien nous garantie que chaque valeur est différente on doit
 * trainer la clé de la valeur. On crée donc des
 * paires new Object()[]{cle, valeur}
 * Pour trier les propriés on doit délaiguer a ub cmparator costum si il existe.
 * Dans le cas contraire les valeurs doivent implémenter
 * Comparable.
 *
 * @author Jean-Maxime Pelletier
 */
public class ComparateurPaire implements Comparator {
  private Comparator comparateurValeur = null;
  private Collator collator = null;

  /**
   * Comparer les valeurs à l'aide d'un
   * comparateur personnalisé.
   * @param comparateurValeur
   */
  public ComparateurPaire(Comparator comparateurValeur) {
    this.comparateurValeur = comparateurValeur;
  }

  /**
   * Créer un Comparateur en spécifiant la précision du tri. Il doit s'agit d'ordonner
   * les valeurs String directement.
   * @param collator
   */
  public ComparateurPaire(Collator collator) {
    this.collator = collator;
  }

  @Override
  public int compare(Object o1, Object o2) {
    /*
     * Chaque objet est une paire clé valeur
     */
    Object[] paire1 = (Object[]) o1;
    Object[] paire2 = (Object[]) o2;

    int resultat = 0;

    if (comparateurValeur == null) {
      resultat = UtilitaireListe.compareValeur(paire1[1], paire2[1], collator);
    } else {
      /*
       * On délaigue le tri au comparator en passant les valeurs
       */
      resultat = comparateurValeur.compare(paire1[1], paire2[1]);
    }

    return resultat;
  }
}
