/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.sofiframework.exception.SOFIException;


/**
 * Classe offrant des services statiques pour la manipulation des dates.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 * @see java.sql.Date
 * @see java.util.Date
 * @see java.sql.Timestamp
 */
public class UtilitaireDate {
  /** Constante pour le format de date année-mois-jour */
  public static int FORMAT_AAAA_MM_JJ = 0;

  /** Constante pour le format de date année mois jour sans espacement */
  public static int FORMAT_AAAAMMJJ = 1;

  /** Constante pour le format de date jour-mois-année */
  public static int FORMAT_JJ_MM_AAAA = 2;

  /** Constante pour le format de date jour-mois-année sans espacement */
  public static int FORMAT_JJMMAAAA = 3;

  /** Une journée en miliseconde **/
  public static final long JOUR_EN_MILIS = 24 * 60 * 60 * 1000;

  /** Format d'une date **/
  public static final String FORMAT_DATE = "yyyy-MM-dd";

  /** Format d'une date sans séparateur "-" **/
  public static final String FORMAT_DATE_SANS_SEPARATEUR = "yyyyMMdd";

  /** Format d'une date avec l'heure **/
  public static final String FORMAT_DATE_HEURE_MINUTE = "yyyy-MM-ddHH:mm";

  /** Format d'une date avec l'heure, minute et seconde **/
  public static final String FORMAT_DATE_HEURE_MINUTE_SECONDE = "yyyy-MM-ddHH:mm:ss";

  /**
   * Obtenir le nombre d'heures et minutes ouvrables séparant deux dates.
   * <p>
   * @param d1 la première date
   * @param d2 la deuxième date
   * @return String le nombre d'heures et de minutes en format HH:MM
   */
  public static String getNbHeuresOuvrablesEntreDeuxDates(java.util.Date d1,
      java.util.Date d2) {
    Calendar dc1 = Calendar.getInstance();
    Calendar dc2 = Calendar.getInstance();

    if (d2.after(d1)) {
      dc2.setTime(d2);
      dc1.setTime(d1);
    } else {
      dc2.setTime(d1);
      dc1.setTime(d2);
    }

    int heures = (23 - dc1.get(Calendar.HOUR_OF_DAY));
    int minutes = (59 - dc1.get(Calendar.MINUTE));
    dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) + 1);
    dc1.set(Calendar.HOUR_OF_DAY, 00);
    dc1.set(Calendar.MINUTE, 00);

    while (dc1.before(dc2)) {
      if ((dc1.get(Calendar.YEAR) == dc2.get(Calendar.YEAR)) &&
          (dc1.get(Calendar.MONTH) == dc2.get(Calendar.MONTH)) &&
          (dc1.get(Calendar.DAY_OF_MONTH) == dc2.get(Calendar.DAY_OF_MONTH))) {
        dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) + 1);
        heures = heures + dc2.get(Calendar.HOUR_OF_DAY);
        minutes = minutes + dc2.get(Calendar.MINUTE);
        heures = heures + (minutes / 60);
        minutes = minutes % 60;
      } else {
        if ((dc1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) ||
            (dc1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
          dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) + 1);
        } else {
          dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) + 1);
          heures = heures + 24;
        }
      }
    }

    return heures + ":" + minutes;
  }

  /**
   * Obtenir le nombre de jours entre deux dates.
   * <p>
   * @param d1 la première date
   * @param d2 la deuxième date
   * @return int le nombre de jours entre les deux dates
   */
  public static int getNbJoursEntreDeuxDates(java.util.Date d1,
      java.util.Date d2) {
    int elapsed = 0;
    Calendar dc1 = Calendar.getInstance();
    Calendar dc2 = Calendar.getInstance();

    if (d2.after(d1)) {
      dc2.setTime(d2);
      dc1.setTime(d1);
    } else {
      dc2.setTime(d1);
      dc1.setTime(d2);
    }

    while (dc1.before(dc2)) {
      elapsed++;
      dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) + 1);
    }

    return elapsed;
  }

  /**
   * Obtenir la date de la journée un certain nombre de jours avant la date du jour.
   * <p>
   * @param nbJoursAvant le nombre de jours
   * @return String la date calculée
   */
  public static String getDateAvantDateJourEnFormat_AAAA_MM_JJ(int nbJoursAvant) {
    Calendar dc1 = Calendar.getInstance();
    Timestamp dateDuJour = UtilitaireDate.getDateJourAvecHeure();
    dc1.setTime(new java.util.Date(dateDuJour.getTime()));

    for (int i = 0; i < nbJoursAvant; i++) {
      dc1.set(Calendar.DAY_OF_MONTH, dc1.get(Calendar.DAY_OF_MONTH) - 1);
    }

    String annee = new Integer(dc1.get(Calendar.YEAR)).toString();
    String mois = new Integer(dc1.get(Calendar.MONTH) + 1).toString();
    String jour = new Integer(dc1.get(Calendar.DAY_OF_MONTH)).toString();

    return UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour(annee, mois, jour);
  }

  /**
   * Convertir une java.sql.Date en format texte AAAA-MM-JJ.
   * <p>
   * @param date la date à convertir
   * @return String la date en format en format texte AAAA-MM-JJ
   */
  public static String convertirEn_AAAA_MM_JJ(java.sql.Date date) {
    if (date != null) {
      String mois = "";
      String jour = "";
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);

      if ((calendar.get(Calendar.MONTH) + 1) < 10) {
        mois = "0" + String.valueOf(calendar.get(Calendar.MONTH) + 1);
      } else {
        mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
      }

      if ((calendar.get(Calendar.DATE)) < 10) {
        jour = "0" + String.valueOf(calendar.get(Calendar.DATE));
      } else {
        jour = String.valueOf(calendar.get(Calendar.DATE));
      }

      return String.valueOf(calendar.get(Calendar.YEAR)) + "-" + mois + "-" +
      jour;
    }

    return null;
  }

  /**
   * Convertir une java.util.Date avec les heures en une java.util.Date sans heures.
   * <p>
   * @param pDate la date à convertir
   * @return java.util.Date la date sans les heures
   */
  public static java.util.Date enleverHeures(java.util.Date date) {
    return new Date(enleverHeures(date.getTime()));
  }

  /**
   * Verifier si une date est la date du jour.
   * <p>
   * @param date la date à vérifier
   * @return boolean vrai si la date est celle du jour sinon faux
   */
  public static boolean isAujourdHui(java.util.Date date) {
    long aujourdhui = enleverHeures(System.currentTimeMillis());

    return enleverHeures(date.getTime()) == aujourdhui;
  }

  /**
   * Convertir une date texte en format AAAA-MM-JJ en java.sql.Date.
   * <p>
   * @param dateAvecTraitsUnions la date à convertir
   * @return java.sql.Date la date convertie
   */
  public static java.sql.Date convertir_AAAA_MM_JJ_En_SqlDate(
      String dateAvecTraitsUnions) {
    if ((dateAvecTraitsUnions != null) && (dateAvecTraitsUnions.length() > 0)) {
      java.util.Date date = convertir_AAAA_MM_JJ_En_UtilDate(dateAvecTraitsUnions);

      return new java.sql.Date(date.getTime());
    }

    return null;
  }

  /**
   * Obtenir la date du jour en format AAAA-MM-JJ.
   * <p>
   * @return String la date du jour en format AAAA-MM-JJ
   */
  public static String getDateJourEn_AAAA_MM_JJ() {
    Calendar monCalendrier = Calendar.getInstance();
    String annee = String.valueOf(monCalendrier.get(Calendar.YEAR));
    String mois = String.valueOf((monCalendrier.get(Calendar.MONTH)) + 1);
    String jour = String.valueOf(monCalendrier.get(Calendar.DAY_OF_MONTH));

    return UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour(annee, mois, jour);
  }

  /**
   * Convertir une date texte en format AAAA-MM-JJ en format long JJ-MOIS-AAAA.
   * <p>
   * Exemple : 12-04-2002 -> 12 avril 2002
   * <p>
   * @param formatCourt la date en format AAAA-MM-JJ
   * @return String la date en format JJ-MOIS-AAAA
   */
  public static String convertir_AAAA_MM_JJ_En_JJ_MONTH(String formatCourt) {
    String formatLong = String.valueOf(Integer.valueOf(formatCourt.substring(
        8, 10)).intValue());

    if (formatLong.equals("1")) {
      formatLong = "1er";
    }

    int mois = Integer.valueOf(formatCourt.substring(5, 7)).intValue();
    formatLong = formatLong + " " + UtilitaireDate.convertir_MM_En_MONTH(mois) +
        " ";
    formatLong = formatLong + formatCourt.substring(0, 4);

    return formatLong;
  }

  /**
   * Obtenir le nom d'un mois à partir de son numéro.
   * <p>
   * Exemple : 4 -> avril
   * <p>
   * @param moisInt le numéro du mois de 1 à 12
   * @return String le mois en format texte
   */
  public static String convertir_MM_En_MONTH(int moisInt) {
    switch (moisInt) {
    case 1:
      return "janvier";

    case 2:
      return "février";

    case 3:
      return "mars";

    case 4:
      return "avril";

    case 5:
      return "mai";

    case 6:
      return "juin";

    case 7:
      return "juillet";

    case 8:
      return "août";

    case 9:
      return "septembre";

    case 10:
      return "octobre";

    case 11:
      return "novembre";

    case 12:
      return "décembre";

    default:
      return " ";
    }
  }

  /**
   * Convertir une date texte en format AAAA-MM-JJ en format MOIS-AAAA.
   * <p>
   * Exemple : 2003-04-12 -> Avril 2003
   * <p>
   * @param formatCourt la date en format AAAA-MM-JJ
   * @return String le mois et l'année de la date en format MOIS-AAAA
   */
  public static String convertir_AAAA_MM_JJ_En_MONTHAnnee(String formatCourt) {
    String formatLong = "";
    int mois = Integer.valueOf(formatCourt.substring(5, 7)).intValue();
    formatLong = formatLong.concat(convertir_MM_En_MONTH(mois));
    formatLong = formatLong.concat(" ");
    formatLong = formatLong.concat(formatCourt.substring(0, 4));

    return formatLong;
  }

  /**
   * Créer une date en format AAAA-MM-JJ à partir d'une année, d'un mois et d'un jour.
   * <p>
   * @param annee l'année
   * @param mois le mois
   * @param jour le jour
   * @return String la date en format AAAA-MM-JJ
   */
  public static String creer_AAAA_MM_JJ_De_AnneeMoisJour(String annee,
      String mois, String jour) {
    if (mois.length() == 1) {
      mois = "0".concat(mois);
    }

    if (jour.length() == 1) {
      jour = "0".concat(jour);
    }

    return annee + "-" + mois + "-" + jour;
  }

  /**
   * Convertir une date en foramt AAAA-MM-JJ en java.util.Date.
   * <p>
   * @param formatCourt la date en format AAAA-MM-JJ
   * @return java.util.Date la date convertie
   * @return
   */
  public static java.util.Date convertir_AAAA_MM_JJ_En_UtilDate(
      String formatCourt) {
    return convertirEn_UtilDate(formatCourt, FORMAT_DATE);
  }

  /**
   * Convertir une chaîne de caractère date sans tiret en java.util.Date
   * @param formatCourt
   */
  public static java.util.Date convertir_AAAAMMJJ_En_UtilDate(
      String formatCourt) {
    return convertirEn_UtilDate(formatCourt, FORMAT_DATE_SANS_SEPARATEUR);
  }

  /**
   * Converti une date en format texte en type java.util.Date.
   * @return une instante de date de type java.util.Date
   * @param format le format de date requise.
   * @param date la date en format texte.
   */
  public static java.util.Date convertirEn_UtilDate(String date, String format) {
    String message = "Erreur lors de la convertion de la date '" + date +
        "' au format '" + format + "'";

    try {
      DateFormat df = new SimpleDateFormat(format);

      java.util.Date dateResultat = null;

      if (!UtilitaireString.isVide(date)) {
        dateResultat = df.parse(date);
      } else {
        return null;
      }

      String dateResultatFormatte = null;

      if (date.length() <= 10) {
        dateResultatFormatte = convertirEn_AAAA_MM_JJ(dateResultat);
      } else {
        if (date.length() >= 16) {
          dateResultatFormatte = convertirEn_AAAA_MM_JJHeureSeconde(dateResultat);
        } else {
          dateResultatFormatte = convertirEn_AAAA_MM_JJHeure(dateResultat);

          //Règle un bug avec le format yyyy:MM:ddHH:mm sans espace
          if (date.length() == 15) {
            dateResultatFormatte = dateResultatFormatte.substring(0, 10) +
                dateResultatFormatte.substring(11, 16);
          }
        }
      }

      if (format.indexOf("-") == -1) {
        dateResultatFormatte = dateResultatFormatte.replaceAll("-", "");
      }

      if (!dateResultatFormatte.equals(date)) {
        // Lancer une exception si la date de sortie n'égale pas la date d'entrée en format texte.
        throw new SOFIException(message, null);
      }

      return df.parse(date);
    } catch (ParseException e) {
      /**@todo Quel type d'exception lancer? */
      throw new SOFIException(message, e);
    }
  }

  /**
   * Convertir une date et une heure en format AAAA-MM-JJ en java.sql.Timestamp.
   * <p>
   * @param dateCourte la date en format AAAA-MM-JJ
   * @param heureCourte l'heure en format HH:MM
   * @return java.sql.Timestamp la date et l'heure converties
   */
  public static Timestamp convertir_AAAA_MM_JJ_HH_MM_En_Timestamp(
      String dateCourte, String heureCourte) {
    int jour = Integer.valueOf(dateCourte.substring(8)).intValue();
    int mois = Integer.valueOf(dateCourte.substring(5, 7)).intValue();
    int annee = Integer.valueOf(dateCourte.substring(0, 4)).intValue();
    int heure = Integer.valueOf(heureCourte.substring(0, 2)).intValue();
    int minute = Integer.valueOf(heureCourte.substring(3, 5)).intValue();
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DATE, jour);
    calendar.set(Calendar.MONTH, mois - 1);
    calendar.set(Calendar.YEAR, annee);

    if (heure > 12) {
      calendar.set(Calendar.AM_PM, Calendar.PM);
      heure = heure - 12;
    } else {
      calendar.set(Calendar.AM_PM, Calendar.AM);
    }

    calendar.set(Calendar.HOUR, heure);
    calendar.set(Calendar.MINUTE, minute);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return new Timestamp(calendar.getTime().getTime());
  }

  /**
   * Convertir une date et une heure dans le format AAAA-MM-JJ HH:MM:SS en java.util.Date.
   * <p>
   * @param dateCourte la date en format AAAA-MM-JJ
   * @param heureSeconde l'heure en format HH:MM:SS
   * @return java.util.Date la date et l'heure converties
   */
  public static java.util.Date convertir_AAAA_MM_JJ_HH_MM_SS_En_UtilDate(
      String dateCourte, String heureSeconde) {
    return convertirEn_UtilDate(dateCourte + heureSeconde,
        FORMAT_DATE_HEURE_MINUTE_SECONDE);
  }

  /**
   * Convertir une date et une heure dans le format AAAA-MM-JJ HH:MM:SS en java.util.Date.
   * <p>
   * @param dateAvecHeure la date en format AAAA-MM-JJHH:MM:SS
   * @return java.util.Date la date et l'heure converties
   */
  public static java.util.Date convertir_AAAA_MM_JJ_HH_MM_SS_En_UtilDate(
      String dateAvecHeure) {
    return convertirEn_UtilDate(dateAvecHeure.trim(),
        FORMAT_DATE_HEURE_MINUTE_SECONDE);
  }

  /**
   * Convertir une date et une heure en format AAAA-MM-JJ en java.sql.Timestamp.
   * <p>
   * @param dateCourte la date en format AAAA-MM-JJ
   * @param heureSeconde l'heure en format HH:MM:SS
   * @return java.sql.Timestamp la date et l'heure converties
   */
  public static Timestamp convertir_AAAA_MM_JJ_HH_MM_SS_En_Timestamp(
      String dateCourte, String heureSeconde) {
    java.util.Date dateHeure = convertir_AAAA_MM_JJ_HH_MM_SS_En_UtilDate(dateCourte,
        heureSeconde);

    return new Timestamp(dateHeure.getTime());
  }

  /**
   * Convertir une date et une heure en format AAAA-MM-JJ en java.util.Date.
   * <p>
   * @param dateCourte la date en format AAAA-MM-JJ
   * @param heureCourte l'heure en format HH:MM
   * @return java.util.Date la date et l'heure converties
   */
  public static java.util.Date convertir_AAAA_MM_JJ_HH_MM_En_DATE(
      String dateCourte, String heureCourte) {
    return convertirEn_UtilDate(dateCourte + heureCourte,
        FORMAT_DATE_HEURE_MINUTE);
  }

  /**
   * Obtenir la date du jour moins un an en format AAAA-MM-JJ.
   * <p>
   * @return String la date du jour moins un an en format AAAA-MM-JJ
   */
  public static String getDateJourAnneePrecedenteEn_AAAA_MM_JJ() {
    Calendar monCalendrier = Calendar.getInstance();
    String annee = String.valueOf(monCalendrier.get(Calendar.YEAR));
    String mois = String.valueOf((monCalendrier.get(Calendar.MONTH)) + 1);
    String jour = String.valueOf(monCalendrier.get(Calendar.DAY_OF_MONTH));
    annee = String.valueOf(Integer.parseInt(annee) - 1);

    return UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour(annee, mois, jour);
  }

  /**
   * Obtenir la date du jour moins un mois en format AAAA-MM-JJ.
   * <p>
   * @return String la date du jour moins un mois en format AAAA-MM-JJ
   */
  public static String getDateJourMoisPrecedentEn_AAAA_MM_JJ() {
    Calendar monCalendrier = Calendar.getInstance();
    String annee = String.valueOf(monCalendrier.get(Calendar.YEAR));
    String mois = String.valueOf((monCalendrier.get(Calendar.MONTH)) + 1);
    String jour = String.valueOf(monCalendrier.get(Calendar.DAY_OF_MONTH));

    mois = new Integer(Integer.valueOf(mois).intValue() - 1).toString();

    if (mois.compareTo("0") == 0) {
      mois = "12";
      annee = new Integer(Integer.valueOf(annee).intValue() - 1).toString();
    }

    if ((mois.compareTo("2") == 0) && (Integer.valueOf(jour).intValue() >= 29)) {
      jour = "28";
    }

    if ((mois.compareTo("4") == 0) || (mois.compareTo("6") == 0) ||
        (mois.compareTo("9") == 0) || (mois.compareTo("11") == 0)) {
      if (Integer.valueOf(jour).intValue() == 31) {
        jour = "30";
      }
    }

    return UtilitaireDate.creer_AAAA_MM_JJ_De_AnneeMoisJour(annee, mois, jour);
  }

  /**
   * Convertir une java.util.Date en format AAAA-MM-JJ.
   * <p>
   * @param date la date en format java.util.Date
   * @return String la date en format AAAA-MM-JJ
   */
  public static String convertirEn_AAAA_MM_JJ(java.util.Date date) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    return df.format(date);
  }

  /**
   * Convertir une java.util.Date en format AAAA-MM-JJ HH:MM.
   * <p>
   * @param date la date en format java.util.Date
   * @return String la date en format AAAA-MM-JJ HH:MM
   */
  public static String convertirEn_AAAA_MM_JJHeure(java.util.Date date) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    return df.format(date);
  }

  /**
   * Est-ce que la date inclus des heures, minutes et/ou seconde?
   * @param date la date à tester
   * @return true si la date inclus des heures, minutes et/ou seconde
   */
  public static boolean isHeureMinuteSecondeIncluse(java.util.Date date) {
    if (date != null) {
      long millis = date.getTime();

      return enleverHeures(millis) != millis;
    } else {
      return false;
    }
  }

  /**
   * Convertir une java.util.Date en format AAAA-MM-JJ HH:MM:SS.
   * <p>
   * @param date la date en format java.util.Date
   * @return String la date en format AAAA-MM-JJ HH:MM:SS
   */
  public static String convertirEn_AAAA_MM_JJHeureSeconde(java.util.Date date) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    return df.format(date);
  }

  /**
   * Obtenir la date du jour dans les heures en foramt java.util.Date.
   * <p>
   * @return java.util.Date la date du jour sans les heures
   */
  public static java.util.Date getDateJourSansHeure() {
    long milis = System.currentTimeMillis();
    milis += TimeZone.getDefault().getOffset(milis);
    milis -= (milis % JOUR_EN_MILIS);
    milis -= TimeZone.getDefault().getOffset(milis);

    return new Date(milis);
  }

  public static long enleverHeures(long millis) {
    millis += TimeZone.getDefault().getOffset(millis);
    millis -= (millis % JOUR_EN_MILIS);
    millis -= TimeZone.getDefault().getOffset(millis);

    return millis;
  }

  /**
   * Obtenir la date du jour avec les heures en format java.sql.Timestamp.
   * <p>
   * @return java.sql.Timestamp la date du jour avec les heures
   */
  public static java.sql.Timestamp getDateJourAvecHeure() {
    return new Timestamp(System.currentTimeMillis());
  }

  /**
   * Inverse l'année et le jour d'une date en format texte.
   * <p>
   * Exemples :<BR>
   * 2003-04-12 -> 12-04-2003<BR>
   * 20030412 -> 12042003<BR>
   * 12-04-2003 -> 2003-04-12<BR>
   * 12042003 -> 20030412<BR>
   * <p>
   * @param date la date à convertir
   * @param formatEntree le format dans lequel la date est passée (voir les constantes de format)
   * @return String la date avec l'année et le jour inversés
   */
  public static String modification_InverserAnneeEtJour(String date,
      int formatEntree) {
    String dateInversee = "";

    if (formatEntree == FORMAT_AAAAMMJJ) {
      dateInversee = dateInversee.concat(date.substring(6));
      dateInversee = dateInversee.concat(date.substring(4, 6));
      dateInversee = dateInversee.concat(date.substring(0, 4));

      return dateInversee;
    }

    if (formatEntree == FORMAT_AAAA_MM_JJ) {
      dateInversee = dateInversee.concat(date.substring(8));
      dateInversee = dateInversee.concat(date.substring(5, 7));
      dateInversee = dateInversee.concat(date.substring(0, 4));

      return modification_AjouterTraitsUnions(dateInversee, FORMAT_JJMMAAAA);
    }

    if (formatEntree == FORMAT_JJ_MM_AAAA) {
      dateInversee = dateInversee.concat(date.substring(6));
      dateInversee = dateInversee.concat(date.substring(3, 5));
      dateInversee = dateInversee.concat(date.substring(0, 2));

      return modification_AjouterTraitsUnions(dateInversee, FORMAT_AAAAMMJJ);
    }

    if (formatEntree == FORMAT_JJMMAAAA) {
      dateInversee = dateInversee.concat(date.substring(4));
      dateInversee = dateInversee.concat(date.substring(2, 4));
      dateInversee = dateInversee.concat(date.substring(0, 2));

      return dateInversee;
    }

    return date;
  }

  /**
   * Enlever les traits d'union d'une date.
   * <p>
   * Exemples :<BR>
   * 2003-04-12 -> 20030412<BR>
   * 12-04-2003 -> 12042003<BR>
   * <p>
   * @param dateAvecTraitsUnions la date dont on veut enlever les traits d'unions
   * @return String la date sans les traits d'unions
   */
  public static String modification_EnleverTraitsUnions(
      String dateAvecTraitsUnions) {
    StringBuffer dateSansTraitsUnions = new StringBuffer();

    while (dateAvecTraitsUnions.indexOf("-") != -1) {
      dateSansTraitsUnions.append(dateAvecTraitsUnions.substring(0,
          dateAvecTraitsUnions.indexOf("-")));
      dateAvecTraitsUnions = dateAvecTraitsUnions.substring(dateAvecTraitsUnions.indexOf(
          "-") + 1);
    }

    dateSansTraitsUnions.append(dateAvecTraitsUnions);

    return dateSansTraitsUnions.toString();
  }

  /**
   * Ajouter les traits d'unions à une date en format texte.
   * <p>
   * Exemples :<BR>
   * 20030412 -> 2003-04-12<BR>
   * 12042003 -> 12-04-2003<BR>
   * <p>
   * @param dateSansTraitsUnions la date à laquelle on veut ajouter les traits d'unions
   * @param formatEntree le format dans lequel la date est passée (voir les constantes de format)
   * @return String la date avec les traits d'unions ajoutés
   */
  public static String modification_AjouterTraitsUnions(
      String dateSansTraitsUnions, int formatEntree) {
    String dateAvecTraitsUnions = "";

    if (formatEntree == FORMAT_AAAAMMJJ) {
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          0, 4));
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat("-");
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          4, 6));
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat("-");
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          6));

      return dateAvecTraitsUnions;
    }

    if (formatEntree == FORMAT_JJMMAAAA) {
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          0, 2));
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat("-");
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          2, 4));
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat("-");
      dateAvecTraitsUnions = dateAvecTraitsUnions.concat(dateSansTraitsUnions.substring(
          4));

      return dateAvecTraitsUnions;
    }

    return dateSansTraitsUnions;
  }

  /**
   * Obtenir l'heure courante en format HH:MM.
   * <p>
   * @return String l'heure courante en format HH:MM
   */
  public static String getHeurePresente_HH_MM() {
    SimpleDateFormat df = new SimpleDateFormat("HH:mm");

    return df.format(new java.util.Date());
  }

  /**
   * Vérifier si une date est valide.
   * <p>
   * @param myDate la date en format AAAA-MM-JJ
   * @return boolean un indicateur si la date est valide ou non
   */
  public static boolean validDate(String myDate) {
    String dateConvertit = "";

    try {
      java.sql.Date dateSql = convertir_AAAA_MM_JJ_En_SqlDate(myDate);
      dateConvertit = convertirEn_AAAA_MM_JJ(dateSql);
    } catch (Exception e) {
      return false;
    }

    if (myDate.compareTo(dateConvertit) == 0) {
      return true;
    }

    return false;
  }

  /**
   * Valider une heure en format HH:MM.
   * <p>
   * @param heureMinute l'heure en format HH:MM
   * @return boolean un indicateur si l'heure est valide ou non
   */
  public static boolean valideFormatHeure(String heureMinute) {
    if (heureMinute.length() != 5) {
      return false;
    } else {
      String heure = heureMinute.substring(0, 2);
      String minute = heureMinute.substring(3);

      if (!((heure.compareTo("00") >= 0) && (heure.compareTo("23") <= 0))) {
        return false;
      }

      if (!((minute.compareTo("00") >= 0) && (minute.compareTo("59") <= 0))) {
        return false;
      }

      if (heureMinute.substring(2, 3).compareTo(":") != 0) {
        return false;
      }
    }

    return true;
  }

  /**
   * Obtenir le lundi précédent de la date passée en paramètre.
   * </p>
   * @param date la date dont on veut obtenir le lundi précédent
   * @return la date du lundi précédent la date passée
   */
  public static String getLundiPourUneDate(String date) {
    java.sql.Date dateDeDebut = UtilitaireDate.convertir_AAAA_MM_JJ_En_SqlDate(date);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dateDeDebut);

    int jour = calendar.get(Calendar.DAY_OF_WEEK);

    if (jour == Calendar.SUNDAY) {
      calendar.add(Calendar.DATE, -6);
    }

    if ((jour != Calendar.SUNDAY) && (jour != Calendar.MONDAY)) {
      int jourDiffAvecLundi = jour - 2;
      calendar.add(Calendar.DATE, -jourDiffAvecLundi);
    }

    return UtilitaireDate.convertirEn_AAAA_MM_JJ(calendar.getTime());
  }

  /**
   * Obtenir l'heure en format HH:MM d'un temps en format long.
   * <p>
   * @param longDate le temps en format long
   * @return String l'heure en format HH:MM
   */
  public static String getHeureHH_MM(long longDate) {
    java.util.Date date = new Date(longDate);

    return convertirEn_AAAA_MM_JJHeure(date).substring(11, 16);
  }

  /**
   * Obtenir l'heure et la date en format AAAA-MM-JJ HH:MM depuis un temps en format long.
   * <p>
   * @param longDate le temps en format long
   * @return String l'heure en format HH:MM
   */
  public static String getDateHeureHH_MM(long longDate) {
    java.util.Date date = new Date(longDate);

    return convertirEn_AAAA_MM_JJHeure(date);
  }

  /**
   * Obtenir le premier jour du mois passé en paramètre.
   * <p>
   * @param date la date dont on veut le premier jour du mois
   * @return String la première journee du mois de la date
   */
  public static String getPremierJourDuMois(String date) {
    String premierDuMois = date.substring(0, 8);
    premierDuMois = premierDuMois.concat("01");

    return premierDuMois;
  }

  /**
   * Obtenir la date du jour plus ou moins un certain nombre d'heures moins 1 minute.
   * <p>
   * @param dateDebut la date à partir de laquelle on veut ajouter les heures
   * @param nbHeures le nombre d'heures à soustraire ou à ajouter à la date du jour
   * @param moinsUneMinute true si on veut soustraire une minute, false sinon
   * @return java.sql.Timestamp la date du jour plus ou moins un certain nombre d'heures moins 1 minute
   */
  public static Timestamp getDatePlusOuMoinsXHeuresMoins1Minute(
      java.util.Date dateDebut, int nbHeures, boolean moinsUneMinute) {
    Timestamp dateHeureJour = new Timestamp(dateDebut.getTime());
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(new java.util.Date(dateHeureJour.getTime()));
    calendrier.set(Calendar.HOUR_OF_DAY,
        (calendrier.get(Calendar.HOUR_OF_DAY) + nbHeures));
    calendrier.set(Calendar.SECOND, 0);
    calendrier.set(Calendar.MILLISECOND, 0);

    if (moinsUneMinute) {
      calendrier.set(Calendar.MINUTE, (calendrier.get(Calendar.MINUTE) - 1));
    }

    Timestamp date = new Timestamp(calendrier.getTime().getTime());

    return date;
  }

  /**
   * Obtenir la date et l'heure du jour moins un nombre x d'heures.
   * <p>
   * @param nbHeures le nombre d'heures à soustraire à la date du jour
   * @return java.sql.Timestamp la date du jour moins un certain nombre d'heures
   */
  public static Timestamp getDateMoinsXHeures(int nbHeures) {
    java.util.Date dateDebut = new java.util.Date();
    Timestamp dateHeureJour = new Timestamp(dateDebut.getTime());
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(new java.util.Date(dateHeureJour.getTime()));
    calendrier.set(Calendar.HOUR_OF_DAY,
        (calendrier.get(Calendar.HOUR_OF_DAY) - nbHeures));
    calendrier.set(Calendar.SECOND, 0);
    calendrier.set(Calendar.MILLISECOND, 0);

    Timestamp date = new Timestamp(calendrier.getTime().getTime());

    return date;
  }

  /**
   * Ajoute un nombre de jours à une date texte en format AAAA-MM-JJ et la retourne en format java.sql.Date.
   * <p>
   * @param dateAvecTraitsUnions date en format AAAA-MM-JJ à laquelle on veut ajouter les jours
   * @param nombreJour le nombre de jours à ajouter
   * @return java.sql.Date la date avec les jours ajoutés
   **/
  public static java.sql.Date ajouter_jr_AAAA_MM_JJ_En_SqlDate(
      String dateAvecTraitsUnions, int nombreJour) {
    Calendar calendar = Calendar.getInstance();
    int jours = Integer.valueOf(dateAvecTraitsUnions.substring(8)).intValue() +
        nombreJour;
    int mois = Integer.valueOf(dateAvecTraitsUnions.substring(5, 7)).intValue();
    int annee = Integer.valueOf(dateAvecTraitsUnions.substring(0, 4)).intValue();
    calendar.set(annee, mois - 1, jours);

    java.util.Date utilDate = calendar.getTime();

    return new java.sql.Date(utilDate.getTime());
  }

  /**
   * Ajoute un nombre de jours à une date en format java.util.Date.
   * <p>
   * @param date date en format java.util.Date
   * @param nombreJour le nombre de jours à ajouter
   * @return java.util.Date la date avec les jours ajoutés
   **/
  public static java.util.Date ajouter_jr_AAAA_MM_JJ_En_UtilDate(
      java.util.Date date, int nombreJour) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, nombreJour);

    java.util.Date utilDate = calendar.getTime();

    return utilDate;
  }

  /**
   * Ajoute un nombre de mois à une date en format java.util.Date.
   * <p>
   * @param date date en format java.util.Date
   * @param nombreJour le nombre de jours à ajouter
   * @return java.util.Date la date avec les jours ajoutés
   **/
  public static java.util.Date ajouter_mois_AAAA_MM_JJ_En_UtilDate(
      java.util.Date date, int nombreMois) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.MONTH, nombreMois);

    java.util.Date utilDate = calendar.getTime();

    return utilDate;
  }
  /**
   * Ajoute une minute à une date en format java.util.Date.
   * <p>
   * @param date la date à laquelle ajouter la minute
   * @return java.util.Date la date avec la minute ajoutée
   **/
  public static java.util.Date ajouterUneMinute(java.util.Date date) {
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(date);
    calendrier.add(Calendar.MINUTE, 1);

    return calendrier.getTime();
  }

  /**
   * Retourne la date et heure du jour présentement sans ponctuation.
   * <p>
   * @return String Date sous format AAAAMMJJHHMM
   **/
  public static String getDatePresenteAAAAMMJJHHMM() {
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmm");

    return df.format(new java.util.Date());
  }

  /**
   * Obtenir l'âge de quelqu'un à partir de sa date de naissance
   * @param dateNaissance la date de naissance de la personne
   * @return un entier identifiant l'âge
   */
  public static int getAge(java.util.Date dateNaissance) {
    Calendar calendrierNaissance = Calendar.getInstance();
    calendrierNaissance.setTime(dateNaissance);

    Calendar calendrierJour = Calendar.getInstance();

    int age = calendrierJour.get(Calendar.YEAR) -
        calendrierNaissance.get(Calendar.YEAR);

    calendrierNaissance.add(Calendar.YEAR, age);

    if (calendrierJour.before(calendrierNaissance)) {
      age--;
    }

    return age;
  }

  /**
   * Vérifier si une date est inférieure à la date du jour
   * @param dateAVerifier la date à vérifier
   * @return vrai si la date est inférieure à la date du jour sinon faux
   */
  public static boolean isDateAvantDateDuJour(java.util.Date dateAVerifier) {
    return dateAVerifier.before(getDateJourSansHeure());
  }

  /**
   * Retourne le mois correspondant à la date spécifié.
   * @return le mois correspondant à la date spécifié.
   * @param date la date à traiter.
   */
  public static int getMois(java.util.Date date) {
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(date);

    return calendrier.get(Calendar.MONTH);
  }

  /**
   * Retourne l'année correspondant à la date spécifié.
   * @return l'année correspondant à la date spécifié.
   * @param date la date à traiter.
   */
  public static int getAnnee(java.util.Date date) {
    Calendar calendrier = Calendar.getInstance();
    calendrier.setTime(date);

    return calendrier.get(Calendar.YEAR);
  }

  /**
   * Obtenir une fraction d'années entre deux dates. La méthode
   * tient compte des années bisextiles.
   * 
   * @param dateDebut Date de début
   * @param dateFin Date de fin
   * @return fraction arrondie à 2 décimales
   */
  public static BigDecimal getFractionAnnee(java.util.Date dateDebut, java.util.Date dateFin) {
    // Calcul de la fraction d'année de départ (de la date à l'année suivante)
    BigDecimal fractionDateDebut = getFractionAnnee(dateDebut, false);

    // Nombre d'années entre les deux
    // +1 car on a deja calculé la premiere fraction avec la première année
    int anneeDebut = UtilitaireDate.getAnnee(dateDebut) + 1;
    int anneeFin = UtilitaireDate.getAnnee(dateFin);
    BigDecimal annees = new BigDecimal(anneeFin - anneeDebut);

    // Calcul de la fraction d'année de fin (du début d'année à la date)
    BigDecimal fractionDateFin = getFractionAnnee(dateFin, true);

    // Nombre d'années total
    BigDecimal nombreAnnee = fractionDateDebut.add(annees).add(fractionDateFin);
    return nombreAnnee;
  }

  /**
   * Obtenir une fraction d'années entre une date et un début d'années.
   * 
   * @param date Date
   * @param precedente Si l'on calcule la fraction d'années avec l'années précédente ou l'années suivante.
   * @return fraction arrondie à 2 décimales
   */
  public static BigDecimal getFractionAnnee(java.util.Date date, boolean precedente) {
    int annee = UtilitaireDate.getAnnee(date);
    java.util.Date premierJanvier = UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate(annee + "-01-01");
    java.util.Date premierJanvierSuivant = UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate((annee + 1) + "-01-01");

    /* Si on veux la fraction de l'année precedente on prend le premier
     * janvier de l'annee courante, sinon on prend le premier janvier
     * de l'année prochaine
     */
    BigDecimal nombreJour = new BigDecimal(
        UtilitaireDate.getNbJoursEntreDeuxDates(date, (precedente ? premierJanvier : premierJanvierSuivant)));
    BigDecimal nombreJourAnnee = new BigDecimal(
        UtilitaireDate.getNbJoursEntreDeuxDates(premierJanvier, premierJanvierSuivant));

    BigDecimal fractionAnnee = nombreJour.divide(nombreJourAnnee, 2, BigDecimal.ROUND_HALF_UP);
    return fractionAnnee;
  }

  /* Convertir une java.util.Date en format AAAA-MM-JJ HH:MM:SS.m.
   * <p>
   * @param date la date en format java.util.Date
   * @return String la date en format AAAA-MM-JJ HH:MM:SS.m
   */
  public static String convertirEn_AAAA_MM_JJHeureSecondeMilli(java.util.Date date) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    return df.format(date);
  }
}
