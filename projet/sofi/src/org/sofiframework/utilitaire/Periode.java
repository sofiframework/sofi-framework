/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.Serializable;
import java.util.Date;

/**
 * Une période est constituée d'une date de début et d'une date de fin.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class Periode implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3791536829926900334L;

  private Date dateDebut;
  private Date dateFin;

  public Periode(Date dateDebut, Date dateFin) {
    this.dateDebut = dateDebut;
    this.dateFin = dateFin;
  }

  public Date getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  public Date getDateFin() {
    return dateFin;
  }

  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

}
