/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Classe utilitaire qui permet certaine fonctionnalité sur les propriétés d'une
 * instance de classe. La plupart des fonctionnalités proviennent de
 * org.apache.commons.beanutils.PropertyUtils.java, sauf la méthode <code>
 * describe(Object bean)</code> qui a été redéfini et renommer par decrire afin
 * de corriger un problème de la fontionnalité de base.
 * <p>
 * De plus s'ajoute plusieurs méthode souvent utilisé sur des objets.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.0.2 evaluerExpression(Object bean, String expression)
 * 
 */
public class UtilitaireObjet extends PropertyUtils {
  /**
   * Permet de décrire les propriétés d'une classe java avec ces valeurs
   * associées.
   * <p>
   * Correction d'un problème avec la version de base de la méthode describe de
   * PropertyUtils.java (org.apache.commons.beanutils)
   * 
   * @param bean
   *          L'objet bean à analyser
   * @return retourne la liste de toutes ses propriétés ainsi que ses valeurs
   *         associées
   * @throws java.lang.IllegalAccessException
   *           Si l'appelant n'a pas accès à l'une des propriétés
   * @throws java.lang.reflect.InvocationTargetException
   *           Si une exception survient lors de l'appel d'une méthode
   * @throws java.lang.NoSuchMethodException
   *           Si la méthode de l'accesseur n'est pas trouvée
   */
  public static Map decrire(Object bean) throws IllegalAccessException, InvocationTargetException,
  NoSuchMethodException {
    if (bean == null) {
      // Si le bean est null. retourne une description du bean vide.
      // Dans la version de base une exception est lancé.
      return (new java.util.HashMap());
    }

    Map description = new HashMap();

    if (bean instanceof DynaBean) {
      DynaProperty[] descriptors = ((DynaBean) bean).getDynaClass().getDynaProperties();

      for (int i = 0; i < descriptors.length; i++) {
        String name = descriptors[i].getName();
        description.put(name, getProperty(bean, name));
      }
    } else {
      PropertyDescriptor[] descriptors = getPropertyDescriptors(bean);

      for (int i = 0; i < descriptors.length; i++) {
        try {
          String name = descriptors[i].getName();

          if (descriptors[i].getReadMethod() != null) {
            description.put(name, getProperty(bean, name));
          }
        } catch (Exception e) {
        }
      }
    }

    return (description);
  }

  /**
   * Permet d'extraire la valeur resultant d'une methode.
   * <p>
   * 
   * @param bean
   *          l'objet d'affaires à partir duquel on effectue l'extraction d'une
   *          valeur d'attribut.
   * @param attribut
   *          L'attribut où l'appelant veut récupérer la valeur
   * @return La valeur sous forme d'objet de l'attribut spécifié
   */
  public static Object getValeurAttribut(Object bean, String attribut) {
    Object valeurAttribut = null;

    try {
      valeurAttribut = PropertyUtils.getNestedProperty(bean, attribut);
    } catch (NoSuchMethodException e) {
      // throw new IllegalArgumentException("Aucun getter pour la propriété "
      // + attribut + " dans la classe " + bean.getClass().getName());
    } catch (IllegalAccessException e) {
      throw new IllegalArgumentException("Le getter pour la propriété " + attribut + " dans la classe "
          + bean.getClass().getName() + " doit être publique");
    } catch (InvocationTargetException e) {
      Throwable cause = e.getCause();

      if (cause instanceof Error) {
        throw (Error) cause;
      } else if (cause instanceof RuntimeException) {
        throw (RuntimeException) cause;
      } else {
        throw new SOFIException(cause);
      }
    } catch (org.apache.commons.beanutils.NestedNullException e) {
      // On doit seulement retourner un null si la propriété est null
      valeurAttribut = null;
    }

    return valeurAttribut;
  }

  /**
   * Retourne le type de classe pour un attribut de l'objet en traitement.
   * 
   * @param bean
   *          l'instance de l'objet en traitement
   * @param attribut
   *          le nom d'attribut dont on désire avoir le type de données.
   * @return le type de classe pour un attribut de l'objet en traitement.
   */
  public static Class getClasse(Object objet, String nomAttribut) {
    Class nomClasse = null;

    try {
      Object valeurInstance = null;

      try {
        valeurInstance = UtilitaireObjet.getValeurAttribut(objet, nomAttribut);
      } catch (Exception e) {
      }

      if (valeurInstance != null) {
        Class typeClasse = getPropertyType(objet, nomAttribut);
        nomClasse = valeurInstance.getClass();

        if (typeClasse.isPrimitive()) {
          nomClasse = typeClasse;
        }
      } else {
        nomClasse = getPropertyType(objet, nomAttribut);
      }
    } catch (NoSuchMethodException e) {
      throw new IllegalArgumentException("Aucun getter pour la propriété " + nomAttribut + " dans la classe "
          + objet.getClass().getName());
    } catch (IllegalAccessException e) {
      throw new IllegalArgumentException("Le getter pour la propriété " + nomAttribut + " dans la classe "
          + objet.getClass().getName() + " doit être publique");
    } catch (InvocationTargetException e) {
      Throwable cause = e.getCause();

      if (cause instanceof Error) {
        throw (Error) cause;
      } else if (cause instanceof RuntimeException) {
        throw (RuntimeException) cause;
      } else {
        throw new SOFIException(cause);
      }
    }

    return nomClasse;
  }

  /**
   * Fixer une valeur pour une propriété d'un objet.
   * 
   * @param objet
   *          l'objet a traité
   * @param nomAttribut
   *          le nom attribut
   * @param valeur
   *          la valeur a fixer
   * @throws java.lang.IllegalAccessException
   * @throws java.lang.reflect.InvocationTargetException
   * @throws java.lang.NoSuchMethodException
   */
  public static void setPropriete(Object objet, String nomAttribut, Object valeur) throws IllegalAccessException,
  InvocationTargetException, NoSuchMethodException {
    setProperty(objet, nomAttribut, valeur);
  }

  /**
   * Méthode qui sert à cloner une liste.
   * <p>
   * Cette méthode clone une liste en profondeur.
   * <p>
   * 
   * @param liste
   *          la liste à cloner
   * @return la liste cloner
   */
  public static List clonerListe(List liste) {
    List nouvelleListe = null;

    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream(100);
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(liste);

      byte[] buf = baos.toByteArray();
      oos.close();

      ByteArrayInputStream bais = new ByteArrayInputStream(buf);
      ObjectInputStream ois = new ObjectInputStream(bais);
      nouvelleListe = (ArrayList) ois.readObject();
      ois.close();
    } catch (IOException e) {
      throw new SOFIException(e);
    } catch (ClassNotFoundException e) {
      throw new SOFIException(e);
    }

    return nouvelleListe;
  }

  /**
   * Méthode qui sert à cloner un objet de tout type.
   * <p>
   * Cette méthode clone un objet en profondeur.
   * <p>
   * 
   * @param objetACloner
   *          un objet à cloner
   * @return un nouveau objet cloné
   */
  public static Object clonerObjet(Object objetACloner) {
    Object nouveauObjet = null;

    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream(100);
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(objetACloner);

      byte[] buf = baos.toByteArray();
      oos.close();

      ByteArrayInputStream bais = new ByteArrayInputStream(buf);
      ObjectInputStream ois = new ObjectInputStream(bais);
      nouveauObjet = ois.readObject();
      ois.close();
    } catch (IOException e) {
      throw new SOFIException(e);
    } catch (ClassNotFoundException e) {
      throw new SOFIException(e);
    }

    return nouveauObjet;
  }

  /**
   * Vérifie si tous les champs du bean sont vide, retourne vrai si c'est le
   * cas, faux sinon.
   */
  public static boolean isVide(Object bean, String[] attributs) {
    for (int i = 0, n = attributs.length; i < n; i++) {
      if (!isVide(bean, attributs[i])) {
        return false;
      }
    }

    return true;
  }

  /**
   * Vérifie si tous les champs du bean sont définis, retourne vrai si c'est le
   * cas, faux sinon.
   */
  public static boolean tousDefinis(Object bean, String[] attributs) {
    for (int i = 0, n = attributs.length; i < n; i++) {
      if (isVide(bean, attributs[i])) {
        return false;
      }
    }

    return true;
  }

  /**
   * Vérifie si tous les champs du bean sont définis, retourne vrai si c'est le
   * cas, faux sinon.
   */
  public static boolean tousDefinis(Object bean, Collection attributs) {
    for (Iterator it = attributs.iterator(); it.hasNext();) {
      if (isVide(bean, it.next().toString())) {
        return false;
      }
    }

    return true;
  }

  /**
   * Vérifie si le champ du bean est vide, retourne vrai si c'est le cas, faux
   * sinon.
   */
  public static boolean isVide(Object bean, String attribut) {
    try {
      Object valeur = PropertyUtils.getProperty(bean, attribut);

      if (valeur instanceof String) {
        return valeur.toString().trim().length() == 0;
      } else if (valeur instanceof Boolean) {
        Class type = PropertyUtils.getPropertyType(bean, attribut);

        return type.isPrimitive() && !((Boolean) valeur).booleanValue();
      } else if (valeur instanceof String[]) {
        if (valeur == null) {
          // Si la liste est null alors elle est vide.
          return true;
        } else {
          for (int i = 0; i < ((String[]) valeur).length; i++) {
            // S'il existe une String dans la liste qui n'est pas vide, alors la
            // liste n'est pas vide
            if (!UtilitaireString.isVide(((String[]) valeur)[i])) {
              return false;
            }
          }
        }

        // Si la liste est égale à zéro ou la totalité des String contenu est
        // vide,
        // alors la liste de String est vide.
        return true;
      } else {
        return valeur == null;
      }
    } catch (RuntimeException e) {
      throw e;
    } catch (InvocationTargetException e) {
      Throwable cause = e.getTargetException();

      if (cause instanceof RuntimeException) {
        throw (RuntimeException) cause;
      } else if (cause instanceof Error) {
        throw (Error) cause;
      } else {
        throw new SOFIException(cause.toString(), cause);
      }
    } catch (Exception e) {
      throw new SOFIException(e);
    }
  }

  /**
   * Obtenir si un bean contien seulement des attributs vides. Les attributs
   * sont identifiés avec le bean et une classe d'arret. Les propriétés incluses
   * dans la classes parents à partir de la classe d'arret ne seront pas prises
   * en compte.
   * 
   * @param bean
   *          Objet
   * @param classeArret
   *          Classe parent ou on ne doit plus prendre les attributs en compte
   * @return true = Vide, false = Non vide
   */
  public static boolean isVide(Object bean, Class classeArret) {
    String[] attributs = getListeAttributs(bean.getClass(), classeArret);
    return isVide(bean, attributs);
  }

  /**
   * Obtenir si un objet de transfert est vide. Les propriétés seronts prises en
   * compte jusqu'a la classe ObjetTransfert.
   * 
   * @param objetTransfert
   *          Objet de transfert
   * @return true = Vide, false = Non vide
   */
  public static boolean isVide(ObjetTransfert objetTransfert) {
    Class clazz = (objetTransfert instanceof ObjetFiltre) ? ObjetFiltre.class : ObjetTransfert.class;
    return isVide(objetTransfert, clazz);
  }

  /**
   * Obtenir la liste des attributs d'une classe.
   * 
   * @param clazz
   *          Classe
   * @return Liste des noms des attributs de la classe
   */
  public static String[] getListeAttributs(Class clazz) {
    return getListeAttributs(clazz, Object.class);
  }

  /**
   * Obtenir la liste des attributs de la classe avec une classe de fin.
   * 
   * @param debut
   *          Classe dont on désire la liste des attributs
   * @param fin
   *          Classe de fin ou l'on doit arrêter de prendre les attributs en
   *          compte
   * @return Liste d'attributs
   */
  public static String[] getListeAttributs(Class debut, Class fin) {
    try {
      BeanInfo info = Introspector.getBeanInfo(debut, fin);
      PropertyDescriptor[] props = info.getPropertyDescriptors();
      ArrayList liste = new ArrayList(props.length);

      for (int i = 0, n = props.length; i < n; i++) {
        PropertyDescriptor propriete = props[i];

        if (propriete.getReadMethod() != null) {
          liste.add(propriete.getName());
        }
      }

      return (String[]) liste.toArray(new String[liste.size()]);
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new SOFIException(e);
    }
  }

  private static void instancierInstancesImbrique(ObjetTransfert objetTransfert, ObjetTransfert objetTransfertParent,
      boolean memeInstance) {
    try {
      Map valeurs = UtilitaireObjet.decrire(objetTransfert);

      Iterator listeAttributsAvecValeurIter = valeurs.keySet().iterator();

      while (listeAttributsAvecValeurIter.hasNext()) {
        String nomAttribut = (String) listeAttributsAvecValeurIter.next();

        // Vérifier le type d'attribut
        Class type = PropertyUtils.getPropertyType(objetTransfert, nomAttribut);

        try {
          if (ObjetTransfert.class.isAssignableFrom(type)) {
            ObjetTransfert nouvelleInstance = null;
            Object valeur = UtilitaireObjet.getValeurAttribut(objetTransfert, nomAttribut);

            if (valeur == null) {
              nouvelleInstance = (ObjetTransfert) type.newInstance();
            } else {
              nouvelleInstance = (ObjetTransfert) valeur;
            }

            UtilitaireObjet.setPropriete(objetTransfert, nomAttribut, nouvelleInstance);

            boolean nouveauMemeInstanceOuIdentiqueAuParent = false;

            // La classe en cours d'instanciation.
            Class classeObjetTransfert = objetTransfert.getClass();

            if (type == classeObjetTransfert) {
              nouveauMemeInstanceOuIdentiqueAuParent = true;
            }

            if ((objetTransfertParent != null) && (type == objetTransfertParent.getClass())) {
              nouveauMemeInstanceOuIdentiqueAuParent = true;
            }

            if (!memeInstance) {
              instancierInstancesImbrique(nouvelleInstance, objetTransfert, nouveauMemeInstanceOuIdentiqueAuParent);
            }
          }
        } catch (Exception e) {
          // ignorer
        }
      }
    } catch (Exception e) {
    }
  }

  /**
   * Permet de combiner des valeurs d'attribut d'un bean à l'aide d'une
   * expression. On doit placer les propriétés entre crochet [maprop]. Il est
   * possible d'insérer n'importe quel séparateur entre les valeurs de
   * propriétés.
   * 
   * @param bean
   *          Objet source des valeurs de l'expression
   * @param expression
   *          Expression
   * @return interprétation de l'expression avec le bean
   */
  public static String evaluerExpression(Object bean, String expression) {
    StringBuffer resultat = null;

    if ((expression != null) && (bean != null)) {
      resultat = new StringBuffer();

      String debut = expression.substring(0, expression.indexOf("["));
      String fin = expression.substring(expression.lastIndexOf("]") + 1);

      String[] attributs = UtilitaireString.substrings(expression, "[", "]");
      String[] separateurs = UtilitaireString.substrings(expression, "]", "[");

      resultat.append(debut);

      for (int i = 0; i < attributs.length; i++) {
        resultat.append(UtilitaireObjet.getValeurAttribut(bean, attributs[i]));

        if (i < separateurs.length) {
          resultat.append(separateurs[i]);
        }
      }

      resultat.append(fin);
    }

    return (resultat != null) ? resultat.toString() : null;
  }

  /**
   * Parcourir un objet de transfert et instancie toutes valeur du type
   * ObjetTransfert non instancié.
   * 
   * @param objetTransfert
   *          l'objet de transfert dont on désire instancier les classes
   *          interne.
   */
  public static void instancierInstancesImbrique(ObjetTransfert objetTransfert) {
    instancierInstancesImbrique(objetTransfert, null, false);
  }

  /**
   * Permet de copier les attibuts d'un objet source vers un objet de
   * destination.
   * 
   * @param destination
   *          la destination
   * @param source
   *          la source.
   * @since SOFI 2.1
   */
  public static void copierAttribut(Object destination, Object source) {
    try {
      PropertyUtils.copyProperties(destination, source);
    } catch (Exception e) {
    }
  }

  /**
   * Instancie une nouvelle classe puis copie les attribut de l'objet à
   * convertir dans le nouvel objet (attributs identiques par leur nom et leur
   * type).
   * 
   * @param objetAConvertir
   *          Objet à convertir
   * @param clazz
   *          Classe du nouvel objet
   * @return Nouvel objet qui contient les valeurs de l'objet à convertir
   */
  @SuppressWarnings("rawtypes")
  public static Object convertirObjet(Object objetAConvertir, Class clazz) {
    Object objetConverti = null;
    if (objetAConvertir != null) {
      try {
        objetConverti = clazz.newInstance();
        PropertyUtils.copyProperties(objetConverti, objetAConvertir);
      } catch (Exception e) {
        throw new ModeleException(e);
      }
    }
    return objetConverti;
  }

  /**
   * Instancie une nouvelle classe puis copie les attribut de la liste d'objets à
   * convertir dans une liste de nouveaux objets (attributs identiques par leur nom et leur
   * type).
   * 
   * @param listeObjet
   *          Liste d'objets à convertir
   * @param clazz
   *          Classe du nouvel objet
   * @return Nouvelle liste d'objets qui contiennent les valeurs de l'objet à convertir
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static List convertirListeObjet(List listeObjet, Class clazz) {
    List listeConvertie = null;

    if (listeObjet != null) {
      listeConvertie = new ArrayList();
      for (Iterator i = listeObjet.iterator(); i.hasNext();) {
        Object objet = i.next();
        if (objet != null) {
          listeConvertie.add(convertirObjet(objet, clazz));
        }
      }
    }
    return listeConvertie;
  }
}
