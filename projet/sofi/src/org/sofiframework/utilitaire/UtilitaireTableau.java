/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


/**
 * Utilitaire permettant d'ajouter d'abord des items
 * dans un liste dynamique pour ensuite les transformer
 * dans une liste statique. Object[].
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireTableau {
  ArrayList liste = null;

  /**
   * Constructeur, la liste dynamique est initialisé.
   */
  public UtilitaireTableau() {
    this.liste = new ArrayList();
  }

  /**
   * Ajouter un item à la liste dynamique d'objet
   * @param objet
   */
  public void ajouter(Object objet) {
    this.liste.add(objet);
  }

  /**
   * Transforme la liste dynamique en tableau statique
   * @return un tableau d'objets.
   */
  public Object[] getTableau() {
    Object[] tableau = new Object[liste.size()];

    for (int i = 0; i < liste.size(); i++) {
      Object objetCourant = liste.get(i);
      tableau[i] = objetCourant;
    }

    return tableau;
  }

  public static Object[] getTableau(ArrayList liste) {
    Object[] tableau = new Object[liste.size()];

    for (int i = 0; i < liste.size(); i++) {
      Object objetCourant = liste.get(i);
      tableau[i] = objetCourant;
    }

    return tableau;
  }

  /**
   * Retourne un tableau d'objet avec une liste de type HashSet.
   * @return un tableau d'objet
   * @param liste une liste de type HashSet.
   */
  public static String[] getTableau(HashSet liste) {
    String[] tableau = new String[liste.size()];
    int i = 0;
    Iterator iterateur = liste.iterator();

    while (iterateur.hasNext()) {
      tableau[i] = (String) iterateur.next();
      i++;
    }

    return tableau;
  }
}
