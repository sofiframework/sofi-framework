/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.Locale;

import org.sofiframework.exception.SOFIException;


/**
 * Classe utilitaire dans la gestion de la locale d'un utilisateur.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 3.0.2
 */
public class UtilitaireLocale {
  /**
   * Retourne une instance <code>Locale</code> pour un code.
   * @param codeLocale le code de la Locale désiré.
   * @return une instance <code>Locale</code> pour un code.
   */
  public static Locale getLocale(String codeLocale) {
    Locale locale = null;

    String codeLangue = null;
    String codePays = null;

    if (codeLocale == null) {
      // Locale par défaut.
      codeLocale = "fr_CA";
    }

    if (codeLocale.indexOf("_") != -1) {
      // Traitement du code de la locale.
      codeLangue = codeLocale.substring(0, codeLocale.indexOf("_"));
      codePays = codeLocale.substring(codeLocale.indexOf("_")+1);
    }else {
      throw new SOFIException("Le code de la locale doit etre sous format : xx_YY");
    }


    locale = new Locale(codeLangue, codePays);

    return locale;
  }
}
