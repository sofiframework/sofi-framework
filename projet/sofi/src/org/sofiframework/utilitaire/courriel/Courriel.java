/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.courriel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Map;
import java.util.StringTokenizer;

import javax.activation.FileDataSource;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.internet.MimeUtility;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.sofiframework.exception.SOFIException;


/**
 * Classe utilitaire facilitant l'envoi de courriel (Email).
 * <p>
 * Cette classe permet d'envoyer tous types de courriers électroniques.
 * Elle offre des services simplifié pour créer un message et y attacher
 * des fichiers. Les service offert prennent en considération le
 * "MimeType" et le "charset" ce qui permet l'intégrité du contenu.
 * Il est habituellement préférable de spécifier le "charset". Dans le cas où
 * aucun charset n'est donné, celui par défaut de la JVM sera utilisé lors de la
 * génération du message.
 * <p>
 * Cette classe est abstraite. Vous devez utiliser l'une des deux saveurs soit :
 * CourrielHtml pour les courriel au contenu "html" ou CourrielTexte pour ceux au
 * contenu de caractères "plain".
 * @author Steven Charette (Nurun inc.)
 * @version SOFI 2.0
 */
public abstract class Courriel {
  /** Structure complete d'un courriel. */
  private MultiPartEmail courriel;

  private String charset;

  /** Constructeur de Courriel */
  public Courriel(MultiPartEmail courriel, String serveurSMTP, String charset) {
    this.courriel = courriel;
    this.charset = charset;
    courriel.setHostName(serveurSMTP);

    //fixer le charset du message et du sujet.
    if (charset != null) {
      courriel.setCharset(charset);
    } else {
      courriel.setCharset(System.getProperty("file.encoding"));
    }
  }

  /** Constructeur de Courriel */
  public Courriel(MultiPartEmail courriel, String serveurSMTP, String usagerSMTP, String motPasseSMTP, boolean sslSmtp, String charset) {
    this.courriel = courriel;
    this.charset = charset;
    courriel.setHostName(serveurSMTP);
    courriel.setAuthentication(usagerSMTP, motPasseSMTP);
    courriel.setSSL(sslSmtp);

    //fixer le charset du message et du sujet.
    if (charset != null) {
      courriel.setCharset(charset);
    } else {
      courriel.setCharset(System.getProperty("file.encoding"));
    }
  }

  /**
   * Fixer le sujet du message.
   * @param sujet Le sujet du message.
   * @return le courriel.
   */
  public Courriel setSujet(String sujet) {
    try {
      courriel.setSubject(MimeUtility.encodeText(sujet, this.charset, "Q"));
    } catch (UnsupportedEncodingException e) {
      courriel.setSubject(sujet);
    }

    return this;
  }

  /**
   * Fixer le message (le contenu) du courriel.
   *
   * @param message Le message du courriel.
   * @throws org.sofiframework.exception.SOFIException Si le message ne peut être fixer.
   * @return Le courriel.
   *
   */
  public abstract Courriel setMessage(String message) throws SOFIException;

  /**
   * Fixer le message (le contenu) du courriel à  partir du contenu d'un fichier.
   *
   * @param fichier Fichier source du contenu.
   * @throws org.sofiframework.exception.SOFIException Si le message ne peut être fixer.
   * @return Le courriel.
   * @return
   */
  public abstract Courriel setMessage(File fichier) throws SOFIException;

  /**
   * Ajouter l'expéditeur du courriel.
   *
   * @param nom Le nom de l'expéditeur.
   * @param adresseCourriel L'adresse courriel de l'expéditeur.
   * @return Le courriel.
   */
  public Courriel setFrom(String adresseCourriel, String nom)
      throws SOFIException {
    try {
      courriel.setFrom(adresseCourriel, nom);
    } catch (EmailException e) {
      throw new SOFIException("Le destinateur n'a pu etre fixer.", e);
    }

    return this;
  }

  /**
   * Ajouter le destinataire du courriel.
   *
   * @param nom Le nom du destinataire.
   * @param adresseCourriel L'adresse courriel du destinataire.
   * @return Le courriel.
   */
  public Courriel ajouterTo(String adresseCourriel, String nom) {
    try {
      courriel.addTo(adresseCourriel, nom);
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }

    return this;
  }

  /**
   * Ajouter les destinataires du courriel.
   *
   * @param nom Le nom du destinataire.
   * @param adressesCourriels les adresses courriels séparées par des virgule,
   * comme mentionné par RFC822.
   * @return Le courriel.
   */
  public Courriel ajouterTo(String adressesCourriels){
    try {
      StringTokenizer tokens = new StringTokenizer(adressesCourriels, ",");

      while(tokens.hasMoreTokens()){
        String adresse = tokens.nextToken().trim();
        courriel.addTo(adresse, adresse);
      }
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }
    return this;
  }

  /**
   * Ajouter un destinataire en copie carbone.
   *
   * @param nom Le nom du destinataire en copie carbone.
   * @param adresseCourriel L'adresse du destinataire en copie carbone.
   * @return Le courriel.
   */
  public Courriel ajouterCc(String adresseCourriel, String nom) {
    try {
      courriel.addCc(adresseCourriel, nom);
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }

    return this;
  }

  /**
   * Ajouter un destinataire en copie carbone.
   *
   * @param nom Le nom du destinataire en copie carbone.
   * @param adresseCourriel les adresses courriels séparées par des virgule,
   * comme mentionné par RFC822.
   * @return Le courriel.
   */
  public Courriel ajouterCc(String adressesCourriels){
    try {
      StringTokenizer tokens = new StringTokenizer(adressesCourriels, ",");

      while(tokens.hasMoreTokens()){
        String adresse = tokens.nextToken().trim();
        courriel.addCc(adresse, adresse);
      }
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }
    return this;
  }

  /**
   * Ajouter un destinataire en copie carbone invisible.
   *
   * @param nom Le nom du destinataire en copie carbone invisible.
   * @param adresseCourriel L'adresse du destinataire en copie carbone invisible.
   * @return Le courriel.
   */
  public Courriel ajouterBcc(String adresseCourriel, String nom) {
    try {
      courriel.addBcc(adresseCourriel, nom);
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }

    return this;
  }

  /**
   * Ajouter des destinataire en copie carbone invisible.
   *
   * @param nom Le nom du destinataire en copie carbone invisible.
   * @param adressesCourriels les adresses courriels séparées par des virgule,
   * comme mentionné par RFC822.
   * @return Le courriel.
   */
  public Courriel ajouterBcc(String adressesCourriels){
    try {
      StringTokenizer tokens = new StringTokenizer(adressesCourriels, ",");

      while(tokens.hasMoreTokens()){
        String adresse = tokens.nextToken().trim();
        courriel.addBcc(adresse, adresse);
      }
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }
    return this;
  }

  /**
   * Attacher un document à  partir d'une chaine d'octets.
   *
   * @param descriptionFichier La description du fichier
   * @param nomFichier Le nom du fichier i.e. <nom.extension>
   * @param fichier La chaine d'octets représentant le document.
   * @return Le courriel.
   */
  public Courriel attacher(byte[] fichier, String nomFichier,
      String descriptionFichier) {
    String mime = new MimetypesFileTypeMap().getContentType(nomFichier);

    try {
      courriel.attach(new ByteArrayDataSource(fichier, mime), nomFichier,
          descriptionFichier);
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    } catch (IOException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }

    return this;
  }

  /**
   * Attacher un document à  partir d'un fichier.
   *
   * @param fichier Le fichier représentant le document.
   * @return Le courriel.
   */
  public Courriel attacher(File fichier) {
    try {
      courriel.attach(new FileDataSource(fichier), fichier.getName(),
          fichier.getName());
    } catch (EmailException e) {
      throw new SOFIException("Un destinataire en copie conforme n'a pu etre ajoute.",
          e);
    }

    return this;
  }

  /**
   * Attacher un document à  partir d'un lien URL.
   *
   * @param nomUrl Le nom du lien URL.
   * @param url Le lien URL. Le format doit respecter le RFC 2396
   * @return Le courriel.
   */
  public Courriel attacher(URL url, String nomUrl) {
    try {
      EmailAttachment attachement = new EmailAttachment();
      attachement.setURL(url);
      attachement.setName(nomUrl);
      courriel.attach(attachement);
    } catch (EmailException e) {
      throw new SOFIException("L'URL n'a pu etre attache.", e);
    }

    return this;
  }

  /**
   * Obtenir l'objet représentant le courriel.
   *
   * @return le courriel.
   */
  protected Email getCourriel() {
    return this.courriel;
  }

  /**
   * Fixer l'objet représentant le courriel.
   * @param courriel l'objet représentant le courriel.
   */
  protected void setEmail(MultiPartEmail courriel) {
    this.courriel = courriel;
  }

  /**
   * Envoyer le courriel.
   */
  public void envoyer() {
    try {
      courriel.send();
    } catch (EmailException e) {
      throw new SOFIException(e);
    }
  }

  /**
   * Obtenir le contenu d'un fichier sous forme de chaine de caractères.
   *
   * @param fichier Le fichier source.
   * @throws java.io.IOException Si une erreur de lecture survient.
   * @throws java.io.FileNotFoundException Si le fichier ne peut être trouvé.
   * @return Le contenu du fichier sous forme de chaine de caractères.
   */
  protected String getContenuFichier(File fichier)
      throws FileNotFoundException, IOException {
    char[] texte = new char[1024];
    String msgCourriel = "";
    FileReader reader = new FileReader(fichier);

    //obtention du message du couriel à  partir du fichier.
    for (int offset = 0, count = 0;
        (count = reader.read(texte, offset, 1024)) != -1; offset += offset) {
      msgCourriel += String.valueOf(texte, offset, count);
    }
    reader.close();
    return msgCourriel;
  }

  /**
   * Fixer des 'tags' a ajouter dans le headers du courriels.
   * <p>
   * @param headers liste des  des 'tags' a ajouter dans le headers du courriels.
   */
  public void setHeaders(Map<String,String> headers) {
    courriel.setHeaders(headers);
  }
}
