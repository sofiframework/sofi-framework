/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.courriel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.sofiframework.exception.SOFIException;


public class CourrielHtml extends Courriel {

  /**
   * Constructeur du courriel au format Html.
   * 
   * @param charset le jeux de caractères à  utilisé pour assurer l'intégrité du
   * contenu du courriel.
   * @param serveurSMTP L'adresse du serveur SMTP (Serveur d'envoi de courriel).
   */
  public CourrielHtml(String serveurSMTP, String charset) {
    super(new HtmlEmail(), serveurSMTP, charset);
  }

  /**
   * Constructeur du courriel au format Html.
   * 
   * @param serveurSMTP L'adresse du serveur SMTP (Serveur d'envoi de courriel).
   * @param usagerSMTP Le nom d'usager à utiliser pour envoyer des courriels sur le SMTP.
   * @param motPasseSMTP Le mot de passe à utiliser pour envoyer des courriels sur le SMTP.
   * @param sslStmp Est-ce que le SMTP doit être en mode SSL?
   * @param charset le jeux de caractères à  utilisé pour assurer l'intégrité du
   * contenu du courriel.
   */
  public CourrielHtml(String serveurSMTP, String usagerSMTP, String motPasseSMTP, boolean sslSmtp, String charset) {
    super(new HtmlEmail(), serveurSMTP, usagerSMTP, motPasseSMTP, sslSmtp, charset);
  }

  /**
   * Constructeur de convénience pour fixer le jeux de caractères à  celui
   * par défaut système
   * @param serveurSMTP L'adresse du serveur SMTP (Serveur d'envoi de courriel).
   */
  public CourrielHtml(String serveurSMTP){
    super(new HtmlEmail(), serveurSMTP, null);
  }

  /**
   * Fixer un message au format Html.
   * 
   * @param message Le message du courriel.
   * @return Le courriel.
   */
  @Override
  public Courriel setMessage(String message) throws SOFIException{
    try{
      ((HtmlEmail)getCourriel()).setHtmlMsg(message);
    }catch(EmailException e){
      throw new SOFIException("Le message HTML n'a pu etre fixe.", e);
    }
    return this;
  }

  /**
   * Fixer un message au format Html à  partir des données d'un fichier.
   * 
   * @param fichier Le fichier Html représentant le contenu du courriel.
   * @return Le courriel.
   */
  @Override
  public Courriel setMessage(File fichier){
    try{
      ((HtmlEmail)getCourriel()).setHtmlMsg(getContenuFichier(fichier));
    }catch(FileNotFoundException e){
      throw new SOFIException("Le fichier : " + fichier.getAbsolutePath() + " n'a pas ete trouver.", e);
    }catch(IOException e){
      throw new SOFIException("Erreur lors de la lecture du fichier : " + fichier.getAbsolutePath(), e);
    }catch(EmailException e){
      throw new SOFIException("Le message n'a pu etre fixe.", e);
    }
    return this;
  }
}