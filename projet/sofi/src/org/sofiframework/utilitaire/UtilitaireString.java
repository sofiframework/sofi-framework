/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;

/**
 * Classe offrant des services statiques pour la manipulation des chaînes de
 * caractères.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @author Pierre-Frédérick Duret (Nurun inc.)
 * @author Steve Tremblay (Nurun inc.)
 * @version SOFI 1.0
 * @since SOFI 2.0.2 substrings(String chaine, String ouverture, String
 *        fermeture)
 */
public class UtilitaireString {
  /** Instance de journalisation **/
  private static Log log = LogFactory.getLog(UtilitaireString.class);

  /**
   * Variables globales identifiant la valeur d'un retour de chariot dans le
   * fichier séquentiel
   */
  private static final String RETOUR_CHARIOT_1 = "\r\n";
  private static final String RETOUR_CHARIOT_2 = "\r";
  private static final String RETOUR_CHARIOT_3 = "\n";

  /**
   * Mettre en majuscule la première lettre d'une chaîne de caractères.
   * <p>
   * 
   * @param chaine
   *          la chaîne à modifier
   * @return String la chaîne avec la première lettre en majuscule
   */
  public static String convertirPremiereLettreEnMajuscule(String chaine) {
    return chaine.substring(0, 1).toUpperCase() + chaine.substring(1, chaine.length());
  }

  /**
   * Mettre en majuscule la première lettre seulement d'une chaîne de
   * caractères, tous les autres lettres vont devenir en minuscule.
   * <p>
   * 
   * @param chaine
   *          la chaîne à modifier
   * @return String la chaîne avec la première lettre en majuscule
   */
  public static String convertirPremiereLettreSeulementEnMajuscule(String chaine) {
    return chaine.substring(0, 1).toUpperCase() + chaine.substring(1, chaine.length()).toLowerCase();
  }

  /**
   * Mettre en minuscule la première lettre d'une chaîne de caractères.
   * 
   * @return chaaîne de caractère avec la première lettre en minuscule
   * @param chaine
   *          La chaîne à modifier
   */
  public static String convertirPremiereLettreEnMinuscule(String chaine) {
    return chaine.substring(0, 1).toLowerCase() + chaine.substring(1, chaine.length());
  }

  /**
   * Permet de mettre en lettre majuscule la première lettre de la valeur. <br/>
   * Si la valeur est une chaine composé (prénom ou nom par exemple), les
   * séparations prisent en compte étant le tiret et l'espace, on met en
   * majuscule également chaque lettre qui suit le séparateur.
   * 
   * @param chaine
   */
  public static String convertirToutesPremieresLettreMajuscule(String chaine) {

    // Traiter les mots séparés par un espace
    String valeurTraitement = WordUtils.capitalizeFully(chaine);

    // Traiter les mots séparés par un tiret
    String valeurRetour = WordUtils.capitalizeFully(valeurTraitement, '-');

    return valeurRetour;

  }

  /**
   * Obtenir le nom du fichier sans le chemin d'accès.
   * <p>
   * Cette méthode retire le chemin d'accès d'un fichier dans une chaine de
   * caractères de type URL. En y passant par exemple
   * "/repertoire1/html/fichier.html" on recevrait en retour "fichier.html".
   * <p>
   * 
   * @param pathFichier
   *          la chaîne contenant le chemin du fichier
   * @return String le nom du fichier
   */
  public static String getNomFichierSansPath(String pathFichier) {
    return pathFichier.substring(pathFichier.lastIndexOf('/') + 1, pathFichier.length());
  }

  /**
   * Obtenir l'extention d'un nom de fichier.
   * <p>
   * 
   * @param nomFichier
   *          le fichier dont on désire connaitre l'extention
   * @return String l'extention du fichier
   */
  public static String getExtensionFichier(String nomFichier) {
    return nomFichier.substring(nomFichier.indexOf(".") + 1, nomFichier.length());
  }

  /**
   * Méthode qui modifie les retours de chariots.
   * <p>
   * Cette méthode remplace toutes les occurences de retour de chariot (selon
   * les types de retours de chariots définits dans la classe UtilitaireString)
   * en changement de ligne pour une page JSP ("<BR>
   * ")
   * <p>
   * 
   * @param texte
   *          la string sur lequel effectuer la modification
   * @return String la string modifiée
   */
  public static String modifierRetourChariotEnChangementLigneWeb(String texte) {
    ArrayList listeRetourChariots = UtilitaireString.getListeRetourDeChariots();

    // Prendre un retour de chariot
    Iterator iterateur = listeRetourChariots.iterator();
    StringBuffer texteConverti = null;

    while (iterateur.hasNext()) {
      String retourChariot = (String) iterateur.next();

      // Faire la transformation pour les différents retour de chariots
      texteConverti = new StringBuffer();

      while (texte.indexOf(retourChariot) != -1) {
        texteConverti.append(texte.substring(0, texte.indexOf(retourChariot)));
        texteConverti.append("<br/>");
        texte = texte.substring(texte.indexOf(retourChariot) + (retourChariot.length()));
      }

      // Ajouter le dernier bout de texte
      if (texte.length() > 0) {
        texteConverti.append(texte);
      }

      texte = texteConverti.toString();
    }

    return texte;
  }

  /**
   * Méthode qui retourne une liste remplie des différents types de retours de
   * chariots traités.
   * <p>
   * Cette méthode fabrique une liste et y ajouter tous les types de retours de
   * chariots traités.
   * <p>
   * 
   * @return ArrayList la liste des retours de chariots
   */
  private static ArrayList getListeRetourDeChariots() {
    ArrayList listeRetourDeChariots = new ArrayList();
    listeRetourDeChariots.add(UtilitaireString.RETOUR_CHARIOT_1);
    listeRetourDeChariots.add(UtilitaireString.RETOUR_CHARIOT_2);
    listeRetourDeChariots.add(UtilitaireString.RETOUR_CHARIOT_3);
    return listeRetourDeChariots;
  }

  /**
   * Méthode qui modifie les changements de lignes web.
   * <p>
   * Cette méthode remplace toutes les occurences de changement de ligne web ("
   * <BR>
   * ") en retour de chariot ("\r\n")
   * <p>
   * 
   * @param texte
   *          la string sur lequel effectuer la modification
   * @return String la string modifiée
   */
  public static String modifierChangementLigneWebEnRetourChariot(String texte) {
    StringBuffer texteConverti = new StringBuffer();

    while (texte.indexOf("<BR>") != -1) {
      texteConverti.append(texte.substring(0, texte.indexOf("<BR>")));
      texteConverti.append(UtilitaireString.RETOUR_CHARIOT_1);
      texte = texte.substring(texte.indexOf("<BR>") + 4);
    }

    while (texte.indexOf("<br/>") != -1) {
      texteConverti.append(texte.substring(0, texte.indexOf("<br/>")));
      texteConverti.append(UtilitaireString.RETOUR_CHARIOT_1);
      texte = texte.substring(texte.indexOf("<br/>") + 5);
    }

    while (texte.indexOf("<br>") != -1) {
      texteConverti.append(texte.substring(0, texte.indexOf("<br>")));
      texteConverti.append(UtilitaireString.RETOUR_CHARIOT_1);
      texte = texte.substring(texte.indexOf("<br>") + 4);
    }

    if (texte.length() > 0) {
      texteConverti.append(texte);
    }

    return texteConverti.toString();
  }

  /**
   * Remplacer les bris de ligne d'une chaine par des retour de chariot.
   * <p>
   * 
   * @param texte
   *          la chaine à modifier
   * @return String la chaîne modifiée
   */
  public static String mettreRetourDeChariot(String texte) {
    if (texte != null) {
      int longueur = texte.length();
      char[] texteTableau = texte.toCharArray();
      StringBuffer texteStringBuffer = new StringBuffer();

      for (int i = 0; i < longueur; i++) {
        if (texteTableau[i] != '\n') {
          texteStringBuffer.append(texteTableau[i]);
        }
      }

      return texteStringBuffer.toString();
    } else {
      return "";
    }
  }

  /**
   * Remplacer les retours de chariot d'une chaine par des bris de ligne.
   * <p>
   * 
   * @param texte
   *          la chaine à modifier
   * @return String la chaîne modifiée
   */
  public static String enleveRetourDeChariot(String texte) {
    if (texte != null) {
      int longueur = texte.length();
      char[] texteTableau = texte.toCharArray();
      StringBuffer texteStringBuffer = new StringBuffer();

      for (int i = 0; i < longueur; i++) {
        if ((i + 1) < longueur) {
          if (texte.substring(i, i + 2).equals("\\r")) {
            texteStringBuffer.append('\r');
            i = i + 1;
          } else {
            texteStringBuffer.append(texteTableau[i]);
          }
        } else {
          texteStringBuffer.append(texteTableau[i]);
        }
      }

      return texteStringBuffer.toString();
    } else {
      return "";
    }
  }

  /**
   * Enlever tous les espaces d'une chaine de caractères.
   * <p>
   * 
   * @param ligne
   *          la chaine à modifier
   * @return String la chaîne modifiée
   */
  public static String enleveTousLesEspaces(String ligne) {
    StringBuffer ligneSansEspace = new StringBuffer();
    ligne = ligne.trim();

    int index = ligne.indexOf(" ");

    while (index > 0) {
      ligneSansEspace.append(ligne.substring(0, index));
      ligne = ligne.substring(index + 1);
      ligne = ligne.trim();
      index = ligne.indexOf(" ");
    }

    if (ligne.length() > 0) {
      ligneSansEspace.append(ligne);
    }

    return ligneSansEspace.toString();
  }

  /**
   * Convertir une chaine de caractères contenant une valeur décimale en
   * pourcentage deux chiffres après le point.
   * <p>
   * Note : La méthode arrondit la deuxième décimale si cette dernière est
   * suivie d'un chiffre plus grand que 4
   * <p>
   * <B>Exemples :</B> <br>
   * <I>98 -> 98.00 %<br>
   * 98.12 -> 98.12%<br>
   * 98.123 -> 98.12%<br>
   * 98.128 -> 98.13%</I><br>
   * <p>
   * 
   * @param nombre
   *          la chaine à modifier
   * @return String la chaîne modifiée
   */
  public static String convertirEnPourcentageDeuxChiffresApresVirgule(String nombre) {
    StringBuffer pourcentage = new StringBuffer();

    if (nombre.indexOf(".") == -1) {
      pourcentage.append(nombre);
      pourcentage.append(".00");
    } else {
      pourcentage.append(nombre.substring(0, nombre.indexOf(".") + 1));
      nombre = nombre.substring(nombre.indexOf(".") + 1);

      if (nombre.length() >= 3) {
        int troisieme = Integer.valueOf(nombre.substring(2, 3)).intValue();

        if (troisieme > 4) {
          pourcentage.append(nombre.substring(0, 1));
          pourcentage.append(Integer.valueOf(nombre.substring(1, 2)).intValue() + 1);
        } else {
          pourcentage.append(nombre.substring(0, 2));
        }
      }

      if (nombre.length() == 2) {
        pourcentage.append(nombre);
      }

      if (nombre.length() == 1) {
        pourcentage.append(nombre);
        pourcentage.append("0");
      }

      if (nombre.length() == 0) {
        pourcentage.append("00");
      }
    }

    pourcentage.append("%");

    return pourcentage.toString();
  }

  /**
   * Convertir une suite de chiffres en un numéro de téléphone pour l'affichage.
   * <p>
   * Note : Il faut fournir un numéro d'au moins dix chiffres.
   * <p>
   * Exemples : <br>
   * 4186272001 -> (418) 627-2001<br>
   * 4186272001101 -> (418) 627-2001 poste 101<br>
   * 41862720011012 -> (418) 627-2001 poste 1012<br>
   * <p>
   * 
   * @param dixChiffresOuPlus
   *          la suite de chiffres à convertir
   * @return String le numéro de téléphone pour affichage
   */
  public static String convertirEnNumeroTelephone(String dixChiffresOuPlus) {
    if (dixChiffresOuPlus == null) {
      return "";
    }

    String numeroDeTelephone = "(";
    numeroDeTelephone = numeroDeTelephone.concat(dixChiffresOuPlus.substring(0, 3));
    dixChiffresOuPlus = dixChiffresOuPlus.substring(3);
    numeroDeTelephone = numeroDeTelephone.concat(")");
    numeroDeTelephone = numeroDeTelephone.concat(dixChiffresOuPlus.substring(0, 3));
    dixChiffresOuPlus = dixChiffresOuPlus.substring(3);
    numeroDeTelephone = numeroDeTelephone.concat("-");

    if (dixChiffresOuPlus.length() > 4) {
      numeroDeTelephone = numeroDeTelephone.concat(dixChiffresOuPlus.substring(0, 4));
      dixChiffresOuPlus = dixChiffresOuPlus.substring(4);
      numeroDeTelephone = numeroDeTelephone.concat(" poste ");
      numeroDeTelephone = numeroDeTelephone.concat(dixChiffresOuPlus);
    } else {
      numeroDeTelephone = numeroDeTelephone.concat(dixChiffresOuPlus);
    }

    return numeroDeTelephone;
  }

  /**
   * Convertir une liste de chaines de caractères en une chaîne délimitée par
   * des virgules.
   * <p>
   * 
   * @param liste
   *          la liste de chaines de caractères
   * @return String le liste délimitée par des virgules
   */
  public static String transformeListeEnStringAVirgule(ArrayList liste) {
    StringBuffer stringAVirgule = new StringBuffer();
    String delimiteur = ", ";
    String guillement = "'";
    Iterator iterator = liste.iterator();

    while (iterator.hasNext()) {
      stringAVirgule.append(guillement);
      stringAVirgule.append((String) iterator.next());
      stringAVirgule.append(guillement);

      if (iterator.hasNext()) {
        stringAVirgule.append(delimiteur);
      }
    }

    return stringAVirgule.toString();
  }

  /**
   * Convertir un suite de neuf chiffres en numéro d'assurance sociale pour
   * affichage.
   * <p>
   * Exemple : 279645123 -> 279 645 123
   * <p>
   * 
   * @param noAssuranceSociale
   *          la suite de chiffres à convertir
   * @return String le numéro d'assurance sociale pour affichage
   */
  public static String getNoAssuranceSocialePourAffichage(String noAssuranceSociale) {
    String noAssuranceSocialeAffichage = null;

    if (noAssuranceSociale.length() != 9) {
      noAssuranceSocialeAffichage = noAssuranceSociale;
    } else {
      noAssuranceSocialeAffichage = (noAssuranceSociale.substring(0, 3).concat(" ")
          .concat(noAssuranceSociale.substring(3, 6)).concat(" ").concat(noAssuranceSociale.substring(6)));
    }

    return noAssuranceSocialeAffichage;
  }

  /**
   * Convertir un suite de douze caractères en numéro d'assurance maladie pour
   * affichage.
   * <p>
   * Exemple : LAPI77072413 -> LAPI 7707 2413
   * <p>
   * 
   * @param noAssuranceMaladie
   *          la suite de chiffres à convertir
   * @return String le numéro d'assurance maladie pour affichage
   */
  public static String getNoAssuranceMaladiePourAffichage(String noAssuranceMaladie) {
    String noAssuranceMaladieAffichage = null;

    if (noAssuranceMaladie.length() != 12) {
      noAssuranceMaladieAffichage = noAssuranceMaladie;
    } else {
      noAssuranceMaladieAffichage = (noAssuranceMaladie.substring(0, 4).concat(" ")
          .concat(noAssuranceMaladie.substring(4, 8)).concat(" ").concat(noAssuranceMaladie.substring(8)));
    }

    return noAssuranceMaladieAffichage.toUpperCase();
  }

  /**
   * Convertir les différentes parties d'un numéro en un numéro de téléphone
   * pour l'affichage.
   * <p>
   * 
   * @param codeRegional
   *          le code régional
   * @param noTelephone
   *          les sept chiffres du numéro de téléphone
   * @param poste
   *          le numéro de poste
   * @return String le numéro de téléphone pour affichage
   */
  public static String getNoTelephonePourAffichage(String codeRegional, String noTelephone, String poste) {
    StringBuffer noTelephoneAffichage = new StringBuffer();

    if (!isVide(codeRegional)) {
      noTelephoneAffichage.append("(" + codeRegional + ") ");
    }

    if (!isVide(noTelephone)) {
      if (noTelephone.length() == 8) {
        noTelephoneAffichage.append(noTelephone);
      } else {
        noTelephoneAffichage.append(noTelephone.substring(0, 3)).append("-").append(noTelephone.substring(3));
      }
    }

    if (!isVide(poste)) {
      noTelephoneAffichage.append(" poste ").append(poste);
    }

    return noTelephoneAffichage.toString();
  }

  /**
   * Convertir une suite de six caractères en un code postal pour affichage.
   * <p>
   * Note : Si le code postal passé est déjà à six caractères il est retourné
   * indemne.
   * <p>
   * 
   * @param newCodePostal
   *          la suite de caractères à convertir
   * @return String le code postal pour affichage
   */
  public static String getCodePostalPourAffichage(String newCodePostal) {
    String codePostal = enleveTousLesEspaces(newCodePostal).toUpperCase();

    if (codePostal.length() == 6) {
      return codePostal.substring(0, 3) + " " + codePostal.substring(3, 6);
    } else {
      return codePostal;
    }
  }

  /**
   * Méthode qui valide si une String correspond à un code postal
   * <p>
   * Cette méthode prend une String et valide si cette dernière est un code
   * postal valide la String passée en paramètre peut avoir un espace ou pas,
   * cela n'a aucune importance.
   * <p>
   * 
   * @param codePostal
   *          la suite de caractères à valider
   * @return boolean indique si la suite de caractères est bien un code postal
   */
  public static boolean isCodePostal(String codePostal) {
    boolean isValide = true;

    codePostal = UtilitaireString.enleveTousLesEspaces(codePostal).toUpperCase();

    if (codePostal.length() != 6) {
      isValide = false;
    } else {
      if ((codePostal.charAt(0) < 'A') || (codePostal.charAt(0) > 'Z')) {
        isValide = false;
      }

      if ((codePostal.charAt(2) < 'A') || (codePostal.charAt(2) > 'Z')) {
        isValide = false;
      }

      if ((codePostal.charAt(4) < 'A') || (codePostal.charAt(4) > 'Z')) {
        isValide = false;
      }

      try {
        Integer.parseInt(codePostal.substring(1, 2));
        Integer.parseInt(codePostal.substring(3, 4));
        Integer.parseInt(codePostal.substring(5));
      } catch (NumberFormatException e) {
        isValide = false;
      }
    }

    return isValide;
  }

  /**
   * Méthode qui valide si une String correspond à une adresse de courriel
   * électronique
   * <p>
   * Cette méthode prend une String et valide si cette dernière est un courriel
   * valide
   * <p>
   * 
   * @param value
   *          la suite de caractères à valider
   * @return boolean indique si la suite de caractères est bien un courriel
   */
  public static boolean isCourriel(String value) {
    Pattern p = Pattern
        .compile("^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");
    Matcher m = p.matcher(value);
    return m.matches();
  }

  /**
   * Mettre la première lettre de chaque mot d'une chaîne en majuscule (incluan
   * les mots séparés avec des tirets).
   * <p>
   * 
   * @param chaine
   *          La chaîne à modifier
   * @return string la chaîne modifiée
   * @deprecated Use
   *             {@link UtilitaireString#convertirToutesPremieresLettreMajuscule}
   *             instead
   */
  @Deprecated
  public static String convertirPremieresLettresEnMajuscule(String chaine) {
    StringBuffer modifier = new StringBuffer();

    for (StringTokenizer tk = new StringTokenizer(chaine, " -", true); tk.hasMoreTokens();) {
      modifier.append(UtilitaireString.convertirPremiereLettreEnMajuscule(tk.nextToken()));
    }

    return modifier.toString();
  }

  /**
   * Traiter les apostrophes pouvant être contenu dans un message.
   * 
   * @param string
   *          Le message avec les apostrophes traités.
   * @param isJavascript
   *          si la fonction est utilisée dans une fonction javascript.
   */
  public static String traiterApostrophe(String string, boolean isJavascript) {
    if ((string == null) || (string.indexOf('\'') < 0)) {
      return (string);
    }

    int n = string.length();
    StringBuffer sb = new StringBuffer(n);

    for (int i = 0; i < n; i++) {
      char ch = string.charAt(i);

      if (ch == '\'') {
        if (isJavascript) {
          sb.append('\\');
        } else {
          sb.append('\'');
        }
      }

      sb.append(ch);
    }

    return (sb.toString());
  }

  /**
   * Traiter les apostrophes pouvant être contenu dans un message.
   * 
   * @param string
   *          Le message avec les apostrophes traités.
   */
  public static String traiterApostrophe(String string) {
    return traiterApostrophe(string, false);
  }

  /**
   * Méthode qui sert à valider si une chaîne de charactères est de type alpha
   * numérique
   * <p>
   * Cette méthode valide que les charactères soient numériques ou de lettres.
   * Les lettres minuscules et majuscules sont acceptées.
   * <p>
   * 
   * @param stringAValider
   *          la chaîne de charactères à valider
   * @return la valeur indiquant si la string est bien alpha numérique
   */
  public static boolean isAlphaNumerique(String stringAValider) {
    boolean valide = false;

    if (stringAValider != null) {
      valide = true;

      char[] tableauCharacteres = stringAValider.trim().toUpperCase().toCharArray();

      for (int i = 0; i < tableauCharacteres.length; i++) {
        char charactere = tableauCharacteres[i];

        if ((charactere < 65) || (charactere > 90)) {
          if ((charactere < 48) || (charactere > 57)) {
            valide = false;
          }
        }
      }
    }

    return valide;
  }

  /**
   * Vérifier si la chaine de caractères est vide ou null.
   * 
   * @param str
   *          la chaine de caractères à valider.
   * @return true si la chaine de caractère est vide.
   */
  public static boolean isVide(String str) {
    return ((str == null) || (str.trim().length() == 0));
  }

  /**
   * Converti une String pour être conforme en JavaScript.
   * 
   * @param s
   *          la chaine de caractère original
   * @return la chaine de caractère convertit.
   */
  public static String convertirEnJavaScript(String s) {
    String traiteEnJS = StringEscapeUtils.escapeJavaScript(s);

    return traiteEnJS;
  }

  /**
   * Converti une String pour être conforme en HTML.
   * 
   * @param s
   *          la chaine de caractère original
   * @return la chaine de caractère convertit.
   */
  public static String convertirEnHtml(String s) {
    String traiteEnHtml = StringEscapeUtils.escapeHtml(s);

    return traiteEnHtml;
  }

  /**
   * Décoder un url
   * 
   * @return l'url décodé
   * @param s
   *          l'url a décodé
   */
  public static String decoderUrl(String s) {
    String traiteEnHtml = null;

    try {
      traiteEnHtml = URLDecoder.decode(s, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new SOFIException(e);
    }

    return traiteEnHtml;
  }

  /**
   * Décoder un url
   * 
   * @return l'url décodé
   * @param s
   *          l'url a décodé
   */
  public static String encoderUrl(String s) {
    String traiteEnHtml = null;

    try {
      traiteEnHtml = URLEncoder.encode(s, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new SOFIException(e);
    }

    return traiteEnHtml;
  }

  /**
   * Permet de remplacer un élément dans une chaine de caractère pour un autre
   * élément.
   * 
   * @return la nouvelle chaine de caractère
   * @param remplacement
   *          la chaine de caractère de remplacement.
   * @param source
   *          la source à remplacer.
   * @param texte
   *          le texte initial.
   */
  public static String remplacerTous(String texte, String source, String remplacement) {
    StringBuffer nouveauTexte = new StringBuffer();
    int indiceSource = 0;

    while ((texte.length() > 0) && (indiceSource != -1)) {
      indiceSource = texte.indexOf(source);

      if (source.equals(" ") && (indiceSource == -1)) {
        // Bug ie lorsque un blanc est utilisé, il correspond à un &nbsp; et non
        // pas un blanc.
        indiceSource = texte.indexOf(160);
      }

      if (indiceSource != -1) {
        String section = texte.substring(0, indiceSource);
        nouveauTexte.append(section);
        nouveauTexte.append(remplacement);
        texte = texte.substring(indiceSource + source.length());
      } else {
        nouveauTexte.append(texte);
      }
    }

    return nouveauTexte.toString();
  }

  /**
   * Valide si une chaine de caractères est composé du bon nombre de chiffre et
   * de lettres.
   * 
   * @param chaine
   * @param nombreMinimalLettre
   * @param nombreMinimalChiffre
   * @return Est-ce que la chaine est bien composé des bons nombres et lettres.
   */
  public static boolean isNombreChiffreEtLettre(String chaine, int nombreMinimalLettre, int nombreMinimalChiffre) {
    char[] caracteres = chaine.toCharArray();
    int nombreLettres = 0;
    int nombreChiffres = 0;

    for (int i = 0; i < caracteres.length; i++) {
      char car = caracteres[i];

      if ("0123456789".indexOf(car) != -1) {
        nombreChiffres++;
      } else {
        nombreLettres++;
      }
    }

    return (nombreLettres >= nombreMinimalLettre) && (nombreChiffres >= nombreMinimalChiffre);
  }

  /**
   * Permet de remplacer un élément dans une chaine de caractère pour un autre
   * élément.
   * 
   * @return la nouvelle chaine de caractère
   * @param remplacement
   *          la chaine de caractère de remplacement.
   * @param source
   *          la source à remplacer.
   * @param texte
   *          le texte initial.
   */
  public static String supprimerTous(String texte, int caractere) {
    StringBuffer nouveauTexte = new StringBuffer();
    int indiceSource = 0;

    while ((texte.length() > 0) && (indiceSource != -1)) {
      indiceSource = texte.indexOf(0);

      if (indiceSource != -1) {
        String section = texte.substring(0, indiceSource);
        nouveauTexte.append(section);
        texte = texte.substring(indiceSource);
      } else {
        nouveauTexte.append(texte);
      }
    }

    return nouveauTexte.toString();
  }

  /**
   * Ajoute un caractères (avant ou après) une d'une chaine de caractère pour
   * une longueur donnée
   * <p>
   * Exemple : "toto" -> "   toto"
   * <p>
   * 
   * @param caracterePadding
   *          de remplissage
   * @param chaineInitiale
   *          de caractere
   * @param longueurTotalChaine
   *          totale de la chaine
   * @param mettrePaddingAvant
   *          les caractères en avant ou après la chaine
   * @return String la chaine de caractère de la longueur demandé
   */
  public static String ajouterCaractere(String caracterePadding, Object chaineInitiale, int longueurTotalChaine,
      boolean mettrePaddingAvant) {
    StringBuffer buffer = new StringBuffer();

    if (chaineInitiale != null) {
      String chaineInitialeTemp = chaineInitiale.toString();
      int longueurChaine = chaineInitialeTemp.length();

      if (!mettrePaddingAvant) {
        buffer.append(chaineInitiale);
      }

      for (int i = 0; i < (longueurTotalChaine - longueurChaine); i++) {
        buffer.append(caracterePadding);
      }

      if (mettrePaddingAvant) {
        buffer.append(chaineInitiale);
      }
    } else {
      for (int i = 0; i < longueurTotalChaine; i++) {
        buffer.append(caracterePadding);
      }
    }

    return buffer.toString();
  }

  /**
   * Décorer une chaine de caractère avec un ou plusieurs caractères pour
   * compléter à gauche la chaine à décorer pour une certaine longueur.
   * 
   * @return la chaine décorer.
   * @param longeurMaximale
   *          la longueur complète de la chaine doit avoir.
   * @param decorateur
   *          la chaine décorateur.
   * @param chaineADecorer
   *          la chaine initiale à décorer.
   */
  public static String decorerAGauche(String chaineADecorer, String decorateur, int longeurMaximale) {
    if (chaineADecorer.length() == 0) {
      return chaineADecorer;
    }

    StringBuffer strb = new StringBuffer(longeurMaximale);
    StringCharacterIterator sci = new StringCharacterIterator(decorateur);

    while (strb.length() < (longeurMaximale - chaineADecorer.length())) {
      for (char ch = sci.first(); ch != CharacterIterator.DONE; ch = sci.next()) {
        if (strb.length() < (longeurMaximale - chaineADecorer.length())) {
          strb.insert(strb.length(), String.valueOf(ch));
        }
      }
    }

    return strb.append(chaineADecorer).toString();
  }

  /**
   * Décorer une chaine de caractère avec un ou plusieurs caractères pour
   * compléter à droite la chaine à décorer pour une certaine longueur.
   * 
   * @return la chaine décorer.
   * @param longeurMaximale
   *          la longueur complète de la chaine doit avoir.
   * @param decorateur
   *          la chaine décorateur.
   * @param chaineADecorer
   *          la chaine initiale à décorer.
   */
  public static String decorerADroite(String chaineADecorer, String decorateur, int longeurMaximale) {
    if (chaineADecorer.length() == 0) {
      return chaineADecorer;
    }

    StringBuffer strb = new StringBuffer(chaineADecorer);
    StringCharacterIterator sci = new StringCharacterIterator(decorateur);

    while (strb.length() < longeurMaximale) {
      for (char ch = sci.first(); ch != CharacterIterator.DONE; ch = sci.next()) {
        if (strb.length() < longeurMaximale) {
          strb.append(String.valueOf(ch));
        }
      }
    }

    return strb.toString();
  }

  /**
   * Supprimer tous les blancs d'une chaine de caractères
   * 
   * @return la chaine de caractères sans les blancs.
   * @param chaineATraiter
   *          la chaine a traiter.
   */
  public static String supprimerTousLesBlancs(String chaineATraiter) {
    char[] tableau = chaineATraiter.toCharArray();
    StringBuffer nombreSansBlanc = new StringBuffer();

    for (int i = 0; i < tableau.length; i++) {
      int asciiCode = tableau[i];

      if ((asciiCode != 32) && (asciiCode != 160)) {
        nombreSansBlanc.append(tableau[i]);
      }
    }

    return nombreSansBlanc.toString();
  }

  /**
   * Retourne le nombre de mot qui est contenu dans une chaine de caractères.
   * 
   * @return le nombre de mot qui est contenu dans une chaine de caractères.
   * @param chaineATraiter
   *          la chaine a traiter.
   * @param mot
   *          le mot a traiter.
   */
  public static int getNbMotDansChaine(String chaineATraiter, String mot) {
    int nbMot = 0;

    if ((chaineATraiter != null) && (mot != null)) {
      StringTokenizer token = new StringTokenizer(chaineATraiter, ".");

      while (token.hasMoreTokens()) {
        String motTrouve = token.nextToken();

        if (motTrouve.equals(mot)) {
          nbMot++;
        }
      }
    }

    return nbMot;
  }

  /**
   * Permet de traiter les mots qui excède une limite permise afin d'y ajouter
   * un saut de ligne HTML.
   * 
   * @return la chaine de valeur convertit avec saut de ligne inséré.
   * @param separateur
   *          le séparateur de saut de ligne.
   * @param nombreCaractereMaximal
   *          le nombre de caractères possibles avant de forcer un saut de
   *          ligne.
   * @param valeur
   *          la valeur convertit avec des sauts de lignes HTML.
   */
  public static String traiterChaineCaractereTropLongueAvecSautLigneHTML(String valeur, Integer nombreCaractereMaximal,
      String separateur) {

    if (separateur == null) {
      separateur = "";
    }

    // Traitement pour longue chaine de caratère sans changement de ligne.
    if (nombreCaractereMaximal != null) {
      StringBuffer chaineComplete = new StringBuffer();

      // Les mots séparés par un séparateur
      StringTokenizer chaineMot = new StringTokenizer(valeur, separateur);

      int longueurMax = nombreCaractereMaximal.intValue();

      boolean initialiser = false;

      while (chaineMot.hasMoreTokens()) {
        String mot = chaineMot.nextToken();

        if (mot.length() > longueurMax) {

          if (!separateur.equals("")) {

            if (initialiser) {
              chaineComplete.append("<br/>");
            }

            chaineComplete.append(mot);
            chaineComplete.append(separateur);
            if (!initialiser) {
              chaineComplete.append("<br/>");
            }
          } else {
            String premierePartie = mot.substring(0, longueurMax);
            String secondePartie = mot.substring(longueurMax);
            chaineComplete.append(premierePartie);
            chaineComplete.append("<br/>");
            chaineMot = new StringTokenizer(secondePartie, separateur);
          }
          initialiser = true;
          longueurMax = nombreCaractereMaximal.intValue();
        } else {
          if (!separateur.equals("")) {
            longueurMax = longueurMax - mot.length() - 1;
            chaineComplete.append(mot);
            if (valeur.indexOf(separateur) != -1) {
              chaineComplete.append(separateur);
            }
          } else {
            longueurMax = longueurMax - mot.length();
            chaineComplete.append(mot);
          }
          initialiser = true;
        }
      }
      valeur = chaineComplete.toString();

      if (StringUtils.isNotEmpty(valeur) && StringUtils.isNotEmpty(separateur)
          && valeur.substring(valeur.length() - 1).indexOf(separateur) != -1) {
        valeur = valeur.substring(0, valeur.length() - 1);
      }
    }

    return valeur;
  }

  /**
   * Récupérer les parties d'une châine de caractère qui se trouve entre deux
   * chaînes délimiteures.
   * 
   * Par exemple :
   * 
   * substringsEntre("abc[allo]def[123]ghi", "[", "]") = [allo,123]
   * 
   * @param chaine
   *          Chaîne que l'on veut découper
   * @param ouverture
   *          chaîne d'ouverture
   * @param fermeture
   *          chaîne de fermeture
   * @return tableau de chaîne avec les sections découpées
   * @since 3.1 Utilisation de <code>StringUtils</code> car même résultat.
   */
  public static String[] substrings(String chaine, String ouverture, String fermeture) {
    return StringUtils.substringsBetween(chaine, ouverture, fermeture);
  }

  /**
   * Convertir une chaîne de caractère pour qu'elle soit sans accent,.
   * 
   * @param chaine
   * @return chaine en majuscule sans accents
   * @since SOFI 2.0.3
   */
  public static String toUpperSansAccents(String chaine, String encoding) throws Exception {
    chaine = new String(chaine.getBytes(), encoding);
    chaine = chaine.toUpperCase();
    chaine = remplace(chaine, 'Ý', 'Y');
    chaine = remplace(chaine, new char[] { 'Ù', 'Ú', 'Û', 'Ü' }, 'U');
    chaine = remplace(chaine, new char[] { 'Ò', 'Ó', 'Ô', 'Õ', 'Ö' }, 'O');
    chaine = remplace(chaine, new char[] { 'Ì', 'Í', 'Î', 'Ï' }, 'I');
    chaine = remplace(chaine, new char[] { 'È', 'É', 'Ê', 'Ë' }, 'E');
    chaine = remplace(chaine, 'Ç', 'C');
    chaine = remplace(chaine, new char[] { 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ' }, 'A');
    chaine = new String(chaine.getBytes(), encoding);
    return chaine;
  }

  public static String remplace(String chaine, char[] caracteres, char remplacement) {
    for (int i = 0; i < caracteres.length; i++) {
      char car = caracteres[i];
      chaine = chaine.replace(car, remplacement);
    }
    return chaine;
  }

  public static String remplace(String chaine, char caractere, char remplacement) {
    return chaine.replace(caractere, remplacement);
  }

  /**
   * Permet de vérifier si une valeur est contenu dans un tableau de valeur
   * 
   * @param tableau
   *          tableau a valider
   * @param valeur
   *          la valeur a tester si présente dans le tableau.
   * @return true si valeur présente dans le tableau.
   * @since SOFI 2.0.4
   */
  public static boolean contient(String[] tableau, String valeur) {
    for (int i = 0; i < tableau.length; i++) {
      if (tableau[i].equals(valeur.trim())) {
        return true;
      }
    }
    return false;
  }

  /**
   * Permet de mettre en lettre majuscule la première lettre de la valeur. <br/>
   * Si la valeur est un prenom (ou nom) composé (les séparations prisent en
   * compte sont le tiret et l'espace), on met en majuscule également chaque
   * lettre qui suit le séparateur.
   * 
   * @param valeur
   */
  public static String capitalizePremiereLettre(String valeur) {

    String valeurRetour = null;

    if (valeur != null) {
      String splitTiret = "-";
      String splitEspace = " ";
      String valeurTraitement = "";
      String[] valeurSplitTiret = valeur.split(splitTiret);
      for (String valeurTiret : valeurSplitTiret) {
        if (StringUtils.isNotBlank(valeurTraitement)) {
          valeurTraitement += splitTiret;
        }
        valeurTraitement += StringUtils.capitalize(valeurTiret);
      }
      valeurRetour = "";
      String[] valeurSplitEspace = valeurTraitement.split(splitEspace);
      for (String valeurEspace : valeurSplitEspace) {
        if (StringUtils.isNotBlank(valeurRetour)) {
          valeurRetour += splitEspace;
        }
        valeurRetour += StringUtils.capitalize(valeurEspace);
      }
    }
    return valeurRetour;

  }

}
