/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;
import org.sofiframework.utilitaire.documentelectronique.UtilitaireDocumentElectronique;

/**
 * Utilitaire dans l'utilisation des rapports JasperReports.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.1
 */
public class UtilitaireJasperReports {

  /**
   * Instance du singleton
   */
  private static UtilitaireJasperReports instance =
      new UtilitaireJasperReports();

  /**
   * Instance commune de journalisation.
   */
  private static final Log log =
      LogFactory.getLog(UtilitaireJasperReports.class);

  /**
   * Constructeur de l'utilitaire.
   */
  protected UtilitaireJasperReports() {
    super();
  }

  /**
   * Retourne l'instance du singleton.
   */
  public static UtilitaireJasperReports getInstance() {
    return instance;
  }

  /**
   * Retourne une instance d'un rapport JasperReports.
   * @param ressource la ressource a utiliser
   * @return une instance d'un rapport JasperReports.
   */
  public JasperReport getJasperReport(String ressource) {

    try {
      InputStream in = getClass().getResourceAsStream(ressource);
      JasperReport jasperReport = (JasperReport)JRLoader.loadObject(in);
      return jasperReport;
    } catch (JRException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour lire la ressource jasper.", e);
      }
      throw new SOFIException("La ressources '" + ressource +
          "' est absente des classes du projet.");
    }

  }

  /**
   * Lancer la génération d'un JasperReport basé sur des collections.
   * @param jasperReport l'instance JasperReport a généré en PDF.
   * @param parametres les paramètres envoyés au JasperReport.
   * @param collection la collection a envoyée au JasperReport.
   * @return le document JasperReport généré en byte[].
   * @throws Exception l'exception généré lors du lancement de la génération
   * du PDF depuis le JasperReport.
   */
  public byte[] genererJasperReport(JasperReport jasperReport, Map parametres,
      Collection collection) throws Exception {
    return JasperRunManager.runReportToPdf(jasperReport, parametres,
        new JRBeanCollectionDataSource(collection));
  }

  /**
   * Lancer la génération d'un JasperReport basé sur des requête SQL.
   * @param jasperReport l'instance JasperReport a généré en PDF.
   * @param parametres les paramètres envoyés au JasperReport.
   * @param connection la connection en cours de traitement.
   * @return le document JasperReport généré en byte[].
   * @throws Exception l'exception généré lors du lancement de la génération
   * du PDF depuis le JasperReport.
   */
  public byte[] genererJasperReportSql(JasperReport jasperReport, Map parametres,
      Connection connection) throws Exception {
    return JasperRunManager.runReportToPdf(jasperReport, parametres,
        connection);
  }

  /**
   * Génération d'un document électronique depuis l'emplacement
   * d'un rapport JasperReport.
   * @param ressource  l'emplacement d'un rapport JasperReport.
   * @param parametres les paramètres a envoyé au rapport JasperReport.
   * @param collection la collection a envoyé au rapport Jasper.
   * @param nomFichier le nom du fichier a générer.
   * @return
   */
  public DocumentElectronique genererDocumentEletronique(String ressource,
      Map parametres, Collection collection,
      String nomFichier) throws Exception {

    JasperReport jasperReport = getJasperReport(ressource);
    DocumentElectronique documentPdf = new DocumentElectronique();

    try {
      byte[] rapportRetour = null;
      rapportRetour =
          genererJasperReport(jasperReport, parametres, collection);
      String typeMime =
          UtilitaireDocumentElectronique.getTypeMimeFichier(nomFichier);
      documentPdf.setCodeTypeMime(typeMime);
      documentPdf.setNomFichier(nomFichier);
      documentPdf.setValeurDocument(rapportRetour);
      documentPdf.setVolumeFichier(new Integer(rapportRetour.length));
    } catch (Exception e) {
      throw e;
    }

    return documentPdf;
  }

  /**
   * Génération d'un document électronique depuis l'emplacement
   * d'un document JasperReport.
   * @param ressource  l'emplacement d'un document JasperReport.
   * @param parametres les paramètres a envoyé au document JasperReport.
   * @param connection la connexion a utiliser pour générer le document Jasper.
   * @param nomFichier le nom du fichier a générer.
   * @return le document électronique prêt au téléchargement de l'utilisateur.
   */
  public DocumentElectronique genererDocumentEletronique(String ressource,
      Map parametres, Connection connection,
      String nomFichier) throws Exception {

    JasperReport jasperReport = getJasperReport(ressource);
    DocumentElectronique documentPdf = new DocumentElectronique();

    try {
      byte[] rapportRetour = null;
      rapportRetour =
          genererJasperReportSql(jasperReport, parametres, connection);
      String typeMime =
          UtilitaireDocumentElectronique.getTypeMimeFichier(nomFichier);
      documentPdf.setCodeTypeMime(typeMime);
      documentPdf.setNomFichier(nomFichier);
      documentPdf.setValeurDocument(rapportRetour);
      documentPdf.setVolumeFichier(new Integer(rapportRetour.length));
    } catch (Exception e) {
      throw e;
    }

    return documentPdf;
  }

  /**
   * Retourne un document électronique depuis un nom de fichier
   * et une valeur de document (déjà générer par JasperReports).
   * @param valeurDocument une valeur du document (déjà générer par JasperReports).
   * @param nomFichier le nom du fichier associé au document.
   * @return un document électronique de type PDF depuis un nom de fichier
   * et une valeur du document (déjà générer par JasperReports).
   */
  public DocumentElectronique getDocumentEletronique(byte[] valeurDocument,
      String nomFichier) {

    DocumentElectronique documentPdf = new DocumentElectronique();

    String typeMime =
        UtilitaireDocumentElectronique.getTypeMimeFichier(nomFichier);
    documentPdf.setCodeTypeMime(typeMime);
    documentPdf.setNomFichier(nomFichier);
    documentPdf.setValeurDocument(valeurDocument);
    documentPdf.setVolumeFichier(new Integer(valeurDocument.length));

    return documentPdf;
  }
}
