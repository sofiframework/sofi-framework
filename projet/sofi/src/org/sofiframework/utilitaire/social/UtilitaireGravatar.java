/*
 * Copyright 2003-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.social;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.securite.encryption.EncryptionException;
import org.sofiframework.securite.encryption.UtilitaireEncryption;

/**
 * Utilitaire qui retourne l'avantar d'utilisateur basé sur Gravatar.
 * 
 * @author Jean-François Brassard (Sun Media Corp)
 * @version SOFI 3.2
 */
public class UtilitaireGravatar {

  private static final String GRAVATAR_HTTP = "http://www.gravatar.com/avatar/";
  private static final String GRAVATAR_HTTPS = "https://secure.gravatar.com/avatar/";

  public static String getUrl(String courriel, String format, boolean https) throws Exception {
    String url = null;
    
    if (StringUtils.isNotEmpty(courriel)) {
      String hash = null;

      try {
        hash = UtilitaireEncryption.hashMD5ToHex(courriel);
      } catch (EncryptionException e) {
        throw new Exception(e);
      }

      try {
        url = construireUrl(hash, format, https);
      } catch (UnsupportedEncodingException e1) {
        throw new Exception(e1);
      }

      try {
        if (!testUrl(url)) {
          url = null;
        }
      } catch (IOException e) {
        throw new Exception(e);
      }
    }
    
    return url;

  }

  static private String construireUrl(String hash, String format, boolean https)
      throws UnsupportedEncodingException {
    StringBuilder sb = new StringBuilder();
    if (https) {
      sb.append(GRAVATAR_HTTPS);
    } else {
      sb.append(GRAVATAR_HTTP);
    }

    sb.append(hash);
    sb.append("?d=404");

    if (format != null) {
      sb.append("&s=").append(format);
    }

    return sb.toString();
  }

  static private boolean testUrl(String path) throws IOException {
    boolean testOk = false;
    URL u = new URL(path);
    HttpURLConnection conn = (HttpURLConnection) u.openConnection();
    conn.setRequestMethod("HEAD");
    testOk = conn.getResponseCode() == 200;
    return testOk;
  }

}