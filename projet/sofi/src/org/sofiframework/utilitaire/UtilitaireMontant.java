/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;


/**
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.3
 */
public class UtilitaireMontant {
  /**
   * Formatter un montant selon le format désiré et le locale de l'utilisateur.
   * @return un nombre
   * @param isFormatMonetaire
   * @param monetaire
   * @param localeUtilisateur
   * @param valeur
   * @param format
   * @deprecated utiliser UtilitaireNombre.getValeurFormate depuis SOFI 1.8
   */
  @Deprecated
  public static String getMontantFormatte(String format, Object valeur,
      Locale localeUtilisateur, boolean monetaire) {
    String decimalFormat = "";

    if ((valeur != null) && String.class.isInstance(valeur)) {
      valeur = UtilitaireNombre.getDouble(valeur.toString(), localeUtilisateur);
    }

    int nbDecimal = 0;

    // Valider si le format doit appliquer les valeurs monétaire.
    if (format.indexOf("$") != -1) {
      monetaire = true;
    }

    if (format.lastIndexOf(".") != -1) {
      String decimal = format.substring(format.lastIndexOf("."));
      nbDecimal = decimal.length() - 1;

      if (monetaire) {
        nbDecimal--;
      }
    }

    if ((Number.class.isInstance(valeur) ||
        BigDecimal.class.isInstance(valeur)) && (format != null)) {
      DecimalFormat form;

      if (monetaire) {
        form = (DecimalFormat) NumberFormat.getCurrencyInstance(localeUtilisateur);
      } else {
        form = (DecimalFormat) NumberFormat.getNumberInstance(localeUtilisateur);
      }

      //form.setGroupingSize(3);
      if (nbDecimal > 0) {
        form.setMaximumFractionDigits(nbDecimal);
        form.setMinimumFractionDigits(nbDecimal);
      }

      decimalFormat = (form).format(valeur);
    }

    if (monetaire) {
      return decimalFormat;
    }

    // patch DecimalFormat pour supprimer caractère bidon a la fin dans le cas d'un format d'un champ de saisie.
    char[] tableau = decimalFormat.toCharArray();
    StringBuffer montantFormatte = new StringBuffer();

    for (int i = 0; i < tableau.length; i++) {
      switch (tableau[i]) {
      case '1':
        montantFormatte.append("1");

        break;

      case '2':
        montantFormatte.append("2");

        break;

      case '3':
        montantFormatte.append("3");

        break;

      case '4':
        montantFormatte.append("4");

        break;

      case '5':
        montantFormatte.append("5");

        break;

      case '6':
        montantFormatte.append("6");

        break;

      case '7':
        montantFormatte.append("7");

        break;

      case '8':
        montantFormatte.append("8");

        break;

      case '9':
        montantFormatte.append("9");

        break;

      case '0':
        montantFormatte.append("0");

        break;

      case '.':
        montantFormatte.append(".");

        break;

      case '-':
        montantFormatte.append("-");

        break;

      case ',':
        montantFormatte.append(",");

        break;

      default:
        montantFormatte.append(" ");

        break;
      }
    }

    return montantFormatte.toString();
  }
}
