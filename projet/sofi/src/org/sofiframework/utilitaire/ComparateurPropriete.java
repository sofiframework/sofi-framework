/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;

public class ComparateurPropriete implements Comparator {
  private String propriete;
  private Comparator comparateur;

  public ComparateurPropriete(String propriete) {
    this(propriete, null);
  }

  public ComparateurPropriete(String propriete, Comparator comparateur) {
    this.propriete = propriete;
    this.comparateur = comparateur;
  }

  @Override
  public int compare(Object o1, Object o2) {
    if (comparateur == null) {
      Comparable c1 = (Comparable) getPropriete(o1);
      Comparable c2 = (Comparable) getPropriete(o2);
      return c1.compareTo(c2);
    } else {
      Object p1 = getPropriete(o1);
      Object p2 = getPropriete(o2);
      return comparateur.compare(p1, p2);
    }
  }

  private Object getPropriete(Object bean) {
    try {
      return PropertyUtils.getProperty(bean, propriete);
    } catch (NoSuchMethodException e) {
      throw new IllegalArgumentException(
          "Aucun getter pour la propriété " + propriete +
          " dans la classe " + bean.getClass().getName());
    } catch (IllegalAccessException e) {
      throw new IllegalArgumentException(
          "Le getter pour la propriété " + propriete +
          " dans la classe " + bean.getClass().getName() +
          " doit être publique");
    } catch (InvocationTargetException e) {
      Throwable cause = e.getCause();
      if (cause instanceof Error) {
        throw (Error) cause;
      } else if (cause instanceof RuntimeException) {
        throw (RuntimeException) cause;
      } else {
        throw new RuntimeException(cause);
      }
    }
  }

}
