/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire.exception;


/**
 * Exception lancé par l'utilitaire des expressions régulières.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.1
 * @see org.sofiframework.utilitaire.UtilitaireExpressionReguliere
 */
public class ExpressionReguliereFormatInvalideException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1264977893652699920L;

  /**
   * Constructeur de l'exception
   */
  public ExpressionReguliereFormatInvalideException() {
    super();
  }

  /**
   * Constructeur à l'aide d'un message spécifique.
   * <p>
   * @param msg Message d'exception spécifique
   */
  public ExpressionReguliereFormatInvalideException(String msg) {
    super(msg);
  }

  /**
   * Constructeur à l'aide d'un message ainsi que de la cause du l'exception.
   * <p>
   * @param msg Message d'exception spécifique
   * @param cause La cause de l'exception
   */
  public ExpressionReguliereFormatInvalideException(String msg, Throwable cause) {
    super(msg, cause);
  }

  /**
   * Constructeur à l'aide d'une cause de l'exception.
   * <p>
   * @param cause La cause de l'exception
   */
  public ExpressionReguliereFormatInvalideException(Throwable cause) {
    super(cause);
  }
}
