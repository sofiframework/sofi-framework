/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.exception.SOFIException;

/**
 * Classe assurant la gestion des requêtes http.
 * <p>
 * Cet objet fournit des utilitaires permettant d'intéragir avec les requêtes
 * Http.
 * <p>
 *
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class UtilitaireHttp {
  /**
   * Instance commune de journalisation.
   */
  private static final Log log = LogFactory.getLog(UtilitaireHttp.class);
  private String urlDomaine;
  private String urlPath;
  private int urlPort;
  private boolean isSecure = false;
  private HttpClient httpClient;
  private GetMethod appelUrl;
  private int codeStatusHttpReponse;

  /**
   * Constructeur par défaut.
   */
  public UtilitaireHttp() {
    enleverVerificationCertificat();
  }

  /**
   * Méthode qui permet de retirer le contenu qui est dans le tag body qui est
   * contenu dans une page HTML
   * <p>
   * S'il n'y a pas de tag body, le méthode ne fait rien.
   * <p>
   *
   * @return
   * @param contenuOriginal
   *            Contenu original du HTML.
   */
  public static String getBody(String contenuOriginal) {
    String retour = null;

    if (contenuOriginal != null) {
      Pattern patternStartTag = Pattern.compile("<(?i)body");
      String[] contenuEnDeuxMorceau = patternStartTag.split(contenuOriginal);

      if (contenuEnDeuxMorceau.length == 2) {
        String body = contenuEnDeuxMorceau[1];
        int positionDebutBody = body.indexOf(">");
        body = body.substring(positionDebutBody+1);
        int positionFin = body.indexOf("</body>");
        body = body.substring(0, positionFin);
        retour = body;
      } else {
        retour = contenuOriginal;
      }
    }

    return retour;
  }

  /**
   * Méthode qui permet d'appeller une page web utilisant le sofi HttpClient.
   * <p>
   *
   * @return HttpClient contenant la configuration du client
   */
  public HttpClient genererURL() {
    this.httpClient = new HttpClient();
    this.httpClient.getHostConfiguration().setHost(getUrlDomaine(),
        getUrlPort(), getTypeConnexion());

    return this.httpClient;
  }

  /**
   * Méthode qui permet d'appeller une page web utilisant le sofi HttpClient.
   * <p>
   *
   * @param isLiberation
   *            Permet de libérer la connection si true. Aucune libération
   *            sinon
   */
  public void appellerURL(boolean isLiberation) {
    try {
      this.genererURL();
      this.appelUrl = new GetMethod(getUrlPath());
      this.httpClient.executeMethod(appelUrl);
    } catch (Exception e) {
      traiterException("Erreur appellerURL", e);
    } finally {
      if (isLiberation && (this.appelUrl != null)) {
        this.appelUrl.releaseConnection();
      }
    }
  }

  /**
   * Méthode qui permet d'envoyer un fichier vers un autre serveur.
   * <p>
   *
   * @param nomFichier
   *            Le nom du fichier xml à transférer
   * @return Code de réponse "HTTP POST method"
   */
  public int envoyerFichierXML(String nomFichier) {
    int resultat = 0;
    PostMethod post = null;

    try {
      // Le contenu de la requête va etre directement extrait
      // flux d'entrée
      post = new PostMethod(getUrlPath());

      File fichier = new File(nomFichier);

      if (fichier.length() < Integer.MAX_VALUE) {
        post.setRequestEntity(new InputStreamRequestEntity(
            new FileInputStream(fichier), fichier.length()));
      } else {
        post.setRequestEntity(new InputStreamRequestEntity(
            new FileInputStream(fichier)));
        post.setContentChunked(true);
      }

      post.setRequestHeader("Content-type", "text/xml; charset=UTF-8");

      // Executer le request
      this.httpClient = genererURL();
      resultat = httpClient.executeMethod(post);
    } catch (Exception e) {
      traiterException("Erreur envoyerFichierXML", e);
    } finally {
      if (post != null) {
        post.releaseConnection();
      }
    }

    return resultat;
  }

  /**
   * Cette méthode permet d'envoyer un fichier sur le serveur HTTP
   * <p>
   *
   * @param fichier
   *            Le fichier à envoyer
   * @return Code de réponse "HTTP POST method"
   */
  public int envoyerFichier(File fichier) {
    int resultat = 0;
    PostMethod post = null;

    try {
      Part p = new FilePart(fichier.getName(), fichier);
      RequestEntity entite = new MultipartRequestEntity(new Part[] { p },
          new HttpMethodParams());
      post = new PostMethod(this.getUrlPath());
      post.setRequestEntity(entite);
      httpClient = genererURL();
      httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
      resultat = httpClient.executeMethod(post);
    } catch (Exception e) {
      traiterException("Erreur envoyerFichier", e);
    } finally {
      if (post != null) {
        post.releaseConnection();
      }
    }

    return resultat;
  }

  /**
   * Méthode qui libère la connexion Http.
   * <p>
   */
  public void libererUrl() {
    appelUrl.releaseConnection();
  }

  public void getContenuFichier(String nomFichier, String repertoireEcriture) {
    this.setUrlPath(getUrlPath() + nomFichier);

    try {
      this.appelUrl = new GetMethod(getUrlPath());
      this.httpClient = genererURL();
      this.httpClient.executeMethod(appelUrl);

      InputStream inputStream = appelUrl.getResponseBodyAsStream();

      // Ecrire le fichier dans le fichier spécifier
      String fichierAvecChemin = repertoireEcriture + nomFichier;
      OutputStream outputStream = new FileOutputStream(fichierAvecChemin);
      byte[] buffer = new byte[8192];

      for (int bytesRead = 0;
          (bytesRead = inputStream.read(buffer, 0, 8192)) != -1;) {
        outputStream.write(buffer, 0, bytesRead);
      }

      inputStream.close();
      outputStream.close();
    } catch (Exception e) {
      traiterException("Erreur getContenuFichier", e);
    } finally {
      if (this.appelUrl != null) {
        this.appelUrl.releaseConnection();
      }
    }
  }

  /**
   * Cette méthode permet d'écrire sur disque le contenu d'une réponse obtenue
   * suite à une requête
   *
   * @param repertoireEcriture
   *            Le chemin  déposer le fichier contenant les données de la
   *            réponse
   * @param nomFichier
   *            Le nom du fichier qui contiendra les données de la réponse
   */
  public void getContenuUrl(String repertoireEcriture, String nomFichier) {
    try {
      this.httpClient = genererURL();
      this.appelUrl = new GetMethod(getUrlPath());
      httpClient.executeMethod(appelUrl);

      InputStream inputStream = this.appelUrl.getResponseBodyAsStream();

      // Ecrire le fichier dans le fichier spécifier
      String fichierAvecChemin = repertoireEcriture + nomFichier;
      OutputStream outputStream = new FileOutputStream(fichierAvecChemin);
      int bytesRead = 0;
      byte[] buffer = new byte[8192];

      while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
        outputStream.write(buffer, 0, bytesRead);
      }

      inputStream.close();
      outputStream.close();
    } catch (IOException e) {
      traiterException("Erreur getContenuUrl", e);
    } finally {
      if (this.appelUrl != null) {
        this.appelUrl.releaseConnection();
      }
    }
  }

  /**
   * Cette méthode permet d'obtenir le contenu d'une réponse obtenue suite à
   * une requête
   *
   * @return suite binaire correspondant au contenu situé � l'adresse affectée
   *         à l'objet utilitaire.
   */
  public byte[] getContenuUrl() {
    byte[] contenu = null;

    try {
      this.appelUrl = new GetMethod(getUrlPath());
      this.httpClient = genererURL();
      this.httpClient.getHttpConnectionManager().getParams()
      .setConnectionTimeout(200000);
      this.httpClient.getHttpConnectionManager().getParams().setSoTimeout(200000);

      int code = this.httpClient.executeMethod(appelUrl);

      if (code == HttpStatus.SC_OK) {
        InputStream in = appelUrl.getResponseBodyAsStream();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];

        for (int taille = 0; (taille = in.read(buffer)) != -1;) {
          out.write(buffer, 0, taille);
        }

        contenu = out.toByteArray();
      } else {
        String message = "Erreur lors de l'appel HTTP, code de statut : " +
            code;

        if (log.isErrorEnabled()) {
          log.error(message + ", Contenu HTML : \n" +
              appelUrl.getResponseBodyAsString());
        }

        throw new SOFIException(message);
      }
    } catch (Exception e) {
      traiterException("Erreur getContenuUrl", e);
    } finally {
      if (this.appelUrl != null) {
        this.appelUrl.releaseConnection();
      }
    }

    return contenu;
  }

  /**
   * Cette méthode permet d'obtenir le contenu d'une réponse obtenue suite à
   * une requête
   *
   * @return suite binaire correspondant au contenu situé a l'adresse affectée
   *         à l'objet utilitaire.
   * @since 3.1
   */
  public static byte[] getContenuUrl(String url) {
    
    enleverVerificationCertificat();
    
    byte[] contenu = null;

    GetMethod get = null;

    try {

      HttpClient client = new HttpClient();
      
      client.getHttpConnectionManager().getParams().setConnectionTimeout(200000);
      client.getHttpConnectionManager().getParams().setSoTimeout(200000);
      get = new GetMethod(url);
      
      if (url.indexOf("@") != -1) {
        String utilisateur = StringUtils.substringBetween(url, "//", ":");
        String motPasse = StringUtils.substringBetween(url, utilisateur + ":", "@");
        if (StringUtils.isNotBlank(utilisateur) && StringUtils.isNotBlank(motPasse)) {
          Credentials defaultcreds = new UsernamePasswordCredentials(utilisateur, motPasse);
          client.getState().setCredentials(AuthScope.ANY, defaultcreds);
        }
      }
      
      get.setFollowRedirects(true);
      int code = client.executeMethod(get);

      if (code == HttpStatus.SC_OK) {
        InputStream in = get.getResponseBodyAsStream();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];

        for (int taille = 0; (taille = in.read(buffer)) != -1;) {
          out.write(buffer, 0, taille);
        }

        contenu = out.toByteArray();
      } else {
        String message = "Erreur lors de l'appel HTTP, code de statut : " +
            code;

        if (log.isInfoEnabled()) {
          log.info(message + ", Contenu HTML : \n" +
              get.getResponseBodyAsString());
        }

        throw new SOFIException(message);
      }
    } catch (Exception e) {
      traiterException("Erreur getContenuUrl", e);
    } finally {
      if (get != null) {
        get.releaseConnection();
      }
    }

    return contenu;
  }

  /**
   *
   * @return Retourne le nom du domaine se trouvant dans la requête
   */
  public String getUrlDomaine() {
    return urlDomaine;
  }

  /**
   * Cette méthode fixe un nouveau nom de domaine
   * <p>
   *
   * @param newUrlDomaine
   *            Le nouveau nom de domaine
   */
  public void setUrlDomaine(String newUrlDomaine) {
    urlDomaine = newUrlDomaine;
  }

  /**
   *
   * @return représentation textuelle de l'URL de la requête
   */
  public String getUrlPath() {
    return urlPath;
  }

  /**
   * Cette méthode fixe le chemin URL d'une requête HTTP
   * <p>
   *
   * @param newUrlPath
   *            Le chemin URL de la requête
   */
  public void setUrlPath(String newUrlPath) {
    urlPath = newUrlPath;
  }

  /**
   * Cette méthode retourne le port du serveur HTTP
   * <p>
   *
   * @return Le port du serveur HTTP
   */
  public int getUrlPort() {
    return urlPort;
  }

  /**
   * Cette méthode fixe le port d'un serveur HTTP
   * <p>
   *
   * @param newUrlPort
   *            Le nouveau port du serveur HTTP
   */
  public void setUrlPort(int newUrlPort) {
    urlPort = newUrlPort;
  }

  /**
   * Cette méthode précise si c'est une requête SSL
   * <p>
   *
   * @return true si SSL, false sinon
   */
  public boolean getIsSecure() {
    return isSecure;
  }

  /**
   * Cette methode spécifie si c'est une requête SSL
   * <p>
   *
   * @param newIsSecure
   *            true si SSL, false sinon
   */
  public void setIsSecure(boolean newIsSecure) {
    isSecure = newIsSecure;
  }

  /**
   * Cette méthode permet de connaître le type de connetion
   * <p>
   *
   * @return le type de connection (HTTPS si SSL, HTTP sinon)
   */
  public String getTypeConnexion() {
    if (isSecure) {
      return "https";
    } else {
      return "http";
    }
  }

  public static void enleverVerificationCertificat() {
    try {
      TrustManager[] trustAllCerts = new TrustManager[] { new TrustManager() };
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new SecureRandom());

      Protocol p = new Protocol("https",
          new ProtocolSocketFactory(sc.getSocketFactory()), 443);
      Protocol.registerProtocol("https", p);
    } catch (Exception e) {
      throw new SOFIException(e);
    }
  }

  /**
   * Retourne un contenu html.
   *
   * @return le contenu html.
   * @param url
   *            l'url dont on désire le contenu.
   */
  public static String getContenuHtml(String url) {
    return getContenuHtml(url, null);
  }

  /**
   * Obtenir la réponse HTML d'un url.
   * @param url URL
   * @param request Requête dont lequel
   * on désire reconduire l'identifiant de session.
   * Sert à faire plusieurs requêtes successives
   * avec la même session.
   * 
   * @return
   */
  public static String getContenuHtml(String url, HttpServletRequest request) {
    enleverVerificationCertificat();

    String html = null;
    GetMethod get = null;

    try {
      // Creer une instance d'HttpClient.
      HttpClient client = new HttpClient();

      if (request != null) {
        Cookie[] cookies = getHttpClientCookies(request.getCookies());
        String host = request.getServerName();
        String path = request.getContextPath();

        for (int i = 0; i < cookies.length; i++) {
          // Si on est localhost on doit laisser le domaine à null
          cookies[i].setDomain(host);
          cookies[i].setPath(path);
        }

        client.getState().addCookies(cookies);
      }

      // Créer un méthode Get.
      get = new GetMethod(url);

      if (url.indexOf("@") != -1) {
        String utilisateur = StringUtils.substringBetween(url, "//", ":");
        String motPasse = StringUtils.substringBetween(url, utilisateur + ":", "@");
        if (StringUtils.isNotBlank(utilisateur) && StringUtils.isNotBlank(motPasse)) {
          Credentials defaultcreds = new UsernamePasswordCredentials(utilisateur, motPasse);
          client.getState().setCredentials(AuthScope.ANY, defaultcreds);
        }
      }

      // Appel http.
      int statusCode = client.executeMethod(get);
      // Lire le réponse.
      html = get.getResponseBodyAsString();

      if (statusCode != HttpStatus.SC_OK) {
        if (html.indexOf("SOFIException") != -1) {
          html = html.substring(html.indexOf(
              "<PRE>org.sofiframework.exception.SOFIException:") + 45);
          html = html.substring(0, html.indexOf("<br>"));    
        }
        html = "URL in error : " + url + " " + html;
        throw new SOFIException(html);
      }
    } catch (Exception e) {
      traiterException("Erreur getContenuHtml", e);
    } finally {
      if (get != null) {
        get.releaseConnection();
      }
    }
    return html;
  }

  /**
   * Convertir une liste de cookies javax.servlet en cookies org.apache.commons.httpclient.
   */
  public static Cookie[] getHttpClientCookies(javax.servlet.http.Cookie[] cookies) {
    int numberOfCookies = 0;
    if(cookies!= null){
      numberOfCookies = cookies.length;
    }

    Cookie[] httpClientCookies = new Cookie[numberOfCookies];

    for (int i = 0; i < numberOfCookies; i++) {
      javax.servlet.http.Cookie c = cookies[i];
      String domain = c.getDomain();
      String name = c.getName();
      String value = c.getValue();
      String path = c.getPath();
      boolean secure = c.getSecure();
      int maxAge = c.getMaxAge();
      Cookie hCookie = new Cookie(domain, name, value, path, maxAge, secure);
      httpClientCookies[i] = hCookie;
    }
    return httpClientCookies;
  }

  public int getCodeStatusHttpReponse() {
    return codeStatusHttpReponse;
  }

  /**
   * Cette méthode permet d'exécuter une requête POST
   * <p>
   *
   * @param donneesPost
   *            les données à envoyer en POST note : chaque parametre doit
   *            être encodé par URLEncoder URLEncoder.encode(parametre,
   *            "UTF-8")
   *
   * @return le contenu de la resource web retournée par le serveur en réponse
   *         à la requête POST
   */
  public byte[] envoyerRequeteAvecPost(String donneesPost) {
    byte[] contenu = null;

    ByteArrayInputStream in = new ByteArrayInputStream(donneesPost.getBytes());

    if (donneesPost != null) {
      contenu = this.executeInputStreamPostMethod(in,
          "application/x-www-form-urlencoded", false);
    }

    return contenu;
  }

  private byte[] executeInputStreamPostMethod(InputStream in,
      String contentType, boolean followRedirect) {
    byte[] contenu = null;
    PostMethod post = null;

    try {
      // Préparer le post
      post = new PostMethod(getUrlPath());
      post.setFollowRedirects(followRedirect);

      if (in.available() < Integer.MAX_VALUE) {
        post.setRequestEntity(new InputStreamRequestEntity(in, in.available()));
      } else {
        post.setRequestEntity(new InputStreamRequestEntity(in));
        post.setContentChunked(true);
      }

      if (contentType != null) {
        post.setRequestHeader("Content-type",
            "application/x-www-form-urlencoded");
      }

      httpClient = genererURL();

      // Executer la request
      codeStatusHttpReponse = httpClient.executeMethod(post);

      if (codeStatusHttpReponse == HttpStatus.SC_OK) {
        contenu = lireContenu(post.getResponseBodyAsStream());
      } else {
        throw new SOFIException("Erreur Http " + codeStatusHttpReponse + " " +
            post.getStatusText());
      }
    } catch (Exception e) {
      traiterException("Erreur executeInputStreamPostMethod", e);
    } finally {
      if (post != null) {
        post.releaseConnection();
      }
    }

    return contenu;
  }

  private static byte[] lireContenu(InputStream in) throws IOException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    byte[] buffer = new byte[4096];

    for (int taille = 0; (taille = in.read(buffer)) != -1;) {
      out.write(buffer, 0, taille);
    }

    return out.toByteArray();
  }

  private static void traiterException(String message, Exception e) {
    if (e instanceof RuntimeException) {
      throw (RuntimeException) e;
    } else {
      throw new SOFIException(e);
    }
  }

  /**
   * Accepte tous les certificats.
   */
  private static class TrustManager implements X509TrustManager {
    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] certs, String authType) {
    }

    @Override
    public void checkServerTrusted(X509Certificate[] certs, String authType) {
    }
  }

  /**
   * Factory pour les socket de communication https.
   * Avertissement : Les paramètres http ne sont pas considérés.
   */
  private static class ProtocolSocketFactory
  implements org.apache.commons.httpclient.protocol.ProtocolSocketFactory {
    private SSLSocketFactory factory;

    public ProtocolSocketFactory(SSLSocketFactory factory) {
      this.factory = factory;
    }

    @Override
    public Socket createSocket(String host, int port)
        throws IOException, UnknownHostException {
      return factory.createSocket(host, port);
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress clientHost,
        int clientPort) throws IOException, UnknownHostException {
      return factory.createSocket(host, port, clientHost, clientPort);
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress clientHost,
        int clientPort, HttpConnectionParams params)
            throws IOException, UnknownHostException, ConnectTimeoutException {
      return factory.createSocket(host, port, clientHost, clientPort);
    }
  }
}
