/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.oro.text.perl.Perl5Util;
import org.sofiframework.utilitaire.exception.ExpressionReguliereFormatInvalideException;


/**
 * Utilitaire permettant de traiter des expressions régulières.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0.1
 */
public class UtilitaireExpressionReguliere {
  /**
   * Retourne une valeur formatté selon un masque et une ou des expressions
   * régulières valide.
   * @param valeur
   * @param format
   * @param expressionReguiere
   */
  public static String getValeurFormatte(String expressionReguiere,
      String format, String valeur)
          throws ExpressionReguliereFormatInvalideException {
    boolean formatValide = false;
    String expressionReguliere = null;

    // La liste des expressions valides.
    StringTokenizer iterateur = new StringTokenizer(expressionReguiere, ",");

    while (iterateur.hasMoreTokens() && !formatValide) {
      expressionReguliere = iterateur.nextToken().trim();
      formatValide = valideExpressionReguliere(valeur, expressionReguliere);
    }

    String formatResultat = "";

    if (formatValide) {
      if (!UtilitaireString.isVide(format)) {
        formatResultat = traiterExpressionReguliere(expressionReguliere,
            format, valeur);
      } else {
        formatResultat = valeur;
      }
    } else {
      throw new ExpressionReguliereFormatInvalideException(
          "Format invalide pour valeur");
    }

    return formatResultat;
  }

  /**
   * Valide que la valeur est valide selon l'expression régulière demandé.
   * @return true si la valeur est valide selon l'expression régulière demandé.
   * @param expressionReguliere l'expression régulière à utiliser pour valider
   * @param valeur la valeur a valider.
   */
  public static boolean valideExpressionReguliere(String valeur,
      String expressionReguliere) {
    if ((expressionReguliere == null) || (expressionReguliere.length() <= 0)) {
      return false;
    }

    Perl5Util matcher = new Perl5Util();

    return matcher.match("/" + expressionReguliere + "/", valeur);
  }

  /**
   *
   * @return
   * @param valeurSaisie
   * @param expressionReguliere
   */
  public static String traiterExpressionReguliere(String expressionReguliere,
      String format, String valeurSaisie) {
    Pattern patternBd = Pattern.compile(expressionReguliere);

    String retour = patternBd.matcher(valeurSaisie).replaceFirst(format);

    return retour;
  }
}
