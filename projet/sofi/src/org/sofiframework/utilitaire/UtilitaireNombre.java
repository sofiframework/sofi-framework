/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.sofiframework.exception.SOFIException;


/**
 * Utilitaire pour les nombres tel que les valeurs monétaires par exemple.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.7
 * @since SOFI 1.8 Le support du format de pourcentage est supporté. Plusieurs
 * méthode utilitaire pour extraire des Float, Double, Long, BigDecimal selon
 * un nombre formaté en String.
 */
public class UtilitaireNombre {
  /**
   * Retourne un nombre formatté selon le format et le locale spécifié.
   * @return un nombre formatté selon le format et le locale spécifié.
   * @param valeurMonetaire appliquer le format en valeur monétaire.
   * @param affichage appliquer le format d'affichage.
   * @param localeUtilisateur le locale de l'utilisateur.
   * @param valeur la valeur a traiter
   * @param format le format a appliquer.
   * @deprecated utiliser maintenant getValeurFormate(String format, Object valeur,
    Locale localeUtilisateur, boolean affichageSeulement). Avec cette méthode,
    vous pouvez spécifier en plus du format monétaire, le format en pourcentage(9%)
   */
  @Deprecated
  public static String getValeurFormate(String format, Object valeur,
      Locale localeUtilisateur, boolean affichage, boolean valeurMonetaire) {
    String decimalFormat = "";

    if ((valeur != null) && String.class.isInstance(valeur)) {
      valeur = new Double(valeur.toString());
    }

    int nbDecimal = 0;

    if (format.lastIndexOf(".") != -1) {
      String decimal = format.substring(format.lastIndexOf("."));
      nbDecimal = decimal.length() - 1;

      if (format.indexOf("$") != -1) {
        nbDecimal--;
      }
    }

    if ((Number.class.isInstance(valeur) ||
        BigDecimal.class.isInstance(valeur)) && (format != null)) {
      DecimalFormat form;

      if (affichage && valeurMonetaire) {
        form = (DecimalFormat) NumberFormat.getCurrencyInstance(localeUtilisateur);
      } else {
        form = (DecimalFormat) NumberFormat.getNumberInstance(localeUtilisateur);
      }

      form.setMaximumFractionDigits(nbDecimal);
      form.setMinimumFractionDigits(nbDecimal);

      decimalFormat = form.format(valeur);
    }

    if (affichage) {
      return decimalFormat;
    }

    return getNombreConformeAvecBlanc(decimalFormat);
  }

  /**
   * Formater un nombre selon le format spécifié et le locale de l'utilisateur.
   * Permet de support des format en nombre, monétaire (9.99$), et en pourcentage (1.00%).
   * @return un nombre formaté.
   * @param affichageSeulement appliquer l'affichage seulement.
   * @param localeUtilisateur le locale de l'utilisateur.
   * @param valeur la valeur a formater.
   * @param format le format a appliquer.
   */
  public static String getValeurFormate(String format, Object valeur,
      Locale localeUtilisateur, boolean affichageSeulement) {
    boolean formatSpecifique = false;

    String decimalFormat = "";

    if ((valeur != null) && String.class.isInstance(valeur)) {
      valeur = UtilitaireNombre.getDouble(valeur.toString(), localeUtilisateur);
    }

    int nbDecimal = 0;

    // Valider si le format doit appliquer les valeurs monétaire.
    if ((format.indexOf("$") != -1) || (format.indexOf("%") != -1)) {
      formatSpecifique = true;
    }

    if (format.lastIndexOf(".") != -1) {
      String decimal = format.substring(format.lastIndexOf("."));
      nbDecimal = decimal.length() - 1;

      if (formatSpecifique) {
        nbDecimal--;
      }
    }

    if ((Number.class.isInstance(valeur) ||
        BigDecimal.class.isInstance(valeur)) && (format != null)) {
      DecimalFormat form = null;

      if (affichageSeulement && formatSpecifique) {
        if (format.indexOf("$") != -1) {
          form = (DecimalFormat) NumberFormat.getCurrencyInstance(localeUtilisateur);
        }

        if (format.indexOf("%") != -1) {
          form = (DecimalFormat) NumberFormat.getPercentInstance(localeUtilisateur);
        }
      } else {
        form = (DecimalFormat) NumberFormat.getNumberInstance(localeUtilisateur);
      }

      form.setMaximumFractionDigits(nbDecimal);
      form.setMinimumFractionDigits(nbDecimal);

      decimalFormat = form.format(valeur);
    }

    if (formatSpecifique) {
      return decimalFormat;
    }

    return getNombreConformeAvecBlanc(decimalFormat);
  }

  /**
   * Retourne un nombre en type Integer.
   * @return un nombre de type Integer
   * @param nombre un nombre en String.
   * @param locale le locale de l'utilisateur.
   */
  public static Integer getInteger(String nombre, Locale locale) {
    if (nombre == null) {
      nombre = "0";
    }

    nombre = standardiserNombre(nombre, locale);

    try {
      return Integer.valueOf(nombre);
    } catch (Exception e) {
      return Integer.valueOf("0");
    }
  }

  /**
   * Retourne un nombre en type Long.
   * @return un nombre en type Long.
   * @param nombre le nombre en String.
   */
  public static Long getLong(String nombre, Locale locale) {
    if (nombre == null) {
      nombre = "0";
    }

    nombre = standardiserNombre(nombre, locale);

    try {
      return Long.valueOf(nombre);
    } catch (Exception e) {
      return Long.valueOf("0");
    }
  }

  /**
   * Cette méthode permet de convertir un nombre formaté selon un Locale
   * spécifique à un nombre standard Java
   *
   * Dernière méthode faîte dans SOFI par Jean-Francois Legendre (2006.10.18)
   *
   * @return un nombre standardisé Java
   * @param locale la locale du nombre texte passé en parametre
   * @param nombre texte à standardiser
   */
  public static String standardiserNombre(String nombre, Locale locale) {

    if (locale == null) {
      throw new SOFIException("La locale ne doit pas être null. Si vous " +
          "utilisez un formatter vous devez utiliser la " +
          "méthode qui prend une locale.");
    }

    DecimalFormat nf = (DecimalFormat) NumberFormat.getNumberInstance(locale);
    DecimalFormatSymbols dfs = nf.getDecimalFormatSymbols();

    nombre = UtilitaireString.remplacerTous(nombre, "$", "");
    nombre = UtilitaireString.remplacerTous(nombre, "%", "");
    nombre = UtilitaireString.remplacerTous(nombre, "(", "-");
    nombre = UtilitaireString.remplacerTous(nombre, ")", "");

    Character separateurDecimal = new Character(dfs.getDecimalSeparator());
    Character separateurGroupe = new Character(dfs.getGroupingSeparator());

    if (separateurGroupe.toString().indexOf(160) != -1) {
      nombre = UtilitaireString.remplacerTous(nombre, " ", "");
      nombre = UtilitaireString.remplacerTous(nombre,
          new Character((char) 160).toString(), "");
    } else {
      nombre = UtilitaireString.remplacerTous(nombre,
          separateurGroupe.toString(), "");
    }

    nombre = UtilitaireString.remplacerTous(nombre,
        separateurDecimal.toString(), ".");

    return nombre;
  }

  public static void main(String[] args) {
    // xxx,xxx.xx => xxxxxx.xx
    System.out.println(UtilitaireNombre.standardiserNombre("456,456,456.036",
        new Locale("en", "CA")));

    // xxx xxx,xx => xxxxxx.xx
    System.out.println(UtilitaireNombre.standardiserNombre("456 456 456,036",
        new Locale("fr", "CA")));
  }

  /**
   * Retourne un nombre en type Float.
   * @return un nombre en type Float.
   * @param nombre le nombre en String.
   * @param locale le Locale du nombre à convertir en BigDecimal.
   */
  public static Float getFloat(String nombre, Locale locale) {
    try {
      return Float.valueOf(getBigDecimal(nombre, locale).toString());
    } catch (Exception e) {
      return Float.valueOf("0");
    }
  }

  /**
   * Retourne un nombre en type Double.
   * @return un nombre en type Double.
   * @param nombre le nombre en String.
   * @param locale le Locale du nombre à convertir en BigDecimal.
   */
  public static Double getDouble(String nombre, Locale locale) {
    try {
      return Double.valueOf(getBigDecimal(nombre, locale).toString());
    } catch (Exception e) {
      return Double.valueOf("0");
    }
  }

  /**
   * Retourne un nombre en type BigDecimal.
   * @return un nombre en type BigDecimal.
   * @param nombre le nombre en String.
   * @param locale le Locale du nombre à convertir en BigDecimal.
   */
  public static BigDecimal getBigDecimal(String nombre, Locale locale) {
    boolean pourcentage = false;
    int nbPositionPourcentage = 4;

    try {
      if ((nombre != null) && (nombre.indexOf("%") != -1)) {
        pourcentage = true;
        nbPositionPourcentage = nombre.length() - 1;
      }

      if (nombre == null) {
        nombre = "0";
      }

      nombre = standardiserNombre(nombre, locale);

      BigDecimal resultat = new BigDecimal(nombre);

      if (pourcentage) {
        resultat = resultat.divide(new BigDecimal(100d), nbPositionPourcentage,
            BigDecimal.ROUND_HALF_EVEN);
      }

      return resultat;
    } catch (Exception e) {
      return new BigDecimal("0");
    }
  }

  static private String getNombreConformeAvecBlanc(String nombreATraiter) {
    char[] tableau = nombreATraiter.toCharArray();
    StringBuffer montantFormatte = new StringBuffer();

    for (int i = 0; i < tableau.length; i++) {
      switch (tableau[i]) {
      case '1':
        montantFormatte.append("1");

        break;

      case '2':
        montantFormatte.append("2");

        break;

      case '3':
        montantFormatte.append("3");

        break;

      case '4':
        montantFormatte.append("4");

        break;

      case '5':
        montantFormatte.append("5");

        break;

      case '6':
        montantFormatte.append("6");

        break;

      case '7':
        montantFormatte.append("7");

        break;

      case '8':
        montantFormatte.append("8");

        break;

      case '9':
        montantFormatte.append("9");

        break;

      case '0':
        montantFormatte.append("0");

        break;

      case '.':
        montantFormatte.append(".");

        break;

      case '-':
        montantFormatte.append("-");

        break;

      case ',':
        montantFormatte.append(",");

        break;

      case '(':
        montantFormatte.append("(");

        break;

      case ')':
        montantFormatte.append(")");

        break;

      default:
        montantFormatte.append(" ");

        break;
      }
    }

    return montantFormatte.toString();
  }

  /* public static void main(String[] args) {
     BigDecimal valeur = UtilitaireNombre.getBigDecimal("0,50");
     System.out.println(valeur);
     System.out.println(UtilitaireNombre.getValeurFormate("9%", valeur,
         Locale.US, true));
   }*/
}
