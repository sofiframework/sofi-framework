/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Permet de détecter les tentatives d'inclusion de script
 * dans les chaînes de caractères.
 * 
 * @author j-m.pelletier
 */
public class UtilitaireXss {

  /**
   * Caractères qui sont parfaois utilisés pour briser les
   * chaînes et empècher la détection des scripts malicieux.
   */
  private static final char[] caracteres = new char[]{
    (char) 92,
    (char) 32,
    (char) 31,
    (char) 30,
    (char) 29,
    (char) 28,
    (char) 13,
    (char) 10,
    (char) 9,
    (char) 0
  };

  /**
   * Balise CDATA qui permet les caractères spéciaux dans le XML.
   */
  private static final String[] cdata = new String[]{
    "<![CDATA[", "]]>"
  };

  /**
   * Balises de commentaires XML/HTML
   */
  private static final String[] commentaireJs = new String[]{
    "<!--", "-->"
  };

  /**
   * Balises d'ouverture de scripts malicieux.
   */
  private static final String[] chaines = new String[]{
    "<script",
    "javascript:",
    "<iframe",
    "vbscript:",
    "mocha:",
    "livescript:",
    ":expression(",
    "<object",
    "<embed"
  };

  public UtilitaireXss() {
    super();
  }

  /**
   * Valider une chaîne pour savoir si elle inclus des tentatives XSS.
   * @param valeur Chaîne à valider.
   * @return vrai (contient cette chaîne), faux (ne la contient pas)
   */
  public static boolean validerInclusionXss(String valeur) {
    boolean valide = true;
    if (valeur != null) {
      valeur = StringEscapeUtils.unescapeXml(valeur);
      valeur = enleverChaines(valeur, cdata);
      valeur = enleverChaines(valeur, commentaireJs);
      valeur = enleverCaracteres(valeur);
      valeur = valeur.toLowerCase();
      valide = !isSequencePresente(valeur);
    }
    return valide;
  }

  private static String enleverCaracteres(String valeur) {
    for (int i = 0; i < caracteres.length; i++)  {
      valeur = StringUtils.remove(valeur, caracteres[i]);
    }
    return valeur;
  }

  private static final String enleverChaines(String valeur, String[] chaines) {
    for (int i = 0; i < chaines.length; i++)  {
      valeur = StringUtils.remove(valeur, chaines[i]);
    }
    return valeur;
  }

  private static boolean isSequencePresente(String valeur) {
    boolean present = false;
    for (int i = 0; i < chaines.length && !present; i++)  {
      present = valeur.indexOf(chaines[i]) != -1;
    }
    return present;
  }
}
