/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.xml.sax;

/**
 * Erreur survenu lors du parsing d'un document
 * Xml avec un parser SAX.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ValidationSaxException extends SaxException {

  private static final long serialVersionUID = -7450518158598727467L;

  private ValidationSax resultatValidation;

  /**
   * Constructeur par défaut
   */
  public ValidationSaxException() {
    super();
  }

  /**
   * Constructeur qui prend  une validationSax.
   * 
   * @param resultat résultat du parsing d'un document.
   */
  public ValidationSaxException(ValidationSax resultat) {
    super();
    this.resultatValidation = resultat;
  }

  /**
   * Obtenir le résultat de la validation qui
   * à provoqué l'erreur.
   * 
   * @return validationSax
   */
  public ValidationSax getResultatValidation() {
    return this.resultatValidation;
  }
}
