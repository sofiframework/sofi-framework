/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.xml.sax.xerces;

import java.io.IOException;

import org.apache.xerces.parsers.SAXParser;
import org.sofiframework.xml.sax.SaxException;
import org.sofiframework.xml.sax.SaxParser;
import org.sofiframework.xml.sax.ValidationSax;
import org.sofiframework.xml.sax.ValidationSaxException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XercesSaxParserImpl implements SaxParser {

  private InputSource xml;

  private String pathXsd;

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.xml.sax.SaxParser#valider()
   */
  @Override
  public void valider() throws ValidationSaxException, SaxException, IOException {
    // Faire le SAXParser qui se chargera de parser le XML avec le XSD
    try {
      SAXParser parser = new SAXParser();
      parser.setFeature("http://xml.org/sax/features/validation", true);
      parser.setFeature("http://apache.org/xml/features/validation/schema", true);
      parser.setProperty("http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation", this.pathXsd);

      ValidationSax validation = new ValidationSax();
      parser.setErrorHandler(validation);

      // Analyser le fichier XML avec le XSD
      parser.parse(xml);

      if (validation.isErreur()) {
        throw new ValidationSaxException(validation);
      }
    } catch (SAXException e) {
      throw new SaxException(e);
    }
  }

  public String getPathXsd() {
    return pathXsd;
  }

  public void setPathXsd(String pathXsd) {
    this.pathXsd = pathXsd;
  }

  public InputSource getXml() {
    return xml;
  }

  public void setXml(InputSource xml) {
    this.xml = xml;
  }
}
