/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.xml.sax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Résultat du "parsing" SAX d'un fichier Xml.
 * Les erreurs de parsing sont regroupées en trois catégories.
 * 
 *  - Erreurs
 *  - Erreurs fatales
 *  - Avertissements
 * 
 * Il est possible de récupérer les erreurs et avertissements
 * à l'aide des méthodes getListeErreur, getListeErreurFatale
 * et getListeAvertissement.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ValidationSax implements ErrorHandler {

  /*
   * Collection de SAXParseException pour les validations de niveau "error".
   */
  private List listeErreur = new ArrayList();

  /*
   * Collection de SAXParseException pour les validations de niveau
   * "fatalError".
   */
  private List listeErreurFatale = new ArrayList();

  /*
   * Collection de SAXParseException pour les validations de niveau "warning".
   */
  private List listeAvertissement = new ArrayList();

  /*
   * (non-Javadoc)
   * 
   * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
   */
  @Override
  public void error(SAXParseException exception) throws SAXException {
    this.listeErreur.add(exception);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
   */
  @Override
  public void fatalError(SAXParseException exception) throws SAXException {
    this.listeErreurFatale.add(exception);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
   */
  @Override
  public void warning(SAXParseException exception) throws SAXException {
    this.listeAvertissement.add(exception);
  }

  /**
   * Si la validation a trouvé au moin
   * une erreur (fatale ou non).
   * 
   * @return true en erreur, false sinon
   */
  public boolean isErreur() {
    return !this.listeErreur.isEmpty() || isErreurFatale();
  }

  /**
   * Si la validation a trouvé au moin
   * une erreur fatale.
   * 
   * @return true en erreur fatale, false sinon
   */
  public boolean isErreurFatale() {
    return !this.listeErreurFatale.isEmpty();
  }

  /**
   * Si la validation a trouvé au moin
   * un avertissement.
   * 
   * @return true avertissement, false sinon
   */
  public boolean isAvertissement() {
    return !this.listeAvertissement.isEmpty();
  }

  /**
   * Obtenir la liste complète des erreurs. Erreur + Erreurs fatales
   * @return liste d'exceptions SAX¾ParseException
   */
  public List getListeCompleteErreur() {
    List listeComplete = new ArrayList();
    listeComplete.addAll(this.listeErreur);
    listeComplete.addAll(this.listeErreurFatale);
    return listeComplete;
  }

  public List getListeAvertissement() {
    return listeAvertissement;
  }

  public void setListeAvertissement(List listeAvertissement) {
    this.listeAvertissement = listeAvertissement;
  }

  public List getListeErreur() {
    return listeErreur;
  }

  public void setListeErreur(List listeErreur) {
    this.listeErreur = listeErreur;
  }

  public List getListeErreurFatale() {
    return listeErreurFatale;
  }

  public void setListeErreurFatale(List listeErreurFatale) {
    this.listeErreurFatale = listeErreurFatale;
  }
}
