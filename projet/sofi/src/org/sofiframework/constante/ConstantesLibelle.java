/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;


/**
 * Classe de constantes utilisé pour signifier des clés de libellé.
 * <p>
 * @author Gino Boucher (Nurun inc.)
 * @version SOFI 1.3
 */
public final class ConstantesLibelle {
  /**
   * Le libellé par défaut pour spécifier le type de résultat de la liste de navigation.
   */
  public static final String INFORMATION_RESULTAT_LISTE_TYPE = "libelle.liste.type.resultat";

  /**
   * La clé du libellé pour spécifier le nombre de page dans la liste de résultat.
   */
  public static final String INFORMATION_RESULTAT_NB_PAGE = "libelle.liste.nb.page";

  /**
   * La clé du libellé pour spécifier l'abréviation d'un kilo-octet.
   */
  public static final String ABREVIATION_KILO_OCTET = "sofi.libelle.commun.abreviation_kilo_octet";
}
