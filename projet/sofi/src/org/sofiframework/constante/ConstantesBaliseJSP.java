/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;


/**
 * Classe de constantes utilisé pour signifier des clés de libellé.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.5
 */
public final class ConstantesBaliseJSP {
  /**
   * Indicateur que la balise de formulaire n'a aucune propriété de spécifier.
   */
  public static final String PROPRIETE_VIDE = "proprieteVide";

  /**
   * Entete de la liste de navigation par défaut si spécifié.
   */
  public static final String ENTETE_LISTE_NAVIGATION_DEFAUT = "enteteListeNavigationDefaut";

  /**
   * Entete du bas de la liste de navigation par défaut si spécifié.
   */
  public static final String ENTETE_BAS_LISTE_NAVIGATION_DEFAUT = "enteteBasListeNavigationDefaut";

  /**
   * Variable qui loge la liste des boutons a acvité lorsque le formulaire est modifié
   */
  public static final String LISTE_BOUTON_A_ACTIVE = "listeBoutonAActive";

  /**
   * Variable qui loge la liste des boutons a inactivé lorsque le formulaire est modifié
   */
  public static final String LISTE_BOUTON_A_INACTIVE = "listeBoutonAInactive";

  /**
   * Le nom de la fonction qui permet d'activer des boutons
   */
  public static final String NOM_FONCTION_ACTIVER_BOUTONS = "activerLesBoutons";

  /**
   * Le nom de la fonction qui permet d'inactiver des boutons
   */
  public static final String NOM_FONCTION_INACTIVER_BOUTONS = "inactiverLesBoutons";

  /**
   * La nom de variable qui loge le ou les fonctions onload personnalisé à appliquer.
   */
  public static final String FONCTION_ONLOAD_PERSONALISE = "fonctionOnloadPersonnalise";

  /**
   * La nom de variable qui loge le ou les fonctions onload personnalisé à appliquer.
   */
  public static final String FONCTION_ONSUBMIT_PERSONALISE = "fonctionOnSubmitPersonnalise";

  /**
   * La nom de variable qui loge le ou les fonctions onclick des boutons submit personnalisé à appliquer.
   */
  public static final String FONCTION_ONCLICK_PERSONALISE = "fonctionOnClickPersonnalise";

  /**
   * Parametre qui initialise tous les paramètres de la liste de navigation.
   */
  public static final String INITIALISER_PARAMETRES_LISTE_NAVIGATION = "sofi_initialiser_liste";

  /**
   * Parametre qui initialise tous les paramètres de la liste de navigation.
   */
  public static final String AJAX_ECLATABLE_FERMETURE_NON_AUTOMATIQUE = "sofi_eclatable_fermeture_non_automatique";

  /**
   * Paramètre qui spécifie un traitement ajax sur un Div.
   */
  public static final String PARAM_AJAX_DIV = "ajaxDiv";

  /**
   * Variable qui loge le code appelé dans le cas que la confimation des
   * modifications du formulaire à été activé
   */
  public static final String CODE_CONFIRMER_MODIFICATION_FORMULAIRE = "if (!confirmerModificationFormulaire(event)) return false;";

  /**
   * Le nom de la fonction qui permet d'application du traitement lors de la modification du fomulaire.
   */
  public static final String NOM_FONCTION_TRAITEMENT_MODIFICATION_FORMULAIRE = "traitementModificationFormulaire";

  /**
   * Le nom de la variable qui loge l'url qui doit être appelé par l'évènement onchange du champs de saisie.
   */
  public static final String LISTE_VALEURS_URL_ONCHANGE = "listeValeursUrlOnchange";

  /**
   * Le nom de la variable qui loge l'url qui doit être appelé par l'évènement onkeyup du champs de saisie.
   */
  public static final String LISTE_VALEURS_URL_ONKEYUP = "listeValeursUrlOnkeyup";

  /**
   * Le nom de la variable qui loge la correspondance entre les param id et property des listes de valeurs.
   */
  public static final String LISTE_VALEURS_CORRESPONDANCE_PROPERTY_ID = "listeValeursCorrespondancePropertyId";

  /**
   *Le nom de la variable qui loge la fonction JS a traiter apres modification de formulaire.
   */
  public static final String FONCTION_JS_APRES_MODIFICATION_FORMULAIRE = "fonctionJSApresModificationFormulaire";

  /**
   * Le nom de la variable qui loge le code javascript a écrire à la fin de la page.
   */
  public static final String FONCTION_JS_ACCUMULE_BAS_PAGE = "fonctionJSAccumuleBasPage";

}
