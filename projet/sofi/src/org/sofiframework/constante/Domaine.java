/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class Domaine {

  /**
   * Les enums qui sont des valeurs de domaines devrait implémenter cette interface.
   */
  protected interface ValeurDomaine {
    public String getValeur();

    @Override
    public String toString();
  }

  public static enum CodeStatut implements ValeurDomaine {
    ACTIF("A"), INACTIF("I");

    private String valeur;

    CodeStatut(String valeur) {
      this.valeur = valeur;
    }

    @Override
    public String getValeur() {
      return valeur;
    }
  }
}
