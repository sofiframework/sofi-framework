/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;


/**
 * Classe de constantes utilisé pour les paramètres système.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public final class ConstantesParametreSysteme {
  /**
   *  Spécifie la valeur qui correspond au oui.
   */
  public static final String OUI = "oui";

  /**
   * Spécifie la valeur qui correspond au non.
   */
  public static final String NON = "non";

  /**
   * Spécifie le nombre maximal de lignes dans une liste.
   */
  public static final String MAX_NB_LISTE = "nbListeParPage";

  /**
   * Spécifie si on désire que le formulaire gère seulement les champs modifiés
   * pour le traitement de la logique d'affaires.
   * <p>
   * Valeur possible: true,false
   */
  public static final String TRAITER_CHAMP_MODIFIE_SEULEMENT = "traiterChampModifieSeulement";

  /**
   * Spécifie la clé du paramètre système qui attribue le paramètre du rapport qui indique
   * le code utilisateur.
   */
  public static final String PARAMETRE_RAPPORT_CODE_UTILISATEUR = "paramRapportCodeUtilisateur";

  /**
   * Emplacement du fichier de configuration des services EJB
   */
  public static final String FICHIER_CONFIG_SERVICE_EJB = "fichierConfigurationServiceEJB";

  /**
   * Le nom de classe qui joue le rôle du modèle, doit inclure les packages qui y sont associé
   */
  public static final String NOM_CLASSE_MODELE = "nomClasseModele";

  /**
   * Le nom des actions qui ne sont pas sécurisé, si plusieurs, séparé de virgule.
   */
  public static final String ACTION_NON_SECURISE = "actionNonSecurise";

  /**
   * L'url qui permet de faire l'authentification de l'utilisateur.
   */
  public static final String URL_AUTHENTIFICATION = "urlAuthentification";

  /**
   * Le code de valeur qui correspond à l'url qui permet de faire l'authentification de l'utilisateur.
   */
  public static final String CODE_VALEUR_URL_AUTHENTIFICATION = "codeValeurUrlAuthentification";

  /**
   * Spécifie si l'application est grand public, donc il n'y aura aucune sécurité sur un utilisateur.
   */
  public static final String APPLICATION_GRAND_PUBLIC = "applicationGrandPublic";

  /**
   * Spécifie le ou les méthodes qui doivent ignorer les erreurs de validations tel que les champs obligatoires, de cohérences et de contextes.
   */
  public static final String FORMULAIRE_METHODE_IGNORER_VALIDATION = "formulaireMethodeIgnorerValidation";

  /**
   * Spécifie le format d'affichage des numéros de téléphone.
   */
  public static final String FORMAT_AFFICHAGE_TELEPHONE = "formatAffichageTelephone";

  /**
   * Spécifie le format de la base de données des numéros de téléphone.
   */
  public static final String FORMAT_BD_TELEPHONE = "formatBdTelephone";

  /**
   * Constante qui spécifie la fonction javascript a appelé pour lancer le menu multi menu horizontal.
   */
  public final static String FONCTION_AFFICHAGE_MULTIMENU_HORIZONTAL = "fonctionAffichageMenuHorizontal";

  /**
   * Constante qui spécifie si l'application doit traiter la journalisation.
   */
  public final static String TRAITER_JOURNALISATION = "traiter_journalisation";

  /**
   * Constante qui spécifie le code utilisateur test.
   */
  public final static String CODE_UTILISATEUR_TEST = "codeUtilisateurTest";

  /**
   * Constante qui spécifie si les formulaires doivent traiter seulement les attributs qui ont été affiché à l'utilisateur..
   */
  public final static String TRAITER_ATTRIBUTS_AFFICHE_SEULEMENT = "traiterAttributsAfficheSeulement";

  /**
   * Constante qui spécifie l'action qui traite la barre de statut ajax.
   */
  public final static String ACTION_BARRE_STATUT_AJAX = "actionBarreStatutAjax";

  /**
   * Constante qui spécifie le div id de la barre de statut ajax.
   */
  public final static String DIV_ID_BARRE_STATUT = "divIdBarreStatut";

  /**
   * Le code du paramètre système qui spécifie l'url d'authentification
   */
  public final static String AUTHENTIFICATION_URL = "authentificationUrl";

  /**
   * Le code du paramètre système qui spécifie si l'authentification est SSL ou pas.
   */
  public final static String AUTHENTIFICATION_SECURE = "authentificationSecure";

  /**
   * Le code du paramètre système qui spécifie le serveur d'authentification
   */
  public final static String AUTHENTIFICATION_SERVEUR = "authentificationServeur";

  /**
   * Le code du paramètre système qui spécifie le domaine dont le cookie d'authentification sera accessible.
   */
  public final static String AUTHENTIFICATION_DOMAINE_COOKIE = "authentificationDomaineCookie";

  /**
   * Le code du paramètre système qui spécifie le nom du cookie d'authentification accessible.
   */
  public final static String AUTHENTIFICATION_NOM_COOKIE = "authentificationNomCookie";

  /**
   * La durée de vie du cookie de l'authentification SOFI. Pour que le cookie ait
   * la durée de vie de la session, utiliser la valeur -1.
   * @since SOFI 2.0.2
   */
  public final static String AUTHENTIFICATION_AGE_COOKIE = "authentificationAgeCookie";
  
  public final static String AUTHENTIFICATION_SECURE_COOKIE = "authentificationSecureCookie";  

  /**
   * Le code du paramètre système qui spécifie le nom du paramètre de l'url retour.
   * @since SOFI 2.0.2
   */
  public final static String AUTHENTIFICATION_NOM_PARAM_URL_RETOUR = "authentificationNomParamUrlRetour";

  /**
   * Spécifie si on doit ignorer la fermeture de session avec indicateur en paramètre.
   */
  public final static String AUTHENTIFICATION_IGNORER_FERMETURE_SESSION = "authentificationIgnorerFermetureSession";

  /**
   * Spécifie si on désire l'authentification unique lorsque vous n'avez pas d'écran d'authentification qui le génère.
   */
  public final static String AUTHENTIFICATION_UNIQUE = "authentificationUnique";

  /**
   * Constante qui spécifie si l'authentification doit se faire seulement via la page d'accueil.
   */
  public final static String AUTHENTIFICATION_VIA_PAGE_ACCUEIL_SEULEMENT = "authentificationViaPageAccueilSeulement";

  /**
   * Le code du paramètre système qui spécifie s'il y a un action qui permet de traiter la fermeture de session.
   */
  public final static String TRAITER_ACTION_QUITTER = "traiterActionQuitter";

  /**
   * Spécifie si vous désirez de l'aide contextuelle sur les libellés spécifiés en erreur.
   * Le défaut est false.
   */
  public final static String AIDE_CONTEXTUELLE_SUR_LIBELLE_ERREUR = "aideContextuelleSurLibelleErreur";

  /**
   * Spécifie la date de déploiement, utiliser afin de gérer la cache des CSS et des JavaScripts du navigateur du client.
   */
  public final static String DATE_DEPLOIEMENT = "dateDeploiement";

  /**
   * Spécifie la clé du message d'avertissement lorsque le formulaire a été modifié et que l'utilisateur
   * sélectionne un élément de menu.
   */
  public final static String CLE_MESSAGE_AVERTISSEMENT_FORMULAIRE_MODIFIE = "cleMessageAvertisementFormulaireModifie";

  /**
   * Spécifie l'url de base de l'aide contextuelle externe
   */
  public final static String AIDE_CONTEXTUELLE_BASE_URL = "aideContextuelleBaseUrl";

  /**
   * Spécifie l'extension de la page de l'aide contextuelle.
   */
  public final static String AIDE_CONTEXTUELLE_PAGE_EXTENSION = "aideContextuellePageExtension";

  /**
   * Spécifie si l'aide contextuelle doit supporter le multilingue.
   */
  public final static String AIDE_CONTEXTUELLE_MULTILINGUE = "aideContextuelleMultiLingue";

  /**
   * Spécifie si on désire toujours afficher une icone d'aide lorsque aide contextuelle.
   * @since SOFI 2.0.3
   */
  public final static String AIDE_CONTEXTUELLE_ICONE_AIDE = "aideContextuelleIconeAide";

  /**
   * Spécifie si on désire toujours afficher une icone d'aide lorsque aide contextuelle à
   * gauche du libellé.
   * @since SOFI 2.1
   */
  public final static String AIDE_CONTEXTUELLE_ICONE_AIDE_A_GAUCHE = "aideContextuelleIconeAideAGauche";

  /**
   * Spécifie si on désire ignorer de prendre l'aide contextuelle associer à l'objet enfant du référentiel
   * si existant.
   * Par défaut, il ne sera pas ignorer.
   * @since SOFI 2.1
   */
  public static final String AIDE_CONTEXTUELLE_IGNORER_AIDE_ENFANT = "aideContextuelleIgnorerAideEnfant";


  /**
   * Spécifie si on désire par défaut mettre l'optimiseur l'itération page par page à false.
   */
  public final static String OPTIMISATION_ITERATEUR_PAGE_PAR_PAGE = "optimisationIterateurPageParPage";

  /**
   * Spécifie si on désire appliquer le mode debug ajax.
   */
  public final static String DEBUG_AJAX = "debugAjax";

  /**
   * Spécifie si on désire désactivé la sélection d'un service dans le menu multi-niveau
   */
  public final static String MULTIMENU_DESACTIVE_SELECTION_SERVICE = "multiMenuDesactiveSelectionService";

  /**
   * Spécifie le ou les fonctions que l'on désire ajouter sur le onload en tout temps.
   */
  public final static String FONCTION_JAVASCRIPT_ONLOAD = "fonctionJavaScriptOnLoad";

  /**
   * Spécifie la clé du message d'attente lors d'un chargement d'une fenêtre dhtml ajax.
   */
  public final static String CLE_MESSAGE_ATTENTE_CHARGEMENT_FENETRE = "cleMessageAttenteChargementFenetre";

  /**
   * Spécifie si on désire activé l'aide globale de l'aplication via la page d'accueil
   */
  public final static String AIDE_APPLICATION_GLOBALE = "aideApplicationGlobale";

  /**
   * Spécifie la structure de l'url externe.
   */
  public final static String AIDE_STRUCTURE__EXTERNE = "aideStructureUrlExterne";

  /**
   * Spécifie la structure de l'url des contenus dynamiques
   */
  public final static String CONTENU_DYNAMIQUE_STRUCTURE__EXTERNE = "contenuDynamiqueStructureUrlExterne";
  
  /**
   * Spécifie la structure de l'url des contenus dynamiques
   */
  public final static String CONTENU_DYNAMIQUE_CACHE_EXPIRATION = "contenuDynamiqueCacheExpiration";

  /**
   * Spécifie le code l'application commun pour une organisation.
   */
  public final static String CODE_APPLICATION_COMMUN = "codeApplicationCommun";

  /**
   * Spécifie si on doit faire distinction du mode readonly et disabled dans la champ de formulaire.
   */
  public final static String READONLY_DISABLED_DISTINCT = "readonlyDisabledDistinct";

  /**
   * Spécifie s'il existe une notion de code utilisateur applicatif qui est différent du code utilisateur utilisé par la sécurité.
   */
  public final static String CODE_UTILISATEUR_APPLICATIF = "codeUtilisateurApplicatif";

  /**
   * Spécifie le code de langue que la journalisation doit appliquer.
   */
  public final static String CODE_LANGUE_JOURNALISATION = "codeLangueJournalisation";

  /**
   * Spécifie si on doit utiliser JSP 1.x.
   */
  public final static String TRAITER_JSP_VERSION_1X = "traiterJSPVersion1x";

  /**
   * Spécifie le séparateur HTML entre chacun des fichiers attachés.
   */
  public final static String FICHIER_ATTACHE_SEPARATEUR_ENTRE_FICHIER = "fichierAttacheSeparateurEntreFichier";

  /**
   * Spécifie le décorateur HTML entre chacun des fichiers attachés.
   * @since SOFI 2.0.4
   */
  public final static String FICHIER_ATTACHE_DECORATEUR_ENTRE_FICHIER = "fichierAttacheDecorateurEntreFichier";

  /**
   * Spécifie si on désire un focus automatique sur le premier champ de saisie sur les formulaires.
   */
  public final static String FORMULAIRE_FOCUS_AUTOMATIQUE = "formulaireFocusAutomatique";

  /**
   * Spécifie si on désire que les paramètres en GET sont injecté dans les paramètres des onglets.
   */
  public final static String INJECTION_PARAMETRE_ONGLET = "injectionParametreOnglet";

  /**
   * Spécifie si on désire s'assurer que l'ordre des onglets sont ajusté avec son
   * parent lié.
   */
  public final static String AJUSTER_ONGLET_AVEC_PARENT = "ajusterOngletAvecParent";

  /**
   * Spécifie la clé du message général pour tous les formulaires de l'application.
   * @since SOFI 2.0.2
   */
  public final static String CLE_MESSAGE_ERREUR_GENERAL = "cleMessageErreurGeneral";

  /**
   * Spécifie la clé du message informatif qui spécifie qu'il y a eu des modifications dans le formulaire de l'application.
   * @since SOFI 2.0.2
   */
  public final static String CLE_MESSAGE_INFORMATION_MODIFICATION_FORMULAIRE = "cleMessageInformationModificationFormulaire";

  /**
   * Spécifie la clé du message informatif qui spécifie qu'il n'y a pas eu de modification depuis la dernière transaction dans le formulaire de l'application.
   * @since SOFI 2.0.2
   */
  public final static String CLE_MESSAGE_INFORMATION_AUCUNE_MODIFICATION_FORMULAIRE = "cleMessageInformationAucuneModificationFormulaire";

  /**
   * Spécifie la clé du message qui fourni le code html à utiliser pour les champs obligatoires.
   * @since SOFI 2.0.4
   */
  public final static String HTML_CHAMP_OBLIGATOIRE = "htmlChampObligatoire";

  /**
   * Permet de cacher les composants SOFI qui possèdent un attribut href lorsque le href n'est pas dans la liste
   * des composants dont l'utilisateur à accès. Par exemple, cacher les hyperliens qui mènent à des unités de
   * traitements dont l'utilisateur n'a pas accès.
   * 
   * @since SOFI 2.0.4
   */
  public final static String CACHER_HREF_SECURISE = "cacherHrefSecurise";

  /**
   * 
   * @since SOFI 2.0.4
   */
  public static final String SEPARATEUR_METHODE_ACTION = "separateurMethodeAction";

  /**
   * Permet de configurer l'ordre de tri utilisé pour les valeurs nulls lors
   * des requêtes avec un accès de données ADF. Lors d'un tri ascendant.
   * <code>
   *   <parametre>
   *    <nom>ordreTriAscendantPourNull</nom>
   *    <valeur>Nulls last</valeur>
   *  </parametre>
   * </code>
   * @since SOFI 2.0.4
   */
  public static final String ORDRE_TRI_ASCENDANT_POUR_NULL = "ordreTriAscendantPourNull";

  /**
   * Permet de configurer l'ordre de tri utilisé pour les valeurs nulls lors
   * des requêtes avec un accès de données ADF. Lors d'un tri descendant.
   * <code>
   *   <parametre>
   *    <nom>ordreTriDescendantPourNull</nom>
   *    <valeur>Nulls first</valeur>
   *  </parametre>
   * </code>
   * @since SOFI 2.0.4
   */
  public static final String ORDRE_TRI_DESCENDANT_POUR_NULL = "ordreTriDescendantPourNull";

  /**
   * Permet de traiter la navigation complète dans le fil d'ariane. La navigation inférieure
   * au service seront donc inclut dans le fil d'ariane.
   * <code>
   *   <parametre>
   *    <nom>traiterNavigationCompleteDansFilAriane</nom>
   *    <valeur>true</valeur>
   *  </parametre>
   * </code>
   * @since SOFI 2.1
   */
  public static final String TRAITER_NAVIGATION_COMPLETE_DANS_FIL_ARIANE = "traiterNavigationCompleteDansFilAriane";


  /**
   * Permet de spécifier si on désire par défaut ou pas le lancement d'une nouvelle recherche sur
   * le clic de l'icone de la loupe dans les listes de valeurs.
   * 
   * Si le paramètre n'est pas spécifié alors par défaut ce sera true.
   * <code>
   *   <parametre>
   *    <nom>listeValeurIconeLoupeNouvelleRecherche</nom>
   *    <valeur>false</valeur>
   *  </parametre>
   * </code>
   * @since SOFI 2.1
   */
  public static final String LISTE_VALEUR_ICONE_LOUPE_NOUVELLE_RECHERCHE = "listeValeurIconeLoupeNouvelleRecherche";

  /**
   * Classe CSS par défaut de l'image utilisée comme icone pour les champs sofi-html:date.
   */
  public static final String CSS_IMAGE_CHAMP_DATE = "cssImageChampDate";

  /**
   * Image pour la loupe du composant liste de valeur.
   */
  public static final String CSS_IMAGE_LISTE_VALEUR = "cssImageListeValeur";

  /**
   * Langue par défaut du système.
   */
  public static final String LANGUE_PAR_DEFAUT = "langueParDefaut";

  /**
   * Liste des codes de langues supportées dans le système exemple FR,EN
   * Consulter le domaine de valeur de langues pour connaître les valeurs possibles.
   */
  public static final String LISTE_LANGUE_SUPPORTEE = "listeLangueSupportee";

  /**
   * Le format d'encodage a utiliser par défaut pour les lecteures et écritures Xml.
   */
  public static final String FORMAT_ENCODAGE_XML = "formatEncodageXml";


  /**
   * Normalisation de l'utilisation du nom de paramètre pour spécifier la méthode d'une action.
   * @since SOFI 3.0.2
   */
  public static final String METHODE_PARAM = "methodeParam";

  /**
   * Normalisation de l'utilisation de l'extension à utilisation lorsqu'on utilise un nom d'action et méthode pour désigner un URL.
   * @since SOFI 3.0.2
   */
  public static final String EXTENSION_ACTION_URL = "extensionActionUrl";
 
  /**
   * Permettre de spécifier le nom du paramètre à utiliser pour spécifier le nombre maximum par page d'une liste de navigation.
   * @since SOFI 3.2
   */
  public static final String PARAM_NOMBRE_MAX_PAR_PAGE = "paramNombreMaxParPage";
  
  
  /**
   * Spécifier 'true' si vous désirez ignorer les redirects SSL lorsque l'application est configuré pour fonctionner par défaut en SSL.
   */
  public static final String SSL_IGNORER_REDIRECT = "sslIgnorerRedirect";

}
