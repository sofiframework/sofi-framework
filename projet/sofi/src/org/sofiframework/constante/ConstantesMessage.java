/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;


/**
 * Classe de constantes utilisé pour signifier des clés de message.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public final class ConstantesMessage {
  /**
   * Message pour afficher le nombre d'erreur dans la barre de statut.
   */
  public static final String ERREUR_BARRE_STATUT_NB_ERREUR = "erreur.commun.barre.statut.compte";

  /**
   * Message avertisement d'une modification de formulaire
   */
  public static final String AVERTISSEMENT_MODIFICATION_FORMULAIRE = "avertissement.modification.formulaire";

  /**
   * Message d'information que l'opération a été traité avec succès.
   */
  public static final String INFORMATION_OPERATION_EFFECTUE_SUCCES = "operation.succes";

  /**
   * Message d'information qui indique qu'aucune modification n'a été faite à la transaction.
   */
  public static final String INFORMATION_OPERATION_TRANSACTION_NON_MODIFIE = "operation.aucune.modification.transaction";

  /**
   * Message d'information que l'opération de la génération du rapport a été traité avec succès.
   */
  public static final String INFORMATION_OPERATION_RAPPORT_SUCCES = "operation.rapport.succes";

  /**
   * Message d'erreur que l'opération de la génération du rapport a échouée.
   */
  public static final String ERREUR_OPERATION_RAPPORT_ECHEC = "operation.rapport.echec";

  /**
   * Message d'erreur pour un champ obligatoire.
   */
  public static final String ERREUR_CHAMP_OBLIGATOIRE = "erreur.champ.obligatoire";
}
