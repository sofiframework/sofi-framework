/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;


/**
 * Classe de constantes utilisé de façon commune.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public final class Constantes {
  /**
   * Code de la valeur oui.
   */
  public static final String OUI = "O";

  /**
   * Code de la valeur non.
   */
  public static final String NON = "N";

  /**
   * Nombre maximal de lignes dans une liste.
   */
  public static final int MAX_NB_LISTE = 20;

  /**
   * Nombre maximal de lignes dans une liste courte.
   */
  public static final int MAX_NB_COURTE_LISTE = 10;

  /**
   * Clé utilisée pour conserver la référence au modèle dans la session.
   */
  public static final String MODELE = "modele";

  /**
   * Clé d'un message pouvant être affiché dans une barre de statut.
   */
  public static final String MESSAGE_BARRE_STATUT = "messageBarreStatut";

  /**
   * Clé utilisée pour transmettre un message d'erreur.
   */
  public static final String MESSAGE_ERREUR = "messageErreur";

  /**
   * Clé du premier message d'erreur concervé en
   * attribut à la requête après l'exécution d'une action ou la
   * validation d'un formulaire.
   */
  public static final String PREMIER_MESSAGE_ERREUR = "premierMessageErreur";

  /**
   * Clé d'un message informatif placé en attribut à la requête.
   */
  public static final String MESSAGE_INFORMATIF = "messageInformatif";

  /**
   * Clé du message utilisé pour identifier le nombre d'éléments d'une liste en affichage.
   */
  public static final String MESSAGE_LISTE_NAVIGATION = "messageListeNavigation";

  /**
   * Identifie si un formulaire est en lecture seulement.
   */
  public static final String LECTURE_SEULEMENT = "lectureSeulement";

  /**
   * Clé pour le nombre d'utilisateur actif.
   */
  public static final String NB_USAGER_ACTIF = "nbUsagerActif";

  /**
   * Clé pour le nombre d'utilisateur total.
   */
  public static final String NB_USAGER_TOTAL = "nbUsagerTotal";

  /**
   * Clé pour le nombre d'utilisateur authentifié.
   */
  public static final String NB_USAGER_AUTHENTIFIER_ACTIF = "nbUsagerAuthentifier";

  /**
   * Clé pour le nombre D'utilisateur authentifié au total.
   */
  public static final String NB_USAGER_AUTHENTIFIER_TOTAL = "nbUsagerAuthentifierTotal";

  /**
   * Clé pour la date de démarrage de l'application.
   */
  public static final String DATE_DEPART_APPLICATION = "dateDepartApplication";

  /**
   * Chemin d'accès au gabarit de courrier électronique.
   */
  public static final String PATH_COURRIEL_TEMPLATE = "pathCourrielTemplate";

  /**
   * Code du paramètre utilisateur.
   */
  public static final String PARAM_UTILISATEUR = "paramUtilisateur";

  /**
   * Code du paramètre de la clé de l'utilisateur.
   */
  public static final String PARAM_CLE_UTILISATEUR = "paramCleUtilisateur";

  /**
   * Si la session doit être fermée.
   */
  public static final String IS_FERMER_SESSION = "isFermerSession";

  /**
   * Clé de la référence au formulaire.
   */
  public static final String FORMULAIRE = "formulaire";

  /**
   * Identifiant d'un navigateur de client est Internet Explorer 5.5.
   */
  public static final int IE_55 = 1;

  /**
   * Identifiant d'un navaigateur client Mozilla.
   */
  public static final int MOZILLA = 2;

  /**
   * Clé du paramètre d'un type de navvigateur du client.
   */
  public static final String TYPE_NAVIGATEUR = "typeNavigateur";

  /**
   * Clé du paramètre la description de navigateur du client.
   */
  public static final String DESC_TYPE_NAVIGATEUR = "descTypeNavigateur";

  /**
   * La clé du nom du service sélectionné.
   */
  public static final String SERVICE_SELECTIONNEE = "serviceSelectionnee";

  /**
   * La clé du nom du service sélectionné parent.
   */
  public static final String SERVICE_SELECTIONNEE_PARENT = "serviceSelectionneeParent";

  /**La clé du nom de l'onglet sélectionné.
   *
   */
  public static final String ONGLET_SELECTIONNEE = "ongletSelectionnee";

  /**
   * Clé du paramèter du certificat d'authentification.
   */
  public static final String CERTIFICAT_AUTHENTIFICATION = "certificatAuthentification";

  /**
   * Clé de la référence à l'utilisateur dans la session.
   */
  public static final String UTILISATEUR = "utilisateur";

  /**
   * Constante qui loge un document PDF.
   * @deprecated Utiliser la constante
   *  DocumentElectronique.DOCUMENT_PDF
   */
  @Deprecated
  public final static String DOCUMENT_PDF = "documentPdf";

  /**
   * Indicateur si on doit traiter une avertissement de modification de formulaire à l'utilisateur.
   */
  public static final String IND_MODIFICATION_FORMULAIRE = "indModificationFormulaire";

  /**
   * Identifiant qui loge la liste des attributs qui ont une validation pour le formulaire.
   */
  public static final String LISTE_ATTRIBUT_VALIDATION_FORMULAIRE = "listeAttributsValidationFormulaire";

  /**
   * Identifiant qui loge un indicateur spécifiant une nouvelle authentification.
   */
  public static final String NOUVELLE_AUTHENTIFICATION = "nouvelleAuthentification";

  /**
   * Identifiant qui loge la feuillle de style de l'utilisateur.
   */
  public static final String FEUILLE_STYLE_UTILISATEUR = "feuilleStyleUtilisateur";

  /**
   * Identifiant de l'adresse de l'action sélectionné.
   */
  public static final String ADRESSE_ACTION_SELECTIONNE = "adresseActionSelectionne";

  /**
   * Identifiant de l'adresse de l'action du formulaire.
   */
  public static final String ADRESSE_ACTION_FORMULAIRE = "adresseActionFormulaire";

  /**
   * Constante qui permet de spécifier si l'on veut mettre l'indicateur à droit
   * ou à gauche du champ
   */
  public static final String POSITION_INDICATEUR_OBLIGATOIRE_DROITE = "positionIndicateurObligatoireDroite";

  /**
   * Constante qui permet de spécifier le certificat de l'utilisateur
   */
  public static final String CERTIFICAT = "certificat";

  /**
   * Constante qui permet de spécifier si l'on veut afficher le message d'erreur
   * de saisie en bas du champ
   */
  public static final String POSITION_MESSAGE_ERREUR_SAISIE = "messageErreurSaisieBas";

  /**
   * Constante qui permet de traiter si le formulaire a déjà été initialiser lors du traitement.
   */
  public static final String INITIALISATION_FORMULAIRE_TRAITE = "initialisationFormulaireTraite";

  /**
   * Constante qui permet de spécifier le nom du dernier formulaire affiché.
   */
  public static final String NOM_DERNIER_FORMULAIRE = "nomDernierFormulaire";

  /**
   * Constante qui loge les attributs qui sont placé dans la session temporairement pour une action précise.
   */
  public final static String ATTRIBUTS_TEMP_SESSION_ACTION = "attributsTempSessionAction";

  /**
   * Répertoire des composants d'interface a exclure.
   */
  public final static String REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE = "repertoireComposantInterfaceExclure";

  /**
   * Répertoire des composants d'interface a exclure général.
   */
  public final static String REPERTOIRE_COMPOSANT_INTERFACE_A_EXCLURE_GENERAL = "repertoireComposantInterfaceExclureGeneral";

  /**
   * Répertoire des composants d'interface a ajouter .
   */
  public final static String REPERTOIRE_COMPOSANT_INTERFACE_A_AJOUTER = "repertoireComposantInterfaceAAjouter";

  /**
   * Répertoire des composants d'interface ajouté .
   */
  public final static String REPERTOIRE_COMPOSANT_INTERFACE_AJOUTE = "repertoireComposantInterfaceAjoute";

  /**
   * Constantes qui loge l'url de retour initialement demandé par l'utilisateur.
   */
  public final static String URL_RETOUR_SESSION_UTILISATEUR = "urlRetourSessionUtilisateur";

  /**
   * Constante qui loge les attributs qui sont placé dans la session temporairement pour un service.
   */
  public final static String ATTRIBUTS_TEMP_SESSION_SERVICE = "attributsTempSessionService";

  /**
   * Constante qui loge les attributs en traitement durant la requête en cours.
   */
  public final static String ATTRIBUTS_TEMP_EN_TRAITEMENT = "SOFIattributsTemporaireEnTraitement";

  /**
   * Constante qui spécifie si le type d'objet du référentiel sélectionné est un service.
   */
  public final static String TYPE_SERVICE_SELECTIONNE = "typeServiceSelectionne";

  /**
   * Constante qui spécifie le code utilisateur de la session.
   */
  public final static String CODE_UTILISATEUR_SESSION = "codeUtilisateurSession";

  /**
   * Constante qui spécifie l'IP de l'utilisateur
   */
  public final static String UTILISATEUR_IP = "utilisateurIP";

  /**
   * Constante qui spécifie qu'il y a une affichage ajax en cours.
   */
  public final static String AFFICHAGE_AJAX = "affichageAjax";

  /**
   * Identifiant de l'adresse de l'action sélectionné.
   */
  public static final String ADRESSE_ACTION_SELECTIONNE_AVANT_REDIRECT = "SOFIadresseActionSelectionneAvantRedirect";

  /**
   * Identifiant de l'adresse de l'action afficher.
   */
  public static final String ADRESSE_ACTION_AFFICHE = "SOFIadresseActionAffiche";

  /**
   * Identifiant de l'adresse de l'action de l'ancienne action affiché à l'utilisateur
   */
  public static final String ADRESSE_ACTION_RETOUR = "SOFIadresseActionRetour";

  /**
   * Identifiant du formulaire en cours.
   */
  public static final String FORMULAIRE_EN_COURS = "formulaireEnCours";

  /**
   * Identifiant qui loge le dernier HREF avec paramètres sélectionné.
   */
  public static final String DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE = "SOFIDernierHrefAvecParametresSelectionne";

  /**
   * Identifiant qui loge l'avant dernier HREF avec paramètres sélectionné.
   */
  public static final String AVANT_DERNIER_HREF_AVEC_PARAMETRES_SELECTIONNE = "SOFIAvantDernierHrefAvecParametresSelectionne";

  /**
   * Identifiant qui loge le code de langue en cours.
   */
  public static final String CODE_LANGUE_EN_COURS = "codeLangueEnCours";

  /**
   * Le nombre d'utilisateur présentement en ligne sur le site.
   */
  public static final String NB_UTILISATEUR_EN_LIGNE = "nbUtilisateurEnLigne";

  /**
   * Le locale de l'utilisateur sélectionné ou selon user-agent.
   */
  public static final String LOCALE_JSTL = "javax.servlet.jsp.jstl.fmt.locale.session";
}
