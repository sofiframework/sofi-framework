/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.constante;

import java.sql.Types;


/**
 * Classe de constantes utilisé pour les types SQL pour associer avec JDBC.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.2
 */
public final class ConstantesSQL {
  /**
   * Le type SQL NUMBER
   */
  public static int NUMBER = Types.NUMERIC;

  /**
   * Le type DATE.
   */
  public static int DATE = Types.DATE;

  /**
   * Le type VARCHAR2
   */
  public static int VARCHAR2 = Types.VARCHAR;
}
