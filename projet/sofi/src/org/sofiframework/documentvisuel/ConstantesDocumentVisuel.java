/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;


/**
 * Classe de constantes utilisées pour la génération de documents visuels.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public final class ConstantesDocumentVisuel {
  /** Constante pour l'alignement à gauche d'un paragraphe */
  public static final int ALIGNEMENT_GAUCHE = 1;

  /** Constante pour l'alignement à droite d'un paragraphe */
  public static final int ALIGNEMENT_DROITE = 2;

  /** Constante pour l'alignement au centre d'un paragraphe */
  public static final int ALIGNEMENT_CENTRE = 3;

  /** Constante pour l'alignement justifié d'un paragraphe */
  public static final int ALIGNEMENT_JUSTIFIE = 4;

  /** Constante pour l'alignement vertical en haut d'un paragraphe */
  public static final int ALIGNEMENT_VERTICAL_HAUT = 5;

  /** Constante pour l'alignement vertical en bas d'un paragraphe */
  public static final int ALIGNEMENT_VERTICAL_BAS = 6;

  /** Constante pour l'alignement vertical au centre d'un paragraphe */
  public static final int ALIGNEMENT_VERTICAL_MILIEU = 7;

  /** Constante pour la police Courrier */
  public static final int POLICE_COURRIER = 1;

  /** Constante pour la police Times New Roman */
  public static final int POLICE_TIMES_NEW_ROMAN = 2;

  /** Constante pour la police Helvetica */
  public static final int POLICE_HELVETICA = 3;

  /** Constante pour la couleur noire */
  public static final Couleur COULEUR_NOIR = new Couleur(0, 0, 0);

  /** Constante pour la couleur rouge */
  public static final Couleur COULEUR_ROUGE = new Couleur(255, 0, 0);

  /** Constante pour la couleur verte */
  public static final Couleur COULEUR_VERT = new Couleur(0, 255, 0);

  /** Constante pour la couleur bleue */
  public static final Couleur COULEUR_BLEU = new Couleur(0, 0, 255);

  /** Constante pour la couleur blanche */
  public static final Couleur COULEUR_BLANC = new Couleur(255, 255, 255);

  /** Constante pour le format de papier lettre (8.5 x 11) */
  public static final int FORMAT_LETTRE = 1;

  /** Constante pour le format de papier légal (8.5 x 14) */
  public static final int FORMAT_LEGAL = 2;
}
