/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;

import java.util.ArrayList;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe du premier niveau représentant un document visuel.
 * <p>
 * Cette classe contient une liste d'objets de type Page ainsi que les attributs pour les marges du document.
 * Par défaut, les marges sont fixées à 36 points.
 * <p>
 * Grâce a un convertisseur, un document visuel peut être généré dans plusieurs formats de sortie.
 * Dans la version 2.0, le convertisseur en format PDF est fourni (UtilitairePDF). Il est possible
 * d'implémenter d'autres convertisseurs de façon personnelle en attendant que d'autres soient fournis, tel
 * que le format Word.
 *
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class DocumentVisuel extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = 2944133120507564376L;

  /** La liste des pages du document */
  private ArrayList listePages;

  /** La valeur en points de la marge du bas */
  private Float margeBas;

  /** La valeur en points de la marge du haut */
  private Float margeHaut;

  /** La valeur en points de la marge de droite */
  private Float margeDroite;

  /** La valeur en points de la marge de gauche */
  private Float margeGauche;
  private boolean pagination;


  /**
   * Constructeur par défaut fixant une nouvelle liste vide de page et les marges par défaut (36 pt).
   */
  public DocumentVisuel() {
    this.setListePages(new ArrayList());
    this.setMargeBas(new Float(36));
    this.setMargeHaut(new Float(36));
    this.setMargeDroite(new Float(36));
    this.setMargeGauche(new Float(36));
  }

  /**
   * Ajouter une nouvelle page au document en dernière position.
   * @param page la nouvelle page à ajouter
   */
  public void ajouterPage(Page page) {
    this.getListePages().add(page);
  }

  /**
   * Fixer la liste des pages.
   * @param listePages la liste des pages.
   */
  public void setListePages(ArrayList listePages) {
    this.listePages = listePages;
  }

  /**
   * Obtenir la liste des pages.
   * @return la liste des pages.
   */
  public ArrayList getListePages() {
    return listePages;
  }

  /**
   * Fixer la marge du bas.
   * @param margeBas la marge du bas.
   */
  public void setMargeBas(Float margeBas) {
    this.margeBas = margeBas;
  }

  /**
   * Obtenir la marge du bas.
   * @return la marge du bas.
   */
  public Float getMargeBas() {
    return margeBas;
  }

  /**
   * Fixer la marge du haut.
   * @param margeHaut la marge du haut.
   */
  public void setMargeHaut(Float margeHaut) {
    this.margeHaut = margeHaut;
  }

  /**
   * Obtenir la marge du haut.
   * @return la marge du haut.
   */
  public Float getMargeHaut() {
    return margeHaut;
  }

  /**
   * Fixer la marge de droite.
   * @param margeDroite la marge de droite.
   */
  public void setMargeDroite(Float margeDroite) {
    this.margeDroite = margeDroite;
  }

  /**
   * Obtenir la marge de droite.
   * @return la marge de droite.
   */
  public Float getMargeDroite() {
    return margeDroite;
  }

  /**
   * Fixer la marge de gauche.
   * @param margeGauche la marge de gauche.
   */
  public void setMargeGauche(Float margeGauche) {
    this.margeGauche = margeGauche;
  }

  /**
   * Obtenir la marge de gauche.
   * @return la marge de gauche.
   */
  public Float getMargeGauche() {
    return margeGauche;
  }


  public void setPagination(boolean pagination) {
    this.pagination = pagination;
  }


  public boolean isPagination() {
    return pagination;
  }

}
