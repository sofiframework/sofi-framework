/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;


/**
 * Classe représentant un paragraphe de texte dans un document.
 * <p>
 * Le paragraphe comprent un texte, un alignement horizontal, une quantité d'indentation à gauche (en pt),
 * une quantité d'espace après (en pt) et une police d'écriture.
 * <p>
 * Par défaut, le paragraphe est justifié, non-indenté, avec 10pt d'espace après et avec la police d'écriture
 * par défaut (Times New Roman).
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Paragraphe extends ElementContenu {
  /**
   * 
   */
  private static final long serialVersionUID = -7012796601470730646L;

  /** Le texte du paragraphe */
  private String texteParagraphe;

  /** L'alignement horizontal */
  private int alignement;

  /** L'indentation à gauche du paragraphe en pt */
  private float indentationGauche;

  /** La quantité d'espace en pt après le paragraphe */
  private float espaceApres;

  /** La police d'écriture */
  private PoliceEcriture policeEcriture;

  /**
   * Le constructeur par défaut avec le texte à blanc. Par défaut, le paragraphe est justifié, non-indenté,
   * avec 10pt d'espace après et avec la police d'écriture par défaut (Times New Roman).
   */
  public Paragraphe() {
    this.setTexteParagraphe("");
    this.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_JUSTIFIE);
    this.setIndentationGauche(0);
    this.setEspaceApres(10);
    this.setPoliceEcriture(new PoliceEcriture());
  }

  /**
   * Le constructeur avec un texte. Par défaut, le paragraphe est justifié, non-indenté, avec 10pt d'espace
   * après et avec la police d'écriture par défaut (Times New Roman).
   *
   * @param texte le texte du paragraphe
   */
  public Paragraphe(String texte) {
    this.setTexteParagraphe(texte);
    this.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_JUSTIFIE);
    this.setIndentationGauche(0);
    this.setEspaceApres(10);
    this.setPoliceEcriture(new PoliceEcriture());
  }

  /**
   * Fixer le texte du paragraphe.
   * @param texteParagraphe le texte du paragraphe
   */
  public void setTexteParagraphe(String texteParagraphe) {
    this.texteParagraphe = texteParagraphe;
  }

  /**
   * Obtenir le texte du paragraphe.
   * @return le texte du paragraphe
   */
  public String getTexteParagraphe() {
    return texteParagraphe;
  }

  /**
   * Fixer l'alignement horizontal du paragraphe.
   * <p>
   * Utiliser les constantes des documents visuels pour fixer l'alignement.
   * @param alignement l'alignement horizontal du paragraphe
   */
  public void setAlignement(int alignement) {
    this.alignement = alignement;
  }

  /**
   * Obtenir l'alignement horizontal du paragraphe.
   * @return l'alignement horizontal du paragraphe
   */
  public int getAlignement() {
    return alignement;
  }

  /**
   * Fixer le nombre de pt d'indentation à gauche du paragraphe.
   * @param indentationGauche le nombre de pt d'indentation à gauche du paragraphe
   */
  public void setIndentationGauche(float indentationGauche) {
    this.indentationGauche = indentationGauche;
  }

  /**
   * Obtenir le nombre de pt d'indentation à gauche du paragraphe.
   * @return le nombre de pt d'indentation à gauche du paragraphe
   */
  public float getIndentationGauche() {
    return indentationGauche;
  }

  /**
   * Fixer l'espace vertical en pt après le paragraphe.
   * @param espaceApres l'espace vertical en pt après le paragraphe
   */
  public void setEspaceApres(float espaceApres) {
    this.espaceApres = espaceApres;
  }

  /**
   * Obtenir l'espace vertical en pt après le paragraphe.
   * @param espaceApres l'espace vertical en pt après le paragraphe
   */
  public float getEspaceApres() {
    return espaceApres;
  }

  /**
   * Fixer la police d'écriture.
   * @param policeEcriture la police d'écriture
   */
  public void setPoliceEcriture(PoliceEcriture policeEcriture) {
    this.policeEcriture = policeEcriture;
  }

  /**
   * Obtenir la police d'écriture.
   * @return la police d'écriture
   */
  public PoliceEcriture getPoliceEcriture() {
    return policeEcriture;
  }
}
