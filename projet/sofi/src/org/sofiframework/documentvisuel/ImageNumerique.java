/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;


/**
 * Élément de contenu représentant une image à afficher dans un document.
 * <p>
 * Le contenu de l'image est représenté par une tableau de byte. On peut également indiquer un
 * alignement grâce aux constantes et indiquer le pourcentage de la taille de l'image à afficher.
 * <p>
 * Par défaut on affiche l'image centrée et à 100% de sa taille.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class ImageNumerique extends ElementContenu {
  /**
   * 
   */
  private static final long serialVersionUID = -9027552453723004130L;

  /** Le contenu de l'image en binaire. */
  private byte[] contenu;

  /** Contstante d'alignement horizontal de l'image */
  private int alignement;

  /** Pourcentage de la taille totale de l'image à afficher */
  private float pourcentageTaille;

  /**
   * Constructeur par défaut avec alignement au centre et 100% de la taille.
   */
  public ImageNumerique() {
    this.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_CENTRE);
    this.setPourcentageTaille(100);
  }

  /**
   * Fixer le contenu binaire de l'image.
   * @param contenu le contenu binaire de l'image
   */
  public void setContenu(byte[] contenu) {
    this.contenu = contenu;
  }

  /**
   * Obtenir le contenu binaire de l'image.
   * @return le contenu binaire de l'image
   */
  public byte[] getContenu() {
    return contenu;
  }

  /**
   * Fixer l'alignement horizontal de l'image.
   * <p>
   * Utiliser les constantes des documents visuels pour définir l'alignement.
   *
   * @param contenu l'alignement horizontal de l'image
   */
  public void setAlignement(int alignement) {
    this.alignement = alignement;
  }

  /**
   * Obtenir l'alignement horizontal de l'image.
   * @return l'alignement horizontal de l'image
   */
  public int getAlignement() {
    return alignement;
  }

  /**
   * Fixer le pourcentage de la taille de l'image à afficher.
   * @param pourcentageTaille le pourcentage de la taille de l'image à afficher
   */
  public void setPourcentageTaille(float pourcentageTaille) {
    this.pourcentageTaille = pourcentageTaille;
  }

  /**
   * Obtenir le pourcentage de la taille de l'image à afficher.
   * @return le pourcentage de la taille de l'image à afficher
   */
  public float getPourcentageTaille() {
    return pourcentageTaille;
  }
}
