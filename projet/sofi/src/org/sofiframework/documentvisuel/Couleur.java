/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe représentant une couleur pour la génération de documents visuels.
 * <p>
 * La classe est représentée par trois attributs pour la quantité de rouge, de bleu et de vert.
 * Il faut inscrire des valeurs entières entre 0 et 255 pour représenter une couleur.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Couleur extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -593688148838836700L;

  /** La valeur de rouge dans la couleur */
  private int rouge;

  /** La valeur de vert dans la couleur */
  private int vert;

  /** La valeur de bleu dans la couleur*/
  private int bleu;

  /** Constructeur par défaut */
  public Couleur() {
  }

  /**
   * Création d'un couleur à partir des trois valeurs.
   *
   * @param bleu la quantité de bleu
   * @param vert la quantité de vert
   * @param rouge la quantité de rouge
   */
  public Couleur(int rouge, int vert, int bleu) {
    this.setRouge(rouge);
    this.setVert(vert);
    this.setBleu(bleu);
  }

  /**
   * Fixer la quantité de rouge.
   * @param rouge la quantité de rouge.
   */
  public void setRouge(int rouge) {
    this.rouge = rouge;
  }

  /**
   * Obtenir la quantité de rouge.
   * @return la quantité de rouge.
   */
  public int getRouge() {
    return rouge;
  }

  /**
   * Fixer la quantité de vert.
   * @param vert la quantité de vert.
   */
  public void setVert(int vert) {
    this.vert = vert;
  }

  /**
   * Obtenir la quantité de vert.
   * @return la quantité de vert.
   */
  public int getVert() {
    return vert;
  }

  /**
   * Fixer la quantité de bleu.
   * @param bleu la quantité de bleu.
   */
  public void setBleu(int bleu) {
    this.bleu = bleu;
  }

  /**
   * Obtenir la quantité de bleu.
   * @return la quantité de bleu.
   */
  public int getBleu() {
    return bleu;
  }
}
