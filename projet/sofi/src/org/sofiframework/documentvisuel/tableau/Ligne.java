/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.tableau;

import java.util.ArrayList;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe représentant une ligne dans un tableau.
 * <p>
 * La classe est représentée simplement par une liste de cellules.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Ligne extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -3106211349346572214L;
  /** la liste des cellules de la ligne */
  private ArrayList listeCellules;

  /** Constructeur par défaut qui fixe une nouvelle liste vide de cellules */
  public Ligne() {
    this.setListeCellules(new ArrayList());
  }

  /**
   * Obtenir la cellule positionnée à la position demandée.
   * @param noColonne le numéro de la colonne dont on veut obtenir la cellule
   * @return la cellule à cette position
   */
  public Cellule getCellule(int noColonne) {
    return (Cellule) this.getListeCellules().get(noColonne - 1);
  }

  /**
   * Obtenir le nombre de colonnes dans la ligne.
   * @return le nombre de colonnes dans la ligne
   */
  public int getNombreColonnes() {
    return this.getListeCellules().size();
  }

  /**
   * Ajouter une cellule dans la ligne.
   * @param cellule la cellule à ajouter
   */
  public void ajouterCellule(Cellule cellule) {
    this.getListeCellules().add(cellule);
  }

  /**
   * Fixer la liste des cellules.
   * @param listeCellules la liste des cellules.
   */
  public void setListeCellules(ArrayList listeCellules) {
    this.listeCellules = listeCellules;
  }

  /**
   * Obtenir la liste des cellules.
   * @return la liste des cellules.
   */
  public ArrayList getListeCellules() {
    return listeCellules;
  }
}
