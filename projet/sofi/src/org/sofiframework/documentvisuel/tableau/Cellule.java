/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.tableau;

import org.sofiframework.documentvisuel.ConstantesDocumentVisuel;
import org.sofiframework.documentvisuel.Paragraphe;
import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe représentant une cellule dans un tableau.
 * <p>
 * La classe est représentée par un paragraphe contenant du texte ou par un tableau imbriqué, par
 * un pourcentage de gris à appliquer, par des alignements horizontaux et verticaux, par une largeur
 * de bordure et par un nombre de colonnes combinées (colspan).
 * <p>
 * Par défaut, le pourcentage de gris est de 0%, l'alignement est centrée horizontalement et en haut verticalement,
 * la largeur de bordure est de 1 et le colspan est à 1.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Cellule extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -2835838565253808656L;

  /** Le paragraphe contenu dans la cellule */
  private Paragraphe paragraphe;

  /** Le tableau imbriqué contenu dans la cellule */
  private Tableau tableau;

  /** Le pourcentage de gris */
  private int pourcentageGris;

  /** L'alignement horizontal */
  private int alignementHorizontal;

  /** L'alignement vertical */
  private int alignementVertical;

  /** La largeur de la bordure */
  private float largeurBordure;

  /** Le nombre de colonnes imbriquées */
  private int colspan = 1;

  /**
   * Le constructeur par défaut qui fixe à 0% de gris, centré horizontalement et en haut verticalement
   * avec une bordure de 1 pt.
   */
  public Cellule() {
    this.setPourcentageGris(0);
    this.setAlignementHorizontal(ConstantesDocumentVisuel.ALIGNEMENT_CENTRE);
    this.setAlignementVertical(ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_HAUT);
    this.setLargeurBordure(1);
  }

  /**
   * Le constructeur avec un paragraphe de contenu avec 0% de gris, centré horizontalement et en haut
   * verticalement avec une bordure de 1 pt.
   * @param paragraphe le paragraphe de contenu
   */
  public Cellule(Paragraphe paragraphe) {
    this();
    this.setParagraphe(paragraphe);
  }

  /**
   * Création d'une cellule à partir d'une autre cellule.
   * <p>
   * Permet rapidement de cloner tous les attributs d'une cellule.
   * @param cellule la cellule de base
   */
  public Cellule(Cellule cellule) {
    this();

    this.paragraphe = new Paragraphe(cellule.getParagraphe().getTexteParagraphe());
    this.pourcentageGris = cellule.getPourcentageGris();
    this.alignementHorizontal = cellule.getAlignementHorizontal();
    this.alignementVertical = cellule.getAlignementVertical();
    this.largeurBordure = cellule.getLargeurBordure();
  }

  /**
   * Création d'une cellule contenant un tableau imbriqué.
   * @param tableau le tableau de contenu
   */
  public Cellule(Tableau tableau) {
    this();
    this.setTableau(tableau);
  }

  /**
   * Le constructeur avec un texte. Un paragraphe par défaut sera créé. Le contenu autra 0% de gris,
   * sera centré horizontalement et en haut verticalement avec une bordure de 1 pt.
   *
   * @param paragraphe le paragraphe de contenu
   */
  public Cellule(String texte) {
    this();
    this.setParagraphe(new Paragraphe(texte));
  }

  /**
   * Le constructeur avec un texte et un pourcentage de gris. Un paragraphe par défaut sera créé. Le contenu
   * sera centré horizontalement et en haut verticalement avec une bordure de 1 pt.
   *
   * @param paragraphe le paragraphe de contenu
   * @param pourcentageGris le pourcentage de gris
   */
  public Cellule(String texte, int pourcentageGris) {
    this();
    this.setParagraphe(new Paragraphe(texte));
    this.setPourcentageGris(pourcentageGris);
  }

  /**
   * Fixer le paragraphe de contenu.
   * @param paragraphe le paragraphe de contenu
   */
  public void setParagraphe(Paragraphe paragraphe) {
    this.paragraphe = paragraphe;
  }

  /**
   * Obtenir le paragraphe de contenu.
   * @return paragraphe le paragraphe de contenu
   */
  public Paragraphe getParagraphe() {
    return paragraphe;
  }

  /**
   * Fixer le pourcentage de gris de la cellule.
   * @param pourcentageGris le pourcentage de gris de la cellule
   */
  public void setPourcentageGris(int pourcentageGris) {
    this.pourcentageGris = pourcentageGris;
  }

  /**
   * Obtenir le pourcentage de gris de la cellule.
   * @return le pourcentage de gris de la cellule
   */
  public int getPourcentageGris() {
    return pourcentageGris;
  }

  /**
   * Fixer le tableau imbriqué.
   * @param tableau le tableau imbriqué
   */
  public void setTableau(Tableau tableau) {
    this.tableau = tableau;
  }

  /**
   * Obtenir le tableau imbriqué.
   * @return le tableau imbriqué
   */
  public Tableau getTableau() {
    return tableau;
  }

  /**
   * Fixer l'alignement horizontal.
   * <p>
   * Utiliser les constantes de documents visuels pour fixer l'alignement.
   *
   * @param alignementHorizontal l'alignement horizontal
   */
  public void setAlignementHorizontal(int alignementHorizontal) {
    this.alignementHorizontal = alignementHorizontal;
  }

  /**
   * Obtenir l'alignement horizontal.
   * @return l'alignement horizontal
   */
  public int getAlignementHorizontal() {
    return alignementHorizontal;
  }

  /**
   * Fixer l'alignement vertical.
   * <p>
   * Utiliser les constantes de documents visuels pour fixer l'alignement.
   *
   * @param alignementVertical l'alignement vertical
   */
  public void setAlignementVertical(int alignementVertical) {
    this.alignementVertical = alignementVertical;
  }

  /**
   * Obtenir l'alignement vertical.
   * @return l'alignement vertical
   */
  public int getAlignementVertical() {
    return alignementVertical;
  }

  /**
   * Fixer la largeur de la bordure en pt.
   * @param largeurBordure la largeur de la bordure en pt
   */
  public void setLargeurBordure(float largeurBordure) {
    this.largeurBordure = largeurBordure;
  }

  /**
   * Obtenir la largeur de la bordure en pt.
   * @return la largeur de la bordure en pt
   */
  public float getLargeurBordure() {
    return largeurBordure;
  }

  /**
   * Fixer le nombre de colonnes combinées par cette cellule.
   * @param colspan le nombre de colonnes combinées par cette cellule
   */
  public void setColspan(int colspan) {
    this.colspan = colspan;
  }

  /**
   * Obtenir le nombre de colonnes combinées par cette cellule.
   * @return le nombre de colonnes combinées par cette cellule
   */
  public int getColspan() {
    return colspan;
  }
}
