/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.tableau;

import java.util.ArrayList;
import java.util.Iterator;

import org.sofiframework.documentvisuel.ConstantesDocumentVisuel;
import org.sofiframework.documentvisuel.ElementContenu;


/**
 * Classe représentant un tableau qui est un élément de contenu des documents visuels.
 * <p>
 * Un taleau est représenté par une liste de lignes, une largeur en pourcentage par rapport à la page,
 * un alignement horizontal, une liste de largeurs de colonnes et une liste d'entêtes.
 * <p>
 * Par défaut, le tableau est centré et mesure 100 % de la largeur de la page.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Tableau extends ElementContenu {
  /**
   * 
   */
  private static final long serialVersionUID = 3976550227802058383L;

  /** La liste des lignes du tableau */
  private ArrayList listeLignes;

  /** le pourcentage de largeur de la page que le tableau prendra */
  private float pourcentageLargeur;

  /** l'alignement horizontal du tableau dans la page */
  private int alignement;

  /** la liste des grandeurs des colonnes en pt */
  private float[] grandeurColonnes;

  /** la liste des entêtes de colonnes */
  private ArrayList enteteColonnes;

  /**
   * Constructeur par défaut qui fixe une nouvelle liste de lignes, aligne le tableau au centre
   * et à 100 % de la longueur de la page.
   */
  public Tableau() {
    this.setListeLignes(new ArrayList());
    this.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_CENTRE);
    this.setPourcentageLargeur(100);
  }

  /**
   * Obtenir la ligne positionnée à la position demandée.
   * @param noLigne le numéro de la ligne à obtenir
   * @return la ligne demandée
   */
  public Ligne getLigne(int noLigne) {
    return (Ligne) this.getListeLignes().get(noLigne - 1);
  }

  /**
   * Obtenir le nombre de colonnes du tableau.
   * <p>
   * Retourne le nombre de colonnes de la ligne ayant le plus de colonnes.
   * @return le nombre de colonnes
   */
  public int getNombreColonnes() {
    int nombreColonnes = 0;

    Iterator iterateurLignes = this.getListeLignes().iterator();

    while (iterateurLignes.hasNext()) {
      Ligne ligne = (Ligne) iterateurLignes.next();

      if (ligne.getNombreColonnes() > nombreColonnes) {
        nombreColonnes = ligne.getNombreColonnes();
      }
    }

    return nombreColonnes;
  }

  /**
   * Fixer les entêtes de colonnes à partir d'un tableau de chaînes de caractères.
   * <p>
   * Pour chaque chaîne, une cellule est créée avec 10% de gris.
   * @param entetes la liste des textes des entêtes
   */
  public void setEnteteColonnes(String[] entetes) {
    this.setEnteteColonnes(new ArrayList());

    for (int i = 0; i < entetes.length; i++) {
      Cellule cellule = new Cellule(entetes[i]);
      cellule.setPourcentageGris(10);
      this.getEnteteColonnes().add(cellule);
    }
  }

  /**
   * Ajouter une nouvelle ligne dans le bas du tableau.
   * @param ligne la ligne à ajouter
   */
  public void ajouterLigne(Ligne ligne) {
    this.getListeLignes().add(ligne);
  }

  /**
   * Fixer la liste des lignes du tableau.
   * @param listeLignes la liste des lignes du tableau
   */
  public void setListeLignes(ArrayList listeLignes) {
    this.listeLignes = listeLignes;
  }

  /**
   * Obtenir la liste des lignes du tableau.
   * @return la liste des lignes du tableau
   */
  public ArrayList getListeLignes() {
    return listeLignes;
  }

  /**
   * Fixer le pourcentage de largeur du tableau par rapport à la page.
   * @param pourcentageLargeur le pourcentage de largeur du tableau par rapport à la page
   */
  public void setPourcentageLargeur(float pourcentageLargeur) {
    this.pourcentageLargeur = pourcentageLargeur;
  }

  /**
   * Obtenir le pourcentage de largeur du tableau par rapport à la page.
   * @return le pourcentage de largeur du tableau par rapport à la page
   */
  public float getPourcentageLargeur() {
    return pourcentageLargeur;
  }

  /**
   * Fixer l'alignement horizontal du tableau.
   * <p>
   * Utiliser les constantes de documents visuels pour fixer l'alignement.
   *
   * @param alignement l'alignement horizontal du tableau
   */
  public void setAlignement(int alignement) {
    this.alignement = alignement;
  }

  /**
   * Obtenir l'alignement horizontal du tableau.
   * @return l'alignement horizontal du tableau
   */
  public int getAlignement() {
    return alignement;
  }

  /**
   * Fixer les grandeurs en pt des colonnes.
   * @param grandeurColonnes les grandeurs en pt des colonnes
   */
  public void setGrandeurColonnes(float[] grandeurColonnes) {
    this.grandeurColonnes = grandeurColonnes;
  }

  /**
   * Obtenir les grandeurs en pt des colonnes.
   * @return les grandeurs en pt des colonnes
   */
  public float[] getGrandeurColonnes() {
    return grandeurColonnes;
  }

  /**
   * Fixer les entêtes des colonnes.
   * @param enteteColonnes une liste de Cellule qui feront les entêtes
   */
  public void setEnteteColonnes(ArrayList enteteColonnes) {
    this.enteteColonnes = enteteColonnes;
  }

  /**
   * Obtenir les entêtes des colonnes.
   * @return les entêtes des colonnes
   */
  public ArrayList getEnteteColonnes() {
    return enteteColonnes;
  }
}
