/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe représentant une police d'écriture pour la génération de documents visuels.
 * <p>
 * La police est représentée par un nom (une constante numérique), une taille en pt, une couleur
 * et des indicateurs pour le gras et l'italique.
 * <p>
 * La police par défaut est Times New Roman 12, en noir, non-gras et non-italique.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class PoliceEcriture extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -7947370085403792913L;

  /** le nom de la police (selon les constantes de documents visuels) */
  private int nom;

  /** la taille en pt de la police */
  private float taille;

  /** indique si le texte sera en gras */
  private boolean gras;

  /** indique si le texte sera en italique */
  private boolean italique;

  /** la couleur de la police */
  private Couleur couleur;

  /**
   * Constructeur par défaut qui crée du Times New Roman 12, noir, non-gras et non-italique.
   */
  public PoliceEcriture() {
    this(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN, 12);
  }

  /**
   * Constructeur avec un nom et une taille, en noir, non-gras et non-italique.
   *
   * @param nom le nom de la police (selon les constantes)
   * @param taille la taille en pt de la police
   */
  public PoliceEcriture(int nom, int taille) {
    this.setNom(nom);
    this.setTaille(taille);
    this.setGras(false);
    this.setItalique(false);
    this.setCouleur(ConstantesDocumentVisuel.COULEUR_NOIR);
  }

  /**
   * Fixer le nom de la police d'écriture.
   * <p>
   * Utiliser les constantes de documents visuels pour fixer le nom.
   * @param nom le nom de la police
   */
  public void setNom(int nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le nom de la police d'écriture.
   * @return le nom de la police
   */
  public int getNom() {
    return nom;
  }

  /**
   * Fixer la taille en pt de la police.
   * @param taille la taille en pt de la police
   */
  public void setTaille(float taille) {
    this.taille = taille;
  }

  /**
   * Obtenir la taille en pt de la police.
   * @return la taille en pt de la police
   */
  public float getTaille() {
    return taille;
  }

  /**
   * Indiquer si la police est en gras.
   * @param gras true si c'est en gras, sinon false
   */
  public void setGras(boolean gras) {
    this.gras = gras;
  }

  /**
   * Vérifier si la police est en gras.
   * @return true si c'est en gras, sinon false
   */
  public boolean isGras() {
    return gras;
  }

  /**
   * Indiquer si la police est en italique.
   * @param italique true si c'est en italique, sinon false
   */
  public void setItalique(boolean italique) {
    this.italique = italique;
  }

  /**
   * Vérifier si la police est en italique.
   * @return true si c'est en italique, sinon false
   */
  public boolean isItalique() {
    return italique;
  }

  /**
   * Fixer la couleur de la police.
   * @param couleur la couleur de la police
   */
  public void setCouleur(Couleur couleur) {
    this.couleur = couleur;
  }

  /**
   * Obtenir la couleur de la police.
   * @return la couleur de la police
   */
  public Couleur getCouleur() {
    return couleur;
  }
}
