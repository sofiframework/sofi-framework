/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;

import java.util.ArrayList;

import org.sofiframework.documentvisuel.tableau.Tableau;
import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Classe représentant une page de contenu.
 * <p>
 * Une page est constituée d'une liste d'éléments de contenu (paragraphe, document visuel, tableau, etc.),
 * d'un format (lettre, légal, etc.) et d'un indicateur pour savoir si elle est en format portait ou paysage.
 * <p>
 * Par défaut, la page est affichée en papier lettre en format portrait.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Page extends ObjetTransfert {
  /**
   * 
   */
  private static final long serialVersionUID = -371191509714980211L;

  /** La liste des éléments de contenu de la page */
  private ArrayList contenu;

  /** Le format de la page (lettre ou légal) */
  private int format;

  /** Indicateur de format portrait ou paysage. */
  private boolean portrait;

  /**
   * Constructeur par défaut qui crée une nouvelle liste de contenu, fixe le format à lettre et portrait.
   */
  public Page() {
    this.setContenu(new ArrayList());
    this.setFormat(ConstantesDocumentVisuel.FORMAT_LETTRE);
    this.setPortrait(true);
  }

  /**
   * Ajouter un paragraphe dans la page comme prochain élément de contenu.
   * @param paragraphe le paragraphe à ajouter.
   */
  public void ajouterParagraphe(Paragraphe paragraphe) {
    this.getContenu().add(paragraphe);
  }

  /**
   * Ajouter un tableau dans la page comme prochain élément de contenu.
   * @param tableau le tableau à ajouter.
   */
  public void ajouterTableau(Tableau tableau) {
    this.getContenu().add(tableau);
  }

  /**
   * Ajouter une image dans la page comme prochain élément de contenu.
   * @param image l'image à ajouter.
   */
  public void ajouterImage(ImageNumerique image) {
    this.getContenu().add(image);
  }

  /**
   * Fixer la liste d'éléments de contenu de la page.
   * @param contenu la liste d'éléments de contenu de la page
   */
  public void setContenu(ArrayList contenu) {
    this.contenu = contenu;
  }

  /**
   * Obtenir la liste d'éléments de contenu de la page.
   * @return la liste d'éléments de contenu de la page
   */
  public ArrayList getContenu() {
    return contenu;
  }

  /**
   * Fixer le format de papier de la page (légal, lettre, etc.).
   * <p>
   * Utiliser les constantes des documents visuel pour définir le format.
   *
   * @param format le format de la page
   */
  public void setFormat(int format) {
    this.format = format;
  }

  /**
   * Obtenir le format de papier de la page (légal, lettre, etc.).
   * @return le format de la page
   */
  public int getFormat() {
    return format;
  }

  /**
   * Indiquer si la page est en portrait ou paysage.
   * @param portrait true si c'est portrait, false si c'est paysage
   */
  public void setPortrait(boolean portrait) {
    this.portrait = portrait;
  }

  /**
   * Vérifier si la page est en portrait ou paysage.
   * @return true si c'est portrait, false si c'est paysage
   */
  public boolean isPortrait() {
    return portrait;
  }
}
