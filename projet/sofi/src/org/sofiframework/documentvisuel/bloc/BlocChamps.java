/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/BlocChamps.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


/**
 * Structure pour contenir des données à afficher dans des colonnes de type
 * "Libellé : valeur". Voici un example :
 * <pre>
 *   BlocChamps bloc = new BlocChamps("Titre");
 *   bloc.ajouterLignes(new String[][][] {
 *     { { "Nom :", "Poirier" } }
 *     { { "Prénom :", "Guillaume" } },
 *     { { "Téléphone :", "418-627-2001" }, { "Poste :", "1234" } },
 *     { { "Autre téléphone :", "418-555-5555" }, { "Poste :", "1234" } },
 *   });
 * </pre>
 * <p>S'affiche avec la structure suivante :</p>
 * <pre>
 *                       Titre
 * --------------------------------------------------
 *             Nom : Poirier
 *          Prénom : Guillaume
 *       Téléphone : 418-627-2001       Poste : 1234
 * Autre téléphone : 418-627-2001       Poste : 1234
 * </pre>
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class BlocChamps extends Bloc {
  private ArrayList listeLignes = new ArrayList();
  private int nombreColonnes;
  private boolean nombreColonnesDynamique = true;

  /**
   * Constructeur par défaut
   */
  public BlocChamps() {
  }

  /**
   * Constructeur avec un titre explicit
   * @param titre Le titre du bloc
   */
  public BlocChamps(String titre) {
    super(titre);
  }

  /**
   * Constructeur avec un titre et un nombre de colonnes explicit
   * @param titre Le titre du bloc
   * @param nombreColonnes Le nombre de colonnes à affiché pour le bloc.
   * Une colonne est une paire "libellé: valeur".
   */
  public BlocChamps(String titre, int nombreColonnes) {
    super(titre);
    this.nombreColonnes = nombreColonnes;
    nombreColonnesDynamique = false;
  }

  /**
   * Ajoute de nouvelle lignes de données.  Un tableau de Champ est une ligne.
   * @param lignes Les données à ajouter
   */
  public void ajouterLignes(Champ[][] lignes) {
    for (int i = 0, n = lignes.length; i < n; i++) {
      ajouterLigne(lignes[i]);
    }
  }

  /**
   * Ajoute de nouvelle lignes de données.  String[] représente un champ, donc
   * String[][] représente une ligne et String[][][] représente plusieurs
   * lignes.
   * @param lignes Les données à ajouter
   */
  public void ajouterLignes(String[][][] lignes) {
    for (int i = 0, n = lignes.length; i < n; i++) {
      ajouterLigne(lignes[i]);
    }
  }

  /**
   * Ajoute une ligne d'un seul champ à partir du libellé et de la valeur
   * définit.
   * @param libelle Le libellé
   * @param valeur La valeur
   */
  public void ajouterLigne(String libelle, String valeur) {
    ajouterLigne(new Champ(libelle, valeur));
  }

  /**
   * Ajoute une ligne qui comporte un seul champ
   * @param champ Le champ à ajouter.
   */
  public void ajouterLigne(Champ champ) {
    ajouterLigne(new Champ[] { champ });
  }

  /**
   * Ajoute une nouvelle ligne de données.  String[] représente un champ,
   * String[][] représente la ligne.
   * @param ligne Les données à ajouter
   */
  public void ajouterLigne(String[][] ligne) {
    Champ[] champs = new Champ[ligne.length];

    for (int i = 0, n = ligne.length; i < n; i++) {
      String[] champ = ligne[i];

      if (champ != null) {
        champs[i] = new Champ(champ[0], champ[1]);
      } else {
        champs[i] = null;
      }
    }

    ajouterLigne(champs);
  }

  /**
   * Ajoute une nouvelle ligne de données.
   * @param champs La liste des champs de la ligne.
   */
  public void ajouterLigne(Champ[] champs) {
    if (nombreColonnesDynamique && (champs.length > nombreColonnes)) {
      nombreColonnes = champs.length;
    }

    listeLignes.add(champs);
  }

  /**
   * Retourne le nombre de colonnes pour le bloc.  Si le nombre de colonnes
   * n'a pas été définit dans le constructeur, le nombre de colonnes est
   * calculé selon la ligne qui a le nombre le plus élevé de champs.
   * @return Le nombre de colonnes pour le bloc.
   */
  public int getNombreColonnes() {
    return nombreColonnes;
  }

  /**
   * Retourne la liste des lignes pour le bloc.
   * @return la liste des lignes pour le bloc.
   */
  public Collection getListeLignes() {
    return Collections.unmodifiableList(listeLignes);
  }
}
