/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/Bloc.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;


/**
 * Classe de base pour les différent types de blocs.  Un BlocEntete correspond
 * à un fieldset en HTML, et un BlocChamps ou un BlocTableau correspond à
 * une table HTML, avec un fieldset autour si le titre du bloc est non-null.
 *
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public abstract class Bloc {
  private String titre;
  private int niveau;

  /**
   * Constructeur par défaut
   */
  public Bloc() {
  }

  /**
   * Constructeur qui permet d'assigné un titre au bloc
   * @param titre le titre du bloc
   */
  public Bloc(String titre) {
    this.titre = titre;
  }

  /**
   * Fixe le titre du bloc
   * @param titre le titre du bloc
   */
  public void setTitre(String titre) {
    this.titre = titre;
  }

  /**
   * Retourne le titre du bloc
   * @return le titre du bloc
   */
  public String getTitre() {
    return titre;
  }

  /**
   * Permet d'assigné le niveau d'imbriquation du bloc.  Ce champ est définit
   * automatiquement pas BlocEntete lorsque des blocs enfants lui sont ajoutés.
   * @param niveau le niveau d'imbriquation du bloc.
   */
  public void setNiveau(int niveau) {
    this.niveau = niveau;
  }

  /**
   * Retourne le niveau d'imbriquation du bloc.  Ce champ est définit
   * automatiquement pas BlocEntete lorsque des blocs enfants lui sont ajoutés.
   */
  public int getNiveau() {
    return niveau;
  }
}
