/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/ConvertisseurLibelleEjb.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import java.text.MessageFormat;

import org.sofiframework.application.message.ejb.RemoteServiceLibelle;
import org.sofiframework.application.message.ejb.ServiceLibelleHome;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.modele.ejb.EjbProxy;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Implémentation de <code>ConvertisseurLibelle</code> qui apelle un EJB
 * pour obtenir la valeur du libellé par sa clé.
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class ConvertisseurLibelleEjb extends ConvertisseurLibelleBase {
  private RemoteServiceLibelle ejb;
  private ConvertisseurLibelle convertisseurDefaut = new ConvertisseurLibelleNul();

  /**
   * Construit un <code>ConvertisseurLibelleEjb</code> avec le service définit
   * dans <code>GestionServiceDistant</code>.
   */
  public ConvertisseurLibelleEjb() {
    this((RemoteServiceLibelle) GestionServiceDistant.getServiceDistant(
        "ServiceLibelleBean"));
  }

  /**
   * Construit un <code>ConvertisseurLibelleEjb</code> le EJB obtenu du EJBHome
   * passé en paramètre.
   */
  public ConvertisseurLibelleEjb(ServiceLibelleHome home) {
    this((RemoteServiceLibelle) EjbProxy.createProxy(home));
  }

  /**
   * Construit un <code>ConvertisseurLibelleEjb</code> le EJB
   * passé en paramètre.
   */
  public ConvertisseurLibelleEjb(RemoteServiceLibelle ejb) {
    this.ejb = ejb;
  }

  /*
   * Voir ConvertisseurLibelle
   */
  @Override
  public String convertirLibelle(String label, String[] args) {
    try {
      Libelle libelle = ejb.getLibelle(label, "FR", null);

      if (libelle != null) {
        String message = UtilitaireString.traiterApostrophe(libelle.getMessage());

        return MessageFormat.format(message, (Object[]) args);
      } else {
        return convertisseurDefaut.convertirLibelle(label, args);
      }
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new SOFIException(e);
    }
  }

  /**
   * Modifie le <code>ConvertisseurLibelle</code> par defaut.  Les appels
   * de méthode sont redirigés vers cette instance si aucun libellé n'est trouvé
   * dans le EJB.
   */
  public void setConvertisseurDefaut(ConvertisseurLibelle convertisseurDefaut) {
    this.convertisseurDefaut = convertisseurDefaut;
  }

  /**
   * Retourne le <code>ConvertisseurLibelle</code> par defaut.  Les appels
   * de méthode sont redirigés vers cette instance si aucun libellé n'est trouvé
   * dans le EJB.
   */
  public ConvertisseurLibelle getConvertisseurDefaut() {
    return convertisseurDefaut;
  }
}
