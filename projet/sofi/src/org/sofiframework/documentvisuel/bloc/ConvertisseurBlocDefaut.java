/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/ConvertisseurBlocDefaut.java $
 * $LastChangedBy: poirgu $
 * $LastChangedDate: 2006-06-12 11:07:02 -0400 (lun., 12 juin 2006) $
 * $LastChangedRevision: 403 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import java.util.Iterator;

import org.sofiframework.documentvisuel.ConstantesDocumentVisuel;
import org.sofiframework.documentvisuel.Page;
import org.sofiframework.documentvisuel.Paragraphe;
import org.sofiframework.documentvisuel.PoliceEcriture;
import org.sofiframework.documentvisuel.tableau.Cellule;
import org.sofiframework.documentvisuel.tableau.Ligne;
import org.sofiframework.documentvisuel.tableau.Tableau;


/**
 * Implémentation simple de <code>ConvertisseurBloc</code>.
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class ConvertisseurBlocDefaut implements ConvertisseurBloc {
  private static final Cellule CELLULE_VIDE;

  static {
    CELLULE_VIDE = new Cellule("");
    CELLULE_VIDE.setLargeurBordure(0);
  }

  private ConvertisseurLibelle convertisseurLibelle;
  private PoliceEcriture policeChampLibelle;
  private PoliceEcriture policeChampValeur;
  private PoliceEcriture policeEnteteBloc;
  private PoliceEcriture policeEnteteTableau;
  private PoliceEcriture policeValeurTableau;

  /**
   * Constructeur par défaut, utilise <code>ConvertisseurLibelleNul</code>
   */
  public ConvertisseurBlocDefaut() {
    this(new ConvertisseurLibelleNul());
  }

  /**
   * Construit un <code>ConvertisseurBlocDefaut</code> avec un
   * <code>ConvertisseurLibelle</code> spécifique.
   * @param convertisseurLibelle le <code>ConvertisseurLibelle</code> à utiliser
   */
  public ConvertisseurBlocDefaut(ConvertisseurLibelle convertisseurLibelle) {
    this.convertisseurLibelle = convertisseurLibelle;
    policeChampLibelle = new PoliceEcriture(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN,
        10);
    policeChampValeur = new PoliceEcriture(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN,
        10);
    policeEnteteBloc = new PoliceEcriture(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN,
        11);
    policeEnteteTableau = new PoliceEcriture(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN,
        10);
    policeValeurTableau = new PoliceEcriture(ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN,
        10);
    policeEnteteTableau.setGras(true);
  }

  /**
   * Applique les styles pour l'entete du bloc et ajoute le résultat dans
   * <code>page</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  private void convertirEntete(Page page, Bloc bloc) {
    String titre = bloc.getTitre();

    if (titre != null) {
      Tableau titreTableau = new Tableau();
      titreTableau.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE);
      titreTableau.setPourcentageLargeur(100);

      Ligne titreLigne = new Ligne();
      Paragraphe p = new Paragraphe(convertisseurLibelle.convertirLibelle(titre));
      p.setPoliceEcriture(policeEnteteBloc);

      Cellule titreCellule = new Cellule(p);
      titreCellule.setPourcentageGris((bloc.getNiveau() * 10) + 25);
      titreCellule.setAlignementHorizontal(ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE);
      titreCellule.setLargeurBordure(0);
      titreLigne.ajouterCellule(titreCellule);
      titreTableau.ajouterLigne(titreLigne);
      page.ajouterTableau(titreTableau);
    }
  }

  /**
   * Applique les styles pour le bloc et ajoute le résultat dans
   * <code>page</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  private void convertirBlocEntete(Page page, BlocEntete bloc) {
    for (Iterator it = bloc.getListeBlocs().iterator(); it.hasNext();) {
      convertirBloc(page, (Bloc) it.next());
    }
  }

  /**
   * Applique les styles pour le bloc et ajoute le résultat dans
   * <code>page</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  private void convertirBlocChamps(Page page, BlocChamps bloc) {
    Tableau table = new Tableau();

    table.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE);
    table.setPourcentageLargeur(100);

    switch (bloc.getNombreColonnes()) {
    case 1:
      table.setGrandeurColonnes(new float[] { 30, 70 });

      break;

    case 2:
      table.setGrandeurColonnes(new float[] { 30, 30, 4, 18, 18 });

      break;

    default:
      throw new IllegalArgumentException("TODO: Ajouter support pour " +
          bloc.getNombreColonnes() + " colonnes");
    }

    for (Iterator it = bloc.getListeLignes().iterator(); it.hasNext();) {
      Object enfant = it.next();
      Champ[] champs = (Champ[]) enfant;
      Ligne ligne = new Ligne();

      int i = 0;

      for (int n = champs.length; i < n; i++) {
        Champ champ = champs[i];

        if (i != 0) {
          ligne.ajouterCellule(CELLULE_VIDE);
        }

        if (champ != null) {
          Paragraphe p;

          p = new Paragraphe(convertisseurLibelle.convertirLibelle(
              champ.getLibelle()));
          p.setPoliceEcriture(policeChampLibelle);

          Cellule gauche = new Cellule(p);
          gauche.setAlignementHorizontal(ConstantesDocumentVisuel.ALIGNEMENT_DROITE);
          gauche.setLargeurBordure(0);

          p = new Paragraphe(convertisseurLibelle.convertirLibelle(
              champ.getValeur()));
          p.setPoliceEcriture(policeChampValeur);

          Cellule droite = new Cellule(p);
          droite.setAlignementHorizontal(ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE);
          droite.setAlignementVertical(ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_BAS);
          droite.setLargeurBordure(0);

          ligne.ajouterCellule(gauche);
          ligne.ajouterCellule(droite);
        } else {
          ligne.ajouterCellule(CELLULE_VIDE);
          ligne.ajouterCellule(CELLULE_VIDE);
        }
      }

      for (int n = bloc.getNombreColonnes(); i < n; i++) {
        ligne.ajouterCellule(CELLULE_VIDE);
        ligne.ajouterCellule(CELLULE_VIDE);
        ligne.ajouterCellule(CELLULE_VIDE);
      }

      table.ajouterLigne(ligne);
    }

    page.ajouterTableau(table);
    page.ajouterParagraphe(new Paragraphe());
  }

  /**
   * Applique les styles pour le bloc et ajoute le résultat dans
   * <code>page</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  private void convertirBlocTableau(Page page, BlocTableau bloc) {
    Tableau table = new Tableau();

    table.setAlignement(ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE);
    table.setPourcentageLargeur(100);

    if (bloc.getGrandeurColonnes() != null) {
      table.setGrandeurColonnes(bloc.getGrandeurColonnes());
    }

    String[] entete = bloc.getEntete();
    int nombreColonnes = bloc.getNombreColonnes();
    Ligne ligne;

    if (entete != null) {
      ligne = new Ligne();

      for (int i = 0, n = entete.length; i < n; i++) {
        Paragraphe p = new Paragraphe(convertisseurLibelle.convertirLibelle(
            entete[i]));
        p.setPoliceEcriture(policeEnteteTableau);

        Cellule cellule = new Cellule(p);
        cellule.setAlignementHorizontal(bloc.getAlignementHorizontalEntete());
        cellule.setAlignementVertical(bloc.getAlignementVerticalEntete());
        cellule.setLargeurBordure(0);
        ligne.ajouterCellule(cellule);
      }

      table.ajouterLigne(ligne);
    }

    for (Iterator it = bloc.getListeLignes().iterator(); it.hasNext();) {
      String[] valeurs = (String[]) it.next();
      ligne = new Ligne();

      for (int i = 0, n = valeurs.length; i < n; i++) {
        Paragraphe p = new Paragraphe(valeurs[i]);
        p.setPoliceEcriture(policeValeurTableau);

        Cellule cellule = new Cellule(p);
        cellule.setAlignementHorizontal(bloc.getAlignementHorizontal());
        cellule.setAlignementVertical(bloc.getAlignementVertical());
        cellule.setLargeurBordure(0);

        //        if (i + 1 == n && n != nombreColonnes) {
        //          cellule.setColspan(nombreColonnes - i);
        //        }
        ligne.ajouterCellule(cellule);
      }

      for (int i = valeurs.length; i < nombreColonnes; i++) {
        ligne.ajouterCellule(CELLULE_VIDE);
      }

      table.ajouterLigne(ligne);
    }

    page.ajouterTableau(table);
    page.ajouterParagraphe(new Paragraphe());
  }

  /**
   * Applique les styles pour le bloc et ajoute le résultat dans
   * <code>page</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  @Override
  public void convertirBloc(Page page, Bloc bloc) {
    convertirEntete(page, bloc);

    if (bloc instanceof BlocChamps) {
      convertirBlocChamps(page, (BlocChamps) bloc);
    } else if (bloc instanceof BlocTableau) {
      convertirBlocTableau(page, (BlocTableau) bloc);
    } else {
      convertirBlocEntete(page, (BlocEntete) bloc);
    }
  }

  /**
   * Fixe la police à utiliser pour les libellés des champs <code>(BlocChamps)</code>
   * @param policeChampLibelle la police
   */
  public void setPoliceChampLibelle(PoliceEcriture policeChampLibelle) {
    this.policeChampLibelle = policeChampLibelle;
  }

  /**
   * Retourne la police à utiliser pour les libellés des champs <code>(BlocChamps)</code>
   * @return la police
   */
  public PoliceEcriture getPoliceChampLibelle() {
    return policeChampLibelle;
  }

  /**
   * Fixe la police à utiliser pour les valeurs des champs <code>(BlocChamps)</code>
   * @param policeChampLibelle la police
   */
  public void setPoliceChampValeur(PoliceEcriture policeChampValeur) {
    this.policeChampValeur = policeChampValeur;
  }

  /**
   * Retourne la police à utiliser pour les valeurs des champs <code>(BlocChamps)</code>
   * @return la police
   */
  public PoliceEcriture getPoliceChampValeur() {
    return policeChampValeur;
  }

  /**
   * Fixe la police à utiliser pour les entête de bloc.
   * @param policeChampLibelle la police
   */
  public void setPoliceEnteteBloc(PoliceEcriture policeEnteteBloc) {
    this.policeEnteteBloc = policeEnteteBloc;
  }

  /**
   * Retourne la police à utiliser pour les entête de bloc.
   * @return la police
   */
  public PoliceEcriture getPoliceEnteteBloc() {
    return policeEnteteBloc;
  }

  /**
   * Fixe la police à utiliser pour les entête de <code>BlocTableau</code>.
   * @param policeChampLibelle la police
   */
  public void setPoliceEnteteTableau(PoliceEcriture policeEnteteTableau) {
    this.policeEnteteTableau = policeEnteteTableau;
  }

  /**
   * Retourne la police à utiliser pour les entête de <code>BlocTableau</code>.
   * @return la police
   */
  public PoliceEcriture getPoliceEnteteTableau() {
    return policeEnteteTableau;
  }

  /**
   * Fixe la police à utiliser pour les valeurs dans les lignes de <code>BlocTableau</code>.
   * @param policeChampLibelle la police
   */
  public void setPoliceValeurTableau(PoliceEcriture policeValeurTableau) {
    this.policeValeurTableau = policeValeurTableau;
  }

  /**
   * Retourne la police à utiliser pour les valeurs dans les lignes de <code>BlocTableau</code>.
   * @return la police
   */
  public PoliceEcriture getPoliceValeurTableau() {
    return policeValeurTableau;
  }
}
