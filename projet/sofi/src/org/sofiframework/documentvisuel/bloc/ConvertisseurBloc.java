/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/ConvertisseurBloc.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import org.sofiframework.documentvisuel.Page;


/**
 * Interface pour écrire un <code>Bloc</code> (structure de donnée)
 * dans une page de document visuel qui contient la présentation des données.
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public interface ConvertisseurBloc {
  /**
   * Ajout les données du bloc dans la page selon un gabarit propre à l'instance
   * concrete du <code>ConvertisseurBloc</code>.
   * @param page la page où ajouter les données
   * @param bloc le bloc de données
   */
  public void convertirBloc(Page page, Bloc bloc);
}
