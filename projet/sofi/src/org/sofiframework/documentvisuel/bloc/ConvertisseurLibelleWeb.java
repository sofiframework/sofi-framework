/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/ConvertisseurLibelleWeb.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;


/**
 * Implémentation de <code>ConvertisseurLibelle</code> qui délègue
 * la résolution du libellé à <code>UtilitaireLibelle</code> selon
 * la locale de l'utilisateur spécifié.
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class ConvertisseurLibelleWeb extends ConvertisseurLibelleBase {
  private Utilisateur utilisateur;

  /**
   * Construit un convertisseur avec l'utilisateur spécifié
   */
  public ConvertisseurLibelleWeb(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  /**
   * Construit un convertisseur avec l'utilisateur pour la requête HTTP courante
   */
  public ConvertisseurLibelleWeb(HttpServletRequest request) {
    this(request.getSession());
  }

  /**
   * Construit un convertisseur avec l'utilisateur pour la session HTTP courante
   */
  public ConvertisseurLibelleWeb(HttpSession session) {
    utilisateur = UtilitaireControleur.getUtilisateur(session);
  }

  /*
   * Voir ConvertisseurLibelle
   */
  @Override
  public String convertirLibelle(String libelle, String[] args) {
    return UtilitaireLibelle.getLibelle(libelle, utilisateur, args).getMessage();
  }
}
