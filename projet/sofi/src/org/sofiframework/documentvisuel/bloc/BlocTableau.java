/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/BlocTableau.java $
 * $LastChangedBy: poirgu $
 * $LastChangedDate: 2006-06-12 11:07:02 -0400 (lun., 12 juin 2006) $
 * $LastChangedRevision: 403 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.sofiframework.documentvisuel.ConstantesDocumentVisuel;


/**
 * Structure pour contenir des données à afficher dans un tableau avec
 * des entetes de colonnes.  e.g.
 *
 * <pre>
 * BlocTableau bloc = new BlocTableau(
 *        "Titre",
 *        new String[] { "Nom", "Prenom", "Téléphone" });
 *
 * bloc.ajouterLigne(new String[] { "Poirier", "Guillaume", "418-627-2001" });
 * bloc.ajouterLigne(new String[] { "Moore", "Roger", "418-555-6666" });
 * </pre>
 *
 * <p>S'affiche avec la structure suivante :</p>
 *
 * <pre>
 *              Titre
 *  ----------------------------
 *  <b>Nom        Prénom       Téléphone</b>
 *  Poirier    Guillaume    418-627-2001
 *  Moore      Roger        418-555-6666
 * </pre>
 *
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class BlocTableau extends Bloc {
  private String[] entete;
  private ArrayList listeLignes = new ArrayList();
  private int nombreColonnes;
  private int alignementVertical = ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_BAS;
  private int alignementHorizontal = ConstantesDocumentVisuel.ALIGNEMENT_CENTRE;
  private int alignementVerticalEntete = ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_HAUT;
  private int alignementHorizontalEntete = ConstantesDocumentVisuel.ALIGNEMENT_CENTRE;
  private float[] grandeurColonnes;

  /**
   * Créer une instance de BlocTableau
   * @param titre le titre du bloc
   * @param entete les entêtes de colonne du tableau
   */
  public BlocTableau(String titre, String[] entete) {
    super(titre);
    this.entete = entete;

    if (entete != null) {
      this.nombreColonnes = entete.length;
    }
  }

  /**
   * Retourne la liste des entêtes de colonne du tableau
   * @return la liste des entêtes de colonne du tableau
   */
  public String[] getEntete() {
    return entete;
  }

  /**
   * Ajoute une ligne de données au tableau
   * @param ligne une ligne de données
   */
  public void ajouterLigne(String[] ligne) {
    listeLignes.add(ligne);
    nombreColonnes = Math.max(nombreColonnes, ligne.length);
  }

  /**
   * Ajoute une liste de lignes de données au tableau
   * @param une liste de lignes de données
   */
  public void ajouterLignes(String[][] lignes) {
    for (int i = 0, n = lignes.length; i < n; i++) {
      ajouterLigne(lignes[i]);
    }
  }

  /**
   * Retourne la liste de lignes du tableau
   * @return  la liste de lignes du tableau
   */
  public Collection getListeLignes() {
    return Collections.unmodifiableList(listeLignes);
  }

  public int getNombreColonnes() {
    return nombreColonnes;
  }

  public void setAlignementVertical(int alignementVertical) {
    this.alignementVertical = alignementVertical;
  }

  public int getAlignementVertical() {
    return alignementVertical;
  }

  public void setAlignementHorizontal(int alignementHorizontal) {
    this.alignementHorizontal = alignementHorizontal;
  }

  public int getAlignementHorizontal() {
    return alignementHorizontal;
  }

  public void setGrandeurColonnes(float[] grandeurColonnes) {
    this.grandeurColonnes = grandeurColonnes;
  }

  public float[] getGrandeurColonnes() {
    return grandeurColonnes;
  }

  public void setAlignementVerticalEntete(int alignementVerticalEntete) {
    this.alignementVerticalEntete = alignementVerticalEntete;
  }

  public int getAlignementVerticalEntete() {
    return alignementVerticalEntete;
  }

  public void setAlignementHorizontalEntete(int alignementHorizontalEntete) {
    this.alignementHorizontalEntete = alignementHorizontalEntete;
  }

  public int getAlignementHorizontalEntete() {
    return alignementHorizontalEntete;
  }
}
