/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/Champ.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;


/**
 * Classe qui contient les données "Libellé : Valeur" d'un <code>BlocChamps</code>.
 *
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class Champ {
  private String libelle;
  private String valeur;

  /**
   * Constructeur par défaut
   */
  public Champ() {
  }

  /**
   * Constructeur avec un libellé et une valeur explicit.
   * @param libelle le libellé du champ
   * @param valeur la valeur du champ
   */
  public Champ(String libelle, String valeur) {
    this.libelle = libelle;
    this.valeur = valeur;
  }

  /**
   * Fixe le libellé du champ
   * @param libelle le libellé du champ
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Retourne le libellé du champ
   * @return  le libellé du champ
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Fixe la valeur du champ
   * @param valeur la valeur du champ
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Retourne la valeur du champ
   * @return  la valeur du champ
   */
  public String getValeur() {
    return valeur;
  }
}
