/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/ConvertisseurLibelle.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;


/**
 * Interface commune pour la conversion de clés à son libellé concret,
 * selon la locale de l'utilisateur.
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public interface ConvertisseurLibelle {
  /**
   * Transforme une clé de libellé en sont texte affichage.
   * @param libelle une clé de libellé
   * @return un libellé
   */
  public String convertirLibelle(String libelle);

  /**
   * Transforme une clé de libellé en sont texte affichage en tentant compte
   * des paramètres spécifiés.
   * @param libelle une clé de libellé
   * @param args les paramètres du libellé
   * @return un libellé
   */
  public String convertirLibelle(String libelle, String[] args);

  /**
   * Transforme une clé de libellé en sont texte affichage en tentant compte
   * des paramètres spécifiés.
   * @param libelle une clé de libellé
   * @param arg1 un paramètre du libellé
   * @return un libellé
   */
  public String convertirLibelle(String libelle, String args1);

  /**
   * Transforme une clé de libellé en sont texte affichage en tentant compte
   * des paramètres spécifiés.
   * @param libelle une clé de libellé
   * @param arg1 un paramètre du libellé
   * @param arg2 un paramètre du libellé
   * @return un libellé
   */
  public String convertirLibelle(String libelle, String args1, String arg2);
}
