/*
 * $URL: svn://qcdoas04.mcc1/SOFI/trunk/sources/SOFI/src/org/sofiframework/documentvisuel/bloc/BlocEntete.java $
 * $LastChangedBy: tremst $
 * $LastChangedDate: 2006-05-15 11:42:28 -0400 (lun., 15 mai 2006) $
 * $LastChangedRevision: 359 $
 */
/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel.bloc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


/**
 * Contenant de bloc, contient un entête et des blocs enfants.
 *
 * @author Guillaume Poirier (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class BlocEntete extends Bloc {
  private ArrayList listeBlocs = new ArrayList();

  /**
   * Constructeur avec un titre
   * @param titre le titre du bloc
   */
  public BlocEntete(String titre) {
    super(titre);
  }

  /**
   * Constructeur avec titre et une liste de blocs.
   * @param titre le titre du bloc
   * @param blocs liste de blocs
   */
  public BlocEntete(String titre, Bloc[] blocs) {
    super(titre);
    ajouterBlocs(blocs);
  }

  /**
   * Ajoute un bloc enfant
   * @param bloc un bloc de données
   */
  public void ajouterBloc(Bloc bloc) {
    listeBlocs.add(bloc);
    bloc.setNiveau(getNiveau() + 1);
  }

  /**
   * Ajoute une liste de blocs enfants.
   * @param blocs une liste de blocs.
   */
  public void ajouterBlocs(Bloc[] blocs) {
    for (int i = 0, n = blocs.length; i < n; i++) {
      ajouterBloc(blocs[i]);
    }
  }

  /**
   * Retourne les blocs enfants
   * @return les blocs enfants
   */
  public Collection getListeBlocs() {
    return Collections.unmodifiableList(listeBlocs);
  }
}
