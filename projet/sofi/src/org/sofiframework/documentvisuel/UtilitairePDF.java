/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.documentvisuel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;

import org.sofiframework.documentvisuel.tableau.Cellule;
import org.sofiframework.documentvisuel.tableau.Ligne;
import org.sofiframework.documentvisuel.tableau.Tableau;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Convertisseur de document visuel en document PDF.
 * <p>
 * Cette classe utilise le framework iText pour générer le PDF à partir des informations du document.
 * Le PDF est retourné sous la forme d'un tableau de byte en format binaire.
 * <p>
 * @author Steve Tremblay (Nurun inc.)
 * @version 1.0
 * @since 2.0
 */
public class UtilitairePDF {
  /**
   * Générer le document PDF à partir du document visuel.
   * @param documentVisuel le document visuel à convertir
   * @return un tableau de byte[] contenant le binaire du document PDF
   * @throws java.io.IOException Normalement ne devrait pas survenir (pas de I/O physique)
   * @throws com.lowagie.text.DocumentException En cas de problème iText
   */
  public static byte[] genererFichier(DocumentVisuel documentVisuel)
      throws DocumentException, IOException {
    Document documentPDF = new Document(convertirFormatPage(
        (Page) documentVisuel.getListePages().get(0)));

    documentPDF.setMargins(documentVisuel.getMargeGauche().floatValue(),
        documentVisuel.getMargeDroite().floatValue(),
        documentVisuel.getMargeHaut().floatValue(),
        documentVisuel.getMargeBas().floatValue());

    ByteArrayOutputStream donnees = null;

    try {
      donnees = new ByteArrayOutputStream();

      PdfWriter writer = PdfWriter.getInstance(documentPDF, donnees);

      documentPDF.open();

      if (documentVisuel.isPagination()) {
        Phrase phrase = new Phrase("", new Font(Font.TIMES_ROMAN, 8));
        HeaderFooter footer = new HeaderFooter(phrase, true);
        footer.setBorder(0);
        footer.setAlignment(Element.ALIGN_RIGHT);

        documentPDF.setFooter(footer);
      }

      Iterator iterateurPages = documentVisuel.getListePages().iterator();

      while (iterateurPages.hasNext()) {
        Page page = (Page) iterateurPages.next();
        documentPDF.setPageSize(convertirFormatPage(page));
        documentPDF.newPage();
        writer.setPageEmpty(false);

        Iterator iterateurContenu = page.getContenu().iterator();

        while (iterateurContenu.hasNext()) {
          ElementContenu element = (ElementContenu) iterateurContenu.next();

          if (element instanceof Paragraphe) {
            documentPDF.add(convertirParagraphe((Paragraphe) element));
          }

          if (element instanceof Tableau) {
            documentPDF.add(convertirTableau((Tableau) element));
          }

          if (element instanceof ImageNumerique) {
            documentPDF.add(convertirImage((ImageNumerique) element));
          }
        }
      }
    } catch (DocumentException e) {
      documentPDF.close();
      throw e;
    } catch (IOException e) {
      documentPDF.close();
      throw e;
    }

    documentPDF.close();

    return donnees.toByteArray();
  }

  /**
   * Convertir un paragraphe visuel en paragraphe iText
   * @param paragraphe le paragraphe visuel à convertir
   * @return un paragraphe iText correspondant
   */
  public static Paragraph convertirParagraphe(Paragraphe paragraphe) {
    Paragraph paragraphePDF = new Paragraph(paragraphe.getTexteParagraphe(),
        convertirPolice(paragraphe.getPoliceEcriture()));
    paragraphePDF.setAlignment(convertirAlignement(paragraphe.getAlignement()));
    paragraphePDF.setIndentationLeft(paragraphe.getIndentationGauche());
    paragraphePDF.setSpacingAfter(paragraphe.getEspaceApres());

    return paragraphePDF;
  }

  /**
   * Convertir une image numérique et Image de iText
   * @param imageNumerique l'image numérique à convertir
   * @return une image iText
   * @throws com.lowagie.text.BadElementException en cas de problème iText
   * @throws java.net.MalformedURLException ne surviendra pas vraiment (pas de fichier physique)
   * @throws java.io.IOException ne surviendra pas vraiment (pas de I/O physique)
   */
  public static Image convertirImage(ImageNumerique imageNumerique)
      throws IOException, BadElementException, MalformedURLException {
    Image image = Image.getInstance(imageNumerique.getContenu());
    image.setAlignment(convertirAlignementImage(imageNumerique.getAlignement()));

    if (imageNumerique.getPourcentageTaille() != 100) {
      image.scalePercent(imageNumerique.getPourcentageTaille());
    }

    return image;
  }

  /**
   * Convertir le format d'une page en format iText
   * @param page la page dont on veut obtenir le format
   * @return le format iText
   */
  public static Rectangle convertirFormatPage(Page page) {
    Rectangle format = null;

    if (page.getFormat() == ConstantesDocumentVisuel.FORMAT_LEGAL) {
      format = PageSize.LEGAL;

      if (!page.isPortrait()) {
        format = format.rotate();
      }
    }

    if (page.getFormat() == ConstantesDocumentVisuel.FORMAT_LETTRE) {
      format = PageSize.LETTER;

      if (!page.isPortrait()) {
        format = format.rotate();
      }
    }

    return format;
  }

  /**
   * Convertir un tableau en tableau iText.
   * @param tableau le tableau à convertir
   * @return un tableau iText
   * @throws com.lowagie.textDocumentException en cas de problème iText
   */
  public static PdfPTable convertirTableau(Tableau tableau)
      throws DocumentException {
    PdfPTable tableauPDF = new PdfPTable(tableau.getNombreColonnes());
    tableauPDF.setHorizontalAlignment(convertirAlignement(
        tableau.getAlignement()));
    tableauPDF.setWidthPercentage(tableau.getPourcentageLargeur());

    if (tableau.getGrandeurColonnes() != null) {
      tableauPDF.setWidths(tableau.getGrandeurColonnes());
    }

    if (tableau.getEnteteColonnes() != null) {
      Iterator iterateurEntetes = tableau.getEnteteColonnes().iterator();

      while (iterateurEntetes.hasNext()) {
        Cellule cellule = (Cellule) iterateurEntetes.next();
        tableauPDF.addCell(convertirCellule(cellule));
      }

      tableauPDF.setHeaderRows(1);
    }

    Iterator iterateurLignes = tableau.getListeLignes().iterator();

    while (iterateurLignes.hasNext()) {
      Ligne ligne = (Ligne) iterateurLignes.next();

      Iterator iterateurColonnes = ligne.getListeCellules().iterator();

      while (iterateurColonnes.hasNext()) {
        Cellule cellule = (Cellule) iterateurColonnes.next();
        tableauPDF.addCell(convertirCellule(cellule));
      }
    }

    return tableauPDF;
  }

  /**
   * Convertir une cellule en cellule iText.
   * @param cellule la cellule à convertir
   * @return la cellule iText
   * @throws com.lowagie.text.DocumentException en cas de probleme iText
   */
  private static PdfPCell convertirCellule(Cellule cellule)
      throws DocumentException {
    PdfPCell cellulePDF = null;

    if (cellule.getParagraphe() != null) {
      cellulePDF = new PdfPCell(convertirParagraphe(cellule.getParagraphe()));
    } else if (cellule.getTableau() != null) {
      cellulePDF = new PdfPCell(convertirTableau(cellule.getTableau()));
    }

    float gris = 1f - (cellule.getPourcentageGris() / 100f);
    cellulePDF.setGrayFill(gris);
    cellulePDF.setHorizontalAlignment(convertirAlignement(
        cellule.getAlignementHorizontal()));
    cellulePDF.setVerticalAlignment(convertirAlignement(
        cellule.getAlignementVertical()));
    cellulePDF.setBorderWidth(cellule.getLargeurBordure());
    cellulePDF.setColspan(cellule.getColspan());

    return cellulePDF;
  }

  /**
   * Convertir un alignement des documents visuel en alignement iText.
   * @param alignement l'alignement à convertir
   * @return l'alignement iText
   */
  private static int convertirAlignement(int alignement) {
    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE) {
      return Element.ALIGN_LEFT;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_DROITE) {
      return Element.ALIGN_RIGHT;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_CENTRE) {
      return Element.ALIGN_CENTER;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_JUSTIFIE) {
      return Element.ALIGN_JUSTIFIED;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_BAS) {
      return Element.ALIGN_BOTTOM;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_HAUT) {
      return Element.ALIGN_TOP;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_VERTICAL_MILIEU) {
      return Element.ALIGN_MIDDLE;
    }

    return 0;
  }

  /**
   * Convertir un alignement d'image en alignement iText.
   * @param alignement l'alignement à convertir
   * @return l'alignement iText
   */
  private static int convertirAlignementImage(int alignement) {
    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_GAUCHE) {
      return Element.ALIGN_LEFT;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_DROITE) {
      return Element.ALIGN_RIGHT;
    }

    if (alignement == ConstantesDocumentVisuel.ALIGNEMENT_CENTRE) {
      return Element.ALIGN_CENTER;
    }

    return 0;
  }

  /**
   * Convertir un nom de police en nom de police iText.
   * @param police la police à convertir
   * @return la police iText
   */
  private static int convertirNomPolice(int police) {
    if (police == ConstantesDocumentVisuel.POLICE_COURRIER) {
      return Font.COURIER;
    }

    if (police == ConstantesDocumentVisuel.POLICE_HELVETICA) {
      return Font.HELVETICA;
    }

    if (police == ConstantesDocumentVisuel.POLICE_TIMES_NEW_ROMAN) {
      return Font.TIMES_ROMAN;
    }

    return 0;
  }

  /**
   * Convertir une police d'écriture en police iText
   * @param police la police à convertir
   * @return la police iText
   */
  private static Font convertirPolice(PoliceEcriture police) {
    Font font = null;

    if (police.isGras() && police.isItalique()) {
      font = new Font(convertirNomPolice(police.getNom()), police.getTaille(),
          Font.BOLDITALIC);
    } else if (police.isGras() && !police.isItalique()) {
      font = new Font(convertirNomPolice(police.getNom()), police.getTaille(),
          Font.BOLD);
    } else if (!police.isGras() && police.isItalique()) {
      font = new Font(convertirNomPolice(police.getNom()), police.getTaille(),
          Font.ITALIC);
    } else {
      font = new Font(convertirNomPolice(police.getNom()), police.getTaille(),
          Font.NORMAL);
    }

    font.setColor(police.getCouleur().getRouge(),
        police.getCouleur().getVert(), police.getCouleur().getBleu());

    return font;
  }

  /**
   * Fusionner deux documents PDF dans un seul.
   * @param document1 le premier document à combiner
   * @param document2 le deuxième document à combiner
   * @return le document combiné, sous forme de tableau de bytes
   * @throws java.lang.Exception En cas de problème
   */
  public static byte[] fusionnerDocumentsPDF(byte[] document1, byte[] document2) throws Exception {

    Document documentPDF = null;
    PdfCopy  writer = null;
    PdfReader reader;
    ByteArrayOutputStream donnees = new ByteArrayOutputStream();

    //Pour les documents 1 et 2 on effectue la copie
    for (int i = 0; i < 2; i++) {

      if(i==0) {
        reader = new PdfReader(document1);
      }
      else {
        reader = new PdfReader(document2);
      }

      reader.consolidateNamedDestinations();
      int n = reader.getNumberOfPages();

      if(i==0) {
        documentPDF = new Document(reader.getPageSizeWithRotation(1));
        writer = new PdfCopy(documentPDF, donnees);
        documentPDF.open();
      }

      // step 4: we add content
      PdfImportedPage page;
      for (int j = 0; j < n; ) {
        ++j;
        page = writer.getImportedPage(reader, j);
        writer.addPage(page);
      }
      writer.freeReader(reader);
    }

    documentPDF.close();

    return donnees.toByteArray();

  }


}
