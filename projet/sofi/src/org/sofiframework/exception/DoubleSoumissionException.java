/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.exception;


/**
 * Classe d'exception spécifiant que l'utilisateur à fait un double
 * soumission de formulaire en rafraichissant la page de son navigateur
 * ou encore en reculant d'une page.
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class DoubleSoumissionException extends SOFIException {

  private static final long serialVersionUID = -8254101081304413072L;

  /**
   * Constructeur d'une exception du modele avec un message détaillé.
   */
  public DoubleSoumissionException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception du modele avec un message détaillé et
   * d'une cause.
   * @param message le message détaillé.
   * @param cause la cause de l'erreur.
   */
  public DoubleSoumissionException(String message, Throwable cause) {
    super(message, cause);
  }
}
