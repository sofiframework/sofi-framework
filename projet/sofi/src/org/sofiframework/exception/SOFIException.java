/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.exception;


/**
 * Runtime Exception provenant de SOFI.
 * @author Jean-François Brassard (Nurun inc.)
 */
public class SOFIException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 5468668850192444058L;

  /**
   * Constructeur d'une exception spécifiant une erreur dans SOFI
   * @param message le message de base.
   */
  public SOFIException(Throwable e) {
    super(e);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur dans SOFI
   * @param message le message de base.
   */
  public SOFIException(String message) {
    super(message, null);
  }

  /**
   * Constructeur d'une exception spécifiant une erreur dans SOFI
   * avec le message de base et la cause.
   * @param message le message de base.
   * @param cause la cause.
   */
  public SOFIException(String message, Throwable cause) {
    super(message, cause);
  }
}
