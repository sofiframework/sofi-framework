/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.pagination;

import java.text.MessageFormat;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.displaytag.pagination.NumberedPage;
import org.displaytag.tags.TableTagParameters;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.constante.ConstantesLibelle;
import org.sofiframework.displaytag.tags.TableTag;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.utilitaire.UtilitaireString;


public class Pagination extends org.displaytag.pagination.Pagination {
  private Integer maxPages;
  private String divId;

  public Pagination(String urlFormatString, String divId) {
    this.urlFormat = new MessageFormat(urlFormatString);
    this.divId = divId;
  }

  @Override
  public Integer getMaxPages() {
    if (maxPages.intValue() == 0) {
      return new Integer(1);
    }

    return maxPages;
  }

  @Override
  public void setMaxPages(Integer maxPages) {
    this.maxPages = maxPages;
  }

  /**
   * Returns the appropriate banner for the pagination.
   * @param numberedPageFormat String to be used for a not selected page
   * @param numberedPageSelectedFormat String to be used for a selected page
   * @param numberedPageSeparator separator beetween pages
   * @param fullBanner String basic banner
   * @return String formatted banner whith pages
   */
  public String getFormattedBanner(String numberedPageFormat,
      String numberedPageSelectedFormat, String numberedPageSeparator,
      String fullBanner, String messageListeNavigation, PageContext pageContext) {
    TableTag tableTag = (TableTag) pageContext.getAttribute("tableTag");
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    String cleClient = GestionMessage.getInstance().getFichierConfiguration()
        .get(ConstantesLibelle.INFORMATION_RESULTAT_NB_PAGE,
            request);
    Message messageNbPage = UtilitaireLibelle.getLibelle(cleClient, request,
        null);

    String prefixMultiPage = "Page : ";

    if (tableTag.getPrefixIterateurMultiPage() != null) {
      prefixMultiPage = tableTag.getPrefixIterateurMultiPage();
    }

    StringBuffer buffer = new StringBuffer(100);

    // numbered page list
    Iterator pageIterator = this.pages.iterator();

    Integer pageSelectionne = null;
    int compteurPage = 1;

    while (pageIterator.hasNext()) {
      NumberedPage page = (NumberedPage) pageIterator.next();

      Integer pageNumber = new Integer(page.getNumber());

      StringBuffer url = new StringBuffer(this.urlFormat.format(
          new Object[] { pageNumber }));
      ajouterParametreIndexDivAjax(url, pageContext);

      if (divId != null) {
        String fonctionApresChargement = (String) pageContext.getAttribute(
            "fonctionJSApresChargement");
        StringBuffer lienAjaxLast = new StringBuffer();
        lienAjaxLast.append("javascript:void(0);");
        lienAjaxLast.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(
            url.toString(), divId, null, null, null, fonctionApresChargement,
            pageContext));
        url = lienAjaxLast;
      }

      // needed for MessageFormat : page number/url
      Object[] pageObjects = { pageNumber, url.toString() };

      if (compteurPage == 1) {
        buffer.append(prefixMultiPage);
      }

      // selected page need a different formatter
      if (page.getSelected()) {
        pageSelectionne = pageNumber;
        buffer.append(MessageFormat.format(numberedPageSelectedFormat,
            pageObjects));
      } else {
        buffer.append(MessageFormat.format(numberedPageFormat, pageObjects));
      }

      if (pageIterator.hasNext()) {
        buffer.append(numberedPageSeparator);
      }

      compteurPage++;
    }

    // Modification permettant de ne pas traité la pagination
    // multipage. Dans ce cas-la, seulement une description du statut
    // de la navigation sera affiché. Comme par exemple: page 1 de 2
    String numberedPageString;

    if (!tableTag.isIterateurMultiPage()) {
      Object[] obj = new Object[2];
      obj[0] = pageSelectionne;
      obj[1] = getMaxPages();

      numberedPageString = MessageFormat.format(messageNbPage.getMessage(), obj);
    } else {
      numberedPageString = buffer.toString();
    }

    //  Object array
    //  {0} full String for numbered pages
    //  {1} first page url
    //  {2} previous page url
    //  {3} next page url
    //  {4} last page url
    Object[] pageObjects = new Object[5];
    pageObjects[0] = numberedPageString;

    if (divId != null) {
      pageObjects[1] = getJSAjax(getFirst(), pageContext);
      pageObjects[2] = getJSAjax(getPrevious(), pageContext);
      pageObjects[3] = getJSAjax(getNext(), pageContext);
      pageObjects[4] = getJSAjax(getLast(), pageContext);
    } else {
      pageObjects[1] = this.urlFormat.format(new Object[] { getFirst() });
      pageObjects[2] = this.urlFormat.format(new Object[] { getPrevious() });
      pageObjects[3] = this.urlFormat.format(new Object[] { getNext() });
      pageObjects[4] = this.urlFormat.format(new Object[] { getLast() });
    }

    // return the full banner
    Object[] fullBannerObj = new Object[1];
    fullBannerObj[0] = MessageFormat.format(fullBanner, pageObjects);

    //escaper les aspostrophes est une précondition à l'utilisation de MessageFormat.
    String fullBannerFormat = MessageFormat.format(messageListeNavigation.replaceAll("'", "''"),
        fullBannerObj);

    fullBannerFormat = UtilitaireString.remplacerTous(fullBannerFormat, "*", "'");

    return fullBannerFormat;
  }

  /**
   * Créer un appel AJAX pour afficher une div de facon asynchrone.
   * @return appel AJAX
   * @param pageContext Contexte de la page
   * @param page Index de la page qui doit être présentée par l'appel
   */
  private String getJSAjax(Integer page, PageContext pageContext) {
    String fonctionApresChargement = (String) pageContext.getAttribute(
        "fonctionJSApresChargement");
    StringBuffer urlAjax = new StringBuffer(this.urlFormat.format(
        new Object[] { page }));
    ajouterParametreIndexDivAjax(urlAjax, pageContext);

    StringBuffer jsAjax = new StringBuffer();
    jsAjax.append("javascript:void(0);");
    jsAjax.append(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(
        urlAjax.toString(), divId, null, null, null, fonctionApresChargement,
        pageContext));

    return jsAjax.toString();
  }

  /**
   * Ajoute le paramètre de l'index du DIV de la fenêtre AJAX en cours
   * @param pageContext Contexte de la page
   * @param url URL
   */
  private void ajouterParametreIndexDivAjax(StringBuffer url,
      PageContext pageContext) {
    String indexDivAjax = pageContext.getRequest().getParameter(TableTagParameters.INDEX_DIV_AJAX);

    if (indexDivAjax != null) {
      url.append("&amp;").append(TableTagParameters.INDEX_DIV_AJAX).append("=")
      .append(indexDivAjax);
    }
  }
}
