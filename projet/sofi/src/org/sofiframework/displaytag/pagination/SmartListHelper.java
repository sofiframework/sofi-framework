/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.pagination;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.properties.TableProperties;
import org.sofiframework.displaytag.tags.TableTag;


/**
 * <p>
 * Utility class that chops up a List of objects into small bite size pieces
 * that are more suitable for display.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class SmartListHelper extends org.displaytag.pagination.SmartListHelper {
  private static Log log = LogFactory.getLog(SmartListHelper.class);

  /**
   * La grandeur de la liste virtuel
   */
  private int fullVirtualListSize;
  private String messageListeNavigation;

  /**
   * Creates a SmarListHelper instance that will help you chop up a list
   * into bite size pieces that are suitable for display.
   * @param list List
   * @param size number of items in a page
   * @param tableProperties TableProperties
   */
  public SmartListHelper(List list, int size, TableProperties tableProperties,
      int virtualSize) {
    if ((list == null) || (size < 1)) {
      throw new IllegalArgumentException("Bad arguments passed into " +
          "SmartListHelper() constructor. List=" + list + ", pagesize=" + size);
    }

    this.properties = tableProperties;
    this.pageSize = size;
    this.fullList = list;
    this.fullVirtualListSize = virtualSize;

    if (this.fullList != null) {
      this.fullListSize = ((virtualSize == -1) ? list.size() : virtualSize);
    } else {
      this.fullListSize = 0;
    }

    this.pageCount = computedPageCount();
    this.currentPage = 1;
  }

  /**
   * Returns the index into the master list of the first object that
   * should appear on the current page that the user is viewing.
   * @return int index of the first object that should appear on the current page
   */
  protected int getFirstIndexForCurrentPage() {
    return getFirstIndexForPage(this.currentPage);
  }

  /**
   * Returns the index into the master list of the last object that should
   * appear on the current page that the user is viewing.
   * @return int
   */
  protected int getLastIndexForCurrentPage() {
    return getLastIndexForPage(this.currentPage);
  }

  /**
   * Returns the index into the master list of the first object that
   * should appear on the given page.
   * @param pageNumber page number
   * @return int index of the first object that should appear on the given page
   */
  protected int getFirstIndexForPage(int pageNumber) {
    return (((this.fullVirtualListSize == -1) ? pageNumber : 1) - 1) * this.pageSize;
  }

  /**
   * Returns the index into the master list of the last object that should
   * appear on the given page.
   * @param pageNumber page number
   * @return int index of the last object that should appear on the given page
   */
  protected int getLastIndexForPage(int pageNumber) {
    int firstIndex = getFirstIndexForPage(pageNumber);
    int pageIndex = this.pageSize - 1;
    int lastIndex = ((this.fullVirtualListSize == -1) ? this.fullListSize
        : this.fullList.size()) -
        1;

    return Math.min(firstIndex + pageIndex, lastIndex);
  }

  /**
   * Returns a subsection of the list that contains just the elements that
   * are supposed to be shown on the current page the user is viewing.
   * @return List subsection of the list that contains the elements that are supposed to be shown on the current page
   */
  public List getListForCurrentPage() {
    return getListForPage(this.currentPage);
  }

  /**
   * Returns a subsection of the list that contains just the elements that
   * are supposed to be shown on the given page.
   * @param pageNumber page number
   * @return List subsection of the list that contains just the elements that
   * are supposed to be shown on the given page
   */
  protected List getListForPage(int pageNumber) {
    List list = new ArrayList(this.pageSize + 1);

    int firstIndex = getFirstIndexForPage(pageNumber);
    int lastIndex = getLastIndexForPage(pageNumber);

    Iterator iterator = this.fullList.iterator();
    int j = 0;

    while (iterator.hasNext()) {
      Object object = iterator.next();

      if (j > lastIndex) {
        break;
      } else if (j >= firstIndex) {
        list.add(object);
      }

      j++;
    }

    return list;
  }

  /**
   * Set's the size of the full list according to the virtual list size
   * @param virtualListSize list size
   */
  public void setVirtualListSize(int virtualListSize) {
    this.fullVirtualListSize = virtualListSize;
  }

  /**
   * Return the little summary message that lets the user know how many
   * objects are in the list they are viewing, and where in the list they
   * are currently positioned.  The message looks like:
   *
   * nnn <item(s)> found, displaying nnn to nnn.
   *
   * <item(s)> is replaced by either itemName or itemNames depending on if
   * it should be signular or plural.
   * @return String
   */
  public String getSearchResultsSummary(PageContext pageContext) {
    //    if (this.fullListSize == 0) {
    //      Object[] objs = { this.properties.getPagingItemsName() };
    //
    //      return MessageFormat.format(this.properties.getPagingFoundNoItems(), objs);
    //    } else if (this.fullListSize == 1) {
    //      Object[] objs = { this.properties.getPagingItemName() };
    //
    //      return MessageFormat.format(this.properties.getPagingFoundOneItem(), objs);
    //    } else if (computedPageCount() == 1) {
    //      Object[] objs = {
    //          new Integer(this.fullListSize), this.properties.getPagingItemsName(),
    //          this.properties.getPagingItemsName()
    //        };
    //
    //      return MessageFormat.format(this.properties.getPagingFoundAllItems(), objs);
    //    } else {
    //      Object[] objs = {
    //          new Integer(this.fullListSize), this.properties.getPagingItemsName(),
    //          new Integer(getFirstIndexForCurrentPage() + 1),
    //          new Integer(getLastIndexForCurrentPage() + 1)
    //        };
    //
    //      return MessageFormat.format(this.properties.getPagingFoundSomeItems(),
    //        objs);
    //    }

    //Toujours retourner vide car fonctionnalités non supporté par SOFI.
    //jfbrassard - 2010-04-02

    return "";
  }

  /**
   * Returns a string containing the nagivation bar that allows the user
   * to move between pages within the list.
   *
   * The urlFormatString should be a URL that looks like the following:
   *
   * somepage.page?page={0}
   * @param urlFormatString String
   * @return String
   */
  public String getPageNavigationBar(String urlFormatString,
      PageContext pageContext, String divId) {
    TableTag tableTag = (TableTag) pageContext.getAttribute("tableTag");

    int maxPages = tableTag.getNombreMaximalMultiPage();

    int currentIndex = this.currentPage;
    int count = this.pageCount;
    int startPage = 1;
    int endPage = maxPages;

    Pagination pagination = new Pagination(urlFormatString, divId);

    pagination.setMaxPages(new Integer(pageCount));

    // if no items are found still add pagination?
    if (count == 0) {
      pagination.addPage(1, true);
    }

    if (currentIndex < maxPages) {
      startPage = 1;
      endPage = maxPages;

      if (count < endPage) {
        endPage = count;
      }
    } else {
      startPage = currentIndex;

      while ((startPage + maxPages) > (count + 1)) {
        startPage--;
      }

      endPage = startPage + (maxPages - 1);
    }

    if (currentIndex != 1) {
      pagination.setFirst(new Integer(1));
      pagination.setPrevious(new Integer(currentIndex - 1));
    }

    for (int j = startPage; j <= endPage; j++) {
      pagination.addPage(j, (j == currentIndex));
    }

    if (currentIndex != count) {
      pagination.setNext(new Integer(currentIndex + 1));
      pagination.setLast(new Integer(count));
    }

    // format for previous/next banner
    String bannerFormat;

    if (pagination.isOnePage()) {
      bannerFormat = this.properties.getPagingBannerOnePage();
    } else if (pagination.isFirst()) {
      bannerFormat = this.properties.getPagingBannerFirst();
    } else if (pagination.isLast()) {
      bannerFormat = this.properties.getPagingBannerLast();
    } else {
      bannerFormat = this.properties.getPagingBannerFull();
    }

    return pagination.getFormattedBanner(this.properties.getPagingPageLink(),
        this.properties.getPagingPageSelected(),
        this.properties.getPagingPageSeparator(), bannerFormat,
        getMessageListeNavigation(), pageContext);
  }

  public void setMessageListeNavigation(String newMessageListeNavigation) {
    this.messageListeNavigation = newMessageListeNavigation;
  }

  public String getMessageListeNavigation() {
    return messageListeNavigation;
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("fullList",
        this.fullList).append("fullListSize", this.fullListSize)
        .append("fullVirtualListSize",
            this.fullVirtualListSize).append("pageSize", this.pageSize)
            .append("pageCount",
                this.pageCount).append("properties", this.properties)
                .append("currentPage",
                    this.currentPage).toString();
  }
}
