/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.displaytag.model.Cell;
import org.displaytag.model.ColumnIterator;
import org.displaytag.model.TableModel;
import org.displaytag.util.TagConstants;


/**
 * Contient les informations d'un rangée d'une table.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class Row {
  private boolean selectionner;

  /**
   * Object holding values for the current row.
   */
  private Object rowObject;

  /**
   * List of cell objects.
   */
  private List staticCells;

  /**
   * Row number.
   */
  private int rowNumber;

  /**
   * TableModel which the row belongs to.
   */
  private TableModel tableModel;

  /**
   * Constructor for Row.
   * @param object Object
   * @param number int
   */
  public Row(Object object, int number) {
    this.rowObject = object;
    this.rowNumber = number;
    this.staticCells = new ArrayList();
  }

  /**
   * Setter for the row number.
   * @param number row number
   */
  public void setRowNumber(int number) {
    this.rowNumber = number;
  }

  /**
   * @return true if the current row number is odd
   */
  public boolean isOddRow() {
    return (this.rowNumber % 2) == 0;
  }

  /**
   * Getter for the row number.
   * @return row number
   */
  public int getRowNumber() {
    return this.rowNumber;
  }

  /**
   * Adds a cell to the row.
   * @param cell Cell
   */
  public void addCell(Cell cell) {
    this.staticCells.add(cell);
  }

  /**
   * getter for the list of Cell object.
   * @return List containing Cell objects
   */
  public List getCellList() {
    return this.staticCells;
  }

  /**
   * getter for the object holding values for the current row.
   * @return Object object holding values for the current row
   */
  public Object getObject() {
    return this.rowObject;
  }

  /**
   * Iterates on columns.
   * @param columns List
   * @return ColumnIterator
   */
  public ColumnIterator getColumnIterator(List columns) {
    return new ColumnIterator(columns, this);
  }

  /**
   * Setter for the table model the row belongs to.
   * @param table TableModel
   */
  public void setParentTable(TableModel table) {
    this.tableModel = table;
  }

  /**
   * Getter for the table model the row belongs to.
   * @return TableModel
   */
  public TableModel getParentTable() {
    return this.tableModel;
  }

  /**
   * Writes the open &lt;tr> tag.
   * @return String &lt;tr> tag with the appropriate css class attribute
   */
  public String getOpenTag() {
    String css = null;

    if (selectionner) {
      css = "ligneSelectionnee";
    } else {
      css = this.tableModel.getProperties().getCssRow(this.rowNumber);
    }

    if (StringUtils.isNotBlank(css)) {
      return TagConstants.TAG_OPEN + TagConstants.TAGNAME_ROW + " " +
          TagConstants.ATTRIBUTE_CLASS + "=\"" + css + "\"" +
          TagConstants.TAG_CLOSE;
    } else {
      return TagConstants.TAG_OPEN + TagConstants.TAGNAME_ROW +
          TagConstants.TAG_CLOSE;
    }
  }

  /**
   * writes the &lt;/tr> tag.
   * @return String &lt;/tr> tag
   */
  public String getCloseTag() {
    return TagConstants.TAG_TR_CLOSE;
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE).append("rowNumber",
        this.rowNumber).append("rowObject", this.rowObject).toString();
  }

  public void setSelectionner(boolean selectionner) {
    this.selectionner = selectionner;
  }

  public boolean isSelectionner() {
    return selectionner;
  }
}
