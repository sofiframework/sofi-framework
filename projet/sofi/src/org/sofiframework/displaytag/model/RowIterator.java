/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.model;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.decorator.TableDecorator;


/**
 * Itérateur d'une rangée d'une table
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public class RowIterator {
  /**
   * logger.
   */
  private static Log log = LogFactory.getLog(RowIterator.class);

  /**
   * internal iterator for Rows.
   */
  private Iterator iterator;

  /**
   * row number counter.
   */
  private int rowNumber;

  /**
   * reference to the table TableDecorator.
   */
  private TableDecorator decorator;

  /**
   * id inherited from the TableTag (needed only for logging).
   */
  private String id;

  /**
   * Constructor for RowIterator.
   * @param rowList List containing Row objects
   * @param columnList List containing CellHeader objects
   * @param tableDecorator TableDecorator
   */
  public RowIterator(List rowList, List columnList,
      TableDecorator tableDecorator) {
    this.iterator = rowList.iterator();
    this.rowNumber = 0;
    this.decorator = tableDecorator;
  }

  /**
   * Setter for the tablemodel id.
   * @param tableId same id of table tag, needed for logging
   */
  public void setId(String tableId) {
    this.id = tableId;
  }

  /**
   * Check if a next row exist.
   * @return boolean true if a new row
   */
  public boolean hasNext() {
    return this.iterator.hasNext();
  }

  /**
   * Returns the next row object.
   * @return Row
   */
  public Row next() {
    int currentRowNumber = this.rowNumber++;

    Object object = this.iterator.next();

    Row row = (Row) object;

    row.setRowNumber(currentRowNumber);

    if (this.decorator != null) {
      this.decorator.initRow(row.getObject(), currentRowNumber, currentRowNumber);
    }

    return row;
  }
}
