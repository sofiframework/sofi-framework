/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.model;

import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.decorator.DateColumnDecorator;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.exception.ObjectLookupException;
import org.displaytag.model.Cell;
import org.displaytag.model.HeaderCell;
import org.displaytag.util.Anchor;
import org.displaytag.util.Href;
import org.displaytag.util.HtmlAttributeMap;
import org.displaytag.util.HtmlTagUtil;
import org.displaytag.util.LinkUtil;
import org.displaytag.util.LookupUtil;
import org.displaytag.util.TagConstants;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.base.BaliseValeurCache;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.utilitaire.UtilitaireSession;
import org.sofiframework.utilitaire.UtilitaireNombre;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Représentation d'un colonne pour une liste.
 * 
 * @author Jean-Francois Brassard
 * @version 1.0
 */
public class Column {
  /** L'instance de journalisation pour cette classe. */
  private Log log = LogFactory.getLog(Column.class);

  /** Row this column belongs to. */
  private Row row;

  /**
   * Header of this column. The header cell contains all the attributes common to all cells in the same column
   */
  private HeaderCell header;

  /**
   * copy of the attribute map from the header cell. Needed to change attributes (title) in this cell only
   */
  private HtmlAttributeMap htmlAttributes;

  /** contains the evaluated body value. Filled in getOpenTag. */
  private String stringValue;

  /** Cell. */
  private Cell cell;

  /**
   * Constructor for Column.
   * 
   * @param headerCell
   *          HeaderCell
   * @param currentCell
   *          Cell
   * @param parentRow
   *          Row
   */
  public Column(HeaderCell headerCell, Cell currentCell, Row parentRow) {
    this.header = headerCell;
    this.row = parentRow;
    this.cell = currentCell;

    // also copy html attributes
    this.htmlAttributes = headerCell.getHtmlAttributes();

    // cell's attributes have precedence on header's attributes.
    this.htmlAttributes.putAll(currentCell.getAttributeMap());
  }

  /**
   * Gets the value, after calling the table / column decorator is requested.
   * 
   * @param decorated
   *          boolean
   * 
   * @return Object
   * 
   * @throws ObjectLookupException
   *           for errors in bean property lookup
   * @throws DecoratorException
   *           if a column decorator is used and an exception is thrown during value decoration
   */
  public Object getValue(boolean decorated) throws ObjectLookupException, DecoratorException {
    if (log.isDebugEnabled()) {
      log.debug("Début de l'extraction de la valeur dans la colonne");
    }

    // a static value has been set?
    if (this.cell.getStaticValue() != null) {
      return this.cell.getStaticValue();
    }

    Object object = null;
    TableDecorator tableDecorator = this.row.getParentTable().getTableDecorator();

    /*
     * Ajout par JMP, lorsqu'aucun décorateur n'est spécifié, on déduit un décorateur par défault à partir du type de
     * donnée.
     */
    if (this.header.getColumnDecorator() == null) {
      Object aDecorer = null;

      try {
        aDecorer = UtilitaireObjet.getValeurAttribut(this.row.getObject(), this.header.getBeanPropertyName());
      } catch (Exception e) {
        // rien faire
      }

      if (aDecorer != null) {
        if (aDecorer instanceof java.util.Date) {
          this.header.setColumnDecorator(new DateColumnDecorator());
        }

        if (header.getFormat() != null) {
          String valeurFormatte = UtilitaireNombre.getValeurFormate(header.getFormat(), aDecorer,
              UtilitaireSession.getLocale(header.getPageContext().getSession()), true);

          return valeurFormatte;
        }
      }
    }

    // Si un decorateur de configure, et si ce decorateur a un get pour la propriete seulement, verifier le decorateur
    if (decorated && (tableDecorator != null) && tableDecorator.hasGetterFor(this.header.getBeanPropertyName())) {
      object = UtilitaireObjet.getValeurAttribut(tableDecorator, this.header.getBeanPropertyName());
    } else {
      // Extraire la valeur de l'objet en cours.
      object = UtilitaireObjet.getValeurAttribut(this.row.getObject(), this.header.getBeanPropertyName());
    }

    if (decorated && (this.header.getColumnDecorator() != null)) {
      object = this.header.getColumnDecorator().decorate(object);
    }

    if ((object == null) || object.equals("null")) {
      if (!this.header.getShowNulls()) {
        object = "";
      }
    }

    // Traiter si objet cache de configuré.
    if (this.header.getCache() != null) {
      String nomCache = this.header.getCache();

      String etiquette = "";

      // Gérer les filtres de cache
      ObjetCache objetCache = GestionCache.getInstance().getCache(nomCache, this.header.getSousCache());
      Map collection = null;

      if (objetCache != null) {
        BaliseValeurCache baliseCache = new ColonneCache(object);
        Object valeurCache = null;
        try {
          collection = UtilitaireBaliseJSP.appliquerFiltreCache(objetCache.getCollection(), this.header.getFiltre(),
              baliseCache, this.getHeader().getPageContext());
          valeurCache = collection.get(object);
        } catch (JspException e) {
          throw new SOFIException("Erreur pour filtrer le cache sur la colonne.", e);
        }

        if (DomaineValeur.class.isInstance(valeurCache)) {
          DomaineValeur domaineValeur = (DomaineValeur) valeurCache;
          Locale locale = UtilitaireBaliseJSP.getLocale(getHeader().getPageContext());

          if ((domaineValeur.getListeDescriptionLangue() != null)
              && (domaineValeur.getListeDescriptionLangue().size() > 0)) {
            etiquette = (String) domaineValeur.getListeDescriptionLangue().get(locale.toString());
          } else {
            etiquette = domaineValeur.getDescription();
          }

        } else if (valeurCache instanceof String) {
          etiquette = valeurCache.toString();
        } else {
          if (getHeader().getProprieteAffichage() != null) {
            try {
              etiquette = PropertyUtils.getProperty(valeurCache, getHeader().getProprieteAffichage()).toString();
            } catch (Exception ex) {
              throw new SOFIException("Erreur pour obtenir la valeur de la propriété "
                  + getHeader().getProprieteAffichage() + " de l'objet de classe " + valeurCache.getClass().getName());
            }
          }
        }

        if (valeurCache != null) {
          StringTokenizer listeToken = new StringTokenizer(etiquette, ".");

          if ((listeToken != null) && (listeToken.countTokens() > 3)) {
            // Traitement pour libellé
            object = UtilitaireLibelle.getLibelle(etiquette, header.getPageContext(), null).getMessage();
          } else {
            object = etiquette;
          }
        }
      }
    }

    // Traitement de la chaine de caractère si trop longue selon la spécification
    // de la colonne.
    if (header.getSautLigneNombreCararactereMaximal() != null) {
      object = UtilitaireString.traiterChaineCaractereTropLongueAvecSautLigneHTML(object.toString(),
          new Integer(header.getSautLigneNombreCararactereMaximal()), header.getSautLigneCararactereSeparateur());
    }

    if ((object != null) && header.isConvertirEnHtml()) {
      // Traitement pour convertir le texte en format HTML, afin de renforcir la sécurité
      object = UtilitaireString.convertirEnHtml(object.toString());

      // permettre les retour de ligne seulement.
      object = UtilitaireString.remplacerTous(object.toString(), "&lt;br/&gt;", "<br/>");
    }

    if (log.isDebugEnabled()) {
      log.debug("Valeur de la colonne : " + object);
    }

    return object;
  }

  /**
   * Generates the cell open tag.
   * 
   * @param contexte
   *          le contexte de la page.
   * 
   * @return String td open tag
   * 
   * @throws ObjectLookupException
   *           for errors in bean property lookup
   * @throws DecoratorException
   *           if a column decorator is used and an exception is thrown during value decoration
   */
  public String getOpenTag(PageContext contexte) throws ObjectLookupException, DecoratorException {
    if (log.isDebugEnabled()) {
      log.debug("Acces : getOpenTag");
    }

    this.stringValue = createChoppedAndLinkedValue(contexte);

    return HtmlTagUtil.createOpenTagString(TagConstants.TAGNAME_COLUMN, this.htmlAttributes);
  }

  /**
   * Generates the cell close tag (&lt;/td>).
   * 
   * @return String td closing tag
   */
  public String getCloseTag() {
    this.stringValue = null;

    return this.header.getCloseTag();
  }

  /**
   * Calculates the cell content, cropping or linking the value as needed.
   * 
   * @param contexte
   *          Le contexte de la page
   * 
   * @return String
   * 
   * @throws ObjectLookupException
   *           for errors in bean property lookup
   * @throws DecoratorException
   *           if a column decorator is used and an exception is thrown during value decoration
   */
  public String createChoppedAndLinkedValue(PageContext contexte) throws ObjectLookupException, DecoratorException {
    if (log.isDebugEnabled()) {
      log.debug("Accès : createChoppedAndLinkedValue.");
    }

    Object choppedValue = getValue(true);

    boolean isChopped = false;
    String fullValue = "";

    if (choppedValue != null) {
      fullValue = choppedValue.toString();
    }

    // trim the string if a maxLength or maxWords is defined
    if ((this.header.getMaxLength() > 0) && (fullValue.length() > this.header.getMaxLength())) {
      choppedValue = StringUtils.abbreviate(fullValue, this.header.getMaxLength() + 3);
      isChopped = true;
    } else if (this.header.getMaxWords() > 0) {
      StringBuffer buffer = new StringBuffer();
      StringTokenizer tokenizer = new StringTokenizer(fullValue);
      int tokensNum = tokenizer.countTokens();

      if (tokensNum > this.header.getMaxWords()) {
        int wordsCount = 0;

        while (tokenizer.hasMoreTokens() && (wordsCount < this.header.getMaxWords())) {
          buffer.append(tokenizer.nextToken() + " ");
          wordsCount++;
        }

        buffer.append("...");
        choppedValue = buffer;
        isChopped = true;
      }
    }

    // chopped content? add the full content to the column "title" attribute
    if (isChopped) {
      // clone the attribute map, don't want to add title to all the columns
      this.htmlAttributes = (HtmlAttributeMap) this.htmlAttributes.clone();

      // add title
      this.htmlAttributes.put(TagConstants.ATTRIBUTE_TITLE, fullValue);
    }

    // Are we supposed to set up a link to the data being displayed in this column...
    if (this.header.getAutoLink()) {
      choppedValue = LinkUtil.autoLink(choppedValue.toString());
    } else {
      if (this.header.getHref() != null) {
        // générer le href pour le lien hypertexte.
        Href colHref = getColumnHref(fullValue, contexte);
        Anchor anchor = new Anchor(colHref, choppedValue.toString(), null);
        choppedValue = anchor.toString();
      } else {
        // Vérifier si c'est une génération automique d'un appel d'une fonction javascript
        // généré automatiquement.
        if (this.header.isFonctionRetourValeur() || this.header.isFonctionRetourValeurAjax()) {
          // générer le href pour le lien hypertexte.
          StringBuffer lienJS = new StringBuffer();
          lienJS.append("javascript:");

          Href colHref = null;

          if (this.header.isFonctionRetourValeur()) {
            lienJS.append(UtilitaireBaliseJSP.NOM_FONCTION_RETOUR_VALEUR);
            lienJS.append("();");
            colHref = new Href(lienJS.toString());
            colHref = genererHrefPourFonctionJS(colHref);
          }

          if (this.header.isFonctionRetourValeurAjax()) {
            /*
             * lienJS.append(UtilitaireBaliseJSP.genererFonctionRetourValeurs( contexte, true));
             */
            lienJS.append(UtilitaireBaliseJSP.genererFonctionRetourValeursAjax(contexte));
            Anchor anchor = new Anchor(remplacerParametresPourFonctionJS(lienJS), choppedValue.toString(), null, true);
            anchor.setId("lien_liste_" + this.header.getColumnNumber() + this.row.getRowNumber());
            choppedValue = anchor.toString();
          } else {
            Anchor anchor = new Anchor(colHref, choppedValue.toString(), null);
            anchor.setId("lien_liste_" + this.header.getColumnNumber() + this.row.getRowNumber());
            choppedValue = anchor.toString();
          }
        }
      }
    }

    if (choppedValue != null) {
      return choppedValue.toString();
    }

    return null;
  }

  private String remplacerParametresPourFonctionJS(StringBuffer lienJS) {
    StringTokenizer tokenizer = new StringTokenizer(this.header.getParamName(), ",");
    String texte = lienJS.toString();

    for (int index = 0; tokenizer.hasMoreTokens(); index++) {
      String jeton = tokenizer.nextToken().trim();
      String valeur = getValeurParametreJS(jeton);
      String valeurJS = UtilitaireString.traiterApostrophe(valeur, true);

      if (valeur != null) {
        valeurJS = valeurJS.replaceAll("\\$", "\\\\\\$");
        texte = texte.replaceAll("[{][" + index + "][}]", valeurJS);
      }
    }

    return texte;
  }

  private String getValeurParametreJS(String nomParametre) {
    Object valeur = null;

    try {
      valeur = LookupUtil.getBeanProperty(this.row.getObject(), nomParametre);
    } catch (Exception ex) {
      // Si la valeur n'existe pas ou est null, il faut mettre '' en paramètre à la fonction
      if (log.isWarnEnabled()) {
        log.warn("Il se peut qu'un paramètre soit invalide ou null.", ex);
      }
    }

    if (valeur == null) {
      valeur = "";
    } else {
      valeur = StringEscapeUtils.escapeXml(valeur.toString());

      // @todo Patch pour remettre les apostrophes afin de convertir en Javascript plus tard.
      valeur = UtilitaireString.remplacerTous(valeur.toString(), "&apos;", "'");
    }

    return "'" + UtilitaireString.traiterApostrophe(valeur.toString(), true) + "'";
  }

  /**
   * Generates the href for the column using paramName/property/scope.
   * 
   * @param columnContent
   *          column body
   * @param contexte
   *          le contexte de la page
   * 
   * @return generated Href
   * 
   * @throws ObjectLookupException
   *           for errors in lookin up object properties
   */
  private Href getColumnHref(String columnContent, PageContext contexte) throws ObjectLookupException {
    // copy href
    Href colHref = new Href(this.header.getHref());

    // do we need to add a param?
    if (this.header.getParamName() != null) {
      if ((colHref.getUrl().indexOf("javascript") >= 0) || (colHref.getUrl().indexOf("javascript".toUpperCase()) >= 0)) {
        return genererHrefPourFonctionJS(colHref);
      } else {
        Object paramValue = null;

        if (this.header.getParamProperty() != null) {
          String tempParamName = this.header.getParamName();
          String tempParamValue = this.header.getParamProperty();

          for (StringTokenizer tkName = new StringTokenizer(tempParamName, ","), tkValue = new StringTokenizer(
              tempParamValue, ","); tkName.hasMoreTokens();) {
            String tokenName = tkName.nextToken().trim();
            String tokenValue = tkValue.nextToken().trim();

            paramValue = LookupUtil.getBeanProperty(this.row.getObject(), tokenValue);

            colHref.addParameter(tokenName, paramValue);
          }
        }
      }
    }

    return colHref;
  }

  private Href genererHrefPourFonctionJS(Href colHref) {
    // Si fonction javascript dans le HREF
    StringBuffer buffer = new StringBuffer();

    for (StringTokenizer tokenizer = new StringTokenizer(this.header.getParamName(), ","); tokenizer.hasMoreTokens();) {
      String jeton = tokenizer.nextToken().trim();

      if (buffer.length() > 0) {
        buffer.append(", ");
      }

      buffer.append(getValeurParametreJS(jeton));
    }

    String url = colHref.getUrl();
    colHref.setUrl(url.substring(0, url.indexOf("(") + 1) + buffer.toString() + url.substring(url.lastIndexOf(")")));

    return colHref;
  }

  /**
   * get the final value to be displayed in the table. This method can only be called after getOpenTag(), where the
   * content is evaluated
   * 
   * @return String final value to be displayed in the table
   */
  public String getChoppedAndLinkedValue() {
    return this.stringValue;
  }

  /**
   * returns the grouping order of this column or -1 if the column is not grouped.
   * 
   * @return int grouping order of this column or -1 if the column is not grouped
   */
  public int getGroup() {
    return this.header.getGroup();
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cell", this.cell)
        .append("header", this.header).append("htmlAttributes", this.htmlAttributes)
        .append("stringValue", this.stringValue).toString();
  }

  /**
   * Retourne le détail de l'entete
   * 
   * @return le détail de l'entete
   */
  public HeaderCell getHeader() {
    return header;
  }

  /**
   * Fixer le détail de l'entete
   * 
   * @param header
   *          le détail de l'entete
   */
  public void setHeader(HeaderCell header) {
    this.header = header;
  }

  /**
   * Classe qui décrit les propriétés de cache de la cellule de la table.
   */
  class ColonneCache implements BaliseValeurCache {

    private Object valeur = null;

    public ColonneCache(Object valeur) {
      this.valeur = valeur;
    }

    @Override
    public String getNomCache() {
      return getHeader().getCache();
    }

    @Override
    public Object getSousCache() {
      return getHeader().getSousCache();
    }

    @Override
    public Object getValeurCache() {
      return this.valeur;
    }

  }
}
