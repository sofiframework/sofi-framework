/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.tags.el;

import javax.servlet.jsp.JspException;

import org.displaytag.tags.el.ExpressionEvaluator;


/**
 * Adds EL support to displaytag's ColumnTag. Also supports a new "titleKey" property that works the same as
 * fmt:message's key property. This tag must be the descendant of a fmt:bundle tag in order to use the titleKey. This
 * is just a shortcut, which makes
 *
 * <pre>
 * &lt;display:column titleKey="bar"/&gt;
 * </pre>
 *
 * behave the same as
 *
 * <pre>
 * &lt;c:set var="foo"&gt;&lt;fmt:message key="bar"/&gt;&lt;/c:set&gt;
 * &lt;display:column title="${foo}"/&gt;
 * </pre>
 *
 * If you don't define a title or a titleKey property on your column, first the tag will attempt to look up the
 * property property in your ResourceBundle. Failing that, it will fall back to the parent class's behavior of just
 * using the property name.
 * @author Tim McCune
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class ColumnTag extends org.displaytag.tags.ColumnTag {

  private static final long serialVersionUID = 2482009361294089801L;

  private String parametresFonction = null;
  private String fonction = null;

  public ColumnTag() {
    super();
  }

  @Override
  public int doStartTag() throws JspException {
    evaluateExpressions();

    return super.doStartTag();
  }

  private void evaluateExpressions() throws JspException {
    ExpressionEvaluator eval = new ExpressionEvaluator(this, pageContext);
    String s;

    if ((getFonction() != null) && (getFonction().trim().length() > 0)) {
      this.setFonctionHref(getFonction());
    }

    if ((s = eval.evalString("paramId", getParamId())) != null) {
      super.setParamId(s);
    }

    if ((s = eval.evalString("paramProperty", getParamProperty())) != null) {
      super.setParamProperty(s);
    }

    s = eval.evalString("property", getProperty());
    super.setProperty(s);

    if ((s = eval.evalString("aideContextuelle", getAideContextuelle())) != null) {
      super.setAideContextuelle(s);
    }

    if (getParametresFonction() != null) {
      super.setParamId((String) eval.eval("_parametresFonction",
          getParametresFonction(), String.class));
    }
  }

  /**
   * Fixe l'url avec la valeur de la focntion javascript.
   * @param valeur de la fonction javascript
   */
  public void setFonctionHref(String value) {
    this.href = new org.displaytag.util.Href(value);
  }

  public String getFonction() {
    return fonction;
  }

  public void setFonction(String fonction) {
    this.fonction = fonction;
  }

  public String getParametresFonction() {
    return parametresFonction;
  }

  public void setParametresFonction(String parametresFonction) {
    this.parametresFonction = parametresFonction;
  }
}
