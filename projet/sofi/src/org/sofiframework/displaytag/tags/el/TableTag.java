/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.tags.el;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.exception.InvalidTagAttributeValueException;
import org.displaytag.tags.el.ExpressionEvaluator;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.utilitaire.UtilitaireObjet;


/**
 * Ajout des expressions régulières (EL) pour la balise TableTag.
 * Intégration du composant de liste de SOFI (ListeNavigation).
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 * @see org.sofiframework.composantweb.ListeNavigation
 */
public class TableTag extends org.sofiframework.displaytag.tags.TableTag {
  /**
   * 
   */
  private static final long serialVersionUID = 5572798019468286017L;
  private Log log = LogFactory.getLog(TableTag.class);
  protected String _virtualSize;
  protected String _decorator;
  protected String _defaultsort;
  protected Object _name;
  protected String _pagesize;
  protected String _requestURI;
  protected String _sort;
  protected String selection;
  protected String colonneLienDefaut;
  protected Object ligneSelectionnee;
  protected Object liste;

  public TableTag() {
    super();
    init();
  }

  @Override
  public String getDecorator() {
    return _decorator;
  }

  @Override
  public void setDecorator(String s) {
    _decorator = s;
  }

  @Override
  public Object getName() {
    return _name;
  }

  public void setName(Object s) {
    _name = s;
  }

  public String getPageSize() {
    return _pagesize;
  }

  @Override
  public void setPagesize(String s) {
    _pagesize = s;
  }

  @Override
  public void setVirtualSize(String s) {
    _virtualSize = s;
  }

  @Override
  public String getRequestURI() {
    return _requestURI;
  }

  @Override
  public void setRequestURI(String s) {
    _requestURI = s;
  }

  public String getSort() {
    return _sort;
  }

  @Override
  public void setSort(String s) {
    _sort = s;
  }

  public String getSelection() {
    return selection;
  }

  public void setSelection(String selection) {
    this.selection = selection;
  }

  @Override
  public String getColonneLienDefaut() {
    return colonneLienDefaut;
  }

  @Override
  public void setColonneLienDefaut(String colonneLienDefaut) {
    this.colonneLienDefaut = colonneLienDefaut;
  }

  private void init() {
    _decorator = null;
    _name = null;
    _pagesize = null;
    _requestURI = null;
    _sort = null;
    _virtualSize = null;
  }

  @Override
  public void release() {
    super.release();
    init();
  }

  @Override
  public int doStartTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    } else {
      if (getName() == null) {
        liste = new ArrayList();
      } else {
        setListe(getName());
      }

      super.setDecorator(_decorator);

      if (getPageSize() != null) {
        super.setPagesize(getPageSize());
      }

      super.setRequestURI(_requestURI);

      if (_sort != null) {
        super.setSort(_sort);
      }

      if (_virtualSize != null) {
        super.setVirtualSize(_virtualSize);
      }
    }

    fixerSpecification();

    String nomListeLigneSelectionne = this.getDivId() + "_ligneSelectionnee";
    HashMap parametresCleListe = (HashMap) pageContext.getSession()
        .getAttribute(nomListeLigneSelectionne);

    setListeParametresCleSelection(parametresCleListe);

    if (this.selection != null) {
      if (getSelection().indexOf("$") != -1) {
        ExpressionEvaluator eval = new ExpressionEvaluator(this, pageContext);
        ligneSelectionnee = eval.eval("selection", selection, Object.class);
      }
    }

    return super.doStartTag();
  }

  @Override
  protected void completerIteration() {
    Object objetCourant = this.currentRow.getObject();

    boolean isSelectionne = false;

    if ((this.currentRow != null) && (ligneSelectionnee != null) &&
        objetCourant.equals(ligneSelectionnee)) {
      isSelectionne = true;
    }

    if (getListeParametresCleSelection() != null) {
      isSelectionne = true;

      Iterator iterateur = getListeParametresCleSelection().keySet().iterator();

      while (iterateur.hasNext()) {
        String cleLigneSelectionne = (String) iterateur.next();
        String valeurCle = getListeParametresCleSelection()
            .get(cleLigneSelectionne).toString();
        String valeurObjetCourant = null;

        try {
          valeurObjetCourant = UtilitaireObjet.getValeurAttribut(objetCourant,
              cleLigneSelectionne).toString();
        } catch (Exception e) {
        }

        isSelectionne = isSelectionne && valeurCle.equals(valeurObjetCourant);
      }
    }

    this.currentRow.setSelectionner(isSelectionne);
  }

  @Override
  protected void evaluerEL() throws JspException {
    ExpressionEvaluator eval = new ExpressionEvaluator(this, pageContext);
    String s;

    // Évaluer le nom de la liste.
    Object liste = eval.eval("name", (String) _name, Object.class);

    if (liste == null) {
      liste = new ArrayList();
    }

    if ((s = eval.evalString("decorator", _decorator)) != null) {
      super.setDecorator(s);
    }

    if ((s = eval.evalString("pagesize", _pagesize)) != null) {
      super.setPagesize(s);
    }

    if ((s = eval.evalString("requestURI", _requestURI)) != null) {
      super.setRequestURI(s);
    }

    if ((s = eval.evalString("sort", _sort)) != null) {
      super.setSort(s);
    }

    // Modifier par J-F
    if ((s = eval.evalString("virtualSize", _virtualSize)) != null) {
      super.setVirtualSize(s);
    }

    setListe(liste);

    super.evaluerEL();
  }

  @Override
  protected void fixerSpecification() {
    ListeNavigation listeNavigation = null;

    if (ListeNavigation.class.isInstance(liste)) {
      listeNavigation = (ListeNavigation) liste;

      try {
        super.setVirtualSize(String.valueOf(listeNavigation.getNbListe()));
      } catch (InvalidTagAttributeValueException e) {
        // Exception impossible!!!
      }

      try {
        this.list = listeNavigation.getListe();
        setListeNavigation(listeNavigation);

        // Fixer l'ordre de tri par défaut
        if (getListeNavigation().getOrdreTri() != null) {
          if (getListeNavigation().getOrdreTri().equals(ListeNavigation.TRI_ASCENDANT)) {
            setDefaultorder("ascending");
          } else {
            setDefaultorder("descending");
          }
        } else {
          setDefaultorder("ascending");
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      setListeNavigation(listeNavigation);
    } else {
      this.list = liste;
    }

    if (log.isDebugEnabled()) {
      if (getListe() instanceof ListeNavigation) {
        ListeNavigation liste = (ListeNavigation) getListe();

        if (liste.getListe() != null) {
          log.debug("Nombre dans la liste :" + liste.getListe().size());
        } else {
          log.debug("Nombre dans la liste : 0");
        }
      } else {
        List liste = (List) getListe();
        log.debug("Nombre dans la liste :" + liste.size());
      }
    }

    if ((getPageSize() == null) && (listeNavigation != null)) {
      try {
        super.setPagesize(listeNavigation.getMaxParPage().toString());
      } catch (Exception e) {
        throw new SOFIException("Le maximum par page n'est pas spécifié");
      }
    }

    super.fixerSpecification();
  }

  @Override
  public int doEndTag() throws JspException {
    super.doEndTag();

    return EVAL_PAGE;
  }

  public void setListe(Object liste) {
    this.liste = liste;
  }

  public Object getListe() {
    return liste;
  }
}
