/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.displaytag.tags;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.decorator.DecoratorFactory;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.exception.InvalidTagAttributeValueException;
import org.displaytag.exception.ObjectLookupException;
import org.displaytag.model.ColumnIterator;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.TableModel;
import org.displaytag.properties.MediaTypeEnum;
import org.displaytag.properties.SortOrderEnum;
import org.displaytag.properties.TableProperties;
import org.displaytag.tags.TableTagExtraInfo;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.Anchor;
import org.displaytag.util.CollectionUtil;
import org.displaytag.util.Href;
import org.displaytag.util.RequestHelper;
import org.displaytag.util.RequestHelperFactory;
import org.displaytag.util.TagConstants;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.UtilitaireMessage;
import org.sofiframework.application.message.UtilitaireMessageFichier;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesBaliseJSP;
import org.sofiframework.displaytag.model.Column;
import org.sofiframework.displaytag.model.Row;
import org.sofiframework.displaytag.model.RowIterator;
import org.sofiframework.displaytag.pagination.SmartListHelper;
import org.sofiframework.presentation.balisesjsp.listenavigation.ColonneTag;
import org.sofiframework.presentation.balisesjsp.utilitaire.EvaluateurExpression;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireAjax;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireNombre;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Cette balise prend une liste d'objets et créer une table pour afficher ces objets. Avec l'aide de balise de colonne,
 * vous n'avez qu'a spécifier le nom des propriétés (avec méthode get de spécifier), ceci permettra d'appeller vos
 * objets dans la liste que vous afficher. Cette balise fonctionne sensiblement comme les balises d'itération Struts, la
 * plupart des noms d'attributs sont les mêmes que Struts et offre aussi les mêmes fonctionnalités.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 * @see org.displaytag.tags.TableTag
 */
public class TableTag extends org.displaytag.tags.TableTag {
  /**
   * 
   */
  private static final long serialVersionUID = -8493018901301250962L;

  /**
   * Classe de journalisation.
   */
  private static Log log = LogFactory.getLog(TableTag.class);
  public static String AJAX_DIV = "liste_navigation_ajax_div";
  public static String AJAX_TRAITEMENT = "liste_navigation_ajax_traitement";

  /**
   * Used by various functions when the person wants to do paging - cleaned in doEndTag().
   */
  protected SmartListHelper listHelper;
  private ArrayList listeParametresSelection;
  private HashMap listeParametresCleSelection;
  private int virtualSize;
  private ListeNavigation listeNavigation;
  private boolean fonctionRetourValeur = false;
  private boolean fonctionRetourValeurAjax = false;
  private String nomFormulaireFonctionRetour;
  private String messageListeVide;
  private String afficheEnteteSiListeVide = null;
  private String divId;
  private boolean ajax = false;
  private String affichageAjax = null;
  private String afficherNavigationBasListe = "false";
  private boolean iterateurMultiPage = false;
  private int nombreMaximalMultiPage = 5;
  private String gabaritBarreNavigation = null;
  private String gabaritBarreNavigationBasListe = null;
  private String prefixIterateurMultiPage = null;
  private String libelleResultat = null;
  private String colonneLienDefaut;
  private String varIndex;

  @Override
  public String getColgroup() {
    return colgroup;
  }

  @Override
  public void setColgroup(String colgroup) {
    this.colgroup = colgroup;
  }

  public ArrayList getListeParametresSelection() {
    return listeParametresSelection;
  }

  public void setListeParametresSelection(ArrayList listeParametresSelection) {
    this.listeParametresSelection = listeParametresSelection;
  }

  public HashMap getListeParametresCleSelection() {
    return listeParametresCleSelection;
  }

  public void setListeParametresCleSelection(HashMap listeParametresCleSelection) {
    this.listeParametresCleSelection = listeParametresCleSelection;
  }

  public String getColonneLienDefaut() {
    return colonneLienDefaut;
  }

  public void setColonneLienDefaut(String colonneLienDefaut) {
    this.colonneLienDefaut = colonneLienDefaut;
  }

  /**
   * Méthode "overwrited" par la classe org.sofiframework.displaytag.tags.el.TableTag qui hérite de la classe présente.
   */
  protected void completerIteration() {
  }

  /**
   * Retourne le nombre virtuel dans la liste.
   * 
   * @return int
   */
  public int getVirtualSize() {
    return this.virtualSize;
  }

  /**
   * Fixer le nombre d'éléments dans la liste de navigation.
   * <p>
   * 
   * @param value
   *          le nombre d'éléments qu'on retrouve dans la liste de navigation
   */
  public void setVirtualSize(String value) throws InvalidTagAttributeValueException {
    try {
      this.virtualSize = (new Integer(value)).intValue();
    } catch (NumberFormatException e) {
      throw new InvalidTagAttributeValueException(getClass(), "virtualSize", value);
    }
  }

  /**
   * Méthode qui sert à remettre des valeurs par défaut dans la liste de navigation
   */
  protected void cleanUp() {
    this.listHelper = null;
    this.export = false;
    this.currentMediaType = null;
    this.decoratorName = null;
    this.pagesize = 0;
    this.length = 0;
    this.offset = 0;
    this.defaultSortedColumn = -1;
    this.rowNumber = 1;
    this.list = null;
    this.sortFullTable = null;
    this.doAfterBodyExecuted = false;
    this.currentRow = null;
    this.tableModel = null;
    this.requestUri = null;
    this.paramEncoder = null;
    this.footer = null;
    this.caption = null;
    this.messageListeVide = null;
    this.afficheEnteteSiListeVide = null;
    this.divId = null;
    this.ajax = false;
    this.affichageAjax = null;
    this.afficherNavigationBasListe = null;
    this.gabaritBarreNavigation = null;
    pageContext.setAttribute("tableTag", this);
  }

  /**
   * Méthode qui sert à obtenir seulement les données à afficher à l'écran.
   * <p>
   * Les données retournées par cette méthodes correspondent donc à une page de donnée parmis toutes les données
   * disponibles. Cette méthode utilise seulement les références des objets, ainsi il est impossible de modifier le data
   * contenu dans la liste. Il est cependant possible de modifier l'ordre de présentation des données.
   * <p>
   * 
   * @return la liste des données à afficher dans la liste de navigation
   */
  public List getViewableData() {
    // Reset de certains paramètre au cas ou les paramètres par défaut auraient changés
    if (this.tableModel.isSortFullTable()) {
      if (this.getVirtualSize() == 0) {
        this.tableModel.sortFullList();
      }
    }

    Object originalData = this.tableModel.getRowListFull();

    // Prendre un sous groupe de données par l'attribut length
    List fullList = CollectionUtil.getListFromObject(originalData, this.offset, this.length);

    // Prendre un sous groupe de données par l'attribut page
    if (this.pagesize > 0) {
      this.listHelper = new SmartListHelper(fullList, this.pagesize, this.properties, this.virtualSize);
      this.listHelper.setCurrentPage(this.pageNumber);
      fullList = this.listHelper.getListForCurrentPage();
    }

    return fullList;
  }

  /**
   * Reads parameters from the request and initialize all the needed table model attributes.
   * 
   * @throws ObjectLookupException
   *           for problems in evaluating the expression in the "name" attribute
   */
  protected void initParameters() throws ObjectLookupException {
    // @todo exception handling
    RequestHelperFactory rhf = this.properties.getRequestHelperFactoryInstance();

    RequestHelper requestHelper = rhf.getRequestHelperInstance(this.pageContext);

    initHref(requestHelper);

    Integer pageNumberParameter = requestHelper.getIntParameter(encodeParameter(TableTagParameters.PARAMETER_PAGE));
    this.pageNumber = (pageNumberParameter == null) ? 1 : pageNumberParameter.intValue();

    // Spécifier l'ancien tri de colonne.
    Integer triCourant = requestHelper.getIntParameter(encodeParameter(TableTagParameters.PARAMETER_SORT));

    if (triCourant == null) {
      String triCourantTmp = (String) pageContext.getSession().getAttribute(
          encodeParameter(TableTagParameters.PARAMETER_SORT));

      if (triCourantTmp != null) {
        triCourant = new Integer(triCourantTmp);
      }
    }

    int sortColumn = (triCourant == null) ? this.defaultSortedColumn : triCourant.intValue();
    this.tableModel.setSortedColumnNumber(sortColumn);

    // default value
    boolean finalSortFull = this.properties.getSortFullList();

    // user value for this single table
    if (this.sortFullTable != null) {
      finalSortFull = this.sortFullTable.booleanValue();
    }

    this.tableModel.setSortFullTable(finalSortFull);

    // Spécifier l'ordre du tri courant.
    Integer ordreTriCourant = requestHelper.getIntParameter(encodeParameter(TableTagParameters.PARAMETER_ORDER));

    if (ordreTriCourant == null) {
      String ancienOrdreTriTmp = (String) pageContext.getSession().getAttribute(
          encodeParameter(TableTagParameters.PARAMETER_ORDER + getDivId()));

      String colonneTri = (String) pageContext.getSession().getAttribute(
          encodeParameter(TableTagParameters.PARAMETER_SORT + getDivId()));

      if ((ancienOrdreTriTmp != null) && (colonneTri != null)) {
        ordreTriCourant = new Integer(ancienOrdreTriTmp);
        this.tableModel.setSortedColumnNumber(new Integer(colonneTri).intValue());
      }
    }

    SortOrderEnum paramOrder = SortOrderEnum.fromIntegerCode(ordreTriCourant);

    // Si aucun paramètre alors utiliser celui par défaut.
    if (paramOrder == null) {
      paramOrder = this.defaultSortOrder;
    }

    boolean order = SortOrderEnum.DESCENDING != paramOrder;
    this.tableModel.setSortOrderAscending(order);

    // if the behaviour is sort full page we need to go back to page one if sort of order is changed
    if (finalSortFull && (sortColumn != -1)) {
      // save actual sort to href
      this.baseHref.addParameter(encodeParameter(TableTagParameters.PARAMETER_PREVIOUSSORT), sortColumn);

      this.previousSortedColumn = (triCourant == null) ? (-1) : triCourant.intValue();

      SortOrderEnum previousParamOrder = SortOrderEnum.fromIntegerCode(requestHelper
          .getIntParameter(encodeParameter(TableTagParameters.PARAMETER_PREVIOUSORDER)));

      this.previousOrder = SortOrderEnum.DESCENDING != previousParamOrder;
    }

    Integer exportTypeParameter = requestHelper
        .getIntParameter(encodeParameter(TableTagParameters.PARAMETER_EXPORTTYPE));
    this.currentMediaType = MediaTypeEnum.fromIntegerCode(exportTypeParameter);

    if (this.currentMediaType == null) {
      this.currentMediaType = MediaTypeEnum.HTML;
    }

    this.tableIterator = IteratorUtils.getIterator(this.list);
  }

  /**
   * Méthode qui construit le code HTML qui sert à afficher la liste de navigation en format HTML.
   * <p>
   * 
   * @param messageListeNavigation
   *          le code HTML correspondant au début de l'affichage de la liste de navigation. Cette String possède un
   *          paramètre dynamique qui sera modifier pour contenir le résultat de l'exécution de cette méthode
   * @return l'intérieur du paramètre dynamique de la string passée en paramètre. Cette partie de code HTML contient
   *         toutes les données à afficher à l'écran.
   * @throws JspException
   *           une exception générique
   */
  protected String getHTMLData(String messageListeNavigation) throws JspException {
    this.previousRow = new Hashtable(10);
    this.nextRow = new Hashtable(10);

    boolean noItems = this.tableModel.getRowListPage().size() == 0;

    // si le paramètre n'est pas spécifié dans le tag
    if (this.getAfficheEnteteSiListeVide() == null) {
      String parametreSOFIAfficheEnteteSiListeVide = (String) GestionParametreSysteme.getInstance()
          .getParametreSysteme("afficheEnteteSiListeVide");

      // si le paramètre n'est pas dans le fichier de configuration de SOFI
      if (parametreSOFIAfficheEnteteSiListeVide == null) {
        // pour garder le comportement tel quel dans les développements existant
        // on doit afficher l'entête même si la liste est vide
        this.setAfficheEnteteSiListeVide("true");
      } else {
        this.setAfficheEnteteSiListeVide(parametreSOFIAfficheEnteteSiListeVide.toString().toLowerCase());
      }
    }

    // pour ne pas afficher l'entete d'une liste vide
    if (this.getAfficheEnteteSiListeVide().equals("false")) {
      // Si la liste est vide, on retourne le message à afficher en cas de liste vide
      if (noItems && !this.properties.getEmptyListShowTable()) {
        return getMessageVide();
      }
    }

    StringBuffer buffer = new StringBuffer();

    if (pageContext.getAttribute(UtilitaireBaliseJSP.NOM_FONCTION_RETOUR_VALEUR) != null) {
      buffer.append(UtilitaireBaliseJSP.genererFonctionRetourValeurs(this.pageContext, false));
    }

    if (pageContext.getAttribute(UtilitaireBaliseJSP.NOM_FONCTION_RETOUR_VALEUR_AJAX) != null) {
      /*
       * buffer.append(UtilitaireBaliseJSP.genererFonctionRetourValeurs( this.pageContext, true));
       */
    }

    if (getListeNavigation() != null) {
      // Ajouter le bout de code servant à désactiver les boutons de navigation lors d'un click
      buffer.append(UtilitaireBaliseJSP.getFonctionInactiverImage(this.pageContext));
    }

    // J-F: Ajouter le message de statut de la liste
    if (messageListeNavigation != null) {
      buffer.append("");
    }

    // Ajouter la banière de navigation du haut dans le bade la liste si demandé
    if (this.properties.getAddPagingBannerTop()) {
      String gabarit = traiterGabaritListeNavigation(getGabaritBarreNavigation());
      String barreNavigation = getSearchResultAndNavigation(gabarit);
      buffer.append(barreNavigation);
    }

    // Ajouter la gestion du CSS
    String css = this.properties.getCssTable();

    if (StringUtils.isNotBlank(css)) {
      this.addClass(css);
    }

    // Ajouter l'ouverture de la table
    buffer.append(getOpenTag());

    // Ajouter le caption
    if (this.caption != null) {
      buffer.append(this.caption);
    }

    // Ajouter l'ourverture de l'en-tête du tableau au besoin
    if (this.properties.getShowHeader()) {
      buffer.append(getTableHeader());
    }

    // Ajouter le body de la table ; C'est cette section qui contient toutes les données
    buffer.append(TagConstants.TAG_TBODY_OPEN);
    buffer.append(getTableBody());
    buffer.append(TagConstants.TAG_TBODY_CLOSE);

    // Ajouter le footer de la table au besoin
    if (this.footer != null) {
      buffer.append(TagConstants.TAG_TFOOTER_OPEN);
      buffer.append(this.footer);
      buffer.append(TagConstants.TAG_TFOOTER_CLOSE);
      this.footer = null;
    }

    // Ajouter la fermeture du la table
    buffer.append(getCloseTag());

    // Ajouter le footer de la table
    buffer.append(this.getTableFooter());

    if (this.tableModel.getTableDecorator() != null) {
      this.tableModel.getTableDecorator().finish();
    }

    return buffer.toString();
  }

  /**
   * Méthode qui sert à générer le code HTML à afficher dans l'en-tête de la table
   * <p>
   * L'en-tête de la table contient les données sur les grandeurs de chacunes des colonnes à afficher, le nom de
   * chacunes des colonnes à afficher ainsi que les liens à fournir dans le cas ou les différentes colonnes peuvent être
   * triées
   * <p>
   * 
   * @return l'en-tête de la table en HTML
   */
  private String getTableHeader() {
    // L'en-tête HTML à l'allure suivante
    // <colgroup width="15%"> <colgroup width="50%"> <colgroup width=" 35%">
    // <thead>
    // <tr>
    // <th class="order1 sorted">Colonne 1</th>
    // <th>Colonne 2</th>
    // <th>Colonne 3</th>
    // </tr>
    // </thead>
    StringBuffer buffer = new StringBuffer();

    // Déterminer toutes les largeurs de colonnes
    if ((getColgroup() != null) && !getColgroup().equals("")) {
      String colgroup = getColgroup();

      for (StringTokenizer tk = new StringTokenizer(colgroup, ","); tk.hasMoreTokens();) {
        String largeur = tk.nextToken();

        if (largeur.indexOf("-") != -1) {
          String nbIteration = largeur.substring(0, largeur.indexOf("-")).trim();

          for (int i = 0; i < Integer.parseInt(nbIteration); i++) {
            buffer.append(" <colgroup width=\"");
            buffer.append(largeur.substring(largeur.indexOf("-") + 1).trim());
            buffer.append("\"/>");
          }
        } else {
          buffer.append(" <colgroup width=\"");
          buffer.append(largeur);
          buffer.append("\"/>");
        }
      }
    }

    // Ajouter l'ouverture du <thead> et <tr>
    buffer.append(TagConstants.TAG_THEAD_OPEN);
    buffer.append(TagConstants.TAG_TR_OPEN);

    // Si la table est vide
    if (this.tableModel.isEmpty()) {
      buffer.append(TagConstants.TAG_TH_OPEN);
      buffer.append(TagConstants.TAG_TH_CLOSE);
    }

    // Itérateur sur les colonnes pour l'entete
    Iterator iterator = this.tableModel.getHeaderCellList().iterator();

    while (iterator.hasNext()) {
      // Extraire une cellule de l'entete
      HeaderCell headerCell = (HeaderCell) iterator.next();

      // Déterminer le style à adopter pour la cellule
      if (headerCell.getSortable()) {
        String cssSortable = this.properties.getCssSortable();
        headerCell.addHeaderClass(cssSortable);
      }

      String nomListeEnTraitement = pageContext.getRequest().getParameter("listeId");

      String ordreTri = null;

      if ((nomListeEnTraitement != null) && nomListeEnTraitement.equals(divId)) {
        // Extraire le tri spécifier en paramètre de l'url.
        ordreTri = pageContext.getRequest().getParameter(encodeParameter(TableTagParameters.PARAMETER_ORDER));
      }

      String attributATrie = null;

      if ((((nomListeEnTraitement != null) && nomListeEnTraitement.equals(divId)) || (nomListeEnTraitement == null))
          && ((getListeNavigation() != null) && (getListeNavigation().getOrdreTri() != null))) {
        if (getListeNavigation().getOrdreTri().equals(ListeNavigation.TRI_ASCENDANT)) {
          ordreTri = "2";
        } else {
          ordreTri = "1";
        }
      }

      if (ordreTri == null) {
        ordreTri = (String) pageContext.getSession().getAttribute(
            encodeParameter(TableTagParameters.PARAMETER_ORDER) + getDivId());
      }

      if (getListeNavigation() != null) {
        attributATrie = getListeNavigation().getTriAttribut();
      }

      String colonneTri = (String) pageContext.getSession().getAttribute(
          encodeParameter(TableTagParameters.PARAMETER_SORT) + getDivId());

      // Modification du style si l'item est déjà trié
      int nbColonneTrie = tableModel.getSortedColumnNumber();

      if ((colonneTri != null) && (nomListeEnTraitement == null)) {
        nbColonneTrie = Integer.valueOf(colonneTri).intValue();
      }

      int colonneEnCours = headerCell.getColumnNumber();

      boolean conditionAttributATrie = ((attributATrie != null) && (headerCell.getBeanPropertyName() != null) && headerCell
          .getBeanPropertyName().toUpperCase().equals(attributATrie.toUpperCase()))
          || ((attributATrie == null) && (nbColonneTrie == colonneEnCours));

      if (conditionAttributATrie) {
        headerCell.addHeaderClass(this.properties.getCssSorted());

        boolean isAscending = false;

        if ((ordreTri != null) && ordreTri.equals("2")) {
          isAscending = true;
        }

        headerCell.addHeaderClass(this.properties.getCssOrder(isAscending));
      }

      // Ajouter la balise <th> et le title="" de la colonne
      buffer.append(headerCell.getHeaderOpenTag());

      /**
       * Ajouter l'aide contextuelle s'il y a lieu
       */
      String header = null;

      if (!headerCell.getSortable()) {
        StringBuffer aideContextuelle = null;

        if (!UtilitaireString.isVide(headerCell.getAideContextuelle())) {
          aideContextuelle = new StringBuffer();
          aideContextuelle.append("<span title=\"");
          aideContextuelle.append(headerCell.getAideContextuelle());
          aideContextuelle.append("\">");
        }

        header = headerCell.getTitle();

        if (aideContextuelle != null) {
          aideContextuelle.append(header);
          aideContextuelle.append("</span>");
          header = aideContextuelle.toString();
        }
      } else {
        header = headerCell.getTitle();
      }

      // Ajouter le lien pour trier la colonne au besoin
      if (headerCell.getSortable()) {
        if (isAjax()) {
          // Traitement Ajax
          String nomDiv = getDivId();
          Href lienColonne = getSortingHref(headerCell);
          lienColonne.setAnchor(null);

          String fonctionApresChargement = (String) pageContext.getAttribute("fonctionJSApresChargement");
          StringBuffer lienAjax = new StringBuffer(UtilitaireAjax.traiterAffichageDivAjaxAsynchrone(
              lienColonne.toString(), nomDiv, null, null, null, fonctionApresChargement, pageContext));

          Anchor anchor = new Anchor(lienAjax.toString(), header, headerCell.getAideContextuelle(), true);
          header = anchor.toString();
        } else {
          Anchor anchor = new Anchor(getSortingHref(headerCell), header, headerCell.getAideContextuelle());
          header = anchor.toString();
        }
      }

      // Ajouter les balises de fermeture </th>
      buffer.append(header);
      buffer.append(headerCell.getHeaderCloseTag());
    }

    // Ajouter les balises de fermeture </tr> et </thead>
    buffer.append(TagConstants.TAG_TR_CLOSE);
    buffer.append(TagConstants.TAG_THEAD_CLOSE);

    return buffer.toString();
  }

  /**
   * Méthode qui sert à générer le lien à ajouter pour fournir la possibilité de tri à une colonne
   * <p>
   * 
   * @param headerCell
   *          l'en-tête de la colonne sur lequel on peut faire un tri
   * @return le lien HTML à appeler pour trier la colonne
   */
  private Href getSortingHref(HeaderCell headerCell) {
    Href href = new Href(this.baseHref);

    if (fonctionRetourValeurAjax) {
      // Propager l'index de la fenetre ajax dans l'url
      String indexDivAjax = pageContext.getRequest().getParameter(TableTagParameters.INDEX_DIV_AJAX);
      href.addParameter(TableTagParameters.INDEX_DIV_AJAX, indexDivAjax);
    }

    // Ajout du numéro de colonne comme paramètre du lien
    href.addParameter(encodeParameter(TableTagParameters.PARAMETER_SORT), headerCell.getColumnNumber());

    // Prendre le nom de l'attribut à trier et l'ajouter comme paramètre
    String nomAttribut = headerCell.getBeanPropertyName();

    if (nomAttribut != null) {
      href.addParameter(encodeParameter("tri"), headerCell.getBeanPropertyName());
    }

    /**
     * Ajouter l'identifiant de la liste dans l'url de la requête:
     */
    if (getDivId() != null) {
      href.addParameter("listeId", getDivId());
    }

    boolean nowOrderAscending = !(headerCell.isAlreadySorted() && this.tableModel.isSortOrderAscending());

    // Déterminer l'ordre de tri à effectuer et l'ajouter en paramètre
    if (nowOrderAscending) {
      href.addParameter(encodeParameter(TableTagParameters.PARAMETER_ORDER), SortOrderEnum.ASCENDING.getCode());
    } else {
      href.addParameter(encodeParameter(TableTagParameters.PARAMETER_ORDER), SortOrderEnum.DESCENDING.getCode());
    }

    // Si on désire trier la table au complet
    if (this.tableModel.isSortFullTable()) {
      // Retourner à la page 1
      if ((headerCell.getColumnNumber() != this.previousSortedColumn) || (nowOrderAscending ^ this.previousOrder)) {
        href.addParameter(encodeParameter(TableTagParameters.PARAMETER_PAGE), 1);
      } else {
        // Retour à la page 1 si un tri est demandé et si le mode vitual size est actif.
        if (getVirtualSize() != 0) {
          href.addParameter(encodeParameter(TableTagParameters.PARAMETER_PAGE), 1);
        }
      }
    }

    // Pour concerver la position dans la page lors du tri
    href.setAnchor(id);

    return href;
  }

  /**
   * Méthode qui sert à grouper des valeurs de colonnes dans le cas où on ne doit pas répeter les records similaires.
   * <p>
   * 
   * @param value
   *          la valeur à afficher dans la colonne
   * @param group
   *          l'index du groupe dans lequel la valeur se trouve
   * @return la valeur à afficher dans la colonne, il est possible de retourner une String vide dans le cas où la valeur
   *         passée en paramètre se retrouve déjà dans la ligne précédente
   */
  private String groupColumns(String value, int group) {
    // Si on est au début de nextRow alors on copie le contenu dans previousRow
    if ((group == 1) & (this.nextRow.size() > 0)) {
      this.previousRow.clear();
      this.previousRow.putAll(this.nextRow);
      this.nextRow.clear();
    }

    // Ajouter la clé dans la nextRow si on ne la trouve pas
    if (!this.nextRow.containsKey(new Integer(group))) {
      this.nextRow.put(new Integer(group), value);
    }

    // Vérifie si on retrouve la valeur dans une previousRow
    if (this.previousRow.containsKey(new Integer(group))) {
      for (int j = 1; j <= group; j++) {
        if (!((String) this.previousRow.get(new Integer(j))).equals((this.nextRow.get(new Integer(j))))) {
          // La valeur n'a pas été trouvée alors on la retourne
          return value;
        }
      }
    }

    // Bout de code utilisé seulement quand il n'y a rien dans la previousRow
    if (this.previousRow.size() == 0) {
      return value;
    }

    return "";
  }

  /**
   * Méthode qui sert à fabriquer le code HTML de l'intérieur de la liste de navigation
   * <p>
   * Le contenu de chacune des colonne est ajouté au HTML et les liens sur chacune des cellules est ajouté au besoin
   * <p>
   * 
   * @return Le code HTML représentant l'intérieur de la liste de navigation
   * @throws ObjectLookupException
   * @throws DecoratorException
   */
  private String getTableBody() throws ObjectLookupException, DecoratorException {
    StringBuffer buffer = new StringBuffer();

    // Itérer dans chacune des lignes
    RowIterator rowIterator = this.tableModel.getRowIterator();

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();

      // Appliquer le décorateur de la table si il en existe un
      if (this.tableModel.getTableDecorator() != null) {
        String stringStartRow = this.tableModel.getTableDecorator().startRow();

        if (stringStartRow != null) {
          buffer.append(stringStartRow);
        }
      }

      // Ajouter l'ouverture de la balise <tr>
      buffer.append(row.getOpenTag());

      // Itérer parmis toutes les colonnes de la ligne
      ColumnIterator columnIterator = row.getColumnIterator(this.tableModel.getHeaderCellList());

      while (columnIterator.hasNext()) {
        Column column = columnIterator.nextColumn();

        String property = column.getHeader().getBeanPropertyName();
        StringBuffer nomListeColonne = new StringBuffer("SOFI_LISTE_COLONNE");
        nomListeColonne.append(property);

        List listeColonne = (List) pageContext.getAttribute(nomListeColonne.toString());

        if ((property != null) && !property.equals("multibox") && (listeColonne != null)) {
          ColonneTag baliseColonneEnTraitement = (ColonneTag) listeColonne.get(row.getRowNumber());

          if ((baliseColonneEnTraitement != null) && (baliseColonneEnTraitement.getHref() != null)) {
            column.getHeader().setHref(baliseColonneEnTraitement.getHref());
          }
        }

        // Ajouter l'ouverture de <td> et ajout du lien HTML si la colonne en possède un
        buffer.append(column.getOpenTag(pageContext));

        Object value = column.getChoppedAndLinkedValue();

        // Appliquer le groupage de colonnes au besoin
        if (column.getGroup() != -1) {
          value = this.groupColumns(value.toString(), column.getGroup());
        }

        // Ajouter la valeur de la colonne ainsi que la fermeture de balise </td>
        buffer.append(value);
        buffer.append(column.getCloseTag());
      }

      // Dans le cas où la ligne ne possède pas de colonne
      if (this.tableModel.isEmpty()) {
        buffer.append(TagConstants.TAG_TD_OPEN).append(row.getObject().toString()).append(TagConstants.TAG_TD_CLOSE);
      }

      // Fermeture de la balise </tr>
      buffer.append(row.getCloseTag());

      // Appliquer le décorateur s'il y en a un pour la fermeture
      if (this.tableModel.getTableDecorator() != null) {
        String endRow = this.tableModel.getTableDecorator().finishRow();

        if (endRow != null) {
          buffer.append(endRow);
        }
      }
    }

    if (getMessageListeVide() == null) {
      String cleDefaut = UtilitaireMessageFichier.getInstance().get(this.properties.getEmptyListMessage());
      Message messageVideDefaut = UtilitaireMessage.get(cleDefaut, (HttpServletRequest) pageContext.getRequest());
      setMessageListeVide(messageVideDefaut.getMessage());
    }

    String messageVide = getMessageVide();

    // Si la table ne contient aucune ligne
    if (this.tableModel.getRowListPage().size() == 0) {
      buffer.append("<tr class=\"empty\"><td colspan=\"");
      buffer.append(this.tableModel.getNumberOfColumns());
      buffer.append("\">");
      buffer.append(messageVide);
      buffer.append("</td></tr>");
    }

    return buffer.toString();
  }

  private String getMessageVide() {
    String message = ((Message) UtilitaireLibelle.getLibelle(getMessageListeVide(), pageContext)).getMessage();

    if (message.equals(getMessageListeVide())) {
      message = (UtilitaireMessage.get(getMessageListeVide(), null, pageContext)).getMessage();
    }

    return message;
  }

  /**
   * Méthode qui sert à générer le footer de la liste de navigation.
   * <p>
   * Le footer de la liste se compose de la petite barre de navigation inter-pages pour la liste de navigation. Bien
   * sûr, si la liste n'est pas configurée pour afficher la barre de navigation dans le bas de la liste, une String vide
   * est retournée
   * <p>
   * 
   * @return le code HTML représentant le footer de la table
   */
  private String getTableFooter() {
    StringBuffer buffer = new StringBuffer(1000);

    // Ajout de la barre de navigation au besoin
    if (isNavigationBasListe()) {
      String gabarit = traiterGabaritListeNavigation(getGabaritBarreNavigationBasListe());

      buffer.append(getSearchResultAndNavigation(gabarit));
    }

    // Ajout du lien d'export au besoin
    if (this.export) {
      buffer.append(getExportLinks());
    }

    return buffer.toString();
  }

  /**
   * Méthode qui sert à générer le code HTML de la barre de navigation de la liste
   * <p>
   * La barre de navigation fait appel à une fonction javascript qui désactive les boutons après avoir cliqué dessus,
   * ainsi il est impossible de cliquer deux fois de suite sur le bouton de navigation. Cependant cette fonction
   * javascript n'est pas ajoutée dans cette méthode
   * <p>
   * 
   * @return le code HTML représentant la barre de navigation
   */
  private String getSearchResultAndNavigation(String template) {
    if ((this.pagesize != 0) && (this.listHelper != null)) {
      TableTag tableTag = (TableTag) pageContext.getAttribute("tableTag");

      // Prendre le bout de code qui correspond à l'en-tête de la liste de navigation
      String messageListeNavigation = null;

      if ((template != null) && isAjax()) {
        messageListeNavigation = UtilitaireString.remplacerTous(template, "'", "*");
      } else {
        messageListeNavigation = (String) pageContext.getSession().getAttribute(Constantes.MESSAGE_LISTE_NAVIGATION);
      }

      // Traiter le libellé de résultat s'il y a lieu.
      if (tableTag.getLibelleResultat() != null) {
        Libelle libelle = UtilitaireLibelle.getLibelle(tableTag.getLibelleResultat(), pageContext);

        if ((messageListeNavigation != null) && (template != null)) {
          // escaper les aspostrophes est une précondition à l'utilisation de MessageFormat.
          Object[] tableauResultat = { new Long(getListeNavigation().getNbListe()),
              libelle.getMessage().replaceAll("'", "''") };
          messageListeNavigation = MessageFormat.format(messageListeNavigation, tableauResultat);

          messageListeNavigation = UtilitaireString.remplacerTous(messageListeNavigation, "{2}", "{0}");
        } else {
          UtilitaireVelocity velocity = new UtilitaireVelocity();

          String nbListe = UtilitaireNombre.getValeurFormate("9", new Long(listeNavigation.getNbListe()),
              UtilitaireBaliseJSP.getLocale(getPageContext()), true);

          velocity.ajouterAuContexte("nbListe", nbListe);
          velocity.ajouterAuContexte("nomTypeOccurence", libelle.getMessage());

          String gabarit = GestionParametreSysteme.getInstance().getString("velocity.gabarit.barre.listeNavigation");

          if (UtilitaireString.isVide(gabarit)) {
            gabarit = "org/sofiframework/presentation/velocity/gabarit/barreListeNavigation.vm";
          }

          messageListeNavigation = velocity.genererHTML(gabarit);
        }
      }

      this.listHelper.setMessageListeNavigation(messageListeNavigation);

      String noPage = String.valueOf(this.listeNavigation.getNoPage());

      // Fabriquer le lien à appeler lors des click sur les boutons de navigation
      Href navigationHref = new Href(this.baseHref);
      navigationHref.addParameter("listeId", getDivId());
      navigationHref.addParameter(encodeParameter(TableTagParameters.PARAMETER_PAGE), "{0,number,#}");

      if (getListeNavigation() != null) {
        String codeTri = encodeParameter("tri");

        if (navigationHref.getParameterMap().get(codeTri) == null) {
          navigationHref.addParameter(codeTri, listeNavigation.getTriAttribut());

          String codeOrdreTri = encodeParameter("o");

          String ordreTri = "1";

          if ((listeNavigation.getOrdreTri() != null) && listeNavigation.getOrdreTri().equals("ASC")) {
            ordreTri = "2";
          }

          navigationHref.addParameter(codeOrdreTri, ordreTri);
        }
      }

      String urlNavigationListe = null;

      if (isAjax()) {
        urlNavigationListe = navigationHref.toString();
      } else {
        urlNavigationListe = "javascript:clickImage(''" + navigationHref.toString() + "'');";
      }

      // Fabriquer l'en-tête complète
      StringBuffer buffer = new StringBuffer();

      if (noPage != null) {
        this.listHelper.setCurrentPage((new Integer(noPage)).intValue());
      }

      if (!isAjax()) {
        // Donne la position de la liste dans la page (pour les url de trie de colonnes.)
        buffer.append("<a name=\"#").append(this.id).append("\"></a>");
      }

      buffer.append(this.listHelper.getSearchResultsSummary(pageContext));

      String divId = null;

      if (isAjax()) {
        divId = getDivId();
        pageContext.setAttribute(AJAX_DIV, divId);
        pageContext.setAttribute(AJAX_TRAITEMENT, Boolean.TRUE);
      }

      buffer.append(this.listHelper.getPageNavigationBar(urlNavigationListe, pageContext, divId));

      return buffer.toString();
    }

    return "";
  }

  /**
   * Méthode exécutée en premier lors de l'interprétation de la balise.
   * <p>
   * Cette méthode initialise certains paramètre et faire un peu de gestion d'erreur afin de s'assurer que les
   * paramètres passés sont bien ceux qu'on s'attend à recevoir.
   * <p>
   * 
   * @return une valeur identifiant à la page quoi faire après avoir traité cette balise
   * @throws JspException
   *           exception générique
   * @see javax.servlet.jsp.tagext.Tag#doStartTag()
   */
  @Override
  public int doStartTag() throws JspException {
    checkCommonsLang();

    this.properties = TableProperties.getInstance();
    this.tableModel = new TableModel(this.properties);

    initParameters();
    pageContext.setAttribute("tableTag", this);

    if (isFonctionRetourValeur() || isFonctionRetourValeurAjax() || (getNomFormulaireFonctionRetour() != null)) {
      if (isFonctionRetourValeur()) {
        this.setAttributNomFonctionRetourContexte(UtilitaireBaliseJSP.NOM_FONCTION_RETOUR_VALEUR);
      }

      if (isFonctionRetourValeurAjax()) {
        this.setAttributNomFonctionRetourContexte(UtilitaireBaliseJSP.NOM_FONCTION_RETOUR_VALEUR_AJAX);
      }
    }

    if (this.currentMediaType != null) {
      this.pageContext.setAttribute(PAGE_ATTRIBUTE_MEDIA, this.currentMediaType);
    }

    doIteration();

    // Retourne EVAL_BODY_TAG
    return 2;
  }

  private void setAttributNomFonctionRetourContexte(String nomFonctionRetourValeur) {
    // Parametres qui indique de faire la génération de la fonction JS qui fait la
    // sélection automatique des champs.
    if (getNomFormulaireFonctionRetour() != null) {
      pageContext.setAttribute(nomFonctionRetourValeur, getNomFormulaireFonctionRetour());
    } else {
      pageContext.setAttribute(nomFonctionRetourValeur, Boolean.TRUE);
    }
  }

  /**
   * Méthode qui sert à générer tout le code HTML nécessaire à l'affichage de la liste
   * <p>
   * Cette méthode s'exécute automatiquement à la fin de la balise de navigationListe.
   * <p>
   * 
   * @return une valeur identifiant à la page quoi faire après avoir traité cette balise
   * @throws JspException
   *           exception générique
   * @see javax.servlet.jsp.tagext.Tag#doEndTag()
   */
  @Override
  public int doEndTag() throws JspException {
    if (EvaluateurExpression.isEvaluerEL(pageContext)) {
      evaluerEL();
    }

    // Bout de code déjà présent qui ne semble là pour être compatible avec d'autre versions
    if (!this.doAfterBodyExecuted) {
      if (this.currentRow != null) {
        this.tableModel.addRow(this.currentRow);
      }

      while (this.tableIterator.hasNext()) {
        Object iteratedObject = this.tableIterator.next();
        this.rowNumber++;
        this.currentRow = new Row(iteratedObject, this.rowNumber);
        this.tableModel.addRow(this.currentRow);
      }
    }

    // Si la table ne possède aucune row
    if (this.tableModel.isEmpty()) {
      describeEmptyTable();
    }

    // Gestion du décorateur de table
    TableDecorator tableDecorator = DecoratorFactory.loadTableDecorator(this.decoratorName);

    if (tableDecorator != null) {
      tableDecorator.init(this.pageContext, this.list);
      this.tableModel.setTableDecorator(tableDecorator);
    }

    // Récupérer les données à afficher dans la page et les trier
    List pageData = getViewableData();
    this.tableModel.setRowListPage(pageData);

    if (!this.tableModel.isSortFullTable()) {
      this.tableModel.sortPageList();
    }

    StringBuffer buffer = new StringBuffer(8000);
    int returnValue = EVAL_PAGE;

    // Ajouter le code HTML dans le cas où on diffuse du HTML
    if (MediaTypeEnum.HTML.equals(this.currentMediaType)) {
      String messageListeNavigation = (String) pageContext.getSession().getAttribute(
          Constantes.MESSAGE_LISTE_NAVIGATION);
      buffer.append(getHTMLData(messageListeNavigation));
      write(buffer);
    } else {
      returnValue = doExport();
    }

    return returnValue;
  }

  /**
   * Méthode appelée après que le body de la balise ait été exécuté.
   * <p>
   * 
   * @return une valeur identifiant à la page quoi faire après avoir traité cette balise
   * @see javax.servlet.jsp.tagext.BodyTag#doAfterBody()
   */
  @Override
  public int doAfterBody() {
    this.doAfterBodyExecuted = true;

    this.rowNumber++;

    return doIteration();
  }

  /**
   * Utility method that is used by both doStartTag() and doAfterBody() to perform an iteration.
   * 
   * @return <code>int</code> either EVAL_BODY_TAG or SKIP_BODY depending on whether another iteration is desired.
   */
  /**
   * Exécute une itération dans toutes les lignes contenues dans la liste et crée des objets de type ObjetTransfert.
   * <p>
   * 
   * @return une valeur identifiant à la page quoi faire après avoir traité cette balise
   */
  protected int doIteration() {
    // Remplir l'objet Row
    if (this.currentRow != null) {
      this.tableModel.addRow(this.currentRow);
      this.currentRow = null;
    }

    // Itérer pour créer les ObjetTransfert
    if (this.tableIterator.hasNext()) {
      Object iteratedObject = this.tableIterator.next();

      if (getId() != null) {
        if ((iteratedObject != null)) {
          // Mettre l'objet dans le pageContext
          this.pageContext.setAttribute(getId(), iteratedObject);
        } else {
          this.pageContext.removeAttribute(getId());
        }

        // Mettre le rowNumber dans le pageContext
        this.pageContext.setAttribute(this.id + TableTagExtraInfo.ROWNUM_SUFFIX, new Integer(this.rowNumber));

        // Mettre le rowNumber dans la variable d'index spécifié.
        if (getVarIndex() != null) {
          this.pageContext.setAttribute(getVarIndex(), new Integer(this.rowNumber));
        }
      }

      this.currentRow = new Row(iteratedObject, this.rowNumber);

      if (log.isInfoEnabled()) {
        log.debug("Avant completerIteration...");
      }

      completerIteration();

      if (log.isInfoEnabled()) {
        log.debug("Apres completerIteration...");
      }

      // Retourne EVAL_BODY_TAG
      return 2;
    } else {
      return SKIP_BODY;
    }
  }

  public ListeNavigation getListeNavigation() {
    return listeNavigation;
  }

  public void setListeNavigation(ListeNavigation listeNavigation) {
    this.listeNavigation = listeNavigation;
  }

  /**
   * Retourne true si vous désirez la génération automatique du retour des valeurs à la page parent. (Utile seulement
   * pour les listes de valeurs).
   * 
   * @return true si vous désirez la génération automatique du retour des valeurs à la page parent.
   */
  public boolean isFonctionRetourValeur() {
    return fonctionRetourValeur;
  }

  /**
   * Fixer true si vous désirez la génération automatique du retour des valeurs à la page parent. (Utile seulement pour
   * les listes de valeurs).
   * 
   * @param fonctionRetourValeur
   */
  public void setFonctionRetourValeur(boolean fonctionRetourValeur) {
    this.fonctionRetourValeur = fonctionRetourValeur;
  }

  public void setNomFormulaireFonctionRetour(String nomFormulaireFonctionRetour) {
    this.nomFormulaireFonctionRetour = nomFormulaireFonctionRetour;
  }

  public String getNomFormulaireFonctionRetour() {
    return nomFormulaireFonctionRetour;
  }

  public void setMessageListeVide(String messageListeVide) {
    this.messageListeVide = messageListeVide;
  }

  public String getMessageListeVide() {
    return messageListeVide;
  }

  public void setAfficheEnteteSiListeVide(String afficheEnteteSiListeVide) {
    this.afficheEnteteSiListeVide = afficheEnteteSiListeVide;
  }

  public String getAfficheEnteteSiListeVide() {
    return this.afficheEnteteSiListeVide;
  }

  public void setDivId(String divId) {
    this.divId = divId;
  }

  public String getDivId() {
    return divId;
  }

  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  public boolean isAjax() {
    return ajax;
  }

  public void setAffichageAjax(String affichageAjax) {
    this.affichageAjax = affichageAjax;
  }

  public String getAffichageAjax() {
    return affichageAjax;
  }

  public boolean isAffichageAjax() {
    if (getAffichageAjax() == null) {
      return false;
    } else {
      Boolean ajax = new Boolean(getAffichageAjax());

      return ajax.booleanValue();
    }
  }

  public void setAfficherNavigationBasListe(String afficherNavigationBasListe) {
    this.afficherNavigationBasListe = afficherNavigationBasListe;
  }

  public String getAfficherNavigationBasListe() {
    return afficherNavigationBasListe;
  }

  public boolean isNavigationBasListe() {
    if ((getAfficherNavigationBasListe() != null) && getAfficherNavigationBasListe().toLowerCase().equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  public void setIterateurMultiPage(boolean iterateurMultiPage) {
    this.iterateurMultiPage = iterateurMultiPage;
  }

  public boolean isIterateurMultiPage() {
    return iterateurMultiPage;
  }

  public void setNombreMaximalMultiPage(int nombreMaximalMultiPage) {
    this.nombreMaximalMultiPage = nombreMaximalMultiPage;
  }

  public int getNombreMaximalMultiPage() {
    return nombreMaximalMultiPage;
  }

  public void setGabaritBarreNavigation(String gabaritBarreNavigation) {
    this.gabaritBarreNavigation = gabaritBarreNavigation;
  }

  public String getGabaritBarreNavigation() {
    return gabaritBarreNavigation;
  }

  /**
   * Évaluer les expressionn régulières
   */
  protected void evaluerEL() throws JspException {
    this.gabaritBarreNavigation = EvaluateurExpression.evaluerString("gabaritBarreNavigation",
        getGabaritBarreNavigation(), this, pageContext);

    this.gabaritBarreNavigationBasListe = EvaluateurExpression.evaluerString("gabaritBarreNavigationBaseListe",
        getGabaritBarreNavigationBasListe(), this, pageContext);

    String afficherListeBasPage = EvaluateurExpression.evaluerString("afficherNavigationBasListe",
        getAfficherNavigationBasListe(), this, pageContext);

    setAfficherNavigationBasListe(afficherListeBasPage);

    String messageListeVide = EvaluateurExpression.evaluerString("messageListeVide", getMessageListeVide(), this,
        pageContext);

    setMessageListeVide(messageListeVide);
  }

  protected void fixerSpecification() {
    if (gabaritBarreNavigation == null) {
      // Vérifier si défaut.
      gabaritBarreNavigation = (String) getPageContext().getServletContext().getAttribute(
          ConstantesBaliseJSP.ENTETE_LISTE_NAVIGATION_DEFAUT);
    }

    setGabaritBarreNavigation(gabaritBarreNavigation);

    if (getGabaritBarreNavigationBasListe() == null) {
      // Vérifier si défaut.
      gabaritBarreNavigationBasListe = (String) getPageContext().getServletContext().getAttribute(
          ConstantesBaliseJSP.ENTETE_BAS_LISTE_NAVIGATION_DEFAUT);
    }

    setGabaritBarreNavigationBasListe(gabaritBarreNavigationBasListe);
  }

  public void setPrefixIterateurMultiPage(String prefixIterateurMultiPage) {
    this.prefixIterateurMultiPage = prefixIterateurMultiPage;
  }

  public String getPrefixIterateurMultiPage() {
    return prefixIterateurMultiPage;
  }

  public void setLibelleResultat(String libelleResultat) {
    this.libelleResultat = libelleResultat;
  }

  public String getLibelleResultat() {
    return libelleResultat;
  }

  public void setGabaritBarreNavigationBasListe(String gabaritBarreNavigationBasListe) {
    this.gabaritBarreNavigationBasListe = gabaritBarreNavigationBasListe;
  }

  public String getGabaritBarreNavigationBasListe() {
    return gabaritBarreNavigationBasListe;
  }

  public String traiterGabaritListeNavigation(String gabarit) {
    if ((getListeNavigation() != null) && (gabarit != null)) {
      String url = getRequestURI();
      String divId = getDivId();
      gabarit = UtilitaireString.remplacerTous(gabarit, "{HREF}", url);
      gabarit = UtilitaireString.remplacerTous(gabarit, "{DIV_RETOUR}", divId);

      StringBuffer source = new StringBuffer("value=\"");
      source.append(getListeNavigation().getMaxParPage().toString());
      source.append("\"");

      StringBuffer remplacement = new StringBuffer("value=\"");
      remplacement.append(getListeNavigation().getMaxParPage().toString());
      remplacement.append("\" selected");
      gabarit = UtilitaireString.remplacerTous(gabarit, source.toString(), remplacement.toString());
    }

    return gabarit;
  }

  public void setVarIndex(String varIndex) {
    this.varIndex = varIndex;
  }

  public String getVarIndex() {
    return varIndex;
  }

  /**
   * Fixer si la liste de navigation doit retourner des valeurs à une liste de valeur configurée Ajax.
   * 
   * @param fonctionRetourValeurAjax
   *          true si la liste de valeur est Ajax, false si non
   */
  public void setFonctionRetourValeurAjax(boolean fonctionRetourValeurAjax) {
    this.fonctionRetourValeurAjax = fonctionRetourValeurAjax;
  }

  /**
   * Spécifie si la liste de navigation doit retourner des valeurs à une liste de valeur configurée Ajax.
   * 
   * @return true si la liste de valeur est Ajax, false si non
   */
  public boolean isFonctionRetourValeurAjax() {
    return fonctionRetourValeurAjax;
  }
}
