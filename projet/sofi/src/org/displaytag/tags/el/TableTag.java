package org.displaytag.tags.el;

import javax.servlet.jsp.JspException;

import org.sofiframework.composantweb.liste.ListeNavigation;


/**
 * Adds EL support to displaytag's TableTag.
 * Intégration du composant de liste de SOFI.
 * @author Tim McCune
 * @author Jean-François Brassard (Nurun inc.)
 * @version $Revision: 85 $ ($Author: guillaume.poirier $)
 * @see org.sofiframework.composantweb.ListeNavigation
 */
public class TableTag extends org.sofiframework.displaytag.tags.TableTag {
  /**
   * 
   */
  private static final long serialVersionUID = -3321366654615698349L;
  protected String _decorator;
  protected String _defaultsort;
  protected String _export;
  protected String _footer;
  protected String _length;
  protected String _name;
  protected String _offset;
  protected String _pagesize;
  protected String _virtualSize;
  protected String _requestURI;
  protected String _sort;

  public TableTag() {
    super();
    init();
  }

  @Override
  public void setDecorator(String s) {
    _decorator = s;
  }

  @Override
  public void setDefaultsort(String s) {
    _defaultsort = s;
  }

  @Override
  public void setExport(String s) {
    _export = s;
  }

  @Override
  public void setFooter(String s) {
    _footer = s;
  }

  @Override
  public void setLength(String s) {
    _length = s;
  }

  @Override
  public void setName(String s) {
    _name = s;
  }

  @Override
  public void setOffset(String s) {
    _offset = s;
  }

  @Override
  public void setPagesize(String s) {
    _pagesize = s;
  }

  @Override
  public void setVirtualSize(String s) {
    _virtualSize = s;
  }

  @Override
  public void setRequestURI(String s) {
    _requestURI = s;
  }

  @Override
  public void setSort(String s) {
    _sort = s;
  }

  private void init() {
    _decorator = null;
    _defaultsort = null;
    _export = null;
    _footer = null;
    _length = null;
    _name = null;
    _offset = null;
    _pagesize = null;
    _requestURI = null;
    _sort = null;
    _virtualSize = null;
  }

  @Override
  public void release() {
    super.release();
    init();
  }

  @Override
  public int doStartTag() throws JspException {
    evaluateExpressions();

    return super.doStartTag();
  }

  private void evaluateExpressions() throws JspException {
    ExpressionEvaluator eval = new ExpressionEvaluator(this, pageContext);
    String s;

    // evaluate name only once
    Object liste = eval.eval("name", _name, Object.class);

    ListeNavigation listeNavigation = null;

    if (ListeNavigation.class.isInstance(liste)) {
      listeNavigation = (ListeNavigation) liste;
      this.list = listeNavigation.getListe();
    } else {
      this.list = liste;
    }

    if ((s = eval.evalString("decorator", _decorator)) != null) {
      super.setDecorator(s);
    }

    if ((s = eval.evalString("defaultsort", _defaultsort)) != null) {
      super.setDefaultsort(s);
    }

    if ((s = eval.evalString("export", _export)) != null) {
      super.setExport(s);
    }

    if ((s = eval.evalString("footer", _footer)) != null) {
      super.setFooter(s);
    }

    if ((s = eval.evalString("length", _length)) != null) {
      super.setLength(s);
    }

    if ((s = eval.evalString("offset", _offset)) != null) {
      super.setOffset(s);
    }

    if ((s = eval.evalString("pagesize", _pagesize)) != null) {
      super.setPagesize(s);
    } else {
      if (listeNavigation != null) {
        super.setPagesize(listeNavigation.getMaxParPage().toString());
      }
    }

    if ((s = eval.evalString("requestURI", _requestURI)) != null) {
      super.setRequestURI(s);
    }

    if ((s = eval.evalString("sort", _sort)) != null) {
      super.setSort(s);
    }

    // Modifier par J-F
    if ((s = eval.evalString("virtualSize", _virtualSize)) != null) {
      super.setVirtualSize(s);
    } else {
      if (listeNavigation != null) {
        super.setVirtualSize(String.valueOf(listeNavigation.getNbListe()));
      }
    }
  }
}
