package org.displaytag.tags;

import org.displaytag.util.BeanInfoUtil;


/**
 * Needed to make the "class" tag attribute working.
 * @author fgiust
 * @version $Revision: 85 $ ($Author: guillaume.poirier $)
 * @see org.displaytag.util.BeanInfoUtil
 */
public class CaptionTagBeanInfo extends BeanInfoUtil {
}
