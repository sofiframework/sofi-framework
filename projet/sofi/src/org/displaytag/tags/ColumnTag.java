package org.displaytag.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.decorator.DecoratorFactory;
import org.displaytag.exception.DecoratorInstantiationException;
import org.displaytag.exception.ObjectLookupException;
import org.displaytag.exception.TagStructureException;
import org.displaytag.model.Cell;
import org.displaytag.model.HeaderCell;
import org.displaytag.properties.MediaTypeEnum;
import org.displaytag.util.Href;
import org.displaytag.util.HtmlAttributeMap;
import org.displaytag.util.MultipleHtmlAttribute;
import org.displaytag.util.TagConstants;
import org.sofiframework.presentation.balisesjsp.utilitaire.UtilitaireBaliseJSP;


/**
 * <p>
 * This tag works hand in hand with the TableTag to display a list of objects. This describes a column of data in the
 * TableTag. There can be any number of columns that make up the list.
 * </p>
 * <p>
 * This tag does no work itself, it is simply a container of information. The TableTag does all the work based on the
 * information provided in the attributes of this tag.
 * <p>
 * @author mraible
 * @version $Revision: 301 $ ($Author: j-f.brassard $)
 */
public class ColumnTag extends BodyTagSupport {
  /**
   * 
   */
  private static final long serialVersionUID = 8282652337108001126L;

  /**
   * logger.
   */
  private static Log log = LogFactory.getLog(ColumnTag.class);

  /**
   * html pass-through attributes for cells.
   */
  private HtmlAttributeMap attributeMap = new HtmlAttributeMap();

  /**
   * html pass-through attributes for cell headers.
   */
  private HtmlAttributeMap headerAttributeMap = new HtmlAttributeMap();

  /**
   * the property method that is called to retrieve the information to be displayed in this column. This method is
   * called on the current object in the iteration for the given row. The property format is in typical struts format
   * for properties (required)
   */
  private String property;

  /**
   * the title displayed for this column. if this is omitted then the property name is used for the title of the
   * column (optional).
   */
  private String title;

  /**
   * by default, null values don't appear in the list, by setting viewNulls to 'true', then null values will appear
   * as "null" in the list (mostly useful for debugging) (optional).
   */
  private boolean nulls;

  /**
   * is the column sortable?
   */
  private boolean sortable;

  /**
   * if set to true, then any email addresses and URLs found in the content of the column are automatically converted
   * into a hypertext link.
   */
  private boolean autolink;

  /**
   * the grouping level (starting at 1 and incrementing) of this column (indicates if successive contain the same
   * values, then they should not be displayed). The level indicates that if a lower level no longer matches, then
   * the matching for this higher level should start over as well. If this attribute is not included, then no
   * grouping is performed. (optional)
   */
  private int group = -1;

  /**
   * if this attribute is provided, then the data that is shown for this column is wrapped inside a &lt;a href&gt;
   * tag with the url provided through this attribute. Typically you would use this attribute along with one of the
   * struts-like param attributes below to create a dynamic link so that each row creates a different URL based on
   * the data that is being viewed. (optional)
   */
  protected Href href;

  /**
   * The name of the request parameter that will be dynamically added to the generated href URL. The corresponding
   * value is defined by the paramProperty and (optional) paramName attributes, optionally scoped by the paramScope
   * attribute. (optional)
   */
  protected String paramId;

  /**
   * The name of a JSP bean that is a String containing the value for the request parameter named by paramId (if
   * paramProperty is not specified), or a JSP bean whose property getter is called to return a String (if
   * paramProperty is specified). The JSP bean is constrained to the bean scope specified by the paramScope property,
   * if it is specified. If paramName is omitted, then it is assumed that the current object being iterated on is the
   * target bean. (optional)
   */
  private String paramName;

  /**
   * The name of a property of the bean specified by the paramName attribute (or the current object being iterated on
   * if paramName is not provided), whose return value must be a String containing the value of the request parameter
   * (named by the paramId attribute) that will be dynamically added to this href URL. (optional)
   * @deprecated use Expressions in paramName
   */
  @Deprecated
  private String paramProperty;

  /**
   * The scope within which to search for the bean specified by the paramName attribute. If not specified, all scopes
   * are searched. If paramName is not provided, then the current object being iterated on is assumed to be the
   * target bean. (optional)
   * @deprecated use Expressions in paramName
   */
  @Deprecated
  private String paramScope;

  /**
   * If this attribute is provided, then the column's displayed is limited to this number of characters. An elipse
   * (...) is appended to the end if this column is linked, and the user can mouseover the elipse to get the full
   * text. (optional)
   */
  private int maxLength;

  /**
   * If this attribute is provided, then the column's displayed is limited to this number of words. An elipse (...)
   * is appended to the end if this column is linked, and the user can mouseover the elipse to get the full text.
   * (optional)
   */
  private int maxWords;

  /**
   * a class that should be used to "decorate" the underlying object being displayed. If a decorator is specified for
   * the entire table, then this decorator will decorate that decorator. (optional)
   */
  private String decorator;

  /**
   * is the column already sorted?
   */
  private boolean alreadySorted;

  /**
   * The media supported attribute.
   */
  private List supportedMedia = Arrays.asList(MediaTypeEnum.ALL);

  /**
   * Aide contextuelle sur le nom de la colonne
   */
  private String aideContextuelle;

  /**
   * Génération de l'appel à une fonction de retour de valeurs automatique.
   */
  private boolean fonctionRetourValeur;

  /**
   * Génération de l'appel à une fonction de retour de valeurs automatique
   * lorsque la page est un fragment dans un div Ajax.
   */
  private boolean fonctionRetourValeurAjax;

  /**
   * Spécifie un format
   */
  private String format;
  private String hrefString;

  /**
   * Identifiant du cache qui sera utilisé pour retrouver la valeur
   */
  private String cache;

  /**
   * Permet de spécifier un identifiant de sous-cache
   * @since SOFI 2.0.3
   */
  private Object sousCache;

  /**
   * Permet de spécifier un filtre pour la liste des objets de cache.
   * @since SOFI 2.0.3
   */
  private String filtre;

  /**
   * Propriété de l'objet cache qui sera utilisé pour l'affichage de la colonne.
   * @since SOFI 2.0.3
   */
  private String proprieteAffichage;

  private boolean traiterCorps = false;

  /**
   * Aide contextuelle sur la cellule en traitement.
   */
  private String aideContextuelleCellule;

  /**
   * Style CSS sur la cellule en traitement.
   */
  private String classCellule;

  /**
   * Spécifie si on doit converir la valeur de la colonne en format HTML.
   *
   * Défaut : true
   */
  private boolean convertirEnHtml = true;

  /**
   *  Spécifie le nombre de caractére maximal avant de forcer un saut de ligne.
   */
  private String sautLigneNombreCararactereMaximal;

  /**
   *   Spécifie le caractére qui est utilisé afin d'appliquer un saut de ligne.
   */
  private String sautLigneCararactereSeparateur;

  public String getProperty() {
    return this.property;
  }

  /**
   * setter for the "property" tag attribute.
   * @param value attribute value
   */
  public void setProperty(String value) {
    this.property = value;
  }

  public String getTitle() {
    return this.title;
  }

  /**
   * setter for the "title" tag attribute.
   * @param value attribute value
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * setter for the "nulls" tag attribute.
   * @param value attribute value
   */
  public void setNulls(String value) {
    if (!Boolean.FALSE.toString().equals(value)) {
      this.nulls = true;
    }
  }

  /**
   * setter for the "sortable" tag attribute.
   * @param value attribute value
   */
  public void setSortable(String value) {
    if (!Boolean.FALSE.toString().equals(value)) {
      this.sortable = true;
    }
  }

  /**
   * @deprecated use setSortable()
   * @param value String
   */
  @Deprecated
  public void setSort(String value) {
    setSortable(value);
  }

  /**
   * setter for the "autolink" tag attribute.
   * @param value attribute value
   */
  public void setAutolink(String value) {
    if (!Boolean.FALSE.toString().equals(value)) {
      this.autolink = true;
    }
  }

  /**
   * setter for the "group" tag attribute.
   * @param value attribute value
   */
  public void setGroup(String value) {
    try {
      this.group = Integer.parseInt(value);
    } catch (NumberFormatException e) {
      // ignore?
      log.warn("Invalid \"group\" attribute: value=\"" + value + "\"");
    }
  }

  public String getHrefString() {
    return this.hrefString;
  }

  public Href getHref() {
    return this.href;
  }

  /**
   * setter for the "href" tag attribute.
   * @param value attribute value
   */
  public void setHref(String value) {
    this.hrefString = value;

    if (!UtilitaireBaliseJSP.isValeurEL(value)) {
      // call encodeURL to preserve session id when cookies are disabled
      String encodedHref = ((HttpServletResponse) this.pageContext.getResponse()).encodeURL(StringUtils.defaultString(value));
      this.href = new Href(encodedHref);
    }
  }

  public String getParamId() {
    return this.paramId;
  }

  /**
   * setter for the "paramId" tag attribute.
   * @param value attribute value
   */
  public void setParamId(String value) {
    this.paramId = value;
  }

  /**
   * setter for the "paramName" tag attribute.
   * @param value attribute value
   */
  public void setParamName(String value) {
    this.paramName = value;
  }

  /**
   * setter for the "paramProperty" tag attribute.
   * @param value attribute value
   */
  public void setParamProperty(String value) {
    this.paramProperty = value;
  }

  /**
   * setter for the "paramScope" tag attribute.
   * @param value attribute value
   */
  public void setParamScope(String value) {
    this.paramScope = value;
  }

  /**
   * setter for the "maxLength" tag attribute.
   * @param value attribute value
   */
  public void setMaxLength(int value) {
    this.maxLength = value;
  }

  /**
   * setter for the "maxWords" tag attribute.
   * @param value attribute value
   */
  public void setMaxWords(int value) {
    this.maxWords = value;
  }

  /**
   * setter for the "style" tag attribute.
   * @param value attribute value
   */
  public void setStyle(String value) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_STYLE, value);
  }

  /**
   * setter for the "class" tag attribute.
   * @param value attribute value
   */
  public void setClass(String value) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_CLASS,
        new MultipleHtmlAttribute(value));
  }

  /**
   * Adds a css class to the class attribute (html class suports multiple values).
   * @param value attribute value
   */
  public void addClass(String value) {
    Object classAttributes = this.attributeMap.get(TagConstants.ATTRIBUTE_CLASS);

    if (classAttributes == null) {
      this.attributeMap.put(TagConstants.ATTRIBUTE_CLASS,
          new MultipleHtmlAttribute(value));
    } else {
      ((MultipleHtmlAttribute) classAttributes).addAttributeValue(value);
    }
  }

  /**
   * setter for the "headerClass" tag attribute.
   * @param value attribute value
   */
  public void setHeaderClass(String value) {
    this.headerAttributeMap.put(TagConstants.ATTRIBUTE_CLASS,
        new MultipleHtmlAttribute(value));
  }

  /**
   * setter for the "headerStyleClass" tag attribute.
   * @param value attribute value
   * @deprecated use setHeaderClass()
   */
  @Deprecated
  public void setHeaderStyleClass(String value) {
    setHeaderClass(value);
  }

  /**
   * setter for the "decorator" tag attribute.
   * @param value attribute value
   */
  public void setDecorator(String value) {
    this.decorator = value;
  }

  /**
   * Fixer l'aide contextuelle
   * @param aideContextuelle l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Is this column configured for the media type?
   * @param mediaType the currentMedia type
   * @return true if the column should be displayed for this request
   */
  public boolean availableForMedia(MediaTypeEnum mediaType) {
    return this.supportedMedia.contains(mediaType);
  }

  /**
   * Tag setter.
   * @param media the space delimited list of supported types
   */
  public void setMedia(String media) {
    if (StringUtils.isBlank(media) ||
        (media.toLowerCase().indexOf("all") > -1)) {
      this.supportedMedia = Arrays.asList(MediaTypeEnum.ALL);

      return;
    }

    this.supportedMedia = new ArrayList();

    String[] values = StringUtils.split(media);

    for (int i = 0; i < values.length; i++) {
      String value = values[i];

      if (!StringUtils.isBlank(value)) {
        MediaTypeEnum type = MediaTypeEnum.fromName(value.toLowerCase());

        if (type == null) { // Should be in a tag validator..

          String msg = "Unknown media type \"" + value +
              "\"; media must be one or more values, space separated." +
              " Possible values are:";

          for (int j = 0; j < MediaTypeEnum.ALL.length; j++) {
            MediaTypeEnum mediaTypeEnum = MediaTypeEnum.ALL[j];
            msg += (" '" + mediaTypeEnum.getName() + "'");
          }

          throw new IllegalArgumentException(msg + ".");
        }

        this.supportedMedia.add(type);
      }
    }
  }

  /**
   * Passes attribute information up to the parent TableTag.
   * <p>
   * When we hit the end of the tag, we simply let our parent (which better be a TableTag) know what the user wants
   * to do with this column. We do that by simple registering this tag with the parent. This tag's only job is to
   * hold the configuration information to describe this particular column. The TableTag does all the work.
   * </p>
   * @return int
   * @throws JspException if this tag is being used outside of a &lt;display:list...&gt; tag.
   * @see javax.servlet.jsp.tagext.Tag#doEndTag()
   */
  @Override
  public int doEndTag() throws JspException {
    MediaTypeEnum currentMediaType = (MediaTypeEnum) this.pageContext.findAttribute(TableTag.PAGE_ATTRIBUTE_MEDIA);

    if ((currentMediaType != null) && !availableForMedia(currentMediaType)) {
      if (log.isDebugEnabled()) {
        log.debug("skipping column body, currentMediaType=" + currentMediaType);
      }

      return SKIP_BODY;
    }

    TableTag tableTag = (TableTag) findAncestorWithClass(this, TableTag.class);

    // Ajouter l'entete de colonne de la liste.
    if (tableTag.isFirstIteration()) {
      addHeaderToTable(tableTag);
    }

    Cell cell;

    if ((this.property == null) || isTraiterCorps()) {
      String value = "";

      if (this.bodyContent != null) {
        value = this.bodyContent.getString();
      }

      cell = new Cell(value);
    } else {
      //Do not use Cell.EMPTY_CELL
      cell = new Cell(null);
    }

    //Ajouter l'aide contextullle à la cellule associé au '<td>'.
    if (getAideContextuelleCellule() != null) {
      cell.getAttributeMap().put("title", getAideContextuelleCellule());
    }

    //Ajouter la classe CSS à la cellule associé au '<td>'.
    if (getClassCellule() != null) {
      cell.getAttributeMap().put("class", getClassCellule());
    }

    tableTag.addCell(cell);

    cleanUp();

    return super.doEndTag();
  }

  /**
   * Adds the current header to the table model calling addColumn in the parent table tag. This method should be
   * called only at first iteration.
   * @param tableTag parent table tag
   * @throws DecoratorInstantiationException for error during column decorator instantiation
   * @throws ObjectLookupException for errors in looking up values
   */
  private void addHeaderToTable(TableTag tableTag)
      throws DecoratorInstantiationException, ObjectLookupException {
    HeaderCell headerCell = new HeaderCell();
    headerCell.setHeaderAttributes((HtmlAttributeMap) this.headerAttributeMap.clone());
    headerCell.setHtmlAttributes((HtmlAttributeMap) this.attributeMap.clone());
    headerCell.setTitle(this.title);
    headerCell.setSortable(this.sortable);
    headerCell.setColumnDecorator(DecoratorFactory.loadColumnDecorator(
        this.decorator));
    headerCell.setBeanPropertyName(this.property);
    headerCell.setShowNulls(this.nulls);
    headerCell.setMaxLength(this.maxLength);
    headerCell.setMaxWords(this.maxWords);
    headerCell.setAutoLink(this.autolink);
    headerCell.setGroup(this.group);
    headerCell.setAideContextuelle(this.aideContextuelle);
    headerCell.setFonctionRetourValeur(isFonctionRetourValeur());
    headerCell.setFonctionRetourValeurAjax(isFonctionRetourValeurAjax());
    headerCell.setFormat(getFormat());
    headerCell.setPageContext(pageContext);
    headerCell.setCache(getCache());

    /*since SOFI 2.0.3*/
    headerCell.setSousCache(getSousCache());
    headerCell.setFiltre(getFiltre());
    headerCell.setProprieteAffichage(getProprieteAffichage());
    /*Fin since SOFI 2.0.3*/

    headerCell.setConvertirEnHtml(isConvertirEnHtml());
    headerCell.setSautLigneNombreCararactereMaximal(getSautLigneNombreCararactereMaximal());
    headerCell.setSautLigneCararactereSeparateur(getSautLigneCararactereSeparateur());

    if (isFonctionRetourValeur() || isFonctionRetourValeurAjax()) {
      headerCell.setParamName(this.paramId);
    }

    // href and parameter, create link
    if ((this.href != null) && (this.paramId != null)) {
      Href colHref = new Href(this.href);

      // parameter value is in a different object than the iterated one
      if ((this.paramName != null) || (this.paramScope != null)) {
        // create a complete string for compatibility with previous version before expression evaluation.
        // this approach is optimized for new expressions, not for previous property/scope parameters
        StringBuffer expression = new StringBuffer();

        // append scope
        if (StringUtils.isNotBlank(this.paramScope)) {
          expression.append(this.paramScope).append("Scope.");
        }

        // base bean name
        if (this.paramId != null) {
          expression.append(this.paramName);
        } else {
          expression.append(tableTag.getName());
        }

        // append property
        if (StringUtils.isNotBlank(this.paramProperty)) {
          expression.append('.').append(this.paramProperty);
        }

        // evaluate expression.
        // note the value is fixed, not based on any object created during iteration
        // this is here for compatibility with the old version mainly
        Object paramValue = tableTag.evaluateExpression(expression.toString());

        // add parameter
        colHref.addParameter(this.paramId, paramValue);
      } else {
        //@todo lookup value as a property on the list object. This should not be done here to avoid useless
        // work when only a part of the list is displayed
        // set id
        headerCell.setParamName(this.paramId);

        // set property
        headerCell.setParamProperty(this.paramProperty);
      }

      // sets the base href
      headerCell.setHref(colHref);
    }

    tableTag.addColumn(headerCell);

    if (log.isDebugEnabled()) {
      log.debug(
          "columnTag.addHeaderToTable() :: first iteration - adding header " +
              headerCell);
    }
  }

  /**
   * @see javax.servlet.jsp.tagext.Tag#release()
   */
  @Override
  public void release() {
    setTraiterCorps(false);
    super.release();
  }

  /**
   * @see javax.servlet.jsp.tagext.Tag#doStartTag()
   */
  @Override
  public int doStartTag() throws JspException {
    TableTag tableTag = (TableTag) findAncestorWithClass(this, TableTag.class);

    if (tableTag == null) {
      throw new TagStructureException(getClass(), "column", "table");
    }

    // If the list is empty, do not execute the body; may result in NPE
    if (tableTag.isEmpty()) {
      return SKIP_BODY;
    } else {
      MediaTypeEnum currentMediaType = (MediaTypeEnum) this.pageContext.findAttribute(TableTag.PAGE_ATTRIBUTE_MEDIA);

      if (!availableForMedia(currentMediaType)) {
        return SKIP_BODY;
      }

      return super.doStartTag();
    }
  }

  /**
   * called to clean up instance variables at the end of tag evaluation.
   */
  protected void cleanUp() {
    this.attributeMap.clear();
    this.attributeMap.clear();
    this.headerAttributeMap.clear();
    this.alreadySorted = false;

    this.aideContextuelle = null;
    this.aideContextuelleCellule = null;
    this.setSautLigneCararactereSeparateur(null);
    this.setSautLigneNombreCararactereMaximal(null);
    this.classCellule = null;

    // this is required to fix bad tag pooling in tomcat, need to be tested in resin
    this.sortable = false;
    this.group = -1;
    this.title = null;

    // fix for tag pooling in tomcat
    setBodyContent(null);
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("bodyContent",
        this.bodyContent).append("group", this.group)
        .append("maxLength",
            this.maxLength).append("decorator", this.decorator)
            .append("href",
                this.href).append("title", this.title)
                .append("paramScope",
                    this.paramScope).append("property", this.property)
                    .append("paramProperty",
                        this.paramProperty).append("headerAttributeMap", this.headerAttributeMap)
                        .append("paramName",
                            this.paramName).append("autolink", this.autolink)
                            .append("nulls",
                                this.nulls).append("maxWords", this.maxWords)
                                .append("attributeMap",
                                    this.attributeMap).append("sortable", this.sortable)
                                    .append("paramId",
                                        this.paramId).append("alreadySorted", this.alreadySorted).toString();
  }

  public void setFonctionRetourValeur(boolean fonctionRetourValeur) {
    this.fonctionRetourValeur = fonctionRetourValeur;
  }

  public boolean isFonctionRetourValeur() {
    return fonctionRetourValeur;
  }

  public HtmlAttributeMap getAttributeMap() {
    return attributeMap;
  }

  public HtmlAttributeMap getHeaderAttributeMap() {
    return headerAttributeMap;
  }

  public boolean isNulls() {
    return nulls;
  }

  public boolean isSortable() {
    return sortable;
  }

  public boolean isAutolink() {
    return autolink;
  }

  public int getGroup() {
    return group;
  }

  public String getParamName() {
    return paramName;
  }

  public String getParamProperty() {
    return paramProperty;
  }

  public String getParamScope() {
    return paramScope;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public int getMaxWords() {
    return maxWords;
  }

  public String getDecorator() {
    return decorator;
  }

  public boolean isAlreadySorted() {
    return alreadySorted;
  }

  public List getSupportedMedia() {
    return supportedMedia;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getFormat() {
    return format;
  }

  public void setCache(String cache) {
    this.cache = cache;
  }

  public String getCache() {
    return cache;
  }

  public void setTraiterCorps(boolean traiterCorps) {
    this.traiterCorps = traiterCorps;
  }

  public boolean isTraiterCorps() {
    return traiterCorps;
  }

  public void setAideContextuelleCellule(String aideContextuelleCellule) {
    this.aideContextuelleCellule = aideContextuelleCellule;
  }

  public String getAideContextuelleCellule() {
    return aideContextuelleCellule;
  }

  /**
   * Remplacer par setAideContextuelleCellule.
   * @param aideContextuelleCellule
   */
  public void setAideContextuelleRangee(String aideContextuelleCellule) {
    this.aideContextuelleCellule = aideContextuelleCellule;
  }

  public void setClassCellule(String classCellule) {
    this.classCellule = classCellule;
  }

  public String getClassCellule() {
    return classCellule;
  }

  /**
   * Fixer si la génération de l'appel à une fonction de retour de valeurs
   * automatique se fait dans un fragment dans un div Ajax.

   * @param fonctionRetourValeurAjax true div Ajax, false dans une page parent.
   */
  public void setFonctionRetourValeurAjax(boolean fonctionRetourValeurAjax) {
    this.fonctionRetourValeurAjax = fonctionRetourValeurAjax;
  }

  /**
   * Obtenir si la génération de l'appel à une fonction de retour de valeurs
   * automatique se fait dans un fragment dans un div Ajax.
   * @return true div Ajax, false dans une page parent.
   */
  public boolean isFonctionRetourValeurAjax() {
    return fonctionRetourValeurAjax;
  }

  public void setSautLigneNombreCararactereMaximal(
      String sautLigneNombreCararactereMaximal) {
    this.sautLigneNombreCararactereMaximal = sautLigneNombreCararactereMaximal;
  }

  public String getSautLigneNombreCararactereMaximal() {
    return sautLigneNombreCararactereMaximal;
  }

  public void setSautLigneCararactereSeparateur(
      String sautLigneCararactereSeparateur) {
    this.sautLigneCararactereSeparateur = sautLigneCararactereSeparateur;
  }

  public String getSautLigneCararactereSeparateur() {
    return sautLigneCararactereSeparateur;
  }

  public void setConvertirEnHtml(boolean convertirEnHtml) {
    this.convertirEnHtml = convertirEnHtml;
  }

  public boolean isConvertirEnHtml() {
    return convertirEnHtml;
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  public Object getSousCache() {
    return sousCache;
  }

  public void setFiltre(String filtre) {
    this.filtre = filtre;
  }

  public String getFiltre() {
    return filtre;
  }

  public void setProprieteAffichage(String proprieteAffichage) {
    this.proprieteAffichage = proprieteAffichage;
  }

  public String getProprieteAffichage() {
    return proprieteAffichage;
  }
}
