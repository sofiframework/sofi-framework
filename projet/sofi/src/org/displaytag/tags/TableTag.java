package org.displaytag.tags;


//Utiliser la version de SOFI
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.exception.InvalidTagAttributeValueException;
import org.displaytag.export.BaseExportView;
import org.displaytag.export.ExportViewFactory;
import org.displaytag.model.Cell;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.TableModel;
import org.displaytag.properties.MediaTypeEnum;
import org.displaytag.properties.SortOrderEnum;
import org.displaytag.properties.TableProperties;
import org.displaytag.util.Anchor;
import org.displaytag.util.Href;
import org.displaytag.util.ParamEncoder;
import org.displaytag.util.RequestHelper;
import org.sofiframework.displaytag.model.Row;
import org.sofiframework.displaytag.pagination.SmartListHelper;
import org.sofiframework.exception.SOFIException;


/**
 * This tag takes a list of objects and creates a table to display those objects. With the help of column tags, you
 * simply provide the name of properties (get Methods) that are called against the objects in your list that gets
 * displayed. This tag works very much like the struts iterator tag, most of the attributes have the same name and
 * functionality as the struts tag.
 * @author mraible
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class TableTag extends HtmlTableTag {
  /**
   * 
   */
  private static final long serialVersionUID = 4212187751450372153L;

  /**
   * name of the attribute added to page scope when exporting, containing an MediaTypeEnum this can be used in column
   * content to detect the output type and to return different data when exporting.
   */
  public static final String PAGE_ATTRIBUTE_MEDIA = "mediaType";

  /**
   * If this buffer has been appended to at all, the contents of the buffer will be served as the sole output of the
   * request. Request variable.
   */
  public static final String FILTER_CONTENT_OVERRIDE_BODY = "org.displaytag.filter.ResponseOverrideFilter.CONTENT_OVERRIDE_BODY";

  /**
   * If the request content is overriden, you must also set the content type appropriately. Request variable.
   */
  public static final String FILTER_CONTENT_OVERRIDE_TYPE = "org.displaytag.filter.ResponseOverrideFilter.CONTENT_OVERRIDE_TYPE";

  /**
   * If the filename is specified, there will be a supplied filename. Request variable.
   */
  public static final String FILTER_CONTENT_OVERRIDE_FILENAME = "org.displaytag.filter.ResponseOverrideFilter.CONTENT_OVERRIDE_FILENAME";

  /**
   * logger.
   */
  private static Log log = LogFactory.getLog(TableTag.class);

  /**
   * Has the commons-lang dependency been checked?
   */
  protected static boolean commonsLangChecked;

  /**
   * Object (collection, list) on which the table is based. Set directly using the "list" attribute or evaluated from
   * expression.
   */
  protected Object list;

  /**
   * Iterator on collection.
   */
  protected Iterator tableIterator;

  /**
   * actual row number, updated during iteration.
   */
  protected int rowNumber = 1;

  /**
   * name of the object to use for iteration. Can contain expression
   */
  private String name;

  /**
   * Map which contains previous row values. Needed for grouping
   */
  protected Map previousRow;

  /**
   * table model - initialized in doStartTag().
   */
  protected TableModel tableModel;

  /**
   * current row.
   */
  protected Row currentRow;

  /**
   * next row.
   */
  protected Map nextRow;

  /**
   * length of list to display - reset in doEndTag().
   */
  protected int length;

  /**
   * table decorator class name - cleaned in doEndTag().
   */
  protected String decoratorName;

  /**
   * page size - reset in doEndTag().
   */
  protected int pagesize;

  /**
   * add export links - reset in doEndTag().
   */
  protected boolean export;

  /**
   * Used by various functions when the person wants to do paging - cleaned in doEndTag().
   */
  protected SmartListHelper listHelper;

  /**
   * base href used for links - set in initParameters().
   */
  protected Href baseHref;

  /**
   * table properties - set in doStartTag().
   */
  protected TableProperties properties;

  /**
   * page number - set in initParameters().
   */
  protected int pageNumber = 1;

  /**
   * list offset - reset in doEndTag().
   */
  protected int offset;

  /**
   * export type - set in initParameters().
   */
  protected MediaTypeEnum currentMediaType;

  /**
   * index of the previously sorted column.
   */
  protected int previousSortedColumn;

  /**
   * previous sorting order.
   */
  protected boolean previousOrder;

  /**
   * sort the full list?
   */
  protected Boolean sortFullTable;

  /**
   * Request uri.
   */
  protected String requestUri;

  /**
   * daAfterBody() has been executed at least once?
   */
  protected boolean doAfterBodyExecuted;

  /**
   * The param encoder used to generate unique parameter names. Initialized at the first use of encodeParameter().
   */
  protected ParamEncoder paramEncoder;

  /**
   * the index of the column sorted by default.
   */
  protected int defaultSortedColumn = -1;

  /**
   * the sorting order for the sorted column.
   */
  protected SortOrderEnum defaultSortOrder;

  /**
   * static footer added using the footer tag.
   */
  protected String footer;

  /**
   * static caption added using the footer tag.
   */
  protected String caption;

  /**
   * Spécifier le détail de chacune des colonnes de la table
   */
  protected String colgroup;

  /**
   * Sets the content of the footer.
   * @param string footer content
   */
  public void setFooter(String string) {
    this.footer = string;
  }

  /**
   * Sets the content of the caption.
   * @param string caption content
   */
  public void setCaption(String string) {
    this.caption = string;
  }

  /**
   * Is the current row empty?
   * @return true if the current row is empty
   */
  protected boolean isEmpty() {
    return this.currentRow == null;
  }

  /**
   * setter for the "sort" attribute.
   * @param value "page" (sort a single page) or "list" (sort the full list)
   * @throws InvalidTagAttributeValueException if value is not "page" or "list"
   */
  public void setSort(String value) throws InvalidTagAttributeValueException {
    if (TableTagParameters.SORT_AMOUNT_PAGE.equals(value)) {
      this.sortFullTable = Boolean.FALSE;
    } else if (TableTagParameters.SORT_AMOUNT_LIST.equals(value)) {
      this.sortFullTable = Boolean.TRUE;
    } else {
      throw new InvalidTagAttributeValueException(getClass(), "sort", value);
    }
  }

  public String getRequestURI() {
    return this.requestUri;
  }

  /**
   * setter for the "requestURI" attribute.
   * @param value base URI for creating links
   */
  public void setRequestURI(String value) {
    this.requestUri = value;
  }

  /**
   * Used to directly set a list (or any object you can iterate on).
   * @param value Object
   * @deprecated use setName() to get the object from the page or request scope instead of setting it directly here
   */
  @Deprecated
  public void setList(Object value) {
    this.list = value;
  }

  /**
   * Sets the name of the object to use for iteration.
   * @param value name of the object to use for iteration. Can contain expression
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * sets the number of items to be displayed in the page.
   * @param value String
   * @throws InvalidTagAttributeValueException if value is not a valid integer
   */
  public void setLength(String value) throws InvalidTagAttributeValueException {
    try {
      this.length = Integer.parseInt(value);
    } catch (NumberFormatException e) {
      throw new InvalidTagAttributeValueException(getClass(), "length", value);
    }
  }

  /**
   * sets the index of the default sorted column.
   * @param value index of the column to sort
   * @throws InvalidTagAttributeValueException if value is not a valid integer
   */
  public void setDefaultsort(String value) {
    try {
      // parse and subtract one (internal index is 0 based)
      this.defaultSortedColumn = Integer.parseInt(value) - 1;
    } catch (NumberFormatException e) {
      throw new SOFIException("Erreur le tri par défaut doit etre numérique");
    }
  }

  /**
   * sets the sorting order for the sorted column.
   * @param value "ascending" or "descending"
   * @throws InvalidTagAttributeValueException if value is not one of "ascending" or "descending"
   */
  public void setDefaultorder(String value)
      throws InvalidTagAttributeValueException {
    this.defaultSortOrder = SortOrderEnum.fromName(value);

    if (this.defaultSortOrder == null) {
      throw new InvalidTagAttributeValueException(getClass(), "defaultorder",
          value);
    }
  }

  /**
   * sets the number of items that should be displayed for a single page.
   * @param value String
   * @throws InvalidTagAttributeValueException if value is not a valid integer
   */
  public void setPagesize(String value)
      throws InvalidTagAttributeValueException {
    try {
      this.pagesize = Integer.parseInt(value);
    } catch (NumberFormatException e) {
      throw new InvalidTagAttributeValueException(getClass(), "pagesize", value);
    }
  }

  public String getDecorator() {
    return this.decoratorName;
  }

  /**
   * Setter for the decorator class name.
   * @param decorator fully qualified name of the table decorator to use
   */
  public void setDecorator(String decorator) {
    this.decoratorName = decorator;
  }

  /**
   * Is export enabled?
   * @param booleanValue <code>true</code> if export should be enabled
   */
  public void setExport(String booleanValue) {
    if (!Boolean.FALSE.toString().equals(booleanValue)) {
      this.export = true;
    }
  }

  /**
   * Setter for the list offset attribute.
   * @param value String
   * @throws InvalidTagAttributeValueException if value is not a valid positive integer
   */
  public void setOffset(String value) throws InvalidTagAttributeValueException {
    try {
      int userOffset = Integer.parseInt(value);

      if (userOffset < 1) {
        throw new InvalidTagAttributeValueException(getClass(), "offset", value);
      }

      // this.offset is 0 based, subtract 1
      this.offset = (userOffset - 1);
    } catch (NumberFormatException e) {
      throw new InvalidTagAttributeValueException(getClass(), "offset", value);
    }
  }

  /**
   * Set the base href used in creating link.
   * @param value Href
   */
  protected void setBaseHref(Href value) {
    this.baseHref = value;
  }

  /**
   * It's a getter.
   * @return the this.pageContext
   */
  public PageContext getPageContext() {
    return this.pageContext;
  }

  /**
   * Returns the properties.
   * @return TableProperties
   */
  protected TableProperties getProperties() {
    return this.properties;
  }

  /**
   * Called by interior column tags to help this tag figure out how it is supposed to display the information in the
   * List it is supposed to display.
   * @param column an internal tag describing a column in this tableview
   */
  public void addColumn(HeaderCell column) {
    if (log.isDebugEnabled()) {
      log.debug("[" + getId() + "] addColumn " + column);
    }

    this.tableModel.addColumnHeader(column);
  }

  /**
   * Adds a cell to the current row. This method is usually called by a contained ColumnTag
   * @param cell Cell to add to the current row
   */
  public void addCell(Cell cell) {
    // check if null: could be null if list is empty, we don't need to fill rows
    if (this.currentRow != null) {
      this.currentRow.addCell(cell);
    }
  }

  /**
   * Is this the first iteration?
   * @return boolean <code>true</code> if this is the first iteration
   */
  protected boolean isFirstIteration() {
    // in first iteration this.rowNumber is 1
    // (this.rowNumber is incremented in doAfterBody)
    return this.rowNumber == 1;
  }

  /**
   * Displaytag requires commons-lang 2.x or better; it is not compatible with earlier versions.
   * @throws JspTagException if the wrong library, or no library at all, is found.
   */
  public static void checkCommonsLang() throws JspTagException {
    if (commonsLangChecked) {
      return;
    }

    try { // Do they have commons lang ?

      Class stringUtils = Class.forName("org.apache.commons.lang.StringUtils");

      try {
        // this method is new in commons-lang 2.0
        stringUtils.getMethod("capitalize", new Class[] { String.class });
      } catch (NoSuchMethodException ee) {
        throw new JspTagException(
            "\n\nYou appear to have an INCOMPATIBLE VERSION of the Commons Lang library.  \n" +
                "Displaytag requires version 2 of this library, and you appear to have a prior version in \n" +
                "your classpath.  You must remove this prior version AND ensure that ONLY version 2 is in \n" +
                "your classpath.\n " +
                "If commons-lang-1.x is in your classpath, be sure to remove it. \n" +
                "Be sure to delete all cached or temporary jar files from your application server; Tomcat \n" +
                "users should be sure to also check the CATALINA_HOME/shared folder; you may need to \n" +
                "restart the server. \n" +
                "commons-lang-2.jar is available in the displaytag distribution, or from the Jakarta \n" +
            "website at http://jakarta.apache.org/commons \n\n.");
      }
    } catch (ClassNotFoundException e) {
      throw new JspTagException(
          "You do not appear to have the Commons Lang library, version 2.  " +
              "commons-lang-2.jar is available in the displaytag distribution, or from the Jakarta website at " +
          "http://jakarta.apache.org/commons .  ");
    }

    commonsLangChecked = true;
  }

  /**
   * init the href object used to generate all the links for pagination, sorting, exporting.
   * @param requestHelper request helper used to extract the base Href
   */
  protected void initHref(RequestHelper requestHelper) {
    // get the href for this request
    Href normalHref = requestHelper.getHref();

    if (this.requestUri != null) {
      // if user has added a requestURI create a new href
      // call encodeURL to preserve session id when cookies are disabled
      String encodedURI = ((HttpServletResponse) this.pageContext.getResponse()).encodeURL(this.requestUri);
      this.baseHref = new Href(encodedURI);

      // ... and copy parameters from the current request
      //Map parameterMap = normalHref.getParameterMap();
      //this.baseHref.addParameterMap(parameterMap);
    } else {
      // simply copy href
      this.baseHref = normalHref;
    }
  }

  /**
   * called to clean up instance variables at the end of tag evaluation.
   */
  private void cleanUp() {
    // clean up
    this.listHelper = null;
    this.export = false;
    this.currentMediaType = null;
    this.decoratorName = null;
    this.pagesize = 0;
    this.length = 0;
    this.offset = 0;
    this.defaultSortedColumn = -1;
    this.rowNumber = 1;
    this.list = null;
    this.sortFullTable = null;
    this.doAfterBodyExecuted = false;
    this.currentRow = null;
    this.tableModel = null;
    this.requestUri = null;
    this.paramEncoder = null;
    this.footer = null;
    this.caption = null;
  }

  /**
   * If no columns are provided, automatically add them from bean properties. Get the first object in the list and
   * get all the properties (except the "class" property which is automatically skipped). Of course this isn't
   * possible for empty lists.
   * @since 1.0
   */
  protected void describeEmptyTable() {
    this.tableIterator = IteratorUtils.getIterator(this.list);

    if (this.tableIterator.hasNext()) {
      Object iteratedObject = this.tableIterator.next();
      Map objectProperties = new HashMap();

      // if it's a map already use key names for column headers
      if (iteratedObject instanceof Map) {
        objectProperties = (Map) iteratedObject;
      } else {
        try {
          objectProperties = BeanUtils.describe(iteratedObject);
        } catch (Exception e) {
          log.warn("Incapable d'ajouter automatiquement la colonne : " + e.getMessage(), e);
        }
      }

      // iterator on properties names
      Iterator propertiesIterator = objectProperties.keySet().iterator();

      while (propertiesIterator.hasNext()) {
        // get the property name
        String propertyName = (String) propertiesIterator.next();

        // dont't want to add the standard "class" property
        if (!"class".equals(propertyName)) {
          // creates a new header and add to the table model
          HeaderCell headerCell = new HeaderCell();
          headerCell.setBeanPropertyName(propertyName);
          this.tableModel.addColumnHeader(headerCell);
        }
      }
    }
  }

  /**
   * called when data are not displayed in a html page but should be exported.
   * @return int EVAL_PAGE or SKIP_PAGE
   * @throws JspException generic exception
   */
  protected int doExport() throws JspException {
    BaseExportView exportView = null;
    boolean exportFullList = this.properties.getExportFullList();

    if (log.isDebugEnabled()) {
      log.debug("[" + getId() + "] currentMediaType=" + this.currentMediaType);
    }

    boolean exportHeader = this.properties.getExportHeader(this.currentMediaType);

    exportView = ExportViewFactory.getView(this.currentMediaType,
        this.tableModel, exportFullList, exportHeader);

    String mimeType = exportView.getMimeType();
    String exportString = exportView.doExport();

    String filename = properties.getExportFileName(this.currentMediaType);

    return writeExport(mimeType, exportString, filename);
  }

  /**
   * Will write the export. The default behavior is to write directly to the response. If the ResponseOverrideFilter
   * is configured for this request, will instead write the export content to a StringBuffer in the Request object.
   * @param mimeType mime type to set in the response
   * @param exportString String
   * @param filename name of the file to be saved. Can be null, if set the content-disposition header will be added.
   * @return int
   * @throws JspException for errors in resetting the response or in writing to out
   */
  protected int writeExport(String mimeType, String exportString,
      String filename) throws JspException {
    HttpServletResponse response = (HttpServletResponse) this.pageContext.getResponse();
    JspWriter out = this.pageContext.getOut();

    HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
    StringBuffer bodyBuffer = (StringBuffer) request.getAttribute(FILTER_CONTENT_OVERRIDE_BODY);

    if (bodyBuffer != null) {
      // We are running under the export filter
      StringBuffer contentTypeOverride = (StringBuffer) request.getAttribute(FILTER_CONTENT_OVERRIDE_TYPE);
      contentTypeOverride.append(mimeType);
      bodyBuffer.append(exportString);

      if (StringUtils.isNotEmpty(filename)) {
        StringBuffer filenameOverride = (StringBuffer) request.getAttribute(FILTER_CONTENT_OVERRIDE_FILENAME);
        filenameOverride.append(filename);
      }
    } else {
      try {
        out.clear();
      } catch (IOException e) {
        throw new JspException(
            "Unable to reset response before returning exported data");
      }

      response.setContentType(mimeType);

      if (StringUtils.isNotEmpty(filename)) {
        response.setHeader("Content-Disposition",
            "attachment; filename=\"" + filename + "\"");
      }

      try {
        out.write(exportString);
        out.flush();
      } catch (IOException e) {
        throw new JspException("IOException while writing data.");
      }
    }

    return SKIP_PAGE;
  }

  /**
   * returns the formatted export links section.
   * @return String export links section
   */
  protected String getExportLinks() {
    // Figure out what formats they want to export, make up a little string
    Href exportHref = new Href(this.baseHref);

    StringBuffer buffer = new StringBuffer();

    Iterator iterator = MediaTypeEnum.iterator();

    while (iterator.hasNext()) {
      MediaTypeEnum currentExportType = (MediaTypeEnum) iterator.next();

      if (this.properties.getAddExport(currentExportType)) {
        if (buffer.length() > 0) {
          buffer.append(this.properties.getExportBannerSeparator());
        }

        exportHref.addParameter(encodeParameter(
            TableTagParameters.PARAMETER_EXPORTTYPE),
            currentExportType.getCode());

        Anchor anchor = new Anchor(exportHref,
            this.properties.getExportLabel(currentExportType), null);
        buffer.append(anchor.toString());
      }
    }

    String[] exportOptions = { buffer.toString() };

    return MessageFormat.format(this.properties.getExportBanner(), exportOptions);
  }

  /**
   * Called by the setProperty tag to override some default behavior or text String.
   * @param propertyName String property name
   * @param propertyValue String property value
   */
  public void setProperty(String propertyName, String propertyValue) {
    this.properties.setProperty(propertyName, propertyValue);
  }

  /**
   * @see javax.servlet.jsp.tagext.Tag#release()
   */
  @Override
  public void release() {
    cleanUp();
    super.release();
  }

  /**
   * Returns the name.
   * @return String
   */
  public Object getName() {
    return this.name;
  }

  /**
   * encode a parameter name to be unique in the page using ParamEncoder.
   * @param parameterName parameter name to encode
   * @return String encoded parameter name
   */
  protected String encodeParameter(String parameterName) {
    // paramEncoder has been already instantiated?
    if (this.paramEncoder == null) {
      // use name and id to get the unique identifier
      this.paramEncoder = new ParamEncoder(this.id, this.name);
    }

    return this.paramEncoder.encodeParameterName(parameterName);
  }

  /**
   * Retourne la définition de largeur des colonnes de la liste.
   * @return la définition de largeur des colonnes de la liste.
   */
  public String getColgroup() {
    return colgroup;
  }

  /**
   * Fixer la définition de largeur des colonnes de la liste.
   * @param colgroup la définition de largeur des colonnes de la liste.
   */
  public void setColgroup(String colgroup) {
    this.colgroup = colgroup;
  }
}
