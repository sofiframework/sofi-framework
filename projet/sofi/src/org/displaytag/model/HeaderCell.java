package org.displaytag.model;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.util.Href;
import org.displaytag.util.HtmlAttributeMap;
import org.displaytag.util.HtmlTagUtil;
import org.displaytag.util.MultipleHtmlAttribute;
import org.displaytag.util.TagConstants;


/**
 * DataObject representing the column header. The header cell contains all the properties common to cells in the same
 * column.
 * @author fgiust
 * @version $Revision: 248 $ ($Author: j-f.brassard $)
 */
public class HeaderCell {
  /**
   * Map containing the html tag attributes for cells (td).
   */
  private HtmlAttributeMap htmlAttributes;

  /**
   * Map containing the html tag attributes for header cells (td).
   */
  private HtmlAttributeMap headerAttributes;

  /**
   * base href for creating dynamic links.
   */
  private Href href;

  /**
   * param name used in adding a link.
   */
  private String paramName;

  /**
   * property of the object where to get the param value from.
   */
  private String paramProperty;

  /**
   * column title.
   */
  private String title;

  /**
   * is the column sortable?
   */
  private boolean sortable;

  /**
   * ColumnDecorator.
   */
  private ColumnDecorator columnDecorator;

  /**
   * column number.
   */
  private int columnNumber;

  /**
   * is the column sorted?
   */
  private boolean alreadySorted;

  /**
   * property name to look up in the bean.
   */
  private String beanPropertyName;

  /**
   * show null values?
   */
  private boolean showNulls;

  /**
   * max length of cell content.
   */
  private int maxLength;

  /**
   * max number of words for cell content.
   */
  private int maxWords;

  /**
   * autolink url?
   */
  private boolean autoLink;

  /**
   * group the column?
   */
  private int group;

  /**
   * Aide contextuelle
   */
  private String aideContextuelle;

  /**
   * Génération de l'appel à une fonction de retour de valeurs automatique.
   */
  private boolean fonctionRetourValeur;

  /**
   * Génération de l'appel à une fonction de retour de valeurs
   * automatique dans un contexte Ajax (dans la même page)
   */
  private boolean fonctionRetourValeurAjax;

  /**
   * Le format d'affichage de la colonne.
   */
  private String format;

  /**
   * Le contexte de la page.
   */
  private PageContext pageContext;

  /**
   * Le nom de la cache à utiliser afin d'extraire la description
   * selon la valeur de la colonne.
   */
  private String cache;

  /**
   * Permet de spécifier un identifiant de sous-cache
   * @since SOFI 2.0.3
   */
  private Object sousCache;

  /**
   * Permet de spécifier un filtre pour la liste des objets de cache.
   * @since SOFI 2.0.3
   */
  private String filtre;

  /**
   * Propriété de l'objet cache qui sera utilisé pour l'affichage de la colonne.
   * @since SOFI 2.0.3
   */
  private String proprieteAffichage;

  /**
   * Spécifie si on doit converir la valeur de la colonne en format HTML.
   *
   * Défaut : true
   */
  private boolean convertirEnHtml = true;

  /**
   *  Spécifie le nombre de caractère maximal avant de forcer un saut de ligne.
   */
  private String sautLigneNombreCararactereMaximal;

  /**
   *   Spécifie le caractère qui est utilisé afin d'appliquer un saut de ligne.
   */
  private String sautLigneCararactereSeparateur;

  /**
   * getter for the grouping index.
   * @return 0 if the column is not grouped or the grouping order
   */
  public int getGroup() {
    return this.group;
  }

  /**
   * setter for the grouping index.
   * @param groupingOrder int grouping order (>0)
   */
  public void setGroup(int groupingOrder) {
    this.group = groupingOrder;
  }

  /**
   * is autolink enabled?
   * @return true if autolink is enabled for the column
   */
  public boolean getAutoLink() {
    return this.autoLink;
  }

  /**
   * enable or disable autolink for the column.
   * @param autoLinkEnabled boolean autolink enabled
   */
  public void setAutoLink(boolean autoLinkEnabled) {
    this.autoLink = autoLinkEnabled;
  }

  /**
   * getter for the max number of characters to display in the column.
   * @return int number of characters to display in the column
   */
  public int getMaxLength() {
    return this.maxLength;
  }

  /**
   * setter for the max number of characters to display in the column.
   * @param numOfChars number of characters to display in the column
   */
  public void setMaxLength(int numOfChars) {
    this.maxLength = numOfChars;
  }

  /**
   * getter for the max number of words to display in the column.
   * @return int number of words to display in the column
   */
  public int getMaxWords() {
    return this.maxWords;
  }

  /**
   * setter for the max number of words to display in the column.
   * @param numOfWords number of words to display in the column
   */
  public void setMaxWords(int numOfWords) {
    this.maxWords = numOfWords;
  }

  /**
   * Should null be displayed?
   * @return true null will be displayed in cell content
   */
  public boolean getShowNulls() {
    return this.showNulls;
  }

  /**
   * Enable or disable displaying of null values.
   * @param outputNulls boolean true if null should be displayed
   */
  public void setShowNulls(boolean outputNulls) {
    this.showNulls = outputNulls;
  }

  /**
   * Getter for the name of the property to look up in the bean.
   * @return String name of the property to look up in the bean
   */
  public String getBeanPropertyName() {
    return this.beanPropertyName;
  }

  /**
   * Setter for the name of the property to look up in the bean.
   * @param propertyName - name of the property to look up in the bean
   */
  public void setBeanPropertyName(String propertyName) {
    this.beanPropertyName = propertyName;
  }

  /**
   * Is the column already sorted?
   * @return true if the column already sorted
   */
  public boolean isAlreadySorted() {
    return this.alreadySorted;
  }

  /**
   * Setter for the sorted property (the column is actually sorted).
   */
  public void setAlreadySorted() {
    this.alreadySorted = true;
  }

  /**
   * Getter for the column number.
   * @return int column number
   */
  public int getColumnNumber() {
    return this.columnNumber;
  }

  /**
   * Setter for the column number.
   * @param number - int column number
   */
  public void setColumnNumber(int number) {
    this.columnNumber = number;
  }

  /**
   * Returns the columnDecorator object for this column.
   * @return ColumnDecorator
   */
  public ColumnDecorator getColumnDecorator() {
    return this.columnDecorator;
  }

  /**
   * Sets the columnDecorator object for this column.
   * @param decorator - the ColumnDecorator
   */
  public void setColumnDecorator(ColumnDecorator decorator) {
    this.columnDecorator = decorator;
  }

  /**
   * Is the column sortable?
   * @return true if the column is sortable
   */
  public boolean getSortable() {
    return this.sortable;
  }

  /**
   * is the column sortable?
   * @param isSortable - true if the column can be sorted
   */
  public void setSortable(boolean isSortable) {
    this.sortable = isSortable;
  }

  /**
   * Gets the column title.
   * @return the column title. If no title is specified the capitalized bean property name is returned
   */
  public String getTitle() {
    if (this.title != null) {
      return this.title;
    } else if (this.beanPropertyName != null) {
      return StringUtils.capitalize(this.beanPropertyName);
    }

    return "";
  }

  /**
   * Setter for the column title.
   * @param value - the column title
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * Retourne l'aide contextuelle
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return this.aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle
   * @param value l'aide contextuelle
   */
  public void setAideContextuelle(String value) {
    this.aideContextuelle = value;
  }

  /**
   * Returns the HtmlAttributeMap containg all the html attributes for the <strong>td</strong> tags.
   * @return HtmlAttributeMap with td attributes
   */
  public HtmlAttributeMap getHtmlAttributes() {
    return this.htmlAttributes;
  }

  /**
   * Sets the HtmlAttributeMap containg all the html attributes for the <strong>td</strong> tags.
   * @param attributes HtmlAttributeMap
   */
  public void setHtmlAttributes(HtmlAttributeMap attributes) {
    this.htmlAttributes = attributes;
  }

  /**
   * returns the HtmlAttributeMap containg all the html attributes for the <strong>th</strong> tag.
   * @return HtmlAttributeMap with th attributes
   */
  public HtmlAttributeMap getHeaderAttributes() {
    return this.headerAttributes;
  }

  /**
   * Sets the HtmlAttributeMap containg all the html attributes for the <strong>th</strong> tag.
   * @param attributes HtmlAttributeMap
   */
  public void setHeaderAttributes(HtmlAttributeMap attributes) {
    this.headerAttributes = attributes;
  }

  /**
   * Adds a css class to the html "class" attribute.
   * @param cssClass String
   */
  public void addHeaderClass(String cssClass) {
    // null safe
    if (StringUtils.isBlank(cssClass)) {
      return;
    }

    Object classAttributes = this.headerAttributes.get(TagConstants.ATTRIBUTE_CLASS);

    // handle multiple values
    if (classAttributes == null) {
      this.headerAttributes.put(TagConstants.ATTRIBUTE_CLASS,
          new MultipleHtmlAttribute(cssClass));
    } else {
      ((MultipleHtmlAttribute) classAttributes).addAttributeValue(cssClass);
    }
  }

  /**
   * return the open tag for a cell (td).
   * @return String &lt;td> tag with attributes
   */
  public String getOpenTag() {
    return HtmlTagUtil.createOpenTagString(TagConstants.TAGNAME_COLUMN,
        this.htmlAttributes);
  }

  /**
   * return the open tag for a column header (th).
   * @return String &lt;th&gt; tag with attributes
   */
  public String getHeaderOpenTag() {
    return HtmlTagUtil.createOpenTagString(TagConstants.TAGNAME_COLUMN_HEADER,
        this.headerAttributes);
  }

  /**
   * return the closing tag for a cell (td).
   * @return String &lt;/td&gt;
   */
  public String getCloseTag() {
    return TagConstants.TAG_OPENCLOSING + TagConstants.TAGNAME_COLUMN +
        TagConstants.TAG_CLOSE;
  }

  /**
   * return the closing tag for a column header (th).
   * @return String &lt;/th&gt;
   */
  public String getHeaderCloseTag() {
    return TagConstants.TAG_OPENCLOSING + TagConstants.TAGNAME_COLUMN_HEADER +
        TagConstants.TAG_CLOSE;
  }

  /**
   * Setter for the href to be used for dinamic links in cells.
   * @param baseHref base href for links
   */
  public void setHref(Href baseHref) {
    this.href = baseHref;
  }

  /**
   * Getter for the href to be used for dinamic links in cells.
   * @return Href base href for links
   */
  public Href getHref() {
    return this.href;
  }

  /**
   * Setter for the name of the param to add to links.
   * @param name name of the param
   */
  public void setParamName(String name) {
    this.paramName = name;
  }

  /**
   * Getter for the name of the param to add to links.
   * @return String name of the param
   */
  public String getParamName() {
    return this.paramName;
  }

  /**
   * Setter for the name of the property to look up in bean to get the param value for links.
   * @param property name of the property to look up in bean to get the param value for links
   */
  public void setParamProperty(String property) {
    this.paramProperty = property;
  }

  /**
   * Getter for the name of the property to look up in bean to get the param value for links.
   * @return String name of the property to look up in bean to get the param value for links
   */
  public String getParamProperty() {
    return this.paramProperty;
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE).append("columnNumber",
        this.columnNumber).append("title", this.title)
        .append("beanPropertyName",
            this.beanPropertyName).toString();
  }

  public void setFonctionRetourValeur(boolean fonctionRetourValeur) {
    this.fonctionRetourValeur = fonctionRetourValeur;
  }

  public boolean isFonctionRetourValeur() {
    return fonctionRetourValeur;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getFormat() {
    return format;
  }

  public void setPageContext(PageContext pageContext) {
    this.pageContext = pageContext;
  }

  public PageContext getPageContext() {
    return pageContext;
  }

  public void setCache(String cache) {
    this.cache = cache;
  }

  public String getCache() {
    return cache;
  }

  public void setFonctionRetourValeurAjax(boolean fonctionRetourValeurAjax) {
    this.fonctionRetourValeurAjax = fonctionRetourValeurAjax;
  }

  public boolean isFonctionRetourValeurAjax() {
    return fonctionRetourValeurAjax;
  }

  public void setConvertirEnHtml(boolean convertirEnHtml) {
    this.convertirEnHtml = convertirEnHtml;
  }

  public boolean isConvertirEnHtml() {
    return convertirEnHtml;
  }

  public void setSautLigneNombreCararactereMaximal(
      String sautLigneNombreCararactereMaximal) {
    this.sautLigneNombreCararactereMaximal = sautLigneNombreCararactereMaximal;
  }

  public String getSautLigneNombreCararactereMaximal() {
    return sautLigneNombreCararactereMaximal;
  }

  public void setSautLigneCararactereSeparateur(
      String sautLigneCararactereSeparateur) {
    this.sautLigneCararactereSeparateur = sautLigneCararactereSeparateur;
  }

  public String getSautLigneCararactereSeparateur() {
    return sautLigneCararactereSeparateur;
  }

  public void setSousCache(Object sousCache) {
    this.sousCache = sousCache;
  }

  public Object getSousCache() {
    return sousCache;
  }

  public void setFiltre(String filtre) {
    this.filtre = filtre;
  }

  public String getFiltre() {
    return filtre;
  }

  public void setProprieteAffichage(String proprieteAffichage) {
    this.proprieteAffichage = proprieteAffichage;
  }

  public String getProprieteAffichage() {
    return proprieteAffichage;
  }
}
