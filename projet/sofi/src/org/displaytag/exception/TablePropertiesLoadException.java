package org.displaytag.exception;


/**
 * Runtime exception thrown for problems in loading the (standard or user defined) property file.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class TablePropertiesLoadException extends BaseNestableRuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 8202672650547717136L;

  /**
   * Constructor for TablePropertiesLoadException.
   * @param source Class where the exception is generated
   * @param propertiesFileName properties file name
   * @param cause previous Exception
   */
  public TablePropertiesLoadException(Class source, String propertiesFileName,
      Throwable cause) {
    super(source, "Unable to load file " + propertiesFileName, cause);
  }

  /**
   * @return SeverityEnum.ERROR
   * @see org.displaytag.exception.BaseNestableRuntimeException#getSeverity()
   * @see org.displaytag.exception.SeverityEnum
   */
  @Override
  public SeverityEnum getSeverity() {
    return SeverityEnum.ERROR;
  }
}
