package org.displaytag.exception;


/**
 * Exception thrown when an invalid value is given for a tag attribute.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class InvalidTagAttributeValueException
extends BaseNestableJspTagException {
  /**
   * 
   */
  private static final long serialVersionUID = -4517808329313309577L;

  /**
   * Constructor for InvalidTagAttributeValueException.
   * @param source Class where the exception is generated
   * @param attributeName String attribute name
   * @param attributeValue String attribute value (invalid)
   */
  public InvalidTagAttributeValueException(Class source, String attributeName,
      String attributeValue) {
    super(source,
        "Invalid value for attribute \"" + attributeName + "\" value=\"" +
            attributeValue + "\"");
  }

  /**
   * @return SeverityEnum.ERROR
   * @see org.displaytag.exception.BaseNestableJspTagException#getSeverity()
   * @see org.displaytag.exception.SeverityEnum
   */
  @Override
  public SeverityEnum getSeverity() {
    return SeverityEnum.ERROR;
  }
}
