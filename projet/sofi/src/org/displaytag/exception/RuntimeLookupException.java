package org.displaytag.exception;


/**
 * runtime exception thrown during sorting when a checked exception can't be used.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class RuntimeLookupException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 6050709029130989010L;

  /**
   * @param sourceClass class where the exception is thrown
   * @param property object property who caused the exception
   * @param cause previous (checked) exception
   */
  public RuntimeLookupException(Class sourceClass, String property,
      BaseNestableJspTagException cause) {
    super("LookupException while trying to fetch property \"" + property +
        "\"\nCause:     " + cause.getMessage());
  }
}
