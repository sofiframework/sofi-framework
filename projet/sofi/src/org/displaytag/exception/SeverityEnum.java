package org.displaytag.exception;

import org.apache.commons.lang.enums.Enum;


/**
 * Enumeration for logging severities.
 * @author fgiust
 * @version $Revision: 342 $ ($Author: j-f.brassard $)
 */
public final class SeverityEnum extends Enum {
  /**
   * 
   */
  private static final long serialVersionUID = -5649715710779283220L;

  /**
   * Severity FATAL.
   */
  public static final SeverityEnum FATAL = new SeverityEnum("fatal");

  /**
   * Severity ERROR.
   */
  public static final SeverityEnum ERROR = new SeverityEnum("error");

  /**
   * Severity WARN.
   */
  public static final SeverityEnum WARN = new SeverityEnum("warn");

  /**
   * Severity INFO.
   */
  public static final SeverityEnum INFO = new SeverityEnum("info");

  /**
   * Severity DEBUG.
   */
  public static final SeverityEnum DEBUG = new SeverityEnum("debug");

  /**
   * private constructor. Use only constants
   * @param severity Severity name as String
   */
  private SeverityEnum(String severity) {
    super(severity);
  }
}
