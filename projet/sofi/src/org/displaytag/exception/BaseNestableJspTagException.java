package org.displaytag.exception;

import javax.servlet.jsp.JspTagException;


/**
 * Base exception: extendes JspTagException providing logging and exception nesting functionalities.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public abstract class BaseNestableJspTagException extends JspTagException {
  /**
   * 
   */
  private static final long serialVersionUID = 3563262807191629899L;

  /**
   * Class where the exception has been generated.
   */
  private final Class sourceClass;

  /**
   * previous exception.
   */
  private Throwable nestedException;

  /**
   * Instantiate a new BaseNestableJspTagException.
   * @param source Class where the exception is generated
   * @param message message
   */
  public BaseNestableJspTagException(Class source, String message) {
    super(message);
    this.sourceClass = source;
  }

  /**
   * Instantiate a new BaseNestableJspTagException.
   * @param source Class where the exception is generated
   * @param message message
   * @param cause previous Exception
   */
  public BaseNestableJspTagException(Class source, String message,
      Throwable cause) {
    super(message);
    this.sourceClass = source;
    this.nestedException = cause;
  }

  /**
   * returns the previous exception.
   * @return Throwable previous exception
   */
  @Override
  public Throwable getCause() {
    return this.nestedException;
  }

  /**
   * basic toString. Returns the message plus the previous exception (if a previous exception exists).
   * @return String
   */
  @Override
  public String toString() {
    StringBuffer buffer = new StringBuffer();

    String className = this.sourceClass.getName();
    className = className.substring(className.lastIndexOf("."));

    buffer.append("Exception: ");
    buffer.append("[").append(className).append("] ");
    buffer.append(getMessage());

    if (this.nestedException != null) {
      buffer.append("\nCause:     ");
      buffer.append(this.nestedException.getMessage());
    }

    return buffer.toString();
  }

  /**
   * subclasses need to define the getSeverity method to provide correct severity for logging.
   * @return SeverityEnum exception severity
   * @see org.displaytag.exception.SeverityEnum
   */
  public abstract SeverityEnum getSeverity();
}
