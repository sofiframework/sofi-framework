package org.displaytag.pagination;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.properties.TableProperties;


/**
 * <p>
 * Utility class that chops up a List of objects into small bite size pieces
 * that are more suitable for display.
 * </p>
 * <p>
 * This class is a stripped down version of the WebListHelper from Tim Dawson (tdawson@is.com)
 * </p>
 * @author epesh
 * @version $Revision: 85 $ ($Author: guillaume.poirier $)
 */
public class SmartListHelper {
  /**
   * logger
   */
  private static Log log = LogFactory.getLog(SmartListHelper.class);

  /**
   * full list
   */
  protected List fullList;

  /**
   * size of the full list
   */
  protected int fullListSize;

  /**
   * number of items in a page
   */
  protected int pageSize;

  /**
   * number of pages
   */
  protected int pageCount;

  /**
   * index of current page
   */
  protected int currentPage;

  /**
   * TableProperties
   */
  protected TableProperties properties = null;

  /**
   * Returns the computed number of pages it would take to show all the
   * elements in the list given the pageSize we are working with.
   * @return int computed number of pages
   */
  protected int computedPageCount() {
    int result = 0;

    if ((this.fullList != null) && (this.pageSize > 0)) {
      int size = this.fullListSize;
      int div = size / this.pageSize;
      int mod = size % this.pageSize;
      result = (mod == 0) ? div : (div + 1);
    }

    return result;
  }

  /**
   * Set's the page number that the user is viewing.
   * @param pageNumber page number
   */
  public void setCurrentPage(int pageNumber) {

    if ((pageNumber < 1) ||
        ((pageNumber != 1) && (pageNumber > this.pageCount))) {
      // invalid page: better don't throw an exception, since this could easily happen
      // (list changed, user bookmarked the page)
      this.currentPage = 1;
    } else {
      this.currentPage = pageNumber;
    }
  }
}
