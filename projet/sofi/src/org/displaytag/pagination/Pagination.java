package org.displaytag.pagination;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Pagination {

  /**
   * logger.
   */
  private static Log log = LogFactory.getLog(Pagination.class);

  /**
   * MessageFormat for urls.
   */
  protected MessageFormat urlFormat;

  /**
   * first page.
   */
  protected Integer firstPage;

  /**
   * last page.
   */
  protected Integer lastPage;

  /**
   * previous page.
   */
  protected Integer previousPage;

  /**
   * next page.
   */
  protected Integer nextPage;

  /**
   * List containg NumberedPage objects.
   * @see org.displaytag.pagination.NumberedPage
   */
  protected List pages = new ArrayList();

  // J-F
  private Integer maxPages;


  /**
   * Adds a page.
   * @param number int page number
   * @param isSelected is the page selected?
   */
  public void addPage(int number, boolean isSelected) {
    this.pages.add(new NumberedPage(number, isSelected));
  }

  /**
   * first page selected?
   * @return boolean
   */
  public boolean isFirst() {
    return this.firstPage == null;
  }

  /**
   * last page selected?
   * @return boolean
   */
  public boolean isLast() {
    return this.lastPage == null;
  }

  /**
   * only one page?
   * @return boolean
   */
  public boolean isOnePage()
  {
    return (this.pages == null) || this.pages.size() <= 1;
  }

  /**
   * Gets the number of the first page.
   * @return Integer number of the first page
   */
  public Integer getFirst() {
    return this.firstPage;
  }

  /**
   * Sets the number of the first page.
   * @param first Integer number of the first page
   */
  public void setFirst(Integer first) {
    this.firstPage = first;
  }

  /**
   * Gets the number of the last page.
   * @return Integer number of the last page
   */
  public Integer getLast() {
    return this.lastPage;
  }

  /**
   * Sets the number of the last page.
   * @param last Integer number of the last page
   */
  public void setLast(Integer last) {
    this.lastPage = last;
  }

  /**
   * Gets the number of the previous page.
   * @return Integer number of the previous page
   */
  public Integer getPrevious() {
    return this.previousPage;
  }

  /**
   * Sets the number of the previous page.
   * @param previous Integer number of the previous page
   */
  public void setPrevious(Integer previous) {
    this.previousPage = previous;
  }

  /**
   * Gets the number of the next page.
   * @return Integer number of the next page
   */
  public Integer getNext() {
    return this.nextPage;
  }

  /**
   * Sets the number of the next page.
   * @param next Integer number of the next page
   */
  public void setNext(Integer next) {
    this.nextPage = next;
  }

  /**
   * Returns the appropriate banner for the pagination.
   * @param numberedPageFormat String to be used for a not selected page
   * @param numberedPageSelectedFormat String to be used for a selected page
   * @param numberedPageSeparator separator beetween pages
   * @param fullBanner String basic banner
   * @return String formatted banner whith pages
   */
  public String getFormattedBanner(
      String numberedPageFormat,
      String numberedPageSelectedFormat,
      String numberedPageSeparator,
      String fullBanner, String messageListeNavigation)
  {

    StringBuffer buffer = new StringBuffer(100);

    // Liste des page numéré.
    Iterator pageIterator = this.pages.iterator();

    // J-F
    Integer pageSelectionne = null;

    while (pageIterator.hasNext()) {

      // Extraire le numéro de la page.
      NumberedPage page = (NumberedPage) pageIterator.next();

      Integer pageNumber = new Integer(page.getNumber());

      String urlString = this.urlFormat.format(new Object[] { pageNumber });

      // nécessaire pour MessageFormat
      Object[] pageObjects = { pageNumber, urlString };

      // selected page need a different formatter
      if (page.getSelected())
      {
        // J-F
        pageSelectionne = pageNumber;
        buffer.append(MessageFormat.format(numberedPageSelectedFormat, pageObjects));
      }
      else
      {
        buffer.append(MessageFormat.format(numberedPageFormat, pageObjects));
      }

      // next? add page separator
      if (pageIterator.hasNext())
      {
        buffer.append(numberedPageSeparator);
      }
    }

    // J-F : Modification permettant de ne pas traité la pagination
    // multipage. Dans ce cas-la, seulement une description du statut
    // de la navigation sera affiché. Comme par exemple: page 1 de 2

    // String for numbered pages
    String numberedPageString;
    if (numberedPageFormat.indexOf("a href") == -1) {

      //J-F
      Object[] obj = new Object[2];
      obj[0]=pageSelectionne;
      obj[1]=getMaxPages();

      numberedPageString = MessageFormat.format(numberedPageFormat, obj);

    }else {
      numberedPageString = buffer.toString();
    }

    //  Object array
    //  {0} full String for numbered pages
    //  {1} first page url
    //  {2} previous page url
    //  {3} next page url
    //  {4} last page url
    Object[] pageObjects =
      {
        numberedPageString,
        this.urlFormat.format(new Object[] { getFirst()}),
        this.urlFormat.format(new Object[] { getPrevious()}),
        this.urlFormat.format(new Object[] { getNext()}),
        this.urlFormat.format(new Object[] { getLast()}),
      };

    // return the full banner
    Object fullBannerObj[] = new Object[1];
    fullBannerObj[0] = MessageFormat.format(fullBanner, pageObjects);

    String fullBannerFormat  = MessageFormat.format(messageListeNavigation, fullBannerObj);
    return fullBannerFormat;
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
    .append("firstPage", this.firstPage)
    .append("lastPage", this.lastPage)
    .append("nextPage", this.nextPage)
    .append("previousPage", this.previousPage)
    .append("pages", this.pages)
    .append("urlFormat", this.urlFormat)
    .toString();
  }
  //J-F
  public Integer getMaxPages() {
    return maxPages;
  }

  public void setMaxPages(Integer maxPages) {
    this.maxPages = maxPages;
  }
}