package org.displaytag.util;

import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Anchor object used to output an html link (an &lt;a> tag).
 * @author fgiust
 * @version $Revision: 267 $ ($Author: j-f.brassard $)
 */
public class Anchor {
  /**
   * Href object to be written in the "href" html attribute.
   */
  private Href href;

  /**
   * link body text.
   */
  private String linkText;

  /**
   * HashMap containing all the html attributes.
   */
  private HtmlAttributeMap attributeMap = new HtmlAttributeMap();

  /**
   * Aide contexteuelle
   */
  private String aideContextuelle;

  /**
   * Traitement ajax
   */
  private boolean ajax;

  /**
   * Lien Ajax;
   */
  private String lienAjax;

  /**
   * Creates a new anchor with the supplied body text.
   * @param linkBody String body text
   */
  public Anchor(String linkBody) {
    this.linkText = linkBody;
  }

  /**
   * Creates a new Anchor whit the supplied Href.
   * @param linkHref Href
   */
  public Anchor(Href linkHref) {
    this.href = linkHref;
  }

  /**
   * Creates a new Anchor whit the supplied Href and body text.
   * @param linkHref baseHref
   * @param linkBody String link body
   */
  public Anchor(Href linkHref, String linkBody, String aideContextuelle) {
    this.href = linkHref;
    this.linkText = linkBody;
    this.aideContextuelle = aideContextuelle;
  }

  public Anchor(String lienAjax, String linkBody, String aideContextuelle,
      boolean ajax) {
    this.lienAjax = lienAjax;
    this.linkText = linkBody;
    this.aideContextuelle = aideContextuelle;
    this.ajax = ajax;
  }

  /**
   * setter the anchor Href.
   * @param linkHref Href
   */
  public void setHref(Href linkHref) {
    this.href = linkHref;
  }

  /**
   * setter for the link body text.
   * @param linkBody String
   */
  public void setText(String linkBody) {
    this.linkText = linkBody;
  }

  /**
   * add a "class" attribute to the html link.
   * @param cssClass String
   */
  public void setClass(String cssClass) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_CLASS, cssClass);
  }

  /**
   * add a "style" attribute to the html link.
   * @param style String
   */
  public void setStyle(String style) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_STYLE, style);
  }

  /**
   * add a "title" attribute to the html link.
   * @param title String
   */
  public void setTitle(String title) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_TITLE, title);
  }

  /**
   * Ajouter la propriété id aux attributs du tag.
   * @param id
   */
  public void setId(String id) {
    this.attributeMap.put(TagConstants.ATTRIBUTE_ID, id);
  }

  /**
   * returns the href attribute, surrounded by quotes and prefixed with " href=".
   * @return String <code> href ="<em>href value</em>"</code> or an emty String if Href is null
   */
  private String getHrefString() {
    if ((this.href == null) && (this.lienAjax == null)) {
      return "";
    }

    if (!isAjax()) {
      return " href=\"" + href.toString() + "\"";
    } else {
      return " href=\"javascript:void(0);" + lienAjax + "\"";
    }
  }

  /**
   * Returns the &lt;a> tag, with rendered href and any html attribute.
   * @return String
   */
  public String getOpenTag() {
    // append all attributes
    StringBuffer buffer = new StringBuffer();

    // shortcut for links with no attributes
    if (this.attributeMap.size() == 0) {
      buffer.append(TagConstants.TAG_OPEN);
      buffer.append(TagConstants.TAGNAME_ANCHOR);
      buffer.append(getHrefString());

      if (!UtilitaireString.isVide(aideContextuelle)) {
        buffer.append(" title=\"");
        buffer.append(aideContextuelle);
        buffer.append("\"");
      }

      buffer.append(TagConstants.TAG_CLOSE);

      return buffer.toString();
    } else {
      buffer.append(TagConstants.TAG_OPEN).append(TagConstants.TAGNAME_ANCHOR)
      .append(getHrefString());

      buffer.append(this.attributeMap);

      if (!UtilitaireString.isVide(aideContextuelle)) {
        buffer.append(" title=\"");
        buffer.append(aideContextuelle);
        buffer.append("\"");
      }

      buffer.append(TagConstants.TAG_CLOSE);
    }

    return buffer.toString();
  }

  /**
   * returns the &lt;/a> tag.
   * @return String
   */
  public String getCloseTag() {
    return TagConstants.TAG_OPENCLOSING + TagConstants.TAGNAME_ANCHOR +
        TagConstants.TAG_CLOSE;
  }

  /**
   * returns the full &lt;a href="">body&lt;/a>.
   * @return String html link
   */
  @Override
  public String toString() {
    return getOpenTag() + this.linkText + getCloseTag();
  }

  public void setAjax(boolean ajax) {
    this.ajax = ajax;
  }

  public boolean isAjax() {
    return ajax;
  }

  public void setLienAjax(String lienAjax) {
    this.lienAjax = lienAjax;
  }

  public String getLienAjax() {
    return lienAjax;
  }
}
