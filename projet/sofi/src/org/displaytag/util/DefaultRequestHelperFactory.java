package org.displaytag.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;


/**
 * Default RequestHelperFactory implementation. Returns instaces of {@link DefaultRequestHelper}.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class DefaultRequestHelperFactory implements RequestHelperFactory {
  /**
   * @see org.displaytag.util.RequestHelperFactory#getRequestHelperInstance(javax.servlet.jsp.PageContext)
   */
  @Override
  public RequestHelper getRequestHelperInstance(PageContext pageContext) {
    return new DefaultRequestHelper((HttpServletRequest) pageContext.getRequest(),
        (HttpServletResponse) pageContext.getResponse());
  }
}
