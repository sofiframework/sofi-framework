package org.displaytag.export;

import org.apache.commons.lang.StringUtils;
import org.displaytag.model.TableModel;


/**
 * Export view for comma separated value exporting.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class CsvView extends BaseExportView {
  /**
   * @see org.displaytag.export.BaseExportView#BaseExportView(TableModel, boolean, boolean)
   */
  public CsvView(TableModel tableModel, boolean exportFullList,
      boolean includeHeader) {
    super(tableModel, exportFullList, includeHeader);
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowStart()
   */
  @Override
  protected String getRowStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowEnd()
   */
  @Override
  protected String getRowEnd() {
    return "\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellStart()
   */
  @Override
  protected String getCellStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellEnd()
   */
  @Override
  protected String getCellEnd() {
    return ",";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentStart()
   */
  @Override
  protected String getDocumentStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentEnd()
   */
  @Override
  protected String getDocumentEnd() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendCellEnd()
   */
  @Override
  protected boolean getAlwaysAppendCellEnd() {
    return false;
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendRowEnd()
   */
  @Override
  protected boolean getAlwaysAppendRowEnd() {
    return true;
  }

  /**
   * @see org.displaytag.export.BaseExportView#getMimeType()
   */
  @Override
  public String getMimeType() {
    return "text/csv";
  }

  /**
   * Escaping for csv format.
   * <ul>
   * <li>Quotes inside quoted strings are escaped with a /</li>
   * <li>Fields containings newlines or , are surrounded by ""</li>
   * </ul>
   * Note this is the standard CVS format and it's not handled well by excel.
   * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
   */
  @Override
  protected Object escapeColumnValue(Object value) {
    if (value != null) {
      String stringValue = StringUtils.trim(value.toString());

      if (!StringUtils.containsNone(stringValue, new char[] { '\n', ',' })) {
        return "\"" + StringUtils.replace(stringValue, "\"", "\\\"") + "\"";
      }

      return stringValue;
    }

    return null;
  }
}
