package org.displaytag.export;

import org.apache.commons.lang.StringEscapeUtils;
import org.displaytag.model.TableModel;


/**
 * Export view for xml exporting.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class XmlView extends BaseExportView {
  /**
   * @see org.displaytag.export.BaseExportView#BaseExportView(TableModel, boolean, boolean)
   */
  public XmlView(TableModel tableModel, boolean exportFullList,
      boolean includeHeader) {
    super(tableModel, exportFullList, includeHeader);
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowStart()
   */
  @Override
  protected String getRowStart() {
    return "<row>\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowEnd()
   */
  @Override
  protected String getRowEnd() {
    return "</row>\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellStart()
   */
  @Override
  protected String getCellStart() {
    return "<column>";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellEnd()
   */
  @Override
  protected String getCellEnd() {
    return "</column>\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentStart()
   */
  @Override
  protected String getDocumentStart() {
    return "<?xml version=\"1.0\"?>\n<table>\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentEnd()
   */
  @Override
  protected String getDocumentEnd() {
    return "</table>\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendCellEnd()
   */
  @Override
  protected boolean getAlwaysAppendCellEnd() {
    return true;
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendRowEnd()
   */
  @Override
  protected boolean getAlwaysAppendRowEnd() {
    return true;
  }

  /**
   * @see org.displaytag.export.BaseExportView#getMimeType()
   */
  @Override
  public String getMimeType() {
    return "text/xml";
  }

  /**
   * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
   */
  @Override
  protected Object escapeColumnValue(Object value) {
    if (value != null) {
      return StringEscapeUtils.escapeXml(value.toString());
    }

    return null;
  }
}
