package org.displaytag.export;

import org.apache.commons.lang.StringUtils;
import org.displaytag.model.TableModel;


/**
 * Export view for excel exporting.
 * @author fgiust
 * @version $Revision: 171 $ ($Author: j-f.brassard $)
 */
public class ExcelView extends BaseExportView {
  /**
   * @see org.displaytag.export.BaseExportView#BaseExportView(TableModel, boolean, boolean)
   */
  public ExcelView(TableModel tableModel, boolean exportFullList,
      boolean includeHeader) {
    super(tableModel, exportFullList, includeHeader);
  }

  /**
   * @see org.displaytag.export.BaseExportView#getMimeType()
   * @return "application/vnd.ms-excel"
   */
  @Override
  public String getMimeType() {
    return "application/vnd.ms-excel";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowStart()
   * @return ""
   */
  @Override
  protected String getRowStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getRowEnd()
   * @return "\n"
   */
  @Override
  protected String getRowEnd() {
    return "\n";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellStart()
   * @return ""
   */
  @Override
  protected String getCellStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getCellEnd()
   * @return "\t"
   */
  @Override
  protected String getCellEnd() {
    return "\t";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentStart()
   * @return ""
   */
  @Override
  protected String getDocumentStart() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getDocumentEnd()
   * @return ""
   */
  @Override
  protected String getDocumentEnd() {
    return "";
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendCellEnd()
   * @return false
   */
  @Override
  protected boolean getAlwaysAppendCellEnd() {
    return false;
  }

  /**
   * @see org.displaytag.export.BaseExportView#getAlwaysAppendRowEnd()
   * @return false
   */
  @Override
  protected boolean getAlwaysAppendRowEnd() {
    return false;
  }

  /**
   * Escaping for excel format.
   * <ul>
   * <li>Quotes inside quoted strings are escaped with a double quote</li>
   * <li>Fields are surrounded by "" (should be optional, but sometimes you get a "Sylk error" without those)</li>
   * </ul>
   * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
   */
  @Override
  protected Object escapeColumnValue(Object value) {
    if (value != null) {
      // quotes around fields are needed to avoid occasional "Sylk format invalid" messages from excel
      return "\"" +
      StringUtils.replace(StringUtils.trim(value.toString()), "\"", "\"\"") +
      "\"";
    }

    return null;
  }
}
