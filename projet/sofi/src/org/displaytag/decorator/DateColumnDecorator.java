package org.displaytag.decorator;

import java.text.SimpleDateFormat;

import org.displaytag.exception.DecoratorException;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;


/**
 * Décorateur de colonne java.util.Date.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class DateColumnDecorator implements ColumnDecorator {
  /** Format a appliquer à la date */
  protected String format = GestionParametreSysteme.getInstance().getString("formatDate");

  /** Constructeur par défaut */
  public DateColumnDecorator() {
  }

  /**
   * Décore une valeur de colonne en format de date.
   * @param columnValue
   * @return chaîne de caractère qui présente une date dans un format déterminé.
   * @throws org.displaytag.exception.DecoratorException
   */
  @Override
  public String decorate(Object columnValue) throws DecoratorException {
    String formatee = "";

    if (columnValue != null) {
      if (!(columnValue instanceof java.util.Date)) {
        throw new DecoratorException(this.getClass(),
            "La colonne décorée doit êter de type java.util.Date.");
      }

      SimpleDateFormat formatter = new SimpleDateFormat(format);
      formatee = formatter.format(columnValue);
    }

    return formatee;
  }
}
