<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE taglib  
  PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN"  
    "http://java.sun.com/dtd/web-jsptaglibrary_1_2.dtd">

<taglib>
  <!-- ============== Description de la librairie des balises JSP ============= -->
  <tlib-version>1.0</tlib-version>
  <jsp-version>1.2</jsp-version>
  <short-name>sofi-liste</short-name>
  <uri>http://www.sofiframework.org/taglib/sofi-liste</uri>
  <description>Librairie des balises permettant l'utilisation de la liste
de navigation de SOFI.</description>
  <tag>
    <name>listeNavigation</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ListeNavigationTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>listeNavigation</display-name>
    <description>
    Balise permettant de traiter un liste de navigation page par page
    avec tri automatisé et plusieurs fonctionnalité ajax.
    </description>    
    <attribute>
      <name>nomListe</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Spécifier le nom de la liste navigation qui accessible dans une portée
        request ou session. Utiliser les expressions régulière par exemple:
        ${listeClients}
      </description>
    </attribute>
    <attribute>
      <name>divId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Identifiant du DIV encapsulant la liste de navigation. On doit spécifier un nom lorsqu'on désire
        référencer un traitement vers la liste de navigation, par exemple lorsqu'on désire déplacer la page vers la navigation
        après une soumission de formulaire ex : action="/rechercheUtilisateur.do?methode=rechercher#LISTE_RECHERCHE_UTILISATEUR".
        Lors de l'utilisation de JSP 1.x, le nom du div de la liste est automatique le nom de la liste de navigation fixé par la
        propriété nomListe.
      </description>
    </attribute>  
    <attribute>
      <name>colgroup</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de découper les colonnes de la liste par groupe.
        Par exemple: 20%,60%,20% en pourcentage, 150,300,150 en pixels,
        s'il y un groupe de tailles égales consécutives vous pouvez
        utiliser: 3-20%, 40%, dans cet exemple il y a 3 colonnes de 20%
        et une de 40%
      </description>
    </attribute>   
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'appliquer un décorateur pour la liste navigation.
      </description>
    </attribute>
    <attribute>
      <name>action</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        URL à laquelle ce formulaire sera soumis. Permet également de choisir
        l'ActionMapping, qui permet d'obtenir le bean associé au formulaire.
      </description>
    </attribute>
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>id</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Nom de l'instance de l'occurence de la liste en traitement.
      </description>
    </attribute>
    <attribute>
      <name>trierPageSeulement</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Tri les occurences de la page seulement. Défaut est false.
      </description>
    </attribute>
    <attribute>
      <name>triDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>int</type>
      <description>
        Le numéro de colonne dont le tri par défaut est fait.
        La base est 0, 1, 2,...
        IMPORTANT : Depuis SOFI 1.8.2, le tri par défaut des composants ListeNavigation se
        fait via la méthode ajouterTriInitial(nomAttributAtrier, ascendant);. Cette propriété
        demeure utilisé lors de l'utilisation de collection.
      </description>
    </attribute>
    <attribute>
      <name>triDescendantDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Appliquer un tri descendant par défaut. Si non spécifier le tri
        sera ascendant par défaut.
        IMPORTANT : Depuis SOFI 1.8.2, le tri par défaut des composants ListeNavigation se
        fait via la méthode ajouterTriInitial(nomAttributAtrier, ascendant);. Cette propriété
        demeure utilisé lors de l'utilisation de collection.
      </description>
    </attribute>
    <attribute>
      <name>selection</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Spécifier les attributs qui spécifie unicité d'une sélection dans la
        liste. Souvent on spécifie l'attribut qui spécifie la clé d'un objet de transfert.
	Par exemple: codeUtilisateur. Si plusieurs veuillez les séparer les attributs par une virgule.
      </description>
    </attribute>  
    <attribute>
      <name>colonneLienDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Spécifier le numéro de colonne qui correspondera au lien par défaut
        lors de la sélection d'une ligne de la liste.
      </description>
    </attribute>  
    <attribute>
      <name>fonctionRetourValeur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer à true si vous désirez la génération automatique du retour des
        valeurs à la page parent. (Utile seulement pour les listes de
        valeurs).
      </description>
    </attribute>
    <attribute>
      <name>fonctionRetourValeurAjax</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer à true si vous désirez la liste de navigation doit retourner des valeurs 
        à une liste de valeur configurée Ajax.
      </description>
    </attribute>    
    <attribute>
      <name>messageListeVide</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer un message spécifique pour la liste lorsque
        le resultat est vide.
      </description>
    </attribute>  
    <attribute>
      <name>afficheEnteteSiListeVide</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>Fixer true si on veut l'entete et la naviguation même si la liste de navigation est vide.</description>
    </attribute>    
    <attribute>
      <name>ajax</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Fixer à true si vous désirez que la liste soit Ajax.
      </description>
    </attribute> 
    <attribute>
      <name>afficherNavigationBasListe</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer à true si vous désirez que la barre de navigation soit aussi
        afficher en bas de la liste.
      </description>
    </attribute> 
    <attribute>
      <name>iterateurMultiPage</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer true si vous désirez avoir un itérateur multi page.
      </description>
    </attribute> 
    <attribute>
      <name>nombreMaximalMultiPage</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer le nombre maximal de page afficher dans l'itérateur.
      </description>
    </attribute>
    <attribute>
      <name>gabaritBarreNavigation</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
       <description>
        Fixer la variable qui loge le gabarit de la barre de navigation.
      </description>
    </attribute> 
    <attribute>
      <name>gabaritBarreNavigationBasListe</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
       <description>
        Fixer la variable qui loge le gabarit de la barre de navigation
        en bas de la liste.
      </description>
    </attribute> 
    <attribute>
      <name>libelleResultat</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer le libellé du résultat de la liste de navigation.
      </description>
    </attribute> 
    <attribute>
      <name>varIndex</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Fixer le nom de variable qui va contenir l'index d'itération
        de la liste. L'index débute à 1.
      </description>
    </attribute>
    <example>../exemples/exemple_sofi-liste_listeNavigation.html</example>
  </tag>
  <tag>
    <name>colonne</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonne</display-name>
    <description>
    Balise permettant de traiter une colonne d'une liste de navigation page par page
    avec tri automatisé.
    </description>      
    <attribute>
      <name>property</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Nom d'attribut qui est contenu dans la colonne de la liste. Doit être
        accessible depuis une instance de la liste.
      </description>
    </attribute>
    <attribute>
      <name>paramId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Les identifiants des paramètres de url appelant la liste de valeurs.
        Si plusieurs identifiants, séparer les d'une virgule.
        
        Ex. noOrgns, noLot
      </description>
    </attribute>
    <attribute>
      <name>paramProperty</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
      	Propriété du bean.
        
        La suite de toutes les valeurs des propriétés à passer dans l'URL.
      
        Les propriétés des paramètres de url appelant la liste de valeurs.
        Si plusieurs propriétés, séparer les d'une virgule.
        
        La propriété de l'objet de transfert qui est la valeur du paramètre.
        
        La propriété du bean qui contient la valeur du ou des paramètres.
        Ex. noOrganisme, noLot
        
	IMPORTANT: Il est préférable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite reférez l'url spécifier 
	dans un var et placer dans la propriété href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>    
    <attribute>
      <name>paramName</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Correspond à la valeur de l'attribut sélectionné pour créer un
        lien hypertexte. Vous pouvez utiliser les expressions régulière Ex.
        ${client.codeUtilisateur} accessible dans la requête ou la session.
        
	IMPORTANT: Il est préférable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite reférez l'url spécifier 
	dans un var et placer dans la propriété href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>
    <attribute>
      <name>fonction</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Le nom de la fonction javascript qui doit être appelé lors de la
        sélection d'un élément de la liste. À utiliser seulement lors de
        l'utilisation de la liste dans une fenêtre flottante.
      </description>
    </attribute>
    <attribute>
      <name>parametresFonction</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Spécifier les attributs dont les valeurs doivent être envoyé à
        la fonction spécifier. Les attributs doivent être séparer de virgule,
        EX: nom,prenom. À utiliser seulement dans une fenêtre flottante.
      </description>
    </attribute>
    <attribute>
      <name>libelle</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier le nom de la colonne, vous pouvez spécifier une clé
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier l'aide contextuelle de la colonne, vous pouvez
        spécifier une clé
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelleCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier l'aide contextuelle pour une cellule spécifique.
      </description>
    </attribute>     
    <attribute>
      <name>trier</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous désirez trier cette colonne. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>lienHypertexteAutomatique</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous désirez un lien hypertexte automatique. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>href</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        L'URL de base pour la construction dynamique du lien hypertexte de la
        ressource à appeler lors du click sur le lien.
      </description>
    </attribute>
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer un décorateur pour une colonne.</description>
    </attribute>
    <attribute>
      <name>fonctionRetourValeur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Fixer à true si vous désirez que cette colonne utilise la fonction JS
        automatique du retour des valeurs à la page parent. (Utile seulement
        pour les listes de valeurs).
      </description>
    </attribute> 
    <attribute>
      <name>fonctionRetourValeurAjax</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer à true si vous désirez la liste de navigation doit retourner des valeurs 
        à une liste de valeur configurée Ajax.
      </description>
    </attribute>        
    <attribute>
      <name>format</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier un format si la colonne est un nombre.
        Ex. ###.#0
      </description>
    </attribute>  
    <attribute>
      <name>cache</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'extraire la valeur associé à une liste d'un objet cache.
      </description>
    </attribute>  
    <attribute>
      <name>traiterCorps</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'évaluer l'intérieur de la balise avec du contenu
        personnalisé.
        
	Défaut : false
      </description>
    </attribute>
    <attribute>
      <name>classCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier une classe CSS spécifique pour une cellule.
      </description>
    </attribute>  
    <attribute>
      <name>convertirEnHtml</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Spécifie si vous désirez la conversion en format HTML. Défaut : true
      </description>
    </attribute>    
    <attribute>
      <name>sautLigneNombreCararactereMaximal</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Spécifie le nombre de caractère maximal avant de forcer un saut de ligne.
      </description>
    </attribute> 
    <attribute>
      <name>sautLigneCararactereSeparateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Spécifie le caractère qui est utilisé afin d'appliquer un saut de ligne.
      </description>
    </attribute>
    <attribute>
      <name>sousCache</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        La valeur de sous-cache qui doit être utilisée pour retrouver la 
        liste des valeurs de cache.
      </description>
    </attribute>
    <attribute>
      <name>filtre</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        La nom de la classe de filtre qui doit être utilisée 
        pour filtrer la liste de cache.
      </description>
    </attribute>
    <attribute>
      <name>proprieteAffichage</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Le nom de la propriete à accéder pour afficher sa valeur. Seulement
        spécifier lorsqu'on veut accéder à une valeur qui n'est pas une
        description de base.
      </description>
    </attribute>
    <example>../exemples/exemple_sofi-liste_colonne.html</example>
  </tag>
  <tag>
    <name>colonneMultibox</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneMultiboxTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneMultibox</display-name>
    <description>
    Balise permettant de traiter une colonne décorant une boite à cocher
    d'une liste de navigation page par page.
    </description>       
    <attribute>
      <name>nomAttributMultibox</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>Spécifier le nom de la boite à cocher multiple.</description>
    </attribute>
    <attribute>
      <name>nomValeurMultibox</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Spécifier le nom d'attribut qui sera la valeur de la boite
        à cocher multiple.
      </description>
    </attribute> 
    <attribute>
      <name>nomListeSelectionnee</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Spécifier le nom de la liste contenant les éléments de la boite à
        cocher multiple qui sont sélectionné. Doit être un HashMap.
      </description>
    </attribute>
    <attribute>
      <name>proprieteActivation</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Spécifier le nom de méthode de l'objet de transfert qui permet de
        spécifier si la boite à cocher est actif ou pas.
      </description>
    </attribute> 
    <attribute>
      <name>href</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        L'URL de base pour la construction dynamique du lien hypertexte de la
        ressource à appeler lors du click sur une des cases à cocher.
      </description>
    </attribute>
    <attribute>
      <name>paramId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Les identifiants des paramètres de l'url appelant la liste de valeurs.
        Si plusieurs identifiants, séparer les d'une virgule.
        
        Ex. noOrgns, noLot
      </description>
    </attribute>
    <attribute>
      <name>paramProperty</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
      	Propriété du bean.
        
        La suite de toutes les valeurs des propriétés à passer dans l'URL.
      
        Les propriétés des paramètres de url appelant la liste de valeurs.
        Si plusieurs propriétés, séparer les d'une virgule.
        
        La propriété de l'objet de transfert qui est la valeur du paramètre.
        
        La propriété du bean qui contient la valeur du ou des paramètres.
        Ex. noOrganisme, noLot
        
	IMPORTANT: Il est préférable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite reférez l'url spécifier 
	dans un var et placer dans la propriété href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>    
    <attribute>
      <name>libelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier le nom de la colonne, vous pouvez spécifier une clé
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier l'aide contextuelle de la colonne, vous pouvez
        spécifier une clé
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelleCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier l'aide contextuelle pour une cellule spécifique.
      </description>
    </attribute>     
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer un décorateur pour une colonne.</description>
    </attribute>
    <attribute>
      <name>onClick</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier du code JavaScript sur la modification de la boite
        à cocher.
      </description>
    </attribute>
    <example>../exemples/exemple_sofi-liste_colonneMultibox.html</example>
  </tag>
  <tag>
    <name>colonneLibre</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneLibre</display-name>
    <description>
      Balise permettant de traiter une colonne avec une valeur arbitraire
      définit dans le body du tag.
    </description>
    <attribute>
      <name>libelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier le nom de la colonne, vous pouvez spécifier une clé
      </description>
    </attribute>    
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une classe CSS pour l'entete de la colonne.</description>
    </attribute>
    <attribute>
      <name>trier</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous désirez trier cette colonne. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>classCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier une classe CSS spécifique pour une cellule.
      </description>
    </attribute>  
    <attribute>
      <name>convertirEnHtml</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Spécifie si vous désirez la conversion en format HTML. Défaut : true
      </description>
    </attribute>    
  </tag>
  <tag>
    <name>entete</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.EnteteTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneLibre</display-name>
    <description>
      Balise permettant de créer une entête personnalisé à la liste de
      navigation.
    </description>
    <attribute>
      <name>var</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de spécifier le nom de la variable qui loge l'entete, cette
        variable aura la porté session.
      </description>
    </attribute>   
    <attribute>
      <name>gabaritBasListe</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de spécifier si le gabarit de l'entete est pour le bas de
        la liste.
	Défaut : true
      </description>
    </attribute>   
  </tag>
  <tag>
    <name>utilitaire</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.UtilitaireListeTag</tag-class>
    <body-content>empty</body-content>
    <display-name>utilitaire</display-name>
    <description>
      Balise qui permet d'offrir des méthodes utilitaire sur des listes de type
      List ou ListeNavigation.
    </description>
    <attribute>
      <name>var</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de spécifier le nom de la variable qui loge le résultat de
        la balise, cette variable aura la porté request.
      </description>
    </attribute>   
    <attribute>
      <name>liste</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier la liste a traiter, vous pouvez spécifier un nom
        de liste de navigation ou de tout autre liste de type List.
      </description>
    </attribute> 
    <attribute>
      <name>position</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de spécifier la position de l'instance dont on désire extraire
        et placer dans un var.
      </description>
    </attribute>
    <attribute>
      <name>valeurComparateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Spécifie la valeur servant de comparateur à utiliser pour valider si une valeur existe dans la liste.
      </description>
    </attribute>       
    <attribute>
      <name>proprieteComparateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Optionnel, cette propriété sert de comparateur à utiliser pour valider si une valeur existe dans la liste.
      </description>
    </attribute>
  </tag>
</taglib>
