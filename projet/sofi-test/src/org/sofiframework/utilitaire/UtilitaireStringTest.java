/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import junit.framework.TestCase;

import org.apache.commons.lang.StringUtils;

public class UtilitaireStringTest extends TestCase {
  public UtilitaireStringTest(String nom) {
    super(nom);
  }

  public void testtraiterChaineCaractereTropLongueAvecSautLigneHTML() {
    String chaineTest = "1234568901234567_123456789_1234";
    String resultat = UtilitaireString.traiterChaineCaractereTropLongueAvecSautLigneHTML(chaineTest, 28, "_" );
    assertTrue(StringUtils.countMatches(resultat, "<br/>") == 1);

    String chaineTest2 = "1234568901234567_123456789_12345678910";
    String resultat2 = UtilitaireString.traiterChaineCaractereTropLongueAvecSautLigneHTML(chaineTest2, 15, "_" );
    assertTrue(StringUtils.countMatches(resultat2, "<br/>") == 2);

    String chaineTest3 = "123456890123456712345678912345678910";
    String resultat3 = UtilitaireString.traiterChaineCaractereTropLongueAvecSautLigneHTML(chaineTest3, 15, null );

    assertTrue(StringUtils.countMatches(resultat3, "<br/>") == 2);
  }

  public void testtoUpperSansAccent() throws Exception {
    String chaine = UtilitaireString.toUpperSansAccents("asd éÝè àÔ sjdf", "ISO-8859-1");
    assertTrue(chaine != null && chaine.equals("ASD EYE AO SJDF"));
  }
}
