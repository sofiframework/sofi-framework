/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.AttributTri;


public class UtilitaireListeTest extends TestCase {
  public UtilitaireListeTest(String nom) {
    super(nom);
  }

  public void testtrierListe() {
    List liste = new ArrayList();

    Utilisateur objet1 = new Utilisateur();
    objet1.setNom("Ti-Paul");
    objet1.setPrenom("Gaston");
    liste.add(objet1);

    Utilisateur objet2 = new Utilisateur();
    objet2.setNom("Lafleur");
    objet2.setPrenom("Bob");

    liste.add(objet2);

    List listeRetour = UtilitaireListe.trierListe(liste,
        new AttributTri[] { new AttributTri("prenom", true) });
    assertNotNull(listeRetour);
  }
}
