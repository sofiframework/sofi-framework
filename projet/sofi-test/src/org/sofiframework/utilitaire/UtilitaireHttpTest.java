package org.sofiframework.utilitaire;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.GetMethod;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

public class UtilitaireHttpTest extends TestCase {
  public void testgetContenuHtml() throws Exception {

    Thread t = new Thread(new  Runnable() {
      @Override
      public void run() {
        Server server = new Server(8080);
        Context root = new Context(server, "/", Context.SESSIONS);
        //Appeler l'url de test ensuite.
        root.addServlet(new ServletHolder(new HttpServlet() {
          private static final long serialVersionUID = 1L;

          @Override
          protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
            String contenu = UtilitaireHttp.getContenuHtml("http://localhost:8080/test2", req);
            resp.getOutputStream().write(contenu.getBytes());
          }
        }), "/test");


        root.addServlet(new ServletHolder(new HttpServlet() {
          private static final long serialVersionUID = 1L;

          @Override
          protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
            resp.getOutputStream().write(req.getCookies()[0].getValue().getBytes());
            resp.getOutputStream().flush();
          }
        }), "/test2");

        try {
          server.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    t.start();

    HttpClient c = new HttpClient();
    HttpState state = new HttpState();
    state.addCookie(new org.apache.commons.httpclient.Cookie("localhost", "moncookie", "allo", "", -1, false));
    c.setState(state);
    GetMethod g = new GetMethod("http://localhost:8080/test");

    c.executeMethod(g);
    String contenu = g.getResponseBodyAsString();
    assertNotNull(contenu);
  }
}
