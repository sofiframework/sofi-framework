/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.utilitaire;

import junit.framework.TestCase;

public class UtilitaireObjetTest extends TestCase {

  public UtilitaireObjetTest(String nom) {
    super(nom);
  }

  public void testevaluerExpression() {
    String chaine = UtilitaireObjet.evaluerExpression(new ObjetBidon("Côté", "Gilles"), "Nom : [nom], [prenom]!!");
    assertNotNull(chaine);
    assertTrue(chaine.equals("Nom : Côté, Gilles!!"));

    chaine = UtilitaireObjet.evaluerExpression(new ObjetBidon("Côté", "Gilles"), "[nom][prenom]");
    assertNotNull(chaine);
    assertTrue(chaine.equals("CôtéGilles"));

    chaine = UtilitaireObjet.evaluerExpression(new ObjetBidon("Côté", "Gilles"), "[nom]-[prenom]");
    assertNotNull(chaine);
    assertTrue(chaine.equals("CôtéGilles"));
  }

  public class ObjetBidon {

    private String nom;

    private String prenom;

    public ObjetBidon(String nom, String prenom) {
      this.prenom = prenom;
      this.nom = nom;
    }

    public String getNom() {
      return nom;
    }

    public void setNom(String nom) {
      this.nom = nom;
    }

    public String getPrenom() {
      return prenom;
    }

    public void setPrenom(String prenom) {
      this.prenom = prenom;
    }
  }
}
