/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.securite.encryption;

import java.security.Key;

import javax.crypto.spec.IvParameterSpec;

import junit.framework.TestCase;

import org.apache.commons.codec.binary.Base64;

public class UtilitaireEncryptionTest extends TestCase {

  public void testGenerateKey() throws Exception {
    Key cle = UtilitaireEncryption.generateKey();
    assertNotNull(cle);
  }

  public void testEncrypt() throws Exception {
    Key cle = UtilitaireEncryption.generateKey();
    String encrypte = UtilitaireEncryption.encrypt(cle, "éèÉÈôâî:;!$%?&**&(*)(__+");
    assertNotNull(encrypte);
  }

  public void testDecrypt() throws Exception {
    String monTexte = "éèÉÈôâî:;!$%?&**&(*)(__+";

    Key cle = UtilitaireEncryption.generateKey();
    String encrypte = UtilitaireEncryption.encrypt(cle, monTexte);

    String decrypte = UtilitaireEncryption.decrypt(cle, encrypte);
    assertNotNull(decrypte);
    assertTrue(monTexte.equals(decrypte));
  }

  public void testDigest() {
    String digest = UtilitaireEncryption.digest("éèÉÈôâî:;!$%?&**&(*)(__+");
    assertNotNull(digest);
  }

  public void testIsDigestsEquals() {
    String digest = UtilitaireEncryption.digest("éèÉÈôâî:;!$%?&**&(*)(__+");
    boolean equals = UtilitaireEncryption.isDigestsEquals(digest, "éèÉÈôâî:;!$%?&**&(*)(__+");
    assertTrue(equals);

    equals = UtilitaireEncryption.isDigestsEquals(digest, "ce nest pas la meme string");
    assertFalse(equals);
  }

  public void testEncrypt3DES_CBC() throws EncryptionException {
    IvParameterSpec ivParameters = new IvParameterSpec(new byte[] { 12, 34, 56, 78, 90, 87, 65, 43 });
    byte[] encryptedData = UtilitaireEncryption.encrypt3DES_CBC("Le texte à encrypter",
        "La clé doit être d'au moins 24 caractères", ivParameters);

    assertNotNull(encryptedData);

    System.out.println(new String(Base64.encodeBase64(encryptedData)));
  }

  public void testDecrypt3DES_CBC() throws Exception {
    String key = "La clé doit être d'au moins 24 caractères";
    String plainText = "Le texte à encrypter";
    IvParameterSpec ivParameters = new IvParameterSpec(new byte[] { 12, 34, 56, 78, 90, 87, 65, 43 });

    byte[] encryptedData = UtilitaireEncryption.encrypt3DES_CBC(plainText, key, ivParameters);
    assertNotNull(encryptedData);

    byte[] decryptedData = UtilitaireEncryption.decrypt3DES_CBC(encryptedData, key, ivParameters);
    assertNotNull(decryptedData);

    System.out.println(new String(decryptedData));

    assertTrue(new String(decryptedData).equals(plainText));

  }
}
