/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;


import junit.textui.TestRunner;

/**
 * Classe de test pour ConfigFiltre
 * 
 * @author Guillaume Poirier
 */
public class ConfigFiltreTest extends AbstractFiltreTest {

  public ConfigFiltreTest(String name) {
    super(name);
  }

  /**
   * Test l'utilisateur de ajouterNested pour définir un filtre imbriqué et sa
   * configuration.
   */
  public void testNested() throws Exception {
    ConfigFiltre config = new ConfigFiltre();
    config.ajouterNested("nested");

    AttributFiltre[] expected = {
        attr("id", FiltreParent.DEFAUT_ID),
        attr("nested", new AttributFiltre[] {
            attr("nom", FiltreNested.DEFAUT_NOM) }) };
    assertValeursEgales(expected, config.getValeurs(new FiltreParent()));
  }

  /**
   * Test l'utilisation d'un alias pour obtenir des critères des objets nested.
   */
  public void testAliasNested() throws Exception {
    ConfigFiltre config = new ConfigFiltre();
    config.ajouterAlias("nested.nom", "nomEntite");
    config.exclureAttribut("nested");

    AttributFiltre[] expected = {
        attr("id", FiltreParent.DEFAUT_ID),
        attr("nomEntite", FiltreNested.DEFAUT_NOM) };
    assertValeursEgales(expected, config.getValeurs(new FiltreParent()));
  }

  /**
   * Test l'utiliation d'un alias sur un objet imbriqué.
   */
  public void testNestedAlias() throws Exception {
    ConfigFiltre config = new ConfigFiltre();
    config.ajouterNested("nested");
    config.ajouterAlias("nested", "alias");

    AttributFiltre[] expected = {
        attr("id", FiltreParent.DEFAUT_ID),
        attr("alias", new AttributFiltre[] {
            attr("nom", FiltreNested.DEFAUT_NOM) }) };
    assertValeursEgales(expected, config.getValeurs(new FiltreParent()));
  }

  /**
   * Test l'exclusion d'attributs du filtre.
   */
  public void testExclureAttribut() {
    FiltreTestIntervalle bean = new FiltreTestIntervalle();
    bean.setDebut(new Integer(4));
    bean.setFin(new Integer(10));
    bean.setString("allo");

    ConfigFiltre config = new ConfigFiltre();
    config.exclureAttribut("fin");

    AttributFiltre[] expected = {
        attr("debut", bean.debut), attr("string", bean.string) };
    assertValeursEgales(expected, new FiltreWrapper(bean, config));
  }

  /**
   * Test la définition d'intervalles.
   */
  public void testAjouterIntervalle() {
    FiltreTestIntervalle bean = new FiltreTestIntervalle();
    bean.setDebut(new Integer(4));

    ConfigFiltre config = new ConfigFiltre();
    config.ajouterIntervalle("debut", "fin", "entier");

    AttributFiltre[] expected = {
        attr("entier", bean.debut, Operateur.PLUS_GRAND_EGALE) };
    assertValeursEgales(expected, config.getValeurs(bean));

    bean.setFin(new Integer(10));
    bean.setString("allo");

    expected = new AttributFiltre[] {
        attr("entier", bean.debut, Operateur.PLUS_GRAND_EGALE),
        attr("entier", bean.fin, Operateur.PLUS_PETIT_EGALE),
        attr("string", bean.string) };
    assertValeursEgales(expected, config.getValeurs(bean));
  }

  /**
   * Test l'ajout d'aliases simples.
   */
  public void testAjouterAlias() {
    FiltreTestIntervalle bean = new FiltreTestIntervalle();
    bean.setFin(new Integer(10));
    bean.setString("allo");

    ConfigFiltre filtre = new ConfigFiltre();
    filtre.ajouterAlias("fin", "entier", Operateur.PLUS_PETIT);

    AttributFiltre[] expected = {
        attr("entier", bean.fin, Operateur.PLUS_PETIT),
        attr("string", bean.string) };
    assertValeursEgales(expected, filtre.getValeurs(bean));
  }

  /**
   * Test la définition de MatchModes.
   */
  public void testSetMatchMode() {
    FiltreTestMatchMode bean = new FiltreTestMatchMode();
    bean.stringUn = "abc";
    bean.stringDeux = "def";
    bean.entierObjet = new Integer(4);

    ConfigFiltre filtre = new ConfigFiltre();
    filtre.setDefautMatchMode(MatchMode.ANYWHERE);
    filtre.setMatchMode("stringUn", MatchMode.START);

    AttributFiltre[] expected = {
        attr("entierObjet", bean.entierObjet),
        attr("stringUn", bean.stringUn, MatchMode.START),
        attr("stringDeux", bean.stringDeux, MatchMode.ANYWHERE) };
    assertValeursEgales(expected, filtre.getValeurs(bean));
  }

  /**
   * Test le comportement du filtre sur les chaines de caractères vides.
   */
  public void testStringVide() throws Exception {
    FiltreTestMatchMode bean = new FiltreTestMatchMode();
    bean.stringUn = "";
    bean.stringDeux = " \t";
    bean.entierObjet = new Integer(4);

    ConfigFiltre filtre = new ConfigFiltre();

    AttributFiltre[] expected = {
        attr("entierObjet", bean.entierObjet) };
    assertValeursEgales(expected, filtre.getValeurs(bean));
  }

  public void testAcceptDefautPrimitive() throws Exception {
    ConfigFiltre filtre = new ConfigFiltre();
    FiltrePrimitif bean = new FiltrePrimitif();

    // Primitifs définits
    bean.booleen = true;
    bean.caractere = 'a';
    bean.entier = -10;

    AttributFiltre[] expected = {
        attr("booleen", Boolean.TRUE),
        attr("entier", new Integer(bean.entier)),
        attr("caractere", new Character(bean.caractere)) };
    assertValeursEgales(expected, filtre.getValeurs(bean));


    // Primitifs nuls
    bean = new FiltrePrimitif();
    assertValeursEgales(new AttributFiltre[0], filtre.getValeurs(bean));

    // Wrapper à zéro
    bean.id = new Integer(0);

    expected = new AttributFiltre[] {
        attr("id", bean.id) };
    assertValeursEgales(expected, filtre.getValeurs(bean));

    // Un primitif définit
    bean = new FiltrePrimitif();
    bean.booleen = true;

    expected = new AttributFiltre[] {
        attr("booleen", Boolean.TRUE) };
    assertValeursEgales(expected, filtre.getValeurs(bean));

    // Toujours inclure les primitifs nuls
    bean = new FiltrePrimitif();
    filtre.setAccepteDefautPrimitives(true);

    expected = new AttributFiltre[] {
        attr("booleen", Boolean.FALSE), attr("entier", new Integer(0)),
        attr("caractere", new Character((char) 0)) };
    assertValeursEgales(expected, filtre.getValeurs(bean));

    // Inclure les primitifs nuls par défaut
    // Setting différent pour un attribut
    bean = new FiltrePrimitif();
    filtre.setDefautPrimitive("caractere", false);

    expected = new AttributFiltre[] {
        attr("booleen", Boolean.FALSE), attr("entier", new Integer(0)) };
    assertValeursEgales(expected, filtre.getValeurs(bean));

    // Exclure les primitifs nuls par défaut
    // Setting différent pour un attribut
    bean = new FiltrePrimitif();
    filtre.setAccepteDefautPrimitives(false);
    filtre.setDefautPrimitive("caractere", true);

    expected = new AttributFiltre[] {
        attr("caractere", new Character((char) 0)) };
    assertValeursEgales(expected, filtre.getValeurs(bean));

  }

  public static class FiltreTestMatchMode {
    private String stringUn;
    private String stringDeux;
    private Integer entierObjet;

    public Integer getEntierObjet() {
      return entierObjet;
    }

    public void setEntierObjet(Integer entierObjet) {
      this.entierObjet = entierObjet;
    }

    public String getStringDeux() {
      return stringDeux;
    }

    public void setStringDeux(String stringDeux) {
      this.stringDeux = stringDeux;
    }

    public String getStringUn() {
      return stringUn;
    }

    public void setStringUn(String stringUn) {
      this.stringUn = stringUn;
    }
  }

  public static class FiltreTestIntervalle {
    private Integer debut;
    private Integer fin;
    private String string;

    public Integer getDebut() {
      return debut;
    }

    public void setDebut(Integer debut) {
      this.debut = debut;
    }

    public Integer getFin() {
      return fin;
    }

    public void setFin(Integer fin) {
      this.fin = fin;
    }

    public String getString() {
      return string;
    }

    public void setString(String string) {
      this.string = string;
    }
  }

  public static class FiltreParent {
    public static final String DEFAUT_ID = "id02";
    private String id = DEFAUT_ID;
    private FiltreNested nested = new FiltreNested();

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public FiltreNested getNested() {
      return nested;
    }

    public void setNested(FiltreNested nested) {
      this.nested = nested;
    }
  }

  public static class FiltreNested {
    public static final String DEFAUT_NOM = "Nom enfant";
    private String nom = DEFAUT_NOM;

    public String getNom() {
      return nom;
    }

    public void setNom(String nom) {
      this.nom = nom;
    }
  }

  public static class FiltrePrimitif {
    private Integer id;
    private int entier;
    private boolean booleen;
    private char caractere;

    public boolean isBooleen() {
      return booleen;
    }

    public void setBooleen(boolean booleen) {
      this.booleen = booleen;
    }

    public char getCaractere() {
      return caractere;
    }

    public void setCaractere(char caractere) {
      this.caractere = caractere;
    }

    public int getEntier() {
      return entier;
    }

    public void setEntier(int entier) {
      this.entier = entier;
    }

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }
  }

  public static void main(String[] args) {
    // TestRunner.run(ConfigFiltreTest.class);
    TestRunner.run(new ConfigFiltreTest("testAcceptPrimitive"));
  }
}
