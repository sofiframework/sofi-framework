/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import junit.framework.TestCase;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AbstractFiltreTest extends TestCase {

  private static final Log log = LogFactory.getLog(AbstractFiltreTest.class);

  protected static AttributFiltre attr(String nom, Object valeur) {
    return new AttributFiltre(nom, valeur);
  }

  protected static AttributFiltre attr(String nom, Object valeur, MatchMode mode) {
    return new AttributFiltre(nom, valeur, mode);
  }

  private static final Comparator comparateur = new Comparator() {
    @Override
    public int compare(Object o1, Object o2) {
      AttributFiltre a1 = (AttributFiltre) o1;
      AttributFiltre a2 = (AttributFiltre) o2;
      CompareToBuilder builder = (new CompareToBuilder());
      builder.append(a1.getNom(), a2.getNom());
      builder.append(a1.getValeur(), a2.getValeur());
      builder.append(a1.getOperateur().toString(), a2.getValeur().toString());
      builder.append(a1.getMatchMode().toString(), a2.getValeur().toString());
      return builder.toComparison();
    }
  };

  protected AttributFiltre attr(String nom, Object valeur, Operateur op) {
    return new AttributFiltre(nom, valeur, op);
  }

  public AbstractFiltreTest(String name) {
    super(name);
  }

  protected void assertValeursEgales(AttributFiltre[] expected, Filtre filtre) {
    assertValeursEgales(expected, filtre.produireValeurs());
  }

  protected void assertValeursEgales(AttributFiltre[] expected, Collection given) {
    log.info(getName() + ", expected : " + Arrays.asList(expected));
    log.info(getName() + ", given : " + given);
    assertNotNull(given);
    assertEquals(expected.length, given.size());
    Arrays.sort(expected, comparateur);

    ArrayList liste = new ArrayList(given);
    Collections.sort(liste, comparateur);

    for (int i = 0, n = expected.length; i < n; i++) {
      assertValeurEgale(expected[i], (AttributFiltre) liste.get(i));
    }
  }

  protected void assertValeurEgale(AttributFiltre expected, AttributFiltre given) {
    assertEquals(expected.getNom(), given.getNom());
    assertEquals(expected.getMatchMode(), given.getMatchMode());
    assertEquals(expected.getOperateur(), given.getOperateur());

    if (expected.getValeur() instanceof AttributFiltre[]) {
      assertTrue(given.getValeur() instanceof Filtre);
      Filtre filtre = (Filtre) given.getValeur();
      AttributFiltre[] expectedNested = (AttributFiltre[]) expected.getValeur();
      assertValeursEgales(expectedNested, filtre.produireValeurs());
    } else {
      assertEquals(expected.getValeur(), given.getValeur());
    }
  }

}
