/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste;


public class ObjetFiltreTest extends AbstractFiltreTest {

  public ObjetFiltreTest(String name) {
    super(name);
  }

  public void testSimple() {
    FiltreTest filtre = new FiltreTest();
    filtre.id = new Long(3);

    AttributFiltre[] expected = {
        attr("id", filtre.id) };
    assertValeursEgales(expected, filtre);
  }

  public void testAssociation() {
    FiltreTest filtre = new FiltreTest();
    filtre.ajouterAssociation("operateur", "id", "attributEntite");

    assertValeursEgales(new AttributFiltre[0], filtre);

    filtre.id = new Long(5);
    filtre.setOperateur(">=");

    AttributFiltre[] expected = {
        attr("attributEntite", filtre.id, Operateur.PLUS_GRAND_EGALE) };
    assertValeursEgales(expected, filtre);
  }

  public static class FiltreTest extends ObjetFiltre {
    /**
     * 
     */
    private static final long serialVersionUID = 681818553367598862L;
    private Long id;
    private String nom;
    private String operateur;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getNom() {
      return nom;
    }

    public void setNom(String nom) {
      this.nom = nom;
    }

    public String getOperateur() {
      return operateur;
    }

    public void setOperateur(String operateur) {
      this.operateur = operateur;
    }
  }
}
