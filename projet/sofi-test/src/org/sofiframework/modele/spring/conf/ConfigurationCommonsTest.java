/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.conf;

import org.sofiframework.modele.spring.junit.AbstractSpringTest;

public class ConfigurationCommonsTest extends AbstractSpringTest {

  public ConfigurationCommonsTest() {
    super("monTest");
  }

  @Override
  protected String[] getConfigurationsSpring() {
    return new String[]{"org/sofiframework/modele/spring/conf/contexte.xml"};
  }

  public void test() {
    Pojo p = (Pojo) this.getBean("monTest");
    System.out.println(p.getA());
    System.out.println(p.getB());
    System.out.println(p.getC());
  }
}
