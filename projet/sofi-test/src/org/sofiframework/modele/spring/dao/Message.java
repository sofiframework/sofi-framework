/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import org.sofiframework.objetstransfert.ObjetTransfert;

public class Message extends ObjetTransfert {

  /**
   * 
   */
  private static final long serialVersionUID = -6671176768351396707L;

  private Long id;

  /**
   * La code qui identifie uniquement le message.
   */
  private String code;

  /**
   * Un code qui indique la langue dans laquelle le message est écrit.
   */
  private String codeLangue;

  /**
   * Le code de l'application à laquelle le message est associé.
   */
  private String codeApplication;

  /**
   * Le contenu du message
   */
  private String texte;

  /**
   * Le texte à afficher dans la bulle d'aide du message, si possible.
   */
  private String aideContextuelle;

  /**
   * Un code qui indique le type de message.
   * e.g. E (Erreur), I (Information), etc.
   */
  private String codeSeverite;

  private TypeMessage type;

  /**
   * Fixe le code de langue du message
   * @param codeLangue le code de langue du message
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue du message
   * @return  le code de langue du message
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixe le code d'application
   * @param codeApplication le code d'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }


  /**
   * Retourne le code d'application
   * @return  le code d'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }


  /**
   * Fixe le code de sévéritié
   * @param codeSeverite le code de sévéritié
   */
  public void setCodeSeverite(String codeSeverite) {
    this.codeSeverite = codeSeverite;
  }

  /**
   * Retourne le code de sévéritié
   * @return  le code de sévéritié
   */
  public String getCodeSeverite() {
    return codeSeverite;
  }


  /**
   * Fixe l'aide contextuelle
   * @param aideContextuelle l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne l'aide contextuelle
   * @return  l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixe le contenu du message
   * @param texte le contenu du message
   */
  public void setTexte(String texte) {
    this.texte = texte;
  }

  /**
   * Retourne le contenu du message
   * @return  le contenu du message
   */
  public String getTexte() {
    return texte;
  }

  public TypeMessage getType() {
    return type;
  }

  public void setType(TypeMessage type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}