/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

import org.sofiframework.composantweb.liste.AttributTri;
import org.sofiframework.composantweb.liste.ConfigFiltre;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.FiltreWrapper;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.MatchMode;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe de base pour tester l'interface BaseDao
 * 
 * @author Guillaume Poirier
 */
public abstract class AbstractBaseDaoTest extends AbstractSpringTest {

  private BaseDao libelleDao;

  private BaseDao referentielDao;

  private BaseDao messageDao;

  private BaseDao roleDao;

  private BaseDao typeMessageDao;

  public AbstractBaseDaoTest(String name) {
    super(name);
  }

  @Override
  protected final void configurer() throws Exception {
    configurerTest();
    libelleDao = creerBaseDao(Libelle.class);
    referentielDao = creerBaseDao(ObjetReferentiel.class);
    messageDao = creerBaseDao(Message.class);
    roleDao = creerBaseDao(Role.class);
    typeMessageDao = creerBaseDao(TypeMessage.class);
  }

  @Override
  public final void detruire() throws Exception {
    detruireTest();
    libelleDao = null;
    referentielDao = null;
    messageDao = null;
    roleDao = null;
    typeMessageDao = null;
  }

  protected abstract void configurerTest() throws Exception;

  protected abstract void detruireTest() throws Exception;

  protected abstract BaseDao creerBaseDao(Class classe);

  private Libelle libelleChampObligatoire() {
    Libelle libelle = new Libelle();
    libelle.setCleLibelle("infra.libelle.commun.champ.obligatoire");
    libelle.setCodeLangue("FR");
    libelle.setCodeApplication("INFRA");
    libelle.setTexteLibelle("Champs obligatoires");
    return libelle;
  }

  public void testGet() {
    assertExisteGet(libelleChampObligatoire());
    LibellePK cle = new LibellePK("infra.libelle.commun.champ.obligatoire",
        "EN");
    Object libelle = libelleDao.get(cle);
    assertNull("Valeur nule attendu pour la clé '" + cle.getCleLibelle()
        + "' et le code de langue '" + cle.getCodeLangue() + "'", libelle);
  }

  public void testCharger() {
    assertExisteCharger(libelleChampObligatoire());
    assertInexistant("infra.libelle.commun.champ.obligatoire", "EN");
  }

  public void testAjouter() throws Exception {
    Libelle nouveau = libelleChampObligatoire();
    nouveau.setCodeLangue("EN");
    assertInexistant(nouveau.getLibellePK());
    libelleDao.ajouter(nouveau);

    assertExisteGet(nouveau);
    assertExisteGet(nouveau);
  }

  public void testModifier() {
    Libelle libelle = libelleChampObligatoire();
    libelle.setCodeApplication("DIAPASON");
    libelleDao.modifier(libelle);

    libelle = libelleChampObligatoire();
    libelle.setCodeApplication("DIAPASON");
    assertExisteGet(libelle);
  }

  public void testSupprimer() {
    Libelle libelle = libelleChampObligatoire();
    assertExisteGet(libelle);
    libelleDao.supprimer(libelle.getLibellePK());
    assertInexistant(libelleChampObligatoire().getLibellePK());
  }

  public void testGetListe() {
    List liste = libelleDao.getListe();
    assertNotNull(liste);
    assertTrue(liste.size() > 0);
  }

  public void testGetListeNavigation() {
    ListeNavigation liste = new ListeNavigation(1, 5);
    liste = libelleDao.getListe(liste);
    assertNotNull(liste);
    long size = liste.getNbListe();
    assertTrue("Size: " + size, size > 5);
    assertEquals(5, liste.getListe().size());

    List total = libelleDao.getListe();
    assertEquals(225, total.size());
    ArrayList testListe = new ArrayList(total.size());

    liste = libelleDao.getListe(new ListeNavigation(1, 100));
    assertEquals(liste.getNbListe(), 225);
    assertEquals(liste.getListe().size(), 100);
    testListe.addAll(liste.getListe());

    liste = libelleDao.getListe(new ListeNavigation(2, 100));
    assertEquals(liste.getNbListe(), 225);
    assertEquals(liste.getListe().size(), 100);
    testListe.addAll(liste.getListe());

    liste = libelleDao.getListe(new ListeNavigation(3, 100));
    assertEquals(liste.getNbListe(), 225);
    assertEquals(liste.getListe().size(), 25);
    testListe.addAll(liste.getListe());

    assertEquals(total.size(), testListe.size());
    assertTrue(total instanceof RandomAccess);
    for (int i = 0, n = total.size(); i < n; i++) {
      assertLibelleEquals((Libelle) total.get(i), (Libelle) testListe.get(i));
    }
  }

  public void testGetListeNavigationTriAsc() {
    ListeNavigation liste = new ListeNavigation(1, 5);
    liste.setTri(new Tri("codeApplication", true));
    liste = libelleDao.getListe(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 1);

    validerTri(liste.getListe(), liste.getTri());
  }

  public void testGetListeNavigationTriDesc() {
    ListeNavigation liste = new ListeNavigation(2, 5);
    liste.setTri(new Tri("codeApplication", false));
    liste = libelleDao.getListe(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 1);

    validerTri(liste.getListe(), liste.getTri());
  }

  public void testGetListeNavigationTriInitial() {
    Tri tri = new Tri();
    tri.ajouterTriInitial("codeApplication", false);

    ListeNavigation liste = new ListeNavigation(2, 5);
    liste.setTri(tri);

    liste = libelleDao.getListe(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 1);

    validerTri(liste.getListe(), new Tri("codeApplication", false));

    tri.setTriAttribut("texteLibelle");
    tri.setOrdreTri("ASC");

    liste = new ListeNavigation(2, 5);
    liste.setTri(tri);

    liste = libelleDao.getListe(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 1);

    validerTri(liste.getListe(), new Tri("texteLibelle", true));
  }

  public void testGetListeNavigationTriLogique() {
    Tri tri = new Tri("testExclure", true);
    tri.ajouterAttributLogiqueAtrier("testExclure", "codeApplication", true);
    tri.ajouterAttributLogiqueAtrier("testExclure", "texteLibelle", true);

    ListeNavigation ln = new ListeNavigation(1, Integer.MAX_VALUE);
    ln.setTri(tri);
    libelleDao.getListe(ln);

    validerTri(ln.getListe(), tri);

    tri.setAscendant(false);
    ln = new ListeNavigation(1, Integer.MAX_VALUE);
    ln.setTri(tri);
    libelleDao.getListe(ln);

    validerTri(ln.getListe(), tri);
  }

  private void validerTri(List liste, final Tri tri) {
    final Collection attributs = tri.attributs();
    ArrayList listeTriOriginal = new ArrayList(liste);
    ArrayList listeTriComparateur = new ArrayList(liste);
    Collections.sort(listeTriComparateur, new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        ObjetTransfert objet1 = (ObjetTransfert) o1;
        ObjetTransfert objet2 = (ObjetTransfert) o2;
        for (Iterator it = attributs.iterator(); it.hasNext();) {
          AttributTri attribut = (AttributTri) it.next();
          String nomAttribut = UtilitaireString
              .convertirPremiereLettreEnMinuscule(attribut.getNom());
          Comparable attribut1 = (Comparable) objet1.getPropriete(nomAttribut);
          Comparable attribut2 = (Comparable) objet2.getPropriete(nomAttribut);
          boolean asc = attribut.isAscendant();
          int resultat = attribut1.compareTo(attribut2) * (asc ? 1 : -1);
          if (resultat != 0) {
            return resultat;
          }
        }
        return 0;
      }
    });
    assertEquals(listeTriComparateur, listeTriOriginal);
  }

  public void testGetListeNavigationFiltreLikes() {
    ConfigFiltre config = new ConfigFiltre();
    config.exclureAttribut("libellePK");

    config.setDefautMatchMode(MatchMode.START);
    validerFiltreLibelle(filtreLibelle("inf", null, config), 120);

    config.setDefautMatchMode(MatchMode.EXACT);
    validerFiltreLibelle(filtreLibelle("inf", null, config), 0);
    validerFiltreLibelle(filtreLibelle("Infra", null, config), 120);
    validerFiltreLibelle(filtreLibelle("INFRA", null, config), 120);
    validerFiltreLibelle(filtreLibelle(null, "page {0} de {1}", config), 1);
  }

  private Filtre filtreLibelle(String codeApplication, String texteLibelle,
      ConfigFiltre config) {
    Libelle libelle = new Libelle();
    libelle.setCodeApplication(codeApplication);
    libelle.setTexteLibelle(texteLibelle);
    return new FiltreWrapper(libelle, config);
  }

  public void testGetListeNavigationFiltreNestedManyToOne() {
    validerFiltreMessage("INFRA", "Erreur", 9);
    validerFiltreMessage("INFRA", "NonExistant", 0);
    validerFiltreMessage("INFRA", null, 14);
    validerFiltreMessage(new FiltreMessage("INFRA", null), 14);
  }

  public void testGetListeNavigationFiltreNestedManyToMany() {
    validerFiltreRole("Pilote%", 3);
    validerFiltreRole("Pilote", 0);
    validerFiltreRole("PiloteDTI", 1);

    // Filtre par la clé primaire de la relation
    validerFiltreRole(null, 42, 2);
    validerFiltreRole(null, 46, 1);
    validerFiltreRole(null, 1, 0);

    // Filtre selon un critère de la relation
    // qui retourne plusieurs résultats de l'entité
    // principale
    // Cette fonctionnalité ne fonctionne pas présentement
    // à cause d'un bug de hibernate
    validerFiltreRole(null, "INFRA", 1);
    validerFiltreRole(null, "DIAPASON", 2);
    validerFiltreRole(null, "DUMMY", 0);
  }

  public void testGetListeNavigationFiltreNestedOneToMany() {
    // Filtre selon un critère de la relation
    // qui retourne plusieurs résultats de l'entité
    // principale
    // Cette fonctionnalité ne fonctionne pas présentement
    // à cause d'un bug de hibernate
    FiltreTypeMessage filtre = new FiltreTypeMessage(new FiltreMessage("INFRA",
        null));
    ConfigFiltre config = new ConfigFiltre();
    config.ajouterNested("filtreMessage");
    config.ajouterAlias("filtreMessage", "messages");
    validerFiltre(typeMessageDao, new FiltreWrapper(filtre, config), 3);
  }

  public void testGetListeNavigationFiltreIntervalle() {
    validerFiltreReferentiel(50, 59, 10);
    validerFiltreReferentiel(59, 50, 0);
    validerFiltreReferentiel(null, new Long(45), 4);
    validerFiltreReferentiel(new Long(60), null, 12);
    validerFiltreReferentiel(10, 20, 0);
  }

  private void validerFiltreRole(ObjetFiltre filtre, int resultats) {
    validerFiltre(roleDao, filtre, resultats);
  }

  private void validerFiltreRole(String codeRole, int resultats) {
    validerFiltre(roleDao, new FiltreRole(codeRole), resultats);
  }

  private void validerFiltreRole(String codeRole, String codeApplication,
      int resultats) {
    validerFiltreRole(new FiltreRole(codeRole, new FiltreReferentiel(
        codeApplication)), resultats);
  }

  private void validerFiltreRole(String codeRole, long seqObjetSecurisable,
      int resultats) {
    validerFiltreRole(new FiltreRole(codeRole, new FiltreReferentiel(new Long(
        seqObjetSecurisable))), resultats);
  }

  private void validerFiltreLibelle(Filtre filtre, int resultats) {
    validerFiltre(libelleDao, filtre, resultats);
  }

  private void validerFiltreMessage(Message filtre, int resultats) {
    ConfigFiltre config = new ConfigFiltre();
    config.ajouterNested("type");
    validerFiltre(messageDao, new FiltreWrapper(filtre, config), resultats);
  }

  private void validerFiltreMessage(String codeApplication, String nomErreur,
      int resultats) {
    FiltreTypeMessage filtreType = new FiltreTypeMessage(null, nomErreur);
    Message filtre = new FiltreMessage(codeApplication, filtreType);
    validerFiltreMessage(filtre, resultats);
  }

  private void validerFiltreReferentiel(Long debut, Long fin, int resultats) {
    validerFiltreReferentiel(new FiltreReferentiel(debut, fin), resultats);
  }

  private void validerFiltreReferentiel(int debut, int fin, int resultats) {
    validerFiltreReferentiel(new FiltreReferentiel(debut, fin), resultats);
  }

  private void validerFiltreReferentiel(ObjetFiltre filtre, int resultats) {
    validerFiltre(referentielDao, filtre, resultats);
  }

  private void validerFiltre(BaseDao dao, Filtre filtre, int resultats) {
    System.out.println(getName() + ": " + filtre.toString());
    ListeNavigation liste = new ListeNavigation(1, 15);
    liste.setFiltre(filtre);
    liste = dao.getListe(liste);
    assertNotNull(liste);
    assertEquals(resultats, liste.getNbListe());
    assertEquals(Math.min(15, resultats), liste.getListe().size());
  }

  protected void assertInexistant(String cleLibelle, String codeLangue) {
    assertInexistant(new LibellePK(cleLibelle, codeLangue));
  }

  protected void assertInexistant(LibellePK cle) {
    try {
      libelleDao.charger(cle);
      fail("ObjetTransfertNotFoundException attendu pour la clé '"
          + cle.getCleLibelle() + "' et le code de langue '"
          + cle.getCodeLangue() + "'");
    } catch (ObjetTransfertNotFoundException e) {
    }
  }

  protected void assertExisteGet(Libelle libelle) {
    LibellePK cle = libelle.getLibellePK();
    Libelle autre = (Libelle) libelleDao.get(cle);
    assertNotNull("Incapable de trouver une libellé pour la clé '"
        + cle.getCleLibelle() + "' et le code de langue '"
        + cle.getCodeLangue() + "'", autre);
    assertLibelleEquals(libelle, autre);
  }

  protected void assertExisteCharger(Libelle libelle) {
    LibellePK cle = libelle.getLibellePK();
    try {
      Libelle autre = (Libelle) libelleDao.charger(cle);
      assertLibelleEquals(libelle, autre);
    } catch (ObjetTransfertNotFoundException e) {
      fail("Incapable de trouver une libellé pour la clé '"
          + cle.getCleLibelle() + "' et le code de langue '"
          + cle.getCodeLangue() + "'");
    }
  }

  protected void assertLibelleEquals(Libelle o1, Libelle o2) {
    assertEquals(o1.getCleLibelle(), o2.getCleLibelle());
    assertEquals(o1.getCodeLangue(), o2.getCodeLangue());
    assertEquals(o1.getCodeApplication(), o2.getCodeApplication());
    assertEquals(o1.getTexteLibelle(), o2.getTexteLibelle());
    assertEquals(o1.getAideContextuelle(), o2.getAideContextuelle());
  }

  public static class FiltreReferentiel extends ObjetFiltre {

    private static final long serialVersionUID = 6959553032400086847L;

    private Long seqObjetSecurisable;

    private String codeApplication;

    private Long sequenceDebut;

    private Long sequenceFin;

    public FiltreReferentiel(Long seqObjetSecurisable) {
      this.seqObjetSecurisable = seqObjetSecurisable;
    }

    public FiltreReferentiel(String codeApplication) {
      this(codeApplication, null, null);
    }

    public FiltreReferentiel(Long sequenceDebut, Long sequenceFin) {
      this(null, sequenceDebut, sequenceFin);
    }

    public FiltreReferentiel(String codeApplication, Long sequenceDebut,
        Long sequenceFin) {
      this.codeApplication = codeApplication;
      this.sequenceDebut = sequenceDebut;
      this.sequenceFin = sequenceFin;

      ajouterIntervalle("sequenceDebut", "sequenceFin", "seqObjetSecurisable");
    }

    public FiltreReferentiel(int sequenceDebut, int sequenceFin) {
      this(null, new Long(sequenceDebut), new Long(sequenceFin));
    }

    public Long getSequenceDebut() {
      return sequenceDebut;
    }

    public void setSequenceDebut(Long sequenceDebut) {
      this.sequenceDebut = sequenceDebut;
    }

    public Long getSequenceFin() {
      return sequenceFin;
    }

    public void setSequenceFin(Long sequenceFin) {
      this.sequenceFin = sequenceFin;
    }

    public String getCodeApplication() {
      return codeApplication;
    }

    public void setCodeApplication(String codeApplication) {
      this.codeApplication = codeApplication;
    }

    public Long getSeqObjetSecurisable() {
      return seqObjetSecurisable;
    }

    public void setSeqObjetSecurisable(Long seqObjetSecurisable) {
      this.seqObjetSecurisable = seqObjetSecurisable;
    }
  }

  public static class FiltreTypeMessage extends TypeMessage {
    private FiltreMessage filtreMessage;

    public FiltreTypeMessage(FiltreMessage messages) {
      this.filtreMessage = messages;
    }

    public FiltreTypeMessage(String code, String nom) {
      setCode(code);
      setNom(nom);
    }

    public void setMessages(FiltreMessage messages) {
      this.filtreMessage = messages;
    }

    public FiltreMessage getFiltreMessage() {
      return filtreMessage;
    }

  }

  public static class FiltreMessage extends Message {

    private static final long serialVersionUID = -5233814563085092620L;

    public FiltreMessage(String codeApplication, TypeMessage type) {
      setCodeApplication(codeApplication);
      setType(type);
    }
  }

  public static class FiltreRole extends ObjetFiltre {

    private static final long serialVersionUID = -1645675050599305973L;

    private String code;

    private String nom;

    private FiltreReferentiel referentiel;

    public FiltreRole(String code) {
      this.code = code;
    }

    public FiltreRole(String code, FiltreReferentiel referentiel) {
      this.code = code;
      this.referentiel = referentiel;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getNom() {
      return nom;
    }

    public void setNom(String nom) {
      this.nom = nom;
    }

    public FiltreReferentiel getReferentiel() {
      return referentiel;
    }

    public void setReferentiel(FiltreReferentiel referentiel) {
      this.referentiel = referentiel;
    }
  }
}
