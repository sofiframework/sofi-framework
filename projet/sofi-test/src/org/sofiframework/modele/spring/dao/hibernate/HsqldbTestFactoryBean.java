/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate;

import java.io.IOException;
import java.io.InputStream;

import javax.sql.DataSource;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

/**
 * Implémentation de <code>FactoryBean</code> pour rendre disponible le
 * <code>DataSource</code> d'HSQLDB dans un contexte Spring.
 * 
 * @author Guillaume Poirier
 */
public class HsqldbTestFactoryBean extends HsqldbDataSource implements FactoryBean,
InitializingBean, DisposableBean {

  /** La référence au script SQL d'initilisation d'HSQLDB */
  private Resource resource;

  @Override
  public Object getObject() throws Exception {
    return this;
  }

  @Override
  public Class getObjectType() {
    return DataSource.class;
  }

  @Override
  public boolean isSingleton() {
    return true;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    InputStream in = resource.getInputStream();
    try {
      init(in);
    } finally {
      try {
        in.close();
      } catch (IOException e) {
      }
    }

  }

  @Override
  public void destroy() throws Exception {
    tearDown();
  }

  /**
   * Définit la référence au script SQL d'initilisation d'HSQLDB.
   */
  public void setResource(Resource resource) {
    this.resource = resource;
  }

}
