/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.spring.dao.BaseDao;
import org.sofiframework.modele.spring.dao.Libelle;
import org.sofiframework.modele.spring.dao.LibellePK;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


/**
 * Test Case pour la classe concrête BaseDaoImpl
 * @author Guillaume Poirier
 */
public class BaseDaoImplTest extends AbstractSpringTest {

  public BaseDaoImplTest(String name) {
    super(name);
  }

  @Override
  protected String[] getConfigurationsSpring() {
    return new String[] {
        "org/sofiframework/modele/spring/dao/hibernate/test/context.xml"
    };
  }

  public void testAutoFlush() {
    BaseDao libelleDao = (BaseDao) this.getBean("libelleDao");

    Libelle l = new Libelle();
    l.setCleLibelle("libelle.test");
    l.setCodeApplication("TEST");
    l.setCodeLangue("fr");
    l.setAideContextuelle("Ceci est une aide");
    l.setTexteLibelle("Mon Libelle");

    libelleDao.ajouter(l);


    l = (Libelle) libelleDao.get(new LibellePK("libelle.test", "fr"));
    assertNotNull(l);
  }

  public void testgetListeNavigation() {
    BaseDao libelleDao = (BaseDao) this.getBean("libelleDao");

    Libelle l = new Libelle();
    l.setCleLibelle("libelle.test");
    l.setCodeApplication("TEST");
    l.setCodeLangue("fr");
    l.setAideContextuelle("Ceci est une aide");
    l.setTexteLibelle("Mon Libelle");

    Libelle l2 = new Libelle();
    l2.setCleLibelle("libelle.test2");
    l2.setCodeApplication("TEST");
    l2.setCodeLangue("fr");
    l2.setAideContextuelle("Ceci est une aide 2");
    l2.setTexteLibelle("Mon Libelle 2");

    libelleDao.ajouter(l);
    libelleDao.ajouter(l2);

    ListeNavigation liste = new ListeNavigation(1, 1);

    liste = libelleDao.getListe(liste);

    assertNotNull(liste);
  }

  public void testIsExiste() throws Exception {
    chargerDonnees();

    LibelleDao dao = (LibelleDao) this.getBean("libelleDao");

    LibellePK l = new LibellePK();
    l.setCleLibelle("infra.libelle.objet.java.service.accueil");
    l.setCodeLangue("FR");

    boolean exist = dao.isExiste(l);

    assertTrue(exist);
  }

  public void testListeAvecHql() throws Exception {
    chargerDonnees();

    ListeNavigation liste = new ListeNavigation(1, 10);
    LibelleDao dao = (LibelleDao) this.getBean("libelleDao");
    liste = dao.getListeLibelleActif(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 0);
  }

  public void testListeAvecFiltreIn() throws Exception {
    chargerDonnees();

    LibelleDao dao = (LibelleDao) this.getBean("libelleDao");
    FiltreLibelleIn filtre = new FiltreLibelleIn();
    filtre.setCodeApplication(new String[]{"PIMIQ", "INFRA"});
    List liste = dao.getListe(filtre);
    assertNotNull(liste);
    assertTrue(liste.size() > 0);
    for (Iterator i = liste.iterator(); i.hasNext();) {
      Libelle libelle = (Libelle) i.next();
      assertTrue(libelle.getCodeApplication().equals("PIMIQ")
          || libelle.getCodeApplication().equals("INFRA"));
    }
  }

  public void testListeNavigationAvecFiltreIn() throws Exception {
    chargerDonnees();

    LibelleDao dao = (LibelleDao) this.getBean("libelleDao");

    FiltreLibelleIn filtre = new FiltreLibelleIn();
    filtre.setCodeApplication(new String[]{"PIMIQ", "INFRA"});

    ListeNavigation liste = new ListeNavigation(1, 10);
    liste.setObjetFiltre(filtre, FiltreLibelleIn.class);

    liste = dao.getListe(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 0);
    for (Iterator i = liste.getListe().iterator(); i.hasNext();) {
      Libelle libelle = (Libelle) i.next();
      assertTrue(libelle.getCodeApplication().equals("PIMIQ")
          || libelle.getCodeApplication().equals("INFRA"));
    }
  }

  private void chargerDonnees() throws SQLException, IOException {
    HsqldbDataSource db = (HsqldbDataSource) this.getBean("datasource");
    Resource resource = new ClassPathResource("org/sofiframework/modele/spring/dao/hibernate/test/infra_donnees.sql");
    InputStream in = resource.getInputStream();
    try {
      db.init(in);
    } finally {
      try {
        in.close();
      } catch (IOException e) {
      }
    }
  }
}
