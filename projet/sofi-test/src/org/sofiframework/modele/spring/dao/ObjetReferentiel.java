/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import java.util.Date;

import org.sofiframework.objetstransfert.ObjetTransfert;

public class ObjetReferentiel extends ObjetTransfert {

  /**
   * 
   */
  private static final long serialVersionUID = 2716395155631957427L;

  /**
   * Constante pour la propriété cle
   */
  private static final String[] NOM_CLES = { "seqObjetSecurisable" };

  /**
   * Desciption de l'objet référentiel
   */
  private String description;

  /**
   * L'adresse Web associé à l'objet référentiel
   */
  private String adresseWeb;

  /**
   * Date à partir de laquel l'objet est actif
   */
  private Date dateDebutActivite;

  /**
   * Date jusqu'à laquelle l'objet est actif
   */
  private Date dateFinActivite;

  /**
   * Le code de l'application à laquelle l'objet appartient.
   */
  private String codeApplication;

  /**
   * La sequence de l'objet sécurisable. Clé unique.
   */
  private Long seqObjetSecurisable;

  /**
   * Le nom du service.
   */
  private String nom;

  /**
   * Le nom de l'action qui est basé l'objet sécurisable s'il y a lieu.
   */
  private String nomAction;

  /**
   * Le nom de paramètre que le nom d'action utilisable.
   */
  private String nomParametre;

  /**
   * Ordre de présentation de l'objet sécurisable.
   */
  private Integer ordrePresentation;

  /**
   * Le type d'objet sécurisable.
   */
  private String type;

  @Override
  public String[] getCle() {
    return NOM_CLES;
  }

  /**
   * Retourne la séquence du service.
   * @return Integer La séquence du service.
   */
  public Long getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  /**
   * Fixer la séquence du service.
   * 
   * @param <seqService> la séquence du service.
   */
  public void setSeqObjetSecurisable(Long seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  /**
   * Retourne le nom de l'objet sécurisé.
   * @return String Le nom de l'objet sécurisé.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'objet sécurisé.
   * 
   * @param <nom> le nom de l'objet sécurisé.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne le nom de l'action dont l'objet sécurisable est basé.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @return le nom de l'action dont l'objet sécurisable est basé
   */
  public String getNomAction() {
    return nomAction;
  }

  /**
   * Fixer le nom d'action dont l'objet sécurisable est basé.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @param nomAction le nom d'action dont l'objet sécurisable est basé
   */
  public void setNomAction(String nomAction) {
    this.nomAction = nomAction;
  }

  /**
   * Retourne le nom paramètre utilisé pour sécurisé l'objet sécurisable.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @return le nom paramètre
   */
  public String getNomParametre() {
    return nomParametre;
  }

  /**
   * Fixer le nom paramètre utilisé pour sécurisé l'objet sécurisable.
   * Permet de générer l'url pour sécurisé l'accès à l'objet sécurisable.
   * @param nomParametre le nom paramètre
   */
  public void setNomParametre(String nomParametre) {
    this.nomParametre = nomParametre;
  }

  /**
   * Retourne l'ordre d'affichage de l'objet sécurisable dans le cas qu'il
   * corresponde à une section, service ou onglet.
   * @return String L'ordre d'affichage
   */
  public Integer getOrdrePresentation() {
    return ordrePresentation;
  }

  /**
   * Fixer l'ordre d'affichage de l'objet sécurisable dans le cas qu'il
   * corresponde à une section, service ou onglet.
   * @param <newOrdreAffichage> l'adresse web.
   */
  public void setOrdreAffichage(Integer ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  /**
   * Retourne le type de l'objet Java
   * @return le type de l'objet Java
   */
  public String getType() {
    return type;
  }

  /**
   * Fixer le type de l'objet Java
   * @param type le type de l'objet Java
   */
  public void setType(String type) {
    this.type = type;
  }

  public void setOrdrePresentation(Integer ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  /**
   * Fixe la description de l'objet référentiel
   * @param description la description de l'objet référentiel
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la description de l'objet référentiel
   * @return  la description de l'objet référentiel
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixe l'adresse Web associé à l'objet référentiel
   * @param adresseWeb l'adresse Web associé à l'objet référentiel
   */
  public void setAdresseWeb(String adresseWeb) {
    this.adresseWeb = adresseWeb;
  }

  /**
   * Retourne l'adresse Web associé à l'objet référentiel
   * @return  l'adresse Web associé à l'objet référentiel
   */
  public String getAdresseWeb() {
    return adresseWeb;
  }


  /**
   * Fixe la date à partir de laquelle l'objet est actif
   * @param dateDebutActivite la date à partir de laquelle l'objet est actif
   */
  public void setDateDebutActivite(Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  /**
   * Retourne la date à partir de laquelle l'objet est actif
   * @return  la date à partir de laquelle l'objet est actif
   */
  public Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  /**
   * Fixe la date jusqu'à laquelle l'objet est actif
   * @param dateFinActivite la date jusqu'à laquelle l'objet est actif
   */
  public void setDateFinActivite(Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  /**
   * Retourne la date jusqu'à laquelle l'objet est actif
   * @return  la date jusqu'à laquelle l'objet est actif
   */
  public Date getDateFinActivite() {
    return dateFinActivite;
  }

  /**
   * Fixe le code de l'application associé à l'objet référentiel
   * @param codeApplication le code de l'application associé à l'objet référentiel
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code de l'application associé à l'objet référentiel
   * @return  le code de l'application associé à l'objet référentiel
   */
  public String getCodeApplication() {
    return codeApplication;
  }

}