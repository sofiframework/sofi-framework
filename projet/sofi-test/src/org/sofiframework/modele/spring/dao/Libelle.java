/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import org.sofiframework.objetstransfert.ObjetTransfert;

public class Libelle extends ObjetTransfert  {

  /**
   * 
   */
  private static final long serialVersionUID = -3687268000829262656L;
  private static final String[] CLE = { "cleLibelle", "codeLangue" };
  private LibellePK libellePK = new LibellePK();
  private String codeApplication;
  private String texteLibelle;
  private String aideContextuelle;

  public Libelle() {
    this.setCle(CLE);
  }

  public void setCleLibelle(String cleLibelle) {
    libellePK.setCleLibelle(cleLibelle);
  }


  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }


  public String getCodeApplication() {
    return codeApplication;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }


  public String getAideContextuelle() {
    return aideContextuelle;
  }


  public void setTexteLibelle(String texteLibelle) {
    this.texteLibelle = texteLibelle;
  }


  public String getTexteLibelle() {
    return texteLibelle;
  }

  public LibellePK getLibellePK() {
    return libellePK;
  }


  public void setLibellePK(LibellePK libellePK) {
    this.libellePK = libellePK;
  }


  public String getCleLibelle() {
    return libellePK.getCleLibelle();
  }


  public String getCodeLangue() {
    return libellePK.getCodeLangue();
  }


  public void setCodeLangue(String codeLangue) {
    libellePK.setCodeLangue(codeLangue);
  }
}