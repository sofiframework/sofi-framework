/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;

public class LibellePK implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1167576947998419035L;
  private String cleLibelle;
  private String codeLangue;

  public LibellePK() {
  }

  public LibellePK(String cleLibelle, String codeLangue) {
    this.cleLibelle = cleLibelle;
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }
  public String getCleLibelle() {
    return cleLibelle;
  }
  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  @Override
  public int hashCode() {
    return (cleLibelle + codeLangue).hashCode();
  }

  @Override
  public boolean equals(Object other) {
    return EqualsBuilder.reflectionEquals(this, other);
  }
}
