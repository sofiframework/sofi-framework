/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.util.Properties;

import oracle.jbo.ApplicationModule;
import oracle.jbo.common.ampool.SessionCookie;
import oracle.jbo.pool.ResourcePoolStatistics;

public class ApplicationModulePoolImpl extends oracle.jbo.common.ampool.ApplicationPoolImpl {

  /**
   * 
   */
  private static final long serialVersionUID = -6705897206757660539L;

  public ApplicationModulePoolImpl() {
  }

  @Override
  public void checkin(ApplicationModule applicationModule) {
    super.checkin(applicationModule);
  }

  @Override
  public ApplicationModule checkout() throws Exception {
    return super.checkout();
  }

  @Override
  public ApplicationModule checkout(String string) {
    return super.checkout(string);
  }

  @Override
  public ApplicationModule createNewInstance() throws Exception {
    return super.createNewInstance();
  }

  @Override
  protected Object createResource(Properties properties) {
    Object objet = super.createResource(properties);

    ResourcePoolStatistics poolStats = this.getResourcePoolStatistics();
    long available = poolStats.mAvgNumOfAvailableInstances;
    long unavailable = poolStats.mAvgNumOfUnavailableInstances;
    long total = poolStats.mAvgNumOfInstances;

    System.out.println("|------------------");
    System.out.println("Available   :" + available);
    System.out.println("Unavailable :" + unavailable);
    System.out.println("Total       :" + total);
    System.out.println("|------------------");

    return objet;
  }

  @Override
  public void releaseApplicationModule(SessionCookie sessionCookie, boolean b) {super.releaseApplicationModule(sessionCookie, b);
  }

  @Override
  public void releaseApplicationModule(SessionCookie sessionCookie, int i) {
    super.releaseApplicationModule(sessionCookie, i);
  }

  @Override
  protected void releaseInstance(ApplicationModule applicationModule) {
    super.releaseInstance(applicationModule);
  }

  @Override
  public void releaseInstances() {
    super.releaseInstances();
  }
}
