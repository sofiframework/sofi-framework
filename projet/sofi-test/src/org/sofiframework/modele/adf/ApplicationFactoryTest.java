/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.util.List;

import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.modele.adf.common.ModeleTest;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;

public class ApplicationFactoryTest extends AbstractSpringTest {

  public ApplicationFactoryTest(String nom) {
    super(nom);
  }

  @Override
  protected String[] getConfigurationsSpring() {
    return new String[]{"org/sofiframework/modele/adf/contexte.xml"};
  }

  public void testService() {
    ModeleTest modele = (ModeleTest) getBean("modele");
    ModeleTest service = (ModeleTest) modele.getServiceX();
    String test = service.getTest();
    assertNotNull(service);
    assertTrue(test.equals("X"));

    service = (ModeleTest) modele.getServiceX();
    String test2 = service.getTest();
    assertTrue(test2.equals("X"));

    for (int i = 0; i < 100000; i++) {
      assertNotNull(modele.getServiceX());
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void testGetX() {
    // Test de connection du modele
    for (int i = 0; i < 10; i++) {
      long debut = System.currentTimeMillis();
      ModeleTest modele = (ModeleTest) getBean("modele");
      String test = modele.getTest();
      assertNotNull(test);
      assertTrue(test.equals("X"));
      long fin = System.currentTimeMillis();
      System.out.println(fin - debut);
    }

    // Test de caches
    List listeStatut = GestionCache.getListeCleValeurs("listeStatut");
    assertNotNull(listeStatut);
    assertTrue(listeStatut.size() > 0);

    // Test d'un parametre du modèle
    String valeurOui = GestionParametreSysteme.getInstance().getString("oui");
    String valeurNon = GestionParametreSysteme.getInstance().getString("non");
    assertTrue(valeurOui.equals("OUI"));
    assertTrue(valeurNon.equals("NON"));
  }
}
