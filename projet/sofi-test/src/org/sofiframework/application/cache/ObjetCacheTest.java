/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.cache;

import java.text.Collator;
import java.util.Comparator;

import junit.framework.TestCase;

import org.sofiframework.composantweb.liste.AttributTri;

public class ObjetCacheTest extends TestCase {

  public void testTrierString() {
    ObjetCache cache = new ObjetCache() {
      /**
       * 
       */
      private static final long serialVersionUID = -3845454583052546683L;

      @Override
      public void chargerDonnees() {
        String[] lettres = new String[] { "z", "d", "a", "d" };

        for (int i = 0; i < lettres.length; i++) {
          this.put(new Integer(i), lettres[i]);
        }
      }
    };

    cache.chargerDonnees();

    System.out.println(cache.getCollection());

    cache.trierListeValeur();

    System.out.println(cache.getCollection());
    assertTrue(cache.getCollection().toString().equals("{2=a, 1=d, 3=d, 0=z}"));
  }

  public void testTrierBeanDescription() {
    ObjetCache cache = new ObjetCache() {
      /**
       * 
       */
      private static final long serialVersionUID = -8030776800599334119L;

      @Override
      public void chargerDonnees() {
        Object[] beans = new Object[] {
            new Bean(new Long(111), null),
            new Bean(new Long(888), "Zzzz"),
            new Bean(new Long(222), "zzaz"),
            new Bean(new Long(555), "Zaaa")
        };

        for (int i = 0; i < beans.length; i++) {
          this.put(new Integer(i), beans[i]);
        }
      }
    };

    cache.chargerDonnees();

    System.out.println(cache.getCollection());
    cache.trierListeValeur(new AttributTri[]{new AttributTri("description", true)});

    System.out.println(cache.getCollection());
    assertTrue(cache.getCollection().toString().equals(
        "{0={111 null}, 3={555 Zaaa}, 1={888 Zzzz}, 2={222 zzaz}}"));
  }

  public void testTrierBeanPlusieursAttributs() {
    ObjetCache cache = new ObjetCache() {
      /**
       * 
       */
      private static final long serialVersionUID = -3651219347309462667L;

      @Override
      public void chargerDonnees() {
        Object[] beans = new Object[] {
            new Bean(new Long(888), "SOFI"),
            new Bean(new Long(888), "Zframework"),
            new Bean(new Long(222), "Zframework"),
            new Bean(new Long(222), "SOFI")
        };

        for (int i = 0; i < beans.length; i++) {
          this.put(new Integer(i), beans[i]);
        }
      }
    };
    cache.chargerDonnees();

    System.out.println("Avant : " + cache.getCollection());

    AttributTri[] tri = new AttributTri[]{
        new AttributTri("description", true),
        new AttributTri("cle", true)
    };
    cache.trierListeValeur(tri);

    System.out.println("Apres : " + cache.getCollection());
    assertTrue(cache.getCollection().toString().equals(
        "{3={222 SOFI}, 0={888 SOFI}, 2={222 Zframework}, 1={888 Zframework}}"));
  }

  public void testTrierBeanAvecComparator() {
    ObjetCache cache = new ObjetCache() {
      /**
       * 
       */
      private static final long serialVersionUID = 7872620795233819061L;

      @Override
      public void chargerDonnees() {
        Object[] beans = new Object[] {
            new Bean(new Long(111), "a"),
            new Bean(new Long(888), "a"),
            new Bean(new Long(222), "a"),
            new Bean(new Long(555), "a")
        };

        for (int i = 0; i < beans.length; i++) {
          this.put(new Integer(i), beans[i]);
        }
      }
    };
    cache.chargerDonnees();

    System.out.println("Avant : " + cache.getCollection());

    cache.trierListeValeur(new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        Bean b1 = (Bean) o1;
        Bean b2 = (Bean) o2;
        return -(b1.getCle().compareTo(b2.getCle()));
      }

    });

    System.out.println("Apres : " + cache.getCollection());
    assertTrue(cache.getCollection().toString().equals(
        "{1={888 a}, 3={555 a}, 2={222 a}, 0={111 a}}"));
  }

  public void testTrierBeanPlusieursAttributsCollator() {
    ObjetCache cache = new ObjetCache() {
      /**
       * 
       */
      private static final long serialVersionUID = -7237830638996631562L;

      @Override
      public void chargerDonnees() {
        Object[] beans = new Object[] {
            new Bean(new Long(222), "Ad"),
            new Bean(new Long(222), "az"),
            new Bean(new Long(888), "Ab"),
            new Bean(new Long(888), "ac")
        };

        for (int i = 0; i < beans.length; i++) {
          this.put(new Integer(i), beans[i]);
        }
      }
    };
    cache.chargerDonnees();

    System.out.println("Avant : " + cache.getCollection());

    AttributTri[] tri = new AttributTri[]{
        new AttributTri("description", true),
        new AttributTri("cle", true)
    };
    Collator c = Collator.getInstance();
    c.setStrength(Collator.TERTIARY);
    cache.trierListeValeur(tri, c);

    System.out.println("Apres : " + cache.getCollection());
  }
}
