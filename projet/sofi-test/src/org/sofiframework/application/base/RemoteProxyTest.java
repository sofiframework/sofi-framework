/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.base;

import java.lang.reflect.UndeclaredThrowableException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import junit.framework.TestCase;

public class RemoteProxyTest extends TestCase {

  private Local creerLocalProxy() {
    return (Local) RemoteProxy.creerProxy(new RemoteImpl(), Local.class);
  }

  public void testHelloWorld() {
    Local proxy = creerLocalProxy();

    assertEquals("Hello, Duke", proxy.hello("Duke"));
    assertEquals("Hello, World", proxy.hello());
  }

  public void testThrowException() {
    try {
      creerLocalProxy().throwException();
      fail("SQLException expected");
    } catch (SQLException e) {
    }
  }
  public void testThrowUndeclaredException() {
    try {
      creerLocalProxy().throwUndeclaredException();
      fail("SQLException expected");
    } catch (UndeclaredThrowableException e) {
      assertEquals(RemoteException.class, e.getCause().getClass());
    }
  }

  public static interface Local {
    public String hello(String name);

    public String hello();

    public void throwException() throws SQLException;

    public void throwUndeclaredException();
  }

  public static interface Remote {
    public String hello(String name);

    public String hello() throws RemoteException;

    public void throwException() throws SQLException;

    public void throwUndeclaredException() throws RemoteException;
  }

  public static class RemoteImpl implements Remote {
    @Override
    public String hello(String name) {
      return "Hello, " + name;
    }

    @Override
    public String hello() throws RemoteException {
      return hello("World");
    }

    @Override
    public void throwException() throws SQLException {
      throw new SQLException("Message");
    }

    @Override
    public void throwUndeclaredException() throws RemoteException {
      throw new RemoteException("Message");
    }
  }
}
