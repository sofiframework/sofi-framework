/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.parametresysteme;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import junit.framework.TestCase;

public class MetaDonneeParametreTest extends TestCase {

  public void testLectureFichier() throws Exception {
    URL resource = this.getClass().getResource("parametre-sofi.xml");
    assertNotNull(resource);

    String path = resource.getPath();
    assertNotNull(path);

    MetaDonneeParametre mdp = new MetaDonneeParametre();
    List parametres = MetaDonneeParametre.getListeParametres(path);
    assertNotNull(parametres);
  }

  public void _test() throws IOException {
    String fichierParametres = "C:\\projets\\PortailSOFI\\Presentation\\public_html\\WEB-INF\\application-parametre-sofi.xml";

    List listeParametres = MetaDonneeParametre.getListeParametres(fichierParametres);

    assertTrue((listeParametres != null) && (listeParametres.size() > 0));
  }

  public void testRepertoireCourant() {
    File repertoirecourrant = new File("./");
    repertoirecourrant.getAbsolutePath();
  }
}
