/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.objetstransfert;

import java.text.ParseException;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportTraitementTest extends TestCase {

  public void testGetDateDebutPeriode() throws ParseException {
    RapportTraitement entite = new RapportTraitement();
    entite.setDateTraitement(DateUtils.parseDate("20130305 13:10:55", new String[] { "yyyyMMdd hh:mm:ss" }));
    entite.setNombrePeriode(4);

    assertEquals(DateUtils.parseDate("20130302 00:00:00", new String[] { "yyyyMMdd hh:mm:ss" }),
        entite.getDateDebutPeriode());
  }

  public void testGetDateFinPeriode() throws ParseException {
    RapportTraitement entite = new RapportTraitement();
    entite.setDateTraitement(DateUtils.parseDate("20130305 13:10:55", new String[] { "yyyyMMdd hh:mm:ss" }));
    entite.setNombrePeriode(4);

    assertEquals(DateUtils.parseDate("20130305 23:59:59,999", new String[] { "yyyyMMdd hh:mm:ss,SSS" }),
        entite.getDateFinPeriode());
  }
}
