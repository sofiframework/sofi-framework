<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>  

<div id="entete_utilisateur">
   <ul>
    <c:forEach items="${listeUtilisateurAccueil}" var="utilisateurSession">
      <li>
        <a class="actif" href="mailto:${utilisateurSession.utilisateur.courriel}">
          ${utilisateurSession.utilisateur.prenom}&nbsp;${utilisateurSession.utilisateur.nom}
        </a>
      </li>
    </c:forEach>
  </ul>
</div>