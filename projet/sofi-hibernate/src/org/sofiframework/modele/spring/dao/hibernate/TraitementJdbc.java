/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate;

import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.jdbc.Work;

/**
 * Classe qui permet de faciliter l'utilisation de la classe Work de hibernate.
 * Au lieu d'être une simple interface celle ci est implémenté avec une
 * propriété qui fourni directement une variable de retour.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class TraitementJdbc implements Work {

  /**
   * Résultat du traitement effectué.
   */
  private Object resultat;

  /**
   * Traitement qui doit être effectué avec la connexion jdbc.
   * 
   * @param connexion
   * @return
   * @throws SQLException
   */
  abstract public Object effectuer(Connection connexion) throws SQLException;

  @Override
  public void execute(Connection connexion) throws SQLException {
    this.resultat = this.effectuer(connexion);
  }

  public Object getResultat() {
    return this.resultat;
  }
}
