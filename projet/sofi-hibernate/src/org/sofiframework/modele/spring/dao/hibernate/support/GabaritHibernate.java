/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate.support;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.dao.ConvertisseurException;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * Gabarit qui permet l'exécution de code Hibernate tout en convertissant les
 * exception de l'engin de persistance vers un type générique à SOFI.
 * @author Jean-Maxime Pelletier
 */
public class GabaritHibernate extends HibernateTemplate {

  /**
   * Convertisseur des exceptions
   */
  private ConvertisseurException convertisseur = null;

  public GabaritHibernate(org.hibernate.SessionFactory gestionnaireSession){
    super(gestionnaireSession);
    this.convertisseur = creerConvertisseurException();
  }

  public GabaritHibernate(org.hibernate.SessionFactory gestionnaireSession, boolean permettreCreation) {
    super(gestionnaireSession, permettreCreation);
    this.convertisseur = creerConvertisseurException();
  }

  /**
   * Réécriture. Ajout de la conversion des types d'exceptions Spring vers SOFI
   * @return Exception Spring
   * @param hibernateExceptione Exception Hibernate
   */
  @Override
  public DataAccessException convertHibernateAccessException(HibernateException hibernateExceptione) {
    DataAccessException exception = super.convertHibernateAccessException(hibernateExceptione);

    throw convertir(exception);
  }

  /**
   * Réécriture. Ajout de la conversion des types d'exceptions Spring vers SOFI
   * @return Exception Spring
   * @param sqlException Exception SQL
   */
  @Override
  protected DataAccessException convertJdbcAccessException(SQLException sqlException) {
    DataAccessException exception = super.convertJdbcAccessException(sqlException);

    throw convertir(exception);
  }

  private ModeleException convertir(DataAccessException exception) {
    return convertisseur.convertirExceptionSpring(exception);
  }

  /**
   * Crée le convertisseur qui sera utilisé pour la conversion des exceptions.
   * @return Convertisseur d'exceptions
   */
  protected ConvertisseurException creerConvertisseurException() {
    return new ConvertisseurExceptionHibernate();
  }
}