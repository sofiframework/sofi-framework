/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate.support;
import org.sofiframework.modele.exception.JdbcException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.dao.ConvertisseurException;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateJdbcException;

/**
 * Convertie les exceptions Spring vers un type personnalisé. Gère les types
 * d'exceptions qui sont spécifiques à Hibernate.
 * @author Jean-Maxime Pelletier
 */
public class ConvertisseurExceptionHibernate extends ConvertisseurException  {

  public ConvertisseurExceptionHibernate() {
    super();
  }

  /**
   * Réécriture. Ajout de la conversion des exceptions Hibernate
   * @return Exception du modele
   * @param e Exception Spring
   */
  @Override
  public ModeleException convertirExceptionSpring(DataAccessException e) {

    ModeleException modeleException = null;

    if (e instanceof HibernateJdbcException)  {
      modeleException = new JdbcException(e.getMessage(), e);
    } else {
      modeleException = super.convertirExceptionSpring(e);
    }

    return modeleException;
  }
}