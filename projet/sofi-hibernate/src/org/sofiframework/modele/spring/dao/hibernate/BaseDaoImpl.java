/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.spring.dao.hibernate;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.composantweb.liste.AttributFiltre;
import org.sofiframework.composantweb.liste.AttributTri;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Operateur;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.dao.BaseDao;
import org.sofiframework.modele.spring.dao.hibernate.support.GabaritHibernate;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Accès de données de base qui utilise la persistance du projet Hibernate.
 * 
 * @author Jean-Maxime Pelletier
 * @author Guillaume Poirier
 */
public class BaseDaoImpl extends HibernateDaoSupport implements BaseDao {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(BaseDaoImpl.class);

  /**
   * Classe d'entité gérée par cet accès de données
   */
  private Class classe = null;

  /**
   * Nom de l'entité gérée par cet accès de données
   */
  private String nomEntite;

  /**
   * Indique si l'accès de données va effectué un flush suite aux opérations qui
   * modifient les données (create, update delete).
   */
  
  
  private ThreadLocal<Boolean> autoFlush = new ThreadLocal<Boolean>() {
    protected Boolean initialValue() {
        return Boolean.FALSE;
    }
  };
 

  /**
   * Construit un BaseDaoImpl en spécifiant seulement la classe. Le nom de
   * l'entité utilisé par ce contructeur est le nom complet de la classe.
   * 
   * @param classe
   *          la classe de l'entité pour le DAO
   */
  public BaseDaoImpl(Class classe) throws IllegalArgumentException, NullPointerException {
    this(classe, classe.getName());
  }

  /**
   * Construit un BaseDaoImpl en spécifiant explicitement le nom de l'entité.
   * 
   * @param classe
   *          la classe de l'entité pour le DAO
   * @param nomEntite
   *          Le nom de l'entité définit dans la mapping Hibernate
   */
  public BaseDaoImpl(Class classe, String nomEntite) {
    this.classe = classe;
    this.nomEntite = nomEntite;
    if (classe == null) {
      throw new NullPointerException("classe");
    }
    if (nomEntite == null) {
      throw new NullPointerException("nomEntite");
    }
  }

  /**
   * Déterminer si une entité existe à partir de sa clé. Si la cléest composée,
   * on peut passer l'entité elle même.
   * 
   * @param id
   *          Identifiant unique de l'entité
   * @return true si l'entité existe, false si non
   */
  @Override
  public boolean isExiste(Serializable id) {
    return (this.get(id) != null);
  }

  /**
   * Obtenir une entité à l'aide de son identifiant unique. Si l'entité n'est
   * pas trouvé la méthode retourne null.
   * 
   * @param id
   *          Identifiant
   * @return Entité correspondant à l'identifiant
   */
  @Override
  public Object get(Serializable id) {
    return getHibernateTemplate().get(nomEntite, id);
  }

  /**
   * Obtenir une entité à l,aide de son identifiant unique. Si l'entité n'est
   * pas trouvé la méthode lance une exception.
   * 
   * @param id
   *          Identifiant
   * @return Entité correspondant à l'identifiant
   */
  @Override
  public Object charger(Serializable id) {
    return getHibernateTemplate().load(nomEntite, id);
  }

  /**
   * Ajouter une nouvelle entité dans la base de données.
   * 
   * @param entite
   *          Nouvelle entité
   * @return l'identifiant géré pour la nouvelle entité
   */
  @Override
  public Serializable ajouter(Object entite) {
    Serializable id = getHibernateTemplate().save(nomEntite, entite);
    this.flushAuto();
    return id;
  }

  /**
   * Applique les modification apportées à l'entité dans la base de données.
   * 
   * @param entite
   *          Entité à modifier
   */
  @Override
  public void modifier(Object entite) {
    getHibernateTemplate().update(nomEntite, entite);
    this.flushAuto();
  }

  /**
   * Supprime l'entité qui correspond à l'identifiant spécifié.
   * 
   * @param id
   *          Identifiant de l'entité à supprimer
   */
  @Override
  public void supprimer(Serializable id) {
    getHibernateTemplate().delete(this.get(id));
    this.flushAuto();
  }

  /**
   * Obtenir la liste des toutes les entités de l'accès de données.
   * 
   * @return Liste d'entités
   */
  @Override
  public List getListe() {
    return getHibernateTemplate().loadAll(this.getClasse());
  }

  /**
   * Obtenir une liste à partir des critères d'un filtre.
   * 
   * @param filtre
   *          Filtre
   * @return Liste des entitées correspondant aux critères du filtre
   */
  @Override
  public List getListe(Filtre filtre) {
    DetachedCriteria criteria = creerCriteria();
    appliquerFiltre(criteria, filtre);
    return getHibernateTemplate().findByCriteria(criteria);
  }

  /**
   * Obtenir une liste à partir des critères d'un filtre.
   * 
   * @param filtre
   *          Filtre
   * @param tri
   *          ordre et propriété de tri
   * @return Liste des entitées correspondant aux critères du filtre
   */
  @Override
  public List getListe(Filtre filtre, Tri tri) {
    DetachedCriteria criteria = creerCriteria();
    appliquerTri(criteria, tri);
    appliquerFiltre(criteria, filtre);
    return getHibernateTemplate().findByCriteria(criteria);
  }

  protected ListeNavigation getListe(String hql, Object[] parametres, ListeNavigation ln) {
    int count = this.compterLigne(hql, parametres);
    ln.setNbListe(count);

    if (count > 0) {
      StringBuffer tri = appliquerTri(ln);
      hql += tri.toString();
      ln.setListe(this.getPage(hql, parametres, ln));
    }

    return ln;
  }

  /**
   * Définition d'une nouvelle méthode getListe afin de pouvoir prendre en
   * compte les paramètres par libelle, et non seulement par index. Cela nous
   * permet de passer en paramètre des listes.
   * 
   * @param hql
   *          Requête HQL à exécuter
   * @param params
   *          Paramètres à passer à la requête
   * @param ln
   *          {@link ListeNavigation} à construire
   * @return {@link ListeNavigation} populée
   */
  protected ListeNavigation getListe(String hql, Map<String, Object> params, ListeNavigation ln) {
    int count = this.compterLigne(hql, params);
    ln.setNbListe(count);

    if (count > 0) {
      StringBuffer tri = appliquerTri(ln);
      hql += tri.toString();
      ln.setListe(this.getPage(hql, params, ln));
    }

    return ln;
  }

  /**
   * @param ln
   *          {@link ListeNavigation}
   * 
   * @return Les attributs sur lesquels le tri est effectué, avec l'ordre de tri
   *         adéquat
   */
  protected StringBuffer appliquerTri(ListeNavigation ln) {
    StringBuffer tri = new StringBuffer();
    Collection attributs = ln.getTri().attributs();
    if (attributs != null && !attributs.isEmpty()) {
      tri.append(" ORDER BY ");
      for (Iterator i = attributs.iterator(); i.hasNext();) {
        AttributTri attribut = (AttributTri) i.next();
        tri.append(UtilitaireString.convertirPremiereLettreEnMinuscule(ln.getTri().getTriAttribut())).append(" ")
        .append(ln.getTri().getOrdreTri());
        if (i.hasNext()) {
          tri.append(", ");
        }
      }

      // Ajouter un tri sur la clé primaire
      try {
        Object entite = this.classe.newInstance();
        if (entite instanceof ObjetTransfert) {
          String[] attributsCle = ((ObjetTransfert) entite).getCle();
          if (attributsCle != null && attributsCle.length > 0) {
            for (int i = 0; i < attributsCle.length; i++) {
              tri.append(", ").append(UtilitaireString.convertirPremiereLettreEnMinuscule(attributsCle[i])).append(" ")
              .append(Tri.TRI_ASCENDANT);
            }
          }
        }
      } catch (Exception e) {
      }
    }

    return tri;
  }

  /**
   * Obtenir une liste d'entités à partir d'un HQL et d'une liste de paramètres.
   * 
   * @param hql
   *          Hibernate query language
   * @param parametres
   *          Paramètres du HQL
   * @return liste d'entités
   */
  protected List getListe(String hql, Object[] parametres) {
    return this.getHibernateTemplate().find(hql, parametres);
  }

  /**
   * Obtenir une liste d'entités à partir d'un HQL.
   * 
   * @param hql
   *          Hibernate query language
   * @return liste d'entités
   */
  protected List getListe(String hql) {
    return this.getHibernateTemplate().find(hql);
  }

  /**
   * Obtenir la liste des entités à partir d'une liste de navigation et de son
   * filtre. Il est possible d'ajouter des critère personnalisés avec le
   * callback CritereSupplementaire.
   * 
   * @param liste
   *          Liste de navigation
   * @param criteres
   *          Critères de recherche hibernate
   * @return liste d'entités paginée
   */
  protected ListeNavigation getListe(ListeNavigation ln, CritereSupplementaire critereSupplementaire) {
    Filtre filtre = ln.getFiltre();

    // Count du nombre de résultat
    DetachedCriteria criteresCount = this.appliquerFiltre(filtre);

    if (critereSupplementaire != null) {
      critereSupplementaire.ajouter(criteresCount);
    }

    int count = this.compterLigne(criteresCount);
    ln.setNbListe(count);

    if (ln.getMaxParPage() > 0) {
      try {
        BigDecimal maxParPage = new BigDecimal(ln.getMaxParPage());
        BigDecimal noPageMaximumTmp = new BigDecimal(count).divide(maxParPage);
        int noPageMaximum = noPageMaximumTmp.intValue();
        float reste = BigDecimal.valueOf(noPageMaximumTmp.doubleValue())
            .divideAndRemainder(BigDecimal.ONE)[1].floatValue();
        if (reste > 0) {
          noPageMaximum++;
        }
        if (noPageMaximum < ln.getNoPage()) {
          ln.setNoPage(ln.getNbPage() - 1);
        }
      } catch (Exception e) {
        // ne rien faire et laisser comportement par défaut.
      }
    }
    if (count > 0) {
      // Recherche de la page
      DetachedCriteria criteresPage = this.creerCriteria();
      this.appliquerFiltre(criteresPage, filtre);
      if (critereSupplementaire != null) {
        critereSupplementaire.ajouter(criteresPage);
      }
      this.appliquerTri(criteresPage, ln.getTri());
      List page = this.getPage(criteresPage, ln);
      ln.setListe(page);
    }

    return ln;
  }

  /**
   * Obtenir la liste des entités qui correspondent à la page décrite par la
   * liste navigation.
   * 
   * @param ln
   *          Liste de navigation
   * @return Liste de navigation
   */
  @Override
  public ListeNavigation getListe(ListeNavigation ln) {
    return this.getListe(ln, null);
  }

  /**
   * Créer un <code>DetachedCriteria</code> avec la classe de l'entité
   */
  protected DetachedCriteria creerCriteria() {
    return DetachedCriteria.forEntityName(nomEntite);
  }

  protected DetachedCriteria appliquerTri(Tri tri) {
    return appliquerTri(creerCriteria(), tri);
  }

  protected DetachedCriteria appliquerTri(DetachedCriteria criteria, Tri tri) {
    for (Iterator it = tri.attributs().iterator(); it.hasNext();) {
      AttributTri attribut = (AttributTri) it.next();
      criteria.addOrder(getOrder(attribut.isAscendant(), attribut.getNom()));
    }

    try {
      Object entite = this.classe.newInstance();
      if (entite instanceof ObjetTransfert) {
        String[] attributsCle = ((ObjetTransfert) entite).getCle();
        if (attributsCle != null && attributsCle.length > 0) {
          for (int i = 0; i < attributsCle.length; i++) {
            criteria.addOrder(getOrder(true, attributsCle[i]));
          }
        }
      }
    } catch (Exception e) {
    }

    return criteria;
  }

  protected DetachedCriteria appliquerFiltre(Filtre filtre) {
    return this.appliquerFiltre(creerCriteria(), filtre);
  }

  protected DetachedCriteria appliquerFiltre(DetachedCriteria criteria, Filtre filtre) {
    DetachedCriteria criteres = this.appliquerFiltre(criteria, filtre, null);
    return criteres;
  }
  
  protected DetachedCriteria appliquerFiltre(DetachedCriteria criteria, Filtre filtre, String nomEntite) {
    DetachedCriteria criteres = this.appliquerFiltre(criteria, filtre, nomEntite, null);
    return criteres;
  }

  /**
   * Applique le filtre selon le filtre reçu.
   * 
   * @param criteria
   * @param filtre
   * @param nomAttribut
   * @return
   */
  protected DetachedCriteria appliquerFiltre(DetachedCriteria criteria, Filtre filtre, String nomEntite, String nomAlias) {
    if (filtre == null) {
      log.debug("Aucun filtre définit");
    } else {
      if (log.isDebugEnabled()) {
        log.debug("Appliquer filtre: " + filtre);
      }

      Collection attributs = filtre.produireValeurs();
      if (nomEntite != null && !attributs.isEmpty()) {
        if (nomAlias != null) {
          criteria = criteria.createCriteria(nomEntite, nomAlias);
        }else {
          criteria = criteria.createCriteria(nomEntite);
        }
      }

      for (Iterator it = attributs.iterator(); it.hasNext();) {
        AttributFiltre attribut = (AttributFiltre) it.next();
        Object valeur = attribut.getValeur();
        String nom = attribut.getNom();

        if (valeur instanceof Filtre) {
          // La valeur est un objetFiltre, On relance l'appliquerFiltre avec les
          // bonnes valeurs.
          // Le nom dans le createCriteria doit absolument être le même que la
          // propriété lié à un autre classe
          this.appliquerFiltre(criteria, (Filtre) valeur, nom);
        } else if (valeur instanceof Iterable) {
          Iterable listeCritere = (Iterable) valeur;
          int i = 0;
          for (Object sousCritere : listeCritere) {
            // LIMITATION :  Le support des sous filtres est seulement pour 1 résultat dans une liste.
            if (sousCritere instanceof Filtre && i == 0) {          
              Filtre sousfiltre = (Filtre) sousCritere;
              this.appliquerFiltre(criteria, sousfiltre, nom, nom+String.valueOf(i));
              i++;
            }
          }
        } else {
          this.appliquerCritere(attribut, criteria, filtre, nomEntite);
        }
      }
    }
    return criteria;
  }

  protected void appliquerCritere(AttributFiltre attribut, DetachedCriteria criteria, Filtre filtre, String nomEntite) {
    String nom = attribut.getNom();
    Object valeur = attribut.getValeur();
    Operateur operateur = attribut.getOperateur();



    if (operateur == Operateur.EGALE) {
      if (valeur instanceof String) {
        String str = ((String) valeur).trim();
        if (str.length() != 0  && !"NULL".equals(valeur)) {
          criteria.add(Restrictions.ilike(nom, attribut.getMatchMode().toMatchString(str)));
        }else {
          criteria.add(Restrictions.isNull(nom));
        }
      } else if (valeur != null && valeur.getClass().isArray()) {
        Object[] listeValeur = null;

        /**
         * Si c'est un tableau d'objet on ne converti pas. Le type doit être le
         * même que l'entité dans le tableau
         */
        if (valeur instanceof Object[]) {
          listeValeur = (Object[]) valeur;

          /*
           * Si il s'agit d'un tableau de string il faut convertir dans le type
           * de la propriété de l'entité.
           */
        } else if (valeur instanceof String[]) {
          PropertyDescriptor prop = BeanUtils.getPropertyDescriptor(this.getClasse(), nom);
          Class type = prop.getPropertyType();
          String[] listeTemp = (String[]) valeur;
          listeValeur = new Object[listeTemp.length];
          boolean convertir = !String.class.isAssignableFrom(type);
          for (int i = 0; i < listeTemp.length; i++) {
            listeValeur[i] = convertir ? ConvertUtils.convert(listeTemp[i], type) : listeTemp[i];
          }
        }

        // on doit générer un in
        criteria.add(Restrictions.in(nom, listeValeur));

      } else if (valeur != null) {
        criteria.add(Restrictions.eq(nom, valeur));
      }
    } else if (operateur == Operateur.PLUS_GRAND) {
      criteria.add(Restrictions.gt(nom, valeur));
    } else if (operateur == Operateur.PLUS_GRAND_EGALE) {
      criteria.add(Restrictions.ge(nom, valeur));
    } else if (operateur == Operateur.PLUS_PETIT) {
      criteria.add(Restrictions.lt(nom, valeur));
    } else if (operateur == Operateur.PLUS_PETIT_EGALE) {
      criteria.add(Restrictions.le(nom, valeur));
    } else {
      throw new AssertionError();
    }
  }

  protected Order getOrder(boolean asc, String nomAttribut) {
    nomAttribut = UtilitaireString.convertirPremiereLettreEnMinuscule(nomAttribut);
    return asc ? Order.asc(nomAttribut) : Order.desc(nomAttribut);
  }

  protected int compterLigne(String hql, Object[] parametres) {
    if (hql.toUpperCase().startsWith("FROM")) {
      hql = "SELECT COUNT(*) " + hql;
    } else {
      hql = "SELECT COUNT(*) FROM (" + hql + ")";
    }

    int indexOrderBy = hql.toUpperCase().indexOf("ORDER BY");
    if (indexOrderBy != -1) {
      int indexFin = hql.indexOf(")", indexOrderBy) - 1;
      String avantOrderBy = hql.substring(0, indexOrderBy);
      if (indexFin != -2) {
        hql = avantOrderBy + hql.substring(indexFin);
      } else {
        hql = avantOrderBy;
      }
    }

    List liste = getHibernateTemplate().find(hql, parametres);
    Number nbr = (Number) liste.get(0);
    return nbr.intValue();
  }

  /**
   * Permet de connaitre le nombre de résultat total de la requête.
   * 
   * @param hql
   *          Requête HQL
   * @param params
   *          Paramètres passés à la requête
   * @return Le nombre de résultat de la requête
   */
  protected int compterLigne(String hql, Map<String, Object> params) {

    StringBuffer requete = new StringBuffer();
    Boolean resultatMultiple = construireRequeteCompte(requete, hql);

    int nbResultat = 0;
    try {
      Query q = this.getSession().createQuery(requete.toString());
      if (params != null) {
        Iterator<Map.Entry<String, Object>> entrees = params.entrySet().iterator();
        while (entrees.hasNext()) {
          Map.Entry<String, Object> entree = entrees.next();
          if (entree.getValue() instanceof Collection) {
            q.setParameterList(entree.getKey(), (Collection) entree.getValue());
          } else {
            q.setParameter(entree.getKey(), entree.getValue());
          }
        }
      }

      if (resultatMultiple) {
        Object[] resultat = (Object[]) q.iterate().next();
        List<Object> listeResultat = Arrays.asList(resultat);
        // Tri descendant de la liste
        Collections.sort(listeResultat, new Comparator<Object>() {

          @Override
          public int compare(Object o1, Object o2) {
            return ((Long) o2).compareTo((Long) o1);
          }
        });
        // Prendre le resultat le plus élevé (le premier de la liste triée)
        nbResultat = ((Long) listeResultat.get(0)).intValue();
      } else {
        nbResultat = ((Long) q.iterate().next()).intValue();
      }
    } catch (Exception e) {
      throw new ModeleException("Impossible d'executer le COUNT sur la requete : " + requete, e.getCause());
    }

    return nbResultat;
  }

  /**
   * Permet de construire la requete retournant le nombre de résultat dans une
   * liste.</br> On retourne un booleen permettant de savoir si le count est
   * fait sur une ou plusieurs valeurs.</br> Si le resultat est sur plusieurs
   * valeurs, il faut prendre le COUNT le plus grand, qui correspond au nombre
   * d'enregistrements dans la liste
   * 
   * @param requeteResultat
   *          Requete contenant le(s) COUNT
   * @param requeteInitiale
   *          Requete initiale à partir de laquelle on construit notre requete
   *          de compte
   * @return Vrai si le resultat contient plusieurs valeurs, Faux sinon
   */
  protected Boolean construireRequeteCompte(StringBuffer requeteResultat, String requeteInitiale) {

    Boolean resultatMultiple = Boolean.FALSE;
    requeteInitiale = requeteInitiale.trim();

    // Cas d'une requete sans SELECT
    if (requeteInitiale.startsWith("FROM")) {
      requeteResultat.append("SELECT COUNT(*) ").append(requeteInitiale);
    }
    // Cas d'une requete sans DISTINCT, on peut faire un COUNT(*)
    else if (!StringUtils.contains(requeteInitiale.substring(0, requeteInitiale.indexOf("FROM")), "DISTINCT")) {
      requeteResultat.append("SELECT COUNT(*) ").append(requeteInitiale.substring(requeteInitiale.indexOf("FROM")));
    }
    // Cas où le SELECT contient un DISTINCT
    else {
      String select = requeteInitiale.substring(requeteInitiale.indexOf(" "), requeteInitiale.indexOf("FROM"));
      // Si l'argument sur lequel est appliqué le DISTINCT est encadré par des
      // parenthèses, supprimer ces parenthèses sinon le COUNT ne fonctionnera
      // pas
      select = UtilitaireString.remplacerTous(select, "(", " ");
      select = UtilitaireString.remplacerTous(select, ")", " ");

      // Si le SELECT contient plusieurs argument, faire un COUNT sur chacun
      // d'eux
      if (StringUtils.contains(select, ",")) {
        resultatMultiple = Boolean.TRUE;
        String[] tabSelect = StringUtils.split(select, ",");
        requeteResultat.append("SELECT COUNT(" + tabSelect[0] + ")");
        for (int i = 1; i < tabSelect.length; i++) {
          requeteResultat.append(", COUNT(" + tabSelect[i] + ")");
        }
        requeteResultat.append(" " + requeteInitiale.substring(requeteInitiale.indexOf("FROM")));
      } else {
        requeteResultat.append("SELECT COUNT(" + select.trim() + ") ").append(
            requeteInitiale.substring(requeteInitiale.indexOf("FROM")));
      }
    }
    return resultatMultiple;
  }

  protected int compterLigne(String hql) {
    return this.compterLigne(hql, (Object[]) null);
  }

  protected int compterLigne(DetachedCriteria criteria) {
    List liste = getHibernateTemplate().findByCriteria(criteria.setProjection(Projections.rowCount()));
    return ((Number) liste.get(0)).intValue();
  }

  /**
   * Obtenir la classe gérée par cet accès de données
   * 
   * @return la classe gérée par cet accès de données
   */
  protected Class getClasse() {
    return classe;
  }

  /**
   * Retourne le nom de l'entité gérée par cet accès de données
   */
  protected String getNomEntite() {
    return nomEntite;
  }

  /**
   * Obtenir la page spécifié par la liste de navigation
   * 
   * @param criteria
   *          Critères de recherche
   * @param ln
   *          Liste de navagation
   * @return Liste des entités
   */
  protected List getPage(DetachedCriteria criteria, ListeNavigation ln) {
    return getHibernateTemplate().findByCriteria(criteria, ln.getPositionListe(), ln.getMaxParPage().intValue());
  }

  protected List getPage(String hql, Object[] parametres, ListeNavigation ln) {
    List page = null;
    try {
      Query q = this.getSession().createQuery(hql);
      if (parametres != null) {
        for (int i = 0; i < parametres.length; i++) {
          q.setParameter(i, parametres[i]);
        }
      }
      q.setFirstResult(ln.getPositionListe());
      q.setMaxResults(ln.getMaxParPage().intValue());
      page = q.list();
    } catch (HibernateException e) {
      throw this.convertHibernateAccessException(e);
    }
    return page;
  }

  /**
   * Méthode permettant de prendre en compte des paramètres setter par des
   * libelles. Prend en compte les listes dans les paramètres.
   * 
   * @param hql
   *          Requête HQL à exécuter
   * @param params
   *          Paramètres à passer à la requête
   * @param ln
   *          {@link ListeNavigation} à construire
   * @return La liste de résultat, à stocker dans la {@link ListeNavigation}
   */
  protected List getPage(String hql, Map<String, Object> params, ListeNavigation ln) {
    List page = null;
    try {
      Query q = this.getSession().createQuery(hql);
      if (params != null) {
        Iterator<Map.Entry<String, Object>> entrees = params.entrySet().iterator();
        while (entrees.hasNext()) {
          Map.Entry<String, Object> entree = entrees.next();
          if (entree.getValue() instanceof Collection) {
            q.setParameterList(entree.getKey(), (Collection) entree.getValue());
          } else {
            q.setParameter(entree.getKey(), entree.getValue());
          }
        }
      }
      q.setFirstResult(ln.getPositionListe());
      q.setMaxResults(ln.getMaxParPage().intValue());
      page = q.list();
    } catch (HibernateException e) {
      throw this.convertHibernateAccessException(e);
    }
    return page;
  }

  /**
   * Obtenir une liste des entités qui correspondent à l'exemple spécifié et
   * dans l'ordre spécifipar l'objet Tri.
   * 
   * @param critere
   *          Objet Example qui décrit les critères de recherche
   * @param tri
   *          Tri qui doit être appliqué à la liste
   * @return Liste d'entités
   */
  protected List getListe(Criterion critere, Tri tri) {
    return getHibernateTemplate().findByCriteria(appliquerTri(tri).add(critere));
  }

  /**
   * Obtenir une entité selon un criterion.
   * 
   * @param critere
   *          un criterion Hibernate
   * @return une entité selon un criterion ou <code>null</code> s'il n'en existe
   *         pas dans la base de données
   */
  protected Object get(Criterion critere) {
    return DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(creerCriteria().add(critere)));
  }

  /**
   * Surcharge qui permet d'utiliser le GabaritHibernate de SOFI au lieu du
   * Hibernate Template de Spring.
   * 
   * @return HibernateTemplate
   * @param gestionnaireSessionHibernate
   *          Gestionnaire de sessions Hibernate (SessionFactory)
   */
  @Override
  protected HibernateTemplate createHibernateTemplate(SessionFactory gestionnaireSessionHibernate) {
    return new GabaritHibernate(gestionnaireSessionHibernate);
  }

  /**
   * Permet d'effectuer des opérations sur les entités sans qu'elles soient
   * répercutés par la persistance Hibernate à la base de données.
   */
  protected void flushNever() {
    getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_NEVER);
  }

  /**
   * Permet de seulement faire le flush si le autoflush est activé.
   */
  protected void flushAuto() {
    if (isAutoFlush()) {
      this.getHibernateTemplate().flush();
    }
  }

  /**
   * Est-ce que l'on doit effectuer un flush suite aux opérations de
   * modification.
   * 
   * @return true on flush
   */
  public boolean isAutoFlush() {
    return autoFlush.get();
  }

  /**
   * Modifie la valeur du flush
   * 
   * @param autoFlush
   *          nouvelle valeur pour le autoflush
   */
  public void setAutoFlush(boolean autoFlush) {
    this.autoFlush.set(autoFlush);;
  }

  /**
   * Activer un Filter Hibernate.
   * 
   * @param nomFiltre
   *          Nom du filtre à activer
   * @param parametres
   *          Paramêtres à ajouter suite a l'activation du filtre
   */
  protected void activerFiltreSession(String nomFiltre, Map<String, Object> parametres) {
    try {
      Filter filtre = getSession().enableFilter(nomFiltre);
      if (parametres != null) {
        for (String nomParametre : parametres.keySet()) {
          Object valeur = parametres.get(nomParametre);
          filtre.setParameter(nomParametre, valeur);
        }
      }
    } catch (HibernateException e) {
      throw this.convertHibernateAccessException(e);
    }
  }

  /**
   * Appliquer un filtre hibernate avec un seul parametre.
   * 
   * @param nomFiltre
   * @param nomParametre
   * @param valeurParametre
   */
  protected void activerFiltreSession(String nomFiltre, String nomParametre, Object valeurParametre) {
    Map parametres = new HashedMap();
    parametres.put(nomParametre, valeurParametre);
    this.activerFiltreSession(nomFiltre, parametres);
  }

  /**
   * Activer un filtre hibebernate qui n'a pas de paramètres.
   * 
   * @param nomFiltre
   */
  protected void activerFiltreSession(String nomFiltre) {
    this.activerFiltreSession(nomFiltre, null);
  }

  /**
   * Effectuer un traitement avec une connexion jdbc.
   * 
   * @param tache
   */
  @SuppressWarnings("unchecked")
  protected void effectuerTraitementJdbc(final TraitementJdbc traitement) {
    this.getHibernateTemplate().execute(new HibernateCallback() {
      @Override
      public Object doInHibernate(Session session) throws HibernateException, SQLException {
        session.doWork(traitement);
        return traitement.getResultat();
      }
    });
  }

  protected String toInParam(String[] array) {
    return ArrayUtils.toString(array).replaceAll("[{]","'").replaceAll("[}]", "'").replaceAll("[,]", "','");
  }
  
  /**
   * Permet de détacher un objet de la session.
   */
  public void evict(Object entity){
    getHibernateTemplate().evict(entity);
  }
}
