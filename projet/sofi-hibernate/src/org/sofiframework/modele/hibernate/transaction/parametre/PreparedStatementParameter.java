/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.transaction.parametre;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.classic.Session;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Parametre de session BD basé sur un Statement à exécuter dans la base de données.
 * <p>
 * Il est possible de modifier la base de données directement afin que toutes les sessions possèdent certains paramètres par défaut,
 * cependant il arrive qu'il soit impossible à l'équipe de développement d'ajouter de nouveaux paramètres dans la BD. Il faut alors
 * modifier chaque session BD manuellement afin d'appliquer certains paramètres.
 * 
 * @author Pierre-Frédérick Duret
 * @see org.sofiframework.infrasofi.modele.transaction.parametre.ParametreSession
 * @see java.sql.PreparedStatement
 */
public abstract class PreparedStatementParameter implements ParametreSession {

  /** Variable permettant d'effecuter des logs applicatifs */
  private static final Log log = LogFactory.getLog(PreparedStatementParameter.class);

  /**
   * Méthode qui applique un statement SQL dans la session.
   */
  @Override
  @SuppressWarnings("deprecation")
  public void initSession(Session session) throws ModeleException {
    PreparedStatement statement = null;
    try {
      statement = session.connection().prepareStatement(this.getStatement());
      log.debug("Application du paramètre de session BD : " + this.getStatement());
      statement.execute();
    } catch (Exception e) {
      throw new ModeleException("Erreur lors de l'initialisation du paramètre : " + this.getClass().getName(), e);
    } finally {
      try {
        statement.close();
      } catch (SQLException e) {
        throw new ModeleException("Incapable de fermer le statement : " + this.getClass().getName(), e);
      }
    }
    log.debug("Paramètre : " + this.getStatement() + " appliqué avec succès.");
  }

  /**
   * Méthode qui retourne le statement à exécuter sur la session BD.
   * <p>
   * Cette méthode est abstraite et doit donc être overwrité afin de l'utiliser.
   * 
   * @return le statement à exécuter.
   */
  protected abstract String getStatement();

}
