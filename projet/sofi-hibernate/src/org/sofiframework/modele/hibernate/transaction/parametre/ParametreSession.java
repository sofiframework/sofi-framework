/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.transaction.parametre;

import org.hibernate.classic.Session;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Interface permettant d'appliquer des paramètres sur la session BD gérée par le gestionnaire de transaction de SOFI.
 * 
 * @author Pierre-Frédérick Duret
 * @see org.sofiframework.infrasofi.modele.transaction.TransactionManager
 */
public interface ParametreSession {

  public void initSession(Session session) throws ModeleException;
}
