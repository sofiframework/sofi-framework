/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.transaction.parametre.oracle;

import org.sofiframework.modele.hibernate.transaction.parametre.PreparedStatementParameter;


/**
 * Classe qui permet d'appliquer un paramètre sur la session BD qui permet d'effectuer des clauses where avec ou sans accents
 * et que le résultat soit le même.
 * <p>
 * Ce paramètre permet aussi de mettre les clauses where "case insensitive".
 * <p>
 * Par exemple, si on applique ce paramètre, la recherche de "like 'tel%' retournera "Téléphone", "telephone" et autre.
 * <p>
 * <b>ATTENTION :</b> Ce paramètre n'est valide que pour les bases de données <b>Oracle 10g R2 et plus</b>.
 * 
 * @author Pierre-Frédérick Duret
 */
public class ComparaisonAccentueEtCaseInsensitive extends PreparedStatementParameter {

  /**
   * Méthode qui retourne le statement qui permet de mettre la session "accent insensitive" et "case insensitive" pour les
   * clauses where de la BD.
   * 
   * @return le statement tel qu'il doit être exécuté dans la base de données.
   */
  @Override
  protected String getStatement() {
    return "ALTER SESSION SET NLS_COMP='LINGUISTIC'";
  }
}
