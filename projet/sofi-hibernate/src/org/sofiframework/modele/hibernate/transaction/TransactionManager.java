/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.transaction;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.modele.hibernate.transaction.parametre.ParametreSession;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;


/**
 * Gestionnaire des transaction qui encapsule les différentes
 * erreurs recu lors du commit en exception du modèle.
 * 
 * @author Jean-Maxime Pelletier
 */
public class TransactionManager extends HibernateTransactionManager {

  private static final long serialVersionUID = 6082974998222635569L;

  private static final Log log = LogFactory.getLog(TransactionManager.class);

  /** une collection de paramètres à appliquer sur la session BD */
  private List<ParametreSession> listeParametreSession;

  @Override
  protected void doBegin(Object transaction, TransactionDefinition definition) {
    if (log.isInfoEnabled()) {
      log.info("Begin transaction "
          + (definition.isReadOnly() ? "(ro)" : "")
          + "...");
    }

    super.doBegin(transaction, definition);

    this.initialiserParametresSession();
  }

  @Override
  protected void doCommit(DefaultTransactionStatus status) {
    if (log.isInfoEnabled()) {
      log.info("Commit transaction...");
    }
    super.doCommit(status);
    if (log.isInfoEnabled()) {
      log.info("Commit done");
    }
  }

  @Override
  protected void doRollback(DefaultTransactionStatus status) {
    if (log.isInfoEnabled()) {
      log.info("Rollback transaction :[...");
    }
    super.doRollback(status);
    if (log.isInfoEnabled()) {
      log.info("Rollback done");
    }
  }

  /**
   * Initialise les différents paramètres de la session en cours.
   * <p>
   * Les paramètres fixés ici servent à faire en sorte que la recherche du nom "éric" et "eric" retournent exactement le même résultat. L'autre
   * paramètre fait en sorte que les tri fonctionnent correctement (ie. "éric" est dans les "e" plutôt qu'après "z".
   */
  protected void initialiserParametresSession() {
    if (this.getListeParametreSession() != null && !this.getListeParametreSession().isEmpty()) {
      for (ParametreSession parametre : getListeParametreSession()) {
        parametre.initSession(this.getSessionFactory().getCurrentSession());
      }
    }
  }

  public void setListeParametreSession(List<ParametreSession> listeParametreSession) {
    this.listeParametreSession = listeParametreSession;
  }

  public List<ParametreSession> getListeParametreSession() {
    return listeParametreSession;
  }
}
