/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.transaction.parametre.oracle;

import org.sofiframework.modele.hibernate.transaction.parametre.PreparedStatementParameter;

/**
 * Classe qui permet d'appliquer un paramètre sur la session BD qui permet d'effectuer des tri avec accents comme si c'était des
 * caractères non accentués.
 * <p>
 * Par exemple, si on applique ce paramètre, le nom "éric" sera trié parmis les "eric".
 * 
 * @author Pierre-Frédérick Duret
 */
public class TriAccentueEtCaseInsensitive extends PreparedStatementParameter {

  /**
   * Méthode qui retourne le statement qui permet de mettre la session "accent insensitive" et "case insensitive" pour les
   * tri BD.
   * 
   * @return le statement tel qu'il doit être exécuté dans la base de données.
   */
  @Override
  protected String getStatement() {
    return "ALTER SESSION SET NLS_SORT='GENERIC_BASELETTER'";
  }

}
