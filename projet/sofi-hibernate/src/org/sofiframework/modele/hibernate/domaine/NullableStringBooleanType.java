package org.sofiframework.modele.hibernate.domaine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;

/**
 * Type Hibernate pour faire rendre un Objet Boolean nullable.
 * Hérite de {@link StringBooleanType} qui garde la gestion standard
 * du type oui_non et ajoute la possibilité d'avoir une valeur nullable
 * @author Sergio Couture
 */
public class NullableStringBooleanType extends StringBooleanType{

  private String yes = "O";

  private String no = "N";

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
      throws HibernateException, SQLException {
    // get Null si la valeur est null sinon vérifie le oui/non
    return rs.getString(names[0]) == null ? null : Boolean.valueOf(yes.equals(rs.getString(names[0])));
  }

  @Override
  public void nullSafeSet(PreparedStatement pstmt, Object value, int index)
      throws HibernateException, SQLException {
    // Set Null si la valeur est null sinon vérifie le oui/non
    pstmt.setString(index, value == null ? null : Boolean.TRUE.equals(value) ? yes : no);
  }

  @Override
  public void setParameterValues(Properties parameters) {
    String value;

    value = parameters.getProperty("yes");
    if (value != null) {
      yes = value;
    }

    value = parameters.getProperty("no");
    if (value != null) {
      no = value;
    }
  }
}
