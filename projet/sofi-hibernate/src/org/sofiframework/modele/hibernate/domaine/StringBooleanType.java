/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.domaine;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

public class StringBooleanType implements UserType, ParameterizedType {

  private String yes = "Y";

  private String no = "N";

  @Override
  public int[] sqlTypes() {
    return new int[] { Types.VARCHAR };
  }

  @Override
  public Class returnedClass() {
    return Boolean.class;
  }

  @Override
  public boolean equals(Object x, Object y) throws HibernateException {
    return x == null ? y == null : x.equals(y);
  }

  @Override
  public int hashCode(Object x) throws HibernateException {
    return x == null ? 0 : x.hashCode();
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
      throws HibernateException, SQLException {
    return Boolean.valueOf(yes.equals(rs.getString(names[0])));
  }

  @Override
  public void nullSafeSet(PreparedStatement pstmt, Object value, int index)
      throws HibernateException, SQLException {
    pstmt.setString(index, Boolean.TRUE.equals(value) ? yes : no);
  }

  @Override
  public Object deepCopy(Object value) throws HibernateException {
    return value;
  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Serializable disassemble(Object value) throws HibernateException {
    return (Boolean) value;
  }

  @Override
  public Object assemble(Serializable cached, Object owner)
      throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(Object original, Object target, Object owner)
      throws HibernateException {
    return original;
  }

  @Override
  public void setParameterValues(Properties parameters) {
    String value;

    value = parameters.getProperty("yes");
    if (value != null) {
      yes = value;
    }

    value = parameters.getProperty("no");
    if (value != null) {
      no = value;
    }
  }

}
