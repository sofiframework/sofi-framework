/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.intercepteur;

import java.io.Serializable;

import org.hibernate.type.Type;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Permet d'intercepter la sauvegarde des données d'une entité et de remplacer
 * les valeur String vide par des valeurs null.
 * 
 * Ceci permet d'éviter de placer des valeurs vides inutilement dans la base de
 * données.
 * 
 * @author Jean-Maxime Pelletier
 */
public class StringVideInterceptor extends InterceptorPlugin {

  private static final long serialVersionUID = 8985219609097656059L;

  /**
   * Lorsqu'une entité est flushée par hibernate et quelle se trouve à être
   * modifiée (dirty).
   */
  @Override
  public boolean onFlushDirty(Object entity, Serializable id,
      Object[] currentState, Object[] previousState, String[] propertyNames,
      Type[] types) {
    Boolean modifie = Boolean.FALSE;

    /*
     * On navige dans les propriétés qui sont en mise à jour. Si la valeur est
     * une string vide, on remplace cette valeur par null.
     */
    for (int i = 0; i < propertyNames.length; i++) {
      Object valeurPropriete = currentState[i];
      if (valeurPropriete instanceof String) {
        if (UtilitaireString.isVide(((String) valeurPropriete).trim())) {
          currentState[i] = null;
          modifie = Boolean.TRUE;
        }
      }
    }

    return modifie;
  }
}
