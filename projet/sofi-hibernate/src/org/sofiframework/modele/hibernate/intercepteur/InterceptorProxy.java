/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.intercepteur;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

/**
 * Permet de délaiguer les interceptions d'entités vers une liste de plugins. En
 * effet, Hibernate permet uniquement un seul entityInterceptor.
 * 
 * @author Jean-Maxime Pelletier
 */
public class InterceptorProxy extends EmptyInterceptor {

  private static final long serialVersionUID = -6441980765826802174L;

  private Set<InterceptorPlugin> interceptors = new LinkedHashSet<InterceptorPlugin>();

  @Override
  public boolean onFlushDirty(Object entity, Serializable id,
      Object[] currentState, Object[] previousState, String[] propertyNames,
      Type[] types) {
    Boolean modifie = Boolean.FALSE;

    if (!getInterceptors().isEmpty()) {
      for (InterceptorPlugin plugin : this.getInterceptors()) {
        Boolean pluginModifie = plugin.onFlushDirty(entity, id, currentState, previousState,
            propertyNames, types);
        modifie = modifie || pluginModifie;
      }
    }

    return modifie;
  }

  @Override
  public boolean onSave(Object entity, Serializable id, Object[] state,
      String[] propertyNames, Type[] types) {
    Boolean modifie = Boolean.FALSE;

    if (!getInterceptors().isEmpty()) {
      for (InterceptorPlugin plugin : this.getInterceptors()) {
        Boolean pluginModifie = plugin.onSave(entity, id, state, propertyNames, types);
        modifie = modifie || pluginModifie;
      }
    }

    return modifie;
  }

  public void setInterceptors(Set<InterceptorPlugin> interceptors) {
    this.interceptors = interceptors;
  }

  public Set<InterceptorPlugin> getInterceptors() {
    return interceptors;
  }
}
