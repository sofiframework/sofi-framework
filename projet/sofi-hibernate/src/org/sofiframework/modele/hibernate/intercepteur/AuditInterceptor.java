/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.intercepteur;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.Type;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;

/**
 * Intercepteur d'audit pour les objets du FQR.
 * <p>
 * Lors de la sauvegarde (en UPDATE ou INSERT) l'intercepteur est appelé avant
 * que la base de données soit modifiée. Les données de l'objet sont modifiées
 * et l'intercepteur va indiquer si une action a été faite avec le retour des
 * méthodes.<br>
 * L'entité reçu doit hérite de <code>gouv.fqr.sofi.modele.entite.Entite</code>.
 * 
 * @author Sergio Couture
 */
public class AuditInterceptor extends InterceptorPlugin {

  private static final long serialVersionUID = 9157835828486299537L;

  // Instance de logging pour l'intercepteur
  private static final Log log = LogFactory.getLog(AuditInterceptor.class);

  private String attributDateCreation;
  private String attributDateModification;
  private String attributCodeUtilisateurCreation;
  private String attributCodeUtilisateurModification;

  /**
   * GestionnaireUtilisateur injecté par Spring On a de besoin de ce
   * gestionnaire pour connaître l'utilisateur enregistrer dans le thread local.
   */
  private transient GestionnaireUtilisateur gestionnaireUtilisateur;

  /**
   * @return Un gestionnaireUtilisateur
   */
  public GestionnaireUtilisateur getGestionnaireUtilisateur() {
    return gestionnaireUtilisateur;
  }

  /**
   * Set le gestionnaireUtilisateur, utilisé par Spring
   * 
   * @param gestionnaireUtilisateur
   */
  public void setGestionnaireUtilisateur(
      GestionnaireUtilisateur gestionnaireUtilisateur) {
    this.gestionnaireUtilisateur = gestionnaireUtilisateur;
  }

  /**
   * Méthode appeler avant de faire les opérations sur la BD sur un UPDATE.
   * 
   * @param entity
   *          L'objet à modifier.
   * @param id
   *          L'id de l'objet.
   * @param currentState
   *          Les données de l'objet pour la sauvegarde.
   * @param previousState
   *          Les anciennes données. L'API suggère de ne jamais modifier ces
   *          données.
   * @param propertyNames
   *          Les noms des propriétés de l'objet. L'ordre est respecter avec
   *          currentState et previousState.
   * @param types
   *          Les types hibernate des propriétés (Int String ...)
   */
  @Override
  public boolean onFlushDirty(Object entity, Serializable id,
      Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
    Boolean modifie = Boolean.FALSE;

    if (log.isDebugEnabled()) {
      log.debug("Ajout de la date et du code utilisateur à l'INSERT "
          + entity.getClass());
    }

    this.setValue(currentState, propertyNames, this.attributDateModification, new Date());
    modifie = Boolean.TRUE;

    // Si il s'agit d'une modification
    if (this.isUtilisateurPresent()) {
      this.setValue(currentState, propertyNames,
          this.attributCodeUtilisateurModification, this
          .getCodeUtilisateur());
    }

    return modifie;
  }

  /**
   * Méthode appeler avant de faire les opérations sur la BD sur un INSERT.
   * 
   * @param entity
   *          L'entité à ajouter.
   * @param id
   *          L'id de l'objet
   * @param state
   *          Les données de l'objet à ajouter
   * @param propertyNames
   *          Les noms des propriétés de l'objet. L'ordre est respecter avec
   *          currentState et previousState.
   * @param typesLes
   *          types hibernate des propriétés (Int String ...)
   */
  @Override
  public boolean onSave(Object entity, Serializable id, Object[] state,
      String[] propertyNames, Type[] types) {
    Boolean modifie = Boolean.FALSE;

    if (log.isDebugEnabled()) {
      log.debug("Ajout de la date et du code utilisateur à l'INSERT "
          + entity.getClass());
    }

    try {
      // Si la propriété dateCreation est déjà renseigné, ne rien faire
      if (BeanUtils.getProperty(entity, this.attributDateCreation) == null) {
        this.setValue(state, propertyNames, this.attributDateCreation, new Date());
        modifie = Boolean.TRUE;
      }
    } catch (IllegalAccessException e) {
      this.setValue(state, propertyNames, this.attributDateCreation, new Date());
      modifie = Boolean.TRUE;
    } catch (InvocationTargetException e) {
      this.setValue(state, propertyNames, this.attributDateCreation, new Date());
      modifie = Boolean.TRUE;
    } catch (NoSuchMethodException e) {
      this.setValue(state, propertyNames, this.attributDateCreation, new Date());
      modifie = Boolean.TRUE;
    }

    if (this.isUtilisateurPresent()) {
      this.setValue(state, propertyNames, this.attributCodeUtilisateurCreation,
          this.getGestionnaireUtilisateur().getUtilisateur()
          .getCodeUtilisateur());
    }

    return modifie;
  }

  /**
   * L'array de String currentState contient les données de l'entité qui vont
   * être enregistrer. L'array propertyNames contient le noms de l'entité.
   * 
   * @param currentState
   * @param propertyNames
   * @param propertyToSet
   * @param value
   * @see http://java.dzone.com/articles/using-a-hibernate-interceptor-
   */
  private void setValue(Object[] currentState, String[] propertyNames,
      String propertyToSet, Object value) {
    int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
    if (index >= 0) {
      currentState[index] = value;
    }
  }

  private Boolean isUtilisateurPresent() {
    return this.getGestionnaireUtilisateur() != null
        && this.getGestionnaireUtilisateur().getUtilisateur() != null;
  }

  private String getCodeUtilisateur() {
    return this.getGestionnaireUtilisateur().getUtilisateur()
        .getCodeUtilisateur();
  }

  public String getAttributDateCreation() {
    return attributDateCreation;
  }

  public void setAttributDateCreation(String attributDateCreation) {
    this.attributDateCreation = attributDateCreation;
  }

  public String getAttributDateModification() {
    return attributDateModification;
  }

  public void setAttributDateModification(String attributDateModification) {
    this.attributDateModification = attributDateModification;
  }

  public String getAttributCodeUtilisateurCreation() {
    return attributCodeUtilisateurCreation;
  }

  public void setAttributCodeUtilisateurCreation(
      String attributCodeUtilisateurCreation) {
    this.attributCodeUtilisateurCreation = attributCodeUtilisateurCreation;
  }

  public String getAttributCodeUtilisateurModification() {
    return attributCodeUtilisateurModification;
  }

  public void setAttributCodeUtilisateurModification(
      String attributCodeUtilisateurModification) {
    this.attributCodeUtilisateurModification = attributCodeUtilisateurModification;
  }
}