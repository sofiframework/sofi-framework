/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.intercepteur;

import java.io.Serializable;

import org.hibernate.type.Type;

/**
 * Permet d'ajouter des foncitonnalités modulaire à la class d'intercepteur
 * d'entités de Hibernate. Comme il est impossible d'ajouter plus d'un
 * intercepteur, la classe InterceptorProxy délaigue des appels vers une liste
 * de plugins.
 * 
 * @author Jean-Maxime Pelletier
 */
public class InterceptorPlugin {

  public boolean onFlushDirty(Object entity, Serializable id,
      Object[] currentState, Object[] previousState, String[] propertyNames,
      Type[] types) {
    return Boolean.FALSE;
  }

  public boolean onSave(Object entity, Serializable id, Object[] state,
      String[] propertyNames, Type[] types) {
    return Boolean.FALSE;
  }
}
