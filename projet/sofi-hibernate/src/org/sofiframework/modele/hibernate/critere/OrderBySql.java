/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.hibernate.critere;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;

/**
 * Un objet qui permet de générer une clause 
 * SQL Order By à partir d'un SQL.
 * 
 * Order Object that takes an SQL statement 
 * to generate a criteria order by clause.
 * 
 * @author Jean-Maxime Pelletier
 */
public class OrderBySql extends Order {

  private static final long serialVersionUID = 2934637982311210240L;

  private String formula;

  protected OrderBySql(String formula) {
    super(formula, true);
    this.formula = formula;
  }

  @Override
  public String toString() {
    return formula;
  }

  @Override
  public String toSqlString(Criteria criteria, CriteriaQuery query) throws HibernateException {
    return formula;
  }

  
  public static Order formula(String formula) {
    return new OrderBySql(formula);
  }
}