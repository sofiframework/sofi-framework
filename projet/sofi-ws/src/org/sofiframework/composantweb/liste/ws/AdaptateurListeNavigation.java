/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste.ws;

import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.exception.ModeleException;

public class AdaptateurListeNavigation {

  public static org.sofiframework.composantweb.liste.ws.ListeNavigation convertir(ListeNavigation liste, Class classeListeNavigationWs) {
    org.sofiframework.composantweb.liste.ws.ListeNavigation listeWs = null;
    try {
      listeWs = (org.sofiframework.composantweb.liste.ws.ListeNavigation) classeListeNavigationWs.newInstance();
      listeWs.setListe(liste.getListe());
      listeWs.setDebutListe(liste.getDebutListe());
      listeWs.setFiltre(liste.getFiltre());
      listeWs.setMaxParPage(liste.getMaxParPage());
      listeWs.setNbListe(liste.getNbListe());
      listeWs.setNoPage(liste.getNoPage());
      listeWs.setPageCourante(liste.getPageCourante());
      listeWs.setTri(liste.getTri());
    } catch (Exception e) {
      throw new ModeleException("Erreur pour convertir la liste.", e);
    }

    return listeWs;
  }

  public static ListeNavigation convertir(org.sofiframework.composantweb.liste.ws.ListeNavigation listeWs) {
    ListeNavigation liste = new ListeNavigation();
    liste.setListe(listeWs.getListe());
    liste.setDebutListe(listeWs.getDebutListe());
    liste.setFiltre((Filtre) listeWs.getFiltre());
    liste.setMaxParPage(listeWs.getMaxParPage());
    liste.setNbListe(listeWs.getNbListe());
    liste.setNoPage(listeWs.getNoPage());
    liste.setTri(listeWs.getTri());
    return liste;
  }
}
