/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.composantweb.liste.ws;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.composantweb.liste.Tri;


/**
 * Liste de navigation qui peut être interprétée pour générer
 * un WSDL. Les interfaces de services qui sont exposée en
 * services web doivent utiliser des classes de
 * ListeNavigation qui sont étendues de cette classe.
 * Une classe adapteur permet de facilement convertir
 * les listenavigation en provenance du modèle.
 * 
 * La classe O est las classe concrete de l'objet
 * de transfert qui est contenu dans la liste.
 * 
 * La classe F est la classe de filtre qui est passée
 * à la couche de logique d'affaire pour restraindre
 * les items de la liste.
 * <p>
 * @author Jean-Maxime Pelletier
 */
public abstract class ListeNavigation<O, F> {

  private F filtre;

  private Tri tri = new Tri();

  /**
   * La liste de navigation
   */
  private List<O> liste;

  /** Le nombre d'items dans la liste complète */
  private long nbListe;

  /** La page courante */
  private int pageCourante;

  /** Le maximum d'items par page */
  private Integer maxParPage;

  /** Le début de la page en cours */
  private int debutListe;

  /** Le no de page a traiter **/
  private int noPage;

  /**
   * Le constructeur de la liste qui permet la navigation page par page.
   * <p>
   * Initialisation de la liste par défaut
   */
  public ListeNavigation() {
    liste = new ArrayList<O>();
  }

  /**
   * Constructeur pour utilisation en mode seul.
   * @param noPage le numéro de page demandé, 1, 2, 3, etc...
   * @param maxParPage le maximum par page
   */
  public ListeNavigation(int noPage, int maxParPage) {
    this.noPage = noPage;
    this.maxParPage = new Integer(maxParPage);
    this.liste = new ArrayList<O>();
  }

  public F getFiltre() {
    return filtre;
  }

  public void setFiltre(F filtre) {
    this.filtre = filtre;
  }

  public Tri getTri() {
    return tri;
  }

  public void setTri(Tri tri) {
    this.tri = tri;
  }

  public List<O> getListe() {
    return liste;
  }

  public void setListe(List<O> liste) {
    this.liste = liste;
  }

  public long getNbListe() {
    return nbListe;
  }

  public void setNbListe(long nbListe) {
    this.nbListe = nbListe;
  }

  public int getPageCourante() {
    return pageCourante;
  }

  public void setPageCourante(int pageCourante) {
    this.pageCourante = pageCourante;
  }

  public Integer getMaxParPage() {
    return maxParPage;
  }

  public void setMaxParPage(Integer maxParPage) {
    this.maxParPage = maxParPage;
  }

  public int getDebutListe() {
    return debutListe;
  }

  public void setDebutListe(int debutListe) {
    this.debutListe = debutListe;
  }

  public int getNoPage() {
    return noPage;
  }

  public void setNoPage(int noPage) {
    this.noPage = noPage;
  }
}